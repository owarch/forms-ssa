
proc sql; 
create table temp as 
   select name 
     from dictionary.macros 
    where scope='GLOBAL' and lowcase(name) like ("l_%");
quit;

%global m_ltrvars;
%let m_ltrvars= ;
data _null_;
   length ltrvars $10000.;
   set temp end=endf;
   retain ltrvars;
   ltrvars=trim(left(ltrvars))||' '||trim(left(name));
   if endf then do;
      call symput('m_ltrvars',trim(left(ltrvars)));
   end;
run;

