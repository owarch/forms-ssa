
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
 * FORMSInitTask - Ant Task Class.  Performs FORMS database and keystore 
 *                 initialization
 */

package com.owarchitects.forms.ssa.build;

import com.owarchitects.forms.commons.db.*;
import com.owarchitects.forms.commons.comp.*;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import java.io.*;
import java.util.*;
import java.net.*;
import java.math.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.mchange.v2.c3p0.ComboPooledDataSource;

import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import java.security.spec.RSAPublicKeySpec;
import javax.security.auth.x500.*;
import javax.security.cert.*;
import org.bouncycastle.x509.*;
import org.bouncycastle.jce.provider.*;
import org.apache.tools.ant.loader.*;


public class FORMSInitTask extends Task {

   // Application Base Directory
   private String apploc; 
   // applicatonContext.xml file path
   private String ctxfile; 
   // KeyStore file path
   private String ksfile; 

   public void execute() throws BuildException {

     // This code helped fix problems where ant and spring were using different classloaders
     // Was getting ClassCastExceptions where a instance could not be cast to its own class

     AntClassLoader2 antClassLoader = null;
     Object obj = this.getClass().getClassLoader();
     if (obj instanceof AntClassLoader2) {
         antClassLoader = (AntClassLoader2) obj;
         antClassLoader.setThreadContextLoader();
     }

     Console console = System.console();

     console.format("\n\nApplication context file:  %1$s\n",ctxfile);
     console.format("Keystore file:  %1$s\n\n",ksfile);

     // Get main database password from user
     char[] mdpassword = console.readPassword("Please enter FORMSMainDB password:");
     console.format("FORMSMainDB password = [%s]%n", new String(mdpassword));
     console.format("\n");

     // Get keystore password from user
     char[] kspassword = console.readPassword("Please enter keystore password:");
     console.format("keystore password = [%s]%n", new String(kspassword));
     console.format("\n");

     // Get formskey password from user
     char[] fkpassword = console.readPassword("Please enter FORMSKey (encryption key) password:");
     console.format("FORMSKey password = [%s]%n", new String(fkpassword));
     console.format("\n");

     // Get admin account name from user
     String acctname = console.readLine("Please enter initial administration account name:");
     console.format("Initial administration account = [%s]%n", new String(acctname));
     console.format("\n");

     // Get admin account password
     char[] acctpass = console.readPassword("Please enter administration account password:");
     console.format("administration account password = [%s]%n", new String(acctpass));
     console.format("\n");


     // Read applicationContext, insert appPath & FORMSMainDB password
     FileSystemXmlApplicationContext fctx = new FileSystemXmlApplicationContext(ctxfile);

     Map map=System.getenv();
     Iterator i=map.entrySet().iterator();
     while (i.hasNext()) {
        Map.Entry e=(Map.Entry)i.next();
        System.out.println(e.getKey() + " -- " + e.getValue());
     }
     map=System.getProperties();
     i=map.entrySet().iterator();
     while (i.hasNext()) {
        Map.Entry e=(Map.Entry)i.next();
        System.out.println(e.getKey() + " -- " + e.getValue());
     }

      // Make sure spring uses the same class loader as the classes loaded in this prog
      fctx.setClassLoader(this.getClass().getClassLoader());
      fctx.refresh();
      ApplicationContext ctx = (ApplicationContext)fctx;
      
      ComboPooledDataSource mds=(ComboPooledDataSource)ctx.getBean("mainDataSource");
      mds.setJdbcUrl(mds.getJdbcUrl().replace("$appPath",apploc));
      mds.setPassword(new String(mdpassword));
      mds.setOverrideDefaultPassword(new String(mdpassword));

      console.format("\n\nMain Data Source:  %1$s\n\n",mds.getJdbcUrl());

      ComboPooledDataSource eds=(ComboPooledDataSource)ctx.getBean("embeddedDataSource");
      eds.setJdbcUrl(eds.getJdbcUrl().replace("$appPath",apploc));
      console.format("\n\nEmbedded Data Source:  %1$s\n\n",eds.getJdbcUrl());

      // Back up existing keystore file if it exists 
      File ksf=new File(ksfile);
      if (ksf.exists()) {
         ksf.renameTo(new File(ksf.getParentFile(),ksf.getName().replaceFirst("\\.",".bak" + new Long(System.currentTimeMillis()).toString() + ".")));
      }
      // Create keystore & FORMSKey
      KeyStore ks=null;
      SecretKey formskey=null;
      try {
         console.format("\nCreating FORMSKey");
       
         ks=java.security.KeyStore.getInstance("JCEKS");
         KeyGenerator kg = KeyGenerator.getInstance("DESede");
         formskey=kg.generateKey();
         ks.load(null,kspassword);
         ks.setKeyEntry("formskey",formskey,fkpassword,null);
         FileOutputStream f = new FileOutputStream(ksf);
         ks.store(f,kspassword);

         console.format("....Done\n\n");
      } catch(Exception e) {
         System.out.println(Stack2string.getString(e));
         System.out.println("\n\nERROR:  Couldn't create FORMSKey");
         System.exit(1);
      }

      // Create forms internal key
      SecretKey formsinternal=null;
      try {
         console.format("\nCreating FORMSInternal Key");
       
         //ks=java.security.KeyStore.getInstance("JCEKS");
         //ks.load(null,kspassword);
         KeyGenerator kg = KeyGenerator.getInstance("DESede");
         formsinternal=kg.generateKey();
         ks.setKeyEntry("formsinternal",formsinternal,kspassword,null);
         FileOutputStream f = new FileOutputStream(ksf);
         ks.store(f,kspassword);

         console.format("....Done\n\n");
      } catch(Exception e) {
         System.out.println(Stack2string.getString(e));
         System.out.println("\n\nERROR:  Couldn't create FORMSInternal Key");
         System.exit(1);
      }

      // Create administrator account keypair
      PublicKey pubkey=null;
      PrivateKey privkey=null;
      try {
         console.format("\nCreating administrator account KeyPair");
 
         KeyPairGenerator kpg=KeyPairGenerator.getInstance("RSA");
         kpg.initialize(512);
         KeyPair kp=kpg.generateKeyPair();
         pubkey=kp.getPublic();
         privkey=kp.getPrivate();
         ks.setKeyEntry(acctname + "_private",privkey,acctpass,getCertChain());
         ks.setKeyEntry(acctname + "_public",pubkey,"public".toCharArray(),null);
         FileOutputStream f = new FileOutputStream(ksf);
         ks.store(f,kspassword);

         console.format("....Done\n\n");
      
      } catch (Exception e) {
         System.out.println(Stack2string.getString(e));
         System.out.println("\n\nERROR:  Couldn't create administrator key pair");
         System.exit(1);
      }

      console.format("\nBegin populating database...\n\n");
      console.format("\nCreate administrator account records");

      // Populate database
      try {

         Embeddedusers euser=new Embeddedusers();

         AccessDAO accessDAO=(AccessDAO)ctx.getBean("accessDAO");
         Access a=new Access();
         a.setFkpassword(EncryptUtil.encryptString(new String(fkpassword),formsinternal));
         a.setDbpassword(EncryptUtil.encryptString(new String(mdpassword),formsinternal));
         accessDAO.save(a);

         LocalusersDAO localusersDAO=(LocalusersDAO)ctx.getBean("localusersDAO");
         AllusersDAO allusersDAO=(AllusersDAO)ctx.getBean("allusersDAO");
         AllinstDAO allinstDAO=(AllinstDAO)ctx.getBean("allinstDAO");
         EmbeddedusersDAO embeddedusersDAO=(EmbeddedusersDAO)ctx.getBean("embeddedusersDAO");
         MainDAO mainDAO=(MainDAO)ctx.getBean("mainDAO");

         euser.setUsername(acctname);
         euser.setPassword(PasswordUtil.pwEncrypt(new String(acctpass)));
         euser.setIsdisabled(false);
         euser.setBadlogincount(new Integer(0));
         euser.setFkpassword(EncryptUtil.encryptString(new String(fkpassword),pubkey));
         euser.setDbpassword(EncryptUtil.encryptString(new String(mdpassword),pubkey));
         embeddedusersDAO.save(euser);

         Localusers luser=new Localusers();
         luser.setUsername(acctname);
         luser.setPassworddt(new Date());
         luser.setUsernumber("999");
         luser.setFullname("FORMS Administrator");
         luser.setEmailaddr("");
         luser.setIsadmin(true);
         luser.setIstemppassword(false);
         luser.setIsdeleted(false);
         localusersDAO.save(luser);

         Allinst ainst=new Allinst();
         Allusers auser=new Allusers();

         ainst.setIslocal(true);
         allinstDAO.save(ainst);

         auser.setAllinst(ainst);
         auser.setLuserid(luser.getLuserid());
         auser.setUserdesc(luser.getUsername() + " [LOCAL]");
         auser.setIsdeleted(false);
         allusersDAO.save(auser);

         //

         console.format("....Done\n\n");

         console.format("\nCreate ALLUSERS system role");

         Resourceroles allusersrole=new Resourceroles();
         allusersrole.setRolelevel(VisibilityConstants.SYSTEM);
         allusersrole.setRoleacr("ALLUSERS");
         allusersrole.setRoledesc("All Users");
         mainDAO.save(allusersrole);

         Rroleuserassign rroleassign=new Rroleuserassign();
         rroleassign.setUser(auser);
         rroleassign.setRole(allusersrole);
         mainDAO.save(rroleassign);

         console.format("....Done\n\n");

         /////
         /////

         console.format("\nCreate Permission categories");
         
         Permissioncats p1=new Permissioncats();
         p1.setPcatvalue(PcatConstants.SYSTEM);
         p1.setPcatscope(PcatscopeConstants.GLOBAL);
         p1.setPcatdesc("System Permissions");
         p1.setPcatorder(1);
         mainDAO.save(p1);

         Permissioncats p2=new Permissioncats();
         p2.setPcatvalue(PcatConstants.SITE);
         p2.setPcatscope(PcatscopeConstants.GLOBAL);
         p2.setPcatdesc("Site-Level Permissions");
         p2.setPcatorder(2);
         mainDAO.save(p2);

         Permissioncats p3=new Permissioncats();
         p3.setPcatvalue(PcatConstants.STUDY);
         p3.setPcatscope(PcatscopeConstants.GLOBAL);
         p3.setPcatdesc("Study-Level Permissions");
         p3.setPcatorder(3);
         mainDAO.save(p3);

         Permissioncats p4=new Permissioncats();
         p4.setPcatvalue(PcatConstants.FORM_DATATABLE);
         p4.setPcatscope(PcatscopeConstants.RESOURCE);
         p4.setPcatdesc("Form/Datatable Permissions");
         p4.setPcatorder(4);
         mainDAO.save(p4);

         Permissioncats p5=new Permissioncats();
         p5.setPcatvalue(PcatConstants.FORMTYPE);
         p5.setPcatscope(PcatscopeConstants.RESOURCE);
         p5.setPcatdesc("Formtype Permissions");
         p5.setPcatorder(5);
         mainDAO.save(p5);

         Permissioncats p6=new Permissioncats();
         p6.setPcatvalue(PcatConstants.REPORT);
         p6.setPcatscope(PcatscopeConstants.RESOURCE);
         p6.setPcatdesc("Reporting Program Permissions");
         p6.setPcatorder(6);
         mainDAO.save(p6);

         Permissioncats p7=new Permissioncats();
         p7.setPcatvalue(PcatConstants.LETTER);
         p7.setPcatscope(PcatscopeConstants.RESOURCE);
         p7.setPcatdesc("Form Letter Permissions");
         p7.setPcatorder(7);
         mainDAO.save(p7);

         Permissioncats p8=new Permissioncats();
         p8.setPcatvalue(PcatConstants.MEDIA);
         p8.setPcatscope(PcatscopeConstants.RESOURCE);
         p8.setPcatdesc("Document/Media Permissions");
         p8.setPcatorder(8);
         mainDAO.save(p8);

         Permissioncats p11=new Permissioncats();
         p11.setPcatvalue(PcatConstants.ANALYST_SYSTEM);
         p11.setPcatscope(PcatscopeConstants.GLOBAL);
         p11.setPcatdesc("Researcher/Analyst Content Management - System Permissions");
         p11.setPcatorder(11);
         mainDAO.save(p11);

         Permissioncats p12=new Permissioncats();
         p12.setPcatvalue(PcatConstants.ANALYST_RESOURCE);
         p12.setPcatscope(PcatscopeConstants.RESOURCE);
         p12.setPcatdesc("Researcher/Analyst Content Management - Resource Permissions");
         p12.setPcatorder(12);
         mainDAO.save(p12);


         console.format("....Done\n\n");

         console.format("\nCreate Permissions");

         com.owarchitects.forms.commons.db.Permissions perm;

         ////
         ////
         ////
         ////

         int pos;
         
         pos=1;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("ACCESSCONTENT");
         perm.setPermdesc("Access Site/Study Related Content");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("INTERNALVIEW");
         perm.setPermdesc("Internal User View");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("ACCTCREATE");
         perm.setPermdesc("Create new user accounts");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("ACCTMOD");
         perm.setPermdesc("Modify/Delete Existing Accounts");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("ACCTPWRESET");
         perm.setPermdesc("Reset user passwords");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("ADDSTUDY");
         perm.setPermdesc("Add New Sites/Studies");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("MODSTUDY");
         perm.setPermdesc("Modify Site/Study Attribites (incl Archive & Delete)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;


         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("INSTMGT");
         perm.setPermdesc("Manage Remote Installations");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("INVALIDMGT");
         perm.setPermdesc("Manage Invalid Logins Table");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;


         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("PERMSMGT");
         perm.setPermdesc("Manage com.owarchitects.forms.commons.db.Permissions");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         ////
         ////
         ////
         ////
         
         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("ROLEPERM");
         perm.setPermdesc("ROLES:  Manage Roles/Assign Permissions (System-Level Roles)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("FTYPECREATE");
         perm.setPermdesc("FORMTYPES:  Create New FormTypes (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("FTYPEEDITOTH");
         perm.setPermdesc("FORMTYPES:  Edit Attrs for FormTypes Created by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("FTYPEDELOTH");
         perm.setPermdesc("FORMTYPES:  Delete FormTypes Created by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("FTYPEPERMOTH");
         perm.setPermdesc("FORMTYPES:  Admin Perms for FormTypes Created by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;


         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("DTFORMCAT");
         perm.setPermdesc("FORMS:  Edit Form/DataTable Categories (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("DTFORMUPLOAD");
         perm.setPermdesc("FORMS:  Upload New Form/Create DataTable (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("FTYPEUPLOADNEW");
         perm.setPermdesc("FORMS:  Upload new FormType for forms Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("DTFORMEDITOTH");
         perm.setPermdesc("FORMS:  Edit Attrs for Forms/DataTables Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("DTFORMMOVEOTH");
         perm.setPermdesc("FORMS:  Move Forms/DataTables Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("DTFORMDELOTH");
         perm.setPermdesc("FORMS:  Delete/Replace Forms/DataTables Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("DTFORMPERMOTH");
         perm.setPermdesc("FORMS:  Admin Perms for Forms/DataTables Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("DTFORMPERMPROF");
         perm.setPermdesc("FORMS:  Create/Edit Form/DataTable Permission Profiles (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("RPTCAT");
         perm.setPermdesc("REPORTS:  Edit Report Categories (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("RPTUPLOAD");
         perm.setPermdesc("REPORTS:  Upload New Reports (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("RPTEDITOTH");
         perm.setPermdesc("REPORTS:  Edit Attrs for Rpts Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("RPTMOVEOTH");
         perm.setPermdesc("REPORTS:  Move Rpts Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("RPTDELOTH");
         perm.setPermdesc("REPORTS:  Delete/Replace Rpts Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("RPTPERMOTH");
         perm.setPermdesc("REPORTS:  Admin Perms for Rpts Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("RPTPERMPROF");
         perm.setPermdesc("REPORTS:  Admin Report Permission Profiles (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("LTRCAT");
         perm.setPermdesc("LETTERS:  Edit Form Letter Categories (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("LTRUPLOAD");
         perm.setPermdesc("LETTERS:  Upload New Form Letters (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("LTREDITOTH");
         perm.setPermdesc("LETTERS:  Edit Attrs for Ltrs Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("LTRMOVEOTH");
         perm.setPermdesc("LETTERS:  Move Ltrs Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("LTRDELOTH");
         perm.setPermdesc("LETTERS:  Delete/Replace Ltrs Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("LTRPERMOTH");
         perm.setPermdesc("LETTERS:  Admin Perms for Ltrs Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("LTRPERMPROF");
         perm.setPermdesc("LETTERS:  Admin Letter Permission Profiles (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("MEDCAT");
         perm.setPermdesc("MEDIA:  Edit Document Categories (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("MEDUPLOAD");
         perm.setPermdesc("MEDIA:  Upload New Documents (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("MEDEDITOTH");
         perm.setPermdesc("MEDIA:  Edit Attrs for Docs Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("MEDMOVEOTH");
         perm.setPermdesc("MEDIA:  Move Docs Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("MEDDELOTH");
         perm.setPermdesc("MEDIA:  Delete/Replace Docs Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("MEDPERMOTH");
         perm.setPermdesc("MEDIA:  Admin Perms for Docs Uploaded by Others (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p1);
         perm.setPermacr("MEDPERMPROF");
         perm.setPermdesc("MEDIA:  Administer Media Permissions Profiles (System-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         ////
         ////
         ////
         ////
         
         pos=1;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("ROLEPERM");
         perm.setPermdesc("ROLES:  Manage Roles/Assign Permissions (Site-Level Roles)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("FTYPECREATE");
         perm.setPermdesc("FORMTYPES:  Create New FormTypes (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("FTYPEEDITOTH");
         perm.setPermdesc("FORMTYPES:  Edit Attrs for FormTypes Created by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("FTYPEDELOTH");
         perm.setPermdesc("FORMTYPES:  Delete FormTypes Created by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("FTYPEPERMOTH");
         perm.setPermdesc("FORMTYPES:  Admin Perms for FormTypes Created by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;


         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("DTFORMCAT");
         perm.setPermdesc("FORMS:  Edit Form/DataTable Categories (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("DTFORMUPLOAD");
         perm.setPermdesc("FORMS:  Upload New Form/DataTable (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("FTYPEUPLOADNEW");
         perm.setPermdesc("FORMS:  Upload new FormType for forms Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("DTFORMEDITOTH");
         perm.setPermdesc("FORMS:  Edit Attrs for Forms/DataTables Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("DTFORMMOVEOTH");
         perm.setPermdesc("FORMS:  Move Forms/DataTables Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("DTFORMDELOTH");
         perm.setPermdesc("FORMS:  Delete/Replace Forms/DataTables Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("DTFORMPERMOTH");
         perm.setPermdesc("FORMS:  Admin Perms for Forms/DataTables Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("DTFORMPERMPROF");
         perm.setPermdesc("FORMS:  Create/Edit Form/DataTable Permission Profiles (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("RPTCAT");
         perm.setPermdesc("REPORTS:  Edit Report Categories (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("RPTUPLOAD");
         perm.setPermdesc("REPORTS:  Upload New Reports (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("RPTEDITOTH");
         perm.setPermdesc("REPORTS:  Edit Attrs for Rpts Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("RPTMOVEOTH");
         perm.setPermdesc("REPORTS:  Move Rpts Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("RPTDELOTH");
         perm.setPermdesc("REPORTS:  Delete/Replace Rpts Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("RPTPERMOTH");
         perm.setPermdesc("REPORTS:  Admin Perms for Rpts Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("RPTPERMPROF");
         perm.setPermdesc("REPORTS:  Admin Report Permission Profiles (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("LTRCAT");
         perm.setPermdesc("LETTERS:  Edit Form Letter Categories (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("LTRUPLOAD");
         perm.setPermdesc("LETTERS:  Upload New Form Letters (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("LTREDITOTH");
         perm.setPermdesc("LETTERS:  Edit Attrs for Ltrs Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("LTRMOVEOTH");
         perm.setPermdesc("LETTERS:  Move Ltrs Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("LTRDELOTH");
         perm.setPermdesc("LETTERS:  Delete/Replace Ltrs Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("LTRPERMOTH");
         perm.setPermdesc("LETTERS:  Admin Perms for Ltrs Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("LTRPERMPROF");
         perm.setPermdesc("LETTERS:  Admin Letter Permission Profiles (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("MEDCAT");
         perm.setPermdesc("MEDIA:  Edit Document Categories (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("MEDUPLOAD");
         perm.setPermdesc("MEDIA:  Upload New Documents (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("MEDEDITOTH");
         perm.setPermdesc("MEDIA:  Edit Attrs for Docs Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("MEDMOVEOTH");
         perm.setPermdesc("MEDIA:  Move Docs Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("MEDDELOTH");
         perm.setPermdesc("MEDIA:  Delete/Replace Docs Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("MEDPERMOTH");
         perm.setPermdesc("MEDIA:  Admin Perms for Docs Uploaded by Others (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p2);
         perm.setPermacr("MEDPERMPROF");
         perm.setPermdesc("MEDIA:  Administer Media Permissions Profiles (Site-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         ////
         ////
         ////
         ////
         
         pos=1;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("ROLEPERM");
         perm.setPermdesc("ROLES:  Manage Roles/Assign Permissions (Study-Level Roles)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("FTYPECREATE");
         perm.setPermdesc("FORMTYPES:  Create New FormTypes (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("FTYPEEDITOTH");
         perm.setPermdesc("FORMTYPES:  Edit Attrs for FormTypes Created by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("FTYPEDELOTH");
         perm.setPermdesc("FORMTYPES:  Delete FormTypes Created by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("FTYPEPERMOTH");
         perm.setPermdesc("FORMTYPES:  Admin Perms for FormTypes Created by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;


         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("DTFORMCAT");
         perm.setPermdesc("FORMS:  Edit Form/DataTable Categories (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("DTFORMUPLOAD");
         perm.setPermdesc("FORMS:  Upload New Form/DataTable (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("FTYPEUPLOADNEW");
         perm.setPermdesc("FORMS:  Upload new FormType for forms Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("DTFORMEDITOTH");
         perm.setPermdesc("FORMS:  Edit Attrs for Forms/DataTables Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("DTFORMMOVEOTH");
         perm.setPermdesc("FORMS:  Move Forms/DataTables Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("DTFORMDELOTH");
         perm.setPermdesc("FORMS:  Delete/Replace Forms/DataTables Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("DTFORMPERMOTH");
         perm.setPermdesc("FORMS:  Admin Perms for Forms/DataTables Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("DTFORMPERMPROF");
         perm.setPermdesc("FORMS:  Create/Edit Form/DataTable Permission Profiles (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("RPTCAT");
         perm.setPermdesc("REPORTS:  Edit Report Categories (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("RPTUPLOAD");
         perm.setPermdesc("REPORTS:  Upload New Reports (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("RPTEDITOTH");
         perm.setPermdesc("REPORTS:  Edit Attrs for Rpts Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("RPTMOVEOTH");
         perm.setPermdesc("REPORTS:  Move Rpts Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("RPTDELOTH");
         perm.setPermdesc("REPORTS:  Delete/Replace Rpts Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("RPTPERMOTH");
         perm.setPermdesc("REPORTS:  Admin Perms for Rpts Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("RPTPERMPROF");
         perm.setPermdesc("REPORTS:  Admin Report Permission Profiles (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("LTRCAT");
         perm.setPermdesc("LETTERS:  Edit Form Letter Categories (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("LTRUPLOAD");
         perm.setPermdesc("LETTERS:  Upload New Form Letters (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("LTREDITOTH");
         perm.setPermdesc("LETTERS:  Edit Attrs for Ltrs Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("LTRMOVEOTH");
         perm.setPermdesc("LETTERS:  Move Ltrs Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("LTRDELOTH");
         perm.setPermdesc("LETTERS:  Delete/Replace Ltrs Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("LTRPERMOTH");
         perm.setPermdesc("LETTERS:  Admin Perms for Ltrs Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("LTRPERMPROF");
         perm.setPermdesc("LETTERS:  Admin Letter Permission Profiles (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("MEDCAT");
         perm.setPermdesc("MEDIA:  Edit Document Categories (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("MEDUPLOAD");
         perm.setPermdesc("MEDIA:  Upload New Documents (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("MEDEDITOTH");
         perm.setPermdesc("MEDIA:  Edit Attrs for Docs Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("MEDMOVEOTH");
         perm.setPermdesc("MEDIA:  Move Docs Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("MEDDELOTH");
         perm.setPermdesc("MEDIA:  Delete/Replace Docs Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("MEDPERMOTH");
         perm.setPermdesc("MEDIA:  Admin Perms for Docs Uploaded by Others (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p3);
         perm.setPermacr("MEDPERMPROF");
         perm.setPermdesc("MEDIA:  Administer Media Permissions Profiles (Study-Level)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         ////
         ////
         ////
         ////
         
         pos=1;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p4);
         perm.setPermacr("VIEWTHIS");
         perm.setPermdesc("View This Form/DataTable");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p4);
         perm.setPermacr("NEWRECORD");
         perm.setPermdesc("Submit new record");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p4);
         perm.setPermacr("READOWN");
         perm.setPermdesc("Read access to records input by self");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p4);
         perm.setPermacr("READOTHER");
         perm.setPermdesc("Read access to records input by others");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p4);
         perm.setPermacr("EDITOWN");
         perm.setPermdesc("Edit access to records input by self");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p4);
         perm.setPermacr("EDITOTHER");
         perm.setPermdesc("Edit access to records input by others");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         ////
         ////
         ////
         ////
         
         pos=1;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p5);
         perm.setPermacr("VIEWTYPE");
         perm.setPermdesc("View Formtype Forms");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         ////
         ////
         ////
         ////
         
         pos=1;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p6);
         perm.setPermacr("RUNREPORT");
         perm.setPermdesc("Run Reporting Program");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         ////
         ////
         ////
         ////
         
         pos=1;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p7);
         perm.setPermacr("OPENLETTER");
         perm.setPermdesc("Open Form Letter");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         ////
         ////
         ////
         ////
         
         pos=1;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p8);
         perm.setPermacr("OPENDOC");
         perm.setPermdesc("Open Document/Media File");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         ////
         ////
         ////
         ////
         
         pos=1;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p11);
         perm.setPermacr("ACCESS");
         perm.setPermdesc("R/A CMS:  Access Researcher/Analyst Management Content");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);

         pos++;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p11);
         perm.setPermacr("ROLES");
         perm.setPermdesc("R/A CMS:  Administer roles/permissions for Researcher/Analyst CMS (Admin)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);
          
         pos++; 

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p11);
         perm.setPermacr("DOCUPLOAD");
         perm.setPermdesc("R/A CMS:  Upload Documents to CMS");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);
          
         pos++; 

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p11);
         perm.setPermacr("DOCEDIT");
         perm.setPermdesc("R/A CMS:  Edit file/category attributes on files created/uploaded by others (Admin)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);
          
         pos++; 

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p11);
         perm.setPermacr("DOCREMOVE");
         perm.setPermdesc("R/A CMS:  Remove content uploaded/Delete categories created/uploaded by others (Admin)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);
          
         pos++; 

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p11);
         perm.setPermacr("DOCMOVE");
         perm.setPermdesc("R/A CMS:  Move content uploaded by others (Admin)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);
          
         pos++; 

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p11);
         perm.setPermacr("DOCPERMS");
         perm.setPermdesc("R/A CMS:  Administer file/category permissions content created/uploaded by others (Admin)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);
          
         pos++; 

         ////
         ////
         ////
         ////

         pos=1;

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p12);
         perm.setPermacr("OPEN");
         perm.setPermdesc("R/A CMS RESOURCE:  Open Document or Directory");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);
          
         pos++; 

         perm=new com.owarchitects.forms.commons.db.Permissions();
         perm.setPermissioncats(p12);
         perm.setPermacr("UPLOAD");
         perm.setPermdesc("R/A CMS RESOURCE:  Upload Content (to Directory)");
         perm.setPermorder(pos);
         perm.setPermpos(pos);
         perm.setIsdeleted(false);
         mainDAO.save(perm);
          
         pos++; 

         ////
         ////
         ////
         ////

         console.format("....Done\n\n");

         ////
         ////
         ////
         ////

         // DISALLOWED FILE FORMATS

         console.format("Create disallowed file formats data....\n\n");


         Disallowedfileextensions d;

         d=new Disallowedfileextensions();
         d.setExtension("ade");
         d.setDescription("Microsoft Access project extension");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("adp");
         d.setDescription("Microsoft Access project");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("asx");
         d.setDescription("Windows Media Audio / Video");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("bas");
         d.setDescription("Microsoft Visual Basic class module");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("bat");
         d.setDescription("Batch file");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("chm");
         d.setDescription("Compiled HTML Help file");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("cmd");
         d.setDescription("Microsoft Windows NT Command script");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("com");
         d.setDescription("Microsoft MS-DOS program");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("cpl");
         d.setDescription("Control Panel extension");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("crt");
         d.setDescription("Security certificate");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("exe");
         d.setDescription("Program");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("hlp");
         d.setDescription("Help file");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("hta");
         d.setDescription("HTML program");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("inf");
         d.setDescription("Setup Information");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("ins");
         d.setDescription("Internet Naming Service");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("isp");
         d.setDescription("Internet Communication settings");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("js");
         d.setDescription("JScript file");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("jse");
         d.setDescription("Jscript Encoded Script file");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("lnk");
         d.setDescription("Shortcut");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("mda");
         d.setDescription("Microsoft Access add-in program");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("mdb");
         d.setDescription("Microsoft Access program");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("mde");
         d.setDescription("Microsoft Access MDE database");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("mdt");
         d.setDescription("Microsoft Access workgroup information");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("mdw");
         d.setDescription("Microsoft Access workgroup information");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("mdz");
         d.setDescription("Microsoft Access wizard program");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("msc");
         d.setDescription("Microsoft Common Console document");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("msi");
         d.setDescription("Microsoft Windows Installer package");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("msp");
         d.setDescription("Microsoft Windows Installer patch");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("mst");
         d.setDescription("Microsoft Windows Installer transform/Microsoft Visual Test source file");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("ops");
         d.setDescription("Office XP settings");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("pcd");
         d.setDescription("Photo CD image; Microsoft Visual compiled script");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("pif");
         d.setDescription("Shortcut to MS-DOS program");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("prf");
         d.setDescription("Microsoft Outlook profile settings");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("reg");
         d.setDescription("Registration entries");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("scf");
         d.setDescription("Windows Explorer command");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("scr");
         d.setDescription("Screen saver");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("sct");
         d.setDescription("Windows Script Component");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("shb");
         d.setDescription("Shell Scrap object");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("shs");
         d.setDescription("Shell Scrap object");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("url");
         d.setDescription("Internet shortcut");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("vb");
         d.setDescription("VBScript file");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("vbe");
         d.setDescription("VBScript Encoded script file");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("vbs");
         d.setDescription("VBScript file");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("wsc");
         d.setDescription("Windows Script Component");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("wsf");
         d.setDescription("Windows Script file");
         mainDAO.saveOrUpdate(d);
         
         d=new Disallowedfileextensions();
         d.setExtension("wsh");
         d.setDescription("Windows Script Host Settings file");
         mainDAO.saveOrUpdate(d);

         console.format("....Done\n\n");

         ////
         ////
         ////
         ////

         // SUPPORTED FILE FORMATS

         console.format("Create supported file formats data....\n\n");

         Supportedfileformats f;
         Fileextensions e;
         
         f=new Supportedfileformats();
         f.setContenttype("application/marc");
         f.setFileformat("MARC marc");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("mrc");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/mathematica");
         f.setFileformat("Mathematica");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("ma");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/msword");
         f.setFileformat("Microsoft Word Document");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("doc");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("docx");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/pdf");
         f.setFileformat("Adobe PDF File");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("pdf");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/postscript");
         f.setFileformat("Postscript File");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("ps");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("eps");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("ai");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/sgml");
         f.setFileformat("SGML");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("sgm");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("sgml");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/vnd.ms-excel");
         f.setFileformat("Microsoft Excel Spreadsheet");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("xls");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("xlsx");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/vnd.ms-powerpoint");
         f.setFileformat("Microsoft Powerpoint");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("ppt");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("pptx");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/vnd.ms-project");
         f.setFileformat("Microsoft Project");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("mpp");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("mpx");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("mpd");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/vnd.visio");
         f.setFileformat("Microsoft Visio");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("vsd");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/wordperfect5.1");
         f.setFileformat("WordPerfect Document");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("wpd");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/x-dvi");
         f.setFileformat("TeXdvi");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("dvi");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/x-filemaker");
         f.setFileformat("FMP3");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("fm");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/x-latex");
         f.setFileformat("LateX");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("latex");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/x-photoshop");
         f.setFileformat("Photoshop");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("psd");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("pdd");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/x-tex");
         f.setFileformat("TeX");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("tex");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("audio/x-aiff");
         f.setFileformat("AIFF");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("aiff");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("aif");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("aifc");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("audio/basic");
         f.setFileformat("audio/basic");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("au");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("snd");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/zip");
         f.setFileformat("Media Archive File");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("zip");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("jar");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("gz");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("tar");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         
         f=new Supportedfileformats();
         f.setContenttype("audio/mp3");
         f.setFileformat("MP3 Audio");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("mp3");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         
         f=new Supportedfileformats();
         f.setContenttype("audio/mp4");
         f.setFileformat("MP4 Audio");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("mp4");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("m4a");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         
         f=new Supportedfileformats();
         f.setContenttype("audio/x-ms-wmv");
         f.setFileformat("Windows Media Video");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("wmv");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         
         f=new Supportedfileformats();
         f.setContenttype("audio/x-mpeg");
         f.setFileformat("MPEG Audio");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("mpa");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("abs");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("mpeg");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("mp1");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("mp2");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("audio/x-pn-realaudio");
         f.setFileformat("RealAudio");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("ra");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("ram");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("audio/x-wav");
         f.setFileformat("WAV");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("wav");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("image/gif");
         f.setFileformat("GIF Image File");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("gif");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("image/jp2");
         f.setFileformat("JPEG 2000 Image File");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("jp2");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("image/jpeg");
         f.setFileformat("JPEG Image File");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("jpeg");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("jpg");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("image/png");
         f.setFileformat("PNG Image File");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("png");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("image/tiff");
         f.setFileformat("TIFF Image File");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("tiff");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("tif");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("image/x-ms-bmp");
         f.setFileformat("BMP Image File");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("bmp");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("image/x-photo-cd");
         f.setFileformat("Photo CD");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("pcd");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("text/html");
         f.setFileformat("HTML Document");
         mainDAO.saveOrUpdate(f);

         // HTML FORMAT NEEDED FOR LATER
         Supportedfileformats ff_html=f;
         
         e=new Fileextensions();
         e.setExtension("html");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("htm");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("text/plain");
         f.setFileformat("Text File");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("txt");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/rtf");
         f.setFileformat("Rich Text Format");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("rtf");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("text/xml");
         f.setFileformat("XML Document");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("xml");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("video/mpeg");
         f.setFileformat("MPEG Video File");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("mpeg");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("mpg");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("mpe");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("video/quicktime");
         f.setFileformat("Video Quicktime");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("mov");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("qt");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("text/x-sas-syntax");
         f.setFileformat("SAS Syntax File");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("sas");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/x-sas-system");
         f.setFileformat("SAS System File");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("sas7bdat");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("sd1");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("sd2");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("sd7");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("ssd01");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("ssd");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("ssd04");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/x-sas-transport");
         f.setFileformat("SAS Transport File");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("xpt");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("cport");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("v5x");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("v6x");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("v7x");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("text/x-spss-syntax");
         f.setFileformat("SPSS Syntax File");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("sps");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/x-spss-sav");
         f.setFileformat("SPSS system file");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("sav");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/x-spss-sav");
         f.setFileformat("SPSS portable file");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("por");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("text/x-stata-syntax");
         f.setFileformat("Stata Syntax file");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("do");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/x-stata");
         f.setFileformat("Stata Binary files");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("dta");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("text/x-r-syntax");
         f.setFileformat("R syntax file");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("r");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("application/x-rlang-transport");
         f.setFileformat("R binary file");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("rdata");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("text/x-fixed-field");
         f.setFileformat("fixed field text data");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("dat");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         e=new Fileextensions();
         e.setExtension("asc");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("text/csv");
         f.setFileformat("CSV File - (Comma separated values)");
         mainDAO.saveOrUpdate(f);

         // CSV FORMAT NEEDED FOR LATER
         Supportedfileformats ff_csv=f;
         
         e=new Fileextensions();
         e.setExtension("csv");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);
         
         f=new Supportedfileformats();
         f.setContenttype("text/tab-separated-values");
         f.setFileformat("TAB File - (Tab separated values)");
         mainDAO.saveOrUpdate(f);
         
         e=new Fileextensions();
         e.setExtension("tab");
         e.setSupportedfileformats(f);
         mainDAO.saveOrUpdate(e);

         //
         //

         Formformats htmlformat=new Formformats();
         htmlformat.setSupportedfileformats(ff_html);
         mainDAO.save(htmlformat);

         //
         //

         Datatableformats csvformat=new Datatableformats();
         csvformat.setSupportedfileformats(ff_csv);
         mainDAO.save(csvformat);

         ////
         ////
         ////
         ////

         console.format("....Done\n\n");
         
      } catch (Exception e) {
         System.out.println(Stack2string.getString(e));
         System.out.println("\n\nERROR:  Couldn't complete database initialization");
         System.exit(1);
      }

   }

   public void setApploc(String apploc) {
      this.apploc=apploc;
   }

   public void setCtxfile(String ctxfile) {
      this.ctxfile=ctxfile;
   }

   public void setKsfile(String ksfile) {
      this.ksfile=ksfile;
   }

   //////////
   //////////
   //////////
   //////////
 
   // Certificate chain required for storing private keys.  Not important for our
   // purposes, so we create simple self-signed chain for this purpose
   private static java.security.cert.Certificate[] getCertChain() throws FORMSException {
     
      try {
 
         Security.addProvider(new BouncyCastleProvider());
 
         KeyPairGenerator kpg=KeyPairGenerator.getInstance("RSA");
         kpg.initialize(512);
         KeyPair kp=kpg.generateKeyPair();
                 
         X509V1CertificateGenerator gc = new X509V1CertificateGenerator();
         X500Principal x500=new X500Principal("CN=FORMS Certificate");
         
         gc.setSerialNumber(BigInteger.valueOf(1));
         gc.setIssuerDN(x500);
         gc.setNotBefore(new Date(new Long(0).longValue()));
         gc.setNotAfter(new Date(new Long(1000*60*60*24*365*100).longValue()));
         gc.setSubjectDN(x500); 
         gc.setPublicKey(kp.getPublic());
         gc.setSignatureAlgorithm("MD5WithRSAEncryption");
         
         java.security.cert.X509Certificate cert = gc.generate(kp.getPrivate(), "BC");
 
         java.security.cert.Certificate[] chain = new java.security.cert.X509Certificate[1];
         chain[0] = cert;
 
         //
         // NOTE:  The following SUN-generated certificate creation code has been replaced
         //        by the above BouncyCastle creation code, to remove complation warnings
         //        about the possible removal of associated proprietary code from JRE.
         //
         /*
         // Generate certificate chain (required for storing PrivateKeys)
         CertAndKeyGen cakg = new CertAndKeyGen("RSA", "MD5WithRSA");
         cakg.generate(512);
         PublicKey publicKey = cakg.getPublicKey();
         PrivateKey privateKey = cakg.getPrivateKey();
         X500Name name = new X500Name("FORMS", "FORMS", "FORMS", "FORMS", "FORMS", "FORMS");
         java.security.cert.Certificate[] chain = new java.security.cert.X509Certificate[1];
         chain[0] = cakg.getSelfCertificate(name, 999999999);
         */
 
         return chain;
 
      } catch (Exception e) {
         throw new FORMSException(e,"Error creating certificate chain");
      }
 
   }

}



