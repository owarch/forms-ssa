 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * AnalystcontentController.java - Form Selection Interface
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.mla.html.table.*;

public class AnalystcontentController extends FORMSSsaServiceClientController {

   public ModelAndView submitRequest() throws FORMSException {

      try {

         FORMSAuth auth=this.getAuth();

         ModelAndView mav=new ModelAndView();
         HttpServletResponse response=this.getResponse();
         PrintWriter out=this.getWriter();

         // Initialize Options (If necessary)
         //auth.initOption("typeopt","ALL");
         //auth.initOption("veropt","REC");
         //auth.initOption("archopt","Y");
         //auth.initOption("fnumopt","");
         auth.initOption("sinstopt","");
         auth.initOption("resourcetype","");
         auth.initOption("resourcetypestr","ANALYSTFILE");

         // clear stored values
         auth.setValue("analystinfoid","");

         AnalystcontentResult result=processData();
         //result.setTypeopt(auth.getOption("typeopt"));
         //result.setVeropt(auth.getOption("veropt"));
         //result.setFnumopt(auth.getOption("fnumopt"));
         result.setSinstopt(auth.getOption("sinstopt"));
         result.setRightslevel(auth.getOption("rightslevel"));
         getSession().setAttribute("iframeresult",result);
         mav.addObject("status","OK");
         return mav;
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Create root hierarchy record if none exists (these are not created at database initialization)
   private void createHaRecord() throws FORMSException, FORMSSecurityException, FORMSKeyException {
      Analystha newha=new Analystha();
      newha.setHaordr(1);
      newha.setHadesc("root");
      getMainDAO().saveOrUpdate(newha);
      return;

   }

   //private AnalystcontentResult processData(boolean showarchivedforms) throws FORMSException {
   private AnalystcontentResult processData() throws FORMSException {

      //AnalystcontentResult result=new AnalystcontentResult();

      try {

        FORMSAuth auth=this.getAuth();

        // Use session-stored lists if available to avoid frequent hits to database for pulling form list
        List mhatemp=(List)auth.getSessionAttribute("analysthalist");
        List minfotemp=(List)auth.getSessionAttribute("analystinfolist");
        HashMap iinfotemp=(HashMap)auth.getSessionAttribute("minfomap");
        Iterator i;

        // If no/empty list available, pull from database
        if (mhatemp==null || minfotemp==null || mhatemp.size()<1 || minfotemp.size()<1) {

           // Make sure root record exists
           Analystha rootha=(Analystha)getMainDAO().execUniqueQuery(
              "select m from Analystha m where m.panalysthaid=null"
              );

           if (rootha==null) {   
              createHaRecord();
           }
         
           // Retrive installation information for later
           List ilist=getMainDAO().execQuery(
              "select a from Allinst a join fetch a.linkedinst where a.islocal!=true"
              );
           i=ilist.iterator();
           HashMap minfomap=new HashMap();
           while (i.hasNext()) {
             Allinst ainst=(Allinst)i.next();
             minfomap.put(new Long(ainst.getAinstid()).toString(),ainst.getLinkedinst().getAcronym());
           }

           mhatemp=getMainDAO().execQuery(
              "select mha from Analystha mha " +
                    "order by haordr"
              );
       
           i=mhatemp.iterator();
           // 1) Write analysthaid to StringBuilder for use in formtypes pull
           // 2) Verify root record exist.  Otherwise create it
           StringBuilder sb=new StringBuilder();
           while (i.hasNext()) {
              Analystha ha=(Analystha)i.next();
              // write id to stringbuffer
              sb.append(new Long(ha.getAnalysthaid()).toString());
              if (i.hasNext()) sb.append(",");
              // check that necessary root hierarchy records exist, else create them
           }
     
           // Pull analystinfo data (Local files)
           if (sb.length()>0) {
              minfotemp=getMainDAO().execQuery(
                 "select minf from Analystinfo minf join fetch minf.supportedfileformats " +
                    "where minf.panalysthaid in (" 
                       + sb.toString() + ")" +
                    "order by minf.description"
              );
           } else {
              minfotemp=(List)new ArrayList();
           }

           auth.setSessionAttribute("analysthalist",mhatemp);
           auth.setSessionAttribute("analystinfolist",minfotemp);
           auth.setSessionAttribute("minfomap",minfomap);

        };

        // Create new list based on stored list
        List mha=(List)new ArrayList(mhatemp);

        // add forms and installation info to final form list
        Iterator minfoi=minfotemp.iterator();
        List allanalystlist=(List)new ArrayList();
        auth.setValue("showsinstopt","N");
        // Add forms to list
        while (minfoi.hasNext()) {
           Analystinfo minfo=(Analystinfo)minfoi.next();
           HashMap minfoimap=new HashMap();
           minfoimap.put("linstid",null);
           minfoimap.put("lstdid",null);
           minfoimap.put("instacr","LOCAL");
           minfoimap.put("analyst",minfo);
           // only add to list if user is permitted to open file
           if (getPermHelper().hasAnalystPerm(new Long(minfo.getAnalystinfoid()).toString(),"OPEN")) {
              allanalystlist.add(minfoimap);
           }   
        }

        /////////////////////////////////////////////////////////
        // Remove analyst files from list based on permissions //
        /////////////////////////////////////////////////////////

/*
        List fpermlist=getPermHelper().getAnalystPermList();

        // Refine list based on options
        HashMap checkver=new HashMap();
        String typeopt=auth.getOption("typeopt");
        i=allanalystlist.iterator();
        while (i.hasNext()) {
           HashMap imap=(HashMap)i.next();
           Analystinfo minfo=(Analystinfo)imap.get("analyst");
           String instacr=(String)imap.get("instacr");
           boolean isremoved=false;
           // Check permissions - (based on analyst permission list)
           // note:  Upload user has automatic rights to file
           if (!(isremoved || minfo.getUploaduser()==new Long(auth.getValue("auserid")).longValue() ||
              auth.getValue("rightslevel").equalsIgnoreCase("ADMIN"))) {
              Long analystinfoid=new Long(minfo.getAnalystinfoid());
              Iterator piter=fpermlist.iterator();
              boolean found=false;
              while (piter.hasNext()) {
                 HashMap map=(HashMap)piter.next();
                 Long compid;
                 try {
                    compid=new Long((String)map.get("analystinfoid"));
                 } catch (ClassCastException cce) {   
                    compid=(Long)map.get("analystinfoid");
                 }   
                 if (compid.equals(analystinfoid)) {
                    found=true;
                    break;
                 }
              }
              if (!found) {
                 i.remove();
                 isremoved=true;
              }
           }
        }
*/

        ////////////////////////////////////////////////////////////////////
        // Organize data into heirarchical format for building HTML table //
        ////////////////////////////////////////////////////////////////////

        // minfo holds number of rows in table.  Keep this info for later
        int tablerows=allanalystlist.size();

        // tlist holds table columns
        ArrayList tlist=new ArrayList();
        // alist holds items within columns
        ArrayList alist=new ArrayList();

        // pull first table column
        i=mha.iterator();
        while (i.hasNext()) {
           Analystha ha=(Analystha)i.next();
           if (ha.getPanalysthaid()==null) {
              HashMap map=new HashMap();
              map.put("isnull","N");
              map.put("obj",ha);
              map.put("nrows",0);
              map.put("ncols",1);
              // remove element from collection so we don't have to compare it again
              i.remove();
              alist.add(map);
           }
        }   
        tlist.add(alist);
        // pull subsequent columns by finding children of elements in previous columns
        for (int ii=1;ii>-1;ii++) {
           // pull elements in prior column
           ArrayList plist=(ArrayList)tlist.get(ii-1);
           alist=new ArrayList();
           Iterator i2=plist.iterator();
           boolean again=false;
           //iterate through prior column elements
           while (i2.hasNext()) {
              // find children
              HashMap pmap=(HashMap)i2.next();
              Analystha pha=(Analystha)pmap.get("obj");
              i=mha.iterator();
              boolean havechild=false;
              int prevsize=alist.size();
              while (i.hasNext()) {
                 Analystha cha=(Analystha)i.next();
                 // write child out to alist
                 if (cha.getPanalysthaid().equals(pha.getAnalysthaid())) {
                    HashMap cmap=new HashMap();
                    cmap.put("isnull","N");
                    cmap.put("obj",cha);
                    cmap.put("nrows",0);
                    cmap.put("ncols",1);
                    // remove element from collection so we don't have to compare it again
                    i.remove();
                    alist.add(cmap);
                    again=true;
                    havechild=true;
                 }
                 
              }
              // create null row to hold records belonging to item when child
              // categories are present
              if (havechild) {
                 HashMap nmap=new HashMap();
                 Analystha nha=new Analystha();
                 nha.setAnalysthaid(pha.getAnalysthaid());
                 nha.setPanalysthaid(pha.getPanalysthaid());
                 nha.setHadesc("&nbsp;");
                 nmap.put("isnull","Y");
                 nmap.put("obj",nha);
                 nmap.put("nrows",0);
                 nmap.put("ncols",1);
                 alist.add(prevsize,nmap);
              }
           }
           if (!again) break;
           tlist.add(alist);
        }
        // add forms to appropriate category (must iterate backwards through tlist
        // since forms are attached as the right most element)
        for (int ii=(tlist.size()-1);ii>=0;ii--) {
           // pull column arraylist
           ArrayList plist=(ArrayList)tlist.get(ii);
           Iterator i2=plist.iterator();
           while (i2.hasNext()) {
              HashMap pmap=(HashMap) i2.next();
              Analystha pha=(Analystha)pmap.get("obj");
              // analystlist holds forms belonging to an element
              ArrayList analystlist=new ArrayList();
              Iterator i3=allanalystlist.iterator();
              while (i3.hasNext()) {
                 HashMap i3map=(HashMap)i3.next();
                 Analystinfo cinfo=(Analystinfo)i3map.get("analyst");
                 Long linstid=(Long)i3map.get("linstid");
                 Long lstdid=(Long)i3map.get("lstdid");
                 String instacr=(String)i3map.get("instacr");
                 // add form to analystlist where there is a match
                 if (cinfo.getPanalysthaid()==pha.getAnalysthaid()) {
                    HashMap flmap=new HashMap();
                    flmap.put("analyst",cinfo);
                    flmap.put("linstid",linstid);
                    flmap.put("lstdid",lstdid);
                    flmap.put("instacr",instacr);
                    analystlist.add(flmap);
                    // remove element from collection so we don't have to compare it again
                    i3.remove();
                 }
              }
              // add analystlist to element's hashmap
              if (analystlist.size()>0) {
                 pmap.put("analystlist",analystlist);
                 pmap.put("nrows",analystlist.size());
                 pmap.put("ncols",tlist.size()-ii);
              }
              // add nrows to parent cells from prior columns
              Long compid=null;
              try {
                 compid=new Long(pha.getPanalysthaid());
              } catch (Exception cie) { }
              // loop through prior columns looking for parent elements
              for (int jj=(ii-1);jj>=0;jj--) {
                 ArrayList pplist=(ArrayList)tlist.get(jj);
                 Iterator i4=pplist.iterator();
                 while (i4.hasNext()) {
                    HashMap ppmap=(HashMap) i4.next();
                    Analystha ppha=(Analystha)ppmap.get("obj");
                    String isnull=(String)ppmap.get("isnull");
                    // if find a parent element, add rows to that element.  Then update
                    // compid to that element's parent for next loop
                    if ((
                           (!(isnull.equals("Y") || compid==null)) && ppha.getAnalysthaid()==compid.longValue()
                        ) ||
                        (
                           ppha.getAnalysthaid()==pha.getAnalysthaid() && ppha.getPanalysthaid()==pha.getPanalysthaid()
                       )) {
                       int nrows=((Integer)ppmap.get("nrows")).intValue();
                       nrows=nrows+analystlist.size();
                       ppmap.put("nrows",nrows);
                       try {
                          compid=new Long(ppha.getPanalysthaid());
                       } catch (Exception cie2) {}
                    }
                 }
              }
           }
        }
        
        /////////////////////////////////
        // Create HTML Table of result //
        /////////////////////////////////

        int tablecols=tlist.size()+1;
        if (tablerows<1 || tablecols<1) {
           throw new FORMSException("Zero length table");
        }
        Table table=new Table(tablerows,tablecols);
        // iterate through table columns;
        for (int currcol=0;currcol<tlist.size();currcol++) {
           ArrayList rowlist=(ArrayList)tlist.get(currcol);
           int prevrow=0;
           Iterator rowiter=rowlist.iterator();
           while (rowiter.hasNext()) {
              HashMap map2=(HashMap)rowiter.next();
              Analystha ha2=(Analystha)map2.get("obj");
              int nrows=((Integer)map2.get("nrows")).intValue();
              int ncols=((Integer)map2.get("ncols")).intValue();
              if (nrows<=0 || ncols<=0) continue;
              Cell cell=new Cell(new Long(ha2.getAnalysthaid()).toString(),nrows,ncols);
              cell.setContent("type","analystha");
              cell.setContent("obj",ha2);
              boolean okvar=false;
              int currrow=prevrow;
              // Use previous row & exceptions to calculate cell row placement.  This could probably
              // be made more efficient by running through a calculation loop, but the loss here 
              // is quite minimal
              int firstrow=0;
              while (!okvar) {
                 try {
                    table.setCell(cell,currrow,currcol);
                    firstrow=currrow;
                    prevrow=currrow+1;
                    okvar=true;
                 } catch (Exception te) { 
                    currrow++;
                    if (currrow>500000) throw new FORMSException("Too many rows returned");
                 }
              }   
              // see if contains any forms (final column)
              ArrayList analystlist=(ArrayList)map2.get("analystlist");
              if (analystlist!=null) {
                 // Rows for items must be placed based on row location of containing element
                 // (first row number of containing element is held in "firstrow")
                 int prevrow2=firstrow;
                 Iterator formiter=analystlist.iterator();
                 while (formiter.hasNext()) {
                    HashMap minfomap=(HashMap)formiter.next();
                    Analystinfo minfo=(Analystinfo)minfomap.get("analyst");
                    Long linstid=(Long)minfomap.get("linstid");
                    Long lstdid=(Long)minfomap.get("lstdid");
                    String instacr=(String)minfomap.get("instacr");
                    cell=new Cell(new Long(minfo.getAnalystinfoid()).toString(),1,1);
                    cell.setContent("type","minfo");
                    cell.setContent("obj",minfo);
                    cell.setContent("instacr",instacr);
                    if (linstid==null) {
                       cell.setContent("linstid",-99999999);
                       cell.setContent("lstdid",-99999999);
                       cell.setContent("islocal",true);

                    } else {
                       cell.setContent("linstid",linstid);
                       cell.setContent("lstdid",lstdid);
                       cell.setContent("islocal",false);
                    }
                    okvar=false;
                    currrow=prevrow2;
                    while (!okvar) {
                       try {
                          table.setCell(cell,currrow,tablecols-1);
                          prevrow2=currrow+1;
                          okvar=true;
                       } catch (Exception te) {
                          currrow++;
                          if (currrow>500000) throw new FORMSException("Too many rows returned");
                       }
                    }
                 }
              }
           }
        }
        // Create Options Menu Map
        HashMap optionsmap=new HashMap();
        //optionsmap.put("typelist",typelist);
        //optionsmap.put("typeopt",auth.getOption("typeopt"));
        //optionsmap.put("veropt",auth.getOption("veropt"));
        //optionsmap.put("archopt",auth.getOption("archopt"));
        //optionsmap.put("fnumopt",auth.getOption("fnumopt"));
        optionsmap.put("sinstopt",auth.getOption("sinstopt"));
        auth.setSessionAttribute("optionsobject",optionsmap);

        // Create & return AnalystcontentResult Object
        AnalystcontentResult result=new AnalystcontentResult();

        result.setTable(table);

        return result;

      } catch (Exception e) {

        AnalystcontentResult result=new AnalystcontentResult();
        return result;

      }

   }

}

