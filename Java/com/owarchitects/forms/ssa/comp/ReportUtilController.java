 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * ReportUtilController.java - Form utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.mla.html.table.*;

public class ReportUtilController extends FORMSSsaServiceClientController {
 
   String spath="";

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // check permissions
      //if (!getAuth().getValue("rightslevel").equalsIgnoreCase("ADMIN")) {
      //   safeRedirect("permission.err");
      //   return null;
      //}

      // Use class-name based view
      mav=new ModelAndView("ReportUtil");

      try {

         HttpServletRequest request=this.getRequest();
         spath=request.getServletPath();

         // addstudy.sutil  (add new study interface screen)
         if (spath.indexOf("removecat.")>=0) {

            mav.addObject("status",removeCat());
            return mav;

         }
         else if (spath.indexOf("movecat.")>=0) {
          
            return moveCat(mav);

         }
         else if (spath.indexOf("movecatsubmit.")>=0) {
          
            mav.addObject("status",moveCatSubmit());
            return mav;

         }
         else if (spath.indexOf("viewallcat.")>=0) {
          
            return viewAllCat(mav);

         }
         else if (spath.indexOf("viewallsubmit.")>=0) {
          
            safeRedirect("reporteditcat.am");
            return null;

         }
         else if (spath.indexOf("editcat.")>=0) {

            Reportha ha=editCat();
            if (ha!=null) {
               // check permissions 
               if (!getPermHelper().hasGlobalPerm(ha.getVisibility(),getAuth().getValue("resourceperm") + "CAT")) {
                  mav.addObject("status","NOAUTH");
                  return mav;
               }
               // continue
               mav.addObject("status","EDITCAT");
               mav.addObject("reportha",editCat());
               return mav;
            }
            return null;

         }
         else if (spath.indexOf("editcatsubmit.")>=0) {

            mav.addObject("status",editCatSubmit());
            return mav;

         // Delete specified profile 
         } else if (spath.indexOf("deletefile.")>=0) {

            mav.addObject("status",deleteFile());
            return mav;

         }
         else if (spath.indexOf("showinfo.")>=0) {

            return showInfo();

         }
         else if (spath.indexOf("editattr.")>=0) {

            return editAttr();

         }
         else if (spath.indexOf("editattrsubmit.")>=0) {

            mav.addObject("status",editAttrSubmit());
            return mav;

         }
         else if (spath.indexOf("addcat.")>=0) {

            Reportha ha=addCat();
            if (ha!=null) {
               // Check permissions
               if (!getPermHelper().hasGlobalPerm(ha.getVisibility(),getAuth().getValue("resourceperm") + "CAT")) {
                  mav.addObject("status","NOAUTH");
                  return mav;
               }
               // continue
               mav.addObject("status","ADDCAT");
               mav.addObject("reportha",addCat());
               return mav;
            }
            return null;

         }
         else if (spath.indexOf("addcatsubmit.")>=0) {

            mav.addObject("status",addCatSubmit());
            return mav;

         // Re-order categories interface
         } else if (spath.indexOf("reordercat.")>=0) {

            List l=reorderCat();
            // check permissions 
            Reportha ha=(Reportha)l.get(0);
            if (!getPermHelper().hasGlobalPerm(ha.getVisibility(),getAuth().getValue("resourceperm") + "DELOTH")) {
               mav.addObject("status","NOAUTH");
               return mav;
            }
            mav.addObject("list",l);
            mav.addObject("status","REORDERCAT");
            return mav;

         // Submit re-ordering of categories
         } else if (spath.indexOf("reordercatsubmit.")>=0) {

            mav.addObject("status",reorderCatSubmit());
            return mav;

         } else if (spath.indexOf("listprofiles.")>=0) {

            return listProfiles(mav);

         // Add new profile interface (PROFILE LEVEL SELECTION)
         } else if (spath.indexOf("newprofile.")>=0) {

            mav.addObject("status","NEWPROFILE");
            mav.addObject("list",new VisibilityConstants().getFieldList());
            return mav;

         // Add new profile interface (ENTER PROFILE ATTRIBUTES)
         } else if (spath.indexOf("addprofile.")>=0) {

            int profilelevel=new Integer(getRequest().getParameter("profilelevel")).intValue();

            // See if user is permitted to modify profile
            if (!getPermHelper().hasGlobalPerm(profilelevel,getAuth().getValue("resourceperm") + "PERMPROF")) {
               // not permitted
               mav.addObject("status","NOAUTH");
               return mav;
            }

            mav.addObject("status","ADDPROFILE");
            mav.addObject("profilelevel",new VisibilityConstants().getFieldName(profilelevel));
            return mav;

         // Submit new profile to system
         } else if (spath.indexOf("addprofilesubmit.")>=0) {

            String status=addProfileSubmit();
            mav.addObject("status",status);
            return mav;

         // Modify profile interface
         } 
         else if (spath.indexOf("profilemenu.")>=0) {

            mav.addObject("status","PROFILEMENU");
            mav.addObject("rprofileid",getRequest().getParameter("rprofileid"));
            return mav;

         } 
         else if (spath.indexOf("modifyprofile.")>=0) {

            mav.addObject("status",modifyProfile());
            return mav;

         // Submit modified profile
         } else if (spath.indexOf("modifyprofilesubmit.")>=0) {

            mav.addObject("status",modifyProfileSubmit());
            return mav;

         // Delete specified profile 
         } else if (spath.indexOf("removeprofilesubmit.")>=0) {

            mav.addObject("status",removeProfileSubmit());
            return mav;

         }
         // add new site interface
         else {
            out.println("BAD REQUEST! - " + spath);
            return null;
         }
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Empty session-stored report lists so data are repulled
   private void clearSessionLists() {   
      try {
         getAuth().setSessionAttribute("reporthalist",new ArrayList());
         getAuth().setSessionAttribute("reportinfolist",new ArrayList());
      } catch (Exception e) { }
   }   

   private Reportha editCat() {
      String reporthaid=getRequest().getParameter("reporthaid");
      Reportha ha=(Reportha)getMainDAO().execUniqueQuery(
         "select ha from Reportha ha where reporthaid=" + reporthaid
         );
      return ha;   
   }

   private String editCatSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      // Check permission

      FORMSAuth auth=getAuth();
      String reporthaid=getRequest().getParameter("reporthaid");
      String hadesc=getRequest().getParameter("hadesc");
      Reportha ha=(Reportha)getMainDAO().execUniqueQuery(
         "select ha from Reportha ha where reporthaid=" + reporthaid
         );

      if (!getPermHelper().hasGlobalPerm(ha.getVisibility(),auth.getValue("resourceperm") + "CAT")) {
         return "NOAUTH";
      }

      // Make sure description doesn't already exist in category   
      List l=getMainDAO().execQuery(
         "select ha from Reportha ha where ha.preporthaid=" + reporthaid + " and upper(ha.hadesc)=upper('" + hadesc + "')" 
         );
      if (l.size()>0) {
         return "CATNOTUNIQUE";
      }
      ha.setHadesc(hadesc);   
      getMainDAO().saveOrUpdate(ha);
      clearSessionLists();
      return "CATEDITED";
   }

   private Reportha addCat() {
      String reporthaid=getRequest().getParameter("reporthaid");
      Reportha ha=(Reportha)getMainDAO().execUniqueQuery(
         "select ha from Reportha ha where reporthaid=" + reporthaid
         );
      return ha;   
   }

   private String addCatSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=getAuth();

      String reporthaid=getRequest().getParameter("reporthaid");
      String hadesc=getRequest().getParameter("hadesc");

      Reportha cha=(Reportha)getMainDAO().execUniqueQuery(
         "select ha from Reportha ha where reporthaid=" + reporthaid
         );
      if (!getPermHelper().hasGlobalPerm(cha.getVisibility(),auth.getValue("resourceperm") + "CAT")) {
         return "NOAUTH";
      }

      Studies currstudy=(Studies)getMainDAO().execUniqueQuery(
         "select std from Studies std where std.studyid=" + auth.getValue("studyid")
         );
      // Make sure description doesn't already exist in category   
      List l=getMainDAO().execQuery(
         "select ha from Reportha ha where ha.preporthaid=" + reporthaid + " and upper(ha.hadesc)=upper('" + hadesc + "')" 
         );
      if (l.size()>0) {
         return "CATNOTUNIQUE";
      }
      Integer haordr=(Integer)getMainDAO().execUniqueQuery(
         "select max(ha.haordr) from Reportha ha where ha.preporthaid=" + reporthaid
         );
      Integer visibility=(Integer)getMainDAO().execUniqueQuery(
         "select ha.visibility from Reportha ha where ha.reporthaid=" + reporthaid
         );
      if (haordr==null) haordr=1;
      else haordr++;
      Reportha ha=new Reportha();
      ha.setPreporthaid(new Long(reporthaid));
      ha.setHadesc(hadesc);   
      ha.setHaordr(haordr);   
      ha.setResourcetype(new Integer(auth.getValue("resourcetype")).intValue());   
      ha.setVisibility(visibility);   
      ha.setStudies(currstudy);   
      getMainDAO().saveOrUpdate(ha);
      clearSessionLists();
      return "CATADDED";
   }

   private ModelAndView moveCat(ModelAndView mav) {
      return moveCat(mav,false);
   }

   private ModelAndView moveCat(ModelAndView mav,boolean iscatrequest) {

      try {

        FORMSAuth auth=getAuth();
        Reportha ftpfha=null;
        Object moveobj=null;

        if (!iscatrequest) {
           if (auth.getValue("islocal").toUpperCase().equals("TRUE")) {
   
              // pull current reportinfo record
              Reportinfo ft=(Reportinfo)getMainDAO().execUniqueQuery(
                 "select ft from Reportinfo ft " +
                    "where ft.reportinfoid=" + request.getParameter("reportinfoid")
                 );   
              moveobj=ft;   
        
              if (ft==null) throw new FORMSException("Couldn't pull report program record");   
       
              // pull record's reportha parent
              ftpfha=(Reportha)getMainDAO().execUniqueQuery(
                 "select ha from Reportha ha where ha.reporthaid=" + new Long(ft.getPreporthaid()).toString()
                 );
   
              // check permissions for move
              if (!ft.getUploaduser().equals(new Long(auth.getValue("auserid")))) {
                 if (!getPermHelper().hasGlobalPerm(ftpfha.getVisibility(),auth.getValue("resourceperm") + "MOVEOTH")) {
                    mav.addObject("status","NOAUTH");
                    return mav;
                 }
              }
   
           } else {
   
              // pull current reportinfo record
              Remotereportcat ft=(Remotereportcat)getMainDAO().execUniqueQuery(
                 "select ft from Remotereportcat ft " +
                    "where ft.lstdid=" + auth.getValue("lstdid") + " and ft.rreportinfoid=" + 
                       request.getParameter("reportinfoid")
                 );   
              moveobj=ft;   
        
              if (ft==null) throw new FORMSException("Couldn't pull report program record");   
       
              // pull record's reportha parent
              ftpfha=(Reportha)getMainDAO().execUniqueQuery(
                 "select ha from Reportha ha where ha.reporthaid=" + new Long(ft.getPreporthaid()).toString()
                 );
   
              // check permissions for move
              if (!getPermHelper().hasGlobalPerm(ftpfha.getVisibility(),auth.getValue("resourceperm") + "MOVEOTH")) {
                 mav.addObject("status","NOAUTH");
                 return mav;
              }
   
           }
        } else {
           // pull reportha record
           ftpfha=(Reportha)getMainDAO().execUniqueQuery(
              "select ha from Reportha ha where ha.reporthaid=" + request.getParameter("reporthaid")
              );
        }
           
        int ftpfhavis=ftpfha.getVisibility();   

        String siteid=auth.getValue("siteid");
        String studyid=auth.getValue("studyid");

        List fha=getMainDAO().execQuery(
           "select fha from Reportha fha " +
                 "where (fha.visibility=" + new Integer(ftpfhavis).toString() + " and " +
                        "((" +
                           "fha.visibility=" + VisibilityConstants.SYSTEM + 
                           ") or (" +
                           "fha.visibility=" + VisibilityConstants.SITE + " and fha.studies.sites.siteid=" + siteid + 
                           ") or (" +
                           "fha.visibility=" + VisibilityConstants.STUDY + " and fha.studies.studyid=" + studyid + 
                        "))) and fha.resourcetype=" + auth.getValue("resourcetype") + " " +
                 "order by haordr"
           );

        Iterator i=fha.iterator();
        StringBuilder sb=new StringBuilder();
        while (i.hasNext()) {
           Reportha ha=(Reportha)i.next();
           sb.append(new Long(ha.getReporthaid()).toString());
           if (i.hasNext()) sb.append(",");
        }

        ////////////////////////////////////////////////////////////////////
        // Organize data into heirarchical format for building HTML table //
        ////////////////////////////////////////////////////////////////////

        // ft holds number of rows in table.  Keep this info for later
        //int tablerows=ft.size();
        int tablerows=0;

        // tlist holds table columns
        ArrayList tlist=new ArrayList();
        // alist holds items within columns
        ArrayList alist=new ArrayList();

        // pull first table column
        i=fha.iterator();
        while (i.hasNext()) {
           Reportha ha=(Reportha)i.next();
           if (ha.getPreporthaid()==null) {
              HashMap map=new HashMap();
              map.put("obj",ha);
              map.put("nrows",0);
              map.put("ncols",1);
              // remove element from collection so we don't have to compare it again
              i.remove();
              alist.add(map);
           }
        }   
        tlist.add(alist);
        // pull subsequent columns by finding children of elements in previous columns
        for (int ii=1;ii>-1;ii++) {
           // pull elements in prior column
           ArrayList plist=(ArrayList)tlist.get(ii-1);
           alist=new ArrayList();
           Iterator i2=plist.iterator();
           boolean again=false;
           //iterate through prior column elements
           while (i2.hasNext()) {
              // find children
              HashMap pmap=(HashMap)i2.next();
              Reportha pha=(Reportha)pmap.get("obj");
              i=fha.iterator();
              boolean havechild=false;
              int prevsize=alist.size();
              while (i.hasNext()) {
                 Reportha cha=(Reportha)i.next();
                 // write child out to alist
                 if (cha.getPreporthaid().equals(pha.getReporthaid())) {
                    HashMap cmap=new HashMap();
                    cmap.put("obj",cha);
                    cmap.put("nrows",0);
                    cmap.put("ncols",1);
                    // remove element from collection so we don't have to compare it again
                    i.remove();
                    alist.add(cmap);
                    again=true;
                    havechild=true;
                 }
                 
              }
              // HA-ONLY Specific Code
              if (!havechild) {
                 tablerows++;
                 pmap.put("nrows",1);
                 pmap.put("havechild","N");
              } else {
                 pmap.put("havechild","Y");
              }
           }
           if (!again) break;
           tlist.add(alist);
        }

        // add forms to appropriate category (must iterate backwards through tlist
        // since forms are attached as the right most element)
        for (int ii=(tlist.size()-1);ii>=0;ii--) {
           // pull column arraylist
           ArrayList plist=(ArrayList)tlist.get(ii);
           Iterator i2=plist.iterator();
           while (i2.hasNext()) {
              HashMap pmap=(HashMap) i2.next();
              Reportha pha=(Reportha)pmap.get("obj");
              // formlist holds forms belonging to an element
              ArrayList reportlist=new ArrayList();
              Long compid=null;
              try {
                 compid=new Long(pha.getPreporthaid());
              } catch (Exception cie) { }
              // loop through prior columns looking for parent elements
              int addrows=1;
              if (pmap.get("havechild").equals("N")){ 
                 pmap.put("ncols",tlist.size()-ii);
                 for (int jj=(ii-1);jj>=0;jj--) {
                    ArrayList pplist=(ArrayList)tlist.get(jj);
                    Iterator i4=pplist.iterator();
                    while (i4.hasNext()) {
                       HashMap ppmap=(HashMap) i4.next();
                       Reportha ppha=(Reportha)ppmap.get("obj");
                       // if find a parent element, add rows to that element.  Then update
                       // compid to that element's parent for next loop
                       if ((
                              (!(compid==null)) && ppha.getReporthaid()==compid.longValue()
                           ) ||
                           (
                              ppha.getReporthaid()==pha.getReporthaid() && ppha.getPreporthaid()==pha.getPreporthaid()
                          )) {
                          int nrows=((Integer)ppmap.get("nrows")).intValue();
                          nrows=nrows+addrows;
                          ppmap.put("nrows",nrows);
                          try {
                             compid=new Long(ppha.getPreporthaid());
                          } catch (Exception cie2) {}
                       }
                    }
                 }
              }
           }
        }
        
        /////////////////////////////////
        // Create HTML Table of result //
        /////////////////////////////////

        int tablecols=tlist.size();
        if (tablerows<1 || tablecols<1) {
           return null;
        }
        Table table=new Table(tablerows,tablecols);
        // iterate through table columns;
        for (int currcol=0;currcol<tlist.size();currcol++) {
           ArrayList rowlist=(ArrayList)tlist.get(currcol);
           int prevrow=0;
           Iterator rowiter=rowlist.iterator();
           while (rowiter.hasNext()) {
              HashMap map2=(HashMap)rowiter.next();
              Reportha ha2=(Reportha)map2.get("obj");
              int nrows=((Integer)map2.get("nrows")).intValue();
              int ncols=((Integer)map2.get("ncols")).intValue();
              if (nrows<=0 || ncols<=0) continue;
              Cell cell=new Cell(new Long(ha2.getReporthaid()).toString(),nrows,ncols);
              cell.setContent("type","reportha");
              cell.setContent("obj",ha2);
              boolean okvar=false;
              int currrow=prevrow;
              // Use previous row & exceptions to calculate cell row placement.  This could probably
              // be made more efficient by running through a calculation loop, but the loss here 
              // is quite minimal
              int firstrow=0;
              while (!okvar) {
                 try {
                    table.setCell(cell,currrow,currcol);
                    firstrow=currrow;
                    prevrow=currrow+1;
                    okvar=true;
                 } catch (Exception te) { 
                    currrow++;
                    if (currrow>500000) throw new FORMSException("Too many rows returned");
                 }
              }   
           }
        }

        mav.addObject("status","MOVECATSEL");
        if (moveobj!=null) mav.addObject("passobj",moveobj);
        mav.addObject("table",table);
        return mav;

      } catch (Exception e) {

        mav.addObject("status","MOVECATERROR");
        return mav;

      }

   }

   private String moveCatSubmit() {
      String reporthaid=getRequest().getParameter("reporthaid");
      if (getRequest().getParameter("objecttype").indexOf("Reportinfo")>=0) {
         // Move form
         String reportinfoid=getRequest().getParameter("objectid");
         Reportinfo ft=(Reportinfo)getMainDAO().execUniqueQuery(
            "select ft from Reportinfo ft where reportinfoid=" + reportinfoid
            );
         ft.setPreporthaid(new Long(reporthaid));
         getMainDAO().saveOrUpdate(ft);
      } else  if (getRequest().getParameter("objecttype").indexOf("Remotereportcat")>=0) {
         // Move form
         String rreportcatid=getRequest().getParameter("objectid");
         Remotereportcat ft=(Remotereportcat)getMainDAO().execUniqueQuery(
            "select ft from Remotereportcat ft where ft.rreportcatid=" + rreportcatid
            );
            
         ft.setPreporthaid(new Long(reporthaid));
         getMainDAO().saveOrUpdate(ft);

      }
      clearSessionLists();
      return "FILEMOVED";
   }

   private String deleteFile() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      FORMSAuth auth=getAuth();
      String reportinfoid=getRequest().getParameter("reportinfoid");
      Reportinfo ft=(Reportinfo)getMainDAO().execUniqueQuery(
         "select ft from Reportinfo ft where ft.reportinfoid=" + reportinfoid
         );

      Reportha ftpfha=(Reportha)getMainDAO().execUniqueQuery(
         "select ha from Reportha ha where ha.reporthaid=" + ft.getPreporthaid()
         );

      // check permissions for move
      if (!ft.getUploaduser().equals(new Long(auth.getValue("auserid")))) {
         if (!getPermHelper().hasGlobalPerm(ftpfha.getVisibility(),auth.getValue("resourceperm") + "DELOTH")) {
            return "NOAUTH";
         }
      }

      getMainDAO().delete(ft);
      clearSessionLists();
      return "FILEDELETED";
   }

   private ModelAndView showInfo() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=this.getAuth();

      Reportinfo rinfo=null;
      Allusers uinfo=null;
      String instacr;

      if (auth.getValue("islocal").equalsIgnoreCase("TRUE")) {

         // PULL LOCAL FORM INFORMATION
         instacr="LOCAL";
         String reportinfoid=auth.getValue("reportinfoid");
         rinfo=(Reportinfo)getMainDAO().execUniqueQuery(
            "select rinfo from Reportinfo rinfo where rinfo.reportinfoid=" + reportinfoid
            );
         uinfo=(Allusers)getMainDAO().execUniqueQuery(
            "select u from Allusers u where u.auserid=" + rinfo.getUploaduser()
            );
         mav.addObject("userdesc",uinfo.getUserdesc());

         Reportha ftpfha=(Reportha)getMainDAO().execUniqueQuery(
            "select ha from Reportha ha where ha.reporthaid=" + rinfo.getPreporthaid()
            );
   
         // check permissions for move
         if (!rinfo.getUploaduser().equals(new Long(auth.getValue("auserid")))) {
            if (!getPermHelper().hasGlobalPerm(ftpfha.getVisibility(),auth.getValue("resourceperm") + "EDITOTH")) {
               mav.addObject("status","NOAUTH");
               return mav;
            }
         }

      } else {

         // PULL REMOTE FORM INFORMATION
         String reportinfoid=auth.getValue("reportinfoid");

         Linkedinst linst=(Linkedinst)getMainDAO().execUniqueQuery(
            "select s.linkedinst from Linkedstudies s where s.lstdid=" + auth.getValue("lstdid")
            );
         instacr=linst.getAcronym();   

         SsaServiceObjectResult result=(SsaServiceObjectResult)submitServiceRequest(linst,"getReportServiceTarget","getInfoRecord",reportinfoid);

         try {

            rinfo=(Reportinfo)result.getObject();

         } catch (Exception e) {

             mav.addObject("status","COULDNOTPULL");
             return mav;

         }

         if (result==null || result.getStatus()!=FORMSServiceConstants.OK || rinfo==null) {

             mav.addObject("status","COULDNOTPULL");
             return mav;
               
         }


      }

      mav.addObject("status","SHOWINFO");
      mav.addObject("rinfo",rinfo);
      mav.addObject("instacr",instacr);
      //mav.addObject("filesize",rinfo.getFilesize());
      return mav;

   }

   private ModelAndView editAttr() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=getAuth();
      String reportinfoid=getRequest().getParameter("reportinfoid");
      Reportinfo ft=(Reportinfo)getMainDAO().execUniqueQuery(
         "select ft from Reportinfo ft where ft.reportinfoid=" + reportinfoid
         );

      Reportha ftpfha=(Reportha)getMainDAO().execUniqueQuery(
         "select ha from Reportha ha where ha.reporthaid=" + ft.getPreporthaid()
         );

      // check permissions for move
      if (!ft.getUploaduser().equals(new Long(auth.getValue("auserid")))) {
         if (!getPermHelper().hasGlobalPerm(ftpfha.getVisibility(),auth.getValue("resourceperm") + "EDITOTH")) {
            mav.addObject("status","NOAUTH");
            return mav;
         }
      }
      mav.addObject("status","EDITATTR");
      mav.addObject("rinfo",ft);
      return mav;

   }

   private String editAttrSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String reportinfoid=getRequest().getParameter("reportinfoid");
      String description=getRequest().getParameter("description");
      String creationdate=getRequest().getParameter("creationdate");
      String notes=getRequest().getParameter("notes");

      Reportinfo ft=(Reportinfo)getMainDAO().execUniqueQuery(
         "select ft from Reportinfo ft where ft.reportinfoid=" + reportinfoid
         );
      ft.setDescription(description);   
      ft.setCreationdate(creationdate);   
      ft.setNotes(notes);   
      getMainDAO().saveOrUpdate(ft);
      clearSessionLists();
      return "EDITCOMPLETE";
   }

   private String removeCat() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      FORMSAuth auth=getAuth();
      String reporthaid=getRequest().getParameter("reporthaid");
      Reportha fa=(Reportha)getMainDAO().execUniqueQuery(
         "select fa from Reportha fa where fa.reporthaid=" + reporthaid
         );
      // check permissions 
      if (!getPermHelper().hasGlobalPerm(fa.getVisibility(),auth.getValue("resourceperm") + "CAT")) {
         return "NOAUTH";
      }
      // make sure category is not a root category
      if (fa.getPreporthaid()==null) {
         return "NOREMOVEROOT";
      }
      // make sure category contains no subcategories
      List clist=getMainDAO().execQuery(
         "select fa from Reportha fa where fa.preporthaid=" + reporthaid
         );
      if (clist.size()>0) {
         return "NOREMOVESUBCAT";
      }
      // Move existing files to parent   
      long preporthaid=fa.getPreporthaid();
      List l=getMainDAO().execQuery(
         "select ft from Reportinfo ft where ft.preporthaid=" + reporthaid
         );
      Iterator i=l.iterator();
      while (i.hasNext()) {
         Reportinfo ft=(Reportinfo)i.next();
         ft.setPreporthaid(preporthaid);
         getMainDAO().saveOrUpdate(ft);
      }
      // Remove record
      getMainDAO().delete(fa);
      clearSessionLists();
      return "CATCOLLAPSED";
   }

   // Re-order categories within parent category
   public List reorderCat() {

      Long reporthaid=new Long(getRequest().getParameter("reporthaid"));

      Long preporthaid=(Long)getMainDAO().execUniqueQuery(
                "select ha.preporthaid from Reportha ha where ha.reporthaid=" + reporthaid
                );

      List l=getMainDAO().execQuery(
                "select ha from Reportha ha where ha.preporthaid=:preporthaid " +
                   "order by ha.haordr" 
                ,new String[] { "preporthaid" }
                ,new Object[] { preporthaid }
                );
      
      return l;

   }

   public String reorderCatSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=getAuth();
      String corderstr=(String)request.getParameter("corderstr");
      String[] sa=corderstr.split(",");
      boolean alreadychecked=false;
      for (int i=0;i<sa.length;i++) {
         Long reporthaid=new Long(sa[i].replaceFirst("^li_",""));

         // Make sure has permission for reorder
         if (!alreadychecked) {
            Reportha cha=(Reportha)getMainDAO().execUniqueQuery(
               "select ha from Reportha ha where reporthaid=" + reporthaid
               );
            if (!getPermHelper().hasGlobalPerm(cha.getVisibility(),auth.getValue("resourceperm") + "CAT")) {
               return "NOAUTH";
            }
         }
         alreadychecked=true;
   
         Reportha fa2mod=(Reportha)getMainDAO().execUniqueQuery(
                   "select ha from Reportha ha where ha.reporthaid=:reporthaid"
                   ,new String[] { "reporthaid" }
                   ,new Object[] { reporthaid }
                   );
         fa2mod.setHaordr(i+1);
         getMainDAO().saveOrUpdate(fa2mod);
      }
      clearSessionLists();
      return "CATREORDERED";

   }

   private ModelAndView viewAllCat(ModelAndView mav) {

      mav=moveCat(mav,true);
      String status=(String)mav.getModelMap().get("status");  
      if (status.equalsIgnoreCase("MOVECATSEL")) {
        mav.addObject("status","VIEWALLSEL"); 
      }
      return mav;
      
   }

   private ModelAndView listProfiles(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {
         FORMSAuth auth=getAuth();
         List l=getMainDAO().execQuery(
            "select p from Reportpermprofiles p where " +
               "((p.profilelevel=" + VisibilityConstants.SYSTEM + ") or " +
               "(p.profilelevel=" + VisibilityConstants.SITE + 
                  " and p.siteid=" + auth.getValue("siteid") + ") or " +
               "(p.profilelevel=" + VisibilityConstants.STUDY + 
                  " and p.studyid=" + auth.getValue("studyid") + ")) and " + 
               "p.resourcetype=" + auth.getValue("resourcetype") 
                  );
         mav.addObject("status","LISTPROFILES");
         Iterator i=l.iterator();
         ArrayList newl=new ArrayList();
         while (i.hasNext()) {
            Reportpermprofiles p=(Reportpermprofiles)i.next();
            HashMap map=new HashMap();
            map.put("profile",p);
            map.put("level",new VisibilityConstants().getFieldName(p.getProfilelevel()));
            newl.add(map);
         }
         mav.addObject("list",newl);
         return mav;
      } catch (Exception e) {
         return null;
      }

   }


   // Submit new profile
   private String addProfileSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=getAuth();

      String status=null;
      String profileacr=getRequest().getParameter("profileacr").toUpperCase();
      String profiledesc=getRequest().getParameter("profiledesc");
      int profilelevel=new VisibilityConstants().getFieldValue(getRequest().getParameter("profilelevel"));
      
      // Make sure profile acronym is not duplicated witin level and site/study
      List l;
      long q_siteid;
      try {
         q_siteid=new Long(auth.getValue("siteid")).longValue();
      } catch (Exception sie) {
         q_siteid=new Long("-999999999").longValue();
      }
      long q_studyid;
      try {
         q_studyid=new Long(auth.getValue("studyid")).longValue();
      } catch (Exception sie) {
         q_studyid=new Long("-999999999").longValue();
      }
      l=getMainDAO().execQuery(
            "select p from Reportpermprofiles p where " +
                "p.profileacr=:profileacr and (" +
                   "(" + profilelevel + "=" + VisibilityConstants.SYSTEM + " and p.profilelevel=" + VisibilityConstants.SYSTEM + ") or " +
                   "(" + profilelevel + "=" + VisibilityConstants.SITE + " and p.profilelevel=" + VisibilityConstants.SITE +
                       " and p.siteid=:siteid) or " +
                   "(" + profilelevel + "=" + VisibilityConstants.STUDY  + " and p.profilelevel=" + VisibilityConstants.STUDY +
                       " and p.studyid=:studyid)" +
                ")"
             ,new String[] { "profileacr","siteid","studyid" }
             ,new Object[] { profileacr,q_siteid,q_studyid }   
         );
      if (l.size()>0) {
         status="DUPLICATEPROFILE";
         return status;
      }
      // If not duplicate, add to database
      Reportpermprofiles profile=new Reportpermprofiles();
      profile.setProfileacr(profileacr);
      profile.setProfiledesc(profiledesc);
      profile.setProfilelevel(profilelevel);
      profile.setResourcetype(new Integer(auth.getValue("resourcetype")).intValue());   
      try {
         profile.setStudyid(new Long(auth.getValue("studyid")).longValue());
      } catch (Exception sse) {
         // may be null for system level profiles
      }
      try {
         profile.setSiteid(new Long(auth.getValue("siteid")).longValue());
      } catch (Exception sse) {
         // may be null for system level profiles
      }
      getMainDAO().saveOrUpdate(profile);
      getMAV().addObject("profilelevel",new VisibilityConstants().getFieldName(profilelevel));
      getMAV().addObject("profileacr",profileacr);
      status="PROFILEADDED";
      return status;

   }

   // Modify profile interface
   private String modifyProfile() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String status=null;
      long rprofileid=new Long(getRequest().getParameter("rprofileid")).longValue();
      
      // Make sure profile acronym is not duplicated witin level and site/study
      Reportpermprofiles p=(Reportpermprofiles)getMainDAO().execUniqueQuery(
            "select p from Reportpermprofiles p where p.rprofileid=" + rprofileid 
         );
      if (p==null) {
         status="PROFILEPULLERROR";
         return status;
      }

      // See if user is permitted to modify profile
      if (!getPermHelper().hasGlobalPerm(p.getProfilelevel(),getAuth().getValue("resourceperm") + "PERMPROF")) {
         // not permitted
         return "NOAUTH";
      }

      getMAV().addObject("profile",p);
      getMAV().addObject("level",new VisibilityConstants().getFieldName(p.getProfilelevel()));
      status="MODIFYPROFILE";
      return status;

   }

   // Submit modified profile
   private String modifyProfileSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=getAuth();

      String status=null;
      String profileacr=getRequest().getParameter("profileacr").toUpperCase();
      String profiledesc=getRequest().getParameter("profiledesc");
      int profilelevel=new VisibilityConstants().getFieldValue(getRequest().getParameter("profilelevel"));
      long rprofileid=new Long(getRequest().getParameter("rprofileid")).longValue();
      
      // Make sure profile acronym is not duplicated witin level and site/study
      List l;
      long q_siteid;
      try {
         q_siteid=new Long(auth.getValue("siteid")).longValue();
      } catch (Exception sie) {
         q_siteid=new Long("-999999999").longValue();
      }
      long q_studyid;
      try {
         q_studyid=new Long(auth.getValue("studyid")).longValue();
      } catch (Exception sie) {
         q_studyid=new Long("-999999999").longValue();
      }
      l=getMainDAO().execQuery(
            "select p from Reportpermprofiles p where " +
                "p.rprofileid!=:rprofileid and " +
                "p.profileacr=:profileacr and (" +
                   "(" + profilelevel + "=" + VisibilityConstants.SYSTEM + " and p.profilelevel=" + VisibilityConstants.SYSTEM + ") or " +
                   "(" + profilelevel + "=" + VisibilityConstants.SITE + " and p.profilelevel=" + VisibilityConstants.SITE +
                       " and p.siteid=:siteid) or " +
                   "(" + profilelevel + "=" + VisibilityConstants.STUDY  + " and p.profilelevel=" + VisibilityConstants.STUDY +
                       " and p.studyid=:studyid)" +
                ")"
             ,new String[] { "rprofileid","profileacr","siteid","studyid" }
             ,new Object[] { rprofileid,profileacr,q_siteid,q_studyid }   
         );
      if (l.size()>0) {
         status="DUPLICATEPROFILE";
         return status;
      }
      // If not duplicate, add to database
      Reportpermprofiles profile=(Reportpermprofiles)getMainDAO().execUniqueQuery(
         "select p from Reportpermprofiles p where rprofileid=" + rprofileid
         );
      if (profile!=null) {   
         profile.setProfileacr(profileacr);
         profile.setProfiledesc(profiledesc);
         getMainDAO().saveOrUpdate(profile);
         status="PROFILEMODIFIED";
         return status;
      }
      status="PROFILEPULLERROR";
      return status;

   }

   // Delete profile
   private String removeProfileSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String status=null;
      long rprofileid=new Long(getRequest().getParameter("rprofileid")).longValue();

      // Can only delete profile if no files are using it
      List l=getMainDAO().execQuery(
         "select pa from Reportpermprofassign pa where pa.reportpermprofiles.rprofileid=" + rprofileid
         );
      if (l.size()>0) {
         status="PROFILENOTEMPTY";
         return status;
      }
      
      Reportpermprofiles profile=(Reportpermprofiles)getMainDAO().execUniqueQuery(
            "select profile from Reportpermprofiles profile where profile.rprofileid=" + rprofileid 
         );
      if (profile==null) {
         status="PROFILEPULLERROR";
         return status;
      }
      // See if user is permitted to modify profile
      if (!getPermHelper().hasGlobalPerm(profile.getProfilelevel(),getAuth().getValue("resourceperm") + "PERMPROF")) {
         // not permitted
         return "NOAUTH";
      }
      getMainDAO().delete(profile);
      status="PROFILEDELETED";
      return status;

   }

}

