 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * GroupmgtController.java - Group Management Controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.lang.Thread;
import javax.servlet.http.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;

public class GroupmgtController extends FORMSController {

   public ModelAndView submitRequest() throws FORMSException {

      try {

         FORMSAuth auth=this.getAuth();

         ModelAndView mav=new ModelAndView();
         HttpServletResponse response=this.getResponse();
         PrintWriter out=this.getWriter();

         String sortcolumn=request.getParameter("SORTCOLUMN");

         if (auth.getValue("sortid")==null || !auth.getValue("sortid").equals("GROUPLIST")) {

            auth.setValue("sortid","GROUPLIST");
            auth.setValue("sortcolumn","groupacr");
            auth.setValue("sortdir","asc");
         }

         if (sortcolumn==null) {

            // Process Data

            String studyid=auth.getValue("studyid");
    
            ArrayList<Map> passList=new ArrayList<Map>();
            List l=getMainDAO().execQuery(
               "select rr from Analystgroups as rr where rr.createuser=" + auth.getValue("auserid") +
               " order by rr." + auth.getValue("sortcolumn") + " " + auth.getValue("sortdir") + "," +
                  " rr.groupacr"
            );

            if (l!=null) {
               Iterator i=l.iterator();
               while (i.hasNext()) {
                  Map<String,Object> map = new HashMap<String,Object>();
                  // NOTE:  May retrieve either studies or studyrights object
                  Object o=i.next();
                  try {
                     Analystgroups agroup=(Analystgroups)o;
                     map.put("agroup",agroup);
                     passList.add(map);
                  } catch (Exception e) { }
               }   
            }   

            ArrayListResult result=new ArrayListResult();
            result.setList(passList);
            result.setStatus("OK");

            this.getSession().setAttribute("iframeresult",result);

            mav.addObject("status","OK");
            return mav;
   
         }
         else if (sortcolumn!=null) {

            String oldsortcol=auth.getValue("sortcolumn");
            String oldsortdir=auth.getValue("sortdir");
            if (oldsortcol.equalsIgnoreCase(sortcolumn)) {
               if (oldsortdir.equalsIgnoreCase("asc")) {
                  auth.setValue("sortdir","desc");
               } else {
                  auth.setValue("sortdir","asc");
               }
            } else {
               auth.setValue("sortcolumn",sortcolumn);
               auth.setValue("sortdir","asc");
            }

            mav.addObject("status","SORT");
            return mav;

         }
         
         return null;
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

}

