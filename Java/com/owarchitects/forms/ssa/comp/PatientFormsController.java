 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * PatientFormsController.java - Form Selection Interface
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.mla.html.table.*;
import org.json.simple.JSONObject;

public class PatientFormsController extends FORMSSsaServiceClientController {

   private PatientFormsSortMap namemap=null;

   public ModelAndView submitRequest() throws FORMSException {

      try {

         FORMSAuth auth=this.getAuth();

         // use classname-based view
         mav=new ModelAndView("PatientForms");

         HttpServletResponse response=this.getResponse();
         PrintWriter out=this.getWriter();

         // Initialize Options (If necessary)
         //auth.initOption("typeopt","ALL");
         //auth.initOption("veropt","REC");
         //auth.initOption("archopt","Y");
         //auth.initOption("fnumopt","");
         //auth.initOption("sinstopt","");

         // clear stored values
         //auth.setValue("reportinfoid","");

         int resourceType;
         // 
         if (getRequest().getServletPath().indexOf("callpatientforms")>=0) {

            return callPatientForms(mav);

         } else if (getRequest().getServletPath().indexOf("patientrecordformtype")>=0) {

            String dtdefid=(String)request.getParameter("dtdefid");
            String datarecordid=(String)request.getParameter("datarecordid");

            // If repull, go straight to SELECTFORMTYPE GUI
            if (dtdefid==null) {
               mav.addObject("status","SELECTFORMTYPE");
               return mav;
            }

            generateFormtypeList(dtdefid,datarecordid);
            List ftsl=(List)auth.getObjectValue("ftsl");
            if (ftsl!=null && ftsl.size()>1) {
               // if multiple formtypes bring up selection screen
               ListResult result=new ListResult();
               result.setList(ftsl);
               this.getSession().setAttribute("iframe2result",result);
               // redirect (call again) to avoid "Page has expired" messages
               safeRedirect("patientrecordformtype.do");
               return null;
            } else {
               Formtypes ft=(Formtypes)ftsl.get(0);
               auth.setValue("formtypeid",new Long(ft.getFormtypeid()).toString());
               safeRedirect("pullpatientrecord.do");
               return null;
            }   

         } else if (getRequest().getServletPath().indexOf("pullpatientrecord")>=0) {

            String fid=(String)getRequest().getParameter("formtypeid");
            if (fid!=null) {
               auth.setValue("formtypeid",fid);
               // redirect (call again) to avoid "Page has expired" messages
               safeRedirect("pullpatientrecord.do");
               return null;
            }   
            pullPatientRecord();
            auth.setObjectValue("ftsl",null);
            return null;

         } else {

            String function=getRequest().getParameter("_function_");
            if (function!=null && function.equals("SHOWRECORDS")) {
               showRecords();
               safeRedirect("patientforms.do");
               return null;
            } else {
               // redirect (call again) to avoid "Page has expired" messages
               mav.addObject("status","SHOWRECORDS");
               mav.addObject("infostr",auth.getValue("infostr"));
               return mav;
            }

         }
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Pull List of ID fields
   private ModelAndView callPatientForms(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Get list of ID variables used in forms
      List hal=getMainDAO().execQuery(
         "select f from Formha f where f.studies.studyid=" + getAuth().getValue("studyid")
         );
      StringBuilder sb=new StringBuilder();
      Iterator i=hal.iterator();
      while (i.hasNext()) {
         Formha ha=(Formha)i.next();
         Long fid=new Long(ha.getFormhaid());
         sb.append(fid);
         if (i.hasNext()) sb.append(",");
      }
      List forml=getMainDAO().execQuery(
         "select f from Datatabledef f join fetch f.allinst where f.pformhaid in (" + sb + ")"
         );
      sb=new StringBuilder();
      ArrayList aidlist=new ArrayList();
      i=forml.iterator();
      while (i.hasNext()) {
         Datatabledef fdef=(Datatabledef)i.next();
         Long fid=new Long(fdef.getDtdefid());
         sb.append(fid);
         aidlist.add(fid);
         if (i.hasNext()) sb.append(",");
      }
      getAuth().setObjectValue("aidlist",aidlist);
      List l=getMainDAO().execQuery(
         "select distinct count(*) from Datatablecoldef f " + 
            "where f.datatabledef.dtdefid in (" + sb + ") and f.isidfield=true " +
               "group by f.datatabledef.dtdefid " +  
               "order by count(*) " 
            );
      ArrayList passlist=new ArrayList();      
      i=l.iterator();
      int ncombs=l.size();
      long maxkeys=0;
      while (i.hasNext()) {
         Long nfields=(Long)i.next();
         passlist.add(nfields);
         maxkeys=nfields>maxkeys ? nfields : maxkeys;
      }
      List vlist=getMainDAO().execQuery(
         "select distinct columnname from Datatablecoldef f " + 
            "where f.datatabledef.dtdefid in (" + sb + ") and f.isidfield=true " +
               "order by columnname " 
            );
      StringBuilder vsb=new StringBuilder();
      i=vlist.iterator();      
      while (i.hasNext()) {
         vsb.append("\'" + i.next() + "\'");
         if (i.hasNext()) vsb.append(",");
      }
      mav.addObject("vstring",vsb.toString());
      mav.addObject("list",passlist);
      mav.addObject("maxkeys",maxkeys);
      mav.addObject("ncombs",ncombs);
      mav.addObject("status","DISPLAYLIST");
      return mav;

   }


   private void generateFormtypeList(String dtdefid,String datarecordid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=getAuth();
      List vlist;
      HttpServletRequest request=getRequest();

      // return ids from form
      auth.setValue("dtdefid",dtdefid);
      auth.setValue("datarecordid",datarecordid);

      List ftsl=(List)new ArrayList();
      List ftl=getFormTypesList();
      Iterator fti=ftl.iterator();
      while (fti.hasNext()) {
         Formtypes ft=(Formtypes)fti.next();
         if (ft.getDatatabledef().getDtdefid()==(new Long(dtdefid).longValue())) {
            ftsl.add(ft);
         }
      }
      auth.setObjectValue("ftsl",ftsl);
      
   }


   // Pull List of ID fields
   private void pullPatientRecord() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=getAuth();
      List vlist;
      HttpServletRequest request=getRequest();

      // return ids from form
      long dtdefid=new Long(auth.getValue("dtdefid")).longValue();
      long datarecordid=new Long(auth.getValue("datarecordid")).longValue();
   
      // Determine whether form is local or remote
      Allinst ainst=(Allinst)getMainDAO().execUniqueQuery(
         "select f.allinst from Datatabledef f where f.dtdefid=" + dtdefid + " and f.allinst.islocal=false"
         );
      int formloc;   
      if (ainst==null) {
         formloc=LockInfo.LOCALFORM;
      } else {
         formloc=LockInfo.REMOTEFORM;
      }


      Linkedinst forminst=null;
      if (formloc==LockInfo.REMOTEFORM) {
         forminst=(Linkedinst)getMainDAO().execUniqueQuery(
               "select a.linkedinst from Allinst a where a.ainstid=" + ainst.getAinstid()
            );
      } 

      // Obtain record lock, if possible
      Datatablerecords rec=null;
      boolean islocked=false;
      boolean lockcleared=false;
      String clearuser="";
      boolean lockobtained=false;
      String lockinfo="";
      String querystring="select f from Datatablerecords f where f.datarecordid=" + datarecordid;
      if (formloc==LockInfo.LOCALFORM) {
         rec=(Datatablerecords)getMainDAO().execUniqueQuery(querystring);
      } else {
         Object[] oarray=execUniqueRemoteQuery(forminst,querystring);
         rec=(Datatablerecords)oarray[0];
      }

      if (!rec.getIslocked()) {
         LockInfo linfo=new LockInfo();
         boolean isok=false;
         if (formloc==LockInfo.LOCALFORM) {
            try {
               lockRecord(rec);
               isok=true;
            } catch (Exception e) { }
         } else {
            try {
               SsaServiceStatusResult result=(SsaServiceStatusResult)submitServiceRequest(forminst,"recordLockingServiceTarget","lockRecord",rec);
               if (result.getStatus()==FORMSServiceConstants.OK) {
                  linfo.setLinkedinst(forminst);
                  isok=true;
               }   
            } catch (Exception e) { 
            }
         }
         if (isok) {
            linfo.setIslocked(true);
            linfo.setFormlocation(formloc);
            linfo.setDatarecordid(datarecordid);
            getAuth().setObjectValue("lockinfo",linfo);
            lockobtained=true;
         } 
      } else {
         islocked=true;
         boolean isok=false;
         LockInfo linfo=new LockInfo();
         // If lock has been held > 2hrs, clear it and issue warning
         if ((System.currentTimeMillis()-rec.getLocktime().getTime())>(1000*60*60*2)) {
            clearuser=rec.getLockinfo();
            if (formloc==LockInfo.LOCALFORM) {
               try {
                  // release current lock
                  unlockRecord(rec);
                  lockcleared=true;
                  islocked=false;
                  // obtain lock for current user
                  unlockRecord(rec);
                  isok=true;
               } catch (Exception e) { }
            } else {
               try {
                  // release current lock
                  SsaServiceStatusResult result=(SsaServiceStatusResult)submitServiceRequest(forminst,"recordLockingServiceTarget","unlockRecord",rec);
                  if (result.getStatus()==FORMSServiceConstants.OK) {
                     lockcleared=true;
                     islocked=false;
                  }   
                  // obtain lock for current user
                  result=(SsaServiceStatusResult)submitServiceRequest(forminst,"recordLockingServiceTarget","lockRecord",rec);
                  if (result.getStatus()==FORMSServiceConstants.OK) {
                     linfo.setLinkedinst(forminst);
                     isok=true;
                  }   
               } catch (Exception e) { }
            }
            // if lock set for current user, update auth object
            if (isok) {
               linfo.setIslocked(true);
               linfo.setFormlocation(formloc);
               linfo.setDatarecordid(datarecordid);
               getAuth().setObjectValue("lockinfo",linfo);
               lockobtained=true;
            } 
         }
         lockinfo=rec.getLockinfo();
      }

      // Pull value list for record
      querystring="select v from Datatablevalues v where v.datatablerecords.datarecordid=" + datarecordid;
      if (formloc==LockInfo.LOCALFORM) {
         vlist=getMainDAO().execQuery(querystring);
      } else {  
         vlist=execRemoteQuery(forminst,querystring);
      }   

      // construct form javascript from values    
      String formtypeid=auth.getValue("formtypeid");

/*
      // NOTE:  There is implicit trust for admin users here.  Even for forms that don't
      // exist or for forms from another study.  We may want to check that the form is
      // under the current study later 
      if (!getPermHelper().hasFormPerm(dtdefid,"NEWRECORD")) {

         mav.addObject("status","NOPERMISSION");
         return mav;

      }
*/

      // Display form
      Formstore fstore=(Formstore)getMainDAO().execUniqueQuery(
         "select f from Formstore f where f.formtypes.formtypeid=" + formtypeid
         );
      //StringBuilder fcontent=new StringBuilder(fstore.getFormcontent());   
      // TEMPORARY TO ALLOW COMPATIBILITY WITH FORMS STORED PRE-VERSION 0.5.0
      StringBuilder fcontent=new StringBuilder(fstore.getFormcontent().replaceFirst("\"formdefid_\"","\"dtdefid_\""));   

      response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
      response.setHeader("Pragma","no-cache"); //HTTP 1.0
      response.setDateHeader ("Expires", -1); //prevents caching at the proxy server

      /*************************************************************/
      /** RETRIEVE VALUES INTO JAVASCRIPT VARIABLES AND OPEN FILE **/
      /*************************************************************/

      ///////////////////////////////////
      // ADD JAVASCRIPT TO FILE STREAM //
      ///////////////////////////////////
   
      // Create LoadScript
      StringBuilder ls=new StringBuilder();    

      ls.append("_numErrors=0;\n");
      ls.append("try { setReadOnly(getFormElementById(\"FormDataOK\")); } catch (e) {}\n");

      // THIS MUST BE LAST, SO WE ARE NOT SETTING PASSVALUE FIELD FROM FILE THAT WAS READ IN
      // Contains server information passed to form
      JSONObject passv=new JSONObject();
      passv.put("submittype","editrecord");
      passv.put("rightslevel",auth.getValue("rightslevel"));
      passv.put("servername",request.getServerName());
      passv.put("continueservlet","selectform.do");
      ls.append("try { _rmsPassValueField='" + passv.toString() + "'; _rmsPassValueObject=jsonParse(_rmsPassValueField); } catch (e) {}\n");

      // get form data values
      Iterator viter=vlist.iterator();
      while (viter.hasNext()) {
         Datatablevalues dvalue=(Datatablevalues)viter.next();
         String encvalue=Base64.encode(dvalue.getVarvalue());
         if (encvalue.length()<100) {
            ls.append("try { setEncodedFormValue(getFormElementById(\"" + dvalue.getVarname() + "\"),'" + encvalue + "'); } catch (e) {}\n");
         } else { 
            // Split long values onto multiple lines
            ArrayList vlist2=new ArrayList();
            while (1==1) {
               String s;
               try {
                  s=encvalue.substring(100);
                  encvalue=encvalue.substring(0,100);
                  vlist2.add("'" + encvalue + "' + \n");
                } catch (Exception ee) {
                  vlist2.add("'" + encvalue + "' + \n");
                  break;
                }
                encvalue=s;
            }
            Iterator viter2=vlist2.iterator();
            StringBuilder sb=new StringBuilder();
            while (viter2.hasNext()) {
              String s=(String)viter2.next();
              sb.append(s);
            }
            encvalue=sb.toString();
            encvalue=encvalue.substring(0,encvalue.lastIndexOf("+"));
            ls.append("try { setEncodedFormValue(getFormElementById(\"" + dvalue.getVarname() + "\")," + encvalue + "); } catch (e) {}\n");
         }
      }

      ls.append("try { getFormElementById(\"dtdefid_\").value='" + dtdefid + "'; } catch (e) { }\n");
      ls.append("_docReady=0;\n");
      ls.append("try { execValidate(); } catch (e) {}\n");
      ls.append("_saveStatus=\"OK\";\n");
      ls.append("_docReady=1;\n");

      if (lockcleared) {
         ls.append("alert('WARNING:  This record was locked by user:  \\n\\n    ' + \n'" +
           clearuser + "\\n\\nHOWVER this lock has expired and has been cleared.    ');"); 
      }     
      if (islocked) {
         ls.append("alert('This record is currently locked by user:  \\n\\n    ' + \n'" +
           lockinfo + "\\n\\nYou will be able to view this form, but not submit it.   ');");
         ls.append("getFormElementById('SubmitButton').disabled='true';");  
      } else if (!lockobtained) {
         ls.append("alert('WARNING:  Record lock could not be obtained.  You may submit this form, ' + \n" +
             "' but submission may not be successful if a lock is held by another user.\\n\\n ' + \n" +
             "'In any case an FORMS Temporary Record will be created allowing for later submission');");
      }

      //auth.setValue("WinNameMissOk","Y");

      String rstr="_insertLoadScriptHere=\"XXXHEREXXX\";";
      int inx=fcontent.indexOf(rstr);
      int inxt =inx+rstr.length();
      String outstring=fcontent.replace(inx,inxt,ls.toString()).toString();
      out.println(outstring);
      out.flush();

   }


   // Show all records matching ID information
   private void showRecords() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      ArrayList namlist=new ArrayList();
      ArrayList reclist=new ArrayList();
      StringBuilder infosb=new StringBuilder();

      try {

         // Construct where clause using specified values
         Enumeration e=request.getParameterNames();
         StringBuilder wsb=new StringBuilder();
         int varcount=0;
         while (e.hasMoreElements()) {
            String k=(String)e.nextElement();
            if (!(k.equalsIgnoreCase("_function_") || k.equalsIgnoreCase("_vstring_"))) {
                varcount++;
                wsb.append(" (f.varname in (" + request.getParameter("_vstring_") + ") and f.varvalue='" + request.getParameter(k) + "') and ");
                String insrtStr=k;
                if (k.equalsIgnoreCase("ID1")) {
                   insrtStr="PrimaryID";
                } else if (k.equalsIgnoreCase("ID2")) {
                   insrtStr="SecondaryID";
                } else if (k.equalsIgnoreCase("ID3")) {
                   insrtStr="TertiaryID";
                } else if (k.equalsIgnoreCase("ID4")) {
                   insrtStr="QuartaryID";
                } else if (k.equalsIgnoreCase("ID5")) {
                   insrtStr="QuintaryID";
                } else if (k.equalsIgnoreCase("ID6")) {
                   insrtStr="SextaryID";
                } 
                infosb.append("&nbsp; " + insrtStr + "=" + request.getParameter(k) + " &nbsp;");
            }
         }
         if (wsb.length()>0) {
            int inx=wsb.lastIndexOf(" and ");
            wsb.delete(inx,inx+4);
         }
   
         // only include records from forms user has access to
         ArrayList locidlist=new ArrayList();
         ArrayList remidlist=new ArrayList();
         ArrayList remfdlist=new ArrayList();
         ArrayList aidlist=(ArrayList)getAuth().getObjectValue("aidlist");
         List ftlist=getFormTypesList();
         Iterator i=ftlist.iterator();
         while (i.hasNext()) {
            Formtypes ft=(Formtypes)i.next();
            if (ft.getDatatabledef().getAllinst().getIslocal()) {
               Long fid=new Long(ft.getDatatabledef().getDtdefid());
               if (aidlist.contains(fid) && (!locidlist.contains(fid))) {
                  locidlist.add(fid);
               }
            } else {
               Long fid=new Long(ft.getDatatabledef().getDtdefid());
               if (aidlist.contains(fid) && (!remidlist.contains(fid))) {
                  remidlist.add(fid);
                  remfdlist.add(ft.getDatatabledef());
               }
            }
         }

         //////////////////////////
         //////////////////////////
         // RETRIEVE LOCAL FORMS //
         //////////////////////////
         //////////////////////////

         // create stringbuffer of resulting ids for insertion into where clause
         i=locidlist.iterator();
         StringBuilder fsb=new StringBuilder();
         while (i.hasNext()) {
            Long fid=(Long)i.next();
            fsb.append(fid);
            if (i.hasNext()) fsb.append(",");
         }
   
         // pull matching records
         List l;
         if (wsb.length()>0 && fsb.length()>0) {
            l=getMainDAO().execQuery(
               "select f.datatablerecords.datarecordid,count(*) from Datatablevalues f " +
                  "where (" + wsb.toString() + ") and " +
                     "(f.datatablerecords.isaudit=false and f.datatablerecords.ishold=false and f.datatablerecords.datatabledef.dtdefid in (" + fsb.toString() + ")) " +
                     "group by f.datatablerecords.datarecordid"
                  );
         } else {
            l=new ArrayList();
         }
         StringBuilder isb=new StringBuilder();
         i=l.iterator();
         while (i.hasNext()) {
            Object[] oarray=(Object[])i.next();
            Long did=(Long)oarray[0];
            Long cnt=(Long)oarray[1];
            if (cnt.intValue()==varcount) {
               isb.append(did + ",");
            }
         }
         if (isb.length()>0) {
            isb.deleteCharAt(isb.lastIndexOf(","));
         }
  
         // create list of all key variables for display (All available key fields will be displayed)
         List vnlist;
         if (fsb.length()>0) {
            vnlist=getMainDAO().execQuery(
               "select distinct f.columnname from Datatablecoldef f where f.datatabledef.dtdefid in (" + fsb.toString() + ") and (iskeyfield=true) "
               );
         } else {
            vnlist=new ArrayList();
         }
         StringBuilder vnsb=new StringBuilder();
         i=vnlist.iterator();   
         while (i.hasNext()) {
            String s=(String)i.next();
            vnsb.append("'" + s + "'" );
            if (i.hasNext()) vnsb.append(",");
         }
  
         // now pull actual key variable values for all forms
         if (isb.length()>0 && vnsb.length()>0) {
            l=getMainDAO().execQuery(
              "select f from Datatablevalues f join fetch f.datatablerecords join fetch f.datatablerecords.datatabledef " +
                 "where f.datatablerecords.datarecordid in (" + isb.toString() + ") and " +
                    "(f.varname in (" + vnsb + ")) " +
                    "order by f.datatablerecords.datatabledef.formno,f.datatablerecords.datatabledef.formver,f.datatablerecords.datarecordid,f.varname"
                 );
         } else {
            l=new ArrayList();
         }
  
         // reorganize data for display 
         i=l.iterator();
         PatientFormsSortMap varmap=null;
         long l_datarecordid=-999999;
         while (i.hasNext()) {
            Datatablevalues v=(Datatablevalues)i.next();
            if (v.getDatatablerecords().getDatarecordid()!=l_datarecordid) {
               // output previous varmap
               if (varmap!=null) reclist.add(varmap);
               varmap=new PatientFormsSortMap();
               varmap.put("datarecordid",v.getDatatablerecords().getDatarecordid());
               varmap.put("dtdefid",v.getDatatablerecords().getDatatabledef().getDtdefid());
               varmap.put("FormNo",v.getDatatablerecords().getDatatabledef().getFormno());
               varmap.put("FormVer",v.getDatatablerecords().getDatatabledef().getFormver());
               varmap.put("FormAcr",v.getDatatablerecords().getDatatabledef().getFormacr());
               varmap.put(v.getVarname(),v.getVarvalue());
               varmap.put("User",getUserName(v.getDatatablerecords().getSaveuser()));
               varmap.put("Time",v.getDatatablerecords().getSavetime().toString().replaceFirst("\\..*$",""));
               l_datarecordid=v.getDatatablerecords().getDatarecordid();
            } else {
               varmap.put(v.getVarname(),v.getVarvalue());
            }
            if (!i.hasNext()) reclist.add(varmap);
         }

         // create field name list
         namlist=new ArrayList();
         namlist.add("datarecordid");
         namlist.add("dtdefid");
         namlist.add("FormNo");
         namlist.add("FormVer");
         namlist.add("FormAcr");
         namlist.addAll(vnlist);
         namlist.add("User");
         namlist.add("Time");

         ///////////////////////////
         ///////////////////////////
         // RETRIEVE REMOTE FORMS //
         ///////////////////////////
         ///////////////////////////

         // Create list for each linked study then submit list to each study

         while (remfdlist.size()>0) {
            i=remfdlist.iterator();
            Allinst ainst=null;
            ArrayList passlist=new ArrayList();
            while (i.hasNext()) {
               Datatabledef fd=(Datatabledef)i.next();
               // should not happen, but just in case 
               if (fd.getAllinst()==null) {
                  i.remove();
               }   
               // cumulate datatabledefs for a single study, then submit list to service
               if (ainst==null) {
                  ainst=fd.getAllinst();
                  passlist.add(fd);
                  i.remove();
               } else if (ainst.getAinstid()==fd.getAllinst().getAinstid()) {
                  passlist.add(fd);
                  i.remove();
               }
            }
            // pull linkedinst record for submission
            Linkedinst linst=(Linkedinst)getMainDAO().execUniqueQuery(
                  "select a.linkedinst from Allinst a where a.ainstid=" + ainst.getAinstid()
               );
            // submit datatabledef list and where clause to server to retrieve patient records
            PatientFormsServiceParms parms=new PatientFormsServiceParms();
            parms.setBeanname("patientRecordsServiceTarget");
            parms.setMethodname("retrievePatientRecords");
            parms.setWhereclause(wsb.toString());
            parms.setVarcount(varcount);
            parms.setList(passlist);
            SsaServiceListResult result=(SsaServiceListResult)submitServiceRequest(linst,parms);
            reclist.addAll(result.getList());

         }

       } catch (Exception eee) {
          // Do nothing (send empty results)
       }

       Collections.sort(reclist);

       HashMap map=new HashMap();
       map.put("reclist",reclist);
       map.put("namlist",namlist);
       ObjectResult result=new ObjectResult();
       result.setObject(map);
       // Save results to HashMap for later retrieval
       HashMap ptmap=new HashMap();
       this.getSession().setAttribute("iframeresult",result);
       getAuth().setValue("infostr",infosb.toString());

   }


   // Create Comparable map class for output
   private List getFormTypesList() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Create all form lists if necessary
      FORMSAuth auth=this.getAuth();
      String siteid=auth.getValue("siteid");
      String studyid=auth.getValue("studyid");

      // Use session-stored lists if available to avoid frequent hits to database for pulling form list
      List fhatemp=(List)auth.getSessionAttribute("formhalist");
      List fttemp=(List)auth.getSessionAttribute("formtypeslist");
      HashMap iinfotemp=(HashMap)auth.getSessionAttribute("iinfomap");
      Iterator i;

      // If no/empty list available, pull from database
      if (fhatemp==null || fttemp==null || fhatemp.size()<1 || fttemp.size()<1) {

         // Retrive installation information for later
         List ilist=getMainDAO().execQuery(
            "select a from Allinst a join fetch a.linkedinst where a.islocal!=true"
            );
         i=ilist.iterator();
         HashMap iinfomap=new HashMap();
         while (i.hasNext()) {
           Allinst ainst=(Allinst)i.next();
           iinfomap.put(new Long(ainst.getAinstid()).toString(),ainst.getLinkedinst().getAcronym());
         }

         fhatemp=getMainDAO().execQuery(
            "select fha from Formha fha " +
                  "where ((" +
                            "fha.visibility=" + VisibilityConstants.SYSTEM + 
                            ") or (" +
                            "fha.visibility=" + VisibilityConstants.SITE + " and fha.studies.sites.siteid=" + siteid + 
                            ") or (" +
                            "fha.visibility=" + VisibilityConstants.STUDY + " and fha.studies.studyid=" + studyid + 
                         ")) " +
                  "order by haordr"
            );
     
         i=fhatemp.iterator();
         // 1) Write formhaid to StringBuilder for use in formtypes pull
         // 2) Verify that root system, site & study records exist.  Otherwise create them
         StringBuilder sb=new StringBuilder();
         boolean hassystem=false, hassite=false, hasstudy=false;
         while (i.hasNext()) {
            Formha ha=(Formha)i.next();
            // write id to stringbuffer
            sb.append(new Long(ha.getFormhaid()).toString());
            if (i.hasNext()) sb.append(",");
            // check that necessary root hierarchy records exist, else create them
            if (ha.getVisibility()==VisibilityConstants.SYSTEM && ha.getPformhaid()==null) {
               hassystem=true;
            } else if (ha.getVisibility()==VisibilityConstants.SITE && ha.getPformhaid()==null) {
               hassite=true;
            } else if (ha.getVisibility()==VisibilityConstants.STUDY && ha.getPformhaid()==null) {
               hasstudy=true;
            } 
         }
         if (!hassystem) createHaRecord(VisibilityConstants.SYSTEM);
         if (!hassite) createHaRecord(VisibilityConstants.SITE);
         if (!hasstudy) createHaRecord(VisibilityConstants.STUDY);

         // Only include formtypes user has permission to
         StringBuilder ftsb=new StringBuilder();
         List ftypespermlist=getPermHelper().getFormtypesPermList();

         i=ftypespermlist.iterator();
         // pull system/site/study permissions 
         Permissions p=(Permissions)getMainDAO().execUniqueQuery(
            "select p from Permissions p join fetch p.permissioncats " +
               "where p.permissioncats.pcatvalue=" + PcatConstants.FORMTYPE + " and " +
                     "p.permissioncats.pcatscope=" + PcatscopeConstants.RESOURCE + " and " + 
                     "p.permacr='VIEWTYPE'"
               );

         while (i.hasNext()) {
            HashMap map=(HashMap)i.next();
            BitSet b=(BitSet)map.get("permlevel");
            if (b.get(p.getPermpos()-1)) { 
               String idstring=(String)map.get("formtypelistid");
               ftsb.append(idstring + ",");
            }
         }   
         int ftsbl=ftsb.length();
         if (ftsbl>0) {
            ftsb.deleteCharAt(ftsbl-1);
         }
   
         // Pull formtypes, datatabledef data
         if (sb.length()>0 && ftsb.length()>0) {
            fttemp=getMainDAO().execQuery(
               "select ft from Formtypes ft join fetch ft.datatabledef join fetch ft.datatabledef.allinst join fetch ft.formtypelist " +
                     "join fetch ft.formformats join fetch ft.formtypelist join fetch ft.formtypelist.allinst " +
                  "where ft.datatabledef.pformhaid in (" 
                     + sb.toString() + ") and ft.formtypelist.formtypelistid in (" 
                       + ftsb.toString() + ") " +
                  "order by ft.datatabledef.formno,ft.datatabledef.formver desc"
            );
         } else {
            fttemp=(List)new ArrayList();
         }

         auth.setSessionAttribute("formhalist",fhatemp);
         auth.setSessionAttribute("formtypeslist",fttemp);
         auth.setSessionAttribute("iinfomap",iinfomap);

      }
      return fttemp;

   }

   // Create root hierarchy record if none exists (these are not created at database initialization)
   private void createHaRecord(int vis) throws FORMSException, FORMSSecurityException, FORMSKeyException {
      Studies s=(Studies)getMainDAO().execUniqueQuery(
         "select s from Studies s where studyid=" + getAuth().getValue("studyid")
         );
      if (s==null) return;   
      Formha newha=new Formha();
      newha.setVisibility(vis);
      newha.setStudies(s);
      newha.setHaordr(1);
      if (vis==VisibilityConstants.STUDY) {
         newha.setHadesc("Study-Level Forms");
      } else if (vis==VisibilityConstants.SITE) {
         newha.setHadesc("Site-Level Forms");
      } else if (vis==VisibilityConstants.SYSTEM) {
         newha.setHadesc("System-Level Forms");
      } else {
         return;
      }
      getMainDAO().saveOrUpdate(newha);
      return;

   }

   private void lockRecord(Datatablerecords rec) throws FORMSException,FORMSKeyException,FORMSSecurityException {      
      rec.setIslocked(true);
      rec.setLocktime(new Date(System.currentTimeMillis()));
      rec.setLockuser(new Long(getAuth().getValue("auserid")));
      rec.setLockinfo((getAuth().getValue("userdesc")));
      getMainDAO().saveOrUpdate(rec);
   }         

   private void unlockRecord(Datatablerecords rec) throws FORMSException,FORMSKeyException,FORMSSecurityException {      
      rec.setIslocked(false);
      rec.setLocktime(null);
      rec.setLockuser(null);
      rec.setLockinfo(null);
      getMainDAO().saveOrUpdate(rec);
   }         

   private String getUserName(long auserid) {

      try {
         if (namemap==null) {
            namemap=new PatientFormsSortMap();
            // create name map
            List ulist=getMainDAO().execQuery("select a from Allusers a");
            Iterator i=ulist.iterator();
            while (i.hasNext()) {
               Allusers user=(Allusers)i.next();
               namemap.put(new Long(user.getAuserid()),user.getUserdesc().replaceFirst("\\[.*$",""));
            }   
         }
         return (String)namemap.get(new Long(auserid));
      } catch (Exception e) {
         return new Long(auserid).toString();
      }

   }

}




