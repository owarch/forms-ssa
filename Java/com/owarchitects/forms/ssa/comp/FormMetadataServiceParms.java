 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 * FormMetadataServiceParms.java - input parameters for SSA forms.services passing a single object
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import java.util.*;
import java.io.*;

public class FormMetadataServiceParms extends FORMSSsaServiceParms implements Serializable {

   private List list;
   private Date lastupdate;

   public FormMetadataServiceParms() { }

   public FormMetadataServiceParms(String beanname,String methodname) {
      setBeanname(beanname);
      setMethodname(methodname);
   }
   
   //
   //

   public void setLastupdate(Date lastupdate) {
      this.lastupdate=lastupdate;
   }

   public Date getLastupdate() {
      return lastupdate;
   }
   
   //
   //

   public void setList(List list) {
      this.list=list;
   }

   public List getList() {
      return list;
   }
   
   //
   //

}

