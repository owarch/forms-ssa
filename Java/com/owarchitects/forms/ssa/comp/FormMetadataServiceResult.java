 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 * FormMetadataServiceResult.java - Output parameters for FORMSSERVICE controllers returning only status and a list field - MRH 03/2007
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import java.util.*;
import java.io.*;

public class FormMetadataServiceResult extends FORMSServiceResult implements Serializable {

   public List dtdefidlist;
   public List datatabledeflist;
   public List datatablecoldeflist;
   public List formtypesidlist;
   public List formtypeslist;
   public List formstorelist;
   public List formtypelistidlist;
   public List formtypelistlist;
   public List formuserpermlist;
   public List formtypeuserpermlist;
   public List userinfolist;

   //

   public void setDtdefidlist(List dtdefidlist) {
      this.dtdefidlist=dtdefidlist;
   }

   public List getDtdefidlist() {
      return dtdefidlist;
   }

   //

   public void setDatatabledeflist(List datatabledeflist) {
      this.datatabledeflist=datatabledeflist;
   }

   public List getDatatabledeflist() {
      return datatabledeflist;
   }

   //

   public void setDatatablecoldeflist(List datatablecoldeflist) {
      this.datatablecoldeflist=datatablecoldeflist;
   }

   public List getDatatablecoldeflist() {
      return datatablecoldeflist;
   }

   //

   public void setFormtypesidlist(List formtypesidlist) {
      this.formtypesidlist=formtypesidlist;
   }

   public List getFormtypesidlist() {
      return formtypesidlist;
   }

   //

   public void setFormtypeslist(List formtypeslist) {
      this.formtypeslist=formtypeslist;
   }

   public List getFormtypeslist() {
      return formtypeslist;
   }

   //

   public void setFormstorelist(List formstorelist) {
      this.formstorelist=formstorelist;
   }

   public List getFormstorelist() {
      return formstorelist;
   }

   //

   public void setFormtypelistidlist(List formtypelistidlist) {
      this.formtypelistidlist=formtypelistidlist;
   }

   public List getFormtypelistidlist() {
      return formtypelistidlist;
   }

   //

   public void setFormtypelistlist(List formtypelistlist) {
      this.formtypelistlist=formtypelistlist;
   }

   public List getFormtypelistlist() {
      return formtypelistlist;
   }

   //

   public void setFormuserpermlist(List formuserpermlist) {
      this.formuserpermlist=formuserpermlist;
   }

   public List getFormuserpermlist() {
      return formuserpermlist;
   }

   //

   public void setFormtypeuserpermlist(List formtypeuserpermlist) {
      this.formtypeuserpermlist=formtypeuserpermlist;
   }

   public List getFormtypeuserpermlist() {
      return formtypeuserpermlist;
   }

   //

   public void setUserinfolist(List userinfolist) {
      this.userinfolist=userinfolist;
   }

   public List getUserinfolist() {
      return userinfolist;
   }

   //

}

