 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * SelectReportController.java - Form Selection Interface
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.mla.html.table.*;

public class SelectReportController extends FORMSSsaServiceClientController {

   public ModelAndView submitRequest() throws FORMSException {

      try {

         FORMSAuth auth=this.getAuth();

         // use classname-based view
         mav=new ModelAndView("SelectReport");

         HttpServletResponse response=this.getResponse();
         PrintWriter out=this.getWriter();

         // Initialize Options (If necessary)
         //auth.initOption("typeopt","ALL");
         //auth.initOption("veropt","REC");
         //auth.initOption("archopt","Y");
         //auth.initOption("fnumopt","");
         auth.initOption("sinstopt","");

         // clear stored values
         auth.setValue("reportinfoid","");

         int resourceType;
         // 
         if (getRequest().getServletPath().indexOf("letter")>=0) {
            resourceType=ResourceTypeConstants.FORMLETTER;
            auth.setValue("resourceperm","LTR");
         } else {
            resourceType=ResourceTypeConstants.REPORT;
            auth.setValue("resourceperm","RPT");
         }
         auth.setValue("resourcetype",new Integer(resourceType).toString());
         auth.setValue("resourcetypestr",new ResourceTypeConstants().getFieldName(resourceType));

         SelectReportResult result=processData();
         //result.setTypeopt(auth.getOption("typeopt"));
         //result.setVeropt(auth.getOption("veropt"));
         //result.setFnumopt(auth.getOption("fnumopt"));
         result.setSinstopt(auth.getOption("sinstopt"));
         result.setRightslevel(auth.getOption("rightslevel"));
         result.setResourcetype(auth.getValue("resourcetypestr"));
         getSession().setAttribute("iframeresult",result);
         mav.addObject("status","OK");
         mav.addObject("resourcetype",auth.getValue("resourcetypestr"));
         return mav;
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Create root hierarchy record if none exists (these are not created at database initialization)
   private void createHaRecord(int vis,int resourcetype) throws FORMSException, FORMSSecurityException, FORMSKeyException {
      Studies s=(Studies)getMainDAO().execUniqueQuery(
         "select s from Studies s where studyid=" + getAuth().getValue("studyid")
         );
      if (s==null) return;   
      Reportha newha=new Reportha();
      newha.setVisibility(vis);
      newha.setResourcetype(resourcetype);
      newha.setStudies(s);
      newha.setHaordr(1);
      if (resourcetype==ResourceTypeConstants.FORMLETTER) {
         if (vis==VisibilityConstants.STUDY) {
            newha.setHadesc("Study-Level Form Letters");
         } else if (vis==VisibilityConstants.SITE) {
            newha.setHadesc("Site-Level Form Letters");
         } else if (vis==VisibilityConstants.SYSTEM) {
            newha.setHadesc("System-Level Form Letters");
         } else {
            return;
         }
      } else {
         if (vis==VisibilityConstants.STUDY) {
            newha.setHadesc("Study-Level Reporting Programs");
         } else if (vis==VisibilityConstants.SITE) {
            newha.setHadesc("Site-Level Reporting Programs");
         } else if (vis==VisibilityConstants.SYSTEM) {
            newha.setHadesc("System-Level Reporting Programs");
         } else {
            return;
         }
      } 
      getMainDAO().saveOrUpdate(newha);
      return;

   }

   //private SelectReportResult processData(boolean showarchivedforms) throws FORMSException {
   private SelectReportResult processData() throws FORMSException {

      //SelectReportResult result=new SelectReportResult();

      try {

        FORMSAuth auth=this.getAuth();
        String siteid=auth.getValue("siteid");
        String studyid=auth.getValue("studyid");

        // Use session-stored lists if available to avoid frequent hits to database for pulling form list
        List mhatemp=(List)auth.getSessionAttribute("reporthalist");
        List minfotemp=(List)auth.getSessionAttribute("reportinfolist");
        HashMap iinfotemp=(HashMap)auth.getSessionAttribute("minfomap");
        Iterator i;

        // If no/empty list available, pull from database
        if (mhatemp==null || minfotemp==null || mhatemp.size()<1 || minfotemp.size()<1) {
         
           // Retrive installation information for later
           List ilist=getMainDAO().execQuery(
              "select a from Allinst a join fetch a.linkedinst where a.islocal!=true"
              );
           i=ilist.iterator();
           HashMap minfomap=new HashMap();
           while (i.hasNext()) {
             Allinst ainst=(Allinst)i.next();
             minfomap.put(new Long(ainst.getAinstid()).toString(),ainst.getLinkedinst().getAcronym());
           }

           mhatemp=getMainDAO().execQuery(
              "select mha from Reportha mha " +
                    "where ((" +
                              "mha.visibility=" + VisibilityConstants.SYSTEM + 
                              ") or (" +
                              "mha.visibility=" + VisibilityConstants.SITE + " and mha.studies.sites.siteid=" + siteid + 
                              ") or (" +
                              "mha.visibility=" + VisibilityConstants.STUDY + " and mha.studies.studyid=" + studyid + 
                           ")) " +
                    "order by haordr"
              );
       
           // 1) Write reporthaid to StringBuilder for use in formtypes pull
           // 2) Verify that root system, site & study records exist.  Otherwise create them
           StringBuilder sb=new StringBuilder();
           // Check for all resourcetypes
           Iterator k=new ResourceTypeConstants().getFieldList().iterator();
           while (k.hasNext()) {

              HashMap kmap=(HashMap)k.next();
              int rtype=((Integer)kmap.get("fieldvalue")).intValue();

              boolean hassystem=false, hassite=false, hasstudy=false;
              i=mhatemp.iterator();
              while (i.hasNext()) {
                 Reportha ha=(Reportha)i.next();
                 // write id to stringbuffer
                 sb.append(new Long(ha.getReporthaid()).toString());
                 if (i.hasNext()) sb.append(",");
                 // check that necessary root hierarchy records exist, else create them
                 if (ha.getVisibility()==VisibilityConstants.SYSTEM && ha.getPreporthaid()==null && ha.getResourcetype()==rtype) {
                    hassystem=true;
                 } else if (ha.getVisibility()==VisibilityConstants.SITE && ha.getPreporthaid()==null && ha.getResourcetype()==rtype) {
                    hassite=true;
                 } else if (ha.getVisibility()==VisibilityConstants.STUDY && ha.getPreporthaid()==null && ha.getResourcetype()==rtype) {
                    hasstudy=true;
                 } 
              }

              if (!hassystem) createHaRecord(VisibilityConstants.SYSTEM,rtype);
              if (!hassite) createHaRecord(VisibilityConstants.SITE,rtype);
              if (!hasstudy) createHaRecord(VisibilityConstants.STUDY,rtype);

           }
     
     
           // Pull reportinfo data (Local files)
           if (sb.length()>0) {
              minfotemp=getMainDAO().execQuery(
                 "select minf from Reportinfo minf join fetch minf.allinst join fetch minf.supportedfileformats " +
                    "where minf.preporthaid in (" 
                       + sb.toString() + ")" +
                    "order by minf.description"
              );
           } else {
              minfotemp=(List)new ArrayList();
           }

           auth.setSessionAttribute("reporthalist",mhatemp);
           auth.setSessionAttribute("reportinfolist",minfotemp);
           auth.setSessionAttribute("minfomap",minfomap);

        };

        // Create new list based on stored list
        List mha=(List)new ArrayList(mhatemp);

        // add forms and installation info to final form list
        Iterator minfoi=minfotemp.iterator();
        List allreportlist=(List)new ArrayList();
        auth.setValue("showsinstopt","N");
        // Add forms to list
        while (minfoi.hasNext()) {
           Reportinfo minfo=(Reportinfo)minfoi.next();
           // drop if not current resourcetype
           if (minfo.getResourcetype()!=new Integer(auth.getValue("resourcetype")).intValue()) {
              continue;
           }
           HashMap minfoimap=new HashMap();
           minfoimap.put("linstid",null);
           minfoimap.put("lstdid",null);
           minfoimap.put("instacr","LOCAL");
           minfoimap.put("report",minfo);
           allreportlist.add(minfoimap);
        }

        ///////////////////////////////////////////////////////////
        // Remove report programs from list based on permissions //
        ///////////////////////////////////////////////////////////

        List fpermlist=getPermHelper().getReportPermList();

        // Refine list based on options
        HashMap checkver=new HashMap();
        String typeopt=auth.getOption("typeopt");
        i=allreportlist.iterator();
        while (i.hasNext()) {
           HashMap imap=(HashMap)i.next();
           Reportinfo minfo=(Reportinfo)imap.get("report");
           String instacr=(String)imap.get("instacr");
           boolean isremoved=false;
           // Check permissions - (based on report permission list)
           // note:  Upload user has automatic rights to file
           if (!(isremoved || minfo.getUploaduser()==new Long(auth.getValue("auserid")).longValue() ||
              auth.getValue("rightslevel").equalsIgnoreCase("ADMIN"))) {
              Long reportinfoid=new Long(minfo.getReportinfoid());
              Iterator piter=fpermlist.iterator();
              boolean found=false;
              while (piter.hasNext()) {
                 HashMap map=(HashMap)piter.next();
                 Long compid;
                 try {
                    compid=new Long((String)map.get("reportinfoid"));
                 } catch (ClassCastException cce) {   
                    compid=(Long)map.get("reportinfoid");
                 }   
                 if (compid.equals(reportinfoid)) {
                    found=true;
                    break;
                 }
              }
              if (!found) {
                 i.remove();
                 isremoved=true;
              }
           }
        }

        ////////////////////////////////////////////////////////////////////////////
        // Pull remote form information (permissions determined by remote server) //
        ////////////////////////////////////////////////////////////////////////////

        List minforemote=getRemoteFiles();

        if (minforemote!=null && minforemote.size()>0) {
           auth.setValue("showsinstopt","Y");
           // add remote files to allreportlist
           Iterator riter=minforemote.iterator();
           while (riter.hasNext()) {
             HashMap map=(HashMap)riter.next();
             Linkedinst inst=(Linkedinst)map.get("inst");
             Linkedstudies lstd=(Linkedstudies)getMainDAO().execUniqueQuery(
                "select s from Linkedstudies s where s.linkedinst.linstid=" + inst.getLinstid() + 
                   " and s.studies.studyid=" + getAuth().getValue("studyid")
                );
             List rcatlist=getMainDAO().execQuery(
                "select c from Remotereportcat c where c.lstdid=" + lstd.getLstdid()
                );
             SsaServiceListResult sslr=(SsaServiceListResult)map.get("result");
             List flist=sslr.getList();
             if (flist==null) continue;
             Iterator fiter=flist.iterator();
             while (fiter.hasNext()) {
                // create hashmap with minfo and inst info and add to allreportlist
                Reportinfo minfo=(Reportinfo)fiter.next();
                // drop if not current resourcetype
                if (minfo.getResourcetype()!=new Integer(auth.getValue("resourcetype")).intValue()) {
                   continue;
                }
                // update preporthaid with one from remotecat
                Iterator ci=rcatlist.iterator();
                while (ci.hasNext()) {
                   Remotereportcat rcat=(Remotereportcat)ci.next();
                   long lid=minfo.getReportinfoid();
                   long rid=rcat.getRreportinfoid();
                   if (lid==rid) {
                      minfo.setPreporthaid(rcat.getPreporthaid());
                   }
                }
                HashMap minfoimap=new HashMap();
                minfoimap.put("linstid",inst.getLinstid());
                minfoimap.put("lstdid",lstd.getLstdid());
                minfoimap.put("instacr",inst.getAcronym());
                minfoimap.put("report",minfo);
                allreportlist.add(minfoimap);
             }
           }
        } else {
           auth.setValue("showsinstopt","N");
           // NO REMOTE FILES
        }


        ////////////////////////////////////////////////////////////////////
        // Organize data into heirarchical format for building HTML table //
        ////////////////////////////////////////////////////////////////////

        // minfo holds number of rows in table.  Keep this info for later
        int tablerows=allreportlist.size();

        // tlist holds table columns
        ArrayList tlist=new ArrayList();
        // alist holds items within columns
        ArrayList alist=new ArrayList();

        // pull first table column
        i=mha.iterator();
        while (i.hasNext()) {
           Reportha ha=(Reportha)i.next();
           if (ha.getPreporthaid()==null) {
              HashMap map=new HashMap();
              map.put("isnull","N");
              map.put("obj",ha);
              map.put("nrows",0);
              map.put("ncols",1);
              // remove element from collection so we don't have to compare it again
              i.remove();
              alist.add(map);
           }
        }   
        tlist.add(alist);
        // pull subsequent columns by finding children of elements in previous columns
        for (int ii=1;ii>-1;ii++) {
           // pull elements in prior column
           ArrayList plist=(ArrayList)tlist.get(ii-1);
           alist=new ArrayList();
           Iterator i2=plist.iterator();
           boolean again=false;
           //iterate through prior column elements
           while (i2.hasNext()) {
              // find children
              HashMap pmap=(HashMap)i2.next();
              Reportha pha=(Reportha)pmap.get("obj");
              i=mha.iterator();
              boolean havechild=false;
              int prevsize=alist.size();
              while (i.hasNext()) {
                 Reportha cha=(Reportha)i.next();
                 // write child out to alist
                 if (cha.getPreporthaid().equals(pha.getReporthaid())) {
                    HashMap cmap=new HashMap();
                    cmap.put("isnull","N");
                    cmap.put("obj",cha);
                    cmap.put("nrows",0);
                    cmap.put("ncols",1);
                    // remove element from collection so we don't have to compare it again
                    i.remove();
                    alist.add(cmap);
                    again=true;
                    havechild=true;
                 }
                 
              }
              // create null row to hold records belonging to item when child
              // categories are present
              if (havechild) {
                 HashMap nmap=new HashMap();
                 Reportha nha=new Reportha();
                 nha.setResourcetype(new Integer(auth.getValue("resourcetype")).intValue());
                 nha.setReporthaid(pha.getReporthaid());
                 nha.setPreporthaid(pha.getPreporthaid());
                 nha.setHadesc("&nbsp;");
                 nmap.put("isnull","Y");
                 nmap.put("obj",nha);
                 nmap.put("nrows",0);
                 nmap.put("ncols",1);
                 alist.add(prevsize,nmap);
              }
           }
           if (!again) break;
           tlist.add(alist);
        }
        // add forms to appropriate category (must iterate backwards through tlist
        // since forms are attached as the right most element)
        for (int ii=(tlist.size()-1);ii>=0;ii--) {
           // pull column arraylist
           ArrayList plist=(ArrayList)tlist.get(ii);
           Iterator i2=plist.iterator();
           while (i2.hasNext()) {
              HashMap pmap=(HashMap) i2.next();
              Reportha pha=(Reportha)pmap.get("obj");
              // reportlist holds forms belonging to an element
              ArrayList reportlist=new ArrayList();
              Iterator i3=allreportlist.iterator();
              while (i3.hasNext()) {
                 HashMap i3map=(HashMap)i3.next();
                 Reportinfo cinfo=(Reportinfo)i3map.get("report");
                 Long linstid=(Long)i3map.get("linstid");
                 Long lstdid=(Long)i3map.get("lstdid");
                 String instacr=(String)i3map.get("instacr");
                 // add form to reportlist where there is a match
                 if (cinfo.getPreporthaid()==pha.getReporthaid()) {
                    HashMap flmap=new HashMap();
                    flmap.put("report",cinfo);
                    flmap.put("linstid",linstid);
                    flmap.put("lstdid",lstdid);
                    flmap.put("instacr",instacr);
                    reportlist.add(flmap);
                    // remove element from collection so we don't have to compare it again
                    i3.remove();
                 }
              }
              // add reportlist to element's hashmap
              if (reportlist.size()>0) {
                 pmap.put("reportlist",reportlist);
                 pmap.put("nrows",reportlist.size());
                 pmap.put("ncols",tlist.size()-ii);
              }
              // add nrows to parent cells from prior columns
              Long compid=null;
              try {
                 compid=new Long(pha.getPreporthaid());
              } catch (Exception cie) { }
              // loop through prior columns looking for parent elements
              for (int jj=(ii-1);jj>=0;jj--) {
                 ArrayList pplist=(ArrayList)tlist.get(jj);
                 Iterator i4=pplist.iterator();
                 while (i4.hasNext()) {
                    HashMap ppmap=(HashMap) i4.next();
                    Reportha ppha=(Reportha)ppmap.get("obj");
                    String isnull=(String)ppmap.get("isnull");
                    // if find a parent element, add rows to that element.  Then update
                    // compid to that element's parent for next loop
                    if ((
                           (!(isnull.equals("Y") || compid==null)) && ppha.getReporthaid()==compid.longValue()
                        ) ||
                        (
                           ppha.getReporthaid()==pha.getReporthaid() && ppha.getPreporthaid()==pha.getPreporthaid()
                       )) {
                       int nrows=((Integer)ppmap.get("nrows")).intValue();
                       nrows=nrows+reportlist.size();
                       ppmap.put("nrows",nrows);
                       try {
                          compid=new Long(ppha.getPreporthaid());
                       } catch (Exception cie2) {}
                    }
                 }
              }
           }
        }
        
        /////////////////////////////////
        // Create HTML Table of result //
        /////////////////////////////////

        int tablecols=tlist.size()+1;
        if (tablerows<1 || tablecols<1) {
           throw new FORMSException("Zero length table");
        }
        Table table=new Table(tablerows,tablecols);
        // iterate through table columns;
        for (int currcol=0;currcol<tlist.size();currcol++) {
           ArrayList rowlist=(ArrayList)tlist.get(currcol);
           int prevrow=0;
           Iterator rowiter=rowlist.iterator();
           while (rowiter.hasNext()) {
              HashMap map2=(HashMap)rowiter.next();
              Reportha ha2=(Reportha)map2.get("obj");
              int nrows=((Integer)map2.get("nrows")).intValue();
              int ncols=((Integer)map2.get("ncols")).intValue();
              if (nrows<=0 || ncols<=0) continue;
              Cell cell=new Cell(new Long(ha2.getReporthaid()).toString(),nrows,ncols);
              cell.setContent("type","reportha");
              cell.setContent("obj",ha2);
              boolean okvar=false;
              int currrow=prevrow;
              // Use previous row & exceptions to calculate cell row placement.  This could probably
              // be made more efficient by running through a calculation loop, but the loss here 
              // is quite minimal
              int firstrow=0;
              while (!okvar) {
                 try {
                    table.setCell(cell,currrow,currcol);
                    firstrow=currrow;
                    prevrow=currrow+1;
                    okvar=true;
                 } catch (Exception te) { 
                    currrow++;
                    if (currrow>500000) throw new FORMSException("Too many rows returned");
                 }
              }   
              // see if contains any forms (final column)
              ArrayList reportlist=(ArrayList)map2.get("reportlist");
              if (reportlist!=null) {
                 // Rows for items must be placed based on row location of containing element
                 // (first row number of containing element is held in "firstrow")
                 int prevrow2=firstrow;
                 Iterator formiter=reportlist.iterator();
                 while (formiter.hasNext()) {
                    HashMap minfomap=(HashMap)formiter.next();
                    Reportinfo minfo=(Reportinfo)minfomap.get("report");
                    Long linstid=(Long)minfomap.get("linstid");
                    Long lstdid=(Long)minfomap.get("lstdid");
                    String instacr=(String)minfomap.get("instacr");
                    cell=new Cell(new Long(minfo.getReportinfoid()).toString(),1,1);
                    cell.setContent("type","minfo");
                    cell.setContent("obj",minfo);
                    cell.setContent("instacr",instacr);
                    if (linstid==null) {
                       cell.setContent("linstid",-99999999);
                       cell.setContent("lstdid",-99999999);
                       cell.setContent("islocal",true);

                    } else {
                       cell.setContent("linstid",linstid);
                       cell.setContent("lstdid",lstdid);
                       cell.setContent("islocal",false);
                    }
                    okvar=false;
                    currrow=prevrow2;
                    while (!okvar) {
                       try {
                          table.setCell(cell,currrow,tablecols-1);
                          prevrow2=currrow+1;
                          okvar=true;
                       } catch (Exception te) {
                          currrow++;
                          if (currrow>500000) throw new FORMSException("Too many rows returned");
                       }
                    }
                 }
              }
           }
        }
        // Create Options Menu Map
        HashMap optionsmap=new HashMap();
        //optionsmap.put("typelist",typelist);
        //optionsmap.put("typeopt",auth.getOption("typeopt"));
        //optionsmap.put("veropt",auth.getOption("veropt"));
        //optionsmap.put("archopt",auth.getOption("archopt"));
        //optionsmap.put("fnumopt",auth.getOption("fnumopt"));
        optionsmap.put("sinstopt",auth.getOption("sinstopt"));
        auth.setSessionAttribute("optionsobject",optionsmap);

        // Create & return SelectReportResult Object
        SelectReportResult result=new SelectReportResult();

        result.setTable(table);

        return result;

      } catch (Exception e) {

        SelectReportResult result=new SelectReportResult();
        return result;

      }

   }

    // Retrieve list of remote report files (search preference - auth (sessionattribute), service)
    private List getRemoteFiles() {
       try {
          String studyid=getAuth().getValue("studyid");
          List rfilelist;
          try {
             rfilelist=(List)getAuth().getSessionAttribute("remotereportsessionlist");
          } catch (Exception rfe) {
             rfilelist=null;
          }
          // See if list has been pulled for study
          if (rfilelist!=null) {
             Iterator i=rfilelist.iterator();
             while (i.hasNext()) {
                HashMap map=(HashMap)i.next();
                if (studyid.equals((String)map.get("studyid"))) {
                   List srlist=(List)map.get("remotefilelist");
                   if (srlist!=null) {
                      initHeirarchy(srlist);
                      return srlist;
                   }
                }
   
             }
          } else {
             rfilelist=(List)new ArrayList();
          }
          SsaServiceSystemStudyParms sparms=new SsaServiceSystemStudyParms();
          sparms.setBeanname("getReportServiceTarget");
          sparms.setMethodname("getReportinfo");
          List srlist=(List)submitServiceStudyRequest(sparms);
          HashMap sfmap=new HashMap();
          sfmap.put("studyid",studyid);
          sfmap.put("remotefilelist",srlist);
          rfilelist.add(sfmap);
          getAuth().setSessionAttribute("remotereportsessionlist",rfilelist);
          initHeirarchy(srlist);
          return srlist;
       } catch (Exception e) {

          // proceed without remote files
          return null;
       }
    }

    // This function verifies that remote files have a heirarchy entry in Remotereportcat
    // If not, entries are created
    private void initHeirarchy(List l) throws FORMSException,FORMSKeyException,FORMSSecurityException {
       FORMSAuth auth=getAuth();
       Iterator i=l.iterator();
       Long preporthaid=null;
       while (i.hasNext()) {
          HashMap map=(HashMap)i.next();
          Linkedinst inst=(Linkedinst)map.get("inst");
          Linkedstudies lstd=(Linkedstudies)getMainDAO().execUniqueQuery(
             "select s from Linkedstudies s where s.linkedinst.linstid=" + inst.getLinstid() + 
                " and s.studies.studyid=" + auth.getValue("studyid")
             );
          SsaServiceListResult sslr=(SsaServiceListResult)map.get("result");
          List flist=sslr.getList();
          if (flist==null) continue;
          Iterator fiter=flist.iterator();
          List rcatlist=getMainDAO().execQuery(
                "select r from Remotereportcat r where r.lstdid=" + lstd.getLstdid()
                );
          List fdidalready=(List)new ArrayList();
          while (fiter.hasNext()) {
             Reportinfo minfo=(Reportinfo)fiter.next();
             // only run first instance of reportinfoid
             if (fdidalready.contains(new Long(minfo.getReportinfoid()))) {
                continue;
             }
             fdidalready.add(new Long(minfo.getReportinfoid()));
             Iterator rcatiter=rcatlist.iterator();
             Remotereportcat rcat=null;
             while (rcatiter.hasNext()) {
                Remotereportcat ccat=(Remotereportcat)rcatiter.next();
                if (ccat.getRreportinfoid()==minfo.getReportinfoid()) {
                   rcat=ccat;
                   rcatiter.remove();
                }
             }
             // if no current record, create new one
             if (rcat==null && minfo.getResourcetype()==(new Integer(auth.getValue("resourcetype")).intValue())) {
                Remotereportcat newcat=new Remotereportcat();
                newcat.setLstdid(lstd.getLstdid());
                newcat.setRreportinfoid(minfo.getReportinfoid());
                // get preporthaid value if necessary
                if (preporthaid==null) {
                   Reportha ha=(Reportha)getMainDAO().execUniqueQuery(
                      "select f from Reportha f where f.preporthaid is null and " +
                         "f.resourcetype=" + auth.getValue("resourcetype") + " and " +
                         "visibility=" + VisibilityConstants.STUDY + " and " +
                         "f.studies.studyid=" + auth.getValue("studyid")
                      );
                   if (ha==null) {
                      throw new FORMSException("Could not pull study Reportha record");
                   }
                   preporthaid=ha.getReporthaid();
                }
                newcat.setPreporthaid(preporthaid.longValue());
                getMainDAO().saveOrUpdate(newcat);
             }
          }
          
       }
    }


}

