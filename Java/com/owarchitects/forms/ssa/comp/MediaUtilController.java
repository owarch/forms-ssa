 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * MediaUtilController.java - Form utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.mla.html.table.*;

public class MediaUtilController extends FORMSSsaServiceClientController {
 
   String spath="";

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // check permissions
      //if (!getAuth().getValue("rightslevel").equalsIgnoreCase("ADMIN")) {
      //   safeRedirect("permission.err");
      //   return null;
      //}

      // Use class-name based view
      mav=new ModelAndView("MediaUtil");

      try {

         HttpServletRequest request=this.getRequest();
         spath=request.getServletPath();

         // addstudy.sutil  (add new study interface screen)
         if (spath.indexOf("removecat.")>=0) {

            mav.addObject("status",removeCat());
            return mav;

         }
         else if (spath.indexOf("movecat.")>=0) {
          
            return moveCat(mav);

         }
         else if (spath.indexOf("movecatsubmit.")>=0) {
          
            mav.addObject("status",moveCatSubmit());
            return mav;

         }
         else if (spath.indexOf("viewallcat.")>=0) {
          
            return viewAllCat(mav);

         }
         else if (spath.indexOf("viewallsubmit.")>=0) {
          
            safeRedirect("mediaeditcat.am");
            return null;

         }
         else if (spath.indexOf("editcat.")>=0) {

            Mediaha ha=editCat();
            if (ha!=null) {
               // check permissions 
               if (!getPermHelper().hasGlobalPerm(ha.getVisibility(),"MEDCAT")) {
                  mav.addObject("status","NOAUTH");
                  return mav;
               }
               // continue
               mav.addObject("status","EDITCAT");
               mav.addObject("mediaha",editCat());
               return mav;
            }
            return null;

         }
         else if (spath.indexOf("editcatsubmit.")>=0) {

            mav.addObject("status",editCatSubmit());
            return mav;

         // Delete specified profile 
         } else if (spath.indexOf("deletefile.")>=0) {

            mav.addObject("status",deleteFile());
            return mav;

         }
         else if (spath.indexOf("showinfo.")>=0) {

            return showInfo();

         }
         else if (spath.indexOf("editattr.")>=0) {

            return editAttr();

         }
         else if (spath.indexOf("editattrsubmit.")>=0) {

            mav.addObject("status",editAttrSubmit());
            return mav;

         }
         else if (spath.indexOf("addcat.")>=0) {

            Mediaha ha=addCat();
            if (ha!=null) {
               // check permissions 
               if (!getPermHelper().hasGlobalPerm(ha.getVisibility(),"MEDCAT")) {
                  mav.addObject("status","NOAUTH");
                  return mav;
               }
               // continue
               mav.addObject("status","ADDCAT");
               mav.addObject("mediaha",addCat());
               return mav;
            }
            return null;

         }
         else if (spath.indexOf("addcatsubmit.")>=0) {

            mav.addObject("status",addCatSubmit());
            return mav;

         // Re-order categories interface
         } else if (spath.indexOf("reordercat.")>=0) {

            List l=reorderCat();
            mav.addObject("list",l);
            mav.addObject("status","REORDERCAT");
            return mav;

         // Submit re-ordering of categories
         } else if (spath.indexOf("reordercatsubmit.")>=0) {

            mav.addObject("status",reorderCatSubmit());
            return mav;

         } else if (spath.indexOf("listprofiles.")>=0) {

            return listProfiles(mav);

         // Add new profile interface (PROFILE LEVEL SELECTION)
         } else if (spath.indexOf("newprofile.")>=0) {

            mav.addObject("status","NEWPROFILE");
            mav.addObject("list",new VisibilityConstants().getFieldList());
            return mav;

         // Add new profile interface (ENTER PROFILE ATTRIBUTES)
         } else if (spath.indexOf("addprofile.")>=0) {

            int profilelevel=new Integer(getRequest().getParameter("profilelevel")).intValue();

            // See if user is permitted to modify profile
            if (!getPermHelper().hasGlobalPerm(getAuth().getValue("auserid"),profilelevel,"MEDPERMPROF")) {
               // not permitted
               mav.addObject("status","NOAUTH");
               return mav;
            }

            mav.addObject("status","ADDPROFILE");
            mav.addObject("profilelevel",new VisibilityConstants().getFieldName(profilelevel));
            return mav;

         // Submit new profile to system
         } else if (spath.indexOf("addprofilesubmit.")>=0) {

            String status=addProfileSubmit();
            mav.addObject("status",status);
            return mav;

         // Modify profile interface
         } 
         else if (spath.indexOf("profilemenu.")>=0) {

            mav.addObject("status","PROFILEMENU");
            mav.addObject("mprofileid",getRequest().getParameter("mprofileid"));
            return mav;

         } 
         else if (spath.indexOf("modifyprofile.")>=0) {

            mav.addObject("status",modifyProfile());
            return mav;

         // Submit modified profile
         } else if (spath.indexOf("modifyprofilesubmit.")>=0) {

            mav.addObject("status",modifyProfileSubmit());
            return mav;

         // Delete specified profile 
         } else if (spath.indexOf("removeprofilesubmit.")>=0) {

            mav.addObject("status",removeProfileSubmit());
            return mav;

         }
         // add new site interface
         else {
            out.println("BAD REQUEST! - " + spath);
            return null;
         }
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Empty session-stored media lists so data are repulled
   private void clearSessionLists() {   
      try {
         getAuth().setSessionAttribute("mediahalist",new ArrayList());
         getAuth().setSessionAttribute("mediainfolist",new ArrayList());
      } catch (Exception e) { }
   }   

   private Mediaha editCat() {
      String mediahaid=getRequest().getParameter("mediahaid");
      Mediaha ha=(Mediaha)getMainDAO().execUniqueQuery(
         "select ha from Mediaha ha where mediahaid=" + mediahaid
         );
      return ha;   
   }

   private String editCatSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      // Check permission

      String mediahaid=getRequest().getParameter("mediahaid");
      String hadesc=getRequest().getParameter("hadesc");
      Mediaha ha=(Mediaha)getMainDAO().execUniqueQuery(
         "select ha from Mediaha ha where mediahaid=" + mediahaid
         );

      if (!getPermHelper().hasGlobalPerm(ha.getVisibility(),"MEDCAT")) {
         return "NOAUTH";
      }

      // Make sure description doesn't already exist in category   
      List l=getMainDAO().execQuery(
         "select ha from Mediaha ha where ha.pmediahaid=" + mediahaid + " and upper(ha.hadesc)=upper('" + hadesc + "')" 
         );
      if (l.size()>0) {
         return "CATNOTUNIQUE";
      }
      ha.setHadesc(hadesc);   
      getMainDAO().saveOrUpdate(ha);
      clearSessionLists();
      return "CATEDITED";
   }

   private Mediaha addCat() {
      String mediahaid=getRequest().getParameter("mediahaid");
      Mediaha ha=(Mediaha)getMainDAO().execUniqueQuery(
         "select ha from Mediaha ha where mediahaid=" + mediahaid
         );
      return ha;   
   }

   private String addCatSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      String mediahaid=getRequest().getParameter("mediahaid");
      String hadesc=getRequest().getParameter("hadesc");

      Mediaha cha=(Mediaha)getMainDAO().execUniqueQuery(
         "select ha from Mediaha ha where mediahaid=" + mediahaid
         );
      if (!getPermHelper().hasGlobalPerm(cha.getVisibility(),"MEDCAT")) {
         return "NOAUTH";
      }

      Studies currstudy=(Studies)getMainDAO().execUniqueQuery(
         "select std from Studies std where std.studyid=" + getAuth().getValue("studyid")
         );
      // Make sure description doesn't already exist in category   
      List l=getMainDAO().execQuery(
         "select ha from Mediaha ha where ha.pmediahaid=" + mediahaid + " and upper(ha.hadesc)=upper('" + hadesc + "')" 
         );
      if (l.size()>0) {
         return "CATNOTUNIQUE";
      }
      Integer haordr=(Integer)getMainDAO().execUniqueQuery(
         "select max(ha.haordr) from Mediaha ha where ha.pmediahaid=" + mediahaid
         );
      Integer visibility=(Integer)getMainDAO().execUniqueQuery(
         "select ha.visibility from Mediaha ha where ha.mediahaid=" + mediahaid
         );
      if (haordr==null) haordr=1;
      else haordr++;
      Mediaha ha=new Mediaha();
      ha.setPmediahaid(new Long(mediahaid));
      ha.setHadesc(hadesc);   
      ha.setHaordr(haordr);   
      ha.setVisibility(visibility);   
      ha.setStudies(currstudy);   
      getMainDAO().saveOrUpdate(ha);
      clearSessionLists();
      return "CATADDED";
   }

   private ModelAndView moveCat(ModelAndView mav) {
      return moveCat(mav,false);
   }

   private ModelAndView moveCat(ModelAndView mav,boolean iscatrequest) {

      try {

        Mediaha ftpfha=null;
        Object moveobj=null;

        if (!iscatrequest) {
           if (getAuth().getValue("islocal").toUpperCase().equals("TRUE")) {
   
              // pull current mediainfo record
              Mediainfo ft=(Mediainfo)getMainDAO().execUniqueQuery(
                 "select ft from Mediainfo ft " +
                    "where ft.mediainfoid=" + request.getParameter("mediainfoid")
                 );   
              moveobj=ft;   
        
              if (ft==null) throw new FORMSException("Couldn't pull document/media record");   
       
              // pull record's mediaha parent
              ftpfha=(Mediaha)getMainDAO().execUniqueQuery(
                 "select ha from Mediaha ha where ha.mediahaid=" + new Long(ft.getPmediahaid()).toString()
                 );
   
              // check permissions for move
              if (!ft.getUploaduser().equals(new Long(getAuth().getValue("auserid")))) {
                 if (!getPermHelper().hasGlobalPerm(ftpfha.getVisibility(),"MEDMOVEOTH")) {
                    mav.addObject("status","NOAUTH");
                    return mav;
                 }
              }
   
           } else {
   
              // pull current mediainfo record
              Remotemediacat ft=(Remotemediacat)getMainDAO().execUniqueQuery(
                 "select ft from Remotemediacat ft " +
                    "where ft.lstdid=" + getAuth().getValue("lstdid") + " and ft.rmediainfoid=" + 
                       request.getParameter("mediainfoid")
                 );   
              moveobj=ft;   
        
              if (ft==null) throw new FORMSException("Couldn't pull document/media record");   
       
              // pull record's mediaha parent
              ftpfha=(Mediaha)getMainDAO().execUniqueQuery(
                 "select ha from Mediaha ha where ha.mediahaid=" + new Long(ft.getPmediahaid()).toString()
                 );
   
              // check permissions for move
              if (!getPermHelper().hasGlobalPerm(ftpfha.getVisibility(),"MEDMOVEOTH")) {
                 mav.addObject("status","NOAUTH");
                 return mav;
              }
   
           }
        } else {
           // pull mediaha record
           ftpfha=(Mediaha)getMainDAO().execUniqueQuery(
              "select ha from Mediaha ha where ha.mediahaid=" + request.getParameter("mediahaid")
              );
        }
           
        int ftpfhavis=ftpfha.getVisibility();   

        FORMSAuth auth=this.getAuth();
        String siteid=auth.getValue("siteid");
        String studyid=auth.getValue("studyid");

        List fha=getMainDAO().execQuery(
           "select fha from Mediaha fha " +
                 "where (fha.visibility=" + new Integer(ftpfhavis).toString() + " and " +
                        "((" +
                           "fha.visibility=" + VisibilityConstants.SYSTEM + 
                           ") or (" +
                           "fha.visibility=" + VisibilityConstants.SITE + " and fha.studies.sites.siteid=" + siteid + 
                           ") or (" +
                           "fha.visibility=" + VisibilityConstants.STUDY + " and fha.studies.studyid=" + studyid + 
                        "))) " +
                 "order by haordr"
           );

        Iterator i=fha.iterator();
        StringBuilder sb=new StringBuilder();
        while (i.hasNext()) {
           Mediaha ha=(Mediaha)i.next();
           sb.append(new Long(ha.getMediahaid()).toString());
           if (i.hasNext()) sb.append(",");
        }

        ////////////////////////////////////////////////////////////////////
        // Organize data into heirarchical format for building HTML table //
        ////////////////////////////////////////////////////////////////////

        // ft holds number of rows in table.  Keep this info for later
        //int tablerows=ft.size();
        int tablerows=0;

        // tlist holds table columns
        ArrayList tlist=new ArrayList();
        // alist holds items within columns
        ArrayList alist=new ArrayList();

        // pull first table column
        i=fha.iterator();
        while (i.hasNext()) {
           Mediaha ha=(Mediaha)i.next();
           if (ha.getPmediahaid()==null) {
              HashMap map=new HashMap();
              map.put("obj",ha);
              map.put("nrows",0);
              map.put("ncols",1);
              // remove element from collection so we don't have to compare it again
              i.remove();
              alist.add(map);
           }
        }   
        tlist.add(alist);
        // pull subsequent columns by finding children of elements in previous columns
        for (int ii=1;ii>-1;ii++) {
           // pull elements in prior column
           ArrayList plist=(ArrayList)tlist.get(ii-1);
           alist=new ArrayList();
           Iterator i2=plist.iterator();
           boolean again=false;
           //iterate through prior column elements
           while (i2.hasNext()) {
              // find children
              HashMap pmap=(HashMap)i2.next();
              Mediaha pha=(Mediaha)pmap.get("obj");
              i=fha.iterator();
              boolean havechild=false;
              int prevsize=alist.size();
              while (i.hasNext()) {
                 Mediaha cha=(Mediaha)i.next();
                 // write child out to alist
                 if (cha.getPmediahaid().equals(pha.getMediahaid())) {
                    HashMap cmap=new HashMap();
                    cmap.put("obj",cha);
                    cmap.put("nrows",0);
                    cmap.put("ncols",1);
                    // remove element from collection so we don't have to compare it again
                    i.remove();
                    alist.add(cmap);
                    again=true;
                    havechild=true;
                 }
                 
              }
              // HA-ONLY Specific Code
              if (!havechild) {
                 tablerows++;
                 pmap.put("nrows",1);
                 pmap.put("havechild","N");
              } else {
                 pmap.put("havechild","Y");
              }
           }
           if (!again) break;
           tlist.add(alist);
        }

        // add forms to appropriate category (must iterate backwards through tlist
        // since forms are attached as the right most element)
        for (int ii=(tlist.size()-1);ii>=0;ii--) {
           // pull column arraylist
           ArrayList plist=(ArrayList)tlist.get(ii);
           Iterator i2=plist.iterator();
           while (i2.hasNext()) {
              HashMap pmap=(HashMap) i2.next();
              Mediaha pha=(Mediaha)pmap.get("obj");
              // formlist holds forms belonging to an element
              ArrayList medialist=new ArrayList();
              Long compid=null;
              try {
                 compid=new Long(pha.getPmediahaid());
              } catch (Exception cie) { }
              // loop through prior columns looking for parent elements
              int addrows=1;
              if (pmap.get("havechild").equals("N")){ 
                 pmap.put("ncols",tlist.size()-ii);
                 for (int jj=(ii-1);jj>=0;jj--) {
                    ArrayList pplist=(ArrayList)tlist.get(jj);
                    Iterator i4=pplist.iterator();
                    while (i4.hasNext()) {
                       HashMap ppmap=(HashMap) i4.next();
                       Mediaha ppha=(Mediaha)ppmap.get("obj");
                       // if find a parent element, add rows to that element.  Then update
                       // compid to that element's parent for next loop
                       if ((
                              (!(compid==null)) && ppha.getMediahaid()==compid.longValue()
                           ) ||
                           (
                              ppha.getMediahaid()==pha.getMediahaid() && ppha.getPmediahaid()==pha.getPmediahaid()
                          )) {
                          int nrows=((Integer)ppmap.get("nrows")).intValue();
                          nrows=nrows+addrows;
                          ppmap.put("nrows",nrows);
                          try {
                             compid=new Long(ppha.getPmediahaid());
                          } catch (Exception cie2) {}
                       }
                    }
                 }
              }
           }
        }
        
        /////////////////////////////////
        // Create HTML Table of result //
        /////////////////////////////////

        int tablecols=tlist.size();
        if (tablerows<1 || tablecols<1) {
           return null;
        }
        Table table=new Table(tablerows,tablecols);
        // iterate through table columns;
        for (int currcol=0;currcol<tlist.size();currcol++) {
           ArrayList rowlist=(ArrayList)tlist.get(currcol);
           int prevrow=0;
           Iterator rowiter=rowlist.iterator();
           while (rowiter.hasNext()) {
              HashMap map2=(HashMap)rowiter.next();
              Mediaha ha2=(Mediaha)map2.get("obj");
              int nrows=((Integer)map2.get("nrows")).intValue();
              int ncols=((Integer)map2.get("ncols")).intValue();
              if (nrows<=0 || ncols<=0) continue;
              Cell cell=new Cell(new Long(ha2.getMediahaid()).toString(),nrows,ncols);
              cell.setContent("type","mediaha");
              cell.setContent("obj",ha2);
              boolean okvar=false;
              int currrow=prevrow;
              // Use previous row & exceptions to calculate cell row placement.  This could probably
              // be made more efficient by running through a calculation loop, but the loss here 
              // is quite minimal
              int firstrow=0;
              while (!okvar) {
                 try {
                    table.setCell(cell,currrow,currcol);
                    firstrow=currrow;
                    prevrow=currrow+1;
                    okvar=true;
                 } catch (Exception te) { 
                    currrow++;
                    if (currrow>500000) throw new FORMSException("Too many rows returned");
                 }
              }   
           }
        }

        mav.addObject("status","MOVECATSEL");
        if (moveobj!=null) mav.addObject("passobj",moveobj);
        mav.addObject("table",table);
        return mav;

      } catch (Exception e) {

        mav.addObject("status","MOVECATERROR");
        return mav;

      }

   }

   private String moveCatSubmit() {
      String mediahaid=getRequest().getParameter("mediahaid");
      if (getRequest().getParameter("objecttype").indexOf("Mediainfo")>=0) {
         // Move form
         String mediainfoid=getRequest().getParameter("objectid");
         Mediainfo ft=(Mediainfo)getMainDAO().execUniqueQuery(
            "select ft from Mediainfo ft where mediainfoid=" + mediainfoid
            );
         ft.setPmediahaid(new Long(mediahaid));
         getMainDAO().saveOrUpdate(ft);
      } else  if (getRequest().getParameter("objecttype").indexOf("Remotemediacat")>=0) {
         // Move form
         String rmediacatid=getRequest().getParameter("objectid");
         Remotemediacat ft=(Remotemediacat)getMainDAO().execUniqueQuery(
            "select ft from Remotemediacat ft where ft.rmediacatid=" + rmediacatid
            );
            
         ft.setPmediahaid(new Long(mediahaid));
         getMainDAO().saveOrUpdate(ft);

      }
      clearSessionLists();
      return "FILEMOVED";
   }

   private String deleteFile() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      String mediainfoid=getRequest().getParameter("mediainfoid");
      Mediainfo ft=(Mediainfo)getMainDAO().execUniqueQuery(
         "select ft from Mediainfo ft where ft.mediainfoid=" + mediainfoid
         );

      Mediaha ftpfha=(Mediaha)getMainDAO().execUniqueQuery(
         "select ha from Mediaha ha where ha.mediahaid=" + ft.getPmediahaid()
         );

      // check permissions for move
      if (!ft.getUploaduser().equals(new Long(getAuth().getValue("auserid")))) {
         if (!getPermHelper().hasGlobalPerm(ftpfha.getVisibility(),"MEDDELOTH")) {
            return "NOAUTH";
         }
      }

      getMainDAO().delete(ft);
      clearSessionLists();
      return "FILEDELETED";
   }

   private ModelAndView showInfo() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=this.getAuth();

      Mediainfo minfo=null;
      Allusers uinfo=null;
      String instacr;

      if (auth.getValue("islocal").equalsIgnoreCase("TRUE")) {

         // PULL LOCAL FORM INFORMATION
         instacr="LOCAL";
         String mediainfoid=auth.getValue("mediainfoid");
         minfo=(Mediainfo)getMainDAO().execUniqueQuery(
            "select minfo from Mediainfo minfo where minfo.mediainfoid=" + mediainfoid
            );
         uinfo=(Allusers)getMainDAO().execUniqueQuery(
            "select u from Allusers u where u.auserid=" + minfo.getUploaduser()
            );
         mav.addObject("userdesc",uinfo.getUserdesc());

         Mediaha ftpfha=(Mediaha)getMainDAO().execUniqueQuery(
            "select ha from Mediaha ha where ha.mediahaid=" + minfo.getPmediahaid()
            );
   
         // check permissions for move
         if (!minfo.getUploaduser().equals(new Long(auth.getValue("auserid")))) {
            if (!getPermHelper().hasGlobalPerm(ftpfha.getVisibility(),"MEDEDITOTH")) {
               mav.addObject("status","NOAUTH");
               return mav;
            }
         }

      } else {

         // PULL REMOTE FORM INFORMATION
         String mediainfoid=getAuth().getValue("mediainfoid");

         Linkedinst linst=(Linkedinst)getMainDAO().execUniqueQuery(
            "select s.linkedinst from Linkedstudies s where s.lstdid=" + getAuth().getValue("lstdid")
            );
         instacr=linst.getAcronym();   

         SsaServiceObjectResult result=(SsaServiceObjectResult)submitServiceRequest(linst,"getMediaServiceTarget","getInfoRecord",mediainfoid);

         try {

            minfo=(Mediainfo)result.getObject();

         } catch (Exception e) {

             mav.addObject("status","COULDNOTPULL");
             return mav;

         }

         if (result==null || result.getStatus()!=FORMSServiceConstants.OK || minfo==null) {

             mav.addObject("status","COULDNOTPULL");
             return mav;
               
         }


      }

      mav.addObject("status","SHOWINFO");
      mav.addObject("minfo",minfo);
      mav.addObject("filesize",FileSizeUtil.displayFileSize(new Long(minfo.getFilesize()).longValue()));
      mav.addObject("instacr",instacr);
      //mav.addObject("filesize",minfo.getFilesize());
      return mav;












   }

   private ModelAndView editAttr() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String mediainfoid=getRequest().getParameter("mediainfoid");
      Mediainfo ft=(Mediainfo)getMainDAO().execUniqueQuery(
         "select ft from Mediainfo ft where ft.mediainfoid=" + mediainfoid
         );

      Mediaha ftpfha=(Mediaha)getMainDAO().execUniqueQuery(
         "select ha from Mediaha ha where ha.mediahaid=" + ft.getPmediahaid()
         );

      // check permissions for move
      if (!ft.getUploaduser().equals(new Long(getAuth().getValue("auserid")))) {
         if (!getPermHelper().hasGlobalPerm(ftpfha.getVisibility(),"MEDEDITOTH")) {
            mav.addObject("status","NOAUTH");
            return mav;
         }
      }
      mav.addObject("status","EDITATTR");
      mav.addObject("minfo",ft);
      return mav;

   }

   private String editAttrSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String mediainfoid=getRequest().getParameter("mediainfoid");
      String description=getRequest().getParameter("description");
      String creationdate=getRequest().getParameter("creationdate");
      String notes=getRequest().getParameter("notes");

      Mediainfo ft=(Mediainfo)getMainDAO().execUniqueQuery(
         "select ft from Mediainfo ft where ft.mediainfoid=" + mediainfoid
         );
      ft.setDescription(description);   
      ft.setCreationdate(creationdate);   
      ft.setNotes(notes);   
      getMainDAO().saveOrUpdate(ft);
      clearSessionLists();
      return "EDITCOMPLETE";
   }

   private String removeCat() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      String mediahaid=getRequest().getParameter("mediahaid");
      Mediaha fa=(Mediaha)getMainDAO().execUniqueQuery(
         "select fa from Mediaha fa where fa.mediahaid=" + mediahaid
         );
      // check permissions 
      if (!getPermHelper().hasGlobalPerm(fa.getVisibility(),"MEDCAT")) {
         return "NOAUTH";
      }
      // make sure category is not a root category
      if (fa.getPmediahaid()==null) {
         return "NOREMOVEROOT";
      }
      // make sure category contains no subcategories
      List clist=getMainDAO().execQuery(
         "select fa from Mediaha fa where fa.pmediahaid=" + mediahaid
         );
      if (clist.size()>0) {
         return "NOREMOVESUBCAT";
      }
      // Move existing files to parent   
      long pmediahaid=fa.getPmediahaid();
      List l=getMainDAO().execQuery(
         "select ft from Mediainfo ft where ft.pmediahaid=" + mediahaid
         );
      Iterator i=l.iterator();
      while (i.hasNext()) {
         Mediainfo ft=(Mediainfo)i.next();
         ft.setPmediahaid(pmediahaid);
         getMainDAO().saveOrUpdate(ft);
      }
      // Remove record
      getMainDAO().delete(fa);
      clearSessionLists();
      return "CATCOLLAPSED";
   }

   // Re-order categories within parent category
   public List reorderCat() {

      Long mediahaid=new Long(getRequest().getParameter("mediahaid"));

      Long pmediahaid=(Long)getMainDAO().execUniqueQuery(
                "select ha.pmediahaid from Mediaha ha where ha.mediahaid=" + mediahaid
                );

      List l=getMainDAO().execQuery(
                "select ha from Mediaha ha where ha.pmediahaid=:pmediahaid " +
                   "order by ha.haordr" 
                ,new String[] { "pmediahaid" }
                ,new Object[] { pmediahaid }
                );
      
      return l;

   }

   public String reorderCatSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String corderstr=(String)request.getParameter("corderstr");
      String[] sa=corderstr.split(",");
      boolean alreadychecked=false;
      for (int i=0;i<sa.length;i++) {
         Long mediahaid=new Long(sa[i].replaceFirst("^li_",""));

         // Make sure has permission for reorder
         if (!alreadychecked) {
            Mediaha cha=(Mediaha)getMainDAO().execUniqueQuery(
               "select ha from Mediaha ha where mediahaid=" + mediahaid
               );
            if (!getPermHelper().hasGlobalPerm(cha.getVisibility(),"MEDCAT")) {
               return "NOAUTH";
            }
         }
         alreadychecked=true;
   
         Mediaha fa2mod=(Mediaha)getMainDAO().execUniqueQuery(
                   "select ha from Mediaha ha where ha.mediahaid=:mediahaid"
                   ,new String[] { "mediahaid" }
                   ,new Object[] { mediahaid }
                   );
         fa2mod.setHaordr(i+1);
         getMainDAO().saveOrUpdate(fa2mod);
      }
      clearSessionLists();
      return "CATREORDERED";

   }

   private ModelAndView viewAllCat(ModelAndView mav) {

      mav=moveCat(mav,true);
      String status=(String)mav.getModelMap().get("status");  
      if (status.equalsIgnoreCase("MOVECATSEL")) {
        mav.addObject("status","VIEWALLSEL"); 
      }
      return mav;
      
   }

   private ModelAndView listProfiles(ModelAndView mav) {

      try {
         List l=getMainDAO().execQuery(
            "select p from Mediapermprofiles p where " +
               "(p.profilelevel=" + VisibilityConstants.SYSTEM + ") or " +
               "(p.profilelevel=" + VisibilityConstants.SITE + 
                  " and p.siteid=" + getAuth().getValue("siteid") + ") or " +
               "(p.profilelevel=" + VisibilityConstants.STUDY + 
                  " and p.studyid=" + getAuth().getValue("studyid") + ") " 
                  );
         mav.addObject("status","LISTPROFILES");
         Iterator i=l.iterator();
         ArrayList newl=new ArrayList();
         while (i.hasNext()) {
            Mediapermprofiles p=(Mediapermprofiles)i.next();
            HashMap map=new HashMap();
            map.put("profile",p);
            map.put("level",new VisibilityConstants().getFieldName(p.getProfilelevel()));
            newl.add(map);
         }
         mav.addObject("list",newl);
         return mav;
      } catch (Exception e) {
         return null;
      }

   }


   // Submit new profile
   private String addProfileSubmit() {

      String status=null;
      String profileacr=getRequest().getParameter("profileacr").toUpperCase();
      String profiledesc=getRequest().getParameter("profiledesc");
      int profilelevel=new VisibilityConstants().getFieldValue(getRequest().getParameter("profilelevel"));
      
      // Make sure profile acronym is not duplicated witin level and site/study
      List l;
      long q_siteid;
      try {
         q_siteid=new Long(getAuth().getValue("siteid")).longValue();
      } catch (Exception sie) {
         q_siteid=new Long("-999999999").longValue();
      }
      long q_studyid;
      try {
         q_studyid=new Long(getAuth().getValue("studyid")).longValue();
      } catch (Exception sie) {
         q_studyid=new Long("-999999999").longValue();
      }
      l=getMainDAO().execQuery(
            "select p from Mediapermprofiles p where " +
                "p.profileacr=:profileacr and (" +
                   "(" + profilelevel + "=" + VisibilityConstants.SYSTEM + " and p.profilelevel=" + VisibilityConstants.SYSTEM + ") or " +
                   "(" + profilelevel + "=" + VisibilityConstants.SITE + " and p.profilelevel=" + VisibilityConstants.SITE +
                       " and p.siteid=:siteid) or " +
                   "(" + profilelevel + "=" + VisibilityConstants.STUDY  + " and p.profilelevel=" + VisibilityConstants.STUDY +
                       " and p.studyid=:studyid)" +
                ")"
             ,new String[] { "profileacr","siteid","studyid" }
             ,new Object[] { profileacr,q_siteid,q_studyid }   
         );
      if (l.size()>0) {
         status="DUPLICATEPROFILE";
         return status;
      }
      // If not duplicate, add to database
      Mediapermprofiles profile=new Mediapermprofiles();
      profile.setProfileacr(profileacr);
      profile.setProfiledesc(profiledesc);
      profile.setProfilelevel(profilelevel);
      try {
         profile.setStudyid(new Long(getAuth().getValue("studyid")).longValue());
      } catch (Exception sse) {
         // may be null for system level profiles
      }
      try {
         profile.setSiteid(new Long(getAuth().getValue("siteid")).longValue());
      } catch (Exception sse) {
         // may be null for system level profiles
      }
      getMainDAO().saveOrUpdate(profile);
      getMAV().addObject("profilelevel",new VisibilityConstants().getFieldName(profilelevel));
      getMAV().addObject("profileacr",profileacr);
      status="PROFILEADDED";
      return status;

   }

   // Modify profile interface
   private String modifyProfile() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String status=null;
      long mprofileid=new Long(getRequest().getParameter("mprofileid")).longValue();
      
      // Make sure profile acronym is not duplicated witin level and site/study
      Mediapermprofiles p=(Mediapermprofiles)getMainDAO().execUniqueQuery(
            "select p from Mediapermprofiles p where p.mprofileid=" + mprofileid 
         );
      if (p==null) {
         status="PROFILEPULLERROR";
         return status;
      }

      // See if user is permitted to modify profile
      if (!getPermHelper().hasGlobalPerm(getAuth().getValue("auserid"),p.getProfilelevel(),"MEDPERMPROF")) {
         // not permitted
         return "NOAUTH";
      }

      getMAV().addObject("profile",p);
      getMAV().addObject("level",new VisibilityConstants().getFieldName(p.getProfilelevel()));
      status="MODIFYPROFILE";
      return status;

   }

   // Submit modified profile
   private String modifyProfileSubmit() {

      String status=null;
      String profileacr=getRequest().getParameter("profileacr").toUpperCase();
      String profiledesc=getRequest().getParameter("profiledesc");
      int profilelevel=new VisibilityConstants().getFieldValue(getRequest().getParameter("profilelevel"));
      long mprofileid=new Long(getRequest().getParameter("mprofileid")).longValue();
      
      // Make sure profile acronym is not duplicated witin level and site/study
      List l;
      long q_siteid;
      try {
         q_siteid=new Long(getAuth().getValue("siteid")).longValue();
      } catch (Exception sie) {
         q_siteid=new Long("-999999999").longValue();
      }
      long q_studyid;
      try {
         q_studyid=new Long(getAuth().getValue("studyid")).longValue();
      } catch (Exception sie) {
         q_studyid=new Long("-999999999").longValue();
      }
      l=getMainDAO().execQuery(
            "select p from Mediapermprofiles p where " +
                "p.mprofileid!=:mprofileid and " +
                "p.profileacr=:profileacr and (" +
                   "(" + profilelevel + "=" + VisibilityConstants.SYSTEM + " and p.profilelevel=" + VisibilityConstants.SYSTEM + ") or " +
                   "(" + profilelevel + "=" + VisibilityConstants.SITE + " and p.profilelevel=" + VisibilityConstants.SITE +
                       " and p.siteid=:siteid) or " +
                   "(" + profilelevel + "=" + VisibilityConstants.STUDY  + " and p.profilelevel=" + VisibilityConstants.STUDY +
                       " and p.studyid=:studyid)" +
                ")"
             ,new String[] { "mprofileid","profileacr","siteid","studyid" }
             ,new Object[] { mprofileid,profileacr,q_siteid,q_studyid }   
         );
      if (l.size()>0) {
         status="DUPLICATEPROFILE";
         return status;
      }
      // If not duplicate, add to database
      Mediapermprofiles profile=(Mediapermprofiles)getMainDAO().execUniqueQuery(
         "select p from Mediapermprofiles p where mprofileid=" + mprofileid
         );
      if (profile!=null) {   
         profile.setProfileacr(profileacr);
         profile.setProfiledesc(profiledesc);
         getMainDAO().saveOrUpdate(profile);
         status="PROFILEMODIFIED";
         return status;
      }
      status="PROFILEPULLERROR";
      return status;

   }

   // Delete profile
   private String removeProfileSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String status=null;
      long mprofileid=new Long(getRequest().getParameter("mprofileid")).longValue();

      // Can only delete profile if no files are using it
      List l=getMainDAO().execQuery(
         "select pa from Mediapermprofassign pa where pa.mediapermprofiles.mprofileid=" + mprofileid
         );
      if (l.size()>0) {
         status="PROFILENOTEMPTY";
         return status;
      }
      
      Mediapermprofiles profile=(Mediapermprofiles)getMainDAO().execUniqueQuery(
            "select profile from Mediapermprofiles profile where profile.mprofileid=" + mprofileid 
         );
      if (profile==null) {
         status="PROFILEPULLERROR";
         return status;
      }
      // See if user is permitted to modify profile
      if (!getPermHelper().hasGlobalPerm(getAuth().getValue("auserid"),profile.getProfilelevel(),"MEDPERMPROF")) {
         // not permitted
         return "NOAUTH";
      }
      getMainDAO().delete(profile);
      status="PROFILEDELETED";
      return status;

   }

}

