 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * RetrieveMediaFileController.java - Download Media File to Client
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.sql.Blob;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
//import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
//import org.springframework.transaction.support.TransactionSynchronizationManager;
//import org.mla.html.table.*;
import org.json.simple.JSONObject;

public class RetrieveMediaFileController extends FORMSSsaServiceClientController {

   public ModelAndView submitRequest() throws FORMSException {

      try {


         if (getAuth().getValue("islocal").toUpperCase().equals("TRUE")) {

            Mediastore store=(Mediastore)getMainDAO().execUniqueQuery(
               "select s from Mediastore s join fetch s.mediainfo join fetch s.mediainfo.supportedfileformats "
                + "where s.mediainfo.mediainfoid=" + getAuth().getValue("mediainfoid")
               );

            HttpServletResponse response=getResponse();
            response.setContentType(store.getMediainfo().getSupportedfileformats().getContenttype());
            response.setContentLength((store.getMediainfo().getFilesize()).intValue());
            // Send content as an attachment (generates open/save dialog)
            response.setHeader("Content-Disposition","attachment;filename=" + store.getMediainfo().getFilename()); 
            // reverse FORMSController-set headers
            response.setHeader("Cache-Control","public"); 
            response.setHeader("Pragma","cache"); 
            response.setDateHeader ("Expires", 5000); 
   
            Blob blob = store.getMediacontent();
            InputStream ip = blob.getBinaryStream();
            int c = 0;
            while (c != -1) {
               byte[] b=new byte[4096];
               c = ip.read(b);
               out.write(new String(b,"ISO-8859-1"));
               out.flush();
            }
            ip.close();
            return null;

         } else {      

            String mediainfoid=getAuth().getValue("mediainfoid");

            Linkedinst linst=(Linkedinst)getMainDAO().execUniqueQuery(
               "select s.linkedinst from Linkedstudies s where s.lstdid=" + getAuth().getValue("lstdid")
               );
            
            SsaServiceObjectResult result=(SsaServiceObjectResult)submitServiceRequest(linst,"getMediaServiceTarget","getContent",mediainfoid);

            if (result==null || result.getStatus()!=FORMSServiceConstants.OK) {

                mav.addObject("status","COULDNOTPULL");
                return mav;
               
            }

            Transferstore store=(Transferstore)result.getObject();
            HttpServletResponse response=getResponse();
            response.setContentType(store.getMediainfo().getSupportedfileformats().getContenttype());
            response.setContentLength((store.getMediainfo().getFilesize()).intValue());
            // Send content as an attachment (generates open/save dialog)
            response.setHeader("Content-Disposition","attachment;filename=" + store.getMediainfo().getFilename()); 
            // reverse FORMSController-set headers
            response.setHeader("Cache-Control","public"); 
            response.setHeader("Pragma","cache"); 
            response.setDateHeader ("Expires", 5000); 
            out.write(store.getMediacontent());
            out.flush();

            return null;
            
         }      

      } catch (Exception e) { 
         mav.addObject("status","COULDNOTPULL");
         return mav;
      }

   }

}

