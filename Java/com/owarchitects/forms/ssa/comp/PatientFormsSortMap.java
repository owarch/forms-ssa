 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 

package com.owarchitects.forms.ssa.comp;

import java.util.*;
import java.io.*;

// Create Comparable map class for output
public class PatientFormsSortMap extends HashMap implements Comparable {

   public int compareTo(Object other) {
      try {
         PatientFormsSortMap omap=(PatientFormsSortMap)other;
         try {
            if (new Long(this.get("FormNo").toString()).longValue()>new Long(omap.get("FormNo").toString()).longValue()) {
               return +1;
            } else if (new Long(this.get("FormNo").toString()).longValue()<new Long(omap.get("FormNo").toString()).longValue()) {
               return -1;
            } else {
               if (new Long(this.get("FormVer").toString()).longValue()>new Long(omap.get("FormVer").toString()).longValue()) {
                  return +1;
               } else if (new Long(this.get("FormVer").toString()).longValue()<new Long(omap.get("FormVer").toString()).longValue()) {
                  return -1;
               } else {
                  int cvalue=((String)this.get("Time")).compareTo(((String)omap.get("Time")));
                  if (cvalue!=0) {
                     return cvalue;
                  }
               }   
            }   
         } catch (Exception cte) {
            if (this.get("FormNo").toString().compareTo(omap.get("FormNo").toString())>0) {
               return +1;
            } else if (this.get("FormNo").toString().compareTo(omap.get("FormNo").toString())<0) {
               return -1;
            } else {
               if (this.get("FormVer").toString().compareTo(omap.get("FormVer").toString())>0) {
                  return +1;
               } else if (this.get("FormVer").toString().compareTo(omap.get("FormVer").toString())<0) {
                  return -1;
               } else {
                  int cvalue=((String)this.get("Time")).compareTo(((String)omap.get("Time")));
                  if (cvalue!=0) {
                     return cvalue;
                  }
               }   
            }   
         }
         return -1;
      } catch (Exception e) {
         return -1;
      }   
   }

}


