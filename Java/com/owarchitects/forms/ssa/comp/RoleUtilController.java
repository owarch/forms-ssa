 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * RoleUtilController.java - User utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.security.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.*;
import javax.crypto.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;


public class RoleUtilController extends FORMSController {

   boolean isbadrequest;

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // check permissions
      if (!getAuth().getValue("rightslevel").equalsIgnoreCase("ADMIN")) {
         safeRedirect("permission.err");
         return null;
      }

      // Use class-name based view
      mav=new ModelAndView("RoleUtil");
      isbadrequest=true;
      String spath=request.getServletPath();

      try {

         // Add new role interface (ROLE LEVEL SELECTION)
         if (spath.indexOf("newrole.")>=0) {

            isbadrequest=false;
            mav.addObject("status","NEWROLE");
            String studyid=this.getAuth().getValue("studyid");
            if (studyid==null || studyid.length()<1) {
               mav.addObject("list",new VisibilityConstants().getFieldList(VisibilityConstants.SYSTEM));
            } else {
               mav.addObject("list",new VisibilityConstants().getFieldList());
            }
            return mav;

         // Add new role interface (ENTER ROLE ATTRIBUTES)
         } else if (spath.indexOf("addrole.")>=0) {

            isbadrequest=false;
            mav.addObject("status","ADDROLE");
            mav.addObject("rolelevel",new VisibilityConstants().getFieldName(
                       new Integer(getRequest().getParameter("rolelevel")).intValue()
                         ));
            return mav;

         // Submit new role to system
         } else if (spath.indexOf("addrolesubmit.")>=0) {

            String status=addRoleSubmit();
            mav.addObject("status",status);
            return mav;

         // Modify role interface
         } else if (spath.indexOf("modifyrole.")>=0) {

            modifyRole();
            isbadrequest=false;
            mav.addObject("status","MODIFYROLE");
            return mav;

         // Submit modified role
         } else if (spath.indexOf("modifyrolesubmit.")>=0) {

            String status=modifyRoleSubmit();
            mav.addObject("status",status);
            return mav;

         // Delete specified role 
         } else if (spath.indexOf("removerolesubmit.")>=0) {

            String status=removeRoleSubmit();
            mav.addObject("status",status);
            return mav;

         // Role membership selection screen
         } else if (spath.indexOf("rolemembership.")>=0) {

            roleMembershipSelection();
            isbadrequest=false;
            mav.addObject("status","ROLEMEMBERSHIP");
            return mav;

         } 

         out.println(spath);
         isbadrequest=false;
         return null;

      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Submit new role
   private String addRoleSubmit() {

            String status=null;
            String roleacr=getRequest().getParameter("roleacr").toUpperCase();
            String roledesc=getRequest().getParameter("roledesc");
            int rolelevel=new VisibilityConstants().getFieldValue(getRequest().getParameter("rolelevel"));
            
            // Make sure role acronym is not duplicated witin level and site/study
            List l;
            long q_siteid;
            try {
               q_siteid=new Long(getAuth().getValue("siteid")).longValue();
            } catch (Exception sie) {
               q_siteid=new Long("-999999999").longValue();
            }
            long q_studyid;
            try {
               q_studyid=new Long(getAuth().getValue("studyid")).longValue();
            } catch (Exception sie) {
               q_studyid=new Long("-999999999").longValue();
            }
            l=getMainDAO().execQuery(
                  "select rr from Resourceroles rr where " +
                      "rr.roleacr=:roleacr and (" +
                         "(" + rolelevel + "=" + VisibilityConstants.SYSTEM + " and rr.rolelevel=" + VisibilityConstants.SYSTEM + ") or " +
                         "(" + rolelevel + "=" + VisibilityConstants.SITE + " and rr.rolelevel=" + VisibilityConstants.SITE +
                             " and rr.siteid=:siteid) or " +
                         "(" + rolelevel + "=" + VisibilityConstants.STUDY  + " and rr.rolelevel=" + VisibilityConstants.STUDY +
                             " and rr.studyid=:studyid)" +
                      ")"
                   ,new String[] { "roleacr","siteid","studyid" }
                   ,new Object[] { roleacr,q_siteid,q_studyid }   
               );
            if (l.size()>0) {
               status="DUPLICATEROLE";
               return status;
            }
            // If not duplicate, add to database
            Resourceroles rrole=new Resourceroles();
            rrole.setRoleacr(roleacr);
            rrole.setRoledesc(roledesc);
            rrole.setRolelevel(rolelevel);
            try {
               rrole.setStudyid(new Long(getAuth().getValue("studyid")).longValue());
            } catch (Exception sse) {
               // may be null for system level roles
            }
            try {
               rrole.setSiteid(new Long(getAuth().getValue("siteid")).longValue());
            } catch (Exception sse) {
               // may be null for system level roles
            }
            getMainDAO().saveOrUpdate(rrole);
            isbadrequest=false;
            getMAV().addObject("rolelevel",new VisibilityConstants().getFieldName(rolelevel));
            getMAV().addObject("roleacr",roleacr);
            status="ROLEADDED";
            return status;

   }

   // Modify role interface
   private String modifyRole() {

            String status=null;
            long rroleid=new Long(getRequest().getParameter("rroleid")).longValue();
            
            // Make sure role acronym is not duplicated witin level and site/study
            Resourceroles rr=(Resourceroles)getMainDAO().execUniqueQuery(
                  "select rr from Resourceroles rr where " +
                      "rr.rroleid=:rroleid" 
                   ,new String[] { "rroleid" }
                   ,new Object[] { rroleid }   
               );
            if (rr==null) {
               status="ROLEPULLERROR";
               return status;
            }
            if (rr.getRoleacr().equalsIgnoreCase("ALLUSERS") && rr.getRolelevel()==VisibilityConstants.SYSTEM) {
               status="NOMODIFYALLUSERS";
               return status;
            }
            getMAV().addObject("rroleid",rroleid);
            getMAV().addObject("rolelevel",new VisibilityConstants().getFieldName(rr.getRolelevel()));
            getMAV().addObject("roleacr",rr.getRoleacr());
            getMAV().addObject("roledesc",rr.getRoledesc());
            status="MODIFYROLE";
            return status;

   }

   // Submit modified role
   private String modifyRoleSubmit() {

            String status=null;
            String roleacr=getRequest().getParameter("roleacr").toUpperCase();
            String roledesc=getRequest().getParameter("roledesc");
            int rolelevel=new VisibilityConstants().getFieldValue(getRequest().getParameter("rolelevel"));
            long rroleid=new Long(getRequest().getParameter("rroleid")).longValue();
            
            // Make sure role acronym is not duplicated witin level and site/study
            List l;
            long q_siteid;
            try {
               q_siteid=new Long(getAuth().getValue("siteid")).longValue();
            } catch (Exception sie) {
               q_siteid=new Long("-999999999").longValue();
            }
            long q_studyid;
            try {
               q_studyid=new Long(getAuth().getValue("studyid")).longValue();
            } catch (Exception sie) {
               q_studyid=new Long("-999999999").longValue();
            }
            l=getMainDAO().execQuery(
                  "select rr from Resourceroles rr where " +
                      "rr.rroleid!=:rroleid and " +
                      "rr.roleacr=:roleacr and (" +
                         "(" + rolelevel + "=" + VisibilityConstants.SYSTEM + " and rr.rolelevel=" + VisibilityConstants.SYSTEM + ") or " +
                         "(" + rolelevel + "=" + VisibilityConstants.SITE + " and rr.rolelevel=" + VisibilityConstants.SITE +
                             " and rr.siteid=:siteid) or " +
                         "(" + rolelevel + "=" + VisibilityConstants.STUDY  + " and rr.rolelevel=" + VisibilityConstants.STUDY +
                             " and rr.studyid=:studyid)" +
                      ")"
                   ,new String[] { "rroleid","roleacr","siteid","studyid" }
                   ,new Object[] { rroleid,roleacr,q_siteid,q_studyid }   
               );
            if (l.size()>0) {
               status="DUPLICATEROLE";
               return status;
            }
            // If not duplicate, add to database
            ResourcerolesDAO resourcerolesDAO=(ResourcerolesDAO)this.getContext().getBean("resourcerolesDAO");
            Resourceroles rrole=resourcerolesDAO.getRecord(rroleid);
            rrole.setRoleacr(roleacr);
            rrole.setRoledesc(roledesc);
            resourcerolesDAO.saveOrUpdate(rrole);
            isbadrequest=false;
            status="ROLEMODIFIED";
            return status;

   }

   // Delete role
   private String removeRoleSubmit() {

            String status=null;
            long rroleid=new Long(getRequest().getParameter("rroleid")).longValue();
            
            // Make sure role acronym is not duplicated witin level and site/study
            Resourceroles rr=(Resourceroles)getMainDAO().execUniqueQuery(
                  "select rr from Resourceroles rr where " +
                      "rr.rroleid=:rroleid" 
                   ,new String[] { "rroleid" }
                   ,new Object[] { rroleid }   
               );
            if (rr==null) {
               status="ROLEPULLERROR";
               return status;
            }
            if (rr.getRoleacr().equalsIgnoreCase("ALLUSERS") && rr.getRolelevel()==VisibilityConstants.SYSTEM) {
               status="NOMODIFYALLUSERS";
               return status;
            }
            isbadrequest=false;
            getMainDAO().delete(rr);
            status="ROLEDELETED";
            return status;

   }

   // Role membership selection screen
   private void roleMembershipSelection() {

            long rroleid=new Long(getRequest().getParameter("rroleid")).longValue();

            ResourcerolesDAO resourcerolesDAO=(ResourcerolesDAO)this.getContext().getBean("resourcerolesDAO");
            Resourceroles thisrole=resourcerolesDAO.getRecord(rroleid);

            // Pull in roles other than the current role.  Then check these roles to see if they are
            // children of the parent role.  If so, use Resourcerolesext to contain the info
            List l =getMainDAO().execQuery(
                  "select distinct rr from Resourceroles rr " +
                     "left join fetch rr.childroleassign " +
                     "where rr.rroleid!=:rroleid " +
                     "order by rolelevel,siteid,studyid,roleacr "
                   ,new String[] { "rroleid" }
                   ,new Object[] { rroleid }   
                );
            // Create list of studyno's, siteno's    
            List templ;

            templ=getMainDAO().getTable("Studies");    
            HashMap studymap=new HashMap<Long,String>();
            for (int i=0;i<templ.size();i++) {
               Studies tempstd=(Studies)templ.get(i);
               studymap.put(tempstd.getStudyid(),tempstd.getStudyno());
            }

            templ=getMainDAO().getTable("Sites");    
            HashMap sitemap=new HashMap<Long,String>();
            for (int i=0;i<templ.size();i++) {
               Sites tempsite=(Sites)templ.get(i);
               sitemap.put(tempsite.getSiteid(),tempsite.getSiteno());
            }

            ArrayList newl=new ArrayList();    
            for (int i=0;i<l.size();i++) {
               Resourceroles jrr=(Resourceroles)l.get(i);
               Resourcerolesext ext=new Resourcerolesext();
               ext.setResourceroles(jrr);
               ext.setIschild(false);
               ext.setRolelevel(new VisibilityConstants().getFieldName(jrr.getRolelevel())); 
               try { 
                  if (jrr.getRolelevel()==VisibilityConstants.SYSTEM ||
                      jrr.getRolelevel()==VisibilityConstants.SITE) {
                     ext.setStudyno(" ");
                  } else {    
                     ext.setStudyno((String)studymap.get(new Long(jrr.getStudyid()))); 
                  } 
               } catch (Exception sete) {
                  ext.setStudyno(" ");
               }
               try { 
                  if (jrr.getRolelevel()==VisibilityConstants.SYSTEM) { 
                     ext.setSiteno(" ");
                  } else {    
                     ext.setSiteno((String)sitemap.get(new Long(jrr.getSiteid())));
                  } 
               } catch (Exception sete) { 
                  ext.setSiteno(" ");
               }
               // get list of roles for which it is the child
               Iterator iter=jrr.getChildroleassign().iterator();
               while (iter.hasNext()) {
                  Rroleroleassign rra=(Rroleroleassign)iter.next();
                  // see if parent is the current parent
                  if (rra.getParentrole().getRroleid()==rroleid) {
                     ext.setIschild(true);
                     break;
                  }
               }
               newl.add(ext);
            }

            RoleUtilResult result=new RoleUtilResult();
            result.setList(newl);
            result.setParentid(rroleid);
            this.getSession().setAttribute("iframeresult",result);

            // PULL IN USER LIST
            l=getMainDAO().execQuery("select distinct a from Allusers a " + 
                                         "left join fetch a.rroleuserassign left join fetch a.allinst " +
                                            "left join fetch a.allinst.linkedinst " +
                                         "where not a.isdeleted=true " +
                                         "order by a.allinst.islocal desc,a.allinst.linkedinst.linstid,a.userdesc");

            // Create list of studyno's, siteno's    
            newl=new ArrayList();    
            for (int i=0;i<l.size();i++) {
               Allusers jrr=(Allusers)l.get(i);
               Allusersext ext=new Allusersext();
               ext.setIschild(false);
               ext.setAllusers(jrr);
               // get list of roles for which it is the child
               Iterator iter=jrr.getRroleuserassign().iterator();
               while (iter.hasNext()) {
                  Rroleuserassign rra=(Rroleuserassign)iter.next();
                  // see if parent is the current parent
                  if (rra.getRole().getRroleid()==rroleid) {
                     ext.setIschild(true);
                     break;
                  }
               }
               // Only keep records with no mactc
               newl.add(ext);
            }

            result=new RoleUtilResult();
            result.setList(newl);
            result.setParentid(rroleid);
            this.getSession().setAttribute("iframe2result",result);
            getMAV().addObject("rolelevel",new VisibilityConstants().getFieldName(thisrole.getRolelevel()));
            getMAV().addObject("roleacr",thisrole.getRoleacr());
            getMAV().addObject("roledesc",thisrole.getRoledesc());

   }


}


