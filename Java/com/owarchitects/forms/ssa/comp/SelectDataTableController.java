
/*
 *
 * SelectDataTableController.java - DataTable Selection Interface
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.mla.html.table.*;

public class SelectDataTableController extends FORMSController {

   public ModelAndView submitRequest() throws FORMSException {

      try {

         FORMSAuth auth=this.getAuth();

         ModelAndView mav=new ModelAndView();
         HttpServletResponse response=this.getResponse();
         PrintWriter out=this.getWriter();

         // Initialize Options (If necessary)
         auth.initOption("archopt","N");
         auth.initOption("sinstopt","");

         // clear stored values
         auth.setValue("dtdefid","");

         SelectDataTableResult result=processData(false);
         result.setArchopt(auth.getOption("archopt"));
         result.setSinstopt(auth.getOption("sinstopt"));
         result.setRightslevel(auth.getOption("rightslevel"));
         this.getSession().setAttribute("iframeresult",result);
         mav.addObject("status","OK");
         return mav;
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Create root hierarchy record if none exists (these are not created at database initialization)
   private void createHaRecord(int vis) throws FORMSException, FORMSSecurityException, FORMSKeyException {
      Studies s=(Studies)getMainDAO().execUniqueQuery(
         "select s from Studies s where studyid=" + getAuth().getValue("studyid")
         );
      if (s==null) return;   
      Datatableha newha=new Datatableha();
      newha.setVisibility(vis);
      newha.setStudies(s);
      newha.setHaordr(1);
      if (vis==VisibilityConstants.STUDY) {
         newha.setHadesc("Study-Level Tables");
      } else if (vis==VisibilityConstants.SITE) {
         newha.setHadesc("Site-Level Tables");
      } else if (vis==VisibilityConstants.SYSTEM) {
         newha.setHadesc("System-Level Tables");
      } else {
         return;
      }
      getMainDAO().saveOrUpdate(newha);
      return;

   }

   //private SelectDataTableResult processData(boolean showarchivedtables) throws FORMSException {
   private SelectDataTableResult processData(boolean showarchivedtables) throws FORMSException {

      //SelectDataTableResult result=new SelectDataTableResult();

      try {

        FORMSAuth auth=this.getAuth();

        // Use session-stored lists if available to avoid frequent hits to database for pulling form list
        List fhatemp=(List)auth.getSessionAttribute("dthalist");
        List dtdtemp=(List)auth.getSessionAttribute("datatablelist");
        HashMap iinfotemp=(HashMap)auth.getSessionAttribute("dtinfomap");
        Iterator i;


        // If no/empty list available, pull from database
        if (fhatemp==null || dtdtemp==null || fhatemp.size()<1 || dtdtemp.size()<1) {

           // Retrive installation information for later
           List ilist=getMainDAO().execQuery(
              "select a from Allinst a join fetch a.linkedinst where a.islocal!=true"
              );
           i=ilist.iterator();
           HashMap dtinfomap=new HashMap();
           while (i.hasNext()) {
             Allinst ainst=(Allinst)i.next();
             dtinfomap.put(new Long(ainst.getAinstid()).toString(),ainst.getLinkedinst().getAcronym());
           }

           fhatemp=getMainDAO().execQuery(
              "select fha from Datatableha fha " +
                    "where ((" +
                              "fha.visibility=" + VisibilityConstants.SYSTEM + 
                              ") or (" +
                              "fha.visibility=" + VisibilityConstants.SITE + " and fha.studies.sites.siteid=" + auth.getValue("siteid") + 
                              ") or (" +
                              "fha.visibility=" + VisibilityConstants.STUDY + " and fha.studies.studyid=" + auth.getValue("studyid")  + 
                           ")) " +
                    "order by haordr"
              );

           i=fhatemp.iterator();
           // 1) Write dthaid to StringBuilder for use in datatabledef pull
           // 2) Verify that root system, site & study records exist.  Otherwise create them
           StringBuilder sb=new StringBuilder();
           boolean hassystem=false, hassite=false, hasstudy=false;
           while (i.hasNext()) {
              Datatableha ha=(Datatableha)i.next();
              // write id to stringbuffer
              sb.append(new Long(ha.getDthaid()).toString());
              if (i.hasNext()) sb.append(",");
              // check that necessary root hierarchy records exist, else create them
              if (ha.getVisibility()==VisibilityConstants.SYSTEM && ha.getPdthaid()==null) {
                 hassystem=true;
              } else if (ha.getVisibility()==VisibilityConstants.SITE && ha.getPdthaid()==null) {
                 hassite=true;
              } else if (ha.getVisibility()==VisibilityConstants.STUDY && ha.getPdthaid()==null) {
                 hasstudy=true;
              } 
           }
           if (!hassystem) createHaRecord(VisibilityConstants.SYSTEM);
           if (!hassite) createHaRecord(VisibilityConstants.SITE);
           if (!hasstudy) createHaRecord(VisibilityConstants.STUDY);
     
           // Pull datatabledef data
           if (sb.length()>0) {
              dtdtemp=getMainDAO().execQuery(
                 "select dtd from Datatabledef dtd join fetch dtd.allinst " +
                    "where dtd.pdthaid in (" 
                       + sb.toString() + ") and dtd.displayas in (" +
                         DisplayAsConstants.DATATABLEONLY + "," + DisplayAsConstants.FORMANDDATATABLE 
                       + ") " +
                    "order by dtd.dtacr"
              );
           } else {
              dtdtemp=(List)new ArrayList();
           }

           auth.setSessionAttribute("dthalist",fhatemp);
           auth.setSessionAttribute("datatablelist",dtdtemp);
           auth.setSessionAttribute("dtinfomap",dtinfomap);

           iinfotemp=(HashMap)auth.getSessionAttribute("dtinfomap");

        }

        // Create new list based on stored list
        List fha=(List)new ArrayList(fhatemp);

        // add forms and installation info to final form list
        Iterator dtdi=dtdtemp.iterator();
        List alltablelist=(List)new ArrayList();
        auth.setValue("showsinstopt","N");
        boolean already=false;
        // Add tables to list
        while (dtdi.hasNext()) {
           Datatabledef dtd=(Datatabledef)dtdi.next();
           HashMap dtdimap=new HashMap();
           if (dtd.getRemotedtdefid()==null) {
              dtdimap.put("linstid",null);
              dtdimap.put("lstdid",null);
              dtdimap.put("instacr","LOCAL");
           } else {
              if (!already) {
                 auth.setValue("showsinstopt","Y");
                 already=true;
              }
              dtdimap.put("linstid",dtd.getAllinst().getAinstid());
              dtdimap.put("lstdid",new Long(getAuth().getValue("studyid")));
              dtdimap.put("instacr",iinfotemp.get(new Long(dtd.getAllinst().getAinstid()).toString()));
           }
           dtdimap.put("table",dtd);
           // check archive status and option and add to list
           String archoptval=auth.getOption("archopt");
           if (archoptval!=null && archoptval.equalsIgnoreCase("N")) { 
              if (!dtd.getIsarchived()) {
                 alltablelist.add(dtdimap);
              }
           } else {
              alltablelist.add(dtdimap);
           }
        }

        List fpermlist=getPermHelper().getFormPermList();

        // Refine list based on options
        // 2) Check permissions
        // 3) Compute recency (is most recent version)
        HashMap checkver=new HashMap();
        ArrayList tabledeflist=new ArrayList();
        tabledeflist.add("ALL");
        String typeopt=auth.getOption("typeopt");
        i=alltablelist.iterator();
        while (i.hasNext()) {
           HashMap imap=(HashMap)i.next();
           Datatabledef dtdef=(Datatabledef)imap.get("table");
           String instacr=(String)imap.get("instacr");
           boolean isremoved=false;
           // Check permissions - (based on data table permission list list)
           if (!(isremoved || auth.getValue("rightslevel").equalsIgnoreCase("ADMIN"))) {
              Long dtdefid=new Long(dtdef.getDtdefid());
              Iterator piter=fpermlist.iterator();
              boolean found=false;
              while (piter.hasNext()) {
                 HashMap map=(HashMap)piter.next();
                 Long compid;
                 try {
                    compid=new Long((String)map.get("dtdefid"));
                 } catch (ClassCastException cce) {   
                    compid=(Long)map.get("dtdefid");
                 }   
                 if (compid.equals(dtdefid)) {
                    found=true;
                    break;
                 }
              }
              if (!found) {
                 i.remove();
                 isremoved=true;
              }
           }
        }

        ////////////////////////////////////////////////////////////////////
        // Organize data into heirarchical format for building HTML table //
        ////////////////////////////////////////////////////////////////////

        // ft holds number of rows in table.  Keep this info for later
        int tablerows=alltablelist.size();

        // tlist holds table columns
        ArrayList tlist=new ArrayList();
        // alist holds items within columns
        ArrayList alist=new ArrayList();

        // pull first table column
        i=fha.iterator();
        while (i.hasNext()) {
           Datatableha ha=(Datatableha)i.next();
           if (ha.getPdthaid()==null) {
              HashMap map=new HashMap();
              map.put("isnull","N");
              map.put("obj",ha);
              map.put("nrows",0);
              map.put("ncols",1);
              // remove element from collection so we don't have to compare it again
              i.remove();
              alist.add(map);
           }
        }   
        tlist.add(alist);
        // pull subsequent columns by finding children of elements in previous columns
        for (int ii=1;ii>-1;ii++) {
           // pull elements in prior column
           ArrayList plist=(ArrayList)tlist.get(ii-1);
           alist=new ArrayList();
           Iterator i2=plist.iterator();
           boolean again=false;
           //iterate through prior column elements
           while (i2.hasNext()) {
              // find children
              HashMap pmap=(HashMap)i2.next();
              Datatableha pha=(Datatableha)pmap.get("obj");
              i=fha.iterator();
              boolean havechild=false;
              int prevsize=alist.size();
              while (i.hasNext()) {
                 Datatableha cha=(Datatableha)i.next();
                 // write child out to alist
                 if (cha.getPdthaid().equals(pha.getDthaid())) {
                    HashMap cmap=new HashMap();
                    cmap.put("isnull","N");
                    cmap.put("obj",cha);
                    cmap.put("nrows",0);
                    cmap.put("ncols",1);
                    // remove element from collection so we don't have to compare it again
                    i.remove();
                    alist.add(cmap);
                    again=true;
                    havechild=true;
                 }
                 
              }
              // create null row to hold records belonging to item when child
              // categories are present
              if (havechild) {
                 HashMap nmap=new HashMap();
                 Datatableha nha=new Datatableha();
                 nha.setDthaid(pha.getDthaid());
                 nha.setPdthaid(pha.getPdthaid());
                 nha.setHadesc("&nbsp;");
                 nmap.put("isnull","Y");
                 nmap.put("obj",nha);
                 nmap.put("nrows",0);
                 nmap.put("ncols",1);
                 alist.add(prevsize,nmap);
              }
           }
           if (!again) break;
           tlist.add(alist);
        }
        // add tables to appropriate category (must iterate backwards through tlist
        // since tables are attached as the right most element)
        for (int ii=(tlist.size()-1);ii>=0;ii--) {
           // pull column arraylist
           ArrayList plist=(ArrayList)tlist.get(ii);
           Iterator i2=plist.iterator();
           while (i2.hasNext()) {
              HashMap pmap=(HashMap) i2.next();
              Datatableha pha=(Datatableha)pmap.get("obj");
              // tablelist holds tables belonging to an element
              ArrayList tablelist=new ArrayList();
              Iterator i3=alltablelist.iterator();
              while (i3.hasNext()) {
                 HashMap i3map=(HashMap)i3.next();
                 Datatabledef dtdef=(Datatabledef)i3map.get("table");
                 Long linstid=(Long)i3map.get("linstid");
                 Long lstdid=(Long)i3map.get("lstdid");
                 String instacr=(String)i3map.get("instacr");
                 // add form to tablelist where there is a match
                 if (dtdef.getPdthaid()==pha.getDthaid()) {
                    HashMap flmap=new HashMap();
                    flmap.put("table",dtdef);
                    flmap.put("linstid",linstid);
                    flmap.put("lstdid",lstdid);
                    flmap.put("instacr",instacr);
                    tablelist.add(flmap);
                    // remove element from collection so we don't have to compare it again
                    i3.remove();
                 }
              }
              // add tablelist to element's hashmap
              if (tablelist.size()>0) {
                 pmap.put("tablelist",tablelist);
                 pmap.put("nrows",tablelist.size());
                 pmap.put("ncols",tlist.size()-ii);
              }
              // add nrows to parent cells from prior columns
              Long compid=null;
              try {
                 compid=new Long(pha.getPdthaid());
              } catch (Exception cie) { }
              // loop through prior columns looking for parent elements
              for (int jj=(ii-1);jj>=0;jj--) {
                 ArrayList pplist=(ArrayList)tlist.get(jj);
                 Iterator i4=pplist.iterator();
                 while (i4.hasNext()) {
                    HashMap ppmap=(HashMap) i4.next();
                    Datatableha ppha=(Datatableha)ppmap.get("obj");
                    String isnull=(String)ppmap.get("isnull");
                    // if find a parent element, add rows to that element.  Then update
                    // compid to that element's parent for next loop
                    if ((
                           (!(isnull.equals("Y") || compid==null)) && ppha.getDthaid()==compid.longValue()
                        ) ||
                        (
                           ppha.getDthaid()==pha.getDthaid() && ppha.getPdthaid()==pha.getPdthaid()
                       )) {
                       int nrows=((Integer)ppmap.get("nrows")).intValue();
                       nrows=nrows+tablelist.size();
                       ppmap.put("nrows",nrows);
                       try {
                          compid=new Long(ppha.getPdthaid());
                       } catch (Exception cie2) {}
                    }
                 }
              }
           }
        }
        
        /////////////////////////////////
        // Create HTML Table of result //
        /////////////////////////////////

        int tablecols=tlist.size()+1;
        if (tablerows<1 || tablecols<1) {
           throw new FORMSException("Zero length table");
        }
        Table table=new Table(tablerows,tablecols);
        // iterate through table columns;
        for (int currcol=0;currcol<tlist.size();currcol++) {
           ArrayList rowlist=(ArrayList)tlist.get(currcol);
           int prevrow=0;
           Iterator rowiter=rowlist.iterator();
           while (rowiter.hasNext()) {
              HashMap map2=(HashMap)rowiter.next();
              Datatableha ha2=(Datatableha)map2.get("obj");
              int nrows=((Integer)map2.get("nrows")).intValue();
              int ncols=((Integer)map2.get("ncols")).intValue();
              if (nrows<=0 || ncols<=0) continue;
              Cell cell=new Cell(new Long(ha2.getDthaid()).toString(),nrows,ncols);
              cell.setContent("type","datatableha");
              cell.setContent("obj",ha2);
              boolean okvar=false;
              int currrow=prevrow;
              // Use previous row & exceptions to calculate cell row placement.  This could probably
              // be made more efficient by running through a calculation loop, but the loss here 
              // is quite minimal
              int firstrow=0;
              while (!okvar) {
                 try {
                    table.setCell(cell,currrow,currcol);
                    firstrow=currrow;
                    prevrow=currrow+1;
                    okvar=true;
                 } catch (Exception te) { 
                    currrow++;
                    if (currrow>500000) throw new FORMSException("Too many rows returned");
                 }
              }   
              // see if contains any tables (final column)
              ArrayList tablelist=(ArrayList)map2.get("tablelist");
              if (tablelist!=null) {
                 // Rows for items must be placed based on row location of containing element
                 // (first row number of containing element is held in "firstrow")
                 int prevrow2=firstrow;
                 Iterator tableiter=tablelist.iterator();
                 while (tableiter.hasNext()) {
                    HashMap ftmap=(HashMap)tableiter.next();
                    Datatabledef datatable=(Datatabledef)ftmap.get("table");
                    Long linstid=(Long)ftmap.get("linstid");
                    Long lstdid=(Long)ftmap.get("lstdid");
                    String instacr=(String)ftmap.get("instacr");
                    cell=new Cell(new Long(datatable.getDtdefid()).toString(),1,1);
                    cell.setContent("type","datatable");
                    cell.setContent("obj",datatable);
                    cell.setContent("instacr",instacr);
                    if (linstid==null) {
                       cell.setContent("linstid",-99999999);
                       cell.setContent("lstdid",-99999999);
                       cell.setContent("islocal",true);

                    } else {
                       cell.setContent("linstid",linstid);
                       cell.setContent("lstdid",lstdid);
                       cell.setContent("islocal",false);
                    }
                    okvar=false;
                    currrow=prevrow2;
                    while (!okvar) {
                       try {
                          table.setCell(cell,currrow,tablecols-1);
                          prevrow2=currrow+1;
                          okvar=true;
                       } catch (Exception te) {
                          currrow++;
                          if (currrow>500000) throw new FORMSException("Too many rows returned");
                       }
                    }
                 }
              }
           }
        }
        // Create Options Menu Map
        HashMap optionsmap=new HashMap();
        optionsmap.put("tabledeflist",tabledeflist);
        optionsmap.put("typeopt",auth.getOption("typeopt"));
        optionsmap.put("veropt",auth.getOption("veropt"));
        optionsmap.put("archopt",auth.getOption("archopt"));
        optionsmap.put("fnumopt",auth.getOption("fnumopt"));
        optionsmap.put("sinstopt",auth.getOption("sinstopt"));
        auth.setSessionAttribute("optionsobject",optionsmap);

        // Create & return SelectDataTableResult Object
        SelectDataTableResult result=new SelectDataTableResult();

        if (tabledeflist.size()>2) {
           result.setShowtype(true);
        } else {   
           result.setShowtype(false);
        }   
        result.setTable(table);

        return result;

      } catch (Exception e) {

        SelectDataTableResult result=new SelectDataTableResult();
        return result;

      }

   }


}

