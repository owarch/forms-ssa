 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * PermsmgtController.java - Permissions Management Interface
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;

public class PermsmgtController extends FORMSController {

   private boolean showsite;

   public ModelAndView submitRequest() throws FORMSException {

      try {

         FORMSAuth auth=this.getAuth();

         ModelAndView mav=new ModelAndView();
         HttpServletResponse response=this.getResponse();
         PrintWriter out=this.getWriter();

         // check permissions
         if (!hasGlobalPerm(PcatConstants.SYSTEM,"PERMSMGT")) {
            safeRedirect("permission.err");
            return null;
         }

         // Determine which categories to show
         String pcatshow=auth.getValue("pcatshow");

         if (getRequest().getParameter("show")!=null) {
            pcatshow=getRequest().getParameter("show");
            auth.setValue("pcatshow",pcatshow);
         }

         if (pcatshow==null || pcatshow.length()<1) {
            auth.setValue("pcatshow","ALL");
            pcatshow="ALL";
         }

         List l;
         
         // pull category list for options menu
         l=getMainDAO().execQuery("select pc from Permissioncats pc order by pc.pcatorder");
         Permissioncats allcat=new Permissioncats();
         allcat.setPcatvalue(PcatConstants.ALL);
         l.add(allcat);
         ArrayList optlist=new ArrayList<HashMap>();
         Iterator i=l.iterator();
         while (i.hasNext()) {
            Permissioncats pc=(Permissioncats)i.next();
            HashMap omap=new HashMap();
            String cvalue=new PcatConstants().getFieldName(pc.getPcatvalue());
            omap.put("cat",cvalue);
            omap.put("desc",pc.getPcatdesc());
            omap.put("catid",pc.getPcatid());
            if (cvalue.equals(pcatshow)) {
               omap.put("checked","checked");
            } else {
               omap.put("checked"," ");
            }
            optlist.add(omap);
         }
         // make category list available to options controller
         auth.setSessionAttribute("optionsobject",optlist);

         // pull permissions from database
         if (pcatshow.equals("ALL")) {
            l=getMainDAO().execQuery("select perm from Permissions perm join fetch perm.permissioncats " +
                                             "where perm.isdeleted!=true " +
                                             "order by perm.permissioncats.pcatorder,perm.permorder "
                                    );
         } else {
            l=getMainDAO().execQuery("select perm from Permissions perm join fetch perm.permissioncats " +
                                             "where perm.isdeleted!=true and perm.permissioncats.pcatvalue=:pcatshow " +
                                             "order by perm.permissioncats.pcatorder,perm.permorder "
                                             ,new String[] { "pcatshow" }
                                             ,new Object[] { (Integer)new PcatConstants().getFieldValue(pcatshow) }
                                    );
         } 
         ArrayList newlist=new ArrayList();
         i=l.iterator();
         while (i.hasNext()) {
            com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)i.next();
            HashMap map=new HashMap();
            map.put("permissions",p);
            map.put("pcatvalue",new PcatConstants().getFieldName(p.getPermissioncats().getPcatvalue()));
            map.put("pcatscope",new PcatscopeConstants().getFieldName(p.getPermissioncats().getPcatscope()));
            newlist.add(map);
         }
         ArrayListResult result=new ArrayListResult();
         result.setList(newlist);
         this.getSession().setAttribute("iframeresult",result);
         mav.addObject("status","OK");
         return mav;
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }
   
}


