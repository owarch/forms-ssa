
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * DatatableUtilController.java - DataTable utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.sql.Timestamp;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.mla.html.table.*;
import com.oreilly.servlet.multipart.*;

public class ExportDatatableController extends FORMSController {
 
   String spath="";

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Use class-name based view
      mav=new ModelAndView("ExportDatatable");

      try {

         HttpServletRequest request=this.getRequest();
         spath=request.getServletPath();

         String dtdefid=getRequest().getParameter("dtdefid");

         if (spath.indexOf("exportdatatable.")>=0 && dtdefid!=null ) {
            mav.addObject("status","QUERY");
            getAuth().setValue("exportid",dtdefid);
            return mav;

         } else if (spath.indexOf("exportdatatableselect.")>=0) {
          
            return getSelectList(mav);

         } else if (spath.indexOf("exportdatatablesubmit.")>=0) {
          
            return exportDatatableSubmit(mav,(String)getRequest().getParameter("exporttype"));

         }
         // add new site interface
         else {
            out.println("BAD REQUEST! - " + spath);
            return null;
         }
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   /**
    * Perform actual export
    */
   private ModelAndView exportDatatableSubmit(ModelAndView mav,String exportType) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String dtdefid=getAuth().getValue("exportid");

      // Make sure user has permission to read all records
      if (!(getPermHelper().hasFormPerm(dtdefid,"READOWN") && getPermHelper().hasFormPerm(dtdefid,"READOTHER"))) {
         safeRedirect("permission.err");
      }

      Datatabledef form=(Datatabledef)getMainDAO().execUniqueQuery(
         "select f from Datatabledef f where f.dtdefid=" + dtdefid
         );

      // build list of field names
      List<String> varlist=getMainDAO().execQuery(
         "select d.columnname from Datatablecoldef d where d.datatabledef.dtdefid=" + dtdefid + 
         " order by d.columnorder" 
         );
      List<Object[]> typelist=getMainDAO().execQuery(
         "select d.fieldname,d.value from Dtfieldattrs d where d.datatabledef.dtdefid=" + dtdefid + 
             " and d.attr='VT'" 
         );

      // create list of identifying fields if requested
      List<String> hipaalist=new ArrayList();   
      if (exportType.equalsIgnoreCase("deidentified")) {
         hipaalist=getMainDAO().execQuery(
            "select d.fieldname from Dtfieldattrs d where d.datatabledef.dtdefid=" + dtdefid + 
             " and d.attr='HIPAA' and d.value='1'" 
            );
      }
      // create list of selected fields if requested
      List<String> selectList=new ArrayList();
      if (exportType.equalsIgnoreCase("select")) {
         selectList=Arrays.asList(getRequest().getParameter("fieldlist").toString().split(","));
         // remove rows from varlist based on selected list
         Iterator<String>viter=varlist.iterator();
         while (viter.hasNext()) {
            String varname=viter.next();
            Iterator<String> siter=selectList.iterator();
            boolean hasmatch=false;
            while (siter.hasNext()) {
               String comparename=siter.next();
               if (varname.equalsIgnoreCase(comparename)) {
                  hasmatch=true;
               }
            }
            if (!hasmatch) {
               viter.remove();
            }
         }
      }

      List<Datatablerecords> records;

      StringBuilder queryString=new StringBuilder(
         "select distinct d from Datatablerecords d join fetch d.datatablevalues " +
            " where d.datatabledef.dtdefid=" + dtdefid + " and d.isaudit!='T'"
         );   
   
      records=getMainDAO().execQuery(queryString.toString());
   
      int nRows=records.size();
      int nCols=varlist.size();

      Iterator<String> variter=varlist.iterator();
      // build table array (use array for performance)
      int i=0;
      String[][] tablearray=new String[nRows][nCols];
      Iterator<Datatablerecords> reciter=records.iterator();
      int r=0;
      while (reciter.hasNext()) {
         Datatablerecords record=reciter.next();
         variter=varlist.iterator();
         i=0;
         while (variter.hasNext()) {
            String colname=variter.next().replaceAll(" &#9660;","").replaceAll(" &#9650;","");
            String colvalue;
            Iterator<Datatablevalues> valiter=record.getDatatablevalues().iterator();
            while (valiter.hasNext()) {
               Datatablevalues value=valiter.next();
               String tdString;
               if (value.getVarname().equalsIgnoreCase(colname)) {
                  //colvalue=value.getVarvalue().replace("<","&lt;").replace(">","&gt;");
                  colvalue=value.getVarvalue();
                  tablearray[r][i]=colvalue;
                  break;
               }
            }
            if (tablearray[r][i]==null) {
               tablearray[r][i]="";
            }
            i++;
         }
         r++;
      }

      // Write array to StringBuilder in csv format

      String eol=System.getProperty("line.separator");

      StringBuilder sb=new StringBuilder();
      variter=varlist.iterator();
      while (variter.hasNext()) {
         sb.append("\""  + variter.next().replaceAll("\"","\"\"") + "\"");
         if (variter.hasNext()) {
            sb.append(",");
         } else {
            sb.append(eol);
         }
      }
      // first, write out field names
      // then values
      for (r=0;r<nRows;r++) {
         for (i=0;i<nCols;i++) {
            if (tablearray[r][i].length()>0) {
               if (!isIdentifyingField(varlist.get(i),hipaalist)) {
                  sb.append("\""  + tablearray[r][i].replaceAll("\"","\"\"") + "\"");
               } else {
                  sb.append("\"XXXX\"");
               }
            } else if (isIdentifyingField(varlist.get(i),hipaalist)) {
               sb.append("\"XXXX\"");
            }
            if (i<(nCols-1)) {
               sb.append(",");
            } else {
               sb.append(eol);
            }
         }
      }

      HttpServletResponse response=getResponse();
      response.setContentType("application/csv");
      response.setContentLength(sb.length());
      // Send content as an attachment (generates open/save dialog)
      response.setHeader("Content-Disposition","attachment;filename=" + form.getDtacr() + ".csv");
      // reverse FORMSController-set headers
      response.setHeader("Cache-Control","public"); 
      response.setHeader("Pragma","cache"); 
      response.setDateHeader ("Expires", 5000); 
      out.write(sb.toString());
      out.flush();
      return null;

   }

   private boolean isIdentifyingField(String varname,List<String> hipaalist) {
      if (hipaalist.size()<1) 
         return false;
      Iterator<String> hIter=hipaalist.iterator();
      while (hIter.hasNext()) {
         String hName=hIter.next();
         if (hName.toLowerCase().equalsIgnoreCase(varname.toLowerCase())) {
            return true;
         }
      }
      return false;
   }

   /**
    *  Pull list of fieldnames, indicating identifying fields
    */
   public ModelAndView getSelectList(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String dtdefid=getAuth().getValue("exportid");

      Datatabledef form=(Datatabledef)getMainDAO().execUniqueQuery(
         "select f from Datatabledef f where f.dtdefid=" + dtdefid
         );

      // build list of field names
      List<String> fieldlist=getMainDAO().execQuery(
         "select d.columnname from Datatablecoldef d where d.datatabledef.dtdefid=" + dtdefid + 
         " order by d.columnorder" 
         );

      // build list of identifying fields if requested
      List<String> hipaalist=getMainDAO().execQuery(
            "select d.fieldname from Dtfieldattrs d where d.datatabledef.dtdefid=" + dtdefid + 
             " and d.attr='HIPAA' and d.value='1'" 
            );
      ListIterator<String> i=fieldlist.listIterator();
      while (i.hasNext()) {
         String varname=i.next();
         if (isIdentifyingField(varname,hipaalist)) {
            varname=varname + "*";
            i.set(varname);
            
         }
      }
      mav.addObject("status","SELECT");
      mav.addObject("fieldlist",fieldlist);
      return mav;

   }         

}

