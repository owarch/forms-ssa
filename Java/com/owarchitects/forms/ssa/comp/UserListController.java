 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * UserListController.java - Local User Administration Interface
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;

public class UserListController extends FORMSController {

   private boolean showsite;

   public ModelAndView submitRequest() throws FORMSException {

      try {

         FORMSAuth auth=this.getAuth();

         ModelAndView mav=new ModelAndView();
         HttpServletResponse response=this.getResponse();
         PrintWriter out=this.getWriter();

         String sortcolumn=request.getParameter("SORTCOLUMN");
         String showdeleted=request.getParameter("showdeleted");
         boolean showdeletedusers=new Boolean(auth.getValue("showdeletedusers")).booleanValue();

         if (auth.getValue("sortid")==null || !auth.getValue("sortid").equals("USERLIST")) {
            auth.setValue("sortid","USERLIST");
            auth.setValue("sortcolumn","username");
            auth.setValue("sortdir","asc");
         }

         if (sortcolumn==null && showdeleted==null) {

            // Process Data

            List l=null;
            
            if (showdeletedusers) {
               l=getMainDAO().execQuery(" select lu from Localusers as lu order by upper(lu." + auth.getValue("sortcolumn") + ") " + auth.getValue("sortdir") );
            } else {
               l=getMainDAO().execQuery(" select lu from Localusers as lu where lu.isdeleted!=true order by upper(lu." + auth.getValue("sortcolumn") + ") " + auth.getValue("sortdir") );
            }
           
            ListResult result=new ListResult();
            result.setList(l);
            result.setStatus("OK");

            this.getSession().setAttribute("iframeresult",result);

            mav.addObject("status","OK");
            return mav;
   
         }
         else if (sortcolumn!=null) {

            String oldsortcol=auth.getValue("sortcolumn");
            String oldsortdir=auth.getValue("sortdir");
            if (oldsortcol.equalsIgnoreCase(sortcolumn)) {
               if (oldsortdir.equalsIgnoreCase("asc")) {
                  auth.setValue("sortdir","desc");
               } else {
                  auth.setValue("sortdir","asc");
               }
            } else {
               auth.setValue("sortcolumn",sortcolumn);
               auth.setValue("sortdir","asc");
            }

            mav.addObject("status","SORT");
            return mav;

         }
/*         
         else if (showdeleted!=null) {

            if (showdeletedusers) {
               auth.setValue("showdeletedusers","false");
            } else {
               auth.setValue("showdeletedusers","true");
            }   

            mav.addObject("status","OPTIONS");
            return mav;

         }
*/         
         return null;
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

}

