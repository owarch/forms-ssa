 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * InstUtilController.java - Study utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;


public class InstUtilController extends FORMSController {

   boolean isbadrequest;

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // check permissions
      if (!hasGlobalPerm(PcatConstants.SYSTEM,"INSTMGT")) {
         safeRedirect("permission.err");
         return null;
      }

      // Use class-name based view
      mav=new ModelAndView("InstUtil");
      isbadrequest=true;
      String spath=request.getServletPath();

      try {

         if (spath.indexOf("addlink.")>=0) {

            isbadrequest=false;
            mav.addObject("status","ADDLINK");
            return mav;

         } 

         else if (spath.indexOf("addlinksubmit.")>=0) {

            return addLinkSubmit();

         }

         else if (spath.indexOf("editattr.")>=0) {

            isbadrequest=false;
            mav.addObject("status","EDITATTR");
            return mav;

         } 

         else if (spath.indexOf("editattrsubmit.")>=0) {

             return editAttrSubmit();

         } 

         else if (spath.indexOf("createkey.")>=0) {

            isbadrequest=false;
            mav.addObject("status","CREATEKEY");
            return mav;

         } 

         else if (spath.indexOf("createkeysubmit.")>=0) {

            return createKeySubmit();

         } 

         else if (spath.indexOf("importkey.")>=0) {

            isbadrequest=false;
            mav.addObject("status","IMPORTKEY");
            return mav;

         } 

         else if (spath.indexOf("importkeysubmit.")>=0) {

            return importKeySubmit();

         } 

         else if (spath.indexOf("importconnectstring.")>=0) {

            isbadrequest=false;
            mav.addObject("status","IMPORTCONNECTSTRING");
            return mav;

         } 

         else if (spath.indexOf("importconnectstringsubmit.")>=0) {

            return importConnectstringSubmit();

         } 

         else if (spath.indexOf("commstatus.")>=0) {

            isbadrequest=false;
            mav.addObject("status","COMMSTATUS");
            return mav;

         } 

         else if (spath.indexOf("commstatussubmit.")>=0) {

            return commStatusSubmit();

         } 

         if (!isbadrequest) {
            mav.addObject("status","OK");
            return mav;
         } else {
            return null;
         }
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   private ModelAndView addLinkSubmit() throws FORMSException {

      try {

         if (request.getContentType().toLowerCase().indexOf("multipart")>=0) {
   
            isbadrequest=false;
            MultipartParser mp = new MultipartParser(request, 1*1024*1024); 
            Part part;
            String inString=""; 
            String acronym=""; 
            String description=""; 
            String codeword=""; 
            String url=""; 
            String instkey=""; 
            String connectstring=""; 
            String createkey=""; 
   
            while ((part = mp.readNextPart()) != null) {
               String name = part.getName();
               if (part.isParam()) {
                  // it's a parameter part
                  ParamPart paramPart = (ParamPart) part;
                  if (name.equalsIgnoreCase("acronym")) {
                     acronym = paramPart.getStringValue();
                  } else if (name.equalsIgnoreCase("description")) {
                     description = paramPart.getStringValue();
                  } else if (name.equalsIgnoreCase("codeword")) {
                     codeword = paramPart.getStringValue();
                  } else if (name.equalsIgnoreCase("url")) {
                     url = paramPart.getStringValue();
                  } else if (name.equalsIgnoreCase("createkey")) {
                     createkey = paramPart.getStringValue();
                  }   
               }
               else if (part.isFile()) {
                  // it's a file part
                  FilePart filePart = (FilePart) part;
                  String fileName = filePart.getFileName();
                  if (fileName != null) {
                     fileName=fileName.trim();
                     ByteArrayOutputStream baos = new ByteArrayOutputStream();
                     long size = filePart.writeTo(baos);
                     if (part.getName().equalsIgnoreCase("instkey")) {
                        instkey=baos.toString("ISO-8859-1");
                     } else if (part.getName().equalsIgnoreCase("connectstring")) {
                        connectstring=baos.toString("ISO-8859-1");
                     }   
                  }
               }
            } 
            out.flush();
   
            // Invoke addNewInst method
            JoinInstParms parms=new JoinInstParms();
            parms.setAcronym(acronym);
            parms.setDescription(description);
            parms.setCodeword(codeword);
            parms.setConnectstring(connectstring);
            parms.setUrl(url);
            parms.setInstkey(instkey);
            parms.setCreatekey(createkey);
            StatusOnlyResult result=addNewInst(parms);
            this.getSession().setAttribute("iframeresult",result);
   
            String status=result.getStatus();
   
            // Direct output based on result
            if (status.equalsIgnoreCase("ADDEDINST") || 
                status.equalsIgnoreCase("NOKEYIMPORT") ||
                status.equalsIgnoreCase("DUPLICATE")
                ) {
   
               mav.addObject("status",status);
               return mav;
      
            } else {
      
               return null;
   
            }
   
         } 

         return null;

      } catch (Exception e) {

        try {
           //this.releaseHibernateSession();
        } catch (Exception e2) { }

        throw new FORMSException("FORMS Service exception:  " + Stack2string.getString(e));

      }

   }
   
   private StatusOnlyResult addNewInst(JoinInstParms parms) throws FORMSException {

      StatusOnlyResult result=new StatusOnlyResult();

      try {

        String acronym=parms.getAcronym();
        String description=parms.getDescription();
        String codeword=parms.getCodeword();
        String connectstring=parms.getConnectstring();
        String url=parms.getUrl();
        String createkey=parms.getCreatekey();
        String instkey=parms.getInstkey();

        LinkedinstDAO linkedinstDAO=(LinkedinstDAO)this.getContext().getBean("linkedinstDAO");
        EmbeddedinstDAO embeddedinstDAO=(EmbeddedinstDAO)this.getContext().getBean("embeddedinstDAO");

        //this.bindHibernateSession();

        List l=null;

        // check linkedinst & embeddedinst to ensure new record doesn't duplicate
        // important fields
        boolean havedup=false;
        l=linkedinstDAO.getTable("Linkedinst");
        Iterator i=l.iterator();
        while (i.hasNext()) {
           // NOTE:  May retrieve either linkedinst or studyrights object
           Linkedinst lsts=(Linkedinst)i.next();
           if (
                lsts.getAcronym().trim().equalsIgnoreCase(acronym.trim()) ||
                lsts.getDescription().trim().equalsIgnoreCase(description.trim()) ||
                lsts.getUrl().trim().equalsIgnoreCase(url.trim())
              ) {
              havedup=true;
           }
        }   
        l=embeddedinstDAO.getTable("Embeddedinst");
        i=l.iterator();
        while (i.hasNext()) {
           // NOTE:  May retrieve either embeddedinst or studyrights object
           Embeddedinst ests=(Embeddedinst)i.next();
           if (
                ests.getAcronym().trim().equalsIgnoreCase(acronym.trim()) ||
                ests.getCodeword().trim().equalsIgnoreCase(codeword.trim()) 
              ) {
              havedup=true;
           }
        }   

        //this.releaseHibernateSession();

        if (havedup) {

           result.setStatus("DUPLICATE");

        } else {

           // Create installation key pair, if requested.
           if (createkey.equals("1")) {
              EncryptUtil.createKeyPair(this.getSession(),"inst_loc_" + acronym.trim(),"private");
           }

           // Import remote installation key if supplied.
           boolean importok=true;
           if (instkey!=null && instkey.length()>0) {
              try {
                 EncryptUtil.importPublicKey(this.getSession(),instkey,"inst_rem_" + acronym);
              } catch (Exception kie) {
                 importok=false;
              }
           } 

           // Append linkedinst record
           Linkedinst ls=new Linkedinst();
           ls.setAcronym(acronym);
           ls.setDescription(description);
           ls.setUrl(url);
           ls.setIsdisabled(false);
           linkedinstDAO.saveOrUpdate(ls);

           // Append embeddedinst record
           Embeddedinst es=new Embeddedinst();
           es.setAcronym(acronym);
           es.setCodeword(codeword);
           embeddedinstDAO.saveOrUpdate(es);

           // Append allinst record
           Allinst as=new Allinst();
           as.setIslocal(false);
           as.setLinkedinst(ls);

           getMainDAO().saveOrUpdate(as);
           
           if (importok) {
              result.setStatus("ADDEDINST");
           } else {
              result.setStatus("NOKEYIMPORT");
           }

        }
        return result;

      } catch (Exception e) {

        try {
           //this.releaseHibernateSession();
        } catch (Exception e2) { }

        throw new FORMSException("FORMS Service exception:  " + Stack2string.getString(e));

      }

   }

   private ModelAndView editAttrSubmit() throws FORMSException {

      String acronym=request.getParameter("acronym");
      String description=request.getParameter("description");
      String codeword=request.getParameter("codeword");
      String url=request.getParameter("url");
      String linstid=request.getParameter("linstid");

      LinkedinstDAO linkedinstDAO=(LinkedinstDAO)this.getContext().getBean("linkedinstDAO");
      EmbeddedinstDAO embeddedinstDAO=(EmbeddedinstDAO)this.getContext().getBean("embeddedinstDAO");
      Linkedinst li=linkedinstDAO.getRecord(new Long(linstid).longValue());
      Embeddedinst ei=embeddedinstDAO.getRecordByAcronym(li.getAcronym());
      if (li!=null && ei!=null) {
         li.setAcronym(acronym);
         li.setDescription(description);
         li.setUrl(url);
         linkedinstDAO.saveOrUpdate(li);
         ei.setAcronym(acronym);
         ei.setCodeword(codeword);
         embeddedinstDAO.saveOrUpdate(ei);
         mav.addObject("status","EDITATTROK");
      } else {
         mav.addObject("status","EDITATTRNOTOK");
      }   
      return mav;

   }

   private ModelAndView createKeySubmit() throws FORMSException {

      try {

         String acronym=request.getParameter("acronym");
         String linstid=request.getParameter("linstid");
         try {
            // Create keypair
            EncryptUtil.createKeyPair(this.getSession(),"inst_loc_" + acronym.trim(),"private");
            mav.addObject("status","KEYCREATED");
         } catch (Exception ie) {
            mav.addObject("status","NOTCREATED");
         }
         return mav;

      } catch (Exception e) {

        throw new FORMSException("FORMS Service exception:  " + Stack2string.getString(e));

      }

   }

   private ModelAndView importKeySubmit() throws FORMSException {

      try {

         MultipartParser mp = new MultipartParser(request, 1*1024*1024); 
         Part part;
         String instkey=""; 

         // Read file

         String acronym="";
         while ((part = mp.readNextPart()) != null) {
            String name = part.getName();
            if (part.isParam()) {
               // it's a parameter part
               ParamPart paramPart = (ParamPart) part;
               if (name.equalsIgnoreCase("acronym")) {
                  acronym = paramPart.getStringValue();
               } 
            } else if (part.isFile()) {
               // it's a file part
               FilePart filePart = (FilePart) part;
               String fileName = filePart.getFileName();
               if (fileName != null) {
                  fileName=fileName.trim();
                  ByteArrayOutputStream baos = new ByteArrayOutputStream();
                  long size = filePart.writeTo(baos);
                  instkey=baos.toString("ISO-8859-1");
               }
            } 
         } 

         // Import to keystore
         try {
            EncryptUtil.importPublicKey(this.getSession(),instkey,"inst_rem_" + acronym);
            mav.addObject("status","KEYIMPORTED");
         } catch (Exception ei) {
            mav.addObject("status","NOTIMPORTED");
         }

         return mav;

      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   private ModelAndView importConnectstringSubmit() throws FORMSException {

      try {

         MultipartParser mp = new MultipartParser(request, 1*1024*1024); 
         Part part;
         String connectstring=""; 

         // Read file

         String acronym="";
         while ((part = mp.readNextPart()) != null) {
            String name = part.getName();
            if (part.isParam()) {
               // it's a parameter part
               ParamPart paramPart = (ParamPart) part;
               if (name.equalsIgnoreCase("acronym")) {
                  acronym = paramPart.getStringValue();
               } 
            } else if (part.isFile()) {
               // it's a file part
               FilePart filePart = (FilePart) part;
               String fileName = filePart.getFileName();
               if (fileName != null) {
                  fileName=fileName.trim();
                  ByteArrayOutputStream baos = new ByteArrayOutputStream();
                  long size = filePart.writeTo(baos);
                  connectstring=baos.toString("ISO-8859-1");
               }
            } 
         } 

         // Update linkedinst with connectstring
         try {

            LinkedinstDAO linkedinstDAO=(LinkedinstDAO)this.getContext().getBean("linkedinstDAO");
            Linkedinst li=linkedinstDAO.getRecord(acronym);
            if (li!=null) {
               li.setConnectstring(connectstring);
               linkedinstDAO.saveOrUpdate(li);
               mav.addObject("status","CONNECTSTRINGIMPORTED");
            } else {

               mav.addObject("status","CSNOTIMPORTED");
            }
         } catch (Exception ei) {
            mav.addObject("status",Stack2string.getString(ei));
         }

         return mav;

      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   private ModelAndView commStatusSubmit() throws FORMSException {

      String isdisabled=request.getParameter("isdisabled");
      String linstid=request.getParameter("linstid");

      LinkedinstDAO linkedinstDAO=(LinkedinstDAO)this.getContext().getBean("linkedinstDAO");
      Linkedinst li=linkedinstDAO.getRecord(new Long(linstid).longValue());
      if (li!=null) {
         li.setIsdisabled(new Boolean(isdisabled));
         linkedinstDAO.saveOrUpdate(li);
         mav.addObject("status","COMMSTATUSOK");
      } else {
         mav.addObject("status","COMMSTATUSNOTOK");
      }   
      return mav;

   }

}


