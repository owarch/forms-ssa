 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 * PatientFormsServiceParms.java - input parameters for SSA forms.services requiring only system parameters
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import java.util.*;
import java.io.*;

public class PatientFormsServiceParms extends FORMSSsaServiceParms implements Serializable {

   String whereclause;
   List list;
   int varcount;

   //

   public void setWhereclause(String whereclause) {
      this.whereclause=whereclause;
   }

   public String getWhereclause() {
      return whereclause;
   }

   //

   public void setList(List list) {
      this.list=list;
   }

   public List getList() {
      return list;
   }

   //

   public void setVarcount(int varcount) {
      this.varcount=varcount;
   }

   public int getVarcount() {
      return varcount;
   }

   //

}

