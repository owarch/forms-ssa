 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * RetrieveEncryptedValueController.java - Retrieve encrypted value for string using FORMS Internal Key
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import javax.servlet.http.*;
import org.apache.commons.lang.ArrayUtils;

public class RetrieveEncryptedValueController extends FORMSController {

   public ModelAndView submitRequest() throws FORMSException {

      try {

         ModelAndView mav=new ModelAndView();
         HttpServletRequest request=this.getRequest();
         HttpServletResponse response=this.getResponse();
         PrintWriter out=this.getWriter();

         // only administrators can use this controller
         if (!getAuth().getValue("rightslevel").equals("ADMIN")) {
            safeRedirect("permission.err");
            return null;
         }

         if (request.getParameter("pstring")==null) {
            mav.addObject("status","OK");
         } else {
            mav.addObject("rstring",EncryptUtil.encryptString((String)request.getParameter("pstring"),
               SessionObjectUtil.getInternalKeyObj(getSession())));
            mav.addObject("status","FINISH");
         }
         return mav;

      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

}

