 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.io.*;
import java.util.*;

public class IltableDwr extends FORMSDwr {

   // Reset current invalid login count by IP Address

   public boolean resetCount(String ipaddr) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Verify authorization, then return public key as string
      FORMSAuth auth=this.getAuth();
      if (auth.isAuth(this.getRequest(),this.getResponse()) && auth.getValue("rightslevel").equalsIgnoreCase("ADMIN")) {

         List l=getEmbeddedDAO().execQuery(
               " select il from Invalidlogins as il " +
                  " where il.ipaddr=:ipaddr ", 
                  new String[] { "ipaddr" } ,
                  new Object[] { ipaddr }
                                                       );
   
         if (l.size()==1) {

            Invalidlogins il=(Invalidlogins)l.get(0);
            il.setCurrcount(new Long("0").longValue());
            getEmbeddedDAO().saveOrUpdate(il);
            return true;
            
         } else {
          
            return false;

         }
   
      } else {

         return false;

      }
      
   }

}




