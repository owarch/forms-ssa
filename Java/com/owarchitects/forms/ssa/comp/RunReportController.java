 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * RunReportController.java - Download Report File to Client
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.sql.Blob;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
//import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
//import org.springframework.transaction.support.TransactionSynchronizationManager;
//import org.mla.html.table.*;
import org.json.simple.JSONObject;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

public class RunReportController extends FORMSSsaServiceClientController {

   com.sas.sasserver.submit.SubmitInterface sas;
   com.sas.sasserver.sclfuncs.SclfuncsV3Interface iscl;

   public static final int SAS=1;
   public static final int R=2;

   private int progType;

   public ModelAndView submitRequest() throws FORMSException {

      try {

         FORMSAuth auth=getAuth();
   
         // Immediately expire headers
         response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
         response.setHeader("Pragma","no-cache"); //HTTP 1.0
         response.setDateHeader ("Expires", -1); //prevents caching at the proxy server

         if (auth.getValue("islocal").toUpperCase().equals("TRUE")) {

            /////////////////////////////
            // LOCAL REPORT SUBMISSION //
            /////////////////////////////
            if (getRequest().getServletPath().indexOf("runreport.do")>=0) {

               String desc=(String)getMainDAO().execUniqueQuery(
                  "select r.description from Reportinfo r where r.reportinfoid=" + auth.getValue("reportinfoid")
                  );
               List slist=getMainDAO().execQuery(
                  "select s from Reportstore s join fetch s.reportinfo join fetch s.reportinfo.supportedfileformats "
                   + "where s.reportinfo.reportinfoid=" + auth.getValue("reportinfoid")
                  );
               Iterator i=slist.iterator();
               Reportstore pstore=null;
               Reportstore hstore=null;
               Reportstore lstore=null;
               while (i.hasNext()) {
                  Reportstore store=(Reportstore)i.next();
                  if (store.getContenttype()==ReportConstants.PROGFILE) {
                     pstore=store;
                     auth.setObjectValue("pstore",pstore);
                  } else if (store.getContenttype()==ReportConstants.HTMLFILE) {
                     hstore=store;
                     auth.setObjectValue("hstore",hstore);
                  } else if (store.getContenttype()==ReportConstants.FORMLETTER) {
                     lstore=store;
                     auth.setObjectValue("lstore",lstore);
                  }
               }

               String contenttype=pstore.getReportinfo().getSupportedfileformats().getContenttype();
               if (contenttype.indexOf("-sas-syntax")>=0) {
                  progType=SAS;
                  sas=SessionObjectUtil.getSasConnection(getRequest(),getResponse()).getSubmit();
                  iscl=SessionObjectUtil.getSasConnection(getRequest(),getResponse()).getIscl();
                  iscl.symput("m_status","INIT");
               } else if (contenttype.indexOf("-r-syntax")>=0) {
                  progType=R;
               } else {
                  throw new FORMSException("Reporting Program Type Is Not Supported");
               }

               ObjectResult result=new ObjectResult();
               if (hstore!=null) {
                  result.setStatus("CONTINUE");
                  result.setObject(velocityResolve(hstore.getReportcontent()));
   
               } else {
                  result.setStatus("DONE");
                  if (lstore==null) {
                     result.setObject(execReport(pstore));
                     auth.setObjectValue("hstore",null);
                     auth.setObjectValue("pstore",null);
                     auth.setObjectValue("lstore",null);
                  } else {
                     execReport(pstore);
                     processLetter(lstore);
                     downloadLetter();
                     auth.setObjectValue("hstore",null);
                     auth.setObjectValue("pstore",null);
                     auth.setObjectValue("lstore",null);
                     return null;
                  }
               }
               getSession().setAttribute("iframeresult",result);
               mav.addObject("description",desc);
               mav.addObject("resourcetype",auth.getValue("resourcetypestr"));
               mav.addObject("status","OK");
               return mav;

            } else if (getRequest().getServletPath().indexOf("getletter.do")>=0) {

               downloadLetter();
               return null;
   
            } else {

               String resultstr=execReport((Reportstore)auth.getObjectValue("pstore"));
               if (progType==SAS) {
                  String status=iscl.symget("m_status");    
                  mav.addObject("status",status);
                  if (status.equalsIgnoreCase("INIT") || status.equalsIgnoreCase("DONE")) {
                     // execute sas process
                     // clear stored objects
                     Reportstore lstore=null;
                     try {
                         lstore=(Reportstore)auth.getObjectValue("lstore");
                     } catch (Exception lse) {
                     }
                     if (lstore!=null) {
                        processLetter(lstore);
                        mav.addObject("status","lettercreated");
                        auth.setObjectValue("hstore",null);
                        auth.setObjectValue("pstore",null);
                        auth.setObjectValue("lstore",null);
                        return mav;
                     }
                     auth.setObjectValue("hstore",null);
                     auth.setObjectValue("pstore",null);
                     auth.setObjectValue("lstore",null);
                     mav.addObject("object",resultstr);
                  } else {
                     // display next round of the html process
                     // execute sas process
                     mav.addObject("object",velocityResolve(((Reportstore)auth.getObjectValue("hstore")).getReportcontent()));
                  }
               } else {
                  mav.addObject("status","DONE");
                  auth.setObjectValue("hstore",null);
                  auth.setObjectValue("pstore",null);
                  auth.setObjectValue("lstore",null);
                  mav.addObject("object",resultstr);
               }
               return mav;
   
            }

         } else {

            //////////////////////////////
            // REMOTE REPORT SUBMISSION //
            //////////////////////////////
   
            Linkedinst linst=(Linkedinst)getMainDAO().execUniqueQuery(
               "select s.linkedinst from Linkedstudies s where s.lstdid=" + getAuth().getValue("lstdid")
               );

            if (getRequest().getServletPath().indexOf("runreport.do")>=0) {

               String reportinfoid=getAuth().getValue("reportinfoid");

               RunReportResult result=(RunReportResult)submitServiceRequest(linst,"runReportServiceTarget","runReport",
                  auth.getValue("reportinfoid"));

               if (result==null || result.getStatus()!=FORMSServiceConstants.OK) {
      
                   mav.addObject("status","COULDNOTPULL");
                   return mav;
                     
               }

               mav.addObject("resourcetype",auth.getValue("resourcetypestr"));
               if ((new Integer(auth.getValue("resourcetype")).intValue())==ResourceTypeConstants.FORMLETTER && 
                     (result.getRunstatus().equalsIgnoreCase("DONE"))) {
                  getAuth().setValue("formletter",(String)result.getObject());
                  downloadLetter();
                  return null;
               } else {
                  getSession().setAttribute("iframeresult",result);
                  mav.addObject("description",result.getDescription());
                  mav.addObject("status","OK");
                  return mav;
               }   

            } else if (getRequest().getServletPath().indexOf("getletter.do")>=0) {

               downloadLetter();
               return null;
   
            } else {

               // PASS REQUEST PARAMETERS TO SAS AS MACRO VARIABLES
               Enumeration e = request.getParameterNames();
               StringBuilder pb=new StringBuilder();
               HashMap passmap=new HashMap();
               while (e.hasMoreElements()) {
                   String name = (String)e.nextElement();
                   // For multiple selection items, just returm space delimited set of values,
                   // otherwise just return the single value.
                   String[] values = request.getParameterValues(name);
                   passmap.put(name,values);
               }

               RunReportResult result=(RunReportResult)submitServiceRequest(linst,"runReportServiceTarget","submitReport", passmap);

               if (result==null || result.getStatus()!=FORMSServiceConstants.OK) {
      
                   mav.addObject("status","COULDNOTPULL");
                   return mav;
                     
               }

               mav.addObject("resourcetype",auth.getValue("resourcetypestr"));
               //getSession().setAttribute("iframeresult",result);
               if ((new Integer(auth.getValue("resourcetype")).intValue())==ResourceTypeConstants.FORMLETTER && 
                     (result.getRunstatus().equalsIgnoreCase("DONE") || result.getRunstatus().equalsIgnoreCase("INIT"))) {
                  mav.addObject("status","lettercreated");
                  getAuth().setValue("formletter",(String)result.getObject());
                  return mav;
               } else {
                  mav.addObject("object",(String)result.getObject());
                  mav.addObject("description",result.getDescription());
                  mav.addObject("status",result.getRunstatus());
                  return mav;
               }

            }

         } 

      } catch (Exception e) { 

         mav.addObject("status","COULDNOTPULL");
         return mav;
      }

   }

   public String execReport(Reportstore pstore) throws FORMSException {

      // call stat software specific methods based on program-type
      if (progType==SAS) {
         return execSASReport(pstore);
      } else if (progType==R) {
         return execRReport(pstore);
      } else {
         throw new FORMSException("Reporting Program Type is not Supported");
      }

   }

   // SAS Report Execution
   public String execSASReport(Reportstore pstore) {

      try {

         // INITIALIZE PREPROCESSING MACRO VARIABLES SO WE DONT PICK UP OLD ONES OR HAVE JAVA NULL VALUES
         iscl.symput("m_diagout","N");
         iscl.symput("m_socketout","Y");
   
         // PASS REQUEST PARAMETERS TO SAS AS MACRO VARIABLES
         Enumeration e = request.getParameterNames();
         StringBuilder pb=new StringBuilder();
         while (e.hasMoreElements()) {
             String name = (String)e.nextElement();
             // For multiple selection items, just returm space delimited set of values,
             // otherwise just return the single value.
             String[] values = request.getParameterValues(name);
             StringBuilder sv=new StringBuilder();
             for (int i=0;i<values.length;i++) {
                sv.append(" "+values[i]);
             }
             String value=sv.toString().substring(1);
             pb.append("<br>" + name + "=" + value);
             iscl.symput(name,value);
         }
   
         com.sas.servlet.util.SocketListener socket=new com.sas.servlet.util.SocketListener();
         int port=socket.setup();
         socket.start();
      
         // Set these in case of socket output
         iscl.symput("m_server",(java.net.InetAddress.getLocalHost()).getHostAddress());
         iscl.symput("m_port",new Integer(port).toString());
   
         String sasout=null;
      
         // Submit program text
         sas.setProgramText(velocityResolve(pstore.getReportcontent()));
   
         String socketout=iscl.symget("m_socketout").trim();
   
         if (socketout.equalsIgnoreCase("Y")) {
   
            ByteArrayOutputStream sos=new ByteArrayOutputStream();
            socket.write(sos);
      
            sasout=new String(sos.toByteArray());
   
         } else {
   
            sasout="<pre><xmp>" + sas.getLastOutputText() + "</xmp></pre>";
   
         }
   
         socket.close();
   
         if (!iscl.symget("m_diagout").trim().equals("Y")) {
            return sasout;
         } else {
            StringBuilder sb=new StringBuilder();
            sb.append("<br><pre>");
            sb.append(sas.getLastLogText());
            sb.append("<br></pre>");
            return pb.toString() + "<br>" + sasout + sb.toString();
         } 

      } catch (Exception e) {
         return "ERROR:  Could not retrieve program results from SAS server" + "<br><br>" + Stack2string.getString(e);
      }

   }

   public String execRReport(Reportstore pstore) {

       
      try {

         // PASS REQUEST PARAMETERS TO R AS GLOBAL VARIABLES
         Enumeration e = request.getParameterNames();
         StringBuilder pb=new StringBuilder();
         while (e.hasMoreElements()) {
             String name = (String)e.nextElement();
             // For multiple selection items, just returm space delimited set of values,
             // otherwise just return the single value.
             String[] values = request.getParameterValues(name);
             StringBuilder sv=new StringBuilder();
             for (int i=0;i<values.length;i++) {
                sv.append(" "+values[i]);
             }
             String value=sv.toString().substring(1).trim();
             pb.append("\n" + name + "<<-\"" + value + "\"\n");
         }
   
         FORMSRConnection rconn=new FORMSRConnection(getRequest(),getResponse());
      
         // Submit program text
         if (pb.length()>0) {
            rconn.submitProgram(pb.toString() + velocityResolve(pstore.getReportcontent()));
         } else {
            rconn.submitProgram(velocityResolve(pstore.getReportcontent()));
         }


         StringBuilder sb=new StringBuilder();
         sb.append("<html>\n");
         sb.append("<script>\n");
         sb.append("function appendText(node,txt)\n");
         sb.append("{\n");
         sb.append("  var ele=document.createElement('div');\n");
         sb.append("  ele.innerHTML=txt;\n");
         sb.append("  node.appendChild(ele);\n");
         sb.append("}\n");
         sb.append("</script>\n");
         sb.append("<body>\n");
         sb.append("<br><pre>\n");
         sb.append(rconn.getTextOutput());
         sb.append("<br></pre><br><br>\n");
         sb.append("</body>\n");
         sb.append("<script>\n");
         List imgNames=rconn.getImageOutput();
         Iterator i=imgNames.iterator();
         // User increasing setTimeout here to load images, otherwise servlet has trouble loading many images at once.   
         int ii=0;
         while (i.hasNext()) {
            ii++;
            String encImgName=EncryptUtil.encryptString((String)i.next(),
               SessionObjectUtil.getInternalKeyObj(getSession()));
            sb.append("setTimeout(\"appendText(document.getElementsByTagName('body')[0],'<br><br><img src=\\\"getimage.do?image=" + encImgName + "\\\"/>');\"," + ii + "*1000);\n");   
            if (!i.hasNext()) {
               ii++;
               sb.append("setTimeout(\"appendText(document.getElementsByTagName('body')[0],'<br><br>');\"," + ii + "*1000);\n");   
            }
         }
         sb.append("</script>\n");
         sb.append("</html>\n");
         return sb.toString();
   
      } catch (Exception e) {
         return "<br><br>ERROR:  Could not retrieve program results from R server. &nbsp;RServe listener may not be running.<br><br>";
      }

   }

   public void processLetter(Reportstore lstore) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String ltr=lstore.getReportcontent();
      try {
         // execute SAS to retrieve macro variable names
         org.apache.velocity.Template template = getVelocityTemplate("getlettervars.sas");
         StringWriter sout=new StringWriter();
         VelocityContext vcontext=new VelocityContext();
         template.merge(vcontext, sout);
         sas.setProgramText(sout.toString());
         String[] mvars=iscl.symget("m_ltrvars").split(" ");
         Iterator m=Arrays.asList(mvars).iterator();
         // Replace parameters in letter file with macro variable values
         while (m.hasNext()) {
            String mv=(String)m.next();
            ltr=ltr.replaceAll("&&" + mv.toLowerCase() + "&&",iscl.symget(mv)).replaceAll("&&" + mv.toUpperCase() + "&&",iscl.symget(mv)); 
         }
      } catch (Exception e) {
         // do nothing
      }

      getAuth().setValue("formletter",ltr);
   }

   private String velocityResolve(String ins) {
      try {
         StringWriter w=new StringWriter();
         VelocityEngine velocity=SessionObjectUtil.getVelocityEngine(session,this.getContext(),this.getAppPath());
         VelocityContext ctx=new VelocityContext();
         ctx.put("m_status",iscl.symget("m_status"));
         velocity.evaluate(ctx,w,"RunReport",ins);
         return w.toString();
         
      } catch (Exception e) {
         return ins;
      }
   }

   private void downloadLetter() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      FORMSAuth auth=getAuth();
      String ltr=auth.getValue("formletter");
      HttpServletResponse response=getResponse();
      response.setContentType("application/rtf");
      response.setContentLength(ltr.length());
      // Send content as an attachment (generates open/save dialog)
      response.setHeader("Content-Disposition","attachment;filename=letter.rtf"); 
      // reverse FORMSController-set headers
      response.setHeader("Cache-Control","public"); 
      response.setHeader("Pragma","cache"); 
      response.setDateHeader ("Expires", 5000); 
      out.write(ltr);
      auth.setValue("formletter","");
   }

}


