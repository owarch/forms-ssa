 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * AnalystUtilController.java - Form utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.mla.html.table.*;

public class AnalystUtilController extends FORMSSsaServiceClientController {
 
   String spath="";

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Use class-name based view
      mav=new ModelAndView("AnalystUtil");

      try {

         HttpServletRequest request=this.getRequest();
         spath=request.getServletPath();

         // addstudy.sutil  (add new study interface screen)
         if (spath.indexOf("removecat.")>=0) {

            if (isHaOwner() || isPermittedSys("DOCREMOVE")) {
               mav.addObject("status",removeCat());
               return mav;
            }

         }
         else if (spath.indexOf("movecat.")>=0) {
          
            if (isFileOwner() || isPermittedSys("DOCMOVE")) {
               return moveCat(mav,true);
            }   

         }
         else if (spath.indexOf("movecatsubmit.")>=0) {
          
               mav.addObject("status",moveCatSubmit());
               return mav;

         }
         else if (spath.indexOf("viewallsubmit.")>=0) {
          
            safeRedirect("analysteditcat.am");
            return null;

         }
         else if (spath.indexOf("editcat.")>=0) {

            if (isHaOwner() || isPermittedSys("DOCEDIT")) {
               Analystha ha=editCat();
               if (ha!=null) {
                  mav.addObject("status","EDITCAT");
                  mav.addObject("analystha",editCat());
                  return mav;
               }
               return null;
            }   

         }
         else if (spath.indexOf("editcatsubmit.")>=0) {

            mav.addObject("status",editCatSubmit());
            return mav;

         }
         else if (spath.indexOf("deletefile.")>=0) {

            if (isFileOwner() || isPermittedSys("DOCREMOVE")) {
               mav.addObject("status",deleteFile());
               return mav;
            }

         }
         else if (spath.indexOf("showinfo.")>=0) {

            return showInfo();

         }
         else if (spath.indexOf("editattr.")>=0) {

            if (isFileOwner() || isPermittedSys("DOCEDIT")) {
               return editAttr();
            }   

         }
         else if (spath.indexOf("editattrsubmit.")>=0) {

            mav.addObject("status",editAttrSubmit());
            return mav;

         }
         else if (spath.indexOf("addcat.")>=0) {

            if (isPermittedHa("UPLOAD") || isPermittedSys("DOCMOVE")) {
               Analystha ha=addCat();
               if (ha!=null) {
                  mav.addObject("status","ADDCAT");
                  mav.addObject("analystha",addCat());
                  return mav;
               }
               return null;
            }   

         }
         else if (spath.indexOf("addcatsubmit.")>=0) {

            mav.addObject("status",addCatSubmit());
            getAuth().setSessionObjectValue("apermlist",null);
            getAuth().setSessionObjectValue("ahapermlist",null);
            return mav;

         // Re-order categories interface
         } else if (spath.indexOf("reordercat.")>=0) {

            if (isHaOwner() || isPermittedSys("DOCEDIT")) {
               List l=reorderCat();
               mav.addObject("list",l);
               mav.addObject("status","REORDERCAT");
               return mav;
            }

         // Submit re-ordering of categories
         } else if (spath.indexOf("reordercatsubmit.")>=0) {

            mav.addObject("status",reorderCatSubmit());
            return mav;

         } else if (spath.indexOf("viewallcat.")>=0) {
          
            return viewAllCat(mav);

         }
         // add new site interface
         else {
         }
         mav.addObject("status","NOAUTH");
         return mav;
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   private boolean isPermitted(String permacr) throws FORMSException,FORMSSecurityException,FORMSKeyException {
       return getPermHelper().hasAnalystPerm(getRequest().getParameter("analystinfoid"),permacr);
   }

   private boolean isPermittedHa(String permacr) throws FORMSException,FORMSSecurityException,FORMSKeyException {
       return getPermHelper().hasAnalysthaPerm(getRequest().getParameter("analysthaid"),permacr);
   }

   private boolean isPermittedSys(String permacr) throws FORMSException,FORMSSecurityException,FORMSKeyException {
       return getPermHelper().hasAnalystSysPerm(permacr);
   }

   private boolean isFileOwner() throws FORMSException,FORMSSecurityException,FORMSKeyException {
      String analystinfoid=getRequest().getParameter("analystinfoid");
      Analystinfo info=(Analystinfo)getMainDAO().execUniqueQuery(
         "select info from Analystinfo info where analystinfoid=" + analystinfoid
         );
      if (info!=null && (info.getUploaduser()==new Long(getAuth().getValue("auserid")).longValue())) {
         return true;
      }
      return false;
   }

   private boolean isHaOwner() throws FORMSException,FORMSSecurityException,FORMSKeyException {
      String analysthaid=getRequest().getParameter("analysthaid");
      Analystha ha=(Analystha)getMainDAO().execUniqueQuery(
         "select ha from Analystha ha where analysthaid=" + analysthaid
         );
      if (ha!=null && (ha.getCreateuser()==new Long(getAuth().getValue("auserid")).longValue())) {
         return true;
      }
      return false;
   }

   // Empty session-stored analyst form lists so data are repulled
   private void clearSessionLists() {   
      try {
         getAuth().setSessionAttribute("analysthalist",new ArrayList());
         getAuth().setSessionAttribute("analystinfolist",new ArrayList());
      } catch (Exception e) { }
   }   

   private Analystha editCat() {
      String analysthaid=getRequest().getParameter("analysthaid");
      Analystha ha=(Analystha)getMainDAO().execUniqueQuery(
         "select ha from Analystha ha where analysthaid=" + analysthaid
         );
      return ha;   
   }

   private String editCatSubmit() {

      String analysthaid=getRequest().getParameter("analysthaid");
      String hadesc=getRequest().getParameter("hadesc");
      Analystha ha=(Analystha)getMainDAO().execUniqueQuery(
         "select ha from Analystha ha where analysthaid=" + analysthaid
         );

      // Make sure description doesn't already exist in category   
      List l=getMainDAO().execQuery(
         "select ha from Analystha ha where ha.panalysthaid=" + analysthaid + " and upper(ha.hadesc)=upper('" + hadesc + "')" 
         );
      if (l.size()>0) {
         return "CATNOTUNIQUE";
      }
      ha.setHadesc(hadesc);   
      getMainDAO().saveOrUpdate(ha);
      clearSessionLists();
      return "CATEDITED";
   }

   private Analystha addCat() {
      String analysthaid=getRequest().getParameter("analysthaid");
      Analystha ha=(Analystha)getMainDAO().execUniqueQuery(
         "select ha from Analystha ha where analysthaid=" + analysthaid
         );
      return ha;   
   }

   private String addCatSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      String analysthaid=getRequest().getParameter("analysthaid");
      String hadesc=getRequest().getParameter("hadesc");

      Analystha cha=(Analystha)getMainDAO().execUniqueQuery(
         "select ha from Analystha ha where analysthaid=" + analysthaid
         );

      // Make sure description doesn't already exist in category   
      List l=getMainDAO().execQuery(
         "select ha from Analystha ha where ha.panalysthaid=" + analysthaid + " and upper(ha.hadesc)=upper('" + hadesc + "')" 
         );
      if (l.size()>0) {
         return "CATNOTUNIQUE";
      }
      Integer haordr=(Integer)getMainDAO().execUniqueQuery(
         "select max(ha.haordr) from Analystha ha where ha.panalysthaid=" + analysthaid
         );
      if (haordr==null) haordr=1;
      else haordr++;
      Analystha ha=new Analystha();
      ha.setPanalysthaid(new Long(analysthaid));
      ha.setHadesc(hadesc);   
      ha.setHaordr(haordr);   
      ha.setCreateuser(new Long(getAuth().getValue("auserid")).longValue());
      getMainDAO().saveOrUpdate(ha);
      clearSessionLists();
      return "CATADDED";
   }

   // If restrictaccess==true, can only see uploadable categories
   private ModelAndView moveCat(ModelAndView mav,boolean restrictaccess) {

      try {

        Analystha ftpfha=null;
        Object moveobj=null;

        if (request.getParameter("analystinfoid")!=null) {

           // pull current analystinfo record
           Analystinfo ft=(Analystinfo)getMainDAO().execUniqueQuery(
              "select ft from Analystinfo ft " +
                 "where ft.analystinfoid=" + request.getParameter("analystinfoid")
              );   
           moveobj=ft;   
       
           if (ft==null) throw new FORMSException("Couldn't pull document/media record");   
      
           // pull record's formha parent
           ftpfha=(Analystha)getMainDAO().execUniqueQuery(
              "select ha from Analystha ha where ha.analysthaid=" + new Long(ft.getPanalysthaid()).toString()
              );
   
        } else {

           ftpfha=(Analystha)getMainDAO().execUniqueQuery(
              "select ha from Analystha ha where ha.analysthaid=" + request.getParameter("analysthaid")
              );

        }

        FORMSAuth auth=this.getAuth();
        String siteid=auth.getValue("siteid");
        String studyid=auth.getValue("studyid");

        List fha=getMainDAO().execQuery(
           "select fha from Analystha fha " +
                 "order by haordr"
           );

        if (restrictaccess) {
           Iterator i=fha.iterator();
           // remove any records for which user doesn't have upload access
           while (i.hasNext()) {
              Analystha ha=(Analystha)i.next();
              if (!(getPermHelper().hasAnalysthaPerm(new Long(ha.getAnalysthaid()).toString(),"UPLOAD") || isPermittedSys("DOCMOVE"))) {
                 i.remove();
              }
           }
        }

        Iterator i=fha.iterator();
        StringBuilder sb=new StringBuilder();
        while (i.hasNext()) {
           Analystha ha=(Analystha)i.next();
           sb.append(new Long(ha.getAnalysthaid()).toString());
           if (i.hasNext()) sb.append(",");
        }

        ////////////////////////////////////////////////////////////////////
        // Organize data into heirarchical format for building HTML table //
        ////////////////////////////////////////////////////////////////////

        // ft holds number of rows in table.  Keep this info for later
        //int tablerows=ft.size();
        int tablerows=0;

        // tlist holds table columns
        ArrayList tlist=new ArrayList();
        // alist holds items within columns
        ArrayList alist=new ArrayList();

        // pull first table column
        i=fha.iterator();
        while (i.hasNext()) {
           Analystha ha=(Analystha)i.next();
           if (ha.getPanalysthaid()==null) {
              HashMap map=new HashMap();
              map.put("obj",ha);
              map.put("nrows",0);
              map.put("ncols",1);
              // remove element from collection so we don't have to compare it again
              i.remove();
              alist.add(map);
           }
        }   
        tlist.add(alist);
        // pull subsequent columns by finding children of elements in previous columns
        for (int ii=1;ii>-1;ii++) {
           // pull elements in prior column
           ArrayList plist=(ArrayList)tlist.get(ii-1);
           alist=new ArrayList();
           Iterator i2=plist.iterator();
           boolean again=false;
           //iterate through prior column elements
           while (i2.hasNext()) {
              // find children
              HashMap pmap=(HashMap)i2.next();
              Analystha pha=(Analystha)pmap.get("obj");
              i=fha.iterator();
              boolean havechild=false;
              int prevsize=alist.size();
              while (i.hasNext()) {
                 Analystha cha=(Analystha)i.next();
                 // write child out to alist
                 if (cha.getPanalysthaid().equals(pha.getAnalysthaid())) {
                    HashMap cmap=new HashMap();
                    cmap.put("obj",cha);
                    cmap.put("nrows",0);
                    cmap.put("ncols",1);
                    // remove element from collection so we don't have to compare it again
                    i.remove();
                    alist.add(cmap);
                    again=true;
                    havechild=true;
                 }
                 
              }
              // HA-ONLY Specific Code
              if (!havechild) {
                 tablerows++;
                 pmap.put("nrows",1);
                 pmap.put("havechild","N");
              } else {
                 pmap.put("havechild","Y");
              }
           }
           if (!again) break;
           tlist.add(alist);
        }

        // add forms to appropriate category (must iterate backwards through tlist
        // since forms are attached as the right most element)
        for (int ii=(tlist.size()-1);ii>=0;ii--) {
           // pull column arraylist
           ArrayList plist=(ArrayList)tlist.get(ii);
           Iterator i2=plist.iterator();
           while (i2.hasNext()) {
              HashMap pmap=(HashMap) i2.next();
              Analystha pha=(Analystha)pmap.get("obj");
              // formlist holds forms belonging to an element
              ArrayList analystlist=new ArrayList();
              Long compid=null;
              try {
                 compid=new Long(pha.getPanalysthaid());
              } catch (Exception cie) { }
              // loop through prior columns looking for parent elements
              int addrows=1;
              if (pmap.get("havechild").equals("N")){ 
                 pmap.put("ncols",tlist.size()-ii);
                 for (int jj=(ii-1);jj>=0;jj--) {
                    ArrayList pplist=(ArrayList)tlist.get(jj);
                    Iterator i4=pplist.iterator();
                    while (i4.hasNext()) {
                       HashMap ppmap=(HashMap) i4.next();
                       Analystha ppha=(Analystha)ppmap.get("obj");
                       // if find a parent element, add rows to that element.  Then update
                       // compid to that element's parent for next loop
                       if ((
                              (!(compid==null)) && ppha.getAnalysthaid()==compid.longValue()
                           ) ||
                           (
                              ppha.getAnalysthaid()==pha.getAnalysthaid() && ppha.getPanalysthaid()==pha.getPanalysthaid()
                          )) {
                          int nrows=((Integer)ppmap.get("nrows")).intValue();
                          nrows=nrows+addrows;
                          ppmap.put("nrows",nrows);
                          try {
                             compid=new Long(ppha.getPanalysthaid());
                          } catch (Exception cie2) {}
                       }
                    }
                 }
              }
           }
        }
        
        /////////////////////////////////
        // Create HTML Table of result //
        /////////////////////////////////

        int tablecols=tlist.size();
        if (tablerows<1 || tablecols<1) {
           return null;
        }
        Table table=new Table(tablerows,tablecols);
        // iterate through table columns;
        for (int currcol=0;currcol<tlist.size();currcol++) {
           ArrayList rowlist=(ArrayList)tlist.get(currcol);
           int prevrow=0;
           Iterator rowiter=rowlist.iterator();
           while (rowiter.hasNext()) {
              HashMap map2=(HashMap)rowiter.next();
              Analystha ha2=(Analystha)map2.get("obj");
              int nrows=((Integer)map2.get("nrows")).intValue();
              int ncols=((Integer)map2.get("ncols")).intValue();
              if (nrows<=0 || ncols<=0) continue;
              Cell cell=new Cell(new Long(ha2.getAnalysthaid()).toString(),nrows,ncols);
              cell.setContent("type","analystha");
              cell.setContent("obj",ha2);
              boolean okvar=false;
              int currrow=prevrow;
              // Use previous row & exceptions to calculate cell row placement.  This could probably
              // be made more efficient by running through a calculation loop, but the loss here 
              // is quite minimal
              int firstrow=0;
              while (!okvar) {
                 try {
                    table.setCell(cell,currrow,currcol);
                    firstrow=currrow;
                    prevrow=currrow+1;
                    okvar=true;
                 } catch (Exception te) { 
                    currrow++;
                    if (currrow>500000) throw new FORMSException("Too many rows returned");
                 }
              }   
           }
        }

        mav.addObject("status","MOVECATSEL");
        if (moveobj!=null) mav.addObject("passobj",moveobj);
        mav.addObject("table",table);
        return mav;

      } catch (Exception e) {

        mav.addObject("status","MOVECATERROR");
        return mav;

      }

   }

   private String moveCatSubmit() {
      String analysthaid=getRequest().getParameter("analysthaid");
      // Move form
      String analystinfoid=getRequest().getParameter("objectid");
      Analystinfo ft=(Analystinfo)getMainDAO().execUniqueQuery(
         "select ft from Analystinfo ft where analystinfoid=" + analystinfoid
         );
      ft.setPanalysthaid(new Long(analysthaid));
      getMainDAO().saveOrUpdate(ft);
      clearSessionLists();
      return "FILEMOVED";
   }

   private String deleteFile() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      String analystinfoid=getRequest().getParameter("analystinfoid");
      Analystinfo ft=(Analystinfo)getMainDAO().execUniqueQuery(
         "select ft from Analystinfo ft where ft.analystinfoid=" + analystinfoid
         );

      Analystha ftpfha=(Analystha)getMainDAO().execUniqueQuery(
         "select ha from Analystha ha where ha.analysthaid=" + ft.getPanalysthaid()
         );

      getMainDAO().delete(ft);
      clearSessionLists();
      return "FILEDELETED";
   }

   private ModelAndView showInfo() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=this.getAuth();

      Analystinfo minfo=null;
      Allusers uinfo=null;

      // PULL LOCAL FORM INFORMATION
      String analystinfoid=auth.getValue("analystinfoid");
      minfo=(Analystinfo)getMainDAO().execUniqueQuery(
         "select minfo from Analystinfo minfo where minfo.analystinfoid=" + analystinfoid
         );
      uinfo=(Allusers)getMainDAO().execUniqueQuery(
         "select u from Allusers u where u.auserid=" + minfo.getUploaduser()
         );
      mav.addObject("userdesc",uinfo.getUserdesc());

      Analystha ftpfha=(Analystha)getMainDAO().execUniqueQuery(
         "select ha from Analystha ha where ha.analysthaid=" + minfo.getPanalysthaid()
         );

      mav.addObject("status","SHOWINFO");
      mav.addObject("minfo",minfo);
      mav.addObject("filesize",FileSizeUtil.displayFileSize(new Long(minfo.getFilesize()).longValue()));
      //mav.addObject("filesize",minfo.getFilesize());
      return mav;

   }

   private ModelAndView editAttr() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String analystinfoid=getRequest().getParameter("analystinfoid");
      Analystinfo ft=(Analystinfo)getMainDAO().execUniqueQuery(
         "select ft from Analystinfo ft where ft.analystinfoid=" + analystinfoid
         );

      Analystha ftpfha=(Analystha)getMainDAO().execUniqueQuery(
         "select ha from Analystha ha where ha.analysthaid=" + ft.getPanalysthaid()
         );

      mav.addObject("status","EDITATTR");
      mav.addObject("minfo",ft);
      return mav;

   }

   private String editAttrSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String analystinfoid=getRequest().getParameter("analystinfoid");
      String description=getRequest().getParameter("description");
      String creationdate=getRequest().getParameter("creationdate");
      String notes=getRequest().getParameter("notes");

      Analystinfo ft=(Analystinfo)getMainDAO().execUniqueQuery(
         "select ft from Analystinfo ft where ft.analystinfoid=" + analystinfoid
         );
      ft.setDescription(description);   
      ft.setCreationdate(creationdate);   
      ft.setNotes(notes);   
      getMainDAO().saveOrUpdate(ft);
      clearSessionLists();
      return "EDITCOMPLETE";
   }

   private String removeCat() {
      String analysthaid=getRequest().getParameter("analysthaid");
      Analystha fa=(Analystha)getMainDAO().execUniqueQuery(
         "select fa from Analystha fa where fa.analysthaid=" + analysthaid
         );
      // make sure category is not a root category
      if (fa.getPanalysthaid()==null) {
         return "NOREMOVEROOT";
      }
      // make sure category contains no subcategories
      List clist=getMainDAO().execQuery(
         "select fa from Analystha fa where fa.panalysthaid=" + analysthaid
         );
      if (clist.size()>0) {
         return "NOREMOVESUBCAT";
      }
      // Move existing files to parent   
      long panalysthaid=fa.getPanalysthaid();
      List l=getMainDAO().execQuery(
         "select ft from Analystinfo ft where ft.panalysthaid=" + analysthaid
         );
      Iterator i=l.iterator();
      while (i.hasNext()) {
         Analystinfo ft=(Analystinfo)i.next();
         ft.setPanalysthaid(panalysthaid);
         getMainDAO().saveOrUpdate(ft);
      }
      // Remove record
      getMainDAO().delete(fa);
      clearSessionLists();
      return "CATCOLLAPSED";
   }

   // Re-order categories within parent category
   public List reorderCat() {

      Long analysthaid=new Long(getRequest().getParameter("analysthaid"));

      Long panalysthaid=(Long)getMainDAO().execUniqueQuery(
                "select ha.panalysthaid from Analystha ha where ha.analysthaid=" + analysthaid
                );

      List l=getMainDAO().execQuery(
                "select ha from Analystha ha where ha.panalysthaid=:panalysthaid " +
                   "order by ha.haordr" 
                ,new String[] { "panalysthaid" }
                ,new Object[] { panalysthaid }
                );
      
      return l;

   }

   public String reorderCatSubmit() {

      String corderstr=(String)request.getParameter("corderstr");
      String[] sa=corderstr.split(",");
      for (int i=0;i<sa.length;i++) {
         Long analysthaid=new Long(sa[i].replaceFirst("^li_",""));
         Analystha fa2mod=(Analystha)getMainDAO().execUniqueQuery(
                   "select ha from Analystha ha where ha.analysthaid=:analysthaid"
                   ,new String[] { "analysthaid" }
                   ,new Object[] { analysthaid }
                   );
         fa2mod.setHaordr(i+1);
         getMainDAO().saveOrUpdate(fa2mod);
      }
      clearSessionLists();
      return "CATREORDERED";

   }

   private ModelAndView viewAllCat(ModelAndView mav) {

      mav=moveCat(mav,false);
      String status=(String)mav.getModelMap().get("status");  
      if (status.equalsIgnoreCase("MOVECATSEL")) {
        mav.addObject("status","VIEWALLSEL"); 
      }
      return mav;
      
   }

}

