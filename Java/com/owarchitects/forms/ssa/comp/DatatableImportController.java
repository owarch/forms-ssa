
/*
 *
 * DatatableImportController.java - Datatable Import controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.nio.charset.Charset;
import javax.servlet.http.*;
import org.hibernate.*;
import org.apache.velocity.app.*;
import org.apache.velocity.*;
import au.id.jericho.lib.html.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.mla.html.table.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;
import java.sql.Clob;
import com.csvreader.CsvReader;

public class DatatableImportController extends AbstractUploadController {

   private byte[] fileSource=null;

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Use class-name based view
      mav=new ModelAndView("DatatableImport");

      FORMSAuth auth=getAuth();

      try {

         HttpServletRequest request=this.getRequest();
         spath=request.getServletPath();

         if (spath.indexOf("datatableimport.")>=0) {

            mav.addObject("status","DATATABLEIMPORT");
            auth.setValue("replacestatus","NONE");
            auth.setValue("replacedesc","false");
            String studyid=auth.getValue("studyid");
            // pull localinst for later use
            this.localinst=(Allinst)getMainDAO().execUniqueQuery("select l from com.owarchitects.forms.commons.db.Allinst l where l.islocal=true");
            if (studyid==null || studyid.length()<1) {
               mav.addObject("list",new VisibilityConstants().getFieldList(VisibilityConstants.SYSTEM));
            } else {
               mav.addObject("list",new VisibilityConstants().getFieldList());
            }
            return mav;

         }
         /*
         else if (spath.indexOf("datatableimportreplace.")>=0) {

            auth.setValue("replacestatus",request.getParameter("replacestatus"));
            auth.setValue("replacedesc",request.getParameter("replacedesc"));
            mav.addObject("zerolength",new ArrayList());
            return callAttrScreen(mav);

         }
         */
         else if (spath.indexOf("datatableimport2.")>=0) {

            datatableImport2(mav);
            return mav;

         }
         else if (spath.indexOf("datatableimportsubmit.")>=0) {

            datatableImportSubmit(mav);
            return mav;
         }
         else if (spath.indexOf("datatableimportshowattrscreen.")>=0) {

            mav.addObject("replacestatus",auth.getValue("replacestatus"));
            mav.addObject("prevInfo",new Boolean(prevInfo).toString());
            mav.addObject("afhash",afhash);
            mav.addObject("ffhash",ffhash);
            mav.addObject("list",passlist);
            mav.addObject("status","SHOWATTRSCREEN");
            return mav;

         }
         else if (spath.indexOf("datatableimportattrsubmit.")>=0) {

            datatableImportAttrSubmit(mav);
            return mav;

         }
         else {
            getWriter().println("BAD REQUEST! - " + spath);
            fileSource=null;
            return null;
         }
   
      } catch (Exception e) { 
         mav.addObject("status","DATATABLEIMPORTERROR");
         mav.addObject("errmsg",Stack2string.getString(e));
         fileSource=null;
         return mav;
      }

   }

   // Second import screen (pass in formtyhpe & datatableformat list for selection)
   private void datatableImport2(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=getAuth();

      // See if user has permission to import forms at requested level
      if (!getPermHelper().hasGlobalPerm(auth.getValue("auserid"),
          new Integer(request.getParameter("datatablelevel")).intValue(),"DTFORMUPLOAD")) {
          mav.addObject("status","NOAUTH");
          fileSource=null;
          return;
      }

      // pull formtypes to pass to GUI
      List formtypelist=getMainDAO().execQuery(
         "select f from Formtypelist f where " +
            "f.allinst.ainstid=" + localinst.getAinstid() + " and " +
            "f.visibility=" + request.getParameter("datatablelevel") + " and " +
            "((f.visibility=" + VisibilityConstants.SYSTEM + ") or " +
            " (f.visibility=" + VisibilityConstants.SITE + " and f.studies.sites.siteid=" + auth.getValue("siteid") + ") or" +
            " (f.visibility=" + VisibilityConstants.STUDY + " and f.studies.studyid=" + auth.getValue("studyid") + "))" 
         );

      // Using HashSet here keeps multiple rows from being returned for the parent
      // table (keeps appropriate one-to-many format).  You work with the HashSet
      // pretty much identically to the List, even through Velocity, but the parent
      // child relationship is correctly mapped
      //
      // See:  http://forum.hibernate.org/viewtopic.php?t=955186
      //
      HashSet dtformatset=new HashSet(getMainDAO().execQuery(
         "select f from Datatableformats f join fetch f.supportedfileformats " +
            "join fetch f.supportedfileformats.fileextensions"
         ));
      // for pre-V0.5.0 compatibility, create record if none exists   
      if (dtformatset.isEmpty()) {
         Datatableformats dtf=new Datatableformats();
         Supportedfileformats sff=(Supportedfileformats)getMainDAO().execUniqueQuery(
            "select f from Supportedfileformats f " +
               " where f.contenttype='text/csv'"
               );
         dtf.setSupportedfileformats(sff);
         getMainDAO().saveOrUpdate(dtf);
         dtformatset=new HashSet(getMainDAO().execQuery(
            "select f from Datatableformats f join fetch f.supportedfileformats " +
               "join fetch f.supportedfileformats.fileextensions"
            ));
      }

      mav.addObject("status","DATATABLEIMPORT2");
      mav.addObject("dtformatset",dtformatset);
      mav.addObject("dtformatsetsize",new Integer(dtformatset.size()));

      auth.setValue("datatablelevel",request.getParameter("datatablelevel"));
      auth.setObjectValue("dtformatset",dtformatset);

   }

   // submit for import
   private ModelAndView datatableImportSubmit(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=getAuth();

      try {

         MultipartParser mp = new MultipartParser(request, 10*1024*1024); 
         Part part;
         String fileName=null;
         String dtacr=null, dtdesc=null, fileformatid=null, notes="";
         HashMap attrmap=new HashMap();
   
         while ((part = mp.readNextPart()) != null) {
            String name = part.getName();
            if (part.isParam()) {
               // it's a parameter part
               ParamPart paramPart = (ParamPart) part;
               if (name.equalsIgnoreCase("dtacr")) {
                  dtacr = paramPart.getStringValue();
               } else if (name.equalsIgnoreCase("dtdesc")) {
                  dtdesc = paramPart.getStringValue();
               } else if (name.equalsIgnoreCase("fileformatid")) {
                  fileformatid = paramPart.getStringValue();
               } else if (name.equalsIgnoreCase("notes")) {
                  notes = paramPart.getStringValue();
               }   
               // Create Attribute map used for processing form part
               attrmap.put(name,paramPart.getStringValue());
            }
            else if (part.isFile()) {
               // it's a file part
               FilePart filePart = (FilePart) part;
               fileName = filePart.getFileName();
               if (fileName != null) {
                  fileName=fileName.trim();
                  ByteArrayOutputStream baos = new ByteArrayOutputStream();
                  long size = filePart.writeTo(baos);
                  fileSource=baos.toByteArray();
               }
            }
         } 

         auth.setObjectValue("attrmap",attrmap);

         // These may be needed later
         auth.setValue("dtacr",dtacr);
         auth.setValue("dtdesc",dtdesc);
         auth.setValue("notes",notes);

         // check file name

         HashSet dtformatset=(HashSet)auth.getObjectValue("dtformatset");
   
         mav.addObject("dtacr",dtacr);
         mav.addObject("dtdesc",dtdesc);
         mav.addObject("fileformatid",fileformatid);
         mav.addObject("filename",fileName);
         mav.addObject("dtformatset",dtformatset);
         mav.addObject("filesource",fileSource);
         mav.addObject("notes",notes);

         // Pull supportedfileformats record for later use
         Supportedfileformats sformat=(Supportedfileformats)getMainDAO().execUniqueQuery(
            "select f from Supportedfileformats f where f.fileformatid=" + fileformatid
            );

         //
         // Make sure fileName as proper extension
         //
         Iterator i=dtformatset.iterator();
         boolean hasvalidextension=false;
         StringBuilder vesb=new StringBuilder();
         while (i.hasNext()) {
            Datatableformats f=(Datatableformats)i.next();
            if (f.getSupportedfileformats().getFileformatid()==sformat.getFileformatid()) {
               Set fes=(Set)f.getSupportedfileformats().getFileextensions();
               Iterator i2=fes.iterator();
               while (i2.hasNext()) {
                  Fileextensions e=(Fileextensions)i2.next();
                  String ext=e.getExtension();
                  vesb.append(ext);
                  if (i2.hasNext()) {
                     vesb.append(",");
                  }
                  if (fileName.toLowerCase().indexOf("." + ext.toLowerCase())>0) {
                     hasvalidextension=true;
                  }
               }
            }
         }
         if (!hasvalidextension) {
            auth.setValue("vesb",vesb.toString());
            mav.addObject("status","INVALIDEXTENSION");
            fileSource=null;
            return mav;
         }

         ////////////////////////////
         // Begin parsing CSV file //
         ////////////////////////////

         CsvReader reader=new CsvReader(new ByteArrayInputStream(fileSource),',',Charset.forName("UTF-8"));

         if (!reader.readHeaders()) {
            mav.addObject("status","HEADERREQUIRED");
            fileSource=null;
            return mav;
         }

         ArrayList infoList=new ArrayList();
         String[] header=reader.getHeaders();
         i=Arrays.asList(header).iterator();
         // Read header line, create columninfo objects
         while (i.hasNext()) {
            DatatableColumnInfo colinfo=new DatatableColumnInfo((String)i.next());
            if (colinfo.getColumnName().matches(".*[\\W].*")) {
               mav.addObject("status","INVALIDVNAME");
               fileSource=null;
               return mav;
            }
            infoList.add(colinfo);
         }

         // Read data rows, update columninfo objects
         while (reader.readRecord()) {
            String[] values=reader.getValues();
            for (int j=0;j<values.length;j++) {
               String value=values[j];
               DatatableColumnInfo colinfo=(DatatableColumnInfo)infoList.get(j);
               // Check for empty/missing values
               if (!colinfo.getHasEmptyValues() && value.trim().length()<1) {
                  colinfo.setHasEmptyValues(true);
               }
               // Check for MultiChar data
               if (!colinfo.getHasMultiCharacterValues() && isMultiChar(value)) {
                  colinfo.setHasMultiCharacterValues(true);
               }
               // Check for LowerCase data
               if (!colinfo.getHasLowerCaseValues() && hasLowerCase(value)) {
                  colinfo.setHasLowerCaseValues(true);
               }
               // No point in looking at other values if multi-character (type will have to be character)
               if (!colinfo.getHasMultiCharacterValues()) {
                  // check for single character
                  if (!colinfo.getHasSingleCharacterValues() && isSingleChar(value)) {
                     colinfo.setHasSingleCharacterValues(true);
                  }
                  //  check for numeric values
                  if (!colinfo.getHasNumericValues() && isValidFloat(value)) {
                     colinfo.setHasNumericValues(true);
                  }
                  //  check for decimal values
                  if (!colinfo.getHasDecimalValues() && isValidFloat(value) && !isValidLong(value)) {
                     colinfo.setHasDecimalValues(true);
                  }
               }
               // get maxlength field
               int tlength=value.trim().length();
               if (tlength>colinfo.getMaxLength()) {
                  colinfo.setMaxLength(tlength);
               }
               ArrayList vlist=colinfo.getUniqueValues(); 
               // update unique values list (maximum stored=25)
               if (vlist.size()<25) {
                  if (value!=null && value.trim().length()>0) {
                     if (isValidLong(value)) {
                        Long lv=new Long(value);
                        if (!vlist.contains(lv)) {
                           vlist.add(lv);
                        }
                     } else if (isValidFloat(value)) {
                        Float lf=new Float(value);
                        if (!vlist.contains(lf)) {
                           vlist.add(lf);
                        }
                     } else {
                        if (!vlist.contains(value)) {
                           vlist.add(value);
                        }
                     }
                  }
               }
               
            }
         }

         //////////////////////////////////////////
         //////////////////////////////////////////
         //////////////////////////////////////////
         // BEGIN CONSTRUCTION OF ATTRIBUTES GUI //
         //////////////////////////////////////////
         //////////////////////////////////////////
         //////////////////////////////////////////
    
         // 
         // Iterate over lists of fields, collecting info to pass to GUI
         // 
    
         Iterator fiter=infoList.iterator();

         // 
         // passlist will contain complete list of form fields
         // 
         passlist=new ArrayList();

         // will be used later
         Long pdthaid=null;

         //
         // Pull current datatableha record for requested level 
         //
         List dthalist=getMainDAO().execQuery(
            "select f from Datatableha f " +
               "where f.visibility=" + auth.getValue("datatablelevel") + " and " +
                   "((f.visibility=" + VisibilityConstants.SYSTEM + ") or " +
                   " (f.visibility=" + VisibilityConstants.SITE + " and f.studies.sites.siteid=" + auth.getValue("siteid") + ") or" +
                   " (f.visibility=" + VisibilityConstants.STUDY + " and f.studies.studyid=" + auth.getValue("studyid") + "))" 
         );

         Iterator dthai=dthalist.iterator();
         StringBuilder dthasb=new StringBuilder();
         while (dthai.hasNext()) {
            Datatableha dtha=(Datatableha)dthai.next();
            if (dtha.getPdthaid()==null) {
               pdthaid=dtha.getDthaid();
            }
            dthasb.append(new Long(dtha.getDthaid()).toString());
            if (dthai.hasNext()) {
               dthasb.append(",");
            }
         }

         //////////////////////////////////////////////////////////////////////////////////////////
         //////////////////////////////////////////////////////////////////////////////////////////
         //////////////////////////////////////////////////////////////////////////////////////////
         // Make sure DataTable with same acronym doesn't already exist at the Datatableha level //
         //////////////////////////////////////////////////////////////////////////////////////////
         //////////////////////////////////////////////////////////////////////////////////////////
         //////////////////////////////////////////////////////////////////////////////////////////

         if (pdthaid!=null) {

             List acrList=getMainDAO().execQuery(
                "select f from Datatabledef f where f.pdthaid=" + pdthaid + " and f.dtacr='" + dtacr + "'"
             );
             if (acrList.size()>0) {
                mav.addObject("status","DUPLICATEACR");
                fileSource=null;
                return mav;
             }

         }

         // will be used later
         Long pformhaid=null;

         //
         // Pull current formha record for requested level 
         //
         List formhalist=getMainDAO().execQuery(
            "select f from Formha f " +
               "where f.visibility=" + auth.getValue("datatablelevel") + " and " +
                   "((f.visibility=" + VisibilityConstants.SYSTEM + ") or " +
                   " (f.visibility=" + VisibilityConstants.SITE + " and f.studies.sites.siteid=" + auth.getValue("siteid") + ") or" +
                   " (f.visibility=" + VisibilityConstants.STUDY + " and f.studies.studyid=" + auth.getValue("studyid") + "))" 
         );

         Iterator fhai=formhalist.iterator();
         StringBuilder fhasb=new StringBuilder();
         while (fhai.hasNext()) {
            Formha fha=(Formha)fhai.next();
            if (fha.getPformhaid()==null) {
               pformhaid=fha.getFormhaid();
            }
            fhasb.append(new Long(fha.getFormhaid()).toString());
            if (fhai.hasNext()) {
               fhasb.append(",");
            }
         }

         List afList=(List)new ArrayList();   
         List ffList=(List)new ArrayList();   
         List ffdList=(List)new ArrayList();   
         List fnamelist=(List)new ArrayList();   
         List dupfnamelist=(List)new ArrayList();   

         boolean fnamedup=false;

         // For now, always importing new table, not replacing
         auth.setObjectValue("hasdatatabledef",new Boolean(false));

         // create formtypes, datatabledef objects to be persisted in later step

         Datatabledef sdef=new Datatabledef();
         sdef.setAllinst(localinst);
         sdef.setDtacr(dtacr);
         sdef.setDtdesc(dtdesc);
         sdef.setIsarchived(new Boolean(false));
         sdef.setIsenrollform(new Boolean(false));
         sdef.setSsaonly(new Boolean(false));
         sdef.setNotes(notes);
         sdef.setUploaduser(new Long(auth.getValue("auserid")).longValue());
         sdef.setUploadtime(new Date());
         sdef.setPformhaid(pformhaid);
         sdef.setPdthaid(pdthaid);
         // For now, interface not assigning this value
         sdef.setDisplayas(DisplayAsConstants.DATATABLEONLY);
         auth.setObjectValue("datatabledefobject",sdef);

         while (fiter.hasNext()) {
    
            StringBuilder sbval=new StringBuilder("");
            StringBuilder sbval2=new StringBuilder("");

            DatatableColumnInfo info=(DatatableColumnInfo)fiter.next();

            String colname=info.getColumnName();

            HashMap passhash=new HashMap(); 
            passhash.put("name",colname);
            passhash.put("hasSingleCharacterValues",new Boolean(info.getHasSingleCharacterValues()));
            passhash.put("hasMultiCharacterValues",new Boolean(info.getHasMultiCharacterValues()));
            passhash.put("hasLowerCaseValues",new Boolean(info.getHasLowerCaseValues()));
            passhash.put("hasNumericValues",new Boolean(info.getHasNumericValues()));
            passhash.put("hasDecimalValues",new Boolean(info.getHasDecimalValues()));
            passhash.put("hasEmptyValues",new Boolean(info.getHasEmptyValues()));
            passhash.put("maxLength",new Integer(info.getMaxLength()));
            passhash.put("proposedLength",new Integer(getProposedLength(info.getMaxLength())));
            passhash.put("uniqueValues",new Integer(info.getUniqueValues().size()));
            passhash.put("uniqueValueString",info.getUniqueValueString());
            passlist.add(passhash);

            // duplicate checks 
            if (fnamelist.contains(colname)) {
               dupfnamelist.add(colname);
               fnamedup=true;
            } else {
               fnamelist.add(colname);
            }


            if (fnamedup) {
               mav.addObject("status","DUPLICATEFIELDNAMES");
               mav.addObject("errmsg",dupfnamelist.toString());
               fileSource=null;
               return mav;
            }   

         }   

         // 
         // retrieve study cross-form field defaults if exist
         // 

         ffhash=new HashMap();

         ffdList=getMainDAO().execQuery(
            "select f from Dtfieldattrdefaults f " +
               "where f.visibility=" + auth.getValue("datatablelevel") + " and " +
                   "((f.visibility=" + VisibilityConstants.SYSTEM + ") or " +
                   " (f.visibility=" + VisibilityConstants.SITE + " and f.studies.sites.siteid=" + auth.getValue("siteid") + ") or" +
                   " (f.visibility=" + VisibilityConstants.STUDY + " and f.studies.studyid=" + auth.getValue("studyid") + "))" 
         );

         Iterator ffditer=ffdList.iterator();
         while (ffditer.hasNext()) {
            Dtfieldattrdefaults ffd=(Dtfieldattrdefaults)ffditer.next();
            ffhash.put(ffd.getFieldname() + "_" + ffd.getAttr(),ffd.getValue());
         }   

         mav.addObject("ffhash",ffhash);
         return callAttrScreen(mav);

      } catch (Exception e) {
         throw new FORMSException("Form Import Error - " + Stack2string.getString(e));
      }

   }

   private int getProposedLength(int len) {
      if (len<=1) return 1;
      else if (len<=5) return 5;
      else if (len<=10) return 10;
      else if (len<=25) return 25;
      else if (len<=50) return 50;
      else if (len<=100) return 100;
      else if (len<=200) return 200;
      else if (len<=250) return 250;
      else if (len<=500) return 500;
      else return len;
   }

   // submit for import
   private ModelAndView datatableImportAttrSubmit(ModelAndView mav) throws Exception {

      mav=abstractAttrSubmit(mav,true);

      if (mav.getModelMap().get("status").toString().equalsIgnoreCase("UPLOADCOMPLETE")) {
         return importDataRecords(mav);
      } 

      fileSource=null;
      return mav;

   }

   // import data records
   private ModelAndView importDataRecords(ModelAndView mav) throws Exception {
         
      FORMSAuth auth=getAuth();
      // get data table definition record
      Datatabledef dtdef=(Datatabledef)auth.getObjectValue("datatabledefobject");

      // re-pull record to avoid transient object exceptions
      dtdef=(Datatabledef)getMainDAO().execUniqueQuery(
         "select d from Datatabledef d where dtdefid=" + dtdef.getDtdefid() 
         );

      // get userid
      long auserid=new Long(auth.getValue("auserid")).longValue();

      // set up reader
      CsvReader reader=new CsvReader(new ByteArrayInputStream(fileSource),',',Charset.forName("UTF-8"));

      ArrayList<String> colNameList=new ArrayList();
      reader.readHeaders();
      String[] header=reader.getHeaders();
      Iterator<String> i=Arrays.asList(header).iterator();

      // Read header line, create columninfo objects
      while (i.hasNext()) {
         colNameList.add(i.next());
      }
      // Read data rows, create and persist records
      while (reader.readRecord()) {

         // create and persist datatable record
         Datatablerecords newrecord=new Datatablerecords();
         newrecord.setDatatabledef(dtdef);
         newrecord.setIshold(false);
         newrecord.setSaveuser(auserid);
         newrecord.setSavetime(new Date());
         newrecord.setIsaudit(false);
         newrecord.setIslocked(false);
         getMainDAO().save(newrecord,false);

         // TODO:  Important.  This section should be converted to do batch inserts.
         // The process below is quite inefficient.
         
         // create and persist datatable record values
         String[] values=reader.getValues();
         for (int j=0;j<values.length;j++) {
            String value=values[j];
            String name=colNameList.get(j);
            if (value!=null && value.trim().length()>0) {
               // create and persist value object
               Datatablevalues newvalue=new Datatablevalues();
               newvalue.setDatatablerecords(newrecord);
               newvalue.setVarname(name);
               newvalue.setVarvalue(value);
               getMainDAO().save(newvalue,false);
            }
         }

      }

      reader.close();
      fileSource=null;
      mav.addObject("status","IMPORTCOMPLETE");
      return mav;

   }

}

