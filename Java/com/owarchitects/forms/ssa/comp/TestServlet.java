 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
* TestServlet.java -- Letter program upload program
* Original Creation:  04/2006 (MRH)
*/

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.io.*;
import java.util.*;
//import com.adobe.fdf.*;
//import com.adobe.fdf.exceptions.*;
//import com.sas.sasserver.sclfuncs.*;
//import com.sas.rmi.*;
//import com.sas.sasserver.submit.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.velocity.app.*;
import org.apache.velocity.*;
import com.sas.sasserver.sclfuncs.*;
import com.sas.sasserver.dataset.*;
import com.sas.rmi.*;
import com.sas.sasserver.submit.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;

public class TestServlet extends HttpServlet {


   public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

      PrintWriter out=response.getWriter();
      out.println("<br>GET CALLED<br>");
      Map m=request.getParameterMap();
      out.println("parameter map size:  " + m.size());
      Enumeration e=request.getParameterNames();
      while (e.hasMoreElements()) {
         String k=(String)e.nextElement();
         out.println("parameter:  " + k + "<br>" + "value:  " + request.getParameter(k));
      }
   }

   public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {



            if (request.getContentType().toLowerCase().indexOf("multipart")>=0) {

               PrintWriter out=response.getWriter();

               MultipartParser mp = new MultipartParser(request, 1*1024*1024); 
               Part part;
               String inString=""; 
               String siteacr=""; 
               String sitedesc=""; 
               String siteurl=""; 
               String sitekey=""; 
               String createkey=""; 
               while ((part = mp.readNextPart()) != null) {
                  String name = part.getName();
                  if (part.isParam()) {
                     // it's a parameter part
                     ParamPart paramPart = (ParamPart) part;
                     if (name.equalsIgnoreCase("siteacr")) {
                        siteacr = paramPart.getStringValue();
                     } else if (name.equalsIgnoreCase("sitedesc")) {
                        sitedesc = paramPart.getStringValue().toUpperCase();
                     } else if (name.equalsIgnoreCase("siteurl")) {
                        siteurl = paramPart.getStringValue().toUpperCase();
                     } else if (name.equalsIgnoreCase("createkey")) {
                        createkey = paramPart.getStringValue().toUpperCase();
                     }
                  }
                  else if (part.isFile()) {
                     // it's a file part
                     FilePart filePart = (FilePart) part;
out.println("<br>HELLO - Have a file part");
out.println("<br>HELLO - getContentType=" + filePart.getContentType());
out.println("<br>HELLO - getFileName=" + filePart.getFileName());
out.println("<br>HELLO - getFilePath=" + filePart.getFilePath());
out.println("<br>HELLO - isFile=" + filePart.isFile());
out.println("<br>HELLO - getName=" + filePart.getName());
out.println("<br>HELLO - isParam=" + filePart.isParam());
                     String fileName = filePart.getFileName();
out.println("<br>HELLO - fileName:  " + fileName);
                     //if (fileName != null) {
                        //fileName=fileName.trim();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        long size = filePart.writeTo(baos);
out.println("<br>HELLO - size=" + size);
                        sitekey=baos.toString("ISO-8859-1");
out.println("<br>HELLO - sitekey=" + sitekey);
                     //}
                  }
               } 
               out.flush();

               out.println("<br>siteacr=" + siteacr);
               out.println("<br>sitedesc=" + sitedesc);
               out.println("<br>siteurl=" + siteurl);
               out.println("<br>createkey=" + createkey);
               out.println("<br>sitekey=" + sitekey);

               //wp.addViewObjects(mav);
               //mav.addObject("status","POST");
               //mav.addObject("poststatus","IMESSAGE");
               //return mav;

            } else {
           
      PrintWriter out=response.getWriter();
      out.println("<br>POST CALLED<br>");
      Map m=request.getParameterMap();
      out.println("parameter map size:  " + m.size());
      Enumeration e=request.getParameterNames();
      while (e.hasMoreElements()) {
         String k=(String)e.nextElement();
         out.println("parameter:  " + k + "<br>");
         out.println("value:  " + request.getParameter(k));
      }


            } 


   }

}

