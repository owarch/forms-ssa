
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * GetImageController.java - Return Filesystem Images
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
//import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import org.apache.commons.io.FileUtils;
//import java.sql.Blob;
//import javax.servlet.http.*;
//import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
//import org.springframework.orm.hibernate3.*;
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang.StringUtils;
//import org.springframework.transaction.support.TransactionSynchronizationManager;
//import org.mla.html.table.*;
//import org.json.simple.JSONObject;
//import org.apache.velocity.VelocityContext;
//import org.apache.velocity.app.VelocityEngine;

public class GetImageController extends FORMSController {

   private static boolean hold=false;

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      while (hold==true) { }
      hold=true;

      try {

         String fileName=EncryptUtil.decryptString((String)getRequest().getParameter("image"),
            SessionObjectUtil.getInternalKeyObj(getSession()));
         File imageFile=new File(fileName);   
         getResponse().setContentType("image/jpeg");
         if (imageFile.exists()) {
            // Don't just return anything requested.  Currently only using this for R Reporting program output
            if (fileName.indexOf("r_outimage")>=0) {
               String imgContent=FileUtils.readFileToString(imageFile,"ISO-8859-1");
               getResponse().setContentLength(imgContent.length());
               out.println(imgContent);
            }
         } 

      } catch (Exception e) { }    

      hold=false;
      return null;

   }   

}


