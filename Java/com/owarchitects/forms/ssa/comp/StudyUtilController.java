 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * StudyUtilController.java - Study utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;

public class StudyUtilController extends FORMSController {

   Sites sitrec;
   Studies stdrec;
   boolean isbadrequest;
   String spath;
   String status;
   String site;
   String study;
   String sitedesc;
   String studydesc;

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Use class-name based view
      mav=new ModelAndView("StudyUtil");
      isbadrequest=true;

      try {

         HttpServletRequest request=this.getRequest();
         spath=request.getServletPath();
         sitrec=new Sites();
         stdrec=new Studies();

         status="";
         site="";
         study="";
         sitedesc="";
         studydesc="";

         // addstudy.sutil  (add new study interface screen)
         if (spath.indexOf("addstudy")>=0) {

            if (!hasGlobalPerm(PcatConstants.SYSTEM,"ADDSTUDY")) {
               mav.addObject("status","NOAUTH");
               return mav;
            }

            isbadrequest=false;
            List l;
            l=getMainDAO().getTable("Sites");
            if (l.size()>0) {
               mav.addObject("status","ADDSTUDY");
               mav.addObject("sitelist",l);
            } else {
               mav.addObject("status","ADDSITE");
               mav.addObject("showsitemsg","Y");
            }
            return mav;

         }

         // add new site interface
         else if (spath.indexOf("addsite")>=0) {

            if (!hasGlobalPerm(PcatConstants.SYSTEM,"ADDSTUDY")) {
               mav.addObject("status","NOAUTH");
               return mav;
            }

            isbadrequest=false;
            mav.addObject("status","ADDSITE");
            mav.addObject("showsitemsg","N");
            return mav;

         }

         // change archive status
         else if (spath.indexOf("archivestatus")>=0) {

            if (!hasGlobalPerm(PcatConstants.SYSTEM,"MODSTUDY")) {
               mav.addObject("status","NOAUTH");
               return mav;
            }

            isbadrequest=false;
            mav.addObject("status","ARCHIVESTATUS");
            return mav;

         }

         // modify study attributes
         else if (spath.indexOf("studymod")>=0) {

            if (!hasGlobalPerm(PcatConstants.SYSTEM,"MODSTUDY")) {
               mav.addObject("status","NOAUTH");
               return mav;
            }

            isbadrequest=false;
            List l;
            l=getMainDAO().getTable("Sites");
            if (l.size()>0) {
               mav.addObject("status","STUDYMOD");
               mav.addObject("sitelist",l);
            } else {
               mav.addObject("status","DATAERR");
            }
            return mav;

         }

         // view/edit site list
         else if (spath.indexOf("listsites")>=0) {

            isbadrequest=false;
            List l;
            l=getMainDAO().getTable("Sites");
            mav.addObject("status","LISTSITES");
            mav.addObject("sitelist",l);
            return mav;

         }

         // Submit new site to system
         else if (spath.indexOf("newsite")>=0) {

            if (!hasGlobalPerm(PcatConstants.SYSTEM,"ADDSTUDY")) {
               mav.addObject("status","NOAUTH");
               return mav;
            }

            status=submitNewSite();
            mav.addObject("status",status);
            return mav;

         }

         // modify site attributes interface 
         else if (spath.indexOf("sitemod")>=0) {

            if (!hasGlobalPerm(PcatConstants.SYSTEM,"MODSTUDY")) {
               mav.addObject("status","NOAUTH");
               return mav;
            }

            isbadrequest=false;
            List l;
            l=getMainDAO().getTable("Sites");
            if (l.size()>0) {
               mav.addObject("status","SITEMOD");
               mav.addObject("sitelist",l);
            } else {
               mav.addObject("status","DATAERR");
            }
            return mav;

         }

         // Submit change in archive status
         else if (spath.indexOf("archstatsubmit")>=0) {

            if (!hasGlobalPerm(PcatConstants.SYSTEM,"MODSTUDY")) {
               mav.addObject("status","NOAUTH");
               return mav;
            }

            isbadrequest=false;
            status=archStatSubmit();

         }

         // stemodsubmit.sutil (modify site attributes)

         if (request.getParameter("sitedesc")!=null) {
            site=request.getParameter("site").toString().trim();
            sitedesc=request.getParameter("sitedesc").toString().trim();
         }
 
         if (spath.indexOf("stemodsubmit")>=0) {

            isbadrequest=false;
            status=siteModSubmit();

         }

         // newstudy.sutil & stdmodsubmit.sutil (add new study submit or submit site changes)

         if (request.getParameter("studydesc")!=null) {
            site=request.getParameter("site").toString().trim();
            study=request.getParameter("study").toString().trim();
            studydesc=request.getParameter("studydesc").toString().trim();
         }

         // Submit new study
         if (spath.indexOf("newstudy")>=0) {

            if (!hasGlobalPerm(PcatConstants.SYSTEM,"ADDSTUDY")) {
               mav.addObject("status","NOAUTH");
               return mav;
            }

            isbadrequest=false;
            status=submitNewStudy();

         }

         // Submit changes in study attributes
         else if (spath.indexOf("stdmodsubmit")>=0) {

            if (!hasGlobalPerm(PcatConstants.SYSTEM,"MODSTUDY")) {
               mav.addObject("status","NOAUTH");
               return mav;
            }

            isbadrequest=false;
            status=studyModSubmit();

         }

         // Delete site
         if (spath.indexOf("sitedelete")>=0) {

            if (!hasGlobalPerm(PcatConstants.SYSTEM,"MODSTUDY")) {
               mav.addObject("status","NOAUTH");
               return mav;
            }

            isbadrequest=false;
            status=deleteSite();

         }

         if (!isbadrequest) {
            mav.addObject("status",status);
            return mav;
         } else {
            out.println("BAD REQUEST! - $spath");
            return null;
         }
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Submit new site to system
   private String submitNewSite() {

            String status="";
            isbadrequest=false;
            if (request.getParameter("sitedesc")!=null) {
               site=request.getParameter("site").toString().trim();
               sitedesc=request.getParameter("sitedesc").toString().trim();
            }

            List l;
            Sites s=(Sites)getMainDAO().execUniqueQuery(
               "select s from Sites s where siteno=:site","site",site
               );
            if (s!=null) {
               status="SITEEXISTS";
               return status;
            } else {
               sitrec.setSiteno(site);
               sitrec.setSitedesc(sitedesc);
               getMainDAO().saveOrUpdate(sitrec);
               status="SITEADDED";
               return status;
            }

   }

   // Submit change in archive status
   private String archStatSubmit() {            

            String status="";
            Long studyid=new Long(request.getParameter("studyid").toString().trim());
            List l;
            // Retrieve current record
            l=getMainDAO().execQuery(
               " select std from Studies as std " +
                  " where std.studyid=:studyid ", 
                  new String[] { "studyid" } ,
                  new Object[] { studyid }
                                                       );
            if (l.size()!=1) {
               status="NORETRIEVE";
               return status;
            }
            stdrec=(Studies)l.get(0);

            if (request.getParameter("isarchived").equals("T")) {
               stdrec.setIsarchived(true);
            } else {
               stdrec.setIsarchived(false);
            }
            getMainDAO().saveOrUpdate(stdrec);
            status="OK";
            return status;

   }

   // submit site attribute modifications
   private String siteModSubmit() {

            String status="";
            Long siteid=new Long(request.getParameter("siteid").toString().trim());
            List l;
            // Retrieve current record
            l=getMainDAO().execQuery(
               " select site from Sites as site " +
                  " where site.siteid=:siteid ", 
                  new String[] { "siteid" } ,
                  new Object[] { siteid }
                                                       );
            if (l.size()!=1) {
               status="NORETRIEVE";
               return status;
            }
            sitrec=(Sites)l.get(0);

            l=getMainDAO().execQuery(
               " select site from Sites as site " +
                  " where site.siteno=:site and site.siteid!=:siteid ", 
                  new String[] { "site","siteid" } ,
                  new Object[] { site, siteid }
               );
   
            if (l.size()>0) {
               status="SITEEXISTS";
               return status;
            }

            sitrec.setSiteno(site);
            sitrec.setSitedesc(sitedesc);
            getMainDAO().saveOrUpdate(sitrec);
            status="OK";
            return status;

   }

   // Submit new study
   private String submitNewStudy() {
   
            String status="";
            List l;
            l=getMainDAO().execQuery(
               " select std from Studies as std " +
                  " where std.sites.siteno=:site and std.studyno=:study ", 
                  new String[] { "site","study" } ,
                  new Object[] { site, study }
                                                       );
   
            if (l.size()>0) {
               status="SSEXISTS";
               return status;
            }
   
            l=getMainDAO().execQuery(
               " select std from Studies as std " +
                  " where std.sites.siteno=:site and std.studydesc=:studydesc ", 
                  new String[] { "site","studydesc" } ,
                  new Object[] { site, studydesc }
                                                       );
   
            if (l.size()>0) {
               status="SDEXISTS";
               return status;
            }

            sitrec=(Sites)getMainDAO().execUniqueQuery(
               "select s from Sites s where siteno=:site","site",site
               );
            if (sitrec==null) {
               status="SITERROR" + site;
               return status;
            } 

            stdrec.setSites(sitrec);
            stdrec.setStudyno(study);
            stdrec.setStudydesc(studydesc);
            stdrec.setIsarchived(false);
            getMainDAO().saveOrUpdate(stdrec);
            status="OK";
            return status;

   }

   // Submit changes in study attributes
   private String studyModSubmit() {

            String status="";
            Long studyid=new Long(request.getParameter("studyid").toString().trim());
            List l;
            // Retrieve current record
            l=getMainDAO().execQuery(
               " select std from Studies as std " +
                  " where std.studyid=:studyid ", 
                  new String[] { "studyid" } ,
                  new Object[] { studyid }
                                                       );
            if (l.size()!=1) {
               status="NORETRIEVE";
               return status;
            }
            stdrec=(Studies)l.get(0);

            l=getMainDAO().execQuery(
               " select std from Studies as std " +
                  " where std.sites.siteno=:site and std.studyno=:study and std.studyid!=:studyid ", 
                  new String[] { "site","study","studyid" } ,
                  new Object[] { site, study, studyid }
                                                       );
   
            if (l.size()>0) {
               status="SSEXISTS";
               return status;
            }
   
            l=getMainDAO().execQuery(
               " select std from Studies as std " +
                  " where std.sites.siteno=:site and std.studydesc=:studydesc and std.studyid!=:studyid ", 
                  new String[] { "site","studydesc","studyid" } ,
                  new Object[] { site, studydesc, studyid }
                                                       );
   
            if (l.size()>0) {
               status="SDEXISTS";
               return status;
            }

            sitrec=(Sites)getMainDAO().execUniqueQuery(
               "select s from Sites s where siteno=:site","site",site
               );
            if (sitrec==null) {
               status="SITERROR";
               return status;
            } 

            stdrec.setSites(sitrec);
            stdrec.setStudyno(study);
            stdrec.setStudydesc(studydesc);
            getMainDAO().saveOrUpdate(stdrec);
            status="OK";
            return status;

   }

   // delete site
   private String deleteSite() {

            Long siteid=new Long(request.getParameter("siteid").toString().trim());
            List l;
            // Retrieve current record
            l=getMainDAO().execQuery(
               " select site from Sites as site " +
                  " where site.siteid=:siteid ", 
                  new String[] { "siteid" } ,
                  new Object[] { siteid }
                                                       );
            if (l.size()!=1) {
               status="NORETRIEVE";
               return status;
            }
            sitrec=(Sites)l.get(0);

            l=getMainDAO().execQuery(
               " select std from Studies as std " +
                  " where std.sites.siteid=:siteid ", 
                  new String[] { "siteid" } ,
                  new Object[] { siteid }
                                                       );
   
            if (l.size()>0) {
               status="NOSITEDELETE";
               return status;
            }
   
            if (sitrec==null) {
               status="SITERROR";
               return status;
            } 

            getMainDAO().delete(sitrec);
            status="SITEDELETED";
            return status;

   }

}


