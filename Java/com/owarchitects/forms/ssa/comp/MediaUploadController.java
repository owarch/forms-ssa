 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * MediaUploadController.java - Form Upload controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.text.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.apache.velocity.app.*;
import org.apache.velocity.*;
import au.id.jericho.lib.html.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.mla.html.table.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;
import java.sql.Clob;

public class MediaUploadController extends FORMSController {
 
   String spath="";

   private ArrayList passlist=null;
   private HashMap afhash;
   private HashMap ffhash;
   private String replaceparm;
   private boolean prevInfo;

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // check permissions
      //if (!getAuth().getValue("rightslevel").equalsIgnoreCase("ADMIN")) {
      //   safeRedirect("permission.err");
      //   return null;
      //}

      // Use class-name based view
      mav=new ModelAndView("MediaUpload");

      try {

         HttpServletRequest request=this.getRequest();
         spath=request.getServletPath();

         if (spath.indexOf("mediaupload.")>=0) {

            mav.addObject("status","MEDIAUPLOAD");
            getAuth().setValue("replacestatus","NONE");
            getAuth().setValue("replacedesc","false");
            String studyid=this.getAuth().getValue("studyid");
            if (studyid==null || studyid.length()<1) {
               mav.addObject("list",new VisibilityConstants().getFieldList(VisibilityConstants.SYSTEM));
            } else {
               mav.addObject("list",new VisibilityConstants().getFieldList());
            }
            return mav;

         }
         /*
         else if (spath.indexOf("mediauploadreplace.")>=0) {

            getAuth().setValue("replacestatus",request.getParameter("replacestatus"));
            getAuth().setValue("replacedesc",request.getParameter("replacedesc"));
            return callAttrScreen(mav);

         }
         */
         else if (spath.indexOf("mediaupload2.")>=0) {

            mediaUpload2(mav);
            // check permission before returning
            if (!getPermHelper().hasGlobalPerm(new Integer(getAuth().getValue("medialevel")).intValue(),"MEDUPLOAD")) {
               mav.addObject("status","NOAUTH");
               return mav;
            }
            return mav;

         }
         else if (spath.indexOf("mediauploadsubmit.")>=0) {

            mediaUploadSubmit(mav);
            return mav;
         }
         else if (spath.indexOf("mediauploadfinalsubmit.")>=0) {

            mediaUploadFinalSubmit(mav);
            return mav;
         }
         else {
            getWriter().println("BAD REQUEST! - " + spath);
            return null;
         }
   
      } catch (Exception e) { 
         getWriter().println(Stack2string.getString(e));
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Second upload screen 
   private void mediaUpload2(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      mav.addObject("status","MEDIAUPLOAD2");
      getAuth().setValue("medialevel",request.getParameter("medialevel"));

   }

   // submit for upload
   private ModelAndView mediaUploadSubmit(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {

         FORMSAuth auth=getAuth();

         if (!getPermHelper().hasGlobalPerm(new Integer(auth.getValue("medialevel")).intValue(),"MEDUPLOAD")) {
            mav.addObject("status","NOAUTH");
            return mav;
         }

         MultipartParser mp = new MultipartParser(request, 500*1024*1024); 
         Part part;
         String filename=null;
         String description=null;
         String creationdate=null;
         String notes=null;
         String mediasource=null;
         ByteArrayOutputStream mediabaos=null;
         String filecontenttype=null;
         String filepath=null;
         long filesize=-1;
   
         while ((part = mp.readNextPart()) != null) {
            String name = part.getName();
            if (part.isParam()) {
               // it's a parameter part
               ParamPart paramPart = (ParamPart) part;
               if (name.equalsIgnoreCase("description")) {
                  description = paramPart.getStringValue();
               } else if (name.equalsIgnoreCase("creationdate")) {
                  creationdate = paramPart.getStringValue();
               } else if (name.equalsIgnoreCase("notes")) {
                  notes = paramPart.getStringValue();
               }   
            }
            else if (part.isFile()) {
               // it's a file part
               FilePart filePart = (FilePart) part;
               filename = filePart.getFileName();
               filepath = filePart.getFilePath();
               filecontenttype = filePart.getContentType();
               if (filename != null) {
                  filename=filename.trim();
                  mediabaos = new ByteArrayOutputStream();
                  filesize = filePart.writeTo(mediabaos);
               }
            }
         } 
         if (description==null || description.trim().length()<1) {
            description=filename;
         }
         if (creationdate==null || creationdate.trim().length()<1) {
            DateFormat fmt=new SimpleDateFormat("MM/dd/yyyy");
            creationdate=fmt.format(new Date(System.currentTimeMillis()));
         }
         
         // NOTE: mediasource, filename & contenttype will be saved also on server to prevent users
         // from final submitting different values than originally passed

         mav.addObject("filename",filename);
         auth.setObjectValue("filesize",new Long(filesize));
         auth.setValue("filename",filename);
         mav.addObject("description",description);
         mav.addObject("creationdate",creationdate);
         mav.addObject("notes",notes);
         //mav.addObject("mediasource",mediasource);
         mav.addObject("filepath",filepath);
         mav.addObject("filecontenttype",filecontenttype);
         mav.addObject("status","DECIDE");
         auth.writeAuthStream("mediasource",mediabaos);

         // pull extension/content type info for form
         String ext=filename.substring(filename.lastIndexOf(".")+1).toLowerCase();

         // make sure file extension is not disallowed
         List dlist=getMainDAO().execQuery(
            "select d from Disallowedfileextensions d where lower(d.extension)='" + ext + "'"
         );
         if (dlist.size()>0) {
            mav.addObject("status","DISALLOWEDTYPE");
            return mav;
         }

         Fileextensions fext=(Fileextensions)getMainDAO().execUniqueQuery(
            "select f from Fileextensions f join fetch f.supportedfileformats where lower(f.extension)='" + ext + "'"
            );
         if (fext!=null) {
            //mav.addObject("extension",fext.getExtension());
            mav.addObject("fileformat",fext.getSupportedfileformats().getFileformat());
            mav.addObject("contenttype",fext.getSupportedfileformats().getContenttype());
            auth.setValue("contenttype",fext.getSupportedfileformats().getContenttype());
            mav.addObject("fileformatid",fext.getSupportedfileformats().getFileformatid());
         } else {
            mav.addObject("status","UNSUPPORTEDTYPE");
             return mav;
         }
         return mav;

      } catch (Exception e) {
         throw new FORMSException("Form Upload Error" + Stack2string.getString(e));
      }

   }

   // submit for upload
   private ModelAndView mediaUploadFinalSubmit(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {

         FORMSAuth auth=getAuth();
         String filename=auth.getValue("filename");
         String contenttype=auth.getValue("contenttype");
         String description=request.getParameter("description");
         String creationdate=request.getParameter("creationdate");;
         String fileformatid=request.getParameter("fileformatid");;
         String notes=request.getParameter("notes");
         Date uploadtime=new Date(System.currentTimeMillis());

         // pull extension/content type info for form
         String ext=filename.substring(filename.lastIndexOf(".")+1).toLowerCase();
   
         // make sure file extension is not disallowed
         List dlist=getMainDAO().execQuery(
            "select d from Disallowedfileextensions d where lower(d.extension)='" + ext + "'"
         );
         if (dlist.size()>0) {
            mav.addObject("status","DISALLOWEDTYPE");
            return mav;
         }

         // pull root media heirarchy record to locate file
         Mediaha mediaha=(Mediaha)getMainDAO().execUniqueQuery(
            "select m from Mediaha m where m.studies.studyid=" + auth.getValue("studyid") + 
               " and m.visibility=" + auth.getValue("medialevel") + " and m.pmediahaid=null"
            );

         if (!getPermHelper().hasGlobalPerm(mediaha.getVisibility(),"MEDUPLOAD")) {
            mav.addObject("status","NOAUTH");
            return mav;
         }

         Long mediahaid=mediaha.getMediahaid();   

         Allinst localinst=(Allinst)getMainDAO().execUniqueQuery("select l from com.owarchitects.forms.commons.db.Allinst l where l.islocal=true");


         Supportedfileformats fmt=(Supportedfileformats)getMainDAO().execUniqueQuery("select s from Supportedfileformats s where s.fileformatid=" + fileformatid);

         try {

            Mediainfo info=new Mediainfo();
            info.setFilename(filename);
            info.setFilesize((Long)auth.getObjectValue("filesize"));
            info.setAllinst(localinst);
            info.setPmediahaid(mediahaid);
            info.setDescription(description);
            info.setCreationdate(creationdate);
            info.setNotes(notes);
            info.setUploadtime(uploadtime);
            info.setUploaduser(new Long(auth.getValue("auserid")));
            info.setSupportedfileformats(fmt);
   
            getMainDAO().saveOrUpdate(info);
   
            Mediastore store=new Mediastore();
            store.setMediacontent(Hibernate.createBlob(auth.getAuthStream("mediasource"),info.getFilesize().intValue()));
            store.setMediainfo(info);
            getMainDAO().saveOrUpdate(store);
            mav.addObject("status","UPLOADCOMPLETE");

         } catch (org.springframework.dao.DataIntegrityViolationException die) {
            mav.addObject("status","INTEGRITYEXCEPTION");
         }

         // clear out media session lists
         clearSessionLists();
         return mav;

      } catch (Exception e) {
         throw new FORMSException("Form Upload Error" + Stack2string.getString(e));
      }

   }

   // Empty session-stored forms lists so data are repulled
   private void clearSessionLists() {   
      try {
         getAuth().objectToAuthStream("mediasource","");
         getAuth().setSessionAttribute("mediahalist",null);
         getAuth().setSessionAttribute("mediainfolist",null);
         getAuth().setSessionAttribute("minfomap",null);
      } catch (Exception e) { }
   }  

}


