 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.io.*;
import java.util.*;
//import java.text.*;
//import org.apache.regexp.RE;
//import org.apache.commons.lang.StringUtils;
//import com.sas.sasserver.sclfuncs.*;
//import com.sas.sasserver.dataset.*;
//import com.sas.rmi.*;
//import com.sas.sasserver.submit.*;
//import org.apache.commons.lang.StringUtils;
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang.ArrayUtils.*;
//import java.util.regex.*;
//import javax.crypto.*;
//import javax.crypto.spec.*;
//import java.security.*;
//import java.security.spec.*;
//import uk.ltd.getahead.dwr.WebContext;
//import uk.ltd.getahead.dwr.WebContextFactory;
//import javax.servlet.*;
//import javax.servlet.http.*;
//import org.apache.velocity.app.*;
//import org.apache.velocity.*;
//import org.apache.commons.httpclient.util.URIUtil;


public class VerifyConnectionDwr extends FORMSSsaServiceClientDwr {

   public String checkStatus(String codeword) {

      try {

         // Verify authorization, then return public key as string
         FORMSAuth auth=this.getAuth();
         if (auth.isAuth(this.getRequest(),this.getResponse()) && auth.getValue("rightslevel").equalsIgnoreCase("admin")) {

            EmbeddedinstDAO embeddedinstDAO=(EmbeddedinstDAO)this.getContext().getBean("embeddedinstDAO");
            Embeddedinst einst=embeddedinstDAO.getRecordByCodeword(codeword);
            if (einst==null) {
               return "ERROR - could not pull installation information from database";
            } 
            SsaServiceSystemParms parms=new SsaServiceSystemParms();
            parms.setBeanname("verifyConnectionServiceTarget");
            parms.setMethodname("checkStatus");

            SsaServiceStatusResult result=(SsaServiceStatusResult)this.submitServiceRequest(einst,parms);

            if (result.getStatus().intValue()==FORMSServiceConstants.OK) {
               return "Successfully connected to remote installation";
            } else {
               return "ERROR:  " + new FORMSServiceConstants().getFieldName(result.getStatus().intValue());
            }

         } else {

            return "Authentication Error.  Re-login to FORMS and retry.";

         }

      } catch (Exception e) {

         FORMSLog.append(this.getSyswork(),Stack2string.getString(e));
         
         return "PROGRAM EXCEPTION:  See FORMSLog\n\n" +
                "NOTE:  This message is a catch-all exception and may be due to\n" +
                "       a network outage or remote server inavailability.";

      }
         
   }

}


