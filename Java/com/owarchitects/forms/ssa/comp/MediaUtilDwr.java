 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.io.*;
import java.util.*;

public class MediaUtilDwr extends FORMSDwr {

   // Empty session-stored forms lists so data are repulled
   private void clearSessionLists() {   
      try {
         getAuth().setSessionAttribute("mediahalist",new ArrayList());
         getAuth().setSessionAttribute("mediainfolist",new ArrayList());
      } catch (Exception e) { }
   }   

   // Move form to different category
   public String moveFile(boolean islocal,String lstdid,String mediainfoid,String mediahaid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Verify authorization, then return public key as string
      FORMSAuth auth=this.getAuth();
      if (auth.isAuth(this.getRequest(),this.getResponse())) {

         try {
            if (islocal) {
               // LOCAL INSTALLATION FORMS
               Mediaha newha=(Mediaha)getMainDAO().execUniqueQuery(
                  "select ha from Mediaha ha where ha.mediahaid=" + mediahaid
               );
               Mediainfo minfo=(Mediainfo)getMainDAO().execUniqueQuery(
                  "select ft from Mediainfo ft " +
                     "where ft.mediainfoid=" + mediainfoid
               );
               Mediaha oldha=(Mediaha)getMainDAO().execUniqueQuery(
                  "select ha from Mediaha ha where ha.mediahaid=" + new Long(minfo.getPmediahaid()).toString()
               );
               if (newha==null || minfo==null || oldha==null) {
                  throw new FORMSException("query returned null value");
               }
               // Verify that newha && oldha have same root (cannot change visibility here)
               if (newha.getVisibility()!=oldha.getVisibility()) {
                  return "NOMODVIS";
               }
               minfo.setPmediahaid(new Long(mediahaid));
               getMainDAO().saveOrUpdate(minfo);
               clearSessionLists();
               return "OK";
            } 
            return "ERROR";

         } catch (Exception e) {
            return "ERROR";
         }

      } else {
         return "ERROR";
      }
      
   }

}


