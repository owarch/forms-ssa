 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 * JoinInstParms.java - Input parameters for JoinInstService - MRH 03/2007
 */

package com.owarchitects.forms.ssa.comp;

import java.util.*;
import java.io.*;

public class JoinInstParms implements Serializable {

   protected String acronym;
   protected String description;
   protected String codeword;
   protected String connectstring;
   protected String url;
   protected String createkey;
   protected String instkey;

   //
   //

   public void setAcronym(String acronym) {
      this.acronym=acronym;
   }

   public String getAcronym() {
      return acronym;
   }

   //
   //

   public void setDescription(String description) {
      this.description=description;
   }

   public String getDescription() {
      return description;
   }

   //
   //

   public void setCodeword(String codeword) {
      this.codeword=codeword;
   }

   public String getCodeword() {
      return codeword;
   }
   //
   //

   public void setConnectstring(String connectstring) {
      this.connectstring=connectstring;
   }

   public String getConnectstring() {
      return connectstring;
   }

   //
   //

   public void setUrl(String url) {
      this.url=url;
   }

   public String getUrl() {
      return url;
   }

   //
   //

   public void setCreatekey(String createkey) {
      this.createkey=createkey;
   }

   public String getCreatekey() {
      return createkey;
   }

   //
   //

   public void setInstkey(String instkey) {
      this.instkey=instkey;
   }

   public String getInstkey() {
      return instkey;
   }

}

