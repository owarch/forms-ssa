 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * GroupUtilController.java - User utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.security.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.*;
import javax.crypto.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;


public class GroupUtilController extends FORMSController {

   boolean isbadrequest;

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Use class-name based view
      mav=new ModelAndView("GroupUtil");
      isbadrequest=true;
      String spath=request.getServletPath();

      try {

         // Add new group interface 
         if (spath.indexOf("newgroup.")>=0) {

            isbadrequest=false;
            mav.addObject("status","NEWGROUP");
            return mav;

         // Submit new group to system
         } else if (spath.indexOf("newgroupsubmit.")>=0) {

            String status=newGroupSubmit();
            mav.addObject("status",status);
            return mav;

         // Modify group interface
         } else if (spath.indexOf("modifygroup.")>=0) {

            modifyGroup();
            isbadrequest=false;
            mav.addObject("status","MODIFYGROUP");
            return mav;

         // Submit modified group
         } else if (spath.indexOf("modifygroupsubmit.")>=0) {

            String status=modifyGroupSubmit();
            mav.addObject("status",status);
            return mav;

         // Delete specified group 
         } else if (spath.indexOf("removegroupsubmit.")>=0) {

            String status=removeGroupSubmit();
            mav.addObject("status",status);
            return mav;

         // Group membership selection screen
         } else if (spath.indexOf("groupmembership.")>=0) {

            groupMembershipSelection();
            isbadrequest=false;
            mav.addObject("status","GROUPMEMBERSHIP");
            return mav;

         } 

         out.println(spath);
         isbadrequest=false;
         return null;

      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Submit new group
   private String newGroupSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

            String status=null;
            String groupacr=getRequest().getParameter("groupacr").toUpperCase();
            String groupdesc=getRequest().getParameter("groupdesc");
            FORMSAuth auth=getAuth();
            Long auserid=new Long(auth.getValue("auserid"));
            
            // Make sure group acronym is not duplicated within user
            List l;
            l=getMainDAO().execQuery(
                  "select rr from Analystgroups rr where " +
                      "rr.groupacr='" + groupacr + "' and rr.createuser=" + auserid
               );
            if (l.size()>0) {
               status="DUPLICATEGROUP";
               return status;
            }
            // If not duplicate, add to database
            Analystgroups agroup=new Analystgroups();
            agroup.setGroupacr(groupacr);
            agroup.setGroupdesc(groupdesc);
            agroup.setCreateuser(auserid);
            getMainDAO().saveOrUpdate(agroup);
            isbadrequest=false;
            getMAV().addObject("groupacr",groupacr);
            status="GROUPADDED";
            return status;

   }

   // Modify group interface
   private String modifyGroup() throws FORMSException,FORMSKeyException,FORMSSecurityException {

            String status=null;
            long agroupid=new Long(getRequest().getParameter("agroupid")).longValue();
            FORMSAuth auth=getAuth();
            Long auserid=new Long(auth.getValue("auserid"));
            
            // Make sure group acronym is not duplicated witin user
            Analystgroups rr=(Analystgroups)getMainDAO().execUniqueQuery(
                  "select rr from Analystgroups rr where " +
                      "rr.agroupid=:agroupid and rr.createuser=:auserid" 
                   ,new String[] { "agroupid","auserid" }
                   ,new Object[] { agroupid,auserid }   
               );
            if (rr==null) {
               status="GROUPPULLERROR";
               return status;
            }
            getMAV().addObject("agroupid",agroupid);
            getMAV().addObject("groupacr",rr.getGroupacr());
            getMAV().addObject("groupdesc",rr.getGroupdesc());
            status="MODIFYGROUP";
            return status;

   }

   // Submit modified group
   private String modifyGroupSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

            String status=null;
            String groupacr=getRequest().getParameter("groupacr").toUpperCase();
            String groupdesc=getRequest().getParameter("groupdesc");
            long agroupid=new Long(getRequest().getParameter("agroupid")).longValue();
            FORMSAuth auth=getAuth();
            Long auserid=new Long(auth.getValue("auserid"));
            
            // Make sure group acronym is not duplicated witin user
            List l=getMainDAO().execQuery(
                  "select rr from Analystgroups rr where " +
                      "rr.agroupid!=:agroupid and rr.createuser=:auserid and rr.groupacr=:groupacr" 
                   ,new String[] { "agroupid","auserid","groupacr" }
                   ,new Object[] { agroupid,auserid,groupacr }   
               );
            if (l.size()>0) {
               status="DUPLICATEGROUP";
               return status;
            }
            // If not duplicate, add to database
            Analystgroups agroup=(Analystgroups)getMainDAO().execUniqueQuery(
               "select a from Analystgroups a where a.agroupid=" + agroupid);
            agroup.setGroupacr(groupacr);
            agroup.setGroupdesc(groupdesc);
            getMainDAO().saveOrUpdate(agroup);
            isbadrequest=false;
            status="GROUPMODIFIED";
            return status;

   }

   // Delete group
   private String removeGroupSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

            String status=null;
            long agroupid=new Long(getRequest().getParameter("agroupid")).longValue();
            FORMSAuth auth=getAuth();
            Long auserid=new Long(auth.getValue("auserid"));
            
            // Make sure group acronym is not duplicated witin level and site/study
            Analystgroups rr=(Analystgroups)getMainDAO().execUniqueQuery(
                  "select rr from Analystgroups rr where " +
                      "rr.agroupid=:agroupid and rr.createuser=:auserid" 
                   ,new String[] { "agroupid","auserid" }
                   ,new Object[] { agroupid,auserid }   
               );
            if (rr==null) {
               status="GROUPPULLERROR";
               return status;
            }
            if (rr.getGroupacr().equalsIgnoreCase("ALLUSERS")) {
               status="NOMODIFYALLUSERS";
               return status;
            }
            isbadrequest=false;
            getMainDAO().delete(rr);
            status="GROUPDELETED";
            return status;

   }

   // Group membership selection screen
   private void groupMembershipSelection() throws FORMSException,FORMSKeyException,FORMSSecurityException {

            long agroupid=new Long(getRequest().getParameter("agroupid")).longValue();

            Analystgroups thisgroup=(Analystgroups)getMainDAO().execUniqueQuery(
               "select a from Analystgroups a where a.agroupid=" + agroupid
               );

            // PULL IN USER LIST
            List ulist=getMainDAO().execQuery("select distinct a from Allusers a " + 
                                            " where not a.isdeleted=true " + 
                                            " order by a.userdesc");

            // see if users are already assigned to this group
            Iterator i=ulist.iterator();
            ArrayList newlist=new ArrayList();
            while (i.hasNext()) {
               Allusers u=(Allusers)i.next();
               // Remove self from list
               if (u.getAuserid()==(new Long(getAuth().getValue("auserid")).longValue())) {
                  i.remove();
                  continue;
               }
               // see if user has access to CMS.  If not, remove from list

               if (!getPermHelper().hasAnalystSysPerm(new Long(u.getAuserid()).toString(),"ACCESS")) {
                  i.remove();
                  continue;
               }
               Analystgroupuserassign assn=(Analystgroupuserassign)getMainDAO().execUniqueQuery(
                  "select a from Analystgroupuserassign a where a.group.agroupid=" + agroupid +
                     " and a.user.auserid=" + u.getAuserid()
                  );
               boolean isassigned=false;
               if (assn!=null) isassigned=true;
               HashMap map=new HashMap();
               map.put("allusers",u);
               map.put("isassigned",isassigned);
               map.put("agroupid",agroupid);
               newlist.add(map);
            }
            ListResult result=new ListResult();
            result.setList(newlist);
            this.getSession().setAttribute("iframeresult",result);
            getMAV().addObject("groupacr",thisgroup.getGroupacr());
            getMAV().addObject("groupdesc",thisgroup.getGroupdesc());

   }


}


