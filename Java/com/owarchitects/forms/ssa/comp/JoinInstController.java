 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * JoinInstController.java - Installation Join Interface
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import javax.servlet.http.*;
import org.apache.commons.lang.ArrayUtils;

public class JoinInstController extends FORMSController {

   public ModelAndView submitRequest() throws FORMSException {

      try {

         ModelAndView mav=new ModelAndView();
         HttpServletRequest request=this.getRequest();
         HttpServletResponse response=this.getResponse();
         PrintWriter out=this.getWriter();

         // Begin processing
         JoinInstResult result=processData();
         // Write result to iframe sessionobject (retrieved by iframe)
         this.getSession().setAttribute("iframeresult",result);
   
         // Direct output based on result
         if (result.getStatus().equalsIgnoreCase("OK")) {
  
            result.addToMAV(mav,new String[] { "status" });
            return mav;
   
         } else if (result.getStatus().equalsIgnoreCase("REDIRECT")) {
   
            safeRedirect("permission.err");
            return null;
   
         } else {
   
            return null;

         }
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Main processing method
   private JoinInstResult processData() throws FORMSException {

      JoinInstResult result=new JoinInstResult();

      try {

        FORMSAuth auth=this.getAuth();

        LinkedinstDAO linkedinstDAO=(LinkedinstDAO)this.getContext().getBean("linkedinstDAO");
        EmbeddedinstDAO embeddedinstDAO=(EmbeddedinstDAO)this.getContext().getBean("embeddedinstDAO");

        //this.bindHibernateSession();

        ArrayList<Map> passList=new ArrayList<Map>();
        List l=null;
        
        boolean isadmin=false;
        String rightslevel="NONE";
       
        // Retrieve list of linkedinst objects
        l=linkedinstDAO.getTable("Linkedinst");
        
        Iterator i=l.iterator();
        while (i.hasNext()) {
           // Create map of linkedinst & embeddedinst objects
           Map<String,Object> map = new HashMap<String,Object>();
           // NOTE:  May retrieve either linkedinst or studyrights object
           Object o=i.next();
           try {
              Linkedinst lsts=(Linkedinst)o;
              // Retrieve corresponding embeddedinst object
              Embeddedinst ests=embeddedinstDAO.getRecordByAcronym(lsts.getAcronym());
              map.put("linkedinst",lsts);
              map.put("embeddedinst",ests);
              // add map to return object list
              passList.add(map);
           } catch (Exception e) { }
        }   

        //this.releaseHibernateSession();

        result.setStatus("OK");
        result.setList(passList);

        return result;

      } catch (Exception e) {

        throw new FORMSException("FORMS Service exception:  " + Stack2string.getString(e));

      }

   }
   
}

