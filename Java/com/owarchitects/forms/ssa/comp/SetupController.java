 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * SetupController.java -  SSA Database Setup Program
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.io.*;
import java.lang.*;
import javax.servlet.http.*;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.*;
import org.springframework.web.context.support.*;
import org.springframework.context.support.AbstractApplicationContext;
import org.apache.axis2.context.*;
import org.apache.axis2.client.*;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.io.FileUtils;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.orm.hibernate3.HibernateJdbcException;
import org.apache.tools.ant.*;
import org.springframework.orm.hibernate3.LocalSessionFactoryBean;
import org.hibernate.*;
import org.hibernate.metadata.*;
import org.springframework.beans.factory.FactoryBean; 
import java.lang.reflect.*;
import org.hibernate.persister.entity.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import java.security.spec.RSAPublicKeySpec;
import javax.security.auth.x500.*;
import javax.security.cert.*;
import org.bouncycastle.x509.*;
import org.bouncycastle.jce.provider.*;
import org.apache.tools.ant.loader.*;
import java.math.BigInteger;


public class SetupController extends FORMSController {

   private String dbpw;
   private String kspw;
   private String fkpw;
   private String user;
   private String passwd;
   private boolean isRunning=false;

   // Override superclass handleRequestInternal method for setup, rather than the usual submitRequest method
   protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws FORMSException {

        try {

           this.request=request;
           this.session=request.getSession();
           this.response=response;
           this.out=response.getWriter();
           this.mav=new ModelAndView();
           wp=SessionObjectUtil.getWinPropsObj(request,response);
           wp.setWinNameMissOk("Y");
     
           // Immediately expire headers
           response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
           response.setHeader("Pragma","no-cache"); //HTTP 1.0
           response.setDateHeader ("Expires", -1); //prevents caching at the proxy server
   
           // add WinProps objects to view
           wp.addViewObjects(mav);

           if (request.getParameter("dbpw")==null) {

              if (!isSetupNeeded()) {
                 safeRedirect("login.do");
                 return null;
              }

              mav.addObject("status","SETUPPARMS");
              return mav;

           } else {

              dbpw=request.getParameter("dbpw");
              kspw=request.getParameter("kspw");
              fkpw=request.getParameter("fkpw");
              user=request.getParameter("username");
              passwd=request.getParameter("passwd");


              // Read mainDB into context using dbpw
              SessionObjectUtil.readMainDBIntoContext(getSession(),dbpw);

              this.syswork=null;
              this.sysconn=null;
              // Pull syswork, sysconn from session attribute preferably, alternately from cookie.
              try {
                 this.syswork=request.getSession().getAttribute("syswork").toString();
              } catch (Exception swe) {}
              if (syswork==null || syswork.length()<1) {
                 this.syswork=Base64.decode(this.getCookieValue(Base64.encode("workdir")).replaceAll("=",""));
              }   
              response.addCookie(new Cookie(Base64.encode("workdir"),Base64.encode(syswork)));
              try {
                 this.sysconn=request.getSession().getAttribute("sysconn").toString();
              } catch (Exception swe) {}
              if (sysconn==null || sysconn.length()<1) {
                 this.sysconn=Base64.decode(this.getCookieValue(Base64.encode("sysconn")).replaceAll("=",""));
              }   
              // formstime used for sysconn and auth
              String formstime=new Long(System.currentTimeMillis()).toString();
   
              if (sysconn==null || sysconn.length()<1) {
                 Sysconn sconn=new Sysconn();
                 sconn.setUsername(user);
                 sconn.setPassword(passwd);
                 sconn.setFormstime(formstime);
                 if (sconn.getUsername()!=null) {
                    try {
                       sysconn=EncryptUtil.encryptString(ObjectXmlUtil.objectToXmlString(sconn),
                                           SessionObjectUtil.getInternalKeyObj(session));
                       request.getSession().setAttribute("sysconn",sysconn);
                       this.refreshContext();
                    } catch (Exception fex) {
                       // ok, keystore may not yet be set up
                    }
                 } 
              }
      
              // Get context
              session=request.getSession();

              try {
   
                 // Create mav used by override method
                 this.mav=new ModelAndView();
                 mav=submitRequest();
   
                 if (mav!=null) {
                    // add WinProps objects to view
                    wp.addViewObjects(mav);
                    // add ServletPath (spath) object to view
                    mav.addObject("spath",request.getServletPath().substring(1));
                 }    
                 return mav;
   
              } catch (Exception ke) {
                    
                 throw new FORMSException("FORMSController exception (submitRequest exception) - <BR>",ke);
              }

           } 

        } catch (Exception e) {

           throw new FORMSException("FORMSController exception - <BR>",e);

        }

   }     

   public ModelAndView submitRequest() throws FORMSException {

      if (request.getParameter("dbpw")!=null) {

         if (!isRunning) {

            SessionObjectUtil.refreshApplicationContext(getSession(),dbpw);

            // Run database creation thread
            SetupThread proc=new SetupThread(getContext(),getMainDAO(),dbpw);
            proc.start();
         }   

         mav.addObject("status","THREAD");
         return mav;

      } else {

         mav.addObject("status","DEFAULT");
         return mav;

      }

    }


    // SetupThread - Performs actual setup as a thread

    private class SetupThread extends Thread {

       com.owarchitects.forms.commons.db.MainDAO maindao;
       AbstractApplicationContext context;

       IframeReloaderResult result;
       StringBuilder sb;
       String dbpassword;

       public SetupThread(AbstractApplicationContext context,com.owarchitects.forms.commons.db.MainDAO maindao,String dbpassword) {
          this.context=context;
          this.maindao=maindao;
          this.dbpassword=dbpassword;
       }

       public void run() {

          if (isRunning) return;

          String pathVar=session.getServletContext().getRealPath("/");

          isRunning=true;
          sb=new StringBuilder();
          sb.append("\n<span style='color:#00AA00'><br>Begin building database:<br><br></span>\n");
          result=new IframeReloaderResult();
          // Only need to set this once, since iframeresult will hold reference to object
          // rather than copy of it
          getSession().setAttribute("iframeresult",result);

          try {

             result.setStatus("RELOAD");

             sbWrite("Create SSA encryption keys: <br><br>\n\n");

             FORMSConfig config=SessionObjectUtil.getConfigObj(getSession());
             String ksfile=config.getKeyStore();

             // Update ksfile with user-entered password
             String configf = pathVar + File.separator + "Conf" + 
                                File.separator + "FORMS.conf";
             File configfile=new File(configf);                   
             String configfstr=FileUtils.readFileToString(configfile,"UTF-8");
             configfstr=configfstr.replaceAll("KeyStorePassword: [ \\S]*","KeyStorePassword:  " + kspw);
             FileUtils.writeStringToFile(configfile,configfstr,"UTF-8");
             // Clear config session object
             session.setAttribute("formsconfig",null);

             // Read mainDB into context using dbpw
             SessionObjectUtil.updateContextCreateDatabase(getSession(),dbpw);

             // Back up existing keystore file if it exists 
             File ksf=new File(getAppPath() + File.separator + ksfile);
             if (ksf.exists()) {
                ksf.renameTo(new File(ksf.getParentFile(),ksf.getName().replaceFirst("\\.",".bak" + new Long(System.currentTimeMillis()).toString() + ".")));
             }
             // Create keystore & FORMSKey
             KeyStore ks=null;
             SecretKey formskey=null;
             try {
                sbWrite("\nCreating FORMSKey");
              
                ks=java.security.KeyStore.getInstance("JCEKS");
                KeyGenerator kg = KeyGenerator.getInstance("DESede");
                formskey=kg.generateKey();
                ks.load(null,kspw.toCharArray());
                ks.setKeyEntry("formskey",formskey,fkpw.toCharArray(),null);
                FileOutputStream f = new FileOutputStream(ksf);
                ks.store(f,kspw.toCharArray());
       
                sbWrite("....Done<br><br>\n\n");
             } catch(Exception e) {
                sb.append(Stack2string.getString(e) + "<br><br>");
                sbWrite("\n\nERROR:  Couldn't create FORMSKey<br><br>");
                stopNow(false);
                return;
             }
       
             // Create forms internal key
             SecretKey formsinternal=null;
             try {
                sbWrite("\nCreating FORMSInternal Key");
              
                //ks=java.security.KeyStore.getInstance("JCEKS");
                //ks.load(null,kspw.toCharArray());
                KeyGenerator kg = KeyGenerator.getInstance("DESede");
                formsinternal=kg.generateKey();
                ks.setKeyEntry("formsinternal",formsinternal,kspw.toCharArray(),null);
                FileOutputStream f = new FileOutputStream(ksf);
                ks.store(f,kspw.toCharArray());
       
                sbWrite("....Done<br><br>\n\n");
             } catch(Exception e) {
                sbWrite(Stack2string.getString(e));
                sbWrite("\n\nERROR:  Couldn't create FORMSInternal Key");
                stopNow(false);
                return;
             }
       
             // Create administrator account keypair
             PublicKey pubkey=null;
             PrivateKey privkey=null;
             try {
                sbWrite("\nCreating administrator account KeyPair");

                KeyPairGenerator kpg=KeyPairGenerator.getInstance("RSA");
                kpg.initialize(512);
                KeyPair kp=kpg.generateKeyPair();
                pubkey=kp.getPublic();
                privkey=kp.getPrivate();
                ks.setKeyEntry(user + "_private",privkey,passwd.toCharArray(),getCertChain());
                ks.setKeyEntry(user + "_public",pubkey,"public".toCharArray(),null);
                FileOutputStream f = new FileOutputStream(ksf);
                ks.store(f,kspw.toCharArray());
       
                sbWrite("....Done<br><br>\n\n");
             
             } catch (Exception e) {
                sbWrite(Stack2string.getString(e));
                sbWrite("\n\nERROR:  Couldn't create administrator key pair<br><br>");
                System.exit(1);
                stopNow(false);
                return;
             }

             // Start ant build process 
             sbWrite("Building SSA embedded database (this may take several minutes):  \n");

             ComboPooledDataSource eds=(ComboPooledDataSource)context.getBean("embeddedDataSource");
             eds.setJdbcUrl(eds.getJdbcUrl().replace("$appPath",pathVar));
             eds.setJdbcUrl(eds.getJdbcUrl().replace("create=false","create=true"));

             LocalSessionFactoryBean elsb=(LocalSessionFactoryBean)context.getBean("&sessionFactory");

             elsb.createDatabaseSchema();

             sbWrite(".....DONE<br><br>\n");
       

             sbWrite("Building SSA main database (this may take several minutes):  \n");

             // Update database connection strings
             ComboPooledDataSource mds=(ComboPooledDataSource)context.getBean("mainDataSource");
             mds.setJdbcUrl(mds.getJdbcUrl().replace("$appPath",pathVar));
             mds.setJdbcUrl(mds.getJdbcUrl().replace("create=false","create=true"));
             mds.setJdbcUrl(mds.getJdbcUrl().replace("bootPassword=[^<;][^<;]*","bootPassword=" + dbpassword));

             refreshContext();

             LocalSessionFactoryBean mlsb=(LocalSessionFactoryBean)context.getBean("&mainSessionFactory");

             mlsb.createDatabaseSchema();

             sbWrite(".....DONE<br><br>\n");

             sbWrite("\nBegin populating database: <br><br>\n\n");
             sbWrite("\nCreate administrator account records");

       
             // Populate database
             try {
       
                Embeddedusers euser=new Embeddedusers();
       
                AccessDAO accessDAO=(AccessDAO)context.getBean("accessDAO");
                Access a=new Access();
                a.setFkpassword(EncryptUtil.encryptString(fkpw,formsinternal));
                a.setDbpassword(EncryptUtil.encryptString(dbpw,formsinternal));
                accessDAO.save(a);
       
                LocalusersDAO localusersDAO=(LocalusersDAO)context.getBean("localusersDAO");
                AllusersDAO allusersDAO=(AllusersDAO)context.getBean("allusersDAO");
                AllinstDAO allinstDAO=(AllinstDAO)context.getBean("allinstDAO");
                EmbeddedusersDAO embeddedusersDAO=(EmbeddedusersDAO)context.getBean("embeddedusersDAO");
                MainDAO mainDAO=(MainDAO)context.getBean("mainDAO");
       
                euser.setUsername(user);
                euser.setPassword(PasswordUtil.pwEncrypt(new String(passwd)));
                euser.setIsdisabled(false);
                euser.setBadlogincount(new Integer(0));
                euser.setFkpassword(EncryptUtil.encryptString(fkpw,pubkey));
                euser.setDbpassword(EncryptUtil.encryptString(dbpw,pubkey));
                embeddedusersDAO.save(euser);
       
                Localusers luser=new Localusers();
                luser.setUsername(user);
                luser.setPassworddt(new Date());
                luser.setUsernumber("999");
                luser.setFullname("FORMS Administrator");
                luser.setEmailaddr("");
                luser.setIsadmin(true);
                luser.setIstemppassword(false);
                luser.setIsdeleted(false);
                localusersDAO.save(luser);
       
                Allinst ainst=new Allinst();
                Allusers auser=new Allusers();
       
                ainst.setIslocal(true);
                allinstDAO.save(ainst);
       
                auser.setAllinst(ainst);
                auser.setLuserid(luser.getLuserid());
                auser.setUserdesc(luser.getUsername() + " [LOCAL]");
                auser.setIsdeleted(false);
                allusersDAO.save(auser);
       
                //
       
                sbWrite("....Done<br><br>\n\n");
       
                sbWrite("\nCreate ALLUSERS system role");
       
                Resourceroles allusersrole=new Resourceroles();
                allusersrole.setRolelevel(VisibilityConstants.SYSTEM);
                allusersrole.setRoleacr("ALLUSERS");
                allusersrole.setRoledesc("All Users");
                mainDAO.save(allusersrole);
       
                Rroleuserassign rroleassign=new Rroleuserassign();
                rroleassign.setUser(auser);
                rroleassign.setRole(allusersrole);
                mainDAO.save(rroleassign);
       
                sbWrite("....Done<br><br>\n\n");
       
                /////
                /////
       
                sbWrite("\nCreate Permission categories");
                
                Permissioncats p1=new Permissioncats();
                p1.setPcatvalue(PcatConstants.SYSTEM);
                p1.setPcatscope(PcatscopeConstants.GLOBAL);
                p1.setPcatdesc("System Permissions");
                p1.setPcatorder(1);
                mainDAO.save(p1);
       
                Permissioncats p2=new Permissioncats();
                p2.setPcatvalue(PcatConstants.SITE);
                p2.setPcatscope(PcatscopeConstants.GLOBAL);
                p2.setPcatdesc("Site-Level Permissions");
                p2.setPcatorder(2);
                mainDAO.save(p2);
       
                Permissioncats p3=new Permissioncats();
                p3.setPcatvalue(PcatConstants.STUDY);
                p3.setPcatscope(PcatscopeConstants.GLOBAL);
                p3.setPcatdesc("Study-Level Permissions");
                p3.setPcatorder(3);
                mainDAO.save(p3);
       
                Permissioncats p4=new Permissioncats();
                p4.setPcatvalue(PcatConstants.FORM_DATATABLE);
                p4.setPcatscope(PcatscopeConstants.RESOURCE);
                p4.setPcatdesc("Form/DataTable Permissions");
                p4.setPcatorder(4);
                mainDAO.save(p4);
       
                Permissioncats p5=new Permissioncats();
                p5.setPcatvalue(PcatConstants.FORMTYPE);
                p5.setPcatscope(PcatscopeConstants.RESOURCE);
                p5.setPcatdesc("Formtype Permissions");
                p5.setPcatorder(5);
                mainDAO.save(p5);
       
                Permissioncats p6=new Permissioncats();
                p6.setPcatvalue(PcatConstants.REPORT);
                p6.setPcatscope(PcatscopeConstants.RESOURCE);
                p6.setPcatdesc("Reporting Program Permissions");
                p6.setPcatorder(6);
                mainDAO.save(p6);
       
                Permissioncats p7=new Permissioncats();
                p7.setPcatvalue(PcatConstants.LETTER);
                p7.setPcatscope(PcatscopeConstants.RESOURCE);
                p7.setPcatdesc("Form Letter Permissions");
                p7.setPcatorder(7);
                mainDAO.save(p7);
       
                Permissioncats p8=new Permissioncats();
                p8.setPcatvalue(PcatConstants.MEDIA);
                p8.setPcatscope(PcatscopeConstants.RESOURCE);
                p8.setPcatdesc("Document/Media Permissions");
                p8.setPcatorder(8);
                mainDAO.save(p8);
       
                Permissioncats p11=new Permissioncats();
                p11.setPcatvalue(PcatConstants.ANALYST_SYSTEM);
                p11.setPcatscope(PcatscopeConstants.GLOBAL);
                p11.setPcatdesc("Researcher/Analyst Content Management - System Permissions");
                p11.setPcatorder(11);
                mainDAO.save(p11);
       
                Permissioncats p12=new Permissioncats();
                p12.setPcatvalue(PcatConstants.ANALYST_RESOURCE);
                p12.setPcatscope(PcatscopeConstants.RESOURCE);
                p12.setPcatdesc("Researcher/Analyst Content Management - Resource Permissions");
                p12.setPcatorder(12);
                mainDAO.save(p12);
       
       
                sbWrite("....Done<br><br>\n\n");
       
                sbWrite("\nCreate Permissions");
       
                com.owarchitects.forms.commons.db.Permissions perm;
       
                ////
                ////
                ////
                ////
       
                int pos;
                
                pos=1;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("ACCESSCONTENT");
                perm.setPermdesc("Access Site/Study Related Content");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("INTERNALVIEW");
                perm.setPermdesc("Internal User View");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("ACCTCREATE");
                perm.setPermdesc("Create new user accounts");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("ACCTMOD");
                perm.setPermdesc("Modify/Delete Existing Accounts");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("ACCTPWRESET");
                perm.setPermdesc("Reset user passwords");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("ADDSTUDY");
                perm.setPermdesc("Add New Sites/Studies");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("MODSTUDY");
                perm.setPermdesc("Modify Site/Study Attribites (incl Archive & Delete)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("INSTMGT");
                perm.setPermdesc("Manage Remote Installations");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("INVALIDMGT");
                perm.setPermdesc("Manage Invalid Logins Table");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("PERMSMGT");
                perm.setPermdesc("Manage com.owarchitects.forms.commons.db.Permissions");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                ////
                ////
                ////
                ////
                
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("ROLEPERM");
                perm.setPermdesc("ROLES:  Manage Roles/Assign Permissions (System-Level Roles)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("FTYPECREATE");
                perm.setPermdesc("FORMTYPES:  Create New FormTypes (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("FTYPEEDITOTH");
                perm.setPermdesc("FORMTYPES:  Edit Attrs for FormTypes Created by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("FTYPEDELOTH");
                perm.setPermdesc("FORMTYPES:  Delete FormTypes Created by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("FTYPEPERMOTH");
                perm.setPermdesc("FORMTYPES:  Admin Perms for FormTypes Created by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("DTFORMCAT");
                perm.setPermdesc("FORMS:  Edit Form Categories (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("DTFORMUPLOAD");
                perm.setPermdesc("FORMS:  Upload New Form (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("FTYPEUPLOADNEW");
                perm.setPermdesc("FORMS:  Upload new FormType for forms Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("DTFORMEDITOTH");
                perm.setPermdesc("FORMS:  Edit Attrs for Forms Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("DTFORMMOVEOTH");
                perm.setPermdesc("FORMS:  Move Forms Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("DTFORMDELOTH");
                perm.setPermdesc("FORMS:  Delete/Replace Forms Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("DTFORMPERMOTH");
                perm.setPermdesc("FORMS:  Admin Perms for Forms Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("DTFORMPERMPROF");
                perm.setPermdesc("FORMS:  Create/Edit Form Permission Profiles (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("RPTCAT");
                perm.setPermdesc("REPORTS:  Edit Report Categories (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("RPTUPLOAD");
                perm.setPermdesc("REPORTS:  Upload New Reports (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("RPTEDITOTH");
                perm.setPermdesc("REPORTS:  Edit Attrs for Rpts Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("RPTMOVEOTH");
                perm.setPermdesc("REPORTS:  Move Rpts Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("RPTDELOTH");
                perm.setPermdesc("REPORTS:  Delete/Replace Rpts Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("RPTPERMOTH");
                perm.setPermdesc("REPORTS:  Admin Perms for Rpts Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("RPTPERMPROF");
                perm.setPermdesc("REPORTS:  Admin Report Permission Profiles (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("LTRCAT");
                perm.setPermdesc("LETTERS:  Edit Form Letter Categories (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("LTRUPLOAD");
                perm.setPermdesc("LETTERS:  Upload New Form Letters (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("LTREDITOTH");
                perm.setPermdesc("LETTERS:  Edit Attrs for Ltrs Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("LTRMOVEOTH");
                perm.setPermdesc("LETTERS:  Move Ltrs Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("LTRDELOTH");
                perm.setPermdesc("LETTERS:  Delete/Replace Ltrs Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("LTRPERMOTH");
                perm.setPermdesc("LETTERS:  Admin Perms for Ltrs Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("LTRPERMPROF");
                perm.setPermdesc("LETTERS:  Admin Letter Permission Profiles (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("MEDCAT");
                perm.setPermdesc("MEDIA:  Edit Document Categories (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("MEDUPLOAD");
                perm.setPermdesc("MEDIA:  Upload New Documents (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("MEDEDITOTH");
                perm.setPermdesc("MEDIA:  Edit Attrs for Docs Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("MEDMOVEOTH");
                perm.setPermdesc("MEDIA:  Move Docs Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("MEDDELOTH");
                perm.setPermdesc("MEDIA:  Delete/Replace Docs Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("MEDPERMOTH");
                perm.setPermdesc("MEDIA:  Admin Perms for Docs Uploaded by Others (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p1);
                perm.setPermacr("MEDPERMPROF");
                perm.setPermdesc("MEDIA:  Administer Media Permissions Profiles (System-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                ////
                ////
                ////
                ////
                
                pos=1;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("ROLEPERM");
                perm.setPermdesc("ROLES:  Manage Roles/Assign Permissions (Site-Level Roles)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("FTYPECREATE");
                perm.setPermdesc("FORMTYPES:  Create New FormTypes (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("FTYPEEDITOTH");
                perm.setPermdesc("FORMTYPES:  Edit Attrs for FormTypes Created by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("FTYPEDELOTH");
                perm.setPermdesc("FORMTYPES:  Delete FormTypes Created by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("FTYPEPERMOTH");
                perm.setPermdesc("FORMTYPES:  Admin Perms for FormTypes Created by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("DTFORMCAT");
                perm.setPermdesc("FORMS:  Edit Form Categories (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("DTFORMUPLOAD");
                perm.setPermdesc("FORMS:  Upload New Form (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("FTYPEUPLOADNEW");
                perm.setPermdesc("FORMS:  Upload new FormType for forms Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("DTFORMEDITOTH");
                perm.setPermdesc("FORMS:  Edit Attrs for Forms Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("DTFORMMOVEOTH");
                perm.setPermdesc("FORMS:  Move Forms Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("DTFORMDELOTH");
                perm.setPermdesc("FORMS:  Delete/Replace Forms Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("DTFORMPERMOTH");
                perm.setPermdesc("FORMS:  Admin Perms for Forms Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("DTFORMPERMPROF");
                perm.setPermdesc("FORMS:  Create/Edit Form Permission Profiles (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("RPTCAT");
                perm.setPermdesc("REPORTS:  Edit Report Categories (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("RPTUPLOAD");
                perm.setPermdesc("REPORTS:  Upload New Reports (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("RPTEDITOTH");
                perm.setPermdesc("REPORTS:  Edit Attrs for Rpts Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("RPTMOVEOTH");
                perm.setPermdesc("REPORTS:  Move Rpts Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("RPTDELOTH");
                perm.setPermdesc("REPORTS:  Delete/Replace Rpts Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("RPTPERMOTH");
                perm.setPermdesc("REPORTS:  Admin Perms for Rpts Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("RPTPERMPROF");
                perm.setPermdesc("REPORTS:  Admin Report Permission Profiles (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("LTRCAT");
                perm.setPermdesc("LETTERS:  Edit Form Letter Categories (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("LTRUPLOAD");
                perm.setPermdesc("LETTERS:  Upload New Form Letters (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("LTREDITOTH");
                perm.setPermdesc("LETTERS:  Edit Attrs for Ltrs Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("LTRMOVEOTH");
                perm.setPermdesc("LETTERS:  Move Ltrs Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("LTRDELOTH");
                perm.setPermdesc("LETTERS:  Delete/Replace Ltrs Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("LTRPERMOTH");
                perm.setPermdesc("LETTERS:  Admin Perms for Ltrs Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("LTRPERMPROF");
                perm.setPermdesc("LETTERS:  Admin Letter Permission Profiles (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("MEDCAT");
                perm.setPermdesc("MEDIA:  Edit Document Categories (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("MEDUPLOAD");
                perm.setPermdesc("MEDIA:  Upload New Documents (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("MEDEDITOTH");
                perm.setPermdesc("MEDIA:  Edit Attrs for Docs Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("MEDMOVEOTH");
                perm.setPermdesc("MEDIA:  Move Docs Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("MEDDELOTH");
                perm.setPermdesc("MEDIA:  Delete/Replace Docs Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("MEDPERMOTH");
                perm.setPermdesc("MEDIA:  Admin Perms for Docs Uploaded by Others (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p2);
                perm.setPermacr("MEDPERMPROF");
                perm.setPermdesc("MEDIA:  Administer Media Permissions Profiles (Site-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                ////
                ////
                ////
                ////
                
                pos=1;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("ROLEPERM");
                perm.setPermdesc("ROLES:  Manage Roles/Assign Permissions (Study-Level Roles)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("FTYPECREATE");
                perm.setPermdesc("FORMTYPES:  Create New FormTypes (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("FTYPEEDITOTH");
                perm.setPermdesc("FORMTYPES:  Edit Attrs for FormTypes Created by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("FTYPEDELOTH");
                perm.setPermdesc("FORMTYPES:  Delete FormTypes Created by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("FTYPEPERMOTH");
                perm.setPermdesc("FORMTYPES:  Admin Perms for FormTypes Created by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("DTFORMCAT");
                perm.setPermdesc("FORMS:  Edit Form Categories (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("DTFORMUPLOAD");
                perm.setPermdesc("FORMS:  Upload New Form (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("FTYPEUPLOADNEW");
                perm.setPermdesc("FORMS:  Upload new FormType for forms Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("DTFORMEDITOTH");
                perm.setPermdesc("FORMS:  Edit Attrs for Forms Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("DTFORMMOVEOTH");
                perm.setPermdesc("FORMS:  Move Forms Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("DTFORMDELOTH");
                perm.setPermdesc("FORMS:  Delete/Replace Forms Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("DTFORMPERMOTH");
                perm.setPermdesc("FORMS:  Admin Perms for Forms Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("DTFORMPERMPROF");
                perm.setPermdesc("FORMS:  Create/Edit Form Permission Profiles (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("RPTCAT");
                perm.setPermdesc("REPORTS:  Edit Report Categories (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("RPTUPLOAD");
                perm.setPermdesc("REPORTS:  Upload New Reports (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("RPTEDITOTH");
                perm.setPermdesc("REPORTS:  Edit Attrs for Rpts Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("RPTMOVEOTH");
                perm.setPermdesc("REPORTS:  Move Rpts Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("RPTDELOTH");
                perm.setPermdesc("REPORTS:  Delete/Replace Rpts Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("RPTPERMOTH");
                perm.setPermdesc("REPORTS:  Admin Perms for Rpts Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("RPTPERMPROF");
                perm.setPermdesc("REPORTS:  Admin Report Permission Profiles (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("LTRCAT");
                perm.setPermdesc("LETTERS:  Edit Form Letter Categories (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("LTRUPLOAD");
                perm.setPermdesc("LETTERS:  Upload New Form Letters (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("LTREDITOTH");
                perm.setPermdesc("LETTERS:  Edit Attrs for Ltrs Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("LTRMOVEOTH");
                perm.setPermdesc("LETTERS:  Move Ltrs Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("LTRDELOTH");
                perm.setPermdesc("LETTERS:  Delete/Replace Ltrs Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("LTRPERMOTH");
                perm.setPermdesc("LETTERS:  Admin Perms for Ltrs Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("LTRPERMPROF");
                perm.setPermdesc("LETTERS:  Admin Letter Permission Profiles (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("MEDCAT");
                perm.setPermdesc("MEDIA:  Edit Document Categories (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("MEDUPLOAD");
                perm.setPermdesc("MEDIA:  Upload New Documents (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("MEDEDITOTH");
                perm.setPermdesc("MEDIA:  Edit Attrs for Docs Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("MEDMOVEOTH");
                perm.setPermdesc("MEDIA:  Move Docs Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("MEDDELOTH");
                perm.setPermdesc("MEDIA:  Delete/Replace Docs Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("MEDPERMOTH");
                perm.setPermdesc("MEDIA:  Admin Perms for Docs Uploaded by Others (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p3);
                perm.setPermacr("MEDPERMPROF");
                perm.setPermdesc("MEDIA:  Administer Media Permissions Profiles (Study-Level)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                ////
                ////
                ////
                ////
                
                pos=1;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p4);
                perm.setPermacr("VIEWTHIS");
                perm.setPermdesc("View This Form/DataTable");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p4);
                perm.setPermacr("NEWRECORD");
                perm.setPermdesc("Submit new record");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p4);
                perm.setPermacr("READOWN");
                perm.setPermdesc("Read access to records input by self");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p4);
                perm.setPermacr("READOTHER");
                perm.setPermdesc("Read access to records input by others");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p4);
                perm.setPermacr("EDITOWN");
                perm.setPermdesc("Edit access to records input by self");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p4);
                perm.setPermacr("EDITOTHER");
                perm.setPermdesc("Edit access to records input by others");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                ////
                ////
                ////
                ////
                
                pos=1;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p5);
                perm.setPermacr("VIEWTYPE");
                perm.setPermdesc("View Formtype Forms");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                ////
                ////
                ////
                ////
                
                pos=1;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p6);
                perm.setPermacr("RUNREPORT");
                perm.setPermdesc("Run Reporting Program");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                ////
                ////
                ////
                ////
                
                pos=1;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p7);
                perm.setPermacr("OPENLETTER");
                perm.setPermdesc("Open Form Letter");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                ////
                ////
                ////
                ////
                
                pos=1;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p8);
                perm.setPermacr("OPENDOC");
                perm.setPermdesc("Open Document/Media File");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                ////
                ////
                ////
                ////
                
                pos=1;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p11);
                perm.setPermacr("ACCESS");
                perm.setPermdesc("R/A CMS:  Access Researcher/Analyst Management Content");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
       
                pos++;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p11);
                perm.setPermacr("ROLES");
                perm.setPermdesc("R/A CMS:  Administer roles/permissions for Researcher/Analyst CMS (Admin)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
                 
                pos++; 
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p11);
                perm.setPermacr("DOCUPLOAD");
                perm.setPermdesc("R/A CMS:  Upload Documents to CMS");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
                 
                pos++; 
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p11);
                perm.setPermacr("DOCEDIT");
                perm.setPermdesc("R/A CMS:  Edit file/category attributes on files created/uploaded by others (Admin)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
                 
                pos++; 
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p11);
                perm.setPermacr("DOCREMOVE");
                perm.setPermdesc("R/A CMS:  Remove content uploaded/Delete categories created/uploaded by others (Admin)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
                 
                pos++; 
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p11);
                perm.setPermacr("DOCMOVE");
                perm.setPermdesc("R/A CMS:  Move content uploaded by others (Admin)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
                 
                pos++; 
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p11);
                perm.setPermacr("DOCPERMS");
                perm.setPermdesc("R/A CMS:  Administer file/category permissions content created/uploaded by others (Admin)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
                 
                pos++; 
       
                ////
                ////
                ////
                ////
       
                pos=1;
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p12);
                perm.setPermacr("OPEN");
                perm.setPermdesc("R/A CMS RESOURCE:  Open Document or Directory");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
                 
                pos++; 
       
                perm=new com.owarchitects.forms.commons.db.Permissions();
                perm.setPermissioncats(p12);
                perm.setPermacr("UPLOAD");
                perm.setPermdesc("R/A CMS RESOURCE:  Upload Content (to Directory)");
                perm.setPermorder(pos);
                perm.setPermpos(pos);
                perm.setIsdeleted(false);
                mainDAO.save(perm);
                 
                pos++; 
       
                ////
                ////
                ////
                ////
       
                sbWrite("....Done<br><br>\n\n");
       
                ////
                ////
                ////
                ////
       
                // DISALLOWED FILE FORMATS
       
                sbWrite("Create disallowed file formats data records....\n\n");
       
       
                Disallowedfileextensions d;
       
                d=new Disallowedfileextensions();
                d.setExtension("ade");
                d.setDescription("Microsoft Access project extension");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("adp");
                d.setDescription("Microsoft Access project");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("asx");
                d.setDescription("Windows Media Audio / Video");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("bas");
                d.setDescription("Microsoft Visual Basic class module");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("bat");
                d.setDescription("Batch file");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("chm");
                d.setDescription("Compiled HTML Help file");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("cmd");
                d.setDescription("Microsoft Windows NT Command script");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("com");
                d.setDescription("Microsoft MS-DOS program");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("cpl");
                d.setDescription("Control Panel extension");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("crt");
                d.setDescription("Security certificate");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("exe");
                d.setDescription("Program");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("hlp");
                d.setDescription("Help file");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("hta");
                d.setDescription("HTML program");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("inf");
                d.setDescription("Setup Information");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("ins");
                d.setDescription("Internet Naming Service");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("isp");
                d.setDescription("Internet Communication settings");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("js");
                d.setDescription("JScript file");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("jse");
                d.setDescription("Jscript Encoded Script file");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("lnk");
                d.setDescription("Shortcut");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("mda");
                d.setDescription("Microsoft Access add-in program");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("mdb");
                d.setDescription("Microsoft Access program");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("mde");
                d.setDescription("Microsoft Access MDE database");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("mdt");
                d.setDescription("Microsoft Access workgroup information");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("mdw");
                d.setDescription("Microsoft Access workgroup information");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("mdz");
                d.setDescription("Microsoft Access wizard program");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("msc");
                d.setDescription("Microsoft Common Console document");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("msi");
                d.setDescription("Microsoft Windows Installer package");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("msp");
                d.setDescription("Microsoft Windows Installer patch");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("mst");
                d.setDescription("Microsoft Windows Installer transform/Microsoft Visual Test source file");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("ops");
                d.setDescription("Office XP settings");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("pcd");
                d.setDescription("Photo CD image; Microsoft Visual compiled script");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("pif");
                d.setDescription("Shortcut to MS-DOS program");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("prf");
                d.setDescription("Microsoft Outlook profile settings");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("reg");
                d.setDescription("Registration entries");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("scf");
                d.setDescription("Windows Explorer command");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("scr");
                d.setDescription("Screen saver");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("sct");
                d.setDescription("Windows Script Component");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("shb");
                d.setDescription("Shell Scrap object");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("shs");
                d.setDescription("Shell Scrap object");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("url");
                d.setDescription("Internet shortcut");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("vb");
                d.setDescription("VBScript file");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("vbe");
                d.setDescription("VBScript Encoded script file");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("vbs");
                d.setDescription("VBScript file");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("wsc");
                d.setDescription("Windows Script Component");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("wsf");
                d.setDescription("Windows Script file");
                mainDAO.saveOrUpdate(d);
                
                d=new Disallowedfileextensions();
                d.setExtension("wsh");
                d.setDescription("Windows Script Host Settings file");
                mainDAO.saveOrUpdate(d);
       
                sbWrite("....Done<br><br>\n\n");
       
                ////
                ////
                ////
                ////
       
                // SUPPORTED FILE FORMATS
       
                sbWrite("Create supported file formats data records....\n\n");
       
                Supportedfileformats f;
                Fileextensions e;
                
                f=new Supportedfileformats();
                f.setContenttype("application/marc");
                f.setFileformat("MARC marc");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("mrc");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/mathematica");
                f.setFileformat("Mathematica");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("ma");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/msword");
                f.setFileformat("Microsoft Word Document");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("doc");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("docx");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/pdf");
                f.setFileformat("Adobe PDF File");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("pdf");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/postscript");
                f.setFileformat("Postscript File");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("ps");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("eps");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("ai");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/sgml");
                f.setFileformat("SGML");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("sgm");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("sgml");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/vnd.ms-excel");
                f.setFileformat("Microsoft Excel Spreadsheet");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("xls");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("xlsx");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/vnd.ms-powerpoint");
                f.setFileformat("Microsoft Powerpoint");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("ppt");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("pptx");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/vnd.ms-project");
                f.setFileformat("Microsoft Project");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("mpp");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("mpx");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("mpd");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/vnd.visio");
                f.setFileformat("Microsoft Visio");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("vsd");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/wordperfect5.1");
                f.setFileformat("WordPerfect Document");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("wpd");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/x-dvi");
                f.setFileformat("TeXdvi");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("dvi");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/x-filemaker");
                f.setFileformat("FMP3");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("fm");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/x-latex");
                f.setFileformat("LateX");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("latex");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/x-photoshop");
                f.setFileformat("Photoshop");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("psd");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("pdd");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/x-tex");
                f.setFileformat("TeX");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("tex");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("audio/x-aiff");
                f.setFileformat("AIFF");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("aiff");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("aif");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("aifc");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("audio/basic");
                f.setFileformat("audio/basic");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("au");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("snd");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/zip");
                f.setFileformat("Media Archive File");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("zip");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("jar");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("gz");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("tar");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                
                f=new Supportedfileformats();
                f.setContenttype("audio/mp3");
                f.setFileformat("MP3 Audio");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("mp3");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                
                f=new Supportedfileformats();
                f.setContenttype("audio/mp4");
                f.setFileformat("MP4 Audio");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("mp4");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("m4a");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                
                f=new Supportedfileformats();
                f.setContenttype("audio/x-ms-wmv");
                f.setFileformat("Windows Media Video");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("wmv");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                
                f=new Supportedfileformats();
                f.setContenttype("audio/x-mpeg");
                f.setFileformat("MPEG Audio");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("mpa");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("abs");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("mpeg");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("mp1");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("mp2");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("audio/x-pn-realaudio");
                f.setFileformat("RealAudio");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("ra");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("ram");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("audio/x-wav");
                f.setFileformat("WAV");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("wav");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("image/gif");
                f.setFileformat("GIF Image File");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("gif");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("image/jp2");
                f.setFileformat("JPEG 2000 Image File");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("jp2");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("image/jpeg");
                f.setFileformat("JPEG Image File");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("jpeg");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("jpg");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("image/png");
                f.setFileformat("PNG Image File");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("png");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("image/tiff");
                f.setFileformat("TIFF Image File");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("tiff");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("tif");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("image/x-ms-bmp");
                f.setFileformat("BMP Image File");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("bmp");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("image/x-photo-cd");
                f.setFileformat("Photo CD");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("pcd");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("text/html");
                f.setFileformat("HTML Document");
                mainDAO.saveOrUpdate(f);
       
                // HTML FORMAT NEEDED FOR LATER
                Supportedfileformats ff_html=f;
                
                e=new Fileextensions();
                e.setExtension("html");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("htm");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("text/plain");
                f.setFileformat("Text File");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("txt");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/rtf");
                f.setFileformat("Rich Text Format");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("rtf");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("text/xml");
                f.setFileformat("XML Document");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("xml");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("video/mpeg");
                f.setFileformat("MPEG Video File");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("mpeg");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("mpg");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("mpe");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("video/quicktime");
                f.setFileformat("Video Quicktime");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("mov");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("qt");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("text/x-sas-syntax");
                f.setFileformat("SAS Syntax File");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("sas");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/x-sas-system");
                f.setFileformat("SAS System File");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("sas7bdat");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("sd1");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("sd2");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("sd7");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("ssd01");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("ssd");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("ssd04");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/x-sas-transport");
                f.setFileformat("SAS Transport File");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("xpt");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("cport");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("v5x");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("v6x");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("v7x");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("text/x-spss-syntax");
                f.setFileformat("SPSS Syntax File");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("sps");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/x-spss-sav");
                f.setFileformat("SPSS system file");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("sav");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/x-spss-sav");
                f.setFileformat("SPSS portable file");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("por");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("text/x-stata-syntax");
                f.setFileformat("Stata Syntax file");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("do");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/x-stata");
                f.setFileformat("Stata Binary files");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("dta");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("text/x-r-syntax");
                f.setFileformat("R syntax file");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("r");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("application/x-rlang-transport");
                f.setFileformat("R binary file");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("rdata");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("text/x-fixed-field");
                f.setFileformat("fixed field text data");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("dat");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                e=new Fileextensions();
                e.setExtension("asc");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("text/csv");
                f.setFileformat("Comma separated values");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("csv");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
                
                f=new Supportedfileformats();
                f.setContenttype("text/tab-separated-values");
                f.setFileformat("Tab separated values");
                mainDAO.saveOrUpdate(f);
                
                e=new Fileextensions();
                e.setExtension("tab");
                e.setSupportedfileformats(f);
                mainDAO.saveOrUpdate(e);
       
                //
                //
       
                Formformats htmlformat=new Formformats();
                htmlformat.setSupportedfileformats(ff_html);
                mainDAO.save(htmlformat);
       
                ////
                ////
                ////
                ////
       
                sbWrite("....Done<br><br>\n\n");
                
             } catch (Exception e) {
                sbWrite(Stack2string.getString(e));
                sbWrite("\n\nERROR:  Couldn't complete database initialization");
                stopNow(false);
                return;
             }
       
          } catch (Exception e) {

             sbWrite("<span style='color:#AA0000'><br><br>Warning!!! Process threw an exception. &nbsp;" +
                       "Please check URL and internet connection.<br><br><span style='color:#000000'>" +
                       Stack2string.getString(e) + "</span><br><br>" +
                       "<br><br></span>\n");
             stopNow(false);
             return;

          }

          stopNow(true);
          return;

       }

       private void stopNow(boolean isok) {
          if (isok) {
             sb.append("<span style='color:#00AA00'>FORMS SSA database setup completed successfully!<br><br></span>\n");
          } else {
             sb.append("<span style='color:#AA0000'>Setup process was unsuccessful<br><br></span>\n");
          }
          // Make ok button visible
          sb.append("<script>\n");
          sb.append("   var b=parent.document.getElementById('okbutn');\n");
          sb.append("   b.style.visibility='visible';\n");
          sbWrite("</script>\n");

          result.setStatus("RELOAD");
          result.setContent(sb.toString());
          safeSleep(500);
   
          result.setStatus("DONE");

          isRunning=false;
       }

       private void sbWrite(String ins) {
          sb.append(ins);
          result.setContent(sb.toString());
       }

       private void safeSleep(int ms) {
          try {
             Thread.sleep(ms);
          } catch (Exception e) {
             // Do nothing
          }
       }

    }


   //////////
   //////////
   //////////
   //////////
 
   // Certificate chain required for storing private keys.  Not important for our
   // purposes, so we create simple self-signed chain for this purpose
   private static java.security.cert.Certificate[] getCertChain() throws FORMSException {
     
      try {
 
         Security.addProvider(new BouncyCastleProvider());
 
         KeyPairGenerator kpg=KeyPairGenerator.getInstance("RSA");
         kpg.initialize(512);
         KeyPair kp=kpg.generateKeyPair();
                 
         X509V1CertificateGenerator gc = new X509V1CertificateGenerator();
         X500Principal x500=new X500Principal("CN=FORMS Certificate");
         
         gc.setSerialNumber(BigInteger.valueOf(1));
         gc.setIssuerDN(x500);
         gc.setNotBefore(new Date(new Long(0).longValue()));
         gc.setNotAfter(new Date(new Long(1000*60*60*24*365*100).longValue()));
         gc.setSubjectDN(x500); 
         gc.setPublicKey(kp.getPublic());
         gc.setSignatureAlgorithm("MD5WithRSAEncryption");
         
         java.security.cert.X509Certificate cert = gc.generate(kp.getPrivate(), "BC");
 
         java.security.cert.Certificate[] chain = new java.security.cert.X509Certificate[1];
         chain[0] = cert;
 
         //
         // NOTE:  The following SUN-generated certificate creation code has been replaced
         //        by the above BouncyCastle creation code, to remove complation warnings
         //        about the possible removal of associated proprietary code from JRE.
         //
         /*
         // Generate certificate chain (required for storing PrivateKeys)
         CertAndKeyGen cakg = new CertAndKeyGen("RSA", "MD5WithRSA");
         cakg.generate(512);
         PublicKey publicKey = cakg.getPublicKey();
         PrivateKey privateKey = cakg.getPrivateKey();
         X500Name name = new X500Name("FORMS", "FORMS", "FORMS", "FORMS", "FORMS", "FORMS");
         java.security.cert.Certificate[] chain = new java.security.cert.X509Certificate[1];
         chain[0] = cakg.getSelfCertificate(name, 999999999);
         */
 
         return chain;
 
      } catch (Exception e) {
         throw new FORMSException(e,"Error creating certificate chain");
      }

   }



   // verify using embedded derby database and that database doesn't exist

   private boolean isSetupNeeded() {
      ComboPooledDataSource csrc=(ComboPooledDataSource)this.getContext().getBean("embeddedDataSource");
      if (csrc!=null) {
        String pvalue=csrc.getDriverClass();
        if (pvalue.indexOf("derby.jdbc.EmbeddedDriver")>0) {
           try {
               // Verify database existance 
               EmbeddedusersDAO eusersDAO=(EmbeddedusersDAO)this.getContext().getBean("embeddedusersDAO");
               Embeddedusers e=eusersDAO.getRecord("XXXX");
            } catch (HibernateJdbcException hje) {
               return true;
            } catch (org.springframework.transaction.CannotCreateTransactionException ccte) {
               return true;
            }
        }

      }
      return false;
   }

}

