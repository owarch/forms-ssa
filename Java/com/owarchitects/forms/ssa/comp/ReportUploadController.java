 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * ReportUploadController.java - Form Upload controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.text.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.apache.velocity.app.*;
import org.apache.velocity.*;
import au.id.jericho.lib.html.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.mla.html.table.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;
import java.sql.Clob;

public class ReportUploadController extends FORMSController {
 
   String spath="";

   private ArrayList passlist=null;
   private HashMap afhash;
   private HashMap ffhash;
   private String replaceparm;
   private boolean prevInfo;

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // check permissions
      //if (!getAuth().getValue("rightslevel").equalsIgnoreCase("ADMIN")) {
      //   safeRedirect("permission.err");
      //   return null;
      //}

      FORMSAuth auth=getAuth();

      // Use class-name based view
      mav=new ModelAndView("ReportUpload");

      // 
      mav.addObject("resourcetype",auth.getValue("resourcetypestr"));

      try {

         HttpServletRequest request=this.getRequest();
         spath=request.getServletPath();

         if (spath.indexOf("reportupload.")>=0) {

            mav.addObject("status","REPORTUPLOAD");
            auth.setValue("replacestatus","NONE");
            auth.setValue("replacedesc","false");
            String studyid=auth.getValue("studyid");
            if (studyid==null || studyid.length()<1) {
               mav.addObject("list",new VisibilityConstants().getFieldList(VisibilityConstants.SYSTEM));
            } else {
               mav.addObject("list",new VisibilityConstants().getFieldList());
            }
            return mav;

         }
         /*
         else if (spath.indexOf("reportuploadreplace.")>=0) {

            auth.setValue("replacestatus",request.getParameter("replacestatus"));
            auth.setValue("replacedesc",request.getParameter("replacedesc"));
            return callAttrScreen(mav);

         }
         */
         else if (spath.indexOf("reportupload2.")>=0) {

            reportUpload2(mav);
            // check permission before returning
            if (!getPermHelper().hasGlobalPerm(new Integer(auth.getValue("reportlevel")).intValue(),auth.getValue("resourceperm") + "UPLOAD")) {
               mav.addObject("status","NOAUTH");
               return mav;
            }
            return mav;

         }
         else if (spath.indexOf("reportuploadsubmit.")>=0) {

            reportUploadSubmit(mav);
            return mav;
         }
         else if (spath.indexOf("reportuploadfinalsubmit.")>=0) {

            reportUploadFinalSubmit(mav);
            return mav;
         }
         else {
            getWriter().println("BAD REQUEST! - " + spath);
            return null;
         }
   
      } catch (Exception e) { 
         getWriter().println(Stack2string.getString(e));
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Second upload screen 
   private void reportUpload2(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      mav.addObject("status","REPORTUPLOAD2");
      getAuth().setValue("reportlevel",request.getParameter("reportlevel"));

   }

   // submit for upload
   private ModelAndView reportUploadSubmit(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {

         FORMSAuth auth=getAuth();

         if (!getPermHelper().hasGlobalPerm(new Integer(auth.getValue("reportlevel")).intValue(),auth.getValue("resourceperm") + "UPLOAD")) {
            mav.addObject("status","NOAUTH");
            return mav;
         }

         MultipartParser mp = new MultipartParser(request, 500*1024*1024); 
         Part part;
         String filename=null;
         String description=null;
         String creationdate=null;
         String notes=null;
         String reportsource=null;
         String letterFileName=null;
         String letterFileText=null;
         String letterFilePath=null;
         String letterFileContentType=null;
         String progFileName=null;
         String progFileText=null;
         String progFilePath=null;
         String progFileContentType=null;
         String htmlFileName=null;
         String htmlFileText=null;
         String htmlFilePath=null;
         String htmlFileContentType=null;
         String filepath=null;
         long filesize=-1;
   
         while ((part = mp.readNextPart()) != null) {
            String name = part.getName();
            if (part.isParam()) {
               // it's a parameter part
               ParamPart paramPart = (ParamPart) part;
               if (name.equalsIgnoreCase("description")) {
                  description = paramPart.getStringValue();
               } else if (name.equalsIgnoreCase("creationdate")) {
                  creationdate = paramPart.getStringValue();
               } else if (name.equalsIgnoreCase("notes")) {
                  notes = paramPart.getStringValue();
               }   
            }
            else if (part.isFile()) {
               // it's a file part
               FilePart filePart = (FilePart) part;
               String fileName = filePart.getFileName();
               if (fileName != null) {
                  fileName=fileName.trim();
                  ByteArrayOutputStream baos = new ByteArrayOutputStream();
                  long size = filePart.writeTo(baos);
                  if ( (fileName.toLowerCase().lastIndexOf(".sas")==fileName.length()-4) ||
                       (fileName.toLowerCase().lastIndexOf(".r")==fileName.length()-2) ) {
                     progFileName=fileName;
                     progFileText=baos.toString("UTF-8");
                     progFilePath=filePart.getFilePath();
                     progFileContentType = filePart.getContentType();
                  } else if ( (fileName.toLowerCase().lastIndexOf(".html")==fileName.length()-5) ||
                              (fileName.toLowerCase().lastIndexOf(".htm")==fileName.length()-4) ) {
                     htmlFileName=fileName;
                     htmlFileText=baos.toString("UTF-8");
                     htmlFilePath=filePart.getFilePath();
                     htmlFileContentType = filePart.getContentType();
                  } else if ( (fileName.toLowerCase().lastIndexOf(".rtf")==fileName.length()-4) ) {
                     letterFileName=fileName;
                     letterFileText=baos.toString("UTF-8");
                     letterFilePath=filePart.getFilePath();
                     letterFileContentType = filePart.getContentType();
                  } else {
                     mav.addObject("status","BADPROGNAME");
                     return mav;
                  }
               }
            }

         } 
         if (description==null || description.trim().length()<1) {
            description=filename;
         }
         if (creationdate==null || creationdate.trim().length()<1) {
            DateFormat fmt=new SimpleDateFormat("MM/dd/yyyy");
            creationdate=fmt.format(new Date(System.currentTimeMillis()));
         }
         
         // NOTE: reportsource, filename & contenttype will be saved also on server to prevent users
         // from final submitting different values than originally passed

         if (letterFileName!=null && letterFileText!=null) {
            auth.setValue("letterfilename",letterFileName);
            auth.setValue("letterfiletext",letterFileText);
            mav.addObject("letterfilepath",letterFilePath);
         } else {
            auth.setValue("letterfilename","");
            auth.setValue("letterfiletext","");
            mav.addObject("letterfilepath","");
         }
         if (htmlFileName!=null && htmlFileText!=null) {
            auth.setValue("htmlfilename",htmlFileName);
            auth.setValue("htmlfiletext",htmlFileText);
            mav.addObject("htmlfilepath",htmlFilePath);
         } else {
            auth.setValue("htmlfilename","");
            auth.setValue("htmlfiletext","");
            mav.addObject("htmlfilepath","");
         }
         auth.setValue("progfilename",progFileName);
         auth.setValue("progfiletext",progFileText);
         mav.addObject("progfilepath",progFilePath);
         mav.addObject("description",description);
         mav.addObject("creationdate",creationdate);
         mav.addObject("notes",notes);

         mav.addObject("status","DECIDE");

         // pull extension/content type info for form
         String progext=progFileName.substring(progFileName.lastIndexOf(".")+1).toLowerCase();
         String htmlext="XXXXX";
         if (htmlFileName!=null && htmlFileName.length()>0) {
            htmlext=htmlFileName.substring(htmlFileName.lastIndexOf(".")+1).toLowerCase();
         }
         String letterext="XXXXX";
         if (letterFileName!=null && letterFileName.length()>0) {
            letterext=letterFileName.substring(letterFileName.lastIndexOf(".")+1).toLowerCase();
         }

         // make sure file extension is not disallowed
         List dlist=getMainDAO().execQuery(
            "select d from Disallowedfileextensions d where lower(d.extension) in ('" + progext + "','" + htmlext + "','" + letterext + "')"
         );
         if (dlist.size()>0) {
            mav.addObject("status","DISALLOWEDTYPE");
            return mav;
         }

         Fileextensions fext=(Fileextensions)getMainDAO().execUniqueQuery(
            "select f from Fileextensions f join fetch f.supportedfileformats where lower(f.extension)='" + progext + "'"
            );
         if (fext!=null) {
            //mav.addObject("extension",fext.getExtension());
            mav.addObject("progfileformat",fext.getSupportedfileformats().getFileformat());
            mav.addObject("progfilecontenttype",fext.getSupportedfileformats().getContenttype());
            mav.addObject("progfileformatid",fext.getSupportedfileformats().getFileformatid());
         } else {
            mav.addObject("status","UNSUPPORTEDTYPE");
             return mav;
         }
         return mav;

      } catch (Exception e) {
         throw new FORMSException("Form Upload Error" + Stack2string.getString(e));
      }

   }

   // submit for upload
   private ModelAndView reportUploadFinalSubmit(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {

         FORMSAuth auth=getAuth();
         String htmlfilename=auth.getValue("htmlfilename");
         String letterfilename=auth.getValue("letterfilename");
         String progfilename=auth.getValue("progfilename");
         String description=request.getParameter("description");
         String creationdate=request.getParameter("creationdate");;
         String progfileformatid=request.getParameter("progfileformatid");;
         String notes=request.getParameter("notes");
         Date uploadtime=new Date(System.currentTimeMillis());

         // pull extension/content type info for form
         String progext=progfilename.substring(progfilename.lastIndexOf(".")+1).toLowerCase();
         String htmlext=htmlfilename.substring(htmlfilename.lastIndexOf(".")+1).toLowerCase();
         String letterext=letterfilename.substring(letterfilename.lastIndexOf(".")+1).toLowerCase();

         // make sure file extension is not disallowed
         List dlist=getMainDAO().execQuery(
            "select d from Disallowedfileextensions d where lower(d.extension) in ('" + progext + "','" + htmlext + "','" + letterext + "')"
         );
         if (dlist.size()>0) {
            mav.addObject("status","DISALLOWEDTYPE");
            return mav;
         }

         // pull root report heirarchy record to locate file
         Reportha reportha=(Reportha)getMainDAO().execUniqueQuery(
            "select m from Reportha m where m.studies.studyid=" + auth.getValue("studyid") + 
               " and m.resourcetype=" + auth.getValue("resourcetype") + 
               " and m.visibility=" + auth.getValue("reportlevel") + " and m.preporthaid=null"
            );
            
         if (!getPermHelper().hasGlobalPerm(reportha.getVisibility(),auth.getValue("resourceperm") + "UPLOAD")) {
            mav.addObject("status","NOAUTH");
            return mav;
         }

         Long reporthaid=reportha.getReporthaid();   

         Allinst localinst=(Allinst)getMainDAO().execUniqueQuery("select l from com.owarchitects.forms.commons.db.Allinst l where l.islocal=true");


         Supportedfileformats fmt=(Supportedfileformats)getMainDAO().execUniqueQuery("select s from Supportedfileformats s where s.fileformatid=" + progfileformatid);

         try {
   
            Reportinfo info=new Reportinfo();
            info.setAllinst(localinst);
            info.setResourcetype(new Integer(auth.getValue("resourcetype")).intValue());
            info.setPreporthaid(reporthaid);
            info.setDescription(description);
            info.setCreationdate(creationdate);
            info.setNotes(notes);
            info.setUploadtime(uploadtime);
            info.setUploaduser(new Long(auth.getValue("auserid")));
            info.setSupportedfileformats(fmt);
   
            getMainDAO().saveOrUpdate(info);
   
            Reportstore store=new Reportstore();
            store.setContenttype(ReportConstants.PROGFILE);
            store.setReportcontent(auth.getValue("progfiletext"));
            store.setReportinfo(info);
            getMainDAO().saveOrUpdate(store);
            if (htmlfilename.length()>0) {
               store=new Reportstore();
               store.setContenttype(ReportConstants.HTMLFILE);
               store.setReportcontent(auth.getValue("htmlfiletext"));
               store.setReportinfo(info);
               getMainDAO().saveOrUpdate(store);
            }
            if (letterfilename.length()>0) {
               store=new Reportstore();
               store.setContenttype(ReportConstants.FORMLETTER);
               store.setReportcontent(auth.getValue("letterfiletext"));
               store.setReportinfo(info);
               getMainDAO().saveOrUpdate(store);
            }
            mav.addObject("status","UPLOADCOMPLETE");

         } catch (org.springframework.dao.DataIntegrityViolationException die) {
            mav.addObject("status","INTEGRITYEXCEPTION");
         }

         // clear out report session lists
         clearSessionLists();

         auth.setValue("htmlfilename","");
         auth.setValue("htmlfiletext","");
         auth.setValue("progfilename","");
         auth.setValue("progfiletext","");
         auth.setValue("letterfilename","");
         auth.setValue("letterfiletext","");

         return mav;

      } catch (Exception e) {
         throw new FORMSException("Form Upload Error" + Stack2string.getString(e));
      }

   }

   // Empty session-stored forms lists so data are repulled
   private void clearSessionLists() {   
      try {
         getAuth().setSessionAttribute("reporthalist",null);
         getAuth().setSessionAttribute("reportinfolist",null);
         getAuth().setSessionAttribute("minfomap",null);
      } catch (Exception e) { }
   }  

}


