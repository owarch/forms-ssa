
/*
 *
 * ViewDataTableController.java - DataTable Selection Interface
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.mla.html.table.*;

public class ViewDataTableController extends FORMSSsaServiceClientController {

   private String dtdefid=null;
   private String dtacr=null;
   private String dtdesc=null;
   private String sortvar=null;
   private String sortdir="asc";

   private boolean isremoteform;
   private Long ainstid;
   private Linkedinst linst;
   private Long remotedtdefid;

   public ModelAndView submitRequest() throws FORMSException {

      // Use class-name based view
      mav=new ModelAndView("ViewDataTable");

      try {

         dtdefid=getRequest().getParameter("dtdefid");
         String function=getRequest().getParameter("_function_");

         if (dtdefid!=null) {

            sortvar=null;
            dtacr=getRequest().getParameter("dtacr");
            dtdesc=getRequest().getParameter("dtdesc");
            getAuth().setValue("dtdefid",dtdefid);
            safeRedirect("viewdatatable.do");

         } else if (getRequest().getServletPath().indexOf("search.do")>0) {

            dtdefid=getAuth().getValue("dtdefid");
            if (getRequest().getParameter("searchtext")!=null) {

               getAuth().setValue("searchtext",getRequest().getParameter("searchtext"));
               getAuth().setValue("matchcase",getRequest().getParameter("matchcase"));
               getAuth().setValue("exactmatch",getRequest().getParameter("exactmatch"));
               safeRedirect("viewdatatablesearch.do");

            } else {   

               buildSearchDisplay((String)getAuth().getValue("searchtext"),(String)getAuth().getValue("matchcase"),
                                  (String)getAuth().getValue("exactmatch"));
               mav.addObject("searchtext",getAuth().getValue("searchtext"));
               if (!getAuth().getValue("matchcase").equals("")) {
                  mav.addObject("matchcase","checked");
               } else {
                  mav.addObject("matchcase","");
               }   
               if (!getAuth().getValue("exactmatch").equals("")) {
                  mav.addObject("exactmatch","checked");
               } else {
                  mav.addObject("exactmatch","");
               }   
               getAuth().setValue("viewdatatablelast","SEARCH");
               mav.addObject("status","SHOWTABLE");
               mav.addObject("dtacr",dtacr);
               mav.addObject("dtdesc",dtdesc);
               mav.addObject("maxrows",getMaxRows());
               return mav;

            }

         } else if (getRequest().getServletPath().indexOf("page.do")>0) {

            dtdefid=getAuth().getValue("dtdefid");
            try {
               Integer maxrows=Integer.parseInt(request.getParameter("maxrows"));
               getAuth().setValue("maxrows",maxrows.toString());
            } catch (NumberFormatException nfe) {
               // do nothing
            }
            safeRedirect("viewdatatable.do");

         } else if (getRequest().getServletPath().indexOf("sort.do")>0) {

            sortvar=getRequest().getParameter("sortvar");
            out.println(sortvar);
            if (sortvar.indexOf(" ")>0 && sortdir.equalsIgnoreCase("asc")) {
               sortdir="desc";
            } else {
               sortdir="asc";
            }
            sortvar=sortvar.replaceAll(" .*","");
            if (getAuth().getValue("viewdatatablelast").equals("SEARCH")) {
               safeRedirect("viewdatatablesearch.do");
            } else {
               safeRedirect("viewdatatable.do");
            }

         } else if (function==null) {
 
            dtdefid=getAuth().getValue("dtdefid");
            buildTableDisplay();
            getAuth().setValue("viewdatatablelast","REG");
            mav.addObject("status","SHOWTABLE");
            mav.addObject("searchtext","");
            mav.addObject("dtacr",dtacr);
            mav.addObject("dtdesc",dtdesc);
            mav.addObject("maxrows",getMaxRows());
            return mav;

         }

         return null;

      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   private Integer getMaxRows() throws FORMSException, FORMSSecurityException, FORMSKeyException {
      Integer maxrows=null;
      try {
         maxrows=new Integer(getAuth().getValue("maxrows"));
      } catch (NumberFormatException nfe) {
         // do nothing
      }
      if (maxrows==null) {
         maxrows=200;
      }
      return maxrows;
   }

   // Create root hierarchy record if none exists (these are not created at database initialization)
   private void buildTableDisplay() throws FORMSException, FORMSSecurityException, FORMSKeyException {

      // set local/remote status
      isremoteform=false;
      String qdtdefid;
      ainstid=(Long)getMainDAO().execUniqueQuery(
            "select f.allinst.ainstid from Datatabledef f " +
               "where f.dtdefid=" + dtdefid +  "and f.allinst.islocal!=true"
               );

      if (ainstid!=null) {
         getAuth().setObjectValue("formloc",new Integer(LockInfo.REMOTEFORM));
         isremoteform=true;
      } else {
         getAuth().setObjectValue("formloc",new Integer(LockInfo.LOCALFORM));
      }

      // pull formtypeid for add record function
      Object formtypeid=(Object)getMainDAO().execUniqueQuery(
          "select f.formtypeid from Formtypes f " + 
             "where f.datatabledef.dtdefid=" + dtdefid + " and f.formtypelist.formtype='_INTERNAL_'"
             );
      if (formtypeid!=null) {
         getAuth().setValue("formtypeid",formtypeid.toString());
      } else {
         getAuth().setValue("formtypeid","_NULL_");
      }

      // build list of field names
      List<String> varlist=getMainDAO().execQuery(
         "select d.columnname from Datatablecoldef d where d.datatabledef.dtdefid=" + dtdefid + 
         " order by d.columnorder" 
         );
      List<Object[]> typelist=getMainDAO().execQuery(
         "select d.fieldname,d.value from Dtfieldattrs d where d.datatabledef.dtdefid=" + dtdefid + 
             " and d.attr='VT'" 
         );

      if (sortvar!=null) {   
   
         // update variable in varlist
         String[] varray=null;
         Iterator<String> variter=varlist.iterator();
         int cv=0;
         while (variter.hasNext()) {
            String varname=variter.next();
            if (varname.equalsIgnoreCase(sortvar)) {
               varray=varlist.toArray(new String[varlist.size()]);
            if (sortdir.equalsIgnoreCase("asc")) {
                  varray[cv]=varname + " &#9660;";
               } else {
                  varray[cv]=varname + " &#9650;";
               }
               break;
            }
            cv++;
         }
         varlist=Arrays.asList(varray);

      }

      List<Datatablerecords> records;


      if (!isremoteform) {

         if (sortvar==null) {   
   
            StringBuilder queryString=new StringBuilder(
               "select distinct d from Datatablerecords d join fetch d.datatablevalues " +
                  " where d.datatabledef.dtdefid=" + dtdefid + " and d.isaudit!='T'"
               );   
      
            records=getMainDAO().execPagedQuery(queryString.toString(),0,getMaxRows().intValue());
      
         } else {
   
            StringBuilder queryString=new StringBuilder(
               "select distinct d.datatablerecords.datarecordid,d.varvalue " +
                   " from Datatablevalues d " +
                  " where d.datatablerecords.datatabledef.dtdefid=" + dtdefid + 
                    " and d.datatablerecords.isaudit!='T'" + 
                      " and upper(d.varname)='" + sortvar + "'" + 
                    " order by d.varvalue " + sortdir
               );   
            List ordridlist=getMainDAO().execQuery(queryString.toString());
   
            // If field is numeric, order by numeric value
            List ckNumList=getMainDAO().execQuery(
                  "select d.value from Dtfieldattrs d " +
                     " where d.datatabledef.dtdefid=" + dtdefid +
                      " and upper(d.fieldname)='" + sortvar + "'" + 
                      " and d.attr='VT'" + 
                      " and d.value in ('INT','DEC') "
               );
            if (!ckNumList.isEmpty()) {
   
               Collections.sort(ordridlist,new CompareByNum((String)ckNumList.get(0)));
   
            }
   
            // construct list for where clause
            Iterator ordriter=ordridlist.iterator();
            StringBuilder idsb=new StringBuilder("(");
            while (ordriter.hasNext()) {
               idsb.append(((Object[])ordriter.next())[0].toString());
               if (ordriter.hasNext()) idsb.append(",");
            }
            idsb.append(")");
            // Merge in missing values if any
            queryString=new StringBuilder(
               "select d.datarecordid from Datatablerecords d " +
                  " where d.datatabledef.dtdefid=" + dtdefid +  
                    " and d.isaudit!='T'"  
               );   
            if (ordridlist.size()>0) {          
               queryString.append(" and d.datarecordid not in " + idsb); 
            }          
            List misslist=getMainDAO().execQuery(queryString.toString());
   
            ArrayList<Long> sortlist=new ArrayList();
   
            Iterator missiter=misslist.iterator();
            ordriter=ordridlist.iterator();
            idsb=new StringBuilder("(");
            int countvar=0;
            if (sortdir.equalsIgnoreCase("asc")) {
               while (missiter.hasNext() && countvar<getMaxRows().intValue()) {
                  countvar++;
                  if (countvar>1) idsb.append(",");
                  Long appValue=(Long)missiter.next();
                  idsb.append(appValue.toString());
                  sortlist.add(appValue);
               }
               while (ordriter.hasNext() && countvar<getMaxRows().intValue()) {
                  countvar++;
                  if (countvar>1) idsb.append(",");
                  Long appValue=(Long)(((Object[])ordriter.next())[0]);
                  idsb.append(appValue.toString());
                  sortlist.add(appValue);
               }
            } else {
               while (ordriter.hasNext() && countvar<getMaxRows().intValue()) {
                  countvar++;
                  if (countvar>1) idsb.append(",");
                  Long appValue=(Long)(((Object[])ordriter.next())[0]);
                  idsb.append(appValue.toString());
                  sortlist.add(appValue);
               }
               while (missiter.hasNext() && countvar<getMaxRows().intValue()) {
                  countvar++;
                  if (countvar>1) idsb.append(",");
                  Long appValue=(Long)missiter.next();
                  idsb.append(appValue.toString());
                  sortlist.add(appValue);
               }
            }
            idsb.append(")");
            
            queryString=new StringBuilder(
               "select distinct d from Datatablerecords d join fetch d.datatablevalues " +
                  " where d.datatabledef.dtdefid=" + dtdefid +  
                    " and d.isaudit!='T'" +  
                    " and d.datarecordid in " + idsb.toString()
               );   
            List<Datatablerecords> trecords=getMainDAO().execQuery(queryString.toString());
            records=new ArrayList(trecords.size());
            // create ordered list for output;
            Iterator sortiter=sortlist.iterator();
            while (sortiter.hasNext()) {
               Long drid=(Long)sortiter.next();
               Iterator<Datatablerecords> titer=trecords.iterator();
               while (titer.hasNext()) {
                  Datatablerecords record=titer.next();
                  if (record.getDatarecordid()==drid.longValue()) {
                     records.add(record);
                     titer.remove();
                  }
               }
            }
   
         }

      } else {

         linst=null;
         linst=(Linkedinst)getMainDAO().execUniqueQuery(
               "select a.linkedinst from Allinst a where a.ainstid=" + ainstid
            );
         remotedtdefid=(Long)getMainDAO().execUniqueQuery("select d.remotedtdefid from Datatabledef d where d.dtdefid=" + dtdefid);  
         SsaServicePassObjectParms parms=new SsaServicePassObjectParms("getDatatableRecordsServiceTarget","getTableDisplayRecords");
         HashMap passmap=new HashMap();
         passmap.put("dtdefid",remotedtdefid.toString());
         passmap.put("sortdir",sortdir);
         if (sortvar!=null) {
            passmap.put("sortvar",sortvar);
         }
         parms.setObject(passmap);
         SsaServiceListResult result=(SsaServiceListResult)this.submitServiceRequest(linst,parms);
         records=result.getList();

      }
   
   
      int nRows=records.size();
      int nCols=varlist.size();

      Iterator<String> variter=varlist.iterator();
      String[] justarray=new String[nCols]; 
      // initialize justarray
      int i=0;
      while (variter.hasNext()) {
         String vname=variter.next().replaceAll(" &#9660;","").replaceAll(" &#9650;","");
         Iterator<Object[]> typeiter=typelist.iterator();
         while (typeiter.hasNext()) {
            Object[] ttypea=(Object[])typeiter.next();
            if (vname.equalsIgnoreCase((String)ttypea[0])) {
               String type=(String)ttypea[1];
               if (type.equalsIgnoreCase("INT") || type.equalsIgnoreCase("DEC")) {
                  justarray[i]="right";
               } else if (type.equalsIgnoreCase("DATE") || type.equalsIgnoreCase("TIME") || type.equalsIgnoreCase("DTTM")) {
                  justarray[i]="center";
               } else {
                  justarray[i]="left";
               }
               typeiter.remove();
               break;
            }
         }
         i++;
      }

      // build table array (use array for performance)
      String[][] tablearray=new String[nRows][nCols];
      Iterator<Datatablerecords> reciter=records.iterator();
      int r=0;
      while (reciter.hasNext()) {
         Datatablerecords record=reciter.next();
         variter=varlist.iterator();
         i=0;
         while (variter.hasNext()) {
            String colname=variter.next().replaceAll(" &#9660;","").replaceAll(" &#9650;","");
            String colvalue;
            Iterator<Datatablevalues> valiter=record.getDatatablevalues().iterator();
            while (valiter.hasNext()) {
               Datatablevalues value=valiter.next();
               String tdString;
               if (value.getVarname().equalsIgnoreCase(colname)) {
                  colvalue=value.getVarvalue().replace("<","&lt;").replace(">","&gt;");
                  if (colvalue.length()>0 && colvalue.length()<25) {
                     tablearray[r][i]="<td class='menutdlink " + justarray[i] + "' onclick='validate(" + record.getDatarecordid() + ",false)'><nobr>" + colvalue + "</nobr></td>";
                  } else if (colvalue.length()>0) {
                     tablearray[r][i]="<td class='menutdlink " + justarray[i] + "' onclick='validate(" + record.getDatarecordid() + ",false)'><nobr>" + colvalue.substring(0,25) +
                     "</nobr>" + colvalue.substring(25) + "</td>";
                  } else {
                     tablearray[r][i]="<td class='menutdlink " + justarray[i] + "' onclick='validate(" + record.getDatarecordid() + ",false)'>&nbsp;</td>";
                  }   
                  break;
               }
            }
            if (tablearray[r][i]==null) {
               tablearray[r][i]="<td class='menutdlink " + justarray[i] + "' onclick='validate(" + record.getDatarecordid() + ",false)'>&nbsp;</td>";
            }
            if (i==0) {
               tablearray[r][i]=
                      "<td class=menutdlink align=center onclick=\"validate(" + record.getDatarecordid() + ",true);\">" +
                           "<img src=\"images/DeleteX.gif\" height=15px style=\"border-width:0px;border-style:solid;border-color:black\">" +
                      "</td>" +
                      tablearray[r][i];
            }
            i++;
         }
         r++;
      }

      HashMap passMap=new HashMap();
      passMap.put("varlist",varlist);
      passMap.put("tablearray",tablearray);
      ObjectResult result=new ObjectResult();
      result.setObject(passMap);
      this.getSession().setAttribute("iframeresult",result);

   }

   // Create root hierarchy record if none exists (these are not created at database initialization)
   private void buildSearchDisplay(String searchtext,String matchcase,String exactmatch) 
      throws FORMSException, FORMSSecurityException, FORMSKeyException {

      // build list of field names
      List<String> varlist=getMainDAO().execQuery(
         "select d.columnname from Datatablecoldef d where d.datatabledef.dtdefid=" + dtdefid + 
         " order by d.columnorder"
         );
      List<Object[]> typelist=getMainDAO().execQuery(
         "select d.fieldname,d.value from Dtfieldattrs d where d.datatabledef.dtdefid=" + dtdefid + 
             " and d.attr='VT'" 
         );
   
      List<Datatablerecords> records;

      if (!isremoteform) {   

         // find matching record values and construct list of datarecordids
         StringBuilder qsb=new StringBuilder();
         qsb.append("select distinct d.datatablerecords.datarecordid from Datatablevalues d " + 
                     "where d.datatablerecords.datatabledef.dtdefid=" + dtdefid + 
                     " and d.datatablerecords.isaudit!='T' and ");
         if (matchcase!=null && exactmatch!=null && matchcase.equalsIgnoreCase("X") && exactmatch.equalsIgnoreCase("X")) {
            qsb.append("d.varvalue='" + searchtext.trim() + "'");
         } else if (matchcase!=null && matchcase.equalsIgnoreCase("X")) {
            qsb.append("d.varvalue like '%" + searchtext.trim() + "%'");
         } else if (exactmatch!=null && exactmatch.equalsIgnoreCase("X")) {
            qsb.append("upper(d.varvalue)=upper('" + searchtext.trim() + "')");
         } else {
            qsb.append("upper(d.varvalue) like upper('%" + searchtext.trim() + "%')");
         } 
   
         List dtvalues=getMainDAO().execQuery(qsb.toString());
   
         if (dtvalues.size()>0) {
   
            Iterator dtiter=dtvalues.iterator();
            StringBuilder insb=new StringBuilder("(");
            while (dtiter.hasNext()) {
               insb.append(dtiter.next().toString());
               if (dtiter.hasNext()) {
                  insb.append(",");
               }
            }
            insb.append(")");
   
            if (sortvar==null) {
   
               StringBuilder queryString=new StringBuilder(
                  "select distinct d from Datatablerecords d join fetch d.datatablevalues where d.datatabledef.dtdefid=" + dtdefid +
                     " and d.isaudit!='T' " +
                     " and d.datarecordid in " + insb.toString() 
                  );   
         
               records=getMainDAO().execPagedQuery(queryString.toString(),0,getMaxRows().intValue());
   
            } else {
   
               // update variable in varlist
               String[] varray=null;
               Iterator<String> variter=varlist.iterator();
               int cv=0;
               while (variter.hasNext()) {
                  String varname=variter.next();
                  if (varname.equalsIgnoreCase(sortvar)) {
                     varray=varlist.toArray(new String[varlist.size()]);
                     if (sortdir.equalsIgnoreCase("asc")) {
                        varray[cv]=varname + " &#9660;";
                     } else {
                        varray[cv]=varname + " &#9650;";
                     }
                     break;
                  }
                  cv++;
               }
               varlist=Arrays.asList(varray);
      
               StringBuilder queryString=new StringBuilder(
                  "select distinct d.datatablerecords.datarecordid,d.varvalue " +
                      " from Datatablevalues d " +
                     " where d.datatablerecords.datatabledef.dtdefid=" + dtdefid + 
                         " and d.datatablerecords.isaudit!='T' " +
                         " and d.datatablerecords.datarecordid in " + insb.toString() + 
                         " and upper(d.varname)='" + sortvar + "'" + 
                       " order by d.varvalue " + sortdir
                  );   
               List ordridlist=getMainDAO().execQuery(queryString.toString());
      
               // If field is numeric, order by numeric value
               List ckNumList=getMainDAO().execQuery(
                     "select d.value from Dtfieldattrs d " +
                        " where d.datatabledef.dtdefid=" + dtdefid +
                         " and upper(d.fieldname)='" + sortvar + "'" + 
                         " and d.attr='VT'" + 
                         " and d.value in ('INT','DEC') "
                  );
               if (!ckNumList.isEmpty()) {
      
                  Collections.sort(ordridlist,new CompareByNum((String)ckNumList.get(0)));
      
               }
      
               // construct list for where clause
               Iterator ordriter=ordridlist.iterator();
               StringBuilder idsb=new StringBuilder("(");
               while (ordriter.hasNext()) {
                  idsb.append(((Object[])ordriter.next())[0].toString());
                  if (ordriter.hasNext()) idsb.append(",");
               }
               idsb.append(")");
               // Merge in missing values if any
               queryString=new StringBuilder(
                  "select d.datarecordid from Datatablerecords d " +
                     " where d.datatabledef.dtdefid=" + dtdefid + 
                       " and d.isaudit!='T' " +
                       " and d.datarecordid in " + insb.toString() 
                  );   
               if (ordridlist.size()>0) {          
                  queryString.append(" and d.datarecordid not in " + idsb); 
               }          
               List misslist=getMainDAO().execQuery(queryString.toString());
      
               ArrayList<Long> sortlist=new ArrayList();
      
               Iterator missiter=misslist.iterator();
               ordriter=ordridlist.iterator();
               idsb=new StringBuilder("(");
               int countvar=0;
               if (sortdir.equalsIgnoreCase("asc")) {
                  while (missiter.hasNext() && countvar<getMaxRows().intValue()) {
                     countvar++;
                     if (countvar>1) idsb.append(",");
                     Long appValue=(Long)missiter.next();
                     idsb.append(appValue.toString());
                     sortlist.add(appValue);
                  }
                  while (ordriter.hasNext() && countvar<getMaxRows().intValue()) {
                     countvar++;
                     if (countvar>1) idsb.append(",");
                     Long appValue=(Long)(((Object[])ordriter.next())[0]);
                     idsb.append(appValue.toString());
                     sortlist.add(appValue);
                  }
               } else {
                  while (ordriter.hasNext() && countvar<getMaxRows().intValue()) {
                     countvar++;
                     if (countvar>1) idsb.append(",");
                     Long appValue=(Long)(((Object[])ordriter.next())[0]);
                     idsb.append(appValue.toString());
                     sortlist.add(appValue);
                  }
                  while (missiter.hasNext() && countvar<getMaxRows().intValue()) {
                     countvar++;
                     if (countvar>1) idsb.append(",");
                     Long appValue=(Long)missiter.next();
                     idsb.append(appValue.toString());
                     sortlist.add(appValue);
                  }
               }
               idsb.append(")");
               
               queryString=new StringBuilder(
                  "select distinct d from Datatablerecords d join fetch d.datatablevalues " +
                     " where d.datatabledef.dtdefid=" + dtdefid +  
                       " and d.isaudit!='T' " +
                       " and d.datarecordid in " + idsb.toString()
                  );   
               List<Datatablerecords> trecords=getMainDAO().execQuery(queryString.toString());
               records=new ArrayList(trecords.size());
               // create ordered list for output;
               Iterator sortiter=sortlist.iterator();
               while (sortiter.hasNext()) {
                  Long drid=(Long)sortiter.next();
                  Iterator<Datatablerecords> titer=trecords.iterator();
                  while (titer.hasNext()) {
                     Datatablerecords record=titer.next();
                     if (record.getDatarecordid()==drid.longValue()) {
                        records.add(record);
                        titer.remove();
                     }
                  }
               }
            }
   
         } else {
   
            records=new ArrayList();
   
         }

      } else {

         SsaServicePassObjectParms parms=new SsaServicePassObjectParms("getDatatableRecordsServiceTarget","getSearchDisplayRecords");
         HashMap passmap=new HashMap();
         passmap.put("dtdefid",remotedtdefid.toString());
         passmap.put("sortdir",sortdir);
         if (sortvar!=null) {
            passmap.put("sortvar",sortvar);
         }
         if (matchcase!=null) {
            passmap.put("matchcase",matchcase);
         }
         if (exactmatch!=null) {
            passmap.put("exactmatch",exactmatch);
         }
         if (searchtext!=null) {
            passmap.put("searchtext",searchtext);
         }
         parms.setObject(passmap);
         SsaServiceListResult result=(SsaServiceListResult)this.submitServiceRequest(linst,parms);
         records=result.getList();

      }

      int nRows=records.size();
      int nCols=varlist.size();

      Iterator<String> variter=varlist.iterator();
      String[] justarray=new String[nCols]; 
      // initialize justarray
      int i=0;
      while (variter.hasNext()) {
         String vname=variter.next().replaceAll(" &#9660;","").replaceAll(" &#9650;","");
         Iterator<Object[]> typeiter=typelist.iterator();
         while (typeiter.hasNext()) {
            Object[] ttypea=(Object[])typeiter.next();
            if (vname.equalsIgnoreCase((String)ttypea[0])) {
               String type=(String)ttypea[1];
               if (type.equalsIgnoreCase("INT") || type.equalsIgnoreCase("DEC")) {
                  justarray[i]="right";
               } else if (type.equalsIgnoreCase("DATE") || type.equalsIgnoreCase("TIME") || type.equalsIgnoreCase("DTTM")) {
                  justarray[i]="center";
               } else {
                  justarray[i]="left";
               }
               typeiter.remove();
               break;
            }
         }
         i++;
      }

      // build table array (use array for performance)
      String[][] tablearray=new String[nRows][nCols];
      Iterator<Datatablerecords> reciter=records.iterator();
      int r=0;
      while (reciter.hasNext()) {
         Datatablerecords record=reciter.next();
         variter=varlist.iterator();
         i=0;
         while (variter.hasNext()) {
            String colname=variter.next().replaceAll(" &#9660;","").replaceAll(" &#9650;","");
            String colvalue;
            Iterator<Datatablevalues> valiter=record.getDatatablevalues().iterator();
            while (valiter.hasNext()) {
               Datatablevalues value=valiter.next();
               String tdString;
               if (searchtext!=null && searchtext.trim().length()>0) {
                  if (value.getVarname().equalsIgnoreCase(colname)) {
                     colvalue=value.getVarvalue().replace("<","&lt;").replace(">","&gt;");
                     if (colvalue.length()>0 && colvalue.length()<25) {
                        tablearray[r][i]="<td class='menutdlink " + justarray[i] + "' onclick='validate(" + record.getDatarecordid() + ",false)'><nobr>" + 
                           colvalue.replaceAll("(?i)" + searchtext,"<span style='background-color:#FFFF99'>$0</span>") + "</nobr></td>";
                     } else if (colvalue.length()>0) {
                        if (colvalue.toUpperCase().indexOf(searchtext.toUpperCase())>=0) {
                           tablearray[r][i]="<td class='menutdlink " + justarray[i] + "' onclick='validate(" + record.getDatarecordid() + ",false)'><nobr>" + 
                              colvalue.replaceAll("(?i)" + searchtext,"<span style='background-color:#FFFF99'>$0</span>") + "</nobr></td>";
                        } else {
                           tablearray[r][i]="<td class='menutdlink " + justarray[i] + "' onclick='validate(" + record.getDatarecordid() + ",false)'><nobr>" + 
                              colvalue.substring(0,25) + "</nobr>" + colvalue.substring(25) + "</td>";
                        }
                     } else {
                        tablearray[r][i]="<td class='menutdlink " + justarray[i] + "' onclick='validate(" + record.getDatarecordid() + ",false)'>&nbsp;</td>";
                     }   
                     break;
                  }
               } else {
                  if (value.getVarname().equalsIgnoreCase(colname)) {
                     colvalue=value.getVarvalue().replace("<","&lt;").replace(">","&gt;");
                     if (colvalue.length()>0 && colvalue.length()<25) {
                        tablearray[r][i]="<td class='menutdlink " + justarray[i] + "' onclick='validate(" + record.getDatarecordid() + ",false)'><nobr>" + colvalue + "</nobr></td>";
                     } else if (colvalue.length()>0) {
                        tablearray[r][i]="<td class='menutdlink " + justarray[i] + "' onclick='validate(" + record.getDatarecordid() + ",false)'><nobr>" + colvalue.substring(0,25) +
                        "</nobr>" + colvalue.substring(25) + "</td>";
                     } else {
                        tablearray[r][i]="<td class='menutdlink " + justarray[i] + "' onclick='validate(" + record.getDatarecordid() + ",false)'>&nbsp;</td>";
                     }   
                     break;
                  }
               }
            }
            if (tablearray[r][i]==null) {
               tablearray[r][i]="<td class='menutdlink " + justarray[i] + "' onclick='validate(" + record.getDatarecordid() + ",false)'>&nbsp;</td>";
            }
            if (i==0) {
               tablearray[r][i]=
                      "<td class=menutdlink align=center onclick=\"validate(" + record.getDatarecordid() + ",true);\">" +
                           "<img src=\"images/DeleteX.gif\" height=15px style=\"border-width:0px;border-style:solid;border-color:black\">" +
                      "</td>" +
                      tablearray[r][i];
            }
            i++;
         }
         r++;
      }

      HashMap passMap=new HashMap();
      passMap.put("varlist",varlist);
      passMap.put("tablearray",tablearray);
      ObjectResult result=new ObjectResult();
      result.setObject(passMap);
      this.getSession().setAttribute("iframeresult",result);

   }

   private class CompareByNum implements Comparator<Object[]>{

      String type;

      public CompareByNum(String type) {
         this.type=type;
      }

      public int compare(Object[] oa1,Object[] oa2) {

         if (type.equalsIgnoreCase("INT")) {

            Long d1;
            try {
               d1=new Long(oa1[1].toString());
            } catch (Exception e) {
               d1=new Long(Long.MIN_VALUE);
            }
            Long d2;
            try {
               d2=new Long(oa2[1].toString());
            } catch (Exception e) {
               d2=new Long(Long.MIN_VALUE);
            }
            if (sortdir.equalsIgnoreCase("ASC")) {
               return d1.compareTo(d2);
            } else {
               return d2.compareTo(d1);
            }

         } else {

            Double d1;
            try {
               d1=new Double(oa1[1].toString());
            } catch (Exception e) {
               d1=new Double(Double.MIN_VALUE);
            }
            Double d2;
            try {
               d2=new Double(oa2[1].toString());
            } catch (Exception e) {
               d2=new Double(Double.MIN_VALUE);
            }
            if (sortdir.equalsIgnoreCase("ASC")) {
               return d1.compareTo(d2);
            } else {
               return d2.compareTo(d1);
            }

         }

      }

   }

}









