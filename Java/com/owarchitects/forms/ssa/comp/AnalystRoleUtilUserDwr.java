 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
 * AnalystRoleUtilUserDwr.java - DWR program to edit row memberships
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.io.*;
import java.util.*;
//import java.text.*;
//import org.apache.regexp.RE;
//import org.apache.commons.lang.StringUtils;
//import com.sas.sasserver.sclfuncs.*;
//import com.sas.sasserver.dataset.*;
//import com.sas.rmi.*;
//import com.sas.sasserver.submit.*;
//import org.apache.commons.lang.StringUtils;
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang.ArrayUtils.*;
//import java.util.regex.*;
import javax.crypto.*;
import java.security.*;
//import javax.crypto.spec.*;
//import java.security.spec.*;
//import uk.ltd.getahead.dwr.WebContext;
//import uk.ltd.getahead.dwr.WebContextFactory;
//import javax.servlet.*;
//import javax.servlet.http.*;
//import org.apache.velocity.app.*;
//import org.apache.velocity.*;
//import org.apache.commons.httpclient.util.URIUtil;


public class AnalystRoleUtilUserDwr extends FORMSDwr {

   // exports local public key for installation as a String

   public String setMembership(boolean savechild,long aroleid,long auserid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Verify authorization, then return public key as string
      FORMSAuth auth=this.getAuth();
      if (auth.isAuth(this.getRequest(),this.getResponse())) {

         try {

            List l=getMainDAO().execQuery("select rra from Analystroleuserassign rra " +
                                             "where rra.role.aroleid=:aroleid and rra.user.auserid=:auserid ",
                                            new String[] { "aroleid","auserid" },
                                            new Object[] { new Long(aroleid),new Long(auserid) } 
                                         );   
            if (!savechild && l.size()>0) {
               Iterator i=l.iterator();
               while (i.hasNext()) {
                  Analystroleuserassign r=(Analystroleuserassign)i.next();
                  getMainDAO().delete(r);
               }
               return "OK";

            } else if (savechild && l.size()<=0) {

               AllusersDAO allusersDAO=(AllusersDAO)this.getContext().getBean("allusersDAO");
               Analystroles role=(Analystroles)getMainDAO().execUniqueQuery(
                  "select a from Analystroles a where a.aroleid=" + aroleid
                  );
               Allusers user=allusersDAO.getRecord(auserid);
               Analystroleuserassign r=new Analystroleuserassign();
               r.setRole(role);
               r.setUser(user);
               getMainDAO().saveOrUpdate(r);
               return "OK";

            } else if (savechild) {

               return "OK";

            }

            return null;

         } catch (Exception e) {

            return null;

         }
   
      } else {

         return null;

      }
      
   }

}


