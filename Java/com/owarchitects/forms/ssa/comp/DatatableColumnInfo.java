
/*
 *
 * DatatableColumnInfo.java - Holds important info about data columns when importing data tables
 *
 */

package com.owarchitects.forms.ssa.comp;

import java.util.ArrayList;
import java.util.Arrays;

public class DatatableColumnInfo {

   private String columnName="";
   private boolean hasSingleCharacterValues=false;
   private boolean hasMultiCharacterValues=false;
   private boolean hasLowerCaseValues=false;
   private boolean hasNumericValues=false;
   private boolean hasDecimalValues=false;
   private boolean hasEmptyValues=false;
   private int maxLength=0;
   private ArrayList uniqueValues=new ArrayList(); 

   /// Constructor
   public DatatableColumnInfo(String columnName) {
      this.columnName=columnName;
   }

   ///

   public String getColumnName() {
      return columnName;
   }   

   public void setColumnName(String columnName) {
      this.columnName=columnName;
   }   

   ///

   public boolean getHasSingleCharacterValues() {
      return hasSingleCharacterValues;
   }   

   public void setHasSingleCharacterValues(boolean hasSingleCharacterValues) {
      this.hasSingleCharacterValues=hasSingleCharacterValues;
   }   

   ///

   public boolean getHasMultiCharacterValues() {
      return hasMultiCharacterValues;
   }   

   public void setHasMultiCharacterValues(boolean hasMultiCharacterValues) {
      this.hasMultiCharacterValues=hasMultiCharacterValues;
   }   

   ///

   public boolean getHasLowerCaseValues() {
      return hasLowerCaseValues;
   }   

   public void setHasLowerCaseValues(boolean hasLowerCaseValues) {
      this.hasLowerCaseValues=hasLowerCaseValues;
   }   

   ///

   public boolean getHasNumericValues() {
      return hasNumericValues;
   }   

   public void setHasNumericValues(boolean hasNumericValues) {
      this.hasNumericValues=hasNumericValues;
   }   

   ///

   public boolean getHasDecimalValues() {
      return hasDecimalValues;
   }   

   public void setHasDecimalValues(boolean hasDecimalValues) {
      this.hasDecimalValues=hasDecimalValues;
   }   

   ///

   public boolean getHasEmptyValues() {
      return hasEmptyValues;
   }   

   public void setHasEmptyValues(boolean hasEmptyValues) {
      this.hasEmptyValues=hasEmptyValues;
   }   

   ///

   public int getMaxLength() {
      return maxLength;
   }   

   public void setMaxLength(int maxLength) {
      this.maxLength=maxLength;
   }   

   ///

   public ArrayList getUniqueValues() {
      return uniqueValues;
   }   

   public void setUniqueValues(ArrayList uniqueValues) {
      this.uniqueValues=uniqueValues;
   }   

   ///

   public String getUniqueValueString() {
      if (uniqueValues==null || uniqueValues.size()<1 || uniqueValues.size()>15) {
         return "";
      }
      String valString=Arrays.toString(uniqueValues.toArray());
      if (valString.charAt(0)=='[' || valString.charAt(0)=='{') {
         valString=valString.substring(1,valString.length()-1);
      }
      return valString;
   }   

   ///

}


