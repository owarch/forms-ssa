 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * MediamgtController.java - Form Selection Interface
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.mla.html.table.*;

public class MediamgtController extends FORMSSsaServiceClientController {

   public ModelAndView submitRequest() throws FORMSException {

      try {

         FORMSAuth auth=this.getAuth();

         ModelAndView mav=new ModelAndView();
         HttpServletResponse response=this.getResponse();
         PrintWriter out=this.getWriter();

         // Initialize Options (If necessary)
         //auth.initOption("typeopt","ALL");
         //auth.initOption("veropt","REC");
         //auth.initOption("archopt","Y");
         //auth.initOption("fnumopt","");
         auth.initOption("sinstopt","");

         // clear stored values
         auth.setValue("mediainfoid","");
         auth.setValue("resourcetype","");
         auth.setValue("resourcetypestr","MEDIAFILE");

         MediamgtResult result=processData();
         //result.setTypeopt(auth.getOption("typeopt"));
         //result.setVeropt(auth.getOption("veropt"));
         //result.setFnumopt(auth.getOption("fnumopt"));
         result.setSinstopt(auth.getOption("sinstopt"));
         result.setRightslevel(auth.getOption("rightslevel"));
         getSession().setAttribute("iframeresult",result);
         mav.addObject("status","OK");
         return mav;
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Create root hierarchy record if none exists (these are not created at database initialization)
   private void createHaRecord(int vis) throws FORMSException, FORMSSecurityException, FORMSKeyException {
      Studies s=(Studies)getMainDAO().execUniqueQuery(
         "select s from Studies s where studyid=" + getAuth().getValue("studyid")
         );
      if (s==null) return;   
      Mediaha newha=new Mediaha();
      newha.setVisibility(vis);
      newha.setStudies(s);
      newha.setHaordr(1);
      if (vis==VisibilityConstants.STUDY) {
         newha.setHadesc("Study-Level Documents/Media");
      } else if (vis==VisibilityConstants.SITE) {
         newha.setHadesc("Site-Level Documents/Media");
      } else if (vis==VisibilityConstants.SYSTEM) {
         newha.setHadesc("System-Level Documents/Media");
      } else {
         return;
      }
      getMainDAO().saveOrUpdate(newha);
      return;

   }

   //private MediamgtResult processData(boolean showarchivedforms) throws FORMSException {
   private MediamgtResult processData() throws FORMSException {

      //MediamgtResult result=new MediamgtResult();

      try {

        FORMSAuth auth=this.getAuth();
        String siteid=auth.getValue("siteid");
        String studyid=auth.getValue("studyid");

        // Use session-stored lists if available to avoid frequent hits to database for pulling form list
        List mhatemp=(List)auth.getSessionAttribute("mediahalist");
        List minfotemp=(List)auth.getSessionAttribute("mediainfolist");
        HashMap iinfotemp=(HashMap)auth.getSessionAttribute("minfomap");
        Iterator i;

        // If no/empty list available, pull from database
        if (mhatemp==null || minfotemp==null || mhatemp.size()<1 || minfotemp.size()<1) {
         
           // Retrive installation information for later
           List ilist=getMainDAO().execQuery(
              "select a from Allinst a join fetch a.linkedinst where a.islocal!=true"
              );
           i=ilist.iterator();
           HashMap minfomap=new HashMap();
           while (i.hasNext()) {
             Allinst ainst=(Allinst)i.next();
             minfomap.put(new Long(ainst.getAinstid()).toString(),ainst.getLinkedinst().getAcronym());
           }

           mhatemp=getMainDAO().execQuery(
              "select mha from Mediaha mha " +
                    "where ((" +
                              "mha.visibility=" + VisibilityConstants.SYSTEM + 
                              ") or (" +
                              "mha.visibility=" + VisibilityConstants.SITE + " and mha.studies.sites.siteid=" + siteid + 
                              ") or (" +
                              "mha.visibility=" + VisibilityConstants.STUDY + " and mha.studies.studyid=" + studyid + 
                           ")) " +
                    "order by haordr"
              );
       
           i=mhatemp.iterator();
           // 1) Write mediahaid to StringBuilder for use in formtypes pull
           // 2) Verify that root system, site & study records exist.  Otherwise create them
           StringBuilder sb=new StringBuilder();
           boolean hassystem=false, hassite=false, hasstudy=false;
           while (i.hasNext()) {
              Mediaha ha=(Mediaha)i.next();
              // write id to stringbuffer
              sb.append(new Long(ha.getMediahaid()).toString());
              if (i.hasNext()) sb.append(",");
              // check that necessary root hierarchy records exist, else create them
              if (ha.getVisibility()==VisibilityConstants.SYSTEM && ha.getPmediahaid()==null) {
                 hassystem=true;
              } else if (ha.getVisibility()==VisibilityConstants.SITE && ha.getPmediahaid()==null) {
                 hassite=true;
              } else if (ha.getVisibility()==VisibilityConstants.STUDY && ha.getPmediahaid()==null) {
                 hasstudy=true;
              } 
           }
           if (!hassystem) createHaRecord(VisibilityConstants.SYSTEM);
           if (!hassite) createHaRecord(VisibilityConstants.SITE);
           if (!hasstudy) createHaRecord(VisibilityConstants.STUDY);
     
     
           // Pull mediainfo data (Local files)
           if (sb.length()>0) {
              minfotemp=getMainDAO().execQuery(
                 "select minf from Mediainfo minf join fetch minf.allinst join fetch minf.supportedfileformats " +
                    "where minf.pmediahaid in (" 
                       + sb.toString() + ")" +
                    "order by minf.description"
              );
           } else {
              minfotemp=(List)new ArrayList();
           }

           auth.setSessionAttribute("mediahalist",mhatemp);
           auth.setSessionAttribute("mediainfolist",minfotemp);
           auth.setSessionAttribute("minfomap",minfomap);

        };

        // Create new list based on stored list
        List mha=(List)new ArrayList(mhatemp);

        // add forms and installation info to final form list
        Iterator minfoi=minfotemp.iterator();
        List allmedialist=(List)new ArrayList();
        auth.setValue("showsinstopt","N");
        // Add forms to list
        while (minfoi.hasNext()) {
           Mediainfo minfo=(Mediainfo)minfoi.next();
           HashMap minfoimap=new HashMap();
           minfoimap.put("linstid",null);
           minfoimap.put("lstdid",null);
           minfoimap.put("instacr","LOCAL");
           minfoimap.put("media",minfo);
           allmedialist.add(minfoimap);
        }

        ///////////////////////////////////////////////////////
        // Remove media files from list based on permissions //
        ///////////////////////////////////////////////////////

        List fpermlist=getPermHelper().getMediaPermList();

        // Refine list based on options
        HashMap checkver=new HashMap();
        String typeopt=auth.getOption("typeopt");
        i=allmedialist.iterator();
        while (i.hasNext()) {
           HashMap imap=(HashMap)i.next();
           Mediainfo minfo=(Mediainfo)imap.get("media");
           String instacr=(String)imap.get("instacr");
           boolean isremoved=false;
           // Check permissions - (based on media permission list)
           // note:  Upload user has automatic rights to file
           if (!(isremoved || minfo.getUploaduser()==new Long(auth.getValue("auserid")).longValue() ||
              auth.getValue("rightslevel").equalsIgnoreCase("ADMIN"))) {
              Long mediainfoid=new Long(minfo.getMediainfoid());
              Iterator piter=fpermlist.iterator();
              boolean found=false;
              while (piter.hasNext()) {
                 HashMap map=(HashMap)piter.next();
                 Long compid;
                 try {
                    compid=new Long((String)map.get("mediainfoid"));
                 } catch (ClassCastException cce) {   
                    compid=(Long)map.get("mediainfoid");
                 }   
                 if (compid.equals(mediainfoid)) {
                    found=true;
                    break;
                 }
              }
              if (!found) {
                 i.remove();
                 isremoved=true;
              }
           }
        }

        ////////////////////////////////////////////////////////////////////////////
        // Pull remote form information (permissions determined by remote server) //
        ////////////////////////////////////////////////////////////////////////////

        List minforemote=getRemoteFiles();

        if (minforemote!=null && minforemote.size()>0) {
           auth.setValue("showsinstopt","Y");
        } else {
           auth.setValue("showsinstopt","N");
        }

        // add remote files to allmedialist
        Iterator riter=minforemote.iterator();
        while (riter.hasNext()) {
          HashMap map=(HashMap)riter.next();
          Linkedinst inst=(Linkedinst)map.get("inst");
          Linkedstudies lstd=(Linkedstudies)getMainDAO().execUniqueQuery(
             "select s from Linkedstudies s where s.linkedinst.linstid=" + inst.getLinstid() + 
                " and s.studies.studyid=" + getAuth().getValue("studyid")
             );
          List rcatlist=getMainDAO().execQuery(
             "select c from Remotemediacat c where c.lstdid=" + lstd.getLstdid()
             );
          SsaServiceListResult sslr=(SsaServiceListResult)map.get("result");
          List flist=sslr.getList();
          if (flist==null) continue;
          Iterator fiter=flist.iterator();
          while (fiter.hasNext()) {
             // create hashmap with minfo and inst info and add to allmedialist
             Mediainfo minfo=(Mediainfo)fiter.next();
             // update pmediahaid with one from remotecat
             Iterator ci=rcatlist.iterator();
             while (ci.hasNext()) {
                Remotemediacat rcat=(Remotemediacat)ci.next();
                long lid=minfo.getMediainfoid();
                long rid=rcat.getRmediainfoid();
                if (lid==rid) {
                   minfo.setPmediahaid(rcat.getPmediahaid());
                }
             }
             HashMap minfoimap=new HashMap();
             minfoimap.put("linstid",inst.getLinstid());
             minfoimap.put("lstdid",lstd.getLstdid());
             minfoimap.put("instacr",inst.getAcronym());
             minfoimap.put("media",minfo);
             allmedialist.add(minfoimap);
          }
        }

        ////////////////////////////////////////////////////////////////////
        // Organize data into heirarchical format for building HTML table //
        ////////////////////////////////////////////////////////////////////

        // minfo holds number of rows in table.  Keep this info for later
        int tablerows=allmedialist.size();

        // tlist holds table columns
        ArrayList tlist=new ArrayList();
        // alist holds items within columns
        ArrayList alist=new ArrayList();

        // pull first table column
        i=mha.iterator();
        while (i.hasNext()) {
           Mediaha ha=(Mediaha)i.next();
           if (ha.getPmediahaid()==null) {
              HashMap map=new HashMap();
              map.put("isnull","N");
              map.put("obj",ha);
              map.put("nrows",0);
              map.put("ncols",1);
              // remove element from collection so we don't have to compare it again
              i.remove();
              alist.add(map);
           }
        }   
        tlist.add(alist);
        // pull subsequent columns by finding children of elements in previous columns
        for (int ii=1;ii>-1;ii++) {
           // pull elements in prior column
           ArrayList plist=(ArrayList)tlist.get(ii-1);
           alist=new ArrayList();
           Iterator i2=plist.iterator();
           boolean again=false;
           //iterate through prior column elements
           while (i2.hasNext()) {
              // find children
              HashMap pmap=(HashMap)i2.next();
              Mediaha pha=(Mediaha)pmap.get("obj");
              i=mha.iterator();
              boolean havechild=false;
              int prevsize=alist.size();
              while (i.hasNext()) {
                 Mediaha cha=(Mediaha)i.next();
                 // write child out to alist
                 if (cha.getPmediahaid().equals(pha.getMediahaid())) {
                    HashMap cmap=new HashMap();
                    cmap.put("isnull","N");
                    cmap.put("obj",cha);
                    cmap.put("nrows",0);
                    cmap.put("ncols",1);
                    // remove element from collection so we don't have to compare it again
                    i.remove();
                    alist.add(cmap);
                    again=true;
                    havechild=true;
                 }
                 
              }
              // create null row to hold records belonging to item when child
              // categories are present
              if (havechild) {
                 HashMap nmap=new HashMap();
                 Mediaha nha=new Mediaha();
                 nha.setMediahaid(pha.getMediahaid());
                 nha.setPmediahaid(pha.getPmediahaid());
                 nha.setHadesc("&nbsp;");
                 nmap.put("isnull","Y");
                 nmap.put("obj",nha);
                 nmap.put("nrows",0);
                 nmap.put("ncols",1);
                 alist.add(prevsize,nmap);
              }
           }
           if (!again) break;
           tlist.add(alist);
        }
        // add forms to appropriate category (must iterate backwards through tlist
        // since forms are attached as the right most element)
        for (int ii=(tlist.size()-1);ii>=0;ii--) {
           // pull column arraylist
           ArrayList plist=(ArrayList)tlist.get(ii);
           Iterator i2=plist.iterator();
           while (i2.hasNext()) {
              HashMap pmap=(HashMap) i2.next();
              Mediaha pha=(Mediaha)pmap.get("obj");
              // medialist holds forms belonging to an element
              ArrayList medialist=new ArrayList();
              Iterator i3=allmedialist.iterator();
              while (i3.hasNext()) {
                 HashMap i3map=(HashMap)i3.next();
                 Mediainfo cinfo=(Mediainfo)i3map.get("media");
                 Long linstid=(Long)i3map.get("linstid");
                 Long lstdid=(Long)i3map.get("lstdid");
                 String instacr=(String)i3map.get("instacr");
                 // add form to medialist where there is a match
                 if (cinfo.getPmediahaid()==pha.getMediahaid()) {
                    HashMap flmap=new HashMap();
                    flmap.put("media",cinfo);
                    flmap.put("linstid",linstid);
                    flmap.put("lstdid",lstdid);
                    flmap.put("instacr",instacr);
                    medialist.add(flmap);
                    // remove element from collection so we don't have to compare it again
                    i3.remove();
                 }
              }
              // add medialist to element's hashmap
              if (medialist.size()>0) {
                 pmap.put("medialist",medialist);
                 pmap.put("nrows",medialist.size());
                 pmap.put("ncols",tlist.size()-ii);
              }
              // add nrows to parent cells from prior columns
              Long compid=null;
              try {
                 compid=new Long(pha.getPmediahaid());
              } catch (Exception cie) { }
              // loop through prior columns looking for parent elements
              for (int jj=(ii-1);jj>=0;jj--) {
                 ArrayList pplist=(ArrayList)tlist.get(jj);
                 Iterator i4=pplist.iterator();
                 while (i4.hasNext()) {
                    HashMap ppmap=(HashMap) i4.next();
                    Mediaha ppha=(Mediaha)ppmap.get("obj");
                    String isnull=(String)ppmap.get("isnull");
                    // if find a parent element, add rows to that element.  Then update
                    // compid to that element's parent for next loop
                    if ((
                           (!(isnull.equals("Y") || compid==null)) && ppha.getMediahaid()==compid.longValue()
                        ) ||
                        (
                           ppha.getMediahaid()==pha.getMediahaid() && ppha.getPmediahaid()==pha.getPmediahaid()
                       )) {
                       int nrows=((Integer)ppmap.get("nrows")).intValue();
                       nrows=nrows+medialist.size();
                       ppmap.put("nrows",nrows);
                       try {
                          compid=new Long(ppha.getPmediahaid());
                       } catch (Exception cie2) {}
                    }
                 }
              }
           }
        }

        /////////////////////////////////
        // Create HTML Table of result //
        /////////////////////////////////

        int tablecols=tlist.size()+1;
        if (tablerows<1 || tablecols<1) {
           throw new FORMSException("Zero length table");
        }
        Table table=new Table(tablerows,tablecols);
        // iterate through table columns;
        for (int currcol=0;currcol<tlist.size();currcol++) {
           ArrayList rowlist=(ArrayList)tlist.get(currcol);
           int prevrow=0;
           Iterator rowiter=rowlist.iterator();
           while (rowiter.hasNext()) {
              HashMap map2=(HashMap)rowiter.next();
              Mediaha ha2=(Mediaha)map2.get("obj");
              int nrows=((Integer)map2.get("nrows")).intValue();
              int ncols=((Integer)map2.get("ncols")).intValue();
              if (nrows<=0 || ncols<=0) continue;
              Cell cell=new Cell(new Long(ha2.getMediahaid()).toString(),nrows,ncols);
              cell.setContent("type","mediaha");
              cell.setContent("obj",ha2);
              boolean okvar=false;
              int currrow=prevrow;
              // Use previous row & exceptions to calculate cell row placement.  This could probably
              // be made more efficient by running through a calculation loop, but the loss here 
              // is quite minimal
              int firstrow=0;
              while (!okvar) {
                 try {
                    table.setCell(cell,currrow,currcol);
                    firstrow=currrow;
                    prevrow=currrow+1;
                    okvar=true;
                 } catch (Exception te) { 
                    currrow++;
                    if (currrow>500000) throw new FORMSException("Too many rows returned");
                 }
              }   
              // see if contains any forms (final column)
              ArrayList medialist=(ArrayList)map2.get("medialist");
              if (medialist!=null) {
                 // Rows for items must be placed based on row location of containing element
                 // (first row number of containing element is held in "firstrow")
                 int prevrow2=firstrow;
                 Iterator formiter=medialist.iterator();
                 while (formiter.hasNext()) {
                    HashMap minfomap=(HashMap)formiter.next();
                    Mediainfo minfo=(Mediainfo)minfomap.get("media");
                    Long linstid=(Long)minfomap.get("linstid");
                    Long lstdid=(Long)minfomap.get("lstdid");
                    String instacr=(String)minfomap.get("instacr");
                    cell=new Cell(new Long(minfo.getMediainfoid()).toString(),1,1);
                    cell.setContent("type","minfo");
                    cell.setContent("obj",minfo);
                    cell.setContent("instacr",instacr);
                    if (linstid==null) {
                       cell.setContent("linstid",-99999999);
                       cell.setContent("lstdid",-99999999);
                       cell.setContent("islocal",true);

                    } else {
                       cell.setContent("linstid",linstid);
                       cell.setContent("lstdid",lstdid);
                       cell.setContent("islocal",false);
                    }
                    okvar=false;
                    currrow=prevrow2;
                    while (!okvar) {
                       try {
                          table.setCell(cell,currrow,tablecols-1);
                          prevrow2=currrow+1;
                          okvar=true;
                       } catch (Exception te) {
                          currrow++;
                          if (currrow>500000) throw new FORMSException("Too many rows returned");
                       }
                    }
                 }
              }
           }
        }
        // Create Options Menu Map
        HashMap optionsmap=new HashMap();
        //optionsmap.put("typelist",typelist);
        //optionsmap.put("typeopt",auth.getOption("typeopt"));
        //optionsmap.put("veropt",auth.getOption("veropt"));
        //optionsmap.put("archopt",auth.getOption("archopt"));
        //optionsmap.put("fnumopt",auth.getOption("fnumopt"));
        optionsmap.put("sinstopt",auth.getOption("sinstopt"));
        auth.setSessionAttribute("optionsobject",optionsmap);

        // Create & return MediamgtResult Object
        MediamgtResult result=new MediamgtResult();

        result.setTable(table);

        return result;

      } catch (Exception e) {

        MediamgtResult result=new MediamgtResult();
        return result;

      }

   }

    // Retrieve list of remote media files (search preference - auth (sessionattribute), service)
    private List getRemoteFiles() {
       try {
          String studyid=getAuth().getValue("studyid");
          List rfilelist;
          try {
             rfilelist=(List)getAuth().getSessionAttribute("remotemediasessionlist");
          } catch (Exception rfe) {
             rfilelist=null;
          }
          // See if list has been pulled for study
          if (rfilelist!=null) {
             Iterator i=rfilelist.iterator();
             while (i.hasNext()) {
                HashMap map=(HashMap)i.next();
                if (studyid.equals((String)map.get("studyid"))) {
                   List srlist=(List)map.get("remotefilelist");
                   if (srlist!=null) {
                      initHeirarchy(srlist);
                      return srlist;
                   }
                }
   
             }
          } else {
             rfilelist=(List)new ArrayList();
          }
          SsaServiceSystemStudyParms sparms=new SsaServiceSystemStudyParms();
          sparms.setBeanname("getMediaServiceTarget");
          sparms.setMethodname("getMediainfo");
          List srlist=(List)submitServiceStudyRequest(sparms);
          HashMap sfmap=new HashMap();
          sfmap.put("studyid",studyid);
          sfmap.put("remotefilelist",srlist);
          rfilelist.add(sfmap);
          getAuth().setSessionAttribute("remotemediasessionlist",rfilelist);
          initHeirarchy(srlist);
          return srlist;
       } catch (Exception e) {
          // proceed without remote files
          return (List)new ArrayList(0);
       }
    }

    // This function verifies that remote files have a heirarchy entry in Remotemediacat
    // If not, entries are created
    private void initHeirarchy(List l) throws FORMSException,FORMSKeyException,FORMSSecurityException {
       Iterator i=l.iterator();
       Long pmediahaid=null;
       while (i.hasNext()) {
          HashMap map=(HashMap)i.next();
          Linkedinst inst=(Linkedinst)map.get("inst");
          Linkedstudies lstd=(Linkedstudies)getMainDAO().execUniqueQuery(
             "select s from Linkedstudies s where s.linkedinst.linstid=" + inst.getLinstid() + 
                " and s.studies.studyid=" + getAuth().getValue("studyid")
             );
          SsaServiceListResult sslr=(SsaServiceListResult)map.get("result");
          List flist=sslr.getList();
          if (flist==null) continue;
          Iterator fiter=flist.iterator();
          List rcatlist=getMainDAO().execQuery(
                "select r from Remotemediacat r where r.lstdid=" + lstd.getLstdid()
                );
          List fdidalready=(List)new ArrayList();
          while (fiter.hasNext()) {
             Mediainfo minfo=(Mediainfo)fiter.next();
             // only run first instance of mediainfoid
             if (fdidalready.contains(new Long(minfo.getMediainfoid()))) {
                continue;
             }
             fdidalready.add(new Long(minfo.getMediainfoid()));
             Iterator rcatiter=rcatlist.iterator();
             Remotemediacat rcat=null;
             while (rcatiter.hasNext()) {
                Remotemediacat ccat=(Remotemediacat)rcatiter.next();
                if (ccat.getRmediainfoid()==minfo.getMediainfoid()) {
                   rcat=ccat;
                   rcatiter.remove();
                }
             }
             // if no current record, create new one
             if (rcat==null) {
                Remotemediacat newcat=new Remotemediacat();
                newcat.setLstdid(lstd.getLstdid());
                newcat.setRmediainfoid(minfo.getMediainfoid());
                // get pmediahaid value if necessary
                if (pmediahaid==null) {
                   Mediaha ha=(Mediaha)getMainDAO().execUniqueQuery(
                      "select f from Mediaha f where f.pmediahaid is null and " +
                         "visibility=" + VisibilityConstants.STUDY + " and " +
                         "f.studies.studyid=" + getAuth().getValue("studyid")
                      );
                   if (ha==null) {
                      throw new FORMSException("Could not pull study Mediaha record");
                   }
                   pmediahaid=ha.getMediahaid();
                }
                newcat.setPmediahaid(pmediahaid.longValue());
                getMainDAO().saveOrUpdate(newcat);
             }
          }
          
       }
    }


}

