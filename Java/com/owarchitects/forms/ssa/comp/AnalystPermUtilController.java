 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * AnalystPermUtilController.java - resource permission assignment utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.security.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.*;
import javax.crypto.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;


public class AnalystPermUtilController extends FORMSController {

   boolean isbadrequest;

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      //// check permissions
      //if (!getPermHelper().hasAnalystPerm("GROUPS")) {
      //   safeRedirect("permission.err");
      //   return null;
      //}

      // Use class-name based view
      mav=new ModelAndView("AnalystPermUtil");
      isbadrequest=true;
      String spath=request.getServletPath();

      try {

         if (spath.indexOf("viewperm.")>=0) {

            List outlist=viewPerm();
            isbadrequest=false;
            mav.addObject("list",outlist);
            mav.addObject("status","VIEWPERM");
            return mav;

         } else if (spath.indexOf("permassign.")>=0) {

            String clearall=permAssign();
            if (!clearall.equals("1")) {
               mav.addObject("status","PERMASSIGNED");
            } else {
               mav.addObject("status","PERMCLEARED");
            }
            return mav;

         } else {
            out.println(spath);
            return null;
         }   

      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // View Permissions
   private List viewPerm() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Long analystinfoid=new Long(getAuth().getValue("analystinfoid"));

      // pull resource permissions 
      List p=getMainDAO().execQuery(
         "select p from Permissions p join fetch p.permissioncats " +
            "where p.permissioncats.pcatvalue in " +
               "(" + PcatConstants.ANALYST_RESOURCE + ")" +
               "order by p.permissioncats.pcatorder,p.permorder"
            );
            
      // Holders for permissions      
      BitSet resourceset=null;
      List a=null;

      if (request.getParameter("agroupid")!=null) {

         Analystgroups group=(Analystgroups)getMainDAO().execUniqueQuery(
            "select a from Analystgroups a where a.agroupid=" + (new Long(request.getParameter("agroupid")).longValue()));

         mav.addObject("acrvar",group.getGroupacr());
         mav.addObject("idvar",group.getAgroupid());
         mav.addObject("typevar","group");

         a=getMainDAO().execQuery(
            "select s from Analystgrouppermassign s where s.analystgroups.agroupid=:agroupid and s.analystinfoid=:analystinfoid"
            ,new String[] { "agroupid","analystinfoid" }
            ,new Object[] { new Long(request.getParameter("agroupid")),analystinfoid }
            );   

         // Assign values to permission holders
         Iterator ia=a.iterator();
         while (ia.hasNext()) {
            Analystgrouppermassign srp=(Analystgrouppermassign)ia.next();
            resourceset=(BitSet)srp.getPermlevel();
         }

      } else if (request.getParameter("auserid")!=null) {

         AllusersDAO adao=(AllusersDAO)this.getContext().getBean("allusersDAO");
         Allusers user=adao.getRecord(new Long(request.getParameter("auserid")).longValue());

         mav.addObject("acrvar",user.getUserdesc());
         mav.addObject("idvar",user.getAuserid());
         mav.addObject("typevar","user");

         a=getMainDAO().execQuery(
            "select s from Analystuserpermassign s " +
               "where s.allusers.auserid=:auserid and s.analystinfoid=:analystinfoid "
            ,new String[] { "auserid","analystinfoid" }
            ,new Object[] { new Long(request.getParameter("auserid")),analystinfoid }
            );   
         
         if (a.size()>0) {
            mav.addObject("haveperm",new Boolean(true));
         } else {
            mav.addObject("haveperm",new Boolean(false));
         }

         // Assign values to permission holders
         Iterator ia=a.iterator();
         while (ia.hasNext()) {
            Analystuserpermassign srp=(Analystuserpermassign)ia.next();
            resourceset=(BitSet)srp.getPermlevel();
         }

      }   
      // Use values from permission holders to set "checked" values in input fields
      Iterator ip=p.iterator();
      ArrayList newlist=new ArrayList();
      while (ip.hasNext()) {
         HashMap map=new HashMap();
         com.owarchitects.forms.commons.db.Permissions perm=(com.owarchitects.forms.commons.db.Permissions)ip.next();
         map.put("permobj",perm);
         int pcatvalue=perm.getPermissioncats().getPcatvalue();
         map.put("pcatvalue",new PcatConstants().getFieldName(pcatvalue));
         if (pcatvalue==PcatConstants.ANALYST_RESOURCE) {
            if (resourceset!=null && resourceset.get(perm.getPermpos()-1)) {
               map.put("permchecked","checked");
            } else {
               map.put("permchecked"," ");
            }   
         } 
         // UPLOAD OPTION IS IGNORED FOR FILES
         if (!perm.getPermacr().equals("UPLOAD")) {
            newlist.add(map);
         }

      }

      return newlist;

   }


   // Assign Permissions
   private String permAssign() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Long analystinfoid=new Long(getAuth().getValue("analystinfoid"));
      String typevar=request.getParameter("typevar");
      String idvar=request.getParameter("idvar");
      String clearall=request.getParameter("clearall");
      if (clearall==null) clearall="";
      // return position parameters of set permissions for setting BitSet values
      Enumeration e=request.getParameterNames();
      ArrayList alist=new ArrayList();
      while (e.hasMoreElements()) {
         String pname=(String)e.nextElement();
         if (pname.substring(0,2).equalsIgnoreCase("r_")) {
            String pvalue=request.getParameter(pname);
            if (pvalue.equals("1")) {
               com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
                  "select p from Permissions p join fetch p.permissioncats " +
                     "where p.permid=:permid "
                  ,new String[] { "permid" }
                  ,new Object[] { new Long(pname.substring(2)) }
                  );
               alist.add(p);
            }
         }
      }

      // process group permissions
      if (typevar.equalsIgnoreCase("GROUP")) {

         Analystgroups group=(Analystgroups)getMainDAO().execUniqueQuery(
            "select a from Analystgroups a where a.agroupid=" + (new Long(idvar).longValue()));

         List l=getMainDAO().execQuery(
            "select p.permissioncats.pcatvalue,max(p.permpos) " +
               "from Permissions p " +
               "group by p.permissioncats.pcatvalue " 
               );
         // This having clause was throwing errors.  I think it's valid, so this
         // might be a bug.  We'll handle the subset later.
         //  + "having p.permissioncats.pcatvalue in ....."

         // Initialize BitSets
         BitSet resourceset=null;
         Iterator i=l.iterator();
         while (i.hasNext()) {
            Object[] oarray=(Object[])i.next();
            int setln=((Integer)oarray[1]).intValue();
            if (((Integer)oarray[0]).intValue()==PcatConstants.ANALYST_RESOURCE) {
               resourceset=new BitSet(setln);
            } 
         }
         i=alist.iterator();
         while (i.hasNext()) {
            com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)i.next();
            if (p.getPermissioncats().getPcatvalue()==PcatConstants.ANALYST_RESOURCE) {
               // set flagged permissions in bitset
               resourceset.set(p.getPermpos()-1,true);
            } 
         }
         if (resourceset!=null) {
            // save value to database
            List srpl=getMainDAO().execQuery(
               "select srp from Analystgrouppermassign srp where srp.analystgroups.agroupid=:agroupid and srp.analystinfoid=:analystinfoid " 
               ,new String[] { "agroupid","analystinfoid" }
               ,new Object[] { new Long(group.getAgroupid()),analystinfoid }
               );   
            Analystgrouppermassign srp;   
            if (srpl.size()>=1) { 
               srp=(Analystgrouppermassign)srpl.get(0);
            } else {
               srp=new Analystgrouppermassign();
               srp.setAnalystgroups(group);
            }
            srp.setPermlevel(resourceset);
            srp.setAnalystinfoid(analystinfoid);
            getMainDAO().saveOrUpdate(srp);
         }
      } 
      
      // process user permissions
      else if (typevar.equalsIgnoreCase("USER")) {

         AllusersDAO adao=(AllusersDAO)getContext().getBean("allusersDAO");
         Allusers user=adao.getRecord(new Long(idvar).longValue());

         List l=getMainDAO().execQuery(
            "select p.permissioncats.pcatvalue,max(p.permpos) " +
               "from Permissions p " +
               "group by p.permissioncats.pcatvalue " 
               );
         // This having clause was throwing errors.  I think it's valid, so this
         // might be a bug.  We'll handle the subset later.
         //  + "having p.permissioncats.pcatvalue in ...."

         // Initialize BitSets
         BitSet resourceset=null;
         Iterator i=l.iterator();
         while (i.hasNext()) {
            Object[] oarray=(Object[])i.next();
            int setln=((Integer)oarray[1]).intValue();
            if (((Integer)oarray[0]).intValue()==PcatConstants.ANALYST_RESOURCE) {
               resourceset=new BitSet(setln);
            } 
         }
         i=alist.iterator();
         while (i.hasNext()) {
            com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)i.next();
            if (p.getPermissioncats().getPcatvalue()==PcatConstants.ANALYST_RESOURCE) {
               // set flagged permissions in bitset
               resourceset.set(p.getPermpos()-1,true);
            } 
         }

         if (resourceset!=null) {
            // save value to database
            List srpl=getMainDAO().execQuery(
               "select srp from Analystuserpermassign srp where srp.allusers.auserid=:auserid and srp.analystinfoid=:analystinfoid " 
               ,new String[] { "auserid","analystinfoid" }
               ,new Object[] { new Long(user.getAuserid()),analystinfoid }
               );   
            Analystuserpermassign srp;   
            if (srpl.size()>=1) { 
               srp=(Analystuserpermassign)srpl.get(0);
               if (clearall.equals("1")) {
                  getMainDAO().delete(srp);
               }
            } else {
               srp=new Analystuserpermassign();
               srp.setAllusers(user);
            }
            if (!clearall.equals("1")) {
               srp.setPermlevel(resourceset);
               srp.setAnalystinfoid(analystinfoid);
               getMainDAO().saveOrUpdate(srp);
            }   
         }
      }
      return clearall;

   }

}

