
/*
 *
 * AbstractUploadController.java - Contains some shared implementation for uploaders
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.apache.velocity.app.*;
import org.apache.velocity.*;
import au.id.jericho.lib.html.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.mla.html.table.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;
import java.sql.Clob;

public abstract class AbstractUploadController extends FORMSController {
 
   String spath="";
   protected ArrayList passlist=null;
   protected HashMap afhash;
   protected HashMap ffhash;
   protected String replaceparm;
   protected boolean prevInfo;
   protected Allinst localinst;

   // submit for upload
   protected ModelAndView callAttrScreen(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      mav.addObject("status","GETATTRSCREEN");
      return mav;

   }

   protected ModelAndView showAttrScreen(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {
       
      mav.addObject("status","SHOWATTRSCREEN");
      return mav;

   }

   // submit for upload
   protected ModelAndView abstractAttrSubmit(ModelAndView mav,boolean isImportType) throws Exception {

    try {

      FORMSAuth auth=getAuth();

      FormMetaUtil.setLastupdatelocal(getMainDAO());

      ///////////////////////////////
      ///////////////////////////////
      // PROCESS ATTRIBUTES SCREEN //
      ///////////////////////////////
      ///////////////////////////////

      Datatabledef currdatatabledef=null;

      // These are needed for attributes records
      Studies currstudy=(Studies)getMainDAO().execUniqueQuery(
         "select s from Studies s join fetch s.sites where s.studyid=" + auth.getValue("studyid")
         );

      Integer visibility;
      if (isImportType) {
         visibility=new Integer(auth.getValue("datatablelevel"));
      } else {
         visibility=new Integer(auth.getValue("formlevel"));
      }
         
      //
      // REMOVE CURRENT ATTRIBUTE, DATATABLECOLDEF RECORDS
      // 
      String dtdefid="";
      if (((Boolean)auth.getObjectValue("hasdatatabledef")).booleanValue()) {

          dtdefid=auth.getValue("dtdefid");

          currdatatabledef=(Datatabledef)getMainDAO().execUniqueQuery(
             "select f from Datatabledef f where f.dtdefid=" + dtdefid
             );

          // replace acronym and description with recent one if requested
          if (auth.getValue("replacedesc").equalsIgnoreCase("true")) {

             currdatatabledef.setFormacr(auth.getValue("formacr"));
             currdatatabledef.setFormdesc(auth.getValue("formdesc"));
             currdatatabledef.setDtacr(auth.getValue("dtacr"));
             currdatatabledef.setDtdesc(auth.getValue("dtdesc"));
             currdatatabledef.setNotes(auth.getValue("notes"));
             getMainDAO().saveOrUpdate(currdatatabledef);

          }


          // REMOVE ATTRIBUTE RECORDS   

          List afList=getMainDAO().execQuery(
             "select fa from Formattrs fa where fa.datatabledef.dtdefid=" + dtdefid
             );

          Iterator afiter=afList.iterator();
          while (afiter.hasNext()) {
             Formattrs fa=(Formattrs)afiter.next();
             getMainDAO().delete(fa);
          }   

          List ffList=getMainDAO().execQuery(
             "select ff from Dtfieldattrs ff where ff.datatabledef.dtdefid=" + dtdefid + 
                " order by ff.fieldname,ff.attr"
             );

          Iterator ffiter=ffList.iterator();
          while (ffiter.hasNext()) {
             Dtfieldattrs ff=(Dtfieldattrs)ffiter.next();
             getMainDAO().delete(ff);
          }   

          // REMOVE DATATABLECOLDEF RECORDS

          List fcList=getMainDAO().execQuery(
             "select fc from Datatablecoldef fc where fc.datatabledef.dtdefid=" + dtdefid +
                " order by fc.columnorder "
             );

          Iterator fciter=fcList.iterator();
          while (fciter.hasNext()) {
             Datatablecoldef fc=(Datatablecoldef)fciter.next();
             getMainDAO().delete(fc);
          }   

      } else {

          // Must persist datatabledef record
          currdatatabledef=(Datatabledef)auth.getObjectValue("datatabledefobject");
          getMainDAO().saveOrUpdate(currdatatabledef);

          dtdefid=new Long(currdatatabledef.getDtdefid()).toString();
          auth.setObjectValue("datatabledefobject",currdatatabledef);

      }

      // persist formtypes record if necessary
      // note:  need formtypes object for later
      Formtypes currformtypes=null;
      if (!(isImportType || ((Boolean)auth.getObjectValue("hasformtypes")).booleanValue())) {

         currformtypes=(Formtypes)auth.getObjectValue("formtypesobject");
         currformtypes.setDatatabledef(currdatatabledef);
         getMainDAO().saveOrUpdate(currformtypes);

      } else {

         currformtypes=(Formtypes)auth.getObjectValue("formtypesobject");

      }

      // REMOVE CURRENT FORMSSOURCE IF ANY AND OTHER FORMTYPES IF REPLACING ALL
      String replacestatus=auth.getValue("replacestatus");
      List sourcelist=(List)new ArrayList();
      if (replacestatus.equalsIgnoreCase("REPLACEALL")) {
         // remove other formtypes
         List typelist=getMainDAO().execQuery(
            "select f from Formtypes f where f.datatabledef.dtdefid=" + dtdefid + 
               " and f.formtypeid!=" + currformtypes.getFormtypeid()
            );
         Iterator i=typelist.iterator();
         while (i.hasNext()) {
            Formtypes ftype=(Formtypes)i.next();
            getMainDAO().delete(ftype);
         }
         // select all formstore for dtdefid into sourcelist
         sourcelist=getMainDAO().execQuery(
            "select s from Formstore s where s.formtypes.datatabledef.dtdefid=" + dtdefid
            );
      } else if (replacestatus.equalsIgnoreCase("REPLACECURR")) {
         // remove formsource for current formtype
         sourcelist=getMainDAO().execQuery(
            "select s from Formstore s where s.formtypes.formtypeid=" + currformtypes.getFormtypeid()
            );
      }
      Iterator siter=sourcelist.iterator();
      while (siter.hasNext()) {
         Formstore source=(Formstore)siter.next();
         getMainDAO().delete(source);
      }

      Map attrmap=(Map)getRequest().getParameterMap();

      String[] defaultArray=new String[0];
      try {
         String defaultList;
         try {
            String[] ova=(String[])attrmap.get((Object)"APPLYDEFAULTS");
            if (ova[0].trim().length()>0) {
               defaultList=ova[0];
               defaultArray=defaultList.split(":");
            }
         } catch (Exception e) {
            String ova=(String)attrmap.get((Object)"APPLYDEFAULTS");
            if (ova.trim().length()>0) {
               defaultList=ova;
               defaultArray=defaultList.split(":");
            }   
         }
      } catch (NullPointerException npe) { }

      if (defaultArray.length>0) {

         // eliminate current defaults
         List l=getMainDAO().execQuery(
            "select f from Dtfieldattrdefaults f where f.studies.studyid=" + currstudy.getStudyid() + 
               " and f.visibility=" + visibility
               );
         Iterator liter=l.iterator();
         while (liter.hasNext()) {
            Dtfieldattrdefaults ffa=(Dtfieldattrdefaults)liter.next();
            getMainDAO().delete(ffa);
         }
      }

      Iterator i=attrmap.keySet().iterator();
      while (i.hasNext()) {
         String kv=(String)i.next();
         // Write FORM ATTRIBUTES
         if (kv.toUpperCase().indexOf("_ACTION_")==0) {
            // SKIP THIS FIELD FOR THESE PURPOSES
         } else if (kv.toUpperCase().indexOf("_FORM_")==0) {
            String outattr=kv.replaceAll("^_[Ff][Oo][Rr][Mm]_","");
            String outvalue="";
            try {
               String[] ova=(String[])attrmap.get((Object)kv);
               outvalue=ova[0];
            } catch (Exception e) {
               String ova=(String)attrmap.get((Object)kv);
               outvalue=ova;
            }
            Formattrs fa=new Formattrs();
            fa.setDatatabledef(currdatatabledef);
            fa.setAttr(outattr);
            fa.setValue(outvalue);
            getMainDAO().save(fa);
         } else if (kv.indexOf("_")>=0) {
            String outfield=kv.substring(0,kv.lastIndexOf("_"));
            String outattr=kv.substring(kv.lastIndexOf("_")+1);
            String outvalue="";
            try {
               String[] ova=(String[])attrmap.get((Object)kv);
               outvalue=ova[0];
            } catch (Exception e) {
               String ova=(String)attrmap.get((Object)kv);
               outvalue=ova;
            }

            Dtfieldattrs ff=new Dtfieldattrs();
            ff.setDatatabledef(currdatatabledef);
            ff.setFieldname(outfield);
            ff.setAttr(outattr);
            if (!outattr.equalsIgnoreCase("QTEXT")) {
               ff.setValue(outvalue);
            }  else {
               ff.setValue(outvalue.replace("&","&amp;").replace("<","&lt;").replace(">","&gt;"));
            }

            // Don't save field type.  That may change for multiple types of the same form (HTML & DataEntry)
            if (!ff.getAttr().equalsIgnoreCase("FT")) {
               // Save field attributes to table
               getMainDAO().save(ff);
               // Save field cross-form study defaults if requested
               for (int k=0;k<defaultArray.length;k++) {
                  if (defaultArray[k].equals(outfield)) {
                     Dtfieldattrdefaults fd=new Dtfieldattrdefaults();
                     fd.setStudies(currstudy);
                     fd.setVisibility(visibility);
                     fd.setFieldname(outfield);
                     fd.setAttr(outattr);
                     fd.setValue(outvalue);
                     getMainDAO().save(fd);
                     break;
                  }   
               }
            }
         }
      }

      ///////////////////////////////////////// 
      ///////////////////////////////////////// 
      // CONSTRUCT JavaScript for form Entry //
      ///////////////////////////////////////// 
      ///////////////////////////////////////// 

      // 
      // re-pull newly saved, ordered attributes
      // 
      List afList=getMainDAO().execQuery(
         "select fa from Formattrs fa where fa.datatabledef.dtdefid=" + currdatatabledef.getDtdefid()
         );
      List ffList=getMainDAO().execQuery(
         "select ff from Dtfieldattrs ff where ff.datatabledef.dtdefid=" + currdatatabledef.getDtdefid() +
            " order by ff.fieldname,ff.attr"
         );


      // IMPORTANT!  Initialize values assigned in following loop to assure an old
      // value is not being maintained.  This can happen especially for checkboxes,
      // because they don't send their value when submitted if they are not checked.
      String formTable="";
      String formIsEnroll="";
      String formCheckEnroll="";
      String formValidate="";
      String formIntsave="";
      String formHideScore="";
      String formScoreJs="";
      String formValidateJs="";
      String formSubmitJs="";
      String formUploadJs="";
      String formBgcolor="";
      String formErrorcolor="";
      String formBordercolor="";
      
      // Create new javascript string buffer
      // 
      StringBuilder jsb=new StringBuilder();

      Iterator afiter=afList.iterator();
      while (afiter.hasNext()) {
         Formattrs fa=(Formattrs)afiter.next();
         
         if (fa.getAttr().equalsIgnoreCase("DSN_")) {
         
            formTable=fa.getValue();

         } else if (fa.getAttr().equalsIgnoreCase("ISENROLL_")) {

            formIsEnroll=fa.getValue();

         } else if (fa.getAttr().equalsIgnoreCase("CHECKENROLL_")) {

            formCheckEnroll=fa.getValue();

         } else if (fa.getAttr().equalsIgnoreCase("VALIDATE_")) {

            formValidate=fa.getValue();

         } else if (fa.getAttr().equalsIgnoreCase("INTSAVE_")) {

            formIntsave=fa.getValue();

         } else if (fa.getAttr().equalsIgnoreCase("HIDESCORE_")) {

            formHideScore=fa.getValue();

         } else if (fa.getAttr().equalsIgnoreCase("SCOREJS_")) {

            formScoreJs=fa.getValue();

         } else if (fa.getAttr().equalsIgnoreCase("VALIDATEJS_")) {

            formValidateJs=fa.getValue();

         } else if (fa.getAttr().equalsIgnoreCase("SUBMITJS_")) {

            formSubmitJs=fa.getValue();

         } else if (fa.getAttr().equalsIgnoreCase("UPLOADJS_")) {

            formUploadJs=fa.getValue();

         } else if (fa.getAttr().equalsIgnoreCase("BGCOLOR_")) {

            formBgcolor=fa.getValue();

         } else if (fa.getAttr().equalsIgnoreCase("ERRORCOLOR_")) {

            formErrorcolor=fa.getValue();

         } else if (fa.getAttr().equalsIgnoreCase("BORDERCOLOR_")) {

            formBordercolor=fa.getValue();

         }

      }   
      
      if (formIsEnroll.trim().length()<1) {
         currdatatabledef.setIsenrollform(false);
         getMainDAO().saveOrUpdate(currdatatabledef);
      } else if (formIsEnroll.trim().equals("1")) {
         // Update datatabledef record to reflect this as an enrollment form
         currdatatabledef.setIsenrollform(true);
         getMainDAO().saveOrUpdate(currdatatabledef);
      } 

      //
      // FORM FIELD JAVASCRIPT CREATION (ALSO CREATE DATATABLECOLDEF RECORDS)
      //

      // reorganize ffList & passlist, so passlist contains all information from ffList in its hashmap
      Iterator pssiter=passlist.iterator();
      long ordervar=0l;
      while (pssiter.hasNext()) {
         HashMap pssmap=(HashMap)pssiter.next();
         String fieldname=(String)pssmap.get("name");
         // begin pulling attributes for fieldname
         Iterator ffiter=ffList.iterator();
         while (ffiter.hasNext()) {
            Dtfieldattrs attr=(Dtfieldattrs)ffiter.next();
            if (attr.getFieldname().equals(fieldname)) {
               pssmap.put(attr.getAttr(),attr.getValue());
               ffiter.remove();
            }
         }
         // Begin creating stuff from hashmap info

         ordervar++;
         Datatablecoldef datatablecoldef=new Datatablecoldef();
         datatablecoldef.setDatatabledef(currdatatabledef);
         datatablecoldef.setColumnname(fieldname.toUpperCase());
         datatablecoldef.setColumnorder(ordervar);
            
         auth.setObjectValue("columnorderlist",new ArrayList());

         String fieldtype=null;
         if (!isImportType) {
            fieldtype=pssmap.get("type").toString();
         } else {

            fieldtype="text";

         }

         String kvalue;

         if (!isImportType) {
            jsb.append("\nfunction e_" + fieldname + "(thisobj) {\n");
         } else {
            jsb.append("\nfunction e_" + fieldname.toUpperCase() + "(thisobj) {\n");
         }

         /*
         kvalue=(String)pssmap.get("SPEC");
         if (kvalue==null || kvalue.trim().length()<1) {
            datatablecoldef.setIsidfield(false);
            datatablecoldef.setIsecode(false);
            datatablecoldef.setIskeyfield(false);
            jsb.append("   _varRequired=\"N\";\n");
         } else if (kvalue.equalsIgnoreCase("REQ")) {
            datatablecoldef.setIsidfield(false);
            datatablecoldef.setIsecode(false);
            datatablecoldef.setIskeyfield(false);
            jsb.append("   _varRequired=\"Y\";\n");
         } else if (kvalue.equalsIgnoreCase("KEY")) {
            datatablecoldef.setIsidfield(false);
            datatablecoldef.setIsecode(false);
            datatablecoldef.setIskeyfield(true);
            jsb.append("   _varRequired=\"Y\";\n");
         } else if (kvalue.equalsIgnoreCase("ID")) {
            datatablecoldef.setIsidfield(true);
            datatablecoldef.setIsecode(false);
            datatablecoldef.setIskeyfield(true);
            jsb.append("   _varRequired=\"Y\";\n");
         } else if (kvalue.equalsIgnoreCase("ECODE")) {
            datatablecoldef.setIsidfield(false);
            datatablecoldef.setIsecode(true);
            datatablecoldef.setIskeyfield(false);
            jsb.append("   _varRequired=\"N\";\n");
         }
         */

         kvalue=(String)pssmap.get("ID");
         if (kvalue==null || (!kvalue.equals("1"))) {
            // not an ID field
            datatablecoldef.setIsidfield(false);

            // Get ecode field attribute
            kvalue=(String)pssmap.get("ECODE");
            if (kvalue==null || (!kvalue.equals("1"))) {

               // not a ECODE field
               datatablecoldef.setIsecode(false);

               // Get key field attribute
               kvalue=(String)pssmap.get("KEY");
               if (kvalue==null || (!kvalue.equals("1"))) {
                  // not a key field
                  datatablecoldef.setIskeyfield(false);
                  // see if required
                  kvalue=(String)pssmap.get("REQ");
                  if (kvalue==null || (!kvalue.equals("1"))) {
                     jsb.append("   _varRequired=\"N\";\n");
                  } else {
                     jsb.append("   _varRequired=\"Y\";\n");
                  }
      
               } else {
                  // is a key field
                  datatablecoldef.setIskeyfield(true);
                  jsb.append("   _varRequired=\"Y\";\n");
               }
   
            } else {
               // is a ECODE field
               datatablecoldef.setIsecode(true);
               datatablecoldef.setIskeyfield(false);
               jsb.append("   _varRequired=\"Y\";\n");
            }

         } else {
            // is an ID field
            datatablecoldef.setIsidfield(true);
            datatablecoldef.setIskeyfield(true);
            datatablecoldef.setIsecode(false);
            jsb.append("   _varRequired=\"Y\";\n");
         }

         kvalue=(String)pssmap.get("VT");
         boolean ischar=false;
         if (kvalue!=null && fieldtype!=null && fieldtype.equalsIgnoreCase("text")) {
            if (kvalue.equals("INT")) {
               jsb.append(
                   "   _objType=\"NF\";\n" +
                   "   _varInteger=\"Y\";\n" +
                   "   _varCaps=\"Y\";\n" 
               );
            } else if (kvalue.equals("DEC")) {
               jsb.append(
                   "   _objType=\"NF\";\n" +
                   "   _varInteger=\"N\";\n" +
                   "   _varCaps=\"Y\";\n" 
               );
            } else if (kvalue.equals("CAPS")) {
               jsb.append(
                   "   _objType=\"CF\";\n" +
                   "   _varCaps=\"Y\";\n" 
               );
               ischar=true;
            } else if (kvalue.equals("MIXC")) {
               jsb.append(
                   "   _objType=\"CF\";\n" +
                   "   _varCaps=\"N\";\n" 
               );
               ischar=true;
            } else if (kvalue.equals("DATE")) {
               jsb.append(
                   "   _objType=\"DT\";\n" +
                   "   _varCaps=\"Y\";\n" 
               );
               ischar=true;
            } else if (kvalue.equals("TIME")) {
               jsb.append(
                   "   _objType=\"DT\";\n" +
                   "   _varCaps=\"Y\";\n" 
               );
            } else if (kvalue.equals("DTTM")) {
               jsb.append(
                   "   _objType=\"DT\";\n" +
                   "   _varCaps=\"Y\";\n" 
               );
            } else if (kvalue.equals("DRUG")) {
               jsb.append(
                   "   _objType=\"DC\";\n" +
                   "   _varCaps=\"Y\";\n" 
               );
               ischar=true;
            }
         }
         if  (kvalue!=null && fieldtype!=null && fieldtype.equalsIgnoreCase("textarea")) {
            if (kvalue.equals("CAPS")) {
               jsb.append(
                   "   _objType=\"CF\";\n" +
                   "   _varCaps=\"Y\";\n" 
               );
               ischar=true;
            } else if (kvalue.equals("MIXC")) {
               jsb.append(
                   "   _objType=\"CF\";\n" +
                   "   _varCaps=\"N\";\n" 
               );
               ischar=true;
            } 
         } else if (kvalue!=null && fieldtype!=null && fieldtype.equalsIgnoreCase("select_single")) {
            jsb.append(
                "   _objType=\"DD\";\n" 
            );
         } else if (kvalue!=null && fieldtype!=null && fieldtype.equalsIgnoreCase("radio")) {
            jsb.append(
                "   _objType=\"RB\";\n" 
            );
         } else if (kvalue!=null && fieldtype!=null && fieldtype.equalsIgnoreCase("checkbox")) {
            jsb.append(
             "   _objType=\"CB\";\n" 
            );
         } 

         // get length from gui assigned length.  If none, get from maxlength
         if (!isImportType) {
            kvalue=(String)pssmap.get("LENGTH");
         } else {   
            kvalue=(String)pssmap.get("LEN");
         } 
         if (kvalue!=null && fieldtype!=null && fieldtype.equalsIgnoreCase("textarea")) {
            jsb.append("   _varLength=\"" + kvalue + "\";\n");
         } else {
            Integer kinteger=(Integer)pssmap.get("maxlength");
            if (kinteger!=null) {
               jsb.append("   _varLength=\"" + kinteger + "\";\n");
            } else if (isImportType && kvalue!=null) {
               jsb.append("   _varLength=\"" + kvalue + "\";\n");
            } else if (ischar) {
               // Default length for character fields
               jsb.append("   _varLength='15';\n");
            }
         }
         
         kvalue=(String)pssmap.get("PAT");
         if (kvalue!=null && kvalue.trim().length()>0) {
            jsb.append("   _varPattern=\"" + kvalue.replace("\\","\\\\\\\\").replaceAll("\"","\\\\\\\\\\\\\"") + "\";\n");
         } else {
            jsb.append("   _varPattern=\"NONE\";\n");
         }

         kvalue=(String)pssmap.get("MSS");
         if (kvalue!=null && kvalue.trim().length()>0) {
            jsb.append("   _varMissingArray=\"" + kvalue.replace("\\","\\\\\\\\").replaceAll("[\"']","") + "\";\n");
         } else {
            jsb.append("   _varMissingArray=\"NONE\";\n");
         }

         kvalue=(String)pssmap.get("CNL");
         if (kvalue!=null && kvalue.trim().length()>0) {
            jsb.append("   _conList=\"" + kvalue.replace("\\","\\\\\\\\").replaceAll("[\"']","") + "\";\n");
         } else {
            jsb.append("   _conList=\"NONE\";\n");
         }

         kvalue=(String)pssmap.get("CNC");
         if (kvalue!=null && kvalue.trim().length()>0) {
            jsb.append("   _conCode=\"" + kvalue.replace("\\","\\\\\\\\\\").replaceAll("\"","\\\\\\\\\\\\\"") + "\";\n");
         } else {
            jsb.append("   _conCode=\"NONE\";\n");
         }

         kvalue=(String)pssmap.get("CNP");
         if (kvalue!=null && kvalue.trim().length()>0) {
            jsb.append("   _conPopUp=\"" + kvalue + "\";\n");
         } else {
            jsb.append("   _conPopUp=\"NONE\";\n");
         }

         jsb.append("}\n\n");
         getMainDAO().save(datatablecoldef);

      }   

      org.apache.velocity.Template template = null;
      StringWriter sout=null;
      VelocityContext vcontext=null;

   
      // 
      // Merge generated JavaScript with template write to sout
      // 
  
      // Transform formValidate to Y/N
      if (formValidate.trim().length()<1) {
         formValidate="N";
      } else {
         formValidate="Y";
      }
      if (formHideScore.trim().length()<1) {
         formHideScore="N";
      } else {
         formHideScore="Y";
      }
      if (formCheckEnroll.trim().length()<1) {
         formCheckEnroll="N";
      } else {
         formCheckEnroll="Y";
      }
 
      template = getVelocityTemplate("FormUploadController.jst");
      sout=new StringWriter();
      vcontext=new VelocityContext();
      vcontext.put("formvalidate",formValidate);
      vcontext.put("formintsave",formIntsave);
      vcontext.put("formhidescore",formHideScore);
      vcontext.put("formcheckenroll",formCheckEnroll);
      vcontext.put("formscorejs",formScoreJs);
      vcontext.put("formvalidatejs",formValidateJs);
      vcontext.put("formsubmitjs",formSubmitJs);
      vcontext.put("formuploadjs",formUploadJs);
      vcontext.put("formbgcolor",ColorConvertUtil.convert(formBgcolor));
      vcontext.put("formerrorcolor",ColorConvertUtil.convert(formErrorcolor));
      vcontext.put("formbordercolor",ColorConvertUtil.convert(formBordercolor));
      vcontext.put("fieldjs",jsb.toString());
      template.merge(vcontext, sout);

   
      // create map to keep track of field lengths.  Will be used to add LEN attributes to dtfieldattr table
      HashMap<String,String> lenMap=new HashMap();
      
      //////////////////////
      // create SAS views //
      //////////////////////
   
      // create passlist entry for formdataok
      HashMap okmap=new HashMap();
      okmap.put("name","FORMDataOK");
      okmap.put("LBL","FORMDataOK");
      okmap.put("VT","INT");
      okmap.put("FT","CheckBox");
      okmap.put("isnum",new Boolean(true));
      passlist.add(okmap);

      // construct view program test
      try {
         StringBuilder vsb=new StringBuilder();
         // Create eav view
         vsb.append(
            " \n" +
            "proc sql;\n" +
            "create view viewlib." + formTable + "_eav as \n" +
            "   select b.datarecordid,b.varname,b.varvalue\n" +
            "     from FORMSLib.datatablerecords a,\n" +
            "          FORMSLib.datatablevalues b\n" +
            "    where a.datarecordid=b.datarecordid and a.dtdefid=" + currdatatabledef.getDtdefid() + "\n" +
            "      and a.ishold='F' and a.isaudit='F' \n" + 
            "    order by datarecordid;\n" +
            "quit;\n" +
            "run;\n" +
            " \n" 
         );

         StringBuilder lsb=new StringBuilder();
         StringBuilder ssb=new StringBuilder();
         pssiter=passlist.iterator();
         while (pssiter.hasNext()) {
            HashMap pssmap=(HashMap)pssiter.next();
            String fieldname=(String)pssmap.get("name");
            Boolean isnum=(Boolean)pssmap.get("isnum");
            String vt=(String)pssmap.get("VT");
            if (vt.equalsIgnoreCase("INT") || vt.equalsIgnoreCase("DEC") || vt.equalsIgnoreCase("DATE") || vt.equalsIgnoreCase("TIME") || vt.equalsIgnoreCase("DTTM")) {

               ////////////////////
               // NUMERIC FIELDS //
               ////////////////////

               // get length & labels
               lsb.append(
                  "   length " + fieldname + " 8.;\n" +
                  "   label " + fieldname + "='" + (String)pssmap.get("LBL") + "';\n" 
               );
               // get len attribute for dtfieldattrs table
               String lvalue;
               if (!isImportType) {
                  lvalue=(String)pssmap.get("LENGTH");
               } else {   
                  lvalue=(String)pssmap.get("LEN");
               } 
               if (lvalue!=null) {
                  lenMap.put(fieldname,lvalue);
               } else {
                  Integer linteger=(Integer)pssmap.get("maxlength");
                  if (linteger!=null) {
                     lenMap.put(fieldname,linteger.toString());
                  } else {
                     // Default length for character fields
                     lenMap.put(fieldname,"15");
                  }
               }
               // character to numeric transformation
               if (vt.equalsIgnoreCase("INT") || vt.equalsIgnoreCase("DEC")) {
                  String mss=(String)pssmap.get("MSS");
                  if (mss!=null) {
                     mss=mss.toUpperCase().replaceAll("[^A-Z]","");
                     if (mss.length()>0) {
                        for (int j=0;j<mss.length();j++) {
                           char c=mss.charAt(j);
                           if (j>0) ssb.append("   else");
                           ssb.append(
                              "    if _cc_" + fieldname + "='" + c + "' then do;\n" + 
                              "       " + fieldname + "=." + c + ";\n" + 
                              "    end;\n"
                           );   
                        }
                        ssb.append(
                           "    else do;\n" + 
                           "      " + fieldname + "=input(trim(left(_cc_" + fieldname + ")),15.);\n" +
                           "    end;\n"
                        );   
                     } else {
                        ssb.append(
                           "   " + fieldname + "=input(trim(left(_cc_" + fieldname + ")),15.);\n" 
                        );   
                     }
                  } else {
                     ssb.append(
                        "   " + fieldname + "=input(trim(left(_cc_" + fieldname + ")),15.);\n" 
                     );   
                  } 
               } else if (vt.equalsIgnoreCase("DATE") || vt.equalsIgnoreCase("TIME") || vt.equalsIgnoreCase("DTTM")) {
                  String mss=(String)pssmap.get("MSS");
                  String varpattern=(String)pssmap.get("PAT");
                  String assstring=null;
                  if (varpattern!=null && varpattern.length()>0) {

                      if (varpattern.toLowerCase().matches("mm[\\W]dd[\\W]yy")) {

                         ssb.append("   format " + fieldname + " mmddyy8.;\n");
                         assstring="   " + fieldname + "=input(trim(left(_cc_" + fieldname + ")),mmddyy8.);\n";

                      } else if (varpattern.toLowerCase().matches("mmddyy")) {

                         ssb.append("   format " + fieldname + " mmddyy6.;\n");
                         assstring="   " + fieldname + "=input(trim(left(_cc_" + fieldname + ")),mmddyy6.);\n";

                      } else if (varpattern.toLowerCase().matches("mm[\\W]dd[\\W]yyyy")) {

                         ssb.append("   format " + fieldname + " mmddyy10.;\n");
                         assstring="   " + fieldname + "=input(trim(left(_cc_" + fieldname + ")),mmddyy10.);\n"; 

                      } else if (varpattern.toLowerCase().matches("yyyy[\\W]mm[\\W]dd")) {

                         ssb.append("   format " + fieldname + " yymmdd10.;\n");
                         assstring="   " + fieldname + "=input(trim(left(_cc_" + fieldname + ")),yymmdd10.);\n";

                      } else if (varpattern.toLowerCase().matches("yy[\\W]mm[\\W]dd")) {

                         ssb.append("   format " + fieldname + " yymmdd8.;\n");
                         assstring="   " + fieldname + "=input(trim(left(_cc_" + fieldname + ")),yymmdd8.);\n";

                      } else if (varpattern.toLowerCase().matches("yymmdd")) {

                         ssb.append("   format " + fieldname + " yymmdd6.;\n");
                         assstring="   " + fieldname + "=input(trim(left(_cc_" + fieldname + ")),yymmdd6.);\n";

                      } else if (varpattern.toLowerCase().matches("hh:mm")) {

                         ssb.append("   format " + fieldname + " time5.;\n");
                         assstring="   " + fieldname + "=input(trim(left(_cc_" + fieldname + ")),time5.);\n";

                      } else if (varpattern.toLowerCase().matches("hhmm")) {

                         ssb.append("   format " + fieldname + " time4.;\n");
                         assstring="   " + fieldname + "=input(trim(left(_cc_" + fieldname + ")),time4.);\n";

                      } else if (varpattern.toLowerCase().matches("mm[\\W]dd[\\W]yyyy[\\W]hh[\\W]mm")) {

                         ssb.append("   format " + fieldname + " datetime19.;\n");
                         assstring="   " + fieldname + "=input(substr(trim(left(_cc_" + fieldname + ")),1,10),mmddyy10.)*24*60*60+" +
                                   "input(substr(trim(left(_cc_" + fieldname + ")),12,5),time5.);\n";

                      } else if (varpattern.toLowerCase().matches("mm[\\W]dd[\\W]yy[\\W]hh[\\W]mm")) {

                         ssb.append("   format " + fieldname + " datetime14.;\n");
                         assstring="   " + fieldname + "=input(substr(trim(left(_cc_" + fieldname + ")),1,8),mmddyy8.)*24*60*60+" +
                                   "input(substr(trim(left(_cc_" + fieldname + ")),10,5),time5.);\n";

                      }

                  }
                  if (mss!=null) {
                     mss=mss.toUpperCase().replaceAll("[^A-Z]","");
                     if (mss.length()>0) {
                        for (int j=0;j<mss.length();j++) {
                           char c=mss.charAt(j);
                           if (j>0) ssb.append("   else");
                           ssb.append(
                              "    if _cc_" + fieldname + "='" + c + "' then do;\n" + 
                              "       " + fieldname + "=." + c + ";\n" + 
                              "    end;\n"
                           );   
                        }
                        ssb.append(
                           "    else do;\n" + 
                              assstring +
                           "    end;\n"
                        );   
                     } else {
                        ssb.append( assstring );   
                     }
                  } else {
                     ssb.append( assstring );   
                  } 
               } 
               ssb.append(
                   "   drop _cc_" + fieldname + ";\n" 
               );   

            } else {

               //////////////////////
               // CHARACTER FIELDS //
               //////////////////////

               // get length & labels
               String lvalue;
               if (!isImportType) {
                  lvalue=(String)pssmap.get("LENGTH");
               } else {   
                  lvalue=(String)pssmap.get("LEN");
               } 
               if (lvalue!=null) {
                  lsb.append( "   length " + fieldname + " $" + lvalue + ".;\n" );
                  lsb.append( "   format " + fieldname + " $" + lvalue + ".;\n" );
                  lsb.append( "   informat " + fieldname + " $" + lvalue + ".;\n" );
                  lenMap.put(fieldname,lvalue);
               } else {
                  Integer linteger=(Integer)pssmap.get("maxlength");
                  if (linteger!=null) {
                     lsb.append( "   length " + fieldname + " $" + linteger + ".;\n" );
                     lsb.append( "   format " + fieldname + " $" + linteger + ".;\n" );
                     lsb.append( "   informat " + fieldname + " $" + linteger + ".;\n" );
                     lenMap.put(fieldname,linteger.toString());
                  } else {
                     // Default length for character fields
                     lsb.append( "   length " + fieldname + " $15.;\n" );
                     lsb.append( "   format " + fieldname + " $15.;\n" );
                     lsb.append( "   informat " + fieldname + " $15.;\n" );
                     lenMap.put(fieldname,"15");
                  }
               }
               lsb.append( "   label " + fieldname + "='" + (String)pssmap.get("LBL") + "';\n" );

            }
         }

         vsb.append(
            " \n" +
            "data viewlib." + formTable + " / view=viewlib." + formTable + ";\n" +
               lsb.toString() +
            "    merge\n"
         );
   

         pssiter=passlist.iterator();
         while (pssiter.hasNext()) {
   
            HashMap pssmap=(HashMap)pssiter.next();
            String fieldname=(String)pssmap.get("name");
            Boolean isnum=(Boolean)pssmap.get("isnum");
            String vt=(String)pssmap.get("VT");
            if (vt.equalsIgnoreCase("INT") || vt.equalsIgnoreCase("DEC") || vt.equalsIgnoreCase("DATE") || vt.equalsIgnoreCase("TIME") || vt.equalsIgnoreCase("DTTM")) {
               vsb.append(
                  "      viewlib." + formTable + "_eav(keep=datarecordid varname varvalue rename=(varname=__varname datarecordid=__datarecordid varvalue=_cc_" + fieldname + 
                     ") where=(__varname='" + fieldname.toUpperCase() + "')) \n"
               );
            } else {
               vsb.append(
                  "      viewlib." + formTable + "_eav(keep=datarecordid varname varvalue rename=(varname=__varname datarecordid=__datarecordid varvalue=" + fieldname + 
                     ") where=(__varname='" + fieldname.toUpperCase() + "')) \n"
               );
            }

         }

         vsb.append(
            "   ;\n" +
            "   by __datarecordid;\n" +
               ssb.toString() +
            "   drop __datarecordid __varname;\n" +
            "run;\n" +
            " \n" 
         );

         com.sas.sasserver.submit.SubmitInterface sas=SessionObjectUtil.getSasConnection(getRequest(),getResponse()).getSubmit();
         sas.setProgramText(vsb.toString());

      } catch (Exception viewex) {
         // continue processing
      }

      // 
      // Write len attributes to dtfieldattr table if none exists
      //
      Iterator<Map.Entry<String,String>> liter=lenMap.entrySet().iterator();
      while (liter.hasNext()) {

         Map.Entry<String,String> lentry=liter.next();
         String fldName=(lentry).getKey();
         String fldValue=(lentry).getValue();
         
         List<Dtfieldattrs> dtfList=getMainDAO().execQuery(
            "select ff from Dtfieldattrs ff where ff.datatabledef.dtdefid=" + currdatatabledef.getDtdefid() +
               "   and ff.fieldname='" + fldName + "' and ff.attr='LEN'"  +
               " order by ff.fieldname,ff.attr"
            );
         if (dtfList.isEmpty()) {
            Dtfieldattrs ff=new Dtfieldattrs();
            ff.setDatatabledef(currdatatabledef);
            ff.setFieldname(fldName);
            ff.setAttr("LEN");
            ff.setValue(fldValue);
            getMainDAO().save(ff);
         }
      }

      //////////////////////////////////
      //////////////////////////////////
      // BUILD FORM WHERE APPROPRIATE // 
      //////////////////////////////////
      //////////////////////////////////


      if (isImportType) {

         auth.setValue("dtdefid",new Long(currdatatabledef.getDtdefid()).toString());
         buildFormFromAttributes();

      }

      /////////////////////////////////////
      /////////////////////////////////////
      // ADD BUTTONS AND SCRIPTS TO FORM //
      /////////////////////////////////////
      /////////////////////////////////////
      
      // 
      // Get header buttons HTML
      // 

      template = getVelocityTemplate("FormUploadController.htt");
      StringWriter hbout=new StringWriter();
      vcontext=new VelocityContext();
      vcontext.put("section","HEADERBUTTONS");
      vcontext.put("checkenroll",formCheckEnroll);
      template.merge(vcontext, hbout);

      // 
      // Get end buttons HTML
      // 

      StringWriter ebout=new StringWriter();
      vcontext.put("section","ENDBUTTONS");
      template.merge(vcontext, ebout);

      // 
      // Insert JavaScript, Buttons, etc. into form
      // 
      Source outsrc=new Source(new StringReader(auth.getValue("outhtmlstring")));
      OutputDocument outdoc=new OutputDocument(outsrc);
      // delete any user-supplied form elements.  entire body must be the form.
      int pos=0;
      while (1==1) {
         Element formele=outsrc.findNextElement(pos,HTMLElementName.FORM);
         if (formele!=null) {
            pos=formele.getBegin()+1;
            StartTag formst=formele.getStartTag();
            if (formst!=null) outdoc.remove(formst);
            EndTag formet=formele.getEndTag();
            if (formet!=null) outdoc.remove(formet);
         } else {
            break;
         }
      }
      Element bodyele=outsrc.findNextElement(0,HTMLElementName.BODY);
      Segment bodyseg=bodyele.getContent();
      // Insert JavaScript prior to <body> tag
      outdoc.insert(bodyele.getStartTag().getEnd()+1,"\n" + hbout + "\n"); 
      outdoc.insert(bodyele.getEndTag().getBegin(),"\n" + ebout + "\n"); 
      outdoc.insert(bodyele.getStartTag().getBegin(),"\n" + sout.toString() + "\n"); 
      Map m=bodyele.getStartTag().getAttributes().populateMap(new HashMap(),false);
      Iterator kiter=m.entrySet().iterator();
      boolean haveOnload=false;
      boolean haveOnbefore=false;
      while (kiter.hasNext()) {
         Map.Entry entry=(Map.Entry)kiter.next();
         String key=entry.getKey().toString();
         if (key.toLowerCase().equals("onload")) {
            haveOnload=true;
            String _keyval=entry.getValue().toString();
            entry.setValue("loadScript();" + _keyval);
         }
         if (key.toLowerCase().equals("onbeforeupload")) {
            haveOnbefore=true;
            String _keyval=entry.getValue().toString();
            entry.setValue(_keyval + ";return saveStatus()");
         }
      }
      if (!haveOnload) {
         m.put("onload","loadScript()");
      }
      if (!haveOnbefore) {
         m.put("onbeforeunload","return saveStatus()");
      }
      outdoc.replace(bodyele.getAttributes(),m);

      //
      // Store form source
      //
      if (!isImportType) {
         //Clob formclob=Hibernate.createClob(outdoc.toString());
         Formstore fstore=new Formstore();
         fstore.setFormtypes(currformtypes);
         //fstore.setFormcontent(formclob);
         fstore.setFormcontent(outdoc.toString());
         getMainDAO().save(fstore);
      } else {

         Formstore fstore=new Formstore();
         fstore.setFormtypes(getOrCreateFormtypes(currdatatabledef,visibility));
         fstore.setFormcontent(outdoc.toString());
         getMainDAO().save(fstore);

      }
   
      //
      // Upload form meta-data
      //

      // Clear out form session lists, so form list is updated
      clearSessionLists();

      mav.addObject("status","UPLOADCOMPLETE");
      mav.addObject("replacestatus",replacestatus);
      FormMetaUtil.setLastupdatelocal(getMainDAO());
      return mav;

    } catch (Exception eee) {
       out.println(Stack2string.getString(eee));
       return null;
    }

   }

   
   // Returns missing value list from value list
   protected StringBuilder getMissingList(StringBuilder insb) {
   
      StringBuilder outsb=new StringBuilder("");
      String[] varray=insb.toString().split(",");
      boolean havenum=false;
      boolean gt1=false;
      for (int l=0;l<varray.length;l++) {
         try {
            Integer j=new Integer(varray[l]);
            havenum=true;
         } catch (Exception ne) {
            if (varray[l].toString().trim().length()>1) {
               gt1=true;
            }
         }
      }
      if (havenum && !gt1) {
         for (int l=0;l<varray.length;l++) {
            try {
               Integer j=new Integer(varray[l]);
            } catch (Exception ne) {
               if (varray[l].toString().trim().length()==1) {
                  if (outsb.length()==0) {
                     outsb.append(varray[l]);
                  } else {
                     outsb.append("," + varray[l]);
                  }
               }
            }
         }
      }
      return outsb;

   }

   // Adds value to existing attribute value or creates new attribute with given value
   protected void addAttributeValue(Map m,String attrname,String attrvalue) {
      if (m.containsKey(attrname)) {
         String newvalue=attrvalue + ";" + m.get(attrname);
         m.put(attrname,newvalue);
      } else {
         m.put(attrname,attrvalue);
      }
   }

   // Empty session-stored forms lists so data are repulled
   protected void clearSessionLists() {   
      try {
         getAuth().setSessionAttribute("formhalist",null);
         getAuth().setSessionAttribute("dthalist",null);
         getAuth().setSessionAttribute("formtypeslist",null);
         getSession().setAttribute("fpermlist",null);
         getSession().setAttribute("ftypespermlist",null);
      } catch (Exception e) { }
   }  

   protected class RadioEntry {

      String name;
      String value;

      public RadioEntry(String name,String value) {
         this.name=name;
         this.value=value;
      }

      public void setName(String name) {
         this.name=name;
      }

      public void setValue(String value) {
         this.value=value;
      }

      public String getName() {
         return name;
      }

      public String getValue() {
         return value;
      }

      public int hashCode() {
         return name.hashCode() + value.hashCode();
      }

      public boolean equals(Object o) {
         if (o!=null && o.getClass().equals(this.getClass())) {
            RadioEntry e=(RadioEntry) o;
            if (e.getName().equals(this.getName()) && e.getValue().equals(this.getValue())) {
               return true; 
            }   
         }
         return false;
      }

   }

   public boolean isValidLong(String ins) {
      try {
         // treat values with leading zeros as character for these purposes
         if (ins.matches("^[0]+[1-9]")) {
            return false;
         }
         Long.parseLong(ins);
         return true;
      } catch (Exception e) {
         return false;
      }
   }

   public boolean isValidFloat(String ins) {
      try {
         // treat values with leading zeros as character for these purposes
         if (ins.matches("^[0]+[1-9]")) {
            return false;
         }
         Float.parseFloat(ins);
         return true;
      } catch (Exception e) {
         return false;
      }
   }

   public boolean isSingleChar(String ins) {
      try {
         if (ins.trim().length()==1 && !isValidLong(ins)) {
            return true;
         }
      } catch (Exception e) { }
      return false;
   }

   public boolean isMultiChar(String ins) {
      try {
         // NOTE:  Count leading zero values as multi-char
         if (ins.trim().length()>1 && (ins.trim().matches("^[0]+[1-9]") || !isValidFloat(ins))) {
            return true;
         }
      } catch (Exception e) { }
      return false;
   }

   public boolean hasLowerCase(String ins) {
      try {
         if (!ins.equals(ins.toUpperCase())) {
            return true;
         }
      } catch (Exception e) { }
      return false;
   }

   private void buildFormFromAttributes() throws Exception {

      FORMSAuth auth=getAuth();
      VelocityContext vcontext=null;
      org.apache.velocity.Template template = null;

      // construct fieldlist from attributes
      ArrayList<HashMap> fieldList=new ArrayList();
      List<String> colList=getMainDAO().execQuery(
         "select d.columnname from Datatablecoldef d where d.datatabledef.dtdefid=" +
             auth.getValue("dtdefid") + " order by d.columnorder"
             );
      List<Dtfieldattrs> fieldAttrs=getMainDAO().execQuery(
        "select ff from Dtfieldattrs ff where ff.datatabledef.dtdefid=" + 
             auth.getValue("dtdefid")  
         );
      Iterator<String> colIter=colList.iterator();
      int colCount=0;
      while (colIter.hasNext()) {
         colCount++;
         HashMap fieldMap=new HashMap();
         String column=colIter.next();
         Iterator<Dtfieldattrs> fiter=fieldAttrs.iterator();
         String qtext="";
         String lbl="";
         int found=0;
         // first get question text
         while (fiter.hasNext()) {
            Dtfieldattrs attr=fiter.next();
            if (!attr.getFieldname().equalsIgnoreCase(column)) {
               continue;
            }
            if (attr.getAttr().equalsIgnoreCase("LBL")) {
               found++;
               String attValue=attr.getValue();
               if (attValue!=null && attValue.trim().length()>0) {
                  lbl=attValue;
               }
            }
            if (attr.getAttr().equalsIgnoreCase("QTEXT")) {
               found++;
               String attValue=attr.getValue();
               if (attValue!=null && attValue.trim().length()>0) {
                  qtext=attValue;
               }
            }
            if (found==2) {
               break;
            }
         }
         if (qtext.length()>0) {
            fieldMap.put("fieldtext",qtext.replace("&amp;","&").replace("&lt;","<").replace("&gt;",">"));
         } else if (lbl.length()>0) {
            fieldMap.put("fieldtext",lbl);
         } else {
            fieldMap.put("fieldtext","FIELD " + colCount + ":");
         }

         // next get form field type
         String fieldType="";
         fiter=fieldAttrs.iterator();
         while (fiter.hasNext()) {
            Dtfieldattrs attr=fiter.next();
            if (!attr.getFieldname().equalsIgnoreCase(column)) {
               continue;
            }
            if (attr.getAttr().equalsIgnoreCase("FRMTYP")) {
               String attValue=attr.getValue();
               if (attValue!=null && attValue.trim().length()>0) {
                  fieldType=attValue;
               }
            }
         }

         // TEXT FIELDS
         if (fieldType.equalsIgnoreCase("Text")) {

            template = getVelocityTemplate("TextField.vm");
            StringWriter fieldOut=new StringWriter();
            vcontext=new VelocityContext();
            // get length 
            String len="";
            fiter=fieldAttrs.iterator();
            while (fiter.hasNext()) {
               Dtfieldattrs attr=fiter.next();
               if (!attr.getFieldname().equalsIgnoreCase(column)) {
                  continue;
               }
               if (attr.getAttr().equalsIgnoreCase("LEN")) {
                  String attValue=attr.getValue();
                  if (attValue!=null && attValue.trim().length()>0) {
                     len=attValue;
                  }
               }
            }
            int sizeVal=30;
            vcontext.put("name",column);
            vcontext.put("length",new Integer(len).intValue());
            template.merge(vcontext, fieldOut);
            fieldMap.put("fieldinput",fieldOut.toString());

         }

         // CHECK BOXES
         if (fieldType.equalsIgnoreCase("CheckBox")) {

            template = getVelocityTemplate("CheckBox.vm");
            StringWriter fieldOut=new StringWriter();
            vcontext=new VelocityContext();
            // get length 
            String cnl="";
            fiter=fieldAttrs.iterator();
            while (fiter.hasNext()) {
               Dtfieldattrs attr=fiter.next();
               if (!attr.getFieldname().equalsIgnoreCase(column)) {
                  continue;
               }
               if (attr.getAttr().equalsIgnoreCase("CNL")) {
                  String attValue=attr.getValue();
                  if (attValue!=null && attValue.trim().length()>0) {
                     cnl=attValue;
                  }
               }
            }
            int sizeVal=30;
            vcontext.put("name",column);
            vcontext.put("value",cnl);
            template.merge(vcontext, fieldOut);
            fieldMap.put("fieldinput",fieldOut.toString());

         }

         // RADIO BUTTON /  DROP DOWN 
         if (fieldType.equalsIgnoreCase("DropDown") || fieldType.equalsIgnoreCase("RadioButton")) {

            if (fieldType.equalsIgnoreCase("DropDown")) {
               template = getVelocityTemplate("DropDown.vm");
            } else if (fieldType.equalsIgnoreCase("RadioButton")) {
               template = getVelocityTemplate("RadioButton.vm");
            }
            StringWriter fieldOut=new StringWriter();
            vcontext=new VelocityContext();
            // get length 
            String cnl="";
            fiter=fieldAttrs.iterator();
            while (fiter.hasNext()) {
               Dtfieldattrs attr=fiter.next();
               if (!attr.getFieldname().equalsIgnoreCase(column)) {
                  continue;
               }
               if (attr.getAttr().equalsIgnoreCase("CNL")) {
                  String attValue=attr.getValue();
                  if (attValue!=null && attValue.trim().length()>0) {
                     cnl=attValue;
                  }
               }
            }
            List valueList=Arrays.asList(cnl.split(","));
            int sizeVal=30;
            vcontext.put("name",column);
            vcontext.put("valuelist",valueList);
            template.merge(vcontext, fieldOut);
            fieldMap.put("fieldinput",fieldOut.toString());

         }
         fieldList.add(fieldMap);
      }
             
      // merge fieldlist into form template
      org.apache.velocity.Template formTemplate = null;
      StringWriter formOut=null;

      formTemplate = getVelocityTemplate("DataTableForm.vm");
      formOut=new StringWriter();
      vcontext=new VelocityContext();
      vcontext.put("dtdesc",auth.getValue("dtdesc"));
      vcontext.put("dtacr",auth.getValue("dtacr"));
      vcontext.put("fieldlist",fieldList);
      formTemplate.merge(vcontext, formOut);
      auth.setValue("outhtmlstring",formOut.toString());

   }

   private Formtypes getOrCreateFormtypes(Datatabledef dtdef,Integer visibility) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=getAuth();

      // Get HTML formformat record used by _INTERNAL_ forms
      Formformats fformat=(Formformats)getMainDAO().execUniqueQuery(
              "select f from Formformats f " +
                 "where f.supportedfileformats.fileformat='HTML Document' or contenttype='text/html'"
                  );
      if (fformat==null) throw new FORMSException("Unable to pull unique HTML Formformat record");            

      // Pull or create Formtypelist

      Formtypelist ftypelist;
      // See if _internal_ Formtype already exists at level.  If so, return it, otherwise create it.
      List ftList=getMainDAO().execQuery(
         "select f from Formtypelist f where " +
            "f.allinst.ainstid=" + localinst.getAinstid() + " and " +
            "f.formtype='_INTERNAL_' and " +
            "f.visibility=" + visibility + " and " +
            "((f.visibility=" + VisibilityConstants.SYSTEM + ") or " +
            " (f.visibility=" + VisibilityConstants.SITE + " and f.studies.sites.siteid=" + auth.getValue("siteid") + ") or" +
            " (f.visibility=" + VisibilityConstants.STUDY + " and f.studies.studyid=" + auth.getValue("studyid") + "))" 
         );

      if (ftList==null || ftList.size()<1) {

         // Return local allinst record
         Allinst ainst=(Allinst)getMainDAO().execUniqueQuery("select a from Allinst a where a.islocal=true");
         // If not duplicate, add to database
         ftypelist=new Formtypelist();
         ftypelist.setVisibility(visibility);
         Studies std=(Studies)getMainDAO().execUniqueQuery(
               "select std from Studies std join fetch std.sites " +
                  "where std.sites.siteid=" + auth.getValue("siteid") +  
                  " and std.studyid=" + auth.getValue("studyid") 
            );
         ftypelist.setStudies(std);
         ftypelist.setAllinst(ainst);
         ftypelist.setFormtype("_INTERNAL_");
         getMainDAO().saveOrUpdate(ftypelist);

      } else {

         ftypelist=(Formtypelist)ftList.get(0);

      }

      // pull or create formtypes record
      Formtypes ftype;
      ftype=(Formtypes)getMainDAO().execUniqueQuery(
         "select f from Formtypes f " + 
          " where f.formtypelist.formtypelistid=" + ftypelist.getFormtypelistid() + " and " +
              "f.datatabledef.dtdefid=" + dtdef.getDtdefid() + " and " +
              "f.formformats.formformatid=" + fformat.getFormformatid() 
         );
      if (ftype==null) {
         ftype=new Formtypes();
         ftype.setDatatabledef(dtdef);
         ftype.setFormtypelist(ftypelist);
         ftype.setFormformats(fformat);
         ftype.setUploaduser(new Long(auth.getValue("auserid")).longValue());
         ftype.setUploadtime(new Date());
         getMainDAO().saveOrUpdate(ftype);
      }
      return ftype;

   }

}


