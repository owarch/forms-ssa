 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * SyspermUtilController.java - System permission assignment utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.security.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.*;
import javax.crypto.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;


public class SyspermUtilController extends FORMSController {

   boolean isbadrequest;

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // check permissions
      if (!hasGlobalPerm(PcatConstants.SYSTEM,"ROLEPERM")) {
         safeRedirect("permission.err");
         return null;
      }

      // Use class-name based view
      mav=new ModelAndView("SyspermUtil");
      isbadrequest=true;
      String spath=request.getServletPath();

      try {

         if (spath.indexOf("viewperm.")>=0) {

            List outlist=viewPerm();
            isbadrequest=false;
            mav.addObject("list",outlist);
            mav.addObject("status","VIEWPERM");
            return mav;

         } else if (spath.indexOf("permassign.")>=0) {

            String clearall=permAssign();
            if (!clearall.equals("1")) {
               mav.addObject("status","PERMASSIGNED");
            } else {
               mav.addObject("status","PERMCLEARED");
            }
            return mav;

         } else {
            out.println(spath);
            return null;
         }   

      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // View Permissions
   private List viewPerm() {

      // pull system/site/study permissions 
      List p=getMainDAO().execQuery(
         "select p from Permissions p join fetch p.permissioncats " +
            "where p.permissioncats.pcatvalue in " +
               "(" + PcatConstants.SYSTEM + "," + PcatConstants.SITE + "," + PcatConstants.STUDY + ")" +
               "order by p.permissioncats.pcatorder,p.permorder"
            );
            
      // Holders for permissions      
      BitSet systemset=null, siteset=null, studyset=null;
      List a=null;

      if (request.getParameter("rroleid")!=null) {

         ResourcerolesDAO rdao=(ResourcerolesDAO)this.getContext().getBean("resourcerolesDAO");
         Resourceroles role=rdao.getRecord(new Long(request.getParameter("rroleid")).longValue());

         mav.addObject("acrvar",role.getRoleacr());
         mav.addObject("idvar",role.getRroleid());
         mav.addObject("typevar","role");

         a=getMainDAO().execQuery(
            "select s from Sysrolepermassign s where s.role.rroleid=:rroleid"
            ,new String[] { "rroleid" }
            ,new Object[] { new Long(request.getParameter("rroleid")) }
            );   

         // Assign values to permission holders
         Iterator ia=a.iterator();
         while (ia.hasNext()) {
            Sysrolepermassign srp=(Sysrolepermassign)ia.next();
            int pcatvalue=srp.getPermissioncats().getPcatvalue();
            if (pcatvalue==PcatConstants.SYSTEM) {
               systemset=(BitSet)srp.getPermlevel();
            } else if (pcatvalue==PcatConstants.SITE) {
               siteset=(BitSet)srp.getPermlevel();
            } else if (pcatvalue==PcatConstants.STUDY) {
               studyset=(BitSet)srp.getPermlevel();
            }
         }

      } else if (request.getParameter("auserid")!=null) {

         AllusersDAO adao=(AllusersDAO)this.getContext().getBean("allusersDAO");
         Allusers user=adao.getRecord(new Long(request.getParameter("auserid")).longValue());

         mav.addObject("acrvar",user.getUserdesc());
         mav.addObject("idvar",user.getAuserid());
         mav.addObject("typevar","user");

         long q_siteid;
         try {
            q_siteid=new Long(getAuth().getValue("siteid")).longValue();
         } catch (Exception sie) {
            q_siteid=new Long("-999999999").longValue();
         }
         long q_studyid;
         try {
            q_studyid=new Long(getAuth().getValue("studyid")).longValue();
         } catch (Exception sie) {
            q_studyid=new Long("-999999999").longValue();
         }

         a=getMainDAO().execQuery(
            "select s from Sysuserpermassign s " +
               "where s.user.auserid=:auserid and " +
                  "((s.permissioncats.pcatvalue=" + PcatConstants.SYSTEM + ") or " + 
                   "(s.permissioncats.pcatvalue=" + PcatConstants.SITE + " and s.siteid=:siteid) or  " + 
                   "(s.permissioncats.pcatvalue=" + PcatConstants.STUDY + " and s.siteid=:siteid and s.studyid=:studyid)) " 
            ,new String[] { "siteid","studyid","auserid" }
            ,new Object[] { q_siteid, q_studyid, new Long(request.getParameter("auserid")) }
            );   
         
         if (a.size()>0) {
            mav.addObject("haveperm",new Boolean(true));
         } else {
            mav.addObject("haveperm",new Boolean(false));
         }

         // Assign values to permission holders
         Iterator ia=a.iterator();
         while (ia.hasNext()) {
            Sysuserpermassign srp=(Sysuserpermassign)ia.next();
            int pcatvalue=srp.getPermissioncats().getPcatvalue();
            if (pcatvalue==PcatConstants.SYSTEM) {
               systemset=(BitSet)srp.getPermlevel();
            } else if (pcatvalue==PcatConstants.SITE) {
               siteset=(BitSet)srp.getPermlevel();
            } else if (pcatvalue==PcatConstants.STUDY) {
               studyset=(BitSet)srp.getPermlevel();
            }
         }

      }   
      // Use values from permission holders to set "checked" values in input fields
      Iterator ip=p.iterator();
      ArrayList newlist=new ArrayList();
      while (ip.hasNext()) {
         HashMap map=new HashMap();
         com.owarchitects.forms.commons.db.Permissions perm=(com.owarchitects.forms.commons.db.Permissions)ip.next();
         map.put("permobj",perm);
         int pcatvalue=perm.getPermissioncats().getPcatvalue();
         map.put("pcatvalue",new PcatConstants().getFieldName(pcatvalue));
         if (pcatvalue==PcatConstants.SYSTEM) {
            if (systemset!=null && systemset.get(perm.getPermpos()-1)) {
               map.put("permchecked","checked");
            } else {
               map.put("permchecked"," ");
            }   
         } else if (pcatvalue==PcatConstants.SITE) {
            if (siteset!=null && siteset.get(perm.getPermpos()-1)) {
               map.put("permchecked","checked");
            } else {
               map.put("permchecked"," ");
            }   
         } else if (pcatvalue==PcatConstants.STUDY) {
            if (studyset!=null && studyset.get(perm.getPermpos()-1)) {
               map.put("permchecked","checked");
            } else {
               map.put("permchecked"," ");
            }   
         }
         newlist.add(map);

      }

      return newlist;

   }


   // Assign Permissions
   private String permAssign() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String typevar=request.getParameter("typevar");
      String idvar=request.getParameter("idvar");
      String clearall=request.getParameter("clearall");
      if (clearall==null) clearall="";
      // return position parameters of set permissions for setting BitSet values
      Enumeration e=request.getParameterNames();
      ArrayList alist=new ArrayList();
      while (e.hasMoreElements()) {
         String pname=(String)e.nextElement();
         if (pname.substring(0,2).equalsIgnoreCase("r_")) {
            String pvalue=request.getParameter(pname);
            if (pvalue.equals("1")) {
               com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
                  "select p from Permissions p join fetch p.permissioncats " +
                     "where p.permid=:permid "
                  ,new String[] { "permid" }
                  ,new Object[] { new Long(pname.substring(2)) }
                  );
               alist.add(p);
            }
         }
      }

      // process role permissions
      if (typevar.equalsIgnoreCase("ROLE")) {

         ResourcerolesDAO rdao=(ResourcerolesDAO)getContext().getBean("resourcerolesDAO");
         Resourceroles role=rdao.getRecord(new Long(idvar).longValue());

         List l=getMainDAO().execQuery(
            "select p.permissioncats.pcatvalue,max(p.permpos) " +
               "from Permissions p " +
               "group by p.permissioncats.pcatvalue " 
               );
         // This having clause was throwing errors.  I think it's valid, so this
         // might be a bug.  We'll handle the subset later.
         //  + "having p.permissioncats.pcatvalue in ....."

         // Initialize BitSets
         BitSet systemset=null, siteset=null, studyset=null;
         Iterator i=l.iterator();
         while (i.hasNext()) {
            Object[] oarray=(Object[])i.next();
            int setln=((Integer)oarray[1]).intValue();
            if (((Integer)oarray[0]).intValue()==PcatConstants.SYSTEM) {
               systemset=new BitSet(setln);
            } else if (((Integer)oarray[0]).intValue()==PcatConstants.SITE) {
               siteset=new BitSet(setln);
            } else if (((Integer)oarray[0]).intValue()==PcatConstants.STUDY) {
               studyset=new BitSet(setln);
            }
         }
         i=alist.iterator();
         while (i.hasNext()) {
            com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)i.next();
            if (p.getPermissioncats().getPcatvalue()==PcatConstants.SYSTEM) {
               // set flagged permissions in bitset
               systemset.set(p.getPermpos()-1,true);
            } else if (p.getPermissioncats().getPcatvalue()==PcatConstants.SITE) {
               // set flagged permissions in bitset
               siteset.set(p.getPermpos()-1,true);
            } else if (p.getPermissioncats().getPcatvalue()==PcatConstants.STUDY) {
               // set flagged permissions in bitset
               studyset.set(p.getPermpos()-1,true);
            }
         }
         if (systemset!=null) {
            // save value to database
            Permissioncats pcat=(Permissioncats)getMainDAO().execUniqueQuery(
               "select p from Permissioncats p where p.pcatvalue=" + PcatConstants.SYSTEM
               );
            List srpl=getMainDAO().execQuery(
               "select srp from Sysrolepermassign srp where srp.role.rroleid=:rroleid and " +
                  "srp.permissioncats.pcatid=:pcatid"
               ,new String[] { "rroleid","pcatid" }
               ,new Object[] { new Long(role.getRroleid()),new Long(pcat.getPcatid()) }
               );   
            Sysrolepermassign srp;   
            if (srpl.size()>=1) { 
               srp=(Sysrolepermassign)srpl.get(0);
            } else {
               srp=new Sysrolepermassign();
               srp.setRole(role);
               srp.setPermissioncats(pcat);
            }
            srp.setPermlevel(systemset);
            getMainDAO().saveOrUpdate(srp);
         }
         if (siteset!=null) {
            // save value to database
            Permissioncats pcat=(Permissioncats)getMainDAO().execUniqueQuery(
               "select p from Permissioncats p where p.pcatvalue=" + PcatConstants.SITE
               );
            List srpl=getMainDAO().execQuery(
               "select srp from Sysrolepermassign srp where srp.role.rroleid=:rroleid and " +
                  "srp.permissioncats.pcatid=:pcatid"
               ,new String[] { "rroleid","pcatid" }
               ,new Object[] { new Long(role.getRroleid()),new Long(pcat.getPcatid()) }
               );   
            Sysrolepermassign srp;   
            if (srpl.size()>=1) { 
               srp=(Sysrolepermassign)srpl.get(0);
            } else {
               srp=new Sysrolepermassign();
               srp.setRole(role);
               srp.setPermissioncats(pcat);
            }
            srp.setPermlevel(siteset);
            getMainDAO().saveOrUpdate(srp);
         }
         if (studyset!=null) {
            // save value to database
            Permissioncats pcat=(Permissioncats)getMainDAO().execUniqueQuery(
               "select p from Permissioncats p where p.pcatvalue=" + PcatConstants.STUDY
               );
            List srpl=getMainDAO().execQuery(
               "select srp from Sysrolepermassign srp where srp.role.rroleid=:rroleid and " +
                  "srp.permissioncats.pcatid=:pcatid"
               ,new String[] { "rroleid","pcatid" }
               ,new Object[] { new Long(role.getRroleid()),new Long(pcat.getPcatid()) }
               );   
            Sysrolepermassign srp;   
            if (srpl.size()>=1) { 
               srp=(Sysrolepermassign)srpl.get(0);
            } else {
               srp=new Sysrolepermassign();
               srp.setRole(role);
               srp.setPermissioncats(pcat);
            }
            srp.setPermlevel(studyset);
            getMainDAO().saveOrUpdate(srp);
         }
         
      } 
      
      // process user permissions
      else if (typevar.equalsIgnoreCase("USER")) {

         AllusersDAO adao=(AllusersDAO)getContext().getBean("allusersDAO");
         Allusers user=adao.getRecord(new Long(idvar).longValue());

         List l=getMainDAO().execQuery(
            "select p.permissioncats.pcatvalue,max(p.permpos) " +
               "from Permissions p " +
               "group by p.permissioncats.pcatvalue " 
               );
         // This having clause was throwing errors.  I think it's valid, so this
         // might be a bug.  We'll handle the subset later.
         //  + "having p.permissioncats.pcatvalue in ...."

         // Initialize BitSets
         BitSet systemset=null, siteset=null, studyset=null;
         Iterator i=l.iterator();
         while (i.hasNext()) {
            Object[] oarray=(Object[])i.next();
            int setln=((Integer)oarray[1]).intValue();
            if (((Integer)oarray[0]).intValue()==PcatConstants.SYSTEM) {
               systemset=new BitSet(setln);
            } else if (((Integer)oarray[0]).intValue()==PcatConstants.SITE) {
               siteset=new BitSet(setln);
            } else if (((Integer)oarray[0]).intValue()==PcatConstants.STUDY) {
               studyset=new BitSet(setln);
            }
         }
         i=alist.iterator();
         while (i.hasNext()) {
            com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)i.next();
            if (p.getPermissioncats().getPcatvalue()==PcatConstants.SYSTEM) {
               // set flagged permissions in bitset
               systemset.set(p.getPermpos()-1,true);
            } else if (p.getPermissioncats().getPcatvalue()==PcatConstants.SITE) {
               // set flagged permissions in bitset
               siteset.set(p.getPermpos()-1,true);
            } else if (p.getPermissioncats().getPcatvalue()==PcatConstants.STUDY) {
               // set flagged permissions in bitset
               studyset.set(p.getPermpos()-1,true);
            }
         }
         // get siteid, studyid
         long q_siteid;
         try {
            q_siteid=new Long(getAuth().getValue("siteid")).longValue();
         } catch (Exception sie) {
            q_siteid=new Long("-999999999").longValue();
         }
         long q_studyid;
         try {
            q_studyid=new Long(getAuth().getValue("studyid")).longValue();
         } catch (Exception sie) {
            q_studyid=new Long("-999999999").longValue();
         }

         if (systemset!=null) {
            // save value to database
            Permissioncats pcat=(Permissioncats)getMainDAO().execUniqueQuery(
               "select p from Permissioncats p where p.pcatvalue=" + PcatConstants.SYSTEM
               );
            List srpl=getMainDAO().execQuery(
               "select srp from Sysuserpermassign srp where srp.user.auserid=:auserid and " +
                  "srp.permissioncats.pcatid=:pcatid"
               ,new String[] { "auserid","pcatid" }
               ,new Object[] { new Long(user.getAuserid()),new Long(pcat.getPcatid()) }
               );   
            Sysuserpermassign srp;   
            if (srpl.size()>=1) { 
               srp=(Sysuserpermassign)srpl.get(0);
               if (clearall.equals("1")) {
                  getMainDAO().delete(srp);
               }
            } else {
               srp=new Sysuserpermassign();
               srp.setUser(user);
               srp.setPermissioncats(pcat);
            }
            if (!clearall.equals("1")) {
               srp.setPermlevel(systemset);
               getMainDAO().saveOrUpdate(srp);
            }   
         }
         if (siteset!=null) {
            // save value to database
            Permissioncats pcat=(Permissioncats)getMainDAO().execUniqueQuery(
               "select p from Permissioncats p where p.pcatvalue=" + PcatConstants.SITE
               );
            List srpl=getMainDAO().execQuery(
               "select srp from Sysuserpermassign srp where srp.user.auserid=:auserid and " +
                  "srp.permissioncats.pcatid=:pcatid and srp.siteid=:siteid"
               ,new String[] { "siteid","auserid","pcatid" }
               ,new Object[] { q_siteid,new Long(user.getAuserid()),new Long(pcat.getPcatid()) }
               );   
            Sysuserpermassign srp;   
            if (srpl.size()>=1) { 
               srp=(Sysuserpermassign)srpl.get(0);
               if (clearall.equals("1")) {
                  getMainDAO().delete(srp);
               }
            } else {
               srp=new Sysuserpermassign();
               srp.setUser(user);
               srp.setSiteid(q_siteid);
               srp.setPermissioncats(pcat);
            }
            if (!clearall.equals("1")) {
               srp.setPermlevel(siteset);
               getMainDAO().saveOrUpdate(srp);
            }
         }
         if (studyset!=null) {
            // save value to database
            Permissioncats pcat=(Permissioncats)getMainDAO().execUniqueQuery(
               "select p from Permissioncats p where p.pcatvalue=" + PcatConstants.STUDY
               );
            List srpl=getMainDAO().execQuery(
               "select srp from Sysuserpermassign srp where srp.user.auserid=:auserid and " +
                  "srp.permissioncats.pcatid=:pcatid and srp.siteid=:siteid and srp.studyid=:studyid"
               ,new String[] { "siteid","studyid","auserid","pcatid" }
               ,new Object[] { q_siteid,q_studyid,new Long(user.getAuserid()),new Long(pcat.getPcatid()) }
               );   
            Sysuserpermassign srp;   
            if (srpl.size()>=1) { 
               srp=(Sysuserpermassign)srpl.get(0);
               if (clearall.equals("1")) {
                  getMainDAO().delete(srp);
               }
            } else {
               srp=new Sysuserpermassign();
               srp.setUser(user);
               srp.setSiteid(q_siteid);
               srp.setStudyid(q_studyid);
               srp.setPermissioncats(pcat);
            }
            if (!clearall.equals("1")) {
               srp.setPermlevel(studyset);
               getMainDAO().saveOrUpdate(srp);
            }   
         }
         
      }
      // permission changes affect form sharing between installations
      FormMetaUtil.setFormModTimes(getAuth(),getMainDAO());
      return clearall;

   }

}

