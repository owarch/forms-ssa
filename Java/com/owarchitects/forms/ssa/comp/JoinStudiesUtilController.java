 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * JoinStudiesUtilController.java - Study utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;
import java.sql.Timestamp;


public class JoinStudiesUtilController extends FORMSSsaServiceClientController {

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // check permissions
      if (!getAuth().getValue("rightslevel").equalsIgnoreCase("ADMIN")) {
         safeRedirect("permission.err");
         return null;
      }

      // Use class-name based view
      mav=new ModelAndView("JoinStudiesUtil");
      String spath=request.getServletPath();

      try {

         if (spath.indexOf("joinselect1.")>=0) {

            if (joinSelect1(mav)) {
               return mav;
            }   

         } 
         if (spath.indexOf("joinselect2.")>=0) {

            if (joinSelect2(mav)) {
               return mav;
            }   

         }
         if (spath.indexOf("joinsubmit.")>=0) {

            if (joinSubmit(mav)) {
               return mav;
            }   

         }
         if (spath.indexOf("unjoin.")>=0) {

            if (unJoin(mav)) {
               return mav;
            }   

         }
         if (spath.indexOf("joininfo.")>=0) {

            if (joinInfo(mav)) {
               return mav;
            }   

         }

         mav.addObject("status","NOTPROCESSED");
         return mav;
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }


   private boolean joinSelect1(ModelAndView mav) throws FORMSException {

      try {
         List studylist=getMainDAO().getTable("Studies");
         List instlist=getMainDAO().getTable("Linkedinst");
         if (studylist.size()<1) {
            mav.addObject("status","JOINNOSTUDIES");
            return true;
         }
         if (instlist.size()<1) {
            mav.addObject("status","JOINNOINST");
            return true;
         }
         mav.addObject("studylist",studylist);
         mav.addObject("instlist",instlist);
         mav.addObject("status","JOINLINK1");
         return true;
      } catch (Exception e) {
         return false;
      }

   }


   private boolean joinSelect2(ModelAndView mav) throws FORMSException {

      try {
         String studyid=getRequest().getParameter("studyid");
         String acronym=getRequest().getParameter("instacronym");
         EmbeddedinstDAO embeddedinstDAO=(EmbeddedinstDAO)this.getContext().getBean("embeddedinstDAO");
         Embeddedinst einst=embeddedinstDAO.getRecordByAcronym(acronym);
         if (einst==null) {
            mav.addObject("status","NOINSTINFO");
          } 
         SsaServiceSystemParms parms=new SsaServiceSystemParms("joinStudiesServiceTarget","getStudiesList");

         SsaServiceListResult result=(SsaServiceListResult)this.submitServiceRequest(einst,parms);

         if (result.getStatus().intValue()==FORMSServiceConstants.OK) {
            mav.addObject("status","JOINLINK2");
            mav.addObject("list",result.getList());
            mav.addObject("instacronym",acronym);
            mav.addObject("studyid",studyid);
         } else {
            mav.addObject("status","NOINSTCONNECT");
         }
         return true;
      } catch (Exception e) {
         return false;
      }

   }


   private boolean joinSubmit(ModelAndView mav) throws FORMSException {

      try {
         String studyid=getRequest().getParameter("studyid");
         String acronym=getRequest().getParameter("instacronym");
         String rstudyid=getRequest().getParameter("remotestudyid");

         Studies std=(Studies)getMainDAO().execUniqueQuery(
                 "select std from Studies std where std.studyid=" + studyid
              );
         Linkedinst inst=(Linkedinst)getMainDAO().execUniqueQuery(
                 "select inst from Linkedinst inst where inst.acronym='" + acronym + "'"
              );
         if (std==null || inst==null) {
            return false;
         }   

         Linkedstudies ls=new Linkedstudies();
         ls.setStudies(std);
         ls.setLinkedinst(inst);
         ls.setRemotestudyid(new Long(rstudyid).longValue());
         getMainDAO().saveOrUpdate(ls);
         mav.addObject("status","JOINOK");
         // Flag user info to run updates
         Userinfomodtime u=(Userinfomodtime)getMainDAO().execUniqueQuery(
             "select a from Userinfomodtime a " +
                "where a.linkedinst.linstid=" + inst.getLinstid()
            );
         if (u==null) {
            u=new Userinfomodtime();
            u.setLinkedinst(inst);
         } 
         u.setRemotemod(new Timestamp(0));
         getMainDAO().saveOrUpdate(u);
         // Set initial dtmodtimes
         FormMetaUtil.setFormModTimes(getAuth(),getMainDAO());
         return true;

      } catch (Exception e) {
         return false;
      }

   }


   private boolean unJoin(ModelAndView mav) throws FORMSException {

      try {
         String lstdid=getRequest().getParameter("lstdid");

         Linkedstudies lstd=(Linkedstudies)getMainDAO().execUniqueQuery(
                 "select lstd from Linkedstudies lstd where lstd.lstdid=" + lstdid 
              );
         if (lstd==null) {
            return false;
         }   

         getMainDAO().delete(lstd);
         mav.addObject("status","UNJOINED");
         return true;

      } catch (Exception e) {
         return false;
      }

   }


   private boolean joinInfo(ModelAndView mav) throws FORMSException {

      try {
         String lstdid=getRequest().getParameter("lstdid");
         String acronym=getRequest().getParameter("instacronym");
         EmbeddedinstDAO embeddedinstDAO=(EmbeddedinstDAO)this.getContext().getBean("embeddedinstDAO");
         Embeddedinst einst=embeddedinstDAO.getRecordByAcronym(acronym);
         if (einst==null) {
            mav.addObject("status","NOINSTINFO");
            return true;
         } 

         Linkedstudies lstd=(Linkedstudies)getMainDAO().execUniqueQuery(
                 "select lstd from Linkedstudies lstd where lstd.lstdid=" + lstdid 
              );
         if (lstd==null) {
            return false;
         }   

         long rstudyid=lstd.getRemotestudyid();
         JoinStudiesParms parms=new JoinStudiesParms();
         parms.setBeanname("joinStudiesServiceTarget");
         parms.setMethodname("getStudy");
         parms.setStudyid(rstudyid);

         JoinStudiesResult result=(JoinStudiesResult)this.submitServiceRequest(einst,parms);

         if (result.getStatus().intValue()==FORMSServiceConstants.OK && result.getStudydesc()!=null) {
            mav.addObject("status","JOININFO");
            mav.addObject("studydesc",result.getStudydesc());
         } else {
            mav.addObject("status","NOINSTCONNECT");
         }
         return true;

      } catch (Exception e) {
         return false;
      }

   }


}























