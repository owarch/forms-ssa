
/*
 *
 * FormUploadController.java - Form Upload controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.apache.velocity.app.*;
import org.apache.velocity.*;
import au.id.jericho.lib.html.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.mla.html.table.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;
import java.sql.Clob;

public class FormUploadController extends AbstractUploadController {
 
   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Use class-name based view
      mav=new ModelAndView("FormUpload");

      FORMSAuth auth=getAuth();

      try {

         HttpServletRequest request=this.getRequest();
         spath=request.getServletPath();

         if (spath.indexOf("formupload.")>=0) {

            mav.addObject("status","DTFORMUPLOAD");
            auth.setValue("replacestatus","NONE");
            auth.setValue("replacedesc","false");
            String studyid=auth.getValue("studyid");
            // pull localinst for later use
            this.localinst=(Allinst)getMainDAO().execUniqueQuery("select l from com.owarchitects.forms.commons.db.Allinst l where l.islocal=true");
            if (studyid==null || studyid.length()<1) {
               mav.addObject("list",new VisibilityConstants().getFieldList(VisibilityConstants.SYSTEM));
            } else {
               mav.addObject("list",new VisibilityConstants().getFieldList());
            }
            return mav;

         }
         else if (spath.indexOf("formuploadreplace.")>=0) {

            auth.setValue("replacestatus",request.getParameter("replacestatus"));
            auth.setValue("replacedesc",request.getParameter("replacedesc"));
            mav.addObject("zerolength",new ArrayList());
            return callAttrScreen(mav);

         }
         else if (spath.indexOf("formupload2.")>=0) {

            formUpload2(mav);
            return mav;

         }
         else if (spath.indexOf("formuploadsubmit.")>=0) {

            formUploadSubmit(mav);
            return mav;
         }
         else if (spath.indexOf("formuploadshowattrscreen.")>=0) {

            mav.addObject("replacestatus",auth.getValue("replacestatus"));
            mav.addObject("prevInfo",new Boolean(prevInfo).toString());
            mav.addObject("afhash",afhash);
            mav.addObject("ffhash",ffhash);
            mav.addObject("list",passlist);
            mav.addObject("status","SHOWATTRSCREEN");
            return mav;

         }
         else if (spath.indexOf("formuploadattrsubmit.")>=0) {

            formUploadAttrSubmit(mav);
            return mav;

         }
         else {
            getWriter().println("BAD REQUEST! - " + spath);
            return null;
         }
   
      } catch (Exception e) { 

         mav.addObject("status","DTFORMUPLOADERROR");
         return mav;
      }

   }

   // Second upload screen (pass in formtyhpe & formformat list for selection)
   private void formUpload2(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=getAuth();

      // See if user has permission to upload forms at requested level
      if (!getPermHelper().hasGlobalPerm(auth.getValue("auserid"),
          new Integer(request.getParameter("formlevel")).intValue(),"DTFORMUPLOAD")) {
          mav.addObject("status","NOAUTH");
          return;
      }

      // pull formtypes to pass to GUI
      List formtypelist=getMainDAO().execQuery(
         "select f from Formtypelist f where " +
            "f.allinst.ainstid=" + localinst.getAinstid() + " and " +
            "f.formtype!='_INTERNAL_' and " +
            "f.visibility=" + request.getParameter("formlevel") + " and " +
            "((f.visibility=" + VisibilityConstants.SYSTEM + ") or " +
            " (f.visibility=" + VisibilityConstants.SITE + " and f.studies.sites.siteid=" + auth.getValue("siteid") + ") or" +
            " (f.visibility=" + VisibilityConstants.STUDY + " and f.studies.studyid=" + auth.getValue("studyid") + "))" 
         );

      // Using HashSet here keeps multiple rows from being returned for the parent
      // table (keeps appropriate one-to-many format).  You work with the HashSet
      // pretty much identically to the List, even through Velocity, but the parent
      // child relationship is correctly mapped
      //
      // See:  http://forum.hibernate.org/viewtopic.php?t=955186
      //
      HashSet formformatset=new HashSet(getMainDAO().execQuery(
         "select f from Formformats f join fetch f.supportedfileformats " +
            "join fetch f.supportedfileformats.fileextensions"
         ));

      mav.addObject("status","DTFORMUPLOAD2");
      mav.addObject("formtypelist",formtypelist);
      mav.addObject("formformatset",formformatset);
      mav.addObject("formtypelistsize",new Integer(formtypelist.size()));
      mav.addObject("formformatsetsize",new Integer(formformatset.size()));

      auth.setValue("formlevel",request.getParameter("formlevel"));
      auth.setObjectValue("formformatset",formformatset);

   }

   // submit for upload
   private ModelAndView formUploadSubmit(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=getAuth();

      try {

         MultipartParser mp = new MultipartParser(request, 10*1024*1024); 
         Part part;
         String fileName=null;
         String fileSource=null;
         String formacr=null, formdesc=null, formtypelistid=null, 
                fileformatid=null, formnumvar=null, formvervar=null,
                dtacr=null, dtdesc=null,
                notes="";
         HashMap attrmap=new HashMap();
   
         while ((part = mp.readNextPart()) != null) {
            String name = part.getName();
            if (part.isParam()) {
               // it's a parameter part
               ParamPart paramPart = (ParamPart) part;
               if (name.equalsIgnoreCase("formacr")) {
                  formacr = paramPart.getStringValue();
               } else if (name.equalsIgnoreCase("formdesc")) {
                  formdesc = paramPart.getStringValue();
               } else if (name.equalsIgnoreCase("formtypelistid")) {
                  formtypelistid = paramPart.getStringValue();
               } else if (name.equalsIgnoreCase("fileformatid")) {
                  fileformatid = paramPart.getStringValue();
               } else if (name.equalsIgnoreCase("formnumvar")) {
                  formnumvar = paramPart.getStringValue();
               } else if (name.equalsIgnoreCase("formvervar")) {
                  formvervar = paramPart.getStringValue();
               } else if (name.equalsIgnoreCase("dtacr")) {
                  dtacr = paramPart.getStringValue();
               } else if (name.equalsIgnoreCase("dtdesc")) {
                  dtdesc = paramPart.getStringValue();
               } else if (name.equalsIgnoreCase("notes")) {
                  notes = paramPart.getStringValue();
               }   
               // Create Attribute map used for processing form part
               attrmap.put(name,paramPart.getStringValue());
            }
            else if (part.isFile()) {
               // it's a file part
               FilePart filePart = (FilePart) part;
               fileName = filePart.getFileName();
               if (fileName != null) {
                  fileName=fileName.trim();
                  ByteArrayOutputStream baos = new ByteArrayOutputStream();
                  long size = filePart.writeTo(baos);
                  fileSource=baos.toString("ISO-8859-1");
               }
            }
         } 

         auth.setObjectValue("attrmap",attrmap);

         // These may be needed later
         auth.setValue("formacr",formacr);
         auth.setValue("formdesc",formdesc);
         auth.setValue("notes",notes);

         if (dtacr==null || dtacr.trim().length()<1) {
            dtacr=formacr;
         }
         if (dtdesc==null || dtdesc.trim().length()<1) {
            dtdesc=formdesc;
         }

         // check file name

         HashSet formformatset=(HashSet)auth.getObjectValue("formformatset");
   
         mav.addObject("formacr",formacr);
         mav.addObject("formdesc",formdesc);
         mav.addObject("formtypelistid",formtypelistid);
         mav.addObject("fileformatid",fileformatid);
         mav.addObject("filename",fileName);
         mav.addObject("formformatset",formformatset);
         mav.addObject("formnumvar",formnumvar);
         mav.addObject("formvervar",formvervar);
         mav.addObject("dtacr",dtacr);
         mav.addObject("dtdesc",dtdesc);
         mav.addObject("filesource",fileSource);
         mav.addObject("notes",notes);

         // Pull supportedfileformats record for later use
         Supportedfileformats sformat=(Supportedfileformats)getMainDAO().execUniqueQuery(
            "select f from Supportedfileformats f where f.fileformatid=" + fileformatid
            );

         //
         // Make sure fileName as proper extension
         //
         Iterator i=formformatset.iterator();
         boolean hasvalidextension=false;
         StringBuilder vesb=new StringBuilder();
         while (i.hasNext()) {
            Formformats f=(Formformats)i.next();
            if (f.getSupportedfileformats().getFileformatid()==sformat.getFileformatid()) {
               Set fes=(Set)f.getSupportedfileformats().getFileextensions();
               Iterator i2=fes.iterator();
               while (i2.hasNext()) {
                  Fileextensions e=(Fileextensions)i2.next();
                  String ext=e.getExtension();
                  vesb.append(ext);
                  if (i2.hasNext()) {
                     vesb.append(",");
                  }
                  if (fileName.toLowerCase().indexOf("." + ext.toLowerCase())>0) {
                     hasvalidextension=true;
                  }
               }
            }
         }
         if (!hasvalidextension) {
            auth.setValue("vesb",vesb.toString());
            mav.addObject("status","INVALIDEXTENSION");
            return mav;
         }

         /////////////////////////////
         // Begin parsing HTML file //
         /////////////////////////////
 
         // 
         // Create ArrayLists and HashMaps to hold parse results
         // formlist saved at class level to be retrieved after gui submission
         // 

         ArrayList formlist=new ArrayList();
         ArrayList selectlist=new ArrayList();
         ArrayList attrlist=new ArrayList();
         HashMap datatablehash=null;
         HashMap selecthash=null;
         HashMap attrhash=null;

         // Keep track of fields with no size, maxlength or value attributes (where maxlength=0)
         ArrayList zerolength=new ArrayList();

         // 
         // Parse HTML String Form Values
         // 
         StringReader sr=new StringReader(fileSource);
         Source htmlsrc=new Source(sr);
         OutputDocument outdoc=new OutputDocument(htmlsrc);
         ListIterator fci=htmlsrc.findFormControls().listIterator();
    
         // 
         // Iterate through all form controls
         // 

         // Set arraylist to hold column order for later use
         ArrayList columnOrder=new ArrayList();
         while (fci.hasNext()) {
            FormControl fc=(FormControl) fci.next();
            String eName=fc.getName();
            columnOrder.add(fc.getName());
            String eType=fc.getFormControlType().toString();
            String eValue=null;
            // 
            // Assuming one value here, is that ok for our purposes (Global fields) ????
            // 
            try {
               eValue=fc.getValues().iterator().next().toString(); 
            } catch (Exception ve) {
               // Operation not valid for all form control types
            }
    
            // 
            // Evaluate SELECT groups
            // 
            selectlist=new ArrayList();
            if (eType.length()>=6 && eType.substring(0,6).equalsIgnoreCase("select")) {
               Iterator oi=fc.getOptionElementIterator();
               while (oi.hasNext()) {
                  Source optsrc=new Source(new StringReader(oi.next().toString()));
                  //optsrc.fullSequentialParse();
                  Iterator ei=optsrc.findAllElements().iterator();
                  while(ei.hasNext()) {
                     Element opt=(Element)ei.next();
                     Iterator oi2 = opt.getAttributes().iterator();
                     while (oi2.hasNext()) {
                        Attribute entry = (Attribute) oi2.next();
                        selecthash=new HashMap();
                        selecthash.put("key",entry.getKey());
                        selecthash.put("value",entry.getValue());
                        selecthash.put("label",opt.getContent().toString());
                        selectlist.add(selecthash);
                     }
                  }
               }   
            }

            auth.setObjectValue("columnorderlist",columnOrder);
    
            //out.println(eName + " - " + eType + " - " + eValue + "<br>");
    
            // 
            // Get attributes
            // 
            Map m=fc.getAttributesMap();
            // 
            // Add escript calls to document
            // 
            addAttributeValue(m,"onfocus","escript(this)");
            addAttributeValue(m,"onkeyup","escript(this)");
            addAttributeValue(m,"onblur","escript(this)");
            addAttributeValue(m,"onclick","escript(this)");
            addAttributeValue(m,"onchange","escript(this)");
            outdoc.replace(fc.getElement().getAttributes(),m);
            Iterator mi = m.entrySet().iterator();
            attrlist=new ArrayList();
            while (mi.hasNext()) {
               Map.Entry entry = (Map.Entry) mi.next();
               attrhash=new HashMap();
               attrhash.put("key",entry.getKey());
               attrhash.put("value",entry.getValue());
               attrlist.add(attrhash);
            }
    
            datatablehash=new HashMap();
            datatablehash.put("name",eName);
            datatablehash.put("type",eType);
            datatablehash.put("value",eValue);
            datatablehash.put("slist",selectlist);
            datatablehash.put("alist",attrlist);
            formlist.add(datatablehash);
    
         }
    
         auth.setValue("outhtmlstring",outdoc.toString());
         
         // 
         // create lists, iterators and hashmaps for retrieving file info
         // 
    
         Iterator fiter;
         HashMap fmap;
    
         ArrayList alist;
         Iterator aiter;
         HashMap amap;
    
         ArrayList slist;
         Iterator siter;
         HashMap smap;
    
         // 
         // retrieve formnumber, formversion information from file to check against
         // metadata for previously entered forms, etc.
         // 
         
         String formNumber="";
         String formVersion="";
 
         fiter=formlist.iterator();
         int n=0;
 
         while (fiter.hasNext()) {
            fmap=(HashMap)fiter.next();
            String name="";
            try { name=(String)fmap.get("name"); } catch (Exception e) {}   
            if (name==null || name.toString().length()==0) {
               mav.addObject("status","UNNAMEDFIELDERROR");
               return mav;
            }
            if (name.equalsIgnoreCase(formnumvar)) {
               try { 
                     String s=(String)fmap.get("value"); 
                     if (s!=null && s.length()>0) {
                        formNumber=s;
                     }
                   } catch (Exception e) {}
    
            } else if (name.equalsIgnoreCase(formvervar)) {
               try { 
                     String s=(String)fmap.get("value"); 
                     if (s!=null && s.length()>0) {
                        formVersion=s;
                     }
                   } catch (Exception e) {}
            }
         }

    
         auth.setValue("formnum",formNumber);
         auth.setValue("formver",formVersion);
    
         if (formNumber.length()<=0 || formVersion.length()<=0) {
            //new MessageOut(request,response,'Y',"ERROR:  Unable to determine FormNumber and FormVersion from file<br>" +
            //                 "Check file structure as well as FormNumber and FormVersion field values");
            mav.addObject("status","CANTFINDFORMNUM");
            return mav;
         }
    
         //out.println("formNumber=" + formNumber + "<br>");
         //out.println("formVersion=" + formVersion + "<br>");
         //out.println("formSite=" + formSite + "<br>");
         //out.println("formStudy=" + formStudy + "<br>");

         //
         // Create list to hold fieldnames to check for duplicates
         //
         List fnamelist=new ArrayList();
         //
         // Create list to hold duplicated fieldnames
         //
         List dupfnamelist=new ArrayList();
         //
         // Create list to hold radio entries to check for duplicate values for a radio button
         //
         List rentrylist=new ArrayList();
         //
         // Create list to hold radio buttons containing duplicate entries
         //
         List duprentrylist=new ArrayList();
         //
         boolean fnamedup=false;
         boolean rentrydup=false;
    
         //////////////////////////////////////////
         //////////////////////////////////////////
         //////////////////////////////////////////
         // BEGIN CONSTRUCTION OF ATTRIBUTES GUI //
         //////////////////////////////////////////
         //////////////////////////////////////////
         //////////////////////////////////////////
    
         // 
         // Iterate over lists of fields, collecting info to pass to GUI
         // 
    
         fiter=formlist.iterator();

         // 
         // passlist will contain complete list of form fields
         // 
         passlist=new ArrayList();
         HashMap passhash;
         // will be used later
         Long pformhaid=null;

         //
         // Pull current datatabledef record if one exists for formnum/formver
         //
         List formhalist=getMainDAO().execQuery(
            "select f from Formha f " +
               "where f.visibility=" + auth.getValue("formlevel") + " and " +
                   "((f.visibility=" + VisibilityConstants.SYSTEM + ") or " +
                   " (f.visibility=" + VisibilityConstants.SITE + " and f.studies.sites.siteid=" + auth.getValue("siteid") + ") or" +
                   " (f.visibility=" + VisibilityConstants.STUDY + " and f.studies.studyid=" + auth.getValue("studyid") + "))" 
         );

         Iterator fhai=formhalist.iterator();
         StringBuilder fhasb=new StringBuilder();
         while (fhai.hasNext()) {
            Formha fha=(Formha)fhai.next();
            if (fha.getPformhaid()==null) {
               pformhaid=fha.getFormhaid();
            }
            fhasb.append(new Long(fha.getFormhaid()).toString());
            if (fhai.hasNext()) {
               fhasb.append(",");
            }
         }

         // will be used later
         Long pdthaid=null;

         //
         // Pull current datatabledef record if one exists for formnum/formver
         //
         List dthalist=getMainDAO().execQuery(
            "select f from Datatableha f " +
               "where f.visibility=" + auth.getValue("formlevel") + " and " +
                   "((f.visibility=" + VisibilityConstants.SYSTEM + ") or " +
                   " (f.visibility=" + VisibilityConstants.SITE + " and f.studies.sites.siteid=" + auth.getValue("siteid") + ") or" +
                   " (f.visibility=" + VisibilityConstants.STUDY + " and f.studies.studyid=" + auth.getValue("studyid") + "))" 
         );

         if (dthalist.size()==0) {
            // Need to create datatableha records
            dthalist=createAndReturnDatatableHaRecord(auth.getValue("formlevel"));
            if (dthalist==null || dthalist.size()==0) {
               mav.addObject("status","DTHAERROR");
               return mav;
            }
         }

         Iterator dthai=dthalist.iterator();
         StringBuilder dthasb=new StringBuilder();
         while (dthai.hasNext()) {
            Datatableha dtha=(Datatableha)dthai.next();
            if (dtha.getPdthaid()==null) {
               pdthaid=dtha.getDthaid();
            }
            dthasb.append(new Long(dtha.getDthaid()).toString());
            if (dthai.hasNext()) {
               dthasb.append(",");
            }
         }

         // Pull in form with given formno, formver

         Datatabledef cdatatabledef=(Datatabledef)getMainDAO().execUniqueQuery(
            "select f from Datatabledef f join fetch f.allinst " +
               "where f.allinst.ainstid=" + localinst.getAinstid() + " and " +
                     "f.formno='" + formNumber + "' and " +
                     "f.formver='" + formVersion + "' and " +
                     "f.pformhaid in (" + fhasb.toString() + ") and " +
                     /* -1 here is for compatibility with pre-0.5.0 version */
                     "f.pdthaid in (-1," + dthasb.toString() + ")"
            );         

         List afList=(List)new ArrayList();   
         List ffList=(List)new ArrayList();   
         List ffdList=(List)new ArrayList();   

         boolean equivFormExists=false;

         auth.setObjectValue("hasdatatabledef",new Boolean(false));

         if (cdatatabledef!=null) {

            equivFormExists=true;
            // 
            // Get any currently defined attributes into list
            // 
            afList=getMainDAO().execQuery(
               "select f from Formattrs f join fetch f.datatabledef " +
                  "where f.datatabledef.dtdefid=" + cdatatabledef.getDtdefid()
                  );

            ffList=getMainDAO().execQuery(
               "select f from Dtfieldattrs f join fetch f.datatabledef " +
                  "where f.datatabledef.dtdefid=" + cdatatabledef.getDtdefid()
                  );

            auth.setObjectValue("hasdatatabledef",new Boolean(true));
            auth.setValue("dtdefid",new Long(cdatatabledef.getDtdefid()).toString());

         } else {

            // create formtypes, datatabledef objects to be persisted in later step

            Datatabledef sdef=new Datatabledef();
            sdef.setAllinst(localinst);
            sdef.setFormno(formNumber);
            sdef.setFormver(formVersion);
            sdef.setFormacr(formacr);
            sdef.setFormdesc(formdesc);
            sdef.setIsarchived(new Boolean(false));
            sdef.setIsenrollform(new Boolean(false));
            sdef.setSsaonly(new Boolean(false));
            sdef.setDtacr(dtacr);
            sdef.setDtdesc(dtdesc);
            sdef.setNotes(notes);
            // For now, interface not assigning this value
            sdef.setDisplayas(DisplayAsConstants.FORMONLY);
            sdef.setUploaduser(new Long(auth.getValue("auserid")).longValue());
            sdef.setUploadtime(new Date());
            // value determined earlier
            sdef.setPformhaid(pformhaid);
            sdef.setPdthaid(pdthaid);
            auth.setObjectValue("datatabledefobject",sdef);

         }
    
         while (fiter.hasNext()) {
    
            StringBuilder sbval=new StringBuilder("");
            StringBuilder sbval2=new StringBuilder("");
    
            // Maxlength holds maximum field length or maximum value size
            int maxlength=0;
            int maxcharlength=0;
            fmap=(HashMap)fiter.next();
            //out.println("<br>" + fmap.get("name") + " - " + fmap.get("type") + " - " + fmap.get("value") + "<br>");
    
            alist=(ArrayList)fmap.get("alist");
            aiter=alist.iterator();
    
            // Skip global fields
            if (fmap.get("name").toString().toLowerCase().indexOf("global_")==0) {
               // drop from list
               fiter.remove();
               continue;
            }
            // Skip printer fields (designed to replace screen fields in printouts)
            if (fmap.get("name").toString().toLowerCase().indexOf("_printfield_")==0) {
               // drop from list
               fiter.remove();
               continue;
            }
    
            // 
            // Isnum holds whether value list contains only numeric or contains character values
            // 
            boolean isnum=true;
            boolean iscaps=true;
            boolean istextarea=(fmap.get("type").toString().equalsIgnoreCase("textarea"));
            boolean isselect=(fmap.get("type").toString().equalsIgnoreCase("select_single"));
            boolean ischeckbox=(fmap.get("type").toString().equalsIgnoreCase("checkbox"));
            boolean isradio=(fmap.get("type").toString().equalsIgnoreCase("radio"));
    
            if (!isselect) {       
               // Non-dropdown fields
               while (aiter.hasNext()) {
                  amap=(HashMap)aiter.next();
                  //out.println("&nbsp;&nbsp;&nbsp;&nbsp;ATTRIBUTES - " + amap.get("key") + " - " + amap.get("value") + " - " + amap.get("label") + "<br>");
                  if (amap.get("key").toString().equalsIgnoreCase("maxlength")) {
                     maxlength=new Integer(amap.get("value").toString()).intValue();
                  } else if (amap.get("key").toString().equalsIgnoreCase("size")) {
                     if (!(isselect || ischeckbox || isradio)) {
                        if (maxlength<1) maxlength=new Integer(amap.get("value").toString()).intValue();
                     }
                  }
                  if ((ischeckbox || isradio) && amap.get("key").toString().equalsIgnoreCase("value")) {
                     maxlength=amap.get("value").toString().length();
                     if (amap.get("value").toString().replaceAll("[0-9]","").trim().length()>0) {
                        maxcharlength=amap.get("value").toString().length();
                        isnum=false;
                     }   
                     if (amap.get("value").toString().trim().length()>0) {
                        if (sbval.length()==0) {
                           sbval.append(amap.get("value").toString());
                        } else {
                           sbval.append("," + amap.get("value").toString());
                        }
                     }
                     if (!(amap.get("value").toString().equals(amap.get("value").toString().toUpperCase()))) {
                        iscaps=false;
                     }
                  } else if (amap.get("key").toString().equalsIgnoreCase("value")) {
                     maxlength=amap.get("value").toString().length();
                     if (amap.get("value").toString().replaceAll("[0-9]","").trim().length()>0) {
                        maxcharlength=amap.get("value").toString().length();
                        isnum=false;
                     }   
                  } 
               }
               // Keep track of fields with no size, maxlength or value attributes (where maxlength=0, set maxlength to 100 & issue warning)
               if (maxlength==0) {
                  maxlength=100;
                  zerolength.add(fmap.get("name"));
               }
            } else {            
               // Dropdown fields
               slist=(ArrayList)fmap.get("slist");
               siter=slist.iterator();
               while (siter.hasNext()) {
                  smap=(HashMap)siter.next();
                  // NEED TO THROW/CATCH EXCEPTION HERE FOR NULL VALUE (THIS CAN PROBABLY JUST BE SKIPPED)
                  if (smap.get("value")!=null) {
                     if (!(smap.get("value").toString().equals(smap.get("value").toString().toUpperCase()))) {
                        iscaps=false;
                     }
                     if (smap.get("value").toString().replaceAll("[0-9]","").trim().length()>0) {
                        if (smap.get("value").toString().length()>maxcharlength) {
                           maxcharlength=smap.get("value").toString().length();
                        }   
                        isnum=false;
                     }
                     if (smap.get("value").toString().length()>maxlength) {
                        maxlength=smap.get("value").toString().length();
                     }
                     if (smap.get("value").toString().trim().length()>0) {
                        if (sbval.length()==0) {
                           sbval.append(smap.get("value").toString());
                        } else {
                           sbval.append("," + smap.get("value").toString());
                        }
                     }
                  }
               }
            }
    
            // 
            // Construct HashMap containing information passed to GUI
            // 
    
            if (!isradio) {
    
               sbval2.append(sbval.toString());
    
               StringBuilder msval=getMissingList(sbval2);
    
               passhash=new HashMap();
               passhash.put("name",fmap.get("name"));
               passhash.put("type",fmap.get("type"));
               passhash.put("isnum",new Boolean(isnum));
               passhash.put("iscaps",new Boolean(iscaps));
               passhash.put("maxlength",new Integer(maxlength));
               passhash.put("maxcharlength",new Integer(maxcharlength));
               passhash.put("mslist",msval.toString());
               passhash.put("cnlist",sbval2.toString());
               //passhash.put("isnum",new Boolean(isnum).toString());
               //passhash.put("maxlength",new Integer(maxlength).toString());
               passlist.add(passhash);

               // duplicate checks 
               String name=fmap.get("name").toString();
               if (fnamelist.contains(name)) {
                  dupfnamelist.add(name);
                  fnamedup=true;
               } else {
                  fnamelist.add(name);
               }

            } else {
               // 
               // Multiple objects for radio buttons.  Only output to GUI once, keeping desired attributes
               // 
               Iterator piter=passlist.iterator();
               boolean havematch=false;
               while (piter.hasNext()) {
                  HashMap psmap=(HashMap)piter.next();
                  if (fmap.get("name").equals(psmap.get("name"))) {
                     havematch=true;
                     if (!(iscaps && psmap.get("iscaps").toString().equalsIgnoreCase("true"))) {
                        iscaps=false;
                     }
                     if (!(isnum && psmap.get("isnum").toString().equalsIgnoreCase("true"))) {
                        isnum=false;
                     }
                     if (new Integer(psmap.get("maxlength").toString()).intValue()>maxlength) {
                        maxlength=new Integer(psmap.get("maxlength").toString()).intValue();
                     }
                     if (psmap.get("cnlist").toString().length()>0) {
                        sbval2.append(psmap.get("cnlist") + "," + sbval);
                     } else {
                        sbval2.append(sbval);
                     }
                     piter.remove();
                  }
               }   
               if (!havematch) {                 
                  sbval2.append(sbval);

                  // duplicate checks 
                  String name=fmap.get("name").toString();
                  if (fnamelist.contains(name)) {
                     dupfnamelist.add(name);
                     fnamedup=true;
                  } else {
                     fnamelist.add(name);
                  }

               }
    
               StringBuilder msval=getMissingList(sbval2);
    
               passhash=new HashMap();
               passhash.put("name",fmap.get("name"));
               passhash.put("type",fmap.get("type"));
               passhash.put("isnum",new Boolean(isnum));
               passhash.put("iscaps",new Boolean(iscaps));
               passhash.put("maxlength",new Integer(maxlength));
               passhash.put("maxcharlength",new Integer(maxcharlength));
               passhash.put("mslist",msval.toString());
               passhash.put("cnlist",sbval2.toString());
               passlist.add(passhash);

               // Radio button name/value duplicate check
               RadioEntry rentry;
               try {
                  String rval="";
                  List avlist=(ArrayList)fmap.get("alist");
                  Iterator aviter=avlist.iterator();
                  while (aviter.hasNext()) {
                     HashMap avmap=(HashMap)aviter.next();
                     if (avmap.get("key").toString().equalsIgnoreCase("value")) {
                        rval=avmap.get("value").toString();
                     }   
                  }   
                  rentry=new RadioEntry(fmap.get("name").toString(),rval);
               } catch (Exception e1) {
                  rentry=new RadioEntry(fmap.get("name").toString(),"");
               }
               if (rentrylist.contains(rentry)) {
                  duprentrylist.add(fmap.get("name").toString());
                  rentrydup=true;
               } else {
                  rentrylist.add(rentry);
               }
    
            } 
    
         }

         //
         // If there are any duplicates, discontinue upload processing and issue an error message
         //


         if (fnamedup) {

            mav.addObject("status","DUPLICATEFIELDNAMES");
            mav.addObject("errmsg",dupfnamelist.toString());
            return mav;

         }   

         if (rentrydup) {

            mav.addObject("status","DUPLICATERADIOVALUES");
            mav.addObject("errmsg",duprentrylist.toString());
            return mav;

         }   

         // 
         // Defined at class level, needs to persist across steps
         // 
         afhash=new HashMap();
         boolean prevInfo=false;
         if (afList.size()>=1) {
            prevInfo=true;
         }
         Iterator afiter=afList.iterator();
         while (afiter.hasNext()) {
            Formattrs fa=(Formattrs)afiter.next();
            afhash.put("_FORM_" + fa.getAttr(),fa.getValue());
         }   
    
         // 
         // Defined at class level, needs to persist across steps
         // 
         ffhash=new HashMap();
    
         if (prevInfo) {
    
            Iterator ffiter=ffList.iterator();
            while (ffiter.hasNext()) {
               Dtfieldattrs fa=(Dtfieldattrs)ffiter.next();
               ffhash.put(fa.getFieldname() + "_" + fa.getAttr(),fa.getValue());
            }   
    
         } else {
    
            // 
            // retrieve study cross-form field defaults if exist
            // 

            ffdList=getMainDAO().execQuery(
               "select f from Dtfieldattrdefaults f " +
                  "where f.visibility=" + auth.getValue("formlevel") + " and " +
                      "((f.visibility=" + VisibilityConstants.SYSTEM + ") or " +
                      " (f.visibility=" + VisibilityConstants.SITE + " and f.studies.sites.siteid=" + auth.getValue("siteid") + ") or" +
                      " (f.visibility=" + VisibilityConstants.STUDY + " and f.studies.studyid=" + auth.getValue("studyid") + "))" 
            );
   
            Iterator ffditer=ffdList.iterator();
            while (ffditer.hasNext()) {
               Dtfieldattrdefaults ffd=(Dtfieldattrdefaults)ffditer.next();
               ffhash.put(ffd.getFieldname() + "_" + ffd.getAttr(),ffd.getValue());
            }   
    
         }

         // if equivalent form already exists, compare form types to see if is replacement
         // and make sure variable list matches

         boolean varListMatches=false;
         boolean formTypeMatches=false;
         boolean hasExistingData=false;

         Formtypes currformtype=new Formtypes();
         Formtypelist ftl=(Formtypelist)getMainDAO().execUniqueQuery(
            "select f from Formtypelist f where f.formtypelistid=" + formtypelistid
            );
         Formformats ff=(Formformats)getMainDAO().execUniqueQuery(
            "select f from Formformats f where f.supportedfileformats.fileformatid=" + sformat.getFileformatid()
            );
         currformtype.setFormtypelist(ftl);
         currformtype.setFormformats(ff);
         currformtype.setUploaduser(new Long(auth.getValue("auserid")).longValue());
         currformtype.setUploadtime(new Date());
         auth.setObjectValue("hasformtypes",new Boolean(false));
         auth.setObjectValue("formtypesobject",currformtype);

         if (equivFormExists) {

            // see if upload formtype is the same as that of existing forms
            List ftl2=(List)getMainDAO().execQuery(
               "select ft from Formtypes ft where ft.formtypelist.formtypelistid=" + formtypelistid + " and " +
                  "ft.datatabledef.dtdefid=" + cdatatabledef.getDtdefid()
               );
            if (ftl2.size()>0) {
               auth.setObjectValue("formtypesobject",(Formtypes)ftl2.get(0));
               auth.setObjectValue("hasformtypes",new Boolean(true));
               formTypeMatches=true;
            }

            // check whether there is existing data for the form
            List existing=getMainDAO().execQuery(
               "select 1 from Datatablerecords f where f.datatabledef.dtdefid=" + cdatatabledef.getDtdefid() 
               );
            if (existing.size()>0) {
               hasExistingData=true;
            }

            // check whether variable list of new form matches that of old form

            List eformvarlist=getMainDAO().execQuery(
               "select f.columnname from Datatablecoldef f where f.datatabledef.dtdefid=" + cdatatabledef.getDtdefid() +
                  " order by f.columnorder "
               );

            List newvarlist=(List)new ArrayList();
            List comvarlist=(List)new ArrayList();
            List oldvarlist=(List)new ArrayList();

            // Iterate through field list of new form
            Iterator pli=passlist.iterator();
            while (pli.hasNext()) {
               HashMap map=(HashMap)pli.next();
               String newname=(String)map.get("name");
               Iterator efi=eformvarlist.iterator();
               boolean hasmatch=false;
               while (efi.hasNext()) {
                  String oldname=(String)efi.next();
                  if (oldname.trim().toLowerCase().equalsIgnoreCase(newname.trim().toLowerCase())) {
                     hasmatch=true;
                     comvarlist.add(newname);
                     efi.remove();
                  }
               }
               if (!hasmatch) {
                  newvarlist.add(newname);
               }
            }
            oldvarlist=(List)new ArrayList(eformvarlist); 
            if (comvarlist.size()>0 && newvarlist.size()==0 && oldvarlist.size()==0) {
               varListMatches=true;
            }

            mav.addObject("formtypematches",new Boolean(formTypeMatches));
            mav.addObject("varlistmatches",new Boolean(varListMatches));
            mav.addObject("hasexistingdata",new Boolean(hasExistingData));
            mav.addObject("comvarlist",comvarlist);
            mav.addObject("newvarlist",newvarlist);
            mav.addObject("oldvarlist",oldvarlist);
            mav.addObject("zerolength",zerolength);
            mav.addObject("status","DECIDE");
            return mav;

         }

         mav.addObject("zerolength",zerolength);
         return callAttrScreen(mav);

      } catch (Exception e) {

         throw new FORMSException("Form Upload Error");
      }

   }

   // submit for upload
   private ModelAndView formUploadAttrSubmit(ModelAndView mav) throws Exception {

      return abstractAttrSubmit(mav,false);

   }

   private ArrayList createAndReturnDatatableHaRecord(String invis) throws FORMSException, FORMSSecurityException, FORMSKeyException {
      try {
         int vis=Integer.parseInt(invis);
         Studies s=(Studies)getMainDAO().execUniqueQuery(
            "select s from Studies s where studyid=" + getAuth().getValue("studyid")
            );
         if (s==null) return null;   
         Datatableha newha=new Datatableha();
         newha.setVisibility(vis);
         newha.setStudies(s);
         newha.setHaordr(1);
         if (vis==VisibilityConstants.STUDY) {
            newha.setHadesc("Study-Level Tables");
         } else if (vis==VisibilityConstants.SITE) {
            newha.setHadesc("Site-Level Tables");
         } else if (vis==VisibilityConstants.SYSTEM) {
            newha.setHadesc("System-Level Tables");
         } else {
            return null;
         }
         getMainDAO().saveOrUpdate(newha);
         ArrayList returnlist=new ArrayList();
         returnlist.add(newha);
         return returnlist;
      } catch (Exception e) {
         return null;
      }
   }
   
}


