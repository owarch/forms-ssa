 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
* FormMetaUtil.java -- Utilities for accessing/modifying form meta-data
* Original Creation -- 06/2007 (MRH)
*/

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.db.*;
import com.owarchitects.forms.commons.comp.*;
import java.io.*;
import java.util.*;
import javax.servlet.http.*;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils.*;
import java.util.regex.*;
import org.apache.commons.httpclient.util.URIUtil;

public class FormMetaUtil {
   
   // Instances should not be created
   public FormMetaUtil() {
      super();
   }

   // sets local form mod times (all studies)
   public static void setLastupdatelocal(com.owarchitects.forms.commons.db.MainDAO maindao) {

      // all studies may be affected by actions
      List studylist=maindao.execQuery(
         "select l from Linkedstudies l where l.linkedinst.isdisabled!=true"
         );
      if (studylist.size()>0) {
         Iterator i=studylist.iterator();
         // update dtmodtime (creating record if necessary)
         while (i.hasNext()) {
            Linkedstudies ls=(Linkedstudies)i.next();
            Dtmodtime mt=(Dtmodtime)maindao.execUniqueQuery(
               "select f from Dtmodtime f where f.linkedstudies.lstdid=" + ls.getLstdid()
               );
            if (mt!=null) {
               // update current record
               mt.setLocalmod(new Date());
               // do not update remote mod time here
               maindao.saveOrUpdate(mt);
            } else {
               // create new record
               mt=new Dtmodtime();
               mt.setLinkedstudies(ls);
               mt.setLocalmod(new Date());
               mt.setRemotemod(new Date(0));
               maindao.saveOrUpdate(mt);
            }
         }
      }
   }

   // gets local form mod times (all studies)
   public static Date getLastupdatelocal(com.owarchitects.forms.commons.db.MainDAO maindao) {

      List l=maindao.getTable("Dtmodtime");
      Iterator i=l.iterator();
      Date returnstamp=new Date(0);
      while (i.hasNext()) {
         Date thistime=((Dtmodtime)i.next()).getLocalmod();
         if (thistime.after(returnstamp)) returnstamp=thistime;
      }
      return returnstamp;

   }

   // gets remote form mod times (all studies)
   public static Date getLastupdateremote(com.owarchitects.forms.commons.db.MainDAO maindao) {

      List l=maindao.getTable("Dtmodtime");
      Iterator i=l.iterator();
      Date returnstamp=new Date(0);
      while (i.hasNext()) {
         Date thistime=((Dtmodtime)i.next()).getRemotemod();
         if (thistime.after(returnstamp)) returnstamp=thistime;
      }
      return returnstamp;

   }

   // sets local and remote form mod times (this study only)
   public static void setFormModTimes(com.owarchitects.forms.commons.comp.FORMSAuth auth,com.owarchitects.forms.commons.db.MainDAO maindao) throws FORMSException {

       // only linked studies affect form mod time
       long studyid=new Long(auth.getValue("studyid")).longValue();
       List studylist=maindao.execQuery(
          "select l from Linkedstudies l where l.studies.studyid=" + studyid
          );
       if (studylist.size()>0) {
          Iterator i=studylist.iterator();
          // update dtmodtime (creating record if necessary)
          while (i.hasNext()) {
             Linkedstudies ls=(Linkedstudies)i.next();
             Dtmodtime mt=(Dtmodtime)maindao.execUniqueQuery(
                "select f from Dtmodtime f where f.linkedstudies.lstdid=" + ls.getLstdid()
                );
             if (mt!=null) {
                // update current record
                mt.setLocalmod(new Date());
                mt.setRemotemod(new Date(0));
                maindao.saveOrUpdate(mt);
             } else {
                // create new record
                mt=new Dtmodtime();
                mt.setLinkedstudies(ls);
                mt.setLocalmod(new Date());
                mt.setRemotemod(new Date(0));
                maindao.saveOrUpdate(mt);
             }
          }
          
       }

   }

   // sets local form mod times (this study only)
   public static void setLocalFormModTime(Date date,com.owarchitects.forms.commons.comp.FORMSAuth auth,com.owarchitects.forms.commons.db.MainDAO maindao) throws FORMSException {

       // only linked studies affect form mod time
       long studyid=new Long(auth.getValue("studyid")).longValue();
       List studylist=maindao.execQuery(
          "select l from Linkedstudies l where l.studies.studyid=" + studyid
          );
       if (studylist.size()>0) {
          Iterator i=studylist.iterator();
          // update dtmodtime (creating record if necessary)
          while (i.hasNext()) {
             Linkedstudies ls=(Linkedstudies)i.next();
             Dtmodtime mt=(Dtmodtime)maindao.execUniqueQuery(
                "select f from Dtmodtime f where f.linkedstudies.lstdid=" + ls.getLstdid()
                );
             if (mt!=null) {
                // update current record
                mt.setLocalmod(date);
                maindao.saveOrUpdate(mt);
             } else {
                // create new record
                mt=new Dtmodtime();
                mt.setLinkedstudies(ls);
                mt.setLocalmod(date);
                maindao.saveOrUpdate(mt);
             }
          }
          
       }

   }

   // sets local form mod times (this study only)
   public static void setLocalFormModTime(com.owarchitects.forms.commons.comp.FORMSAuth auth,com.owarchitects.forms.commons.db.MainDAO maindao) throws FORMSException {
      setLocalFormModTime(new Date(),auth,maindao);
   }


   // sets remote form mod times (this study only)
   public static void setRemoteFormModTime(Date date,com.owarchitects.forms.commons.comp.FORMSAuth auth,com.owarchitects.forms.commons.db.MainDAO maindao) throws FORMSException {

       // only linked studies affect form mod time
       long studyid=new Long(auth.getValue("studyid")).longValue();
       List studylist=maindao.execQuery(
          "select l from Linkedstudies l where l.studies.studyid=" + studyid
          );
       if (studylist.size()>0) {
          Iterator i=studylist.iterator();
          // update dtmodtime (creating record if necessary)
          while (i.hasNext()) {
             Linkedstudies ls=(Linkedstudies)i.next();
             Dtmodtime mt=(Dtmodtime)maindao.execUniqueQuery(
                "select f from Dtmodtime f where f.linkedstudies.lstdid=" + ls.getLstdid()
                );
             if (mt!=null) {
                // update current record
                mt.setRemotemod(date);
                maindao.saveOrUpdate(mt);
             } else {
                // create new record
                mt=new Dtmodtime();
                mt.setLinkedstudies(ls);
                mt.setRemotemod(date);
                maindao.saveOrUpdate(mt);
             }
          }
       }

   }

   // sets remote form mod times (this study only)
   public static void setRemoteFormModTime(com.owarchitects.forms.commons.comp.FORMSAuth auth,com.owarchitects.forms.commons.db.MainDAO maindao) throws FORMSException {
      setRemoteFormModTime(new Date(0),auth,maindao);
   }

   // gets local and remote form mod times (this study only)
   public static HashMap getFormModTimes(com.owarchitects.forms.commons.comp.FORMSAuth auth,com.owarchitects.forms.commons.db.MainDAO maindao) {

      try {
          long studyid=new Long(auth.getValue("studyid")).longValue();
          List studylist=maindao.execQuery(
             "select l from Linkedstudies l where l.studies.studyid=" + studyid
             );
          if (studylist.size()>0) {
             Iterator i=studylist.iterator();
             // update dtmodtime (creating record if necessary)
             while (i.hasNext()) {
                Linkedstudies ls=(Linkedstudies)i.next();
                Dtmodtime mt=(Dtmodtime)maindao.execUniqueQuery(
                   "select f from Dtmodtime f where f.linkedstudies.lstdid=" + ls.getLstdid()
                   );
                if (mt!=null) {
                   HashMap map=new HashMap();
                   map.put("localmod",mt.getLocalmod());
                   map.put("remotemod",mt.getRemotemod());
                   return map;
                } 
             }
          }
          return null;
      } catch (Exception e) {
         return null;
      }
      
   }

   // Sets modification time for remote installation forms to specified tme
   public static Date getLocalFormModTime(com.owarchitects.forms.commons.comp.FORMSAuth auth,com.owarchitects.forms.commons.db.MainDAO maindao) {

      try {
          // only linked studies affect form mod time
          long studyid=new Long(auth.getValue("studyid")).longValue();
          List studylist=maindao.execQuery(
             "select l from Linkedstudies l where l.studies.studyid=" + studyid
             );
          if (studylist.size()>0) {
             Iterator i=studylist.iterator();
             // update dtmodtime (creating record if necessary)
             while (i.hasNext()) {
                Linkedstudies ls=(Linkedstudies)i.next();
                Dtmodtime mt=(Dtmodtime)maindao.execUniqueQuery(
                   "select f from Dtmodtime f where f.linkedstudies.lstdid=" + ls.getLstdid()
                   );
                if (mt!=null) {
                   return mt.getLocalmod();
                } 
             }
          }
          return null;
      } catch (Exception e) {
         return null;
      }
      
   }

   // gets remote form mod times (this study only)
   public static Date getRemoteFormModTime(com.owarchitects.forms.commons.comp.FORMSAuth auth,com.owarchitects.forms.commons.db.MainDAO maindao) {

      try {
          // only linked studies affect form mod time
          long studyid=new Long(auth.getValue("studyid")).longValue();
          List studylist=maindao.execQuery(
             "select l from Linkedstudies l where l.studies.studyid=" + studyid
             );
          if (studylist.size()>0) {
             Iterator i=studylist.iterator();
             // update dtmodtime (creating record if necessary)
             while (i.hasNext()) {
                Linkedstudies ls=(Linkedstudies)i.next();
                Dtmodtime mt=(Dtmodtime)maindao.execUniqueQuery(
                   "select f from Dtmodtime f where f.linkedstudies.lstdid=" + ls.getLstdid()
                   );
                if (mt!=null) {
                   return mt.getRemotemod();
                } 
             }
          }
          return null;
      } catch (Exception e) {
         return null;
      }
      
   }


}

        


