 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/**
 * MediapermUtilDwr.java - DWR program to edit media permissions information
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.io.*;
import java.util.*;
//import java.text.*;
//import org.apache.regexp.RE;
//import org.apache.commons.lang.StringUtils;
//import com.sas.sasserver.sclfuncs.*;
//import com.sas.sasserver.dataset.*;
//import com.sas.rmi.*;
//import com.sas.sasserver.submit.*;
//import org.apache.commons.lang.StringUtils;
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang.ArrayUtils.*;
//import java.util.regex.*;
import javax.crypto.*;
import java.security.*;
//import javax.crypto.spec.*;
//import java.security.spec.*;
//import uk.ltd.getahead.dwr.WebContext;
//import uk.ltd.getahead.dwr.WebContextFactory;
//import javax.servlet.*;
//import javax.servlet.http.*;
//import org.apache.velocity.app.*;
//import org.apache.velocity.*;
//import org.apache.commons.httpclient.util.URIUtil;


public class MediapermUtilDwr extends FORMSDwr {

   // Sets Permissions Profile used by Form.
   public String setMediaProfile(String mediainfoid,String mprofileid) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Verify authorization, then return public key as string
      FORMSAuth auth=this.getAuth();
      if (auth.isAuth(this.getRequest(),this.getResponse())) {

         try {

            // Pull current profile assignment, if any
            Mediapermprofassign asn=(Mediapermprofassign)getMainDAO().execUniqueQuery(
               "select a from Mediapermprofassign a where a.mediainfoid=" + mediainfoid
               );
            if (asn==null) {
               if (new Long(mprofileid).longValue()==0) {
                  // no current profile assignment, none requested
                  return "OK";
               }
               Mediapermprofiles mpp=(Mediapermprofiles)getMainDAO().execUniqueQuery(
                  "select f from Mediapermprofiles f where f.mprofileid=" + mprofileid
                  );
               if (mpp==null) {
                  return "ERROR";
               } 
               // create new profile assignment
               asn=new Mediapermprofassign();
               asn.setMediainfoid(new Long(mediainfoid).longValue());
               asn.setMediapermprofiles(mpp);
               getMainDAO().saveOrUpdate(asn);
               return "OK";
            } else {
               if (new Long(mprofileid).longValue()==0) {
                  // clear current profile assignment per request
                  getMainDAO().delete(asn);
                  return "OK";
               }   
               Mediapermprofiles mpp=(Mediapermprofiles)getMainDAO().execUniqueQuery(
                  "select f from Mediapermprofiles f where f.mprofileid=" + mprofileid
                  );
               if (mpp==null) {
                  return "ERROR";
               } 
               // update existing profile assignment per request
               asn.setMediapermprofiles(mpp);
               getMainDAO().saveOrUpdate(asn);
               return "OK";
            }

         } catch (Exception e) {

            return null;

         }
   
      } else {

         return null;

      }
      
   }

}


