 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * SyspermsController.java - User utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.security.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.*;
import javax.crypto.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;


public class SyspermsController extends FORMSController {

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // check permissions
      if (!hasGlobalPerm(PcatConstants.SYSTEM,"ROLEPERM")) {
         safeRedirect("permission.err");
         return null;
      }

      // Use class-name based view
      mav=new ModelAndView("Sysperms");
      String spath=request.getServletPath();

      // Pull role list and user list

      // Make sure role acronym is not duplicated witin level and site/study
      List l;
      long q_siteid;
      try {
         q_siteid=new Long(getAuth().getValue("siteid")).longValue();
      } catch (Exception sie) {
         q_siteid=new Long("-999999999").longValue();
      }
      long q_studyid;
      try {
         q_studyid=new Long(getAuth().getValue("studyid")).longValue();
      } catch (Exception sie) {
         q_studyid=new Long("-999999999").longValue();
      }
      l=getMainDAO().execQuery(
            "select rr from Resourceroles rr where " +
                   "(rr.rolelevel=" + VisibilityConstants.SYSTEM + ") or " +
                   "(rr.rolelevel=" + VisibilityConstants.SITE +
                       " and rr.siteid=:siteid) or " +
                   "(rr.rolelevel=" + VisibilityConstants.STUDY +
                       " and rr.studyid=:studyid) " +
               "order by rr.rolelevel,rr.roleacr "    
             ,new String[] { "siteid","studyid" }
             ,new Object[] { q_siteid,q_studyid }   
         );

      // Create HashMap list (with rolelevel as a string) to pass to iframe
      ArrayList rolelist=new ArrayList();
      Iterator i=l.iterator();
      while (i.hasNext()) {
         HashMap map=new HashMap();
         Resourceroles r=(Resourceroles)i.next();
         String rolelevel=new VisibilityConstants().getFieldName(r.getRolelevel());
         map.put("rolelevel",rolelevel);
         map.put("role",r);
         rolelist.add(map);
      }
      ListResult result;
      result=new ListResult();
      result.setList(rolelist);
      result.setStatus("OK");
      this.getSession().setAttribute("iframeresult",result);

      // PULL IN USER LIST
      AllusersDAO adao=(AllusersDAO)this.getContext().getBean("allusersDAO");      
      l=adao.getCurrentUsers();

      // Create typed list to pass to iframe
      ArrayList userlist=new ArrayList(Arrays.asList(l.toArray(new Allusers[0])));
      result=new ListResult();
      result.setList(userlist);
      result.setStatus("OK");
      this.getSession().setAttribute("iframe2result",result);

      mav.addObject("status","OK");
      mav.addObject("studydesc",getAuth().getValue("studydesc"));
      return mav;

   }

}

