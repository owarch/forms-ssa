 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * AnalysthaPermsController.java - User utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.security.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.*;
import javax.crypto.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;


public class AnalysthaPermsController extends FORMSController {

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Long analysthaid=new Long(getRequest().getParameter("id"));
      Analystha ha=(Analystha)getMainDAO().execUniqueQuery(
         "select ha from Analystha ha where ha.analysthaid=" + analysthaid
         );

      if (ha==null) {
         safeRedirect("permission.err");
         return null;
      }
      // Permissions cannot be assigned for root category
      if (ha.getPanalysthaid()==null) {
         mav.addObject("status","NOROOT");
         return mav;
      }
      // Check permissions
      if (!(ha.getCreateuser()==new Long(getAuth().getValue("auserid")).longValue() || getPermHelper().hasAnalystSysPerm("DOCPERMS"))) {
         safeRedirect("permission.err");
         return null;
      }


      Boolean remove=new Boolean(getRequest().getParameter("remove"));
      if (remove==null) remove=new Boolean(false);
      if (remove.booleanValue()) {
         removePerms(analysthaid);
         mav.addObject("status","REMOVED");
         return mav;
      }

      getAuth().setValue("analysthaid",analysthaid.toString());

      // Use class-name based view
      mav=new ModelAndView("AnalysthaPerms");
      String spath=request.getServletPath();

      // Pull role list and user list

      // Make sure role acronym is not duplicated witin level and site/study
      List l=getMainDAO().execQuery(
            "select rr from Analystgroups rr where rr.createuser=" + getAuth().getValue("auserid") + " " +
               "order by rr.groupacr "    
         );

      ListResult result;
      result=new ListResult();
      result.setList(l);
      result.setStatus("OK");
      this.getSession().setAttribute("iframeresult",result);

      // PULL IN USER LIST
      AllusersDAO adao=(AllusersDAO)this.getContext().getBean("allusersDAO");      
      l=adao.getCurrentUsers();

      // eliminate users with no access to CMS
      Iterator i=l.iterator();
      while (i.hasNext()) {
         Allusers u=(Allusers)i.next();
         // Remove self from list
         if (u.getAuserid()==(new Long(getAuth().getValue("auserid")).longValue())) {
            i.remove();
            continue;
         }
         // see if user has access to CMS.  If not, remove from list
         if (!getPermHelper().hasAnalystSysPerm(new Long(u.getAuserid()).toString(),"ACCESS")) {
            i.remove();
            continue;
         }
      }

      // Create typed list to pass to iframe
      ArrayList userlist=new ArrayList(Arrays.asList(l.toArray(new Allusers[0])));
      result=new ListResult();
      result.setList(userlist);
      result.setStatus("OK");
      this.getSession().setAttribute("iframe2result",result);

      mav.addObject("status","OK");
      mav.addObject("analystha",ha);
      return mav;

   }

   private void removePerms(Long analysthaid) {
    
      List l=getMainDAO().execQuery(
         "select p from Analysthagrouppermassign p where analysthaid=" + analysthaid
         );
      Iterator i=l.iterator();
      while (i.hasNext()) {
         Analysthagrouppermassign p=(Analysthagrouppermassign)i.next();
         getMainDAO().delete(p);
      }
    
      l=getMainDAO().execQuery(
         "select p from Analysthauserpermassign p where analysthaid=" + analysthaid
         );
      i=l.iterator();
      while (i.hasNext()) {
         Analysthauserpermassign p=(Analysthauserpermassign)i.next();
         getMainDAO().delete(p);
      }

   }

}

