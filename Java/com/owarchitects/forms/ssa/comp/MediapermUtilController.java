 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * MediapermUtilController.java - User utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.security.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.*;
import javax.crypto.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;


public class MediapermUtilController extends FORMSController {

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // check permissions
      //if (!getAuth().getValue("rightslevel").equalsIgnoreCase("ADMIN")) {
      //   safeRedirect("permission.err");
      //   return null;
      //}

      // Use class-name based view
      mav=new ModelAndView("MediapermUtil");
      String spath=request.getServletPath();

      // profile permissions
      if (spath.indexOf("profilepermissions.")>=0) {

         mav.addObject("status",profilePermissions());
         return mav;

      }
      else if (spath.indexOf("viewprofileperm.")>=0) {

         List outlist=viewProfilePerm();
         mav.addObject("list",outlist);
         mav.addObject("status","VIEWPROFILEPERM");
         return mav;

      }
      else if (spath.indexOf("profilepermassign.")>=0) {

         mav.addObject("status",profilePermAssign());
         return mav;

      }
      else if (spath.indexOf("filepermissions.")>=0) {

         mav.addObject("status",filePermissions());
         return mav;

      }
      else if (spath.indexOf("viewfileperm.")>=0) {

         mav.addObject("list",viewFilePerm());
         mav.addObject("status","VIEWFILEPERM");
         return mav;

      }
      else if (spath.indexOf("filepermassign.")>=0) {

         mav.addObject("status",filePermAssign());
         return mav;

      }

      // add new site interface
      else {
         out.println("BAD REQUEST! - " + spath);
         return null;
      }

   }

   // Manage profile permissions
   private String profilePermissions() {

      String status=null;
      long mprofileid=new Long(getRequest().getParameter("mprofileid")).longValue();

      Mediapermprofiles profile=(Mediapermprofiles)getMainDAO().execUniqueQuery(
            "select profile from Mediapermprofiles profile where profile.mprofileid=" + mprofileid
            );
      if (profile==null) {
         status="PROFILEPULLERROR";
         return status;
      }
      getMAV().addObject("profile",profile);

      // Pull role list and user list
      // Make sure role acronym is not duplicated witin level and site/study
      List l;
      long q_siteid;
      try {
         q_siteid=new Long(getAuth().getValue("siteid")).longValue();
      } catch (Exception sie) {
         q_siteid=new Long("-999999999").longValue();
      }
      long q_studyid;
      try {
         q_studyid=new Long(getAuth().getValue("studyid")).longValue();
      } catch (Exception sie) {
         q_studyid=new Long("-999999999").longValue();
      }
      l=getMainDAO().execQuery(
            "select rr from Resourceroles rr where " +
                   "(rr.rolelevel=:profilelevel) and (" + 
                      "(rr.rolelevel=" + VisibilityConstants.SYSTEM + ") or " +
                      "(rr.rolelevel=" + VisibilityConstants.SITE +
                          " and rr.siteid=:siteid) or " +
                      "(rr.rolelevel=" + VisibilityConstants.STUDY +
                          " and rr.studyid=:studyid) " +
                   ") " +
               "order by rr.rolelevel,rr.roleacr "    
             ,new String[] { "siteid","studyid","profilelevel" }
             ,new Object[] { q_siteid,q_studyid,profile.getProfilelevel() }   
         );

      // Create HashMap list (with rolelevel as a string) to pass to iframe
      ArrayList rolelist=new ArrayList();
      Iterator i=l.iterator();
      while (i.hasNext()) {
         HashMap map=new HashMap();
         Resourceroles r=(Resourceroles)i.next();
         String rolelevel=new VisibilityConstants().getFieldName(r.getRolelevel());
         map.put("rolelevel",rolelevel);
         map.put("role",r);
         rolelist.add(map);
      }
      ListResult result;
      result=new ListResult();
      result.setList(rolelist);
      result.setStatus("OK");
      this.getSession().setAttribute("iframeresult",result);

      // NO USER LIST FOR PROFILES

      status="PROFILEPERMISSIONS";
      return status;

   }

   // View Profile Permissions
   public List viewProfilePerm() {

      String mprofileid=getRequest().getParameter("mprofileid");

      // pull system/site/study permissions 
      List p=getMainDAO().execQuery(
         "select p from Permissions p join fetch p.permissioncats " +
            "where p.permissioncats.pcatvalue=" + PcatConstants.MEDIA + " and " +
                  "p.permissioncats.pcatscope=" + PcatscopeConstants.RESOURCE 
            );

      // Holders for permissions      
      BitSet permset=null;
      List a=null;

      ResourcerolesDAO rdao=(ResourcerolesDAO)this.getContext().getBean("resourcerolesDAO");
      Resourceroles role=rdao.getRecord(new Long(request.getParameter("rroleid")).longValue());

      mav.addObject("mprofileid",mprofileid);
      mav.addObject("acrvar",role.getRoleacr());
      mav.addObject("idvar",role.getRroleid());
      mav.addObject("typevar","role");

      Mediarolepermprofassign assign=(Mediarolepermprofassign)getMainDAO().execUniqueQuery(
         "select p from Mediarolepermprofassign p where p.resourceroles.rroleid=:rroleid and " +
            "p.mediapermprofiles.mprofileid=:mprofileid "
         ,new String[] { "rroleid","mprofileid" }
         ,new Object[] { new Long(request.getParameter("rroleid")),new Long(mprofileid) }
         );   
      if (assign!=null) {
         permset=(BitSet)assign.getPermlevel();
      }

      // Use values from permission holders to set "checked" values in input fields
      Iterator ip=p.iterator();
      ArrayList newlist=new ArrayList();
      while (ip.hasNext()) {
         HashMap map=new HashMap();
         com.owarchitects.forms.commons.db.Permissions perm=(com.owarchitects.forms.commons.db.Permissions)ip.next();
         map.put("permobj",perm);
         int pcatvalue=perm.getPermissioncats().getPcatvalue();
         map.put("pcatvalue",new PcatConstants().getFieldName(pcatvalue));
         if (permset!=null && permset.get(perm.getPermpos()-1)) {
            map.put("permchecked","checked");
         } else {
            map.put("permchecked"," ");
         }   
         newlist.add(map);

      }
      return newlist;

   }


   // Assign Permissions
   public String profilePermAssign() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String mprofileid=request.getParameter("mprofileid");
      String typevar=request.getParameter("typevar");
      String idvar=request.getParameter("idvar");
      
      // make sure user has permission to change permission
      Integer profilelevel=(Integer)getMainDAO().execUniqueQuery(
            "select m.profilelevel from Mediapermprofiles m where m.mprofileid=" + mprofileid
         );
      if (profilelevel!=null && getPermHelper().hasGlobalPerm(getAuth().getValue("auserid"),profilelevel,"MEDPERMPROF")) {
         // do nothing, continue
      } else {
         // not permitted
         return "NOAUTH";
      }

      // return position parameters of set permissions for setting BitSet values
      Enumeration e=request.getParameterNames();
      ArrayList alist=new ArrayList();
      while (e.hasMoreElements()) {
         String pname=(String)e.nextElement();
         if (pname.substring(0,2).equalsIgnoreCase("r_")) {
            String pvalue=request.getParameter(pname);
            if (pvalue.equals("1")) {
               com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
                  "select p from Permissions p join fetch p.permissioncats " +
                     "where p.permid=:permid "
                  ,new String[] { "permid" }
                  ,new Object[] { new Long(pname.substring(2)) }
                  );
               alist.add(p);
            }
         }
      }

      ResourcerolesDAO rdao=(ResourcerolesDAO)getContext().getBean("resourcerolesDAO");
      Resourceroles role=rdao.getRecord(new Long(idvar).longValue());

      List l=getMainDAO().execQuery(
         "select p.permissioncats.pcatvalue,max(p.permpos) " +
            "from Permissions p " +
            "group by p.permissioncats.pcatvalue " 
            );
      // This having clause was throwing errors.  I think it's valid, so this
      // might be a bug.  We'll handle the subset later.
      //  + "having p.permissioncats.pcatvalue in ....."

      // Initialize BitSets
      BitSet permset=null;
      Iterator i=l.iterator();
      while (i.hasNext()) {
         Object[] oarray=(Object[])i.next();
         int setln=((Integer)oarray[1]).intValue();
         permset=new BitSet(setln);
      }
      i=alist.iterator();
      while (i.hasNext()) {
         com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)i.next();
         // set flagged permissions in bitset
         permset.set(p.getPermpos()-1,true);
      }
      if (permset!=null) {
         Mediarolepermprofassign frppa=(Mediarolepermprofassign)getMainDAO().execUniqueQuery(
            "select srp from Mediarolepermprofassign srp where srp.resourceroles.rroleid=:rroleid and " +
               "srp.mediapermprofiles.mprofileid=:mprofileid"
            ,new String[] { "rroleid","mprofileid" }
            ,new Object[] { new Long(role.getRroleid()),new Long(mprofileid) }
            );   
         if (frppa==null) {
            Mediapermprofiles profile=(Mediapermprofiles)getMainDAO().execUniqueQuery(
               "select p from Mediapermprofiles p where p.mprofileid=" + mprofileid
               );
            if (profile==null) return "PROFILEPULLERROR";
            frppa=new Mediarolepermprofassign();
            frppa.setResourceroles(role);
            frppa.setMediapermprofiles(profile);
         }
         frppa.setPermlevel(permset);
         getMainDAO().saveOrUpdate(frppa);
         return "PROFILEPERMASSIGNED";
      }
      return "PERMASSIGNERROR";

   }

   // Manage file permissions
   private String filePermissions() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String status=null;

      if (!new Boolean(getRequest().getParameter("islocal")).booleanValue()) {
         status="REMOTEFILE";
         return status;
      }

      long mediainfoid=new Long(getRequest().getParameter("mediainfoid")).longValue();

      Mediainfo minfo=(Mediainfo)getMainDAO().execUniqueQuery(
            "select ft from Mediainfo ft where ft.mediainfoid=" + mediainfoid
            );
      if (minfo==null) {
         status="FILEPULLERROR";
         return status;
      }
      getMAV().addObject("minfo",minfo);

      Mediaha mha=(Mediaha)getMainDAO().execUniqueQuery(
         "select mha from Mediaha mha where mha.mediahaid=" + minfo.getPmediahaid()
         );
      if (mha==null) {
         status="FILEPULLERROR";
         return status;
      }

      // check permissions for move
      if (!minfo.getUploaduser().equals(new Long(getAuth().getValue("auserid")))) {
         if (!getPermHelper().hasGlobalPerm(mha.getVisibility(),"MEDPERMOTH")) {
            return "NOAUTH";
         }
      }

      getMAV().addObject("mha",mha);

      List l;
      long q_siteid;
      try {
         q_siteid=new Long(getAuth().getValue("siteid")).longValue();
      } catch (Exception sie) {
         q_siteid=new Long("-999999999").longValue();
      }
      long q_studyid;
      try {
         q_studyid=new Long(getAuth().getValue("studyid")).longValue();
      } catch (Exception sie) {
         q_studyid=new Long("-999999999").longValue();
      }

      // Get list of relevant profiles
      l=getMainDAO().execQuery(
            "select p from Mediapermprofiles p where " +
                   "(p.profilelevel=:visibility) and (" + 
                      "(p.profilelevel=" + VisibilityConstants.SYSTEM + ") or " +
                      "(p.profilelevel=" + VisibilityConstants.SITE +
                          " and p.siteid=:siteid) or " +
                      "(p.profilelevel=" + VisibilityConstants.STUDY +
                          " and p.studyid=:studyid) " +
                   ") " +
               "order by p.profilelevel,p.profileacr "    
             ,new String[] { "siteid","studyid","visibility" }
             ,new Object[] { q_siteid,q_studyid,mha.getVisibility() }   
         );
      getMAV().addObject("profilelist",l);
      // create null profile, add to list
      Mediapermprofiles nullprofile=new Mediapermprofiles();
      nullprofile.setProfileacr("_NONE_");
      nullprofile.setProfiledesc("No profile assigned");
      l.add(0,nullprofile);


      // Get File Permissions Profile Value
      Mediapermprofiles currprofile=(Mediapermprofiles)getMainDAO().execUniqueQuery(
         "select f.mediapermprofiles from Mediapermprofassign f where f.mediainfoid=" + minfo.getMediainfoid()
         );
      if (currprofile==null) currprofile=nullprofile;
      getMAV().addObject("currprofile",currprofile);

      HashMap pmap=new HashMap();
      pmap.put("currprofile",currprofile);
      pmap.put("profilelist",l);
      pmap.put("mediainfoid",minfo.getMediainfoid());
      this.getSession().setAttribute("iframe3result",pmap);

      // Pull role list and user list
      // Make sure role acronym is not duplicated witin level and site/study
      l=getMainDAO().execQuery(
            "select rr from Resourceroles rr where " +
                   "(rr.rolelevel=:visibility) and (" + 
                      "(rr.rolelevel=" + VisibilityConstants.SYSTEM + ") or " +
                      "(rr.rolelevel=" + VisibilityConstants.SITE +
                          " and rr.siteid=:siteid) or " +
                      "(rr.rolelevel=" + VisibilityConstants.STUDY +
                          " and rr.studyid=:studyid) " +
                   ") " +
               "order by rr.rolelevel,rr.roleacr "    
             ,new String[] { "siteid","studyid","visibility" }
             ,new Object[] { q_siteid,q_studyid,mha.getVisibility() }   
         );

      // Create HashMap list (with rolelevel as a string) to pass to iframe
      ArrayList rolelist=new ArrayList();
      Iterator i=l.iterator();
      while (i.hasNext()) {
         HashMap map=new HashMap();
         Resourceroles r=(Resourceroles)i.next();
         String rolelevel=new VisibilityConstants().getFieldName(r.getRolelevel());
         map.put("rolelevel",rolelevel);
         map.put("role",r);
         rolelist.add(map);
      }
      ListResult result;
      result=new ListResult();
      result.setList(rolelist);
      result.setStatus("OK");
      this.getSession().setAttribute("iframeresult",result);

      // PULL IN USER LIST
      AllusersDAO adao=(AllusersDAO)this.getContext().getBean("allusersDAO");      
      l=adao.getCurrentUsers();

      // Create typed list to pass to iframe
      ArrayList userlist=new ArrayList(Arrays.asList(l.toArray(new Allusers[0])));
      result=new ListResult();
      result.setList(userlist);
      result.setStatus("OK");
      this.getSession().setAttribute("iframe2result",result);

      status="FILEPERMISSIONS";
      return status;

   }

   // View Profile Permissions
   public List viewFilePerm() throws FORMSException, FORMSKeyException, FORMSSecurityException {

      String mediainfoid=getRequest().getParameter("mediainfoid");

      Mediainfo minfo=(Mediainfo)getMainDAO().execUniqueQuery(
            "select ft from Mediainfo ft where ft.mediainfoid=" + mediainfoid
            );
      if (minfo==null) {
         return null;
      }
      getMAV().addObject("mediainfoid",minfo.getMediainfoid());

      // pull system/site/study permissions 
      List p=getMainDAO().execQuery(
         "select p from Permissions p join fetch p.permissioncats " +
            "where p.permissioncats.pcatvalue=" + PcatConstants.MEDIA + " and " +
                  "p.permissioncats.pcatscope=" + PcatscopeConstants.RESOURCE 
            );
            
      // Holders for permissions      
      BitSet permset=null;
      List a=null;

      boolean haveperm=false;
      
      if (request.getParameter("rroleid")!=null) {

         ResourcerolesDAO rdao=(ResourcerolesDAO)this.getContext().getBean("resourcerolesDAO");
         Resourceroles role=rdao.getRecord(new Long(request.getParameter("rroleid")).longValue());
   
         mav.addObject("acrvar",role.getRoleacr());
         mav.addObject("idvar",role.getRroleid());
         mav.addObject("typevar","role");
   
         Mediarolepermassign assign=(Mediarolepermassign)getMainDAO().execUniqueQuery(
            "select p from Mediarolepermassign p where p.resourceroles.rroleid=:rroleid and " +
               "p.mediainfoid=:mediainfoid "
            ,new String[] { "rroleid","mediainfoid" }
            ,new Object[] { new Long(request.getParameter("rroleid")),new Long(minfo.getMediainfoid()) }
            );   
         if (assign!=null) {
            permset=(BitSet)assign.getPermlevel();
            haveperm=true;
         }

      } else {

         String auserid=getRequest().getParameter("auserid");

         AllusersDAO rdao=(AllusersDAO)this.getContext().getBean("allusersDAO");
         Allusers user=rdao.getRecord(new Long(auserid).longValue());
   
         mav.addObject("acrvar",user.getUserdesc());
         mav.addObject("idvar",user.getAuserid());
         mav.addObject("typevar","user");
   
         Mediauserpermassign assign=(Mediauserpermassign)getMainDAO().execUniqueQuery(
            "select p from Mediauserpermassign p where p.allusers.auserid=:auserid and " +
               "p.mediainfoid=:mediainfoid "
            ,new String[] { "auserid","mediainfoid" }
            ,new Object[] { new Long(auserid),new Long(minfo.getMediainfoid()) }
            );   
         if (assign!=null) {
            permset=(BitSet)assign.getPermlevel();
            haveperm=true;
         }


      }
      getMAV().addObject("haveperm",new Boolean(haveperm));

      // Use values from permission holders to set "checked" values in input fields
      Iterator ip=p.iterator();
      ArrayList newlist=new ArrayList();
      while (ip.hasNext()) {
         HashMap map=new HashMap();
         com.owarchitects.forms.commons.db.Permissions perm=(com.owarchitects.forms.commons.db.Permissions)ip.next();
         map.put("permobj",perm);
         int pcatvalue=perm.getPermissioncats().getPcatvalue();
         map.put("pcatvalue",new PcatConstants().getFieldName(pcatvalue));
         if (permset!=null && permset.get(perm.getPermpos()-1)) {
            map.put("permchecked","checked");
         } else {
            map.put("permchecked"," ");
         }   
         newlist.add(map);

      }
      return newlist;

   }


   // Assign Permissions
   public String filePermAssign() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String mediainfoid=request.getParameter("mediainfoid");

      // CHECK PERMISSIONS
      Mediainfo minfo=(Mediainfo)getMainDAO().execUniqueQuery(
            "select ft from Mediainfo ft where ft.mediainfoid=" + mediainfoid
            );
      Mediaha mha=(Mediaha)getMainDAO().execUniqueQuery(
         "select mha from Mediaha mha where mha.mediahaid=" + minfo.getPmediahaid()
         );
      if (!minfo.getUploaduser().equals(new Long(getAuth().getValue("auserid")))) {
         if (!getPermHelper().hasGlobalPerm(mha.getVisibility(),"MEDPERMOTH")) {
            return "NOAUTH";
         }
      }

      String typevar=request.getParameter("typevar");
      String idvar=request.getParameter("idvar");
      String clearall=request.getParameter("clearall");
      if (clearall==null) clearall="";
      // return position parameters of set permissions for setting BitSet values
      Enumeration e=request.getParameterNames();
      ArrayList alist=new ArrayList();
      while (e.hasMoreElements()) {
         String pname=(String)e.nextElement();
         if (pname.substring(0,2).equalsIgnoreCase("r_")) {
            String pvalue=request.getParameter(pname);
            if (pvalue.equals("1")) {
               com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
                  "select p from Permissions p join fetch p.permissioncats " +
                     "where p.permid=:permid "
                  ,new String[] { "permid" }
                  ,new Object[] { new Long(pname.substring(2)) }
                  );
               alist.add(p);
            }
         }
      }

      List l=getMainDAO().execQuery(
         "select p.permissioncats.pcatvalue,max(p.permpos) " +
            "from Permissions p " +
            "group by p.permissioncats.pcatvalue " 
            );
      // This having clause was throwing errors.  I think it's valid, so this
      // might be a bug.  We'll handle the subset later.
      //  + "having p.permissioncats.pcatvalue in ....."
      
   
      // Initialize BitSets
      BitSet permset=null;
      Iterator i=l.iterator();
      while (i.hasNext()) {
         Object[] oarray=(Object[])i.next();
         int setln=((Integer)oarray[1]).intValue();
         permset=new BitSet(setln);
      }
      i=alist.iterator();
      while (i.hasNext()) {
         com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)i.next();
         // set flagged permissions in bitset
         permset.set(p.getPermpos()-1,true);
      }
      if (permset!=null) {

         if (typevar.equalsIgnoreCase("role")) {

            ResourcerolesDAO rdao=(ResourcerolesDAO)getContext().getBean("resourcerolesDAO");
            Resourceroles role=rdao.getRecord(new Long(idvar).longValue());
            Mediarolepermassign frpa=(Mediarolepermassign)getMainDAO().execUniqueQuery(
               "select srp from Mediarolepermassign srp where srp.resourceroles.rroleid=:rroleid and " +
                  "srp.mediainfoid=:mediainfoid"
               ,new String[] { "rroleid","mediainfoid" }
               ,new Object[] { new Long(role.getRroleid()),new Long(mediainfoid) }
               );   
            if (frpa==null) {
               if (clearall.equals("1")) {
                  return "NOMESSAGE";
               }
               frpa=new Mediarolepermassign();
               frpa.setResourceroles(role);
               frpa.setMediainfoid(new Long(mediainfoid).longValue());
            } else if (clearall.equals("1")) {
               getMainDAO().delete(frpa);
               return "FILEPERMCLEARED";
            }
            frpa.setPermlevel(permset);
            getMainDAO().saveOrUpdate(frpa);
            return "FILEPERMASSIGNED";
         }
         else if (typevar.equalsIgnoreCase("user")) {

            AllusersDAO adao=(AllusersDAO)getContext().getBean("allusersDAO");
            Allusers user=adao.getRecord(new Long(idvar).longValue());
            Mediauserpermassign frpa=(Mediauserpermassign)getMainDAO().execUniqueQuery(
               "select srp from Mediauserpermassign srp where srp.allusers.auserid=:auserid and " +
                  "srp.mediainfoid=:mediainfoid"
               ,new String[] { "auserid","mediainfoid" }
               ,new Object[] { new Long(user.getAuserid()),new Long(mediainfoid) }
               );   
            if (frpa==null) {
               if (clearall.equals("1")) {
                  return "NOMESSAGE";
               }
               frpa=new Mediauserpermassign();
               frpa.setAllusers(user);
               frpa.setMediainfoid(new Long(mediainfoid).longValue());
            } else if (clearall.equals("1")) {
               getMainDAO().delete(frpa);
               return "FILEPERMCLEARED";
            }
            frpa.setPermlevel(permset);
            getMainDAO().saveOrUpdate(frpa);
            return "FILEPERMASSIGNED";

         }
      }
      return "PERMASSIGNERROR";

   }


}


