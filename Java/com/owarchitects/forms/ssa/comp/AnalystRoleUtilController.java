 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * AnalystRoleUtilController.java - User utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.security.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.*;
import javax.crypto.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;


public class AnalystRoleUtilController extends FORMSController {

   boolean isbadrequest;

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // check permissions
      if (!getPermHelper().hasAnalystSysPerm("ROLES")) {
         safeRedirect("permission.err");
         return null;
      }

      // Use class-name based view
      mav=new ModelAndView("AnalystRoleUtil");
      isbadrequest=true;
      String spath=request.getServletPath();

      try {

         // Add new role interface 
         if (spath.indexOf("newrole.")>=0) {

            isbadrequest=false;
            mav.addObject("status","NEWROLE");
            return mav;

         // Submit new role to system
         } else if (spath.indexOf("newrolesubmit.")>=0) {

            String status=newRoleSubmit();
            mav.addObject("status",status);
            return mav;

         // Modify role interface
         } else if (spath.indexOf("modifyrole.")>=0) {

            modifyRole();
            isbadrequest=false;
            mav.addObject("status","MODIFYROLE");
            return mav;

         // Submit modified role
         } else if (spath.indexOf("modifyrolesubmit.")>=0) {

            String status=modifyRoleSubmit();
            mav.addObject("status",status);
            return mav;

         // Delete specified role 
         } else if (spath.indexOf("removerolesubmit.")>=0) {

            String status=removeRoleSubmit();
            mav.addObject("status",status);
            return mav;

         // Role membership selection screen
         } else if (spath.indexOf("rolemembership.")>=0) {

            roleMembershipSelection();
            isbadrequest=false;
            mav.addObject("status","ROLEMEMBERSHIP");
            return mav;

         } 

         out.println(spath);
         isbadrequest=false;
         return null;

      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Submit new role
   private String newRoleSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

            String status=null;
            String roleacr=getRequest().getParameter("roleacr").toUpperCase();
            String roledesc=getRequest().getParameter("roledesc");
            FORMSAuth auth=getAuth();
            Long auserid=new Long(auth.getValue("auserid"));
            
            // Make sure role acronym is not duplicated within user
            List l;
            l=getMainDAO().execQuery(
                  "select rr from Analystroles rr where rr.roleacr='" + roleacr + "'"
               );
            if (l.size()>0) {
               status="DUPLICATEROLE";
               return status;
            }
            // If not duplicate, add to database
            Analystroles arole=new Analystroles();
            arole.setRoleacr(roleacr);
            arole.setRoledesc(roledesc);
            getMainDAO().saveOrUpdate(arole);
            isbadrequest=false;
            getMAV().addObject("roleacr",roleacr);
            status="ROLEADDED";
            return status;

   }

   // Modify role interface
   private String modifyRole() throws FORMSException,FORMSKeyException,FORMSSecurityException {

            String status=null;
            long aroleid=new Long(getRequest().getParameter("aroleid")).longValue();
            FORMSAuth auth=getAuth();
            Long auserid=new Long(auth.getValue("auserid"));
            
            // pull role
            Analystroles rr=(Analystroles)getMainDAO().execUniqueQuery(
                  "select rr from Analystroles rr where " + 
                      "rr.aroleid=:aroleid" 
                   ,new String[] { "aroleid" }
                   ,new Object[] { aroleid }   
               );
            if (rr==null) {
               status="ROLEPULLERROR";
               return status;
            }
            getMAV().addObject("aroleid",aroleid);
            getMAV().addObject("roleacr",rr.getRoleacr());
            getMAV().addObject("roledesc",rr.getRoledesc());
            status="MODIFYROLE";
            return status;

   }

   // Submit modified role
   private String modifyRoleSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

            String status=null;
            String roleacr=getRequest().getParameter("roleacr").toUpperCase();
            String roledesc=getRequest().getParameter("roledesc");
            long aroleid=new Long(getRequest().getParameter("aroleid")).longValue();
            FORMSAuth auth=getAuth();
            Long auserid=new Long(auth.getValue("auserid"));
            
            // Make sure role acronym is not duplicated witin user
            List l=getMainDAO().execQuery(
                  "select rr from Analystroles rr where " +
                      "rr.aroleid!=:aroleid and rr.roleacr=:roleacr" 
                   ,new String[] { "aroleid","roleacr" }
                   ,new Object[] { aroleid,roleacr }   
               );
            if (l.size()>0) {
               status="DUPLICATEROLE";
               return status;
            }
            // If not duplicate, add to database
            Analystroles arole=(Analystroles)getMainDAO().execUniqueQuery(
               "select a from Analystroles a where a.aroleid=" + aroleid);
            arole.setRoleacr(roleacr);
            arole.setRoledesc(roledesc);
            getMainDAO().saveOrUpdate(arole);
            isbadrequest=false;
            status="ROLEMODIFIED";
            return status;

   }

   // Delete role
   private String removeRoleSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

            String status=null;
            long aroleid=new Long(getRequest().getParameter("aroleid")).longValue();
            FORMSAuth auth=getAuth();
            Long auserid=new Long(auth.getValue("auserid"));
            
            // Make sure role acronym is not duplicated witin level and site/study
            Analystroles rr=(Analystroles)getMainDAO().execUniqueQuery(
                  "select rr from Analystroles rr where " +
                      "rr.aroleid=:aroleid " 
                   ,new String[] { "aroleid" }
                   ,new Object[] { aroleid }   
               );
            if (rr==null) {
               status="ROLEPULLERROR";
               return status;
            }
            if (rr.getRoleacr().equalsIgnoreCase("ALLUSERS")) {
               status="NOMODIFYALLUSERS";
               return status;
            }
            isbadrequest=false;
            getMainDAO().delete(rr);
            status="ROLEDELETED";
            return status;

   }

   // Role membership selection screen
   private void roleMembershipSelection() throws FORMSException,FORMSKeyException,FORMSSecurityException {

            long aroleid=new Long(getRequest().getParameter("aroleid")).longValue();

            Analystroles thisrole=(Analystroles)getMainDAO().execUniqueQuery(
               "select a from Analystroles a where a.aroleid=" + aroleid
               );

            // PULL IN USER LIST
            List ulist=getMainDAO().execQuery("select distinct a from Allusers a " + 
                                            " where not a.isdeleted=true " + 
                                            " order by a.userdesc");

            // see if users are already assigned to this role
            Iterator i=ulist.iterator();
            ArrayList newlist=new ArrayList();
            while (i.hasNext()) {
               Allusers u=(Allusers)i.next();
               Analystroleuserassign assn=(Analystroleuserassign)getMainDAO().execUniqueQuery(
                  "select a from Analystroleuserassign a where a.role.aroleid=" + aroleid +
                     " and a.user.auserid=" + u.getAuserid()
                  );
               boolean isassigned=false;
               if (assn!=null) isassigned=true;
               HashMap map=new HashMap();
               map.put("allusers",u);
               map.put("isassigned",isassigned);
               map.put("aroleid",aroleid);
               newlist.add(map);
            }
            ListResult result=new ListResult();
            result.setList(newlist);
            this.getSession().setAttribute("iframeresult",result);
            getMAV().addObject("roleacr",thisrole.getRoleacr());
            getMAV().addObject("roledesc",thisrole.getRoledesc());

   }


}


