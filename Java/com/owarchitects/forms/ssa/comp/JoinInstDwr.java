 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.io.*;
import java.util.*;
//import java.text.*;
//import org.apache.regexp.RE;
//import org.apache.commons.lang.StringUtils;
//import com.sas.sasserver.sclfuncs.*;
//import com.sas.sasserver.dataset.*;
//import com.sas.rmi.*;
//import com.sas.sasserver.submit.*;
//import org.apache.commons.lang.StringUtils;
//import org.apache.commons.io.FileUtils;
//import org.apache.commons.lang.ArrayUtils.*;
//import java.util.regex.*;
import javax.crypto.*;
import java.security.*;
//import javax.crypto.spec.*;
//import java.security.spec.*;
//import uk.ltd.getahead.dwr.WebContext;
//import uk.ltd.getahead.dwr.WebContextFactory;
//import javax.servlet.*;
//import javax.servlet.http.*;
//import org.apache.velocity.app.*;
//import org.apache.velocity.*;
//import org.apache.commons.httpclient.util.URIUtil;


public class JoinInstDwr extends FORMSDwr {

   // exports local public key for installation as a String

   public String exportPublicKey(String acronym) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Verify authorization, then return public key as string
      FORMSAuth auth=this.getAuth();
      if (auth.isAuth(this.getRequest(),this.getResponse())) {

         return EncryptUtil.exportPublicKey(this.getSession(),"inst_loc_" + acronym + "_public");
       
      } else {

         return null;

      }
      
   }

   // Exports local connectstring to an XML string encrypted by inst_loc_acronym_public

   public String exportConnectstring(String acronym) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Verify authorization, then return public key as string
      FORMSAuth auth=this.getAuth();
      if (auth.isAuth(this.getRequest(),this.getResponse())) {

         try {

            AccessDAO accessDAO=(AccessDAO)this.getContext().getBean("accessDAO");
            Access euser=accessDAO.getRecord();
   
            // get formsinternal secret key for retrieving passwords from database
            SecretKey sk=EncryptUtil.getSecretKey(session,"formsinternal",SessionObjectUtil.getConfigObj(session).getKeyStorePassword());
   
            // get local installations public key for this site
            PublicKey pk=EncryptUtil.getPublicKey(this.getSession(),"inst_loc_" + acronym);
   
            // populate connectstring object with passwords from database
            Connectstring cs=new Connectstring();
            cs.setFkpassword(EncryptUtil.decryptString(euser.getFkpassword(),sk));
            cs.setDbpassword(EncryptUtil.decryptString(euser.getDbpassword(),sk));

            String rtnobj=EncryptUtil.encryptLongString(ObjectXmlUtil.objectToXmlString(cs),pk);
            PrivateKey rk=EncryptUtil.getPrivateKey(this.getSession(),"inst_loc_" + acronym,"private");
            String outobj=EncryptUtil.decryptLongString(rtnobj,rk);

            // return encrypted connectstring object
            return rtnobj; //EncryptUtil.encryptLongString(ObjectXmlUtil.objectToXmlString(cs),pk);

         } catch (Exception e) {

            return null;

         }
   
      } else {

         return null;

      }
      
   }

}


