 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * LoginController.java -  FORMS Login point
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.util.regex.*;
import java.io.*;
import java.security.*;
import javax.crypto.SecretKey;
import javax.servlet.http.*;
import javax.servlet.http.HttpSession;
import java.sql.Timestamp;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import org.springframework.web.servlet.*;
import org.springframework.web.context.support.*;
import org.apache.axis2.context.*;
import org.apache.axis2.client.*;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.io.FileUtils;
import org.springframework.orm.hibernate3.HibernateJdbcException;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;


public class LoginController extends FORMSSsaServiceClientController {

   // Don't perform initial auth check;
   { 
      autoAuthCheckOff(); 
   }

   public ModelAndView submitRequest() throws FORMSException {

      // Get context
      HttpSession session=request.getSession();
      WinProps wp=SessionObjectUtil.getWinPropsObj(request,response);
      wp.setWinNameMissOk("N");

      try {
    
        // PrintWriter for diagnostic output

        PrintWriter out=response.getWriter();        

        if (request.getParameter("directive")==null && session.getAttribute("encryptKey")==null) {


           // If using embedded Derby database (Self-contained FORMS), verify database existance.  Run setup if necessary

           try {
           
              ComboPooledDataSource csrc=(ComboPooledDataSource)this.getContext().getBean("embeddedDataSource");

              if (csrc!=null) {

                String pvalue=csrc.getDriverClass();

                if (pvalue.indexOf("derby.jdbc.EmbeddedDriver")>0) {

                   try {
        
                       // Verify database existance 
                       EmbeddedusersDAO eusersDAO=(EmbeddedusersDAO)this.getContext().getBean("embeddedusersDAO");
                       Embeddedusers e=eusersDAO.getRecord("XXXX");
        
                    } catch (HibernateJdbcException hje) {

                       callSetup(hje,csrc.getJdbcUrl());
                       return null;
         
                    } catch (org.springframework.transaction.CannotCreateTransactionException ccte) {

                       callSetup(ccte,csrc.getJdbcUrl());
                       return null;
         
                    }
        
                }

              }

           } catch (FORMSException fex) {

               mav.addObject("status","CANTCONNECT");
               return mav;

           } catch (Exception ex) {

              // do nonthing, continue
        
           } 

           // Check machine lockout status
           Authenticator cauth=new Authenticator(request,response);

           int cstat=cauth.checkHost();
           mav.addObject("status",new AuthenticatorConstants().getFieldName(cstat));
           //mav.addObject("status","HOST_ALLOWED");
           String user=(String)session.getAttribute("user");
           if (user==null) user="";
           mav.addObject("user",user);

           return mav;

        // Verify Login Attempt
        } else if (request.getParameter("directive").equalsIgnoreCase("LOGIN")) {

           // Receive request parameters

           String user=request.getParameter("user");
           String passwd=request.getParameter("passwd");

           // Set session attribute to be retrieved later
           session.setAttribute("user",user);

           Authenticator cauth=new Authenticator(request,response);

           LoginResult lr=cauth.login(user,passwd);

           if (lr.getStatus()==AuthenticatorConstants.LOGGED_IN) {      

              FORMSAuth auth=getAuth();
           
              // Set apptype
              auth.setValue("apptype","SSA");

              // Initialize permissions lists
              getPermHelper().initPermLists();

              // Update data from linked remote installations if any exist
              List lil=getMainDAO().execQuery("select l from Linkedinst l where isdisabled!=true");

              if (lil.size()>0) {

                 // See if any installation needs updated
                 Iterator lii=lil.iterator();
                 boolean needuserupdate=false;
                 boolean needformupdate=false;
                 while (lii.hasNext()) {
                    Linkedinst inst=(Linkedinst)lii.next();
                    SsaServiceSystemParms parms=new SsaServiceSystemParms();
                    parms.setBeanname("instInfoModTimeServiceTarget");
                    parms.setMethodname("getModTimes");

                    SsaServiceObjectResult result=(SsaServiceObjectResult)this.submitServiceRequest(inst,parms);

                    if (result!=null && result.getStatus().intValue()==FORMSServiceConstants.OK) {

                       HashMap rmap=(HashMap)result.getObject();
                       Timestamp rumod=(Timestamp)rmap.get("usermodtime");
                       Timestamp rfmod=(Timestamp)rmap.get("dtmodtime");
                       // compare values against those stored here
                       Timestamp lumod=(Timestamp)getMainDAO().execUniqueQuery(
                          "select a.remotemod from Userinfomodtime a " +
                             "where a.linkedinst.linstid=" + inst.getLinstid()
                             );
                       if (lumod==null) lumod=new Timestamp(0);      
                       Timestamp lfmod=(Timestamp)getMainDAO().execUniqueQuery(
                          "select max(f.remotemod) from Dtmodtime f " +
                             "where f.linkedstudies.linkedinst.linstid=" + inst.getLinstid()
                             );
                       if (lfmod==null) lfmod=new Timestamp(0);      

                       if (rumod.after(lumod)) {
                           needuserupdate=true;
                       }
                       if (rfmod.after(lfmod)) {
                           needformupdate=true;
                       }

                    }
                 }

                 if (needuserupdate || needformupdate) {
                    // Update remote installation data
                    InstInfoParms parms=new InstInfoParms();
                    parms.setBeanname("ssaUpdateInstInfoSoapTarget");
                    parms.setMethodname("updateRemoteInstInfo");
                    parms.setNeeduserupdate(needuserupdate);
                    parms.setNeedformupdate(needformupdate);
                    // submit asynchronously so this step doesn't hold up login
                    this.submitLocalNonBlockingSoapRequest(parms,new FORMSBasicCallback());
                    // Use blocking submit only for testing 
                    //FORMSServiceResult result=this.submitLocalSoapRequest(parms);
                 }

              }

              // Clear out certain stored objects
              auth.setSessionObjectValue("remoteformsessionlist",null);
              auth.setSessionObjectValue("ftypespermlist",null);
              auth.setSessionObjectValue("rrolelist",null);
              auth.setSessionObjectValue("rroleidlist",null);
              auth.setSessionObjectValue("fpermlist",null);
              auth.setSessionObjectValue("mpermlist",null);
              auth.setSessionObjectValue("ftypespermlist",null);
              auth.setSessionObjectValue("gpermlist",null);
              auth.setSessionAttribute("formhalist",null);
              auth.setSessionAttribute("datatablehalist",null);
              auth.setSessionAttribute("dthalist",null);
              auth.setSessionAttribute("datatablelist",null);
              auth.setSessionAttribute("formtypeslist",null);
              auth.setSessionAttribute("mediahalist",null);
              auth.setSessionAttribute("mediainfolist",null);
              auth.setSessionAttribute("remotemediasessionlist",null);
              auth.setSessionAttribute("remotereportsessionlist",null);
              auth.setSessionObjectValue("anasyspermlist",null);
              auth.setSessionObjectValue("arolelist",null);
              auth.setSessionObjectValue("aroleidlist",null);
              auth.setSessionObjectValue("agrouplist",null);
              auth.setSessionObjectValue("agroupidlist",null);
              auth.setSessionObjectValue("apermlist",null);
              auth.setSessionObjectValue("ahapermlist",null);
              auth.setValue("rightslevel","");

              // See if sync directory contains unsynced files
              boolean needsync=false;
              try {

                 String syncdirpath = getSession().getServletContext().getRealPath("/") + File.separator + 
                                      "SyncFiles";
                 Iterator i=Arrays.asList(new File(syncdirpath).listFiles()).iterator();                     
                 while (i.hasNext()) {   
                    File f=(File)i.next();
                    if (f.isFile() && f.getName().toLowerCase().indexOf("xml.encrypt")>0) {
                       needsync=true;
                       break;
                    }
                 }

                 if (needsync) {
                    Localusers luser=(Localusers)getMainDAO().execUniqueQuery(
                       "select l from Localusers l where l.luserid=" + auth.getValue("luserid")
                       );
                    if (luser.getIsadmin() || hasGlobalPerm(PcatConstants.SYSTEM,"INTERNALVIEW")) {
                       // Redirect user to sync screen
                       mav.addObject("status","NEEDSYNC");
                       return mav;
                    }
                 }

              } catch (Exception syncex) {
                 // do nothing
              }
  
              // Redirect user to next screen
              safeRedirect("selectstudy.do");
              return null;

           } else {

              mav.addObject("status",new AuthenticatorConstants().getFieldName(lr.getStatus()));
              mav.addObject("user",session.getAttribute("user"));
              mav.addObject("badlogincount",lr.getBadlogincount());
              mav.addObject("daystoexpire",new Long(lr.getDaystoexpire()).toString());
              return mav;

           }

        } 

      } catch (Exception e) {

        // try reloading
        //mav.addObject("status","RELOAD");
        //return mav;
        throw new FORMSException("Login Controller Error",e);

      } 

      return null;

    }

    private void callSetup(Exception e,String jurl) throws FORMSException {
      
       Pattern p1=Pattern.compile("^.*cause: *Database.*not found\\..*$",Pattern.MULTILINE);
       Pattern p2=Pattern.compile("Connections could not be acquired from the underlying database!.*$",Pattern.MULTILINE);
       Matcher m1=p1.matcher(Stack2string.getString(e));
       Matcher m2=p2.matcher(Stack2string.getString(e));
       if (m1.find() || m2.find()) {
          // Proceed to setup only if database truly does not exist, otherwise just throw exception
          jurl=jurl.replaceFirst("[Jj][Dd][Bb][Cc]:[Dd][Ee][Rr][Bb][Yy]:","");
          jurl=jurl.replaceFirst(";.*$","");
          if (!new File(jurl).exists()) {
             safeRedirect("setup.do");
          } else {
             throw new FORMSException("Database file exists but cannot connect to it");
          }
       } 

    }

}
 














