
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * DatatableUtilController.java - DataTable utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.sql.Timestamp;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.mla.html.table.*;
import com.oreilly.servlet.multipart.*;

public class DatatableUtilController extends FORMSController {
 
   String spath="";

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Use class-name based view
      mav=new ModelAndView("DatatableUtil");

      try {

         HttpServletRequest request=this.getRequest();
         spath=request.getServletPath();

         // addstudy.sutil  (add new study interface screen)
         if (spath.indexOf("removecat.")>=0) {

            mav.addObject("status",removeCat());
            return mav;

         }
         else if (spath.indexOf("viewallcat.")>=0) {
          
            return viewAllCat(mav);

         }
         else if (spath.indexOf("viewallsubmit.")>=0) {
          
            safeRedirect("selectdteditcat.am");
            return mav;

         }
         else if (spath.indexOf("editattrs.")>=0) {
          
            return editAttrs(mav);

         }
         else if (spath.indexOf("editattrssubmit.")>=0) {
          
            return editAttrsSubmit(mav);

         }
         else if (spath.indexOf("deletetable.")>=0) {
          
            return deleteTable(mav);

         }
         else if (spath.indexOf("gethtmldocument.")>=0) {
          
            return getHtmlDocument(mav);

         }
         else if (spath.indexOf("replacehtmldocument.")>=0) {
          
            return replaceHtmlDocument(mav);

         }
         else if (spath.indexOf("replacehtmldocumentsubmit.")>=0) {
          
            return replaceHtmlDocumentSubmit(mav);

         }
         else if (spath.indexOf("archivestatus.")>=0) {
          
            return archiveStatus(mav);

         }
         else if (spath.indexOf("archivestatussubmit.")>=0) {

            return archiveStatusSubmit(mav);

         }
         else if (spath.indexOf("movecat.")>=0) {
          
            return moveCat(mav);

         }
         else if (spath.indexOf("movecatsubmit.")>=0) {
          
            mav.addObject("status",moveCatSubmit());
            return mav;

         }
         else if (spath.indexOf("editcat.")>=0) {

            Datatableha ha=editCat();
            if (ha!=null) {
               // check permission
               if (!getPermHelper().hasGlobalPerm(getAuth().getValue("auserid"),
                   ha.getVisibility(),"DTFORMCAT")) {
                   mav.addObject("status","NOAUTH");
                   return mav;
               }
               // continue
               mav.addObject("status","EDITCAT");
               mav.addObject("datatableha",editCat());
               return mav;
            }
            return null;

         }
         else if (spath.indexOf("editcatsubmit.")>=0) {

            mav.addObject("status",editCatSubmit());
            return mav;

         }
         else if (spath.indexOf("addcat.")>=0) {

            Datatableha ha=addCat();
            if (ha!=null) {
               // See if user has permission to do category edits   
               if (!getPermHelper().hasGlobalPerm(getAuth().getValue("auserid"),
                   ha.getVisibility(),"DTFORMCAT")) {
                   mav.addObject("status","NOAUTH");
                   return mav;
               }
               mav.addObject("status","ADDCAT");
               mav.addObject("datatableha",addCat());
               return mav;
            }
            return null;

         }
         else if (spath.indexOf("addcatsubmit.")>=0) {

            mav.addObject("status",addCatSubmit());
            return mav;

         // Re-order categories interface
         } else if (spath.indexOf("reordercat.")>=0) {

            List l=reorderCat();
            // See if user has permission to do category edits   
            if (l.size()>0) {
               Datatableha ha=(Datatableha)l.get(0);
               if (!getPermHelper().hasGlobalPerm(getAuth().getValue("auserid"),
                   ha.getVisibility(),"DTFORMCAT")) {
                   mav.addObject("status","NOAUTH");
                   return mav;
               }
               // continue 
               mav.addObject("list",l);
               mav.addObject("status","REORDERCAT");
               return mav;
            } else {
               mav.addObject("status","NOREORDERROOT");
               return mav;
            }

         // Submit re-ordering of categories
         } else if (spath.indexOf("reordercatsubmit.")>=0) {

            mav.addObject("status",reorderCatSubmit());
            return mav;

         } else if (spath.indexOf("listprofiles.")>=0) {

            return listProfiles(mav);

         // Add new profile interface (PROFILE LEVEL SELECTION)
         } else if (spath.indexOf("newprofile.")>=0) {

            mav.addObject("status","NEWPROFILE");
            mav.addObject("list",new VisibilityConstants().getFieldList());
            return mav;

         // Add new profile interface (ENTER PROFILE ATTRIBUTES)
         } else if (spath.indexOf("addprofile.")>=0) {

            int profilelevel=new Integer(getRequest().getParameter("profilelevel")).intValue();

            // See if user is permitted to modify profile
            if (!getPermHelper().hasGlobalPerm(getAuth().getValue("auserid"),profilelevel,"DTPERMPROF")) {
               // not permitted
               mav.addObject("status","NOAUTH");
               return mav;
            }

            mav.addObject("status","ADDPROFILE");
            mav.addObject("profilelevel",new VisibilityConstants().getFieldName(profilelevel));
            return mav;

         // Submit new profile to system
         } else if (spath.indexOf("addprofilesubmit.")>=0) {

            String status=addProfileSubmit();
            mav.addObject("status",status);
            return mav;

         // Modify profile interface
         } 
         else if (spath.indexOf("profilemenu.")>=0) {

            mav.addObject("status","PROFILEMENU");
            mav.addObject("dtprofileid",getRequest().getParameter("dtprofileid"));
            return mav;

         } 
         else if (spath.indexOf("modifyprofile.")>=0) {

            mav.addObject("status",modifyProfile());
            return mav;

         // Submit modified profile
         } else if (spath.indexOf("modifyprofilesubmit.")>=0) {

            mav.addObject("status",modifyProfileSubmit());
            return mav;

         // Delete specified profile 
         } else if (spath.indexOf("removeprofilesubmit.")>=0) {

            mav.addObject("status",removeProfileSubmit());
            return mav;

         }
         // add new site interface
         else {
            out.println("BAD REQUEST! - " + spath);
            return null;
         }
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   private ModelAndView editAttrs(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      String dtdefid=getRequest().getParameter("dtdefid");
      boolean islocal=new Boolean(getRequest().getParameter("islocal")).booleanValue();
      if (!islocal) {
         mav.addObject("status","REMOTEDT");
         return mav;
      }
      Datatabledef form=(Datatabledef)getMainDAO().execUniqueQuery(
         "select f from Datatabledef f where f.dtdefid=" + dtdefid
         );
      if (form==null) {
         mav.addObject("status","DTPULLERROR");
         return mav;
      }

      // See if user has permission to edit this form
      FORMSAuth auth=getAuth();
      if (!(form.getUploaduser()==new Long(auth.getValue("auserid")).longValue())) {
         // pull visibility level for form
         Datatableha fha=(Datatableha)getMainDAO().execUniqueQuery(
            "select f from Datatableha f where f.dthaid=" + form.getPdthaid()
            );
         if (!getPermHelper().hasGlobalPerm(auth.getValue("auserid"),
             fha.getVisibility(),"DTEDITOTH")) {
             mav.addObject("status","NOAUTH");
             return mav;
         }
      }
      mav.addObject("status","EDITATTRS");
      mav.addObject("dtdefid",dtdefid);
      mav.addObject("dtacr",form.getDtacr());
      mav.addObject("dtdesc",form.getDtdesc());
      mav.addObject("notes",form.getNotes());
      return mav;
   }

   private ModelAndView editAttrsSubmit(ModelAndView mav) {
      String dtdefid=getRequest().getParameter("dtdefid");
      Datatabledef form=(Datatabledef)getMainDAO().execUniqueQuery(
         "select f from Datatabledef f where f.dtdefid=" + dtdefid
         );
      if (form==null) {
         mav.addObject("status","DTPULLERROR");
         return mav;
      }
      form.setDtacr(getRequest().getParameter("dtacr"));
      form.setDtdesc(getRequest().getParameter("dtdesc"));
      form.setNotes(getRequest().getParameter("notes"));
      getMainDAO().saveOrUpdate(form);
      clearSessionLists();
      mav.addObject("status","EDITATTROK");
      return mav;
   }

   private ModelAndView deleteTable(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      String dtdefid=getRequest().getParameter("dtdefid");


      try {
          // If most recent record entered more than two hours after first record, assume data has been entered and
          // don't delete.  Otherwise allow delete.
          Object[] oarray=(Object[])getMainDAO().execUniqueQuery(
             "select min(savetime),max(savetime) from Datatablerecords f where f.datatabledef.dtdefid=" + dtdefid + " and isaudit!=true"
             );
          if (oarray!=null) {
             Long minTime=((Timestamp)oarray[0]).getTime();
             Long maxTime=((Timestamp)oarray[1]).getTime();
             if ((maxTime-minTime)>(1000*60*60*2)) {
                mav.addObject("status","DTDELETEERROR");
                return mav;
             }
          }
      } catch (Exception e) {
         // Do nothing
      }

      Datatabledef form=(Datatabledef)getMainDAO().execUniqueQuery("select f from Datatabledef f where dtdefid=" + dtdefid);

      // See if user has permission to move this form
      FORMSAuth auth=getAuth();
      if (!(form.getUploaduser()==new Long(auth.getValue("auserid")).longValue())) {
         // pull visibility level for form
         Datatableha fha=(Datatableha)getMainDAO().execUniqueQuery(
            "select f from Datatableha f where f.dthaid=" + form.getPdthaid()
            );
         if (!getPermHelper().hasGlobalPerm(auth.getValue("auserid"),
             fha.getVisibility(),"DTFORMDELOTH")) {
             mav.addObject("status","NOAUTH");
             return mav;
         }
      }

      // Delete form and associated permissions
      List l;
      List l2;
      Iterator i;
      Iterator i2;

      l=getMainDAO().execQuery("select f from Formattrs f where dtdefid=" + dtdefid);
      i=l.iterator();
      while (i.hasNext()) {
         Formattrs f=(Formattrs)i.next();
         getMainDAO().delete(f);
      }
      l=getMainDAO().execQuery("select f from Dtfieldattrs f where dtdefid=" + dtdefid);
      i=l.iterator();
      while (i.hasNext()) {
         Dtfieldattrs f=(Dtfieldattrs)i.next();
         getMainDAO().delete(f);
      }
      l=getMainDAO().execQuery("select f from Datatablecoldef f where dtdefid=" + dtdefid);
      i=l.iterator();
      while (i.hasNext()) {
         Datatablecoldef f=(Datatablecoldef)i.next();
         getMainDAO().delete(f);
      }
      l=getMainDAO().execQuery("select f from Datatablerecords f where dtdefid=" + dtdefid);
      i=l.iterator();
      while (i.hasNext()) {
         Datatablerecords f=(Datatablerecords)i.next();

         l2=getMainDAO().execQuery("select f from Datatablevalues f where datarecordid=" + f.getDatarecordid());
         i2=l2.iterator();
         while (i2.hasNext()) {
            Datatablevalues f2=(Datatablevalues)i2.next();
            getMainDAO().delete(f2);
         }

         getMainDAO().delete(f);
      }

      l=getMainDAO().execQuery("select f from Formtypes f where dtdefid=" + dtdefid);
      i=l.iterator();
      while (i.hasNext()) {
         Formtypes f=(Formtypes)i.next();

         l2=getMainDAO().execQuery("select f from Formstore f where formtypeid=" + f.getFormtypeid());
         i2=l2.iterator();
         while (i2.hasNext()) {
            Formstore f2=(Formstore)i2.next();
            getMainDAO().delete(f2);
         }

         getMainDAO().delete(f);
      }

      l=getMainDAO().execQuery("select f from Dtpermprofassign f where dtdefid=" + dtdefid);
      i=l.iterator();
      while (i.hasNext()) {
         Dtpermprofassign f=(Dtpermprofassign)i.next();
         getMainDAO().delete(f);
      }
      l=getMainDAO().execQuery("select f from Dtrolepermassign f where dtdefid=" + dtdefid);
      i=l.iterator();
      while (i.hasNext()) {
         Dtrolepermassign f=(Dtrolepermassign)i.next();
         getMainDAO().delete(f);
      }
      l=getMainDAO().execQuery("select f from Dtuserpermassign f where dtdefid=" + dtdefid);
      i=l.iterator();
      while (i.hasNext()) {
         Dtuserpermassign f=(Dtuserpermassign)i.next();
         getMainDAO().delete(f);
      }
      getMainDAO().delete(form);
      clearSessionLists();
      mav.addObject("status","DTDELETEOK");
      return mav;
   }

   private ModelAndView getHtmlDocument(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // pull formtypeid for add record function
      Object formtypeid=(Object)getMainDAO().execUniqueQuery(
          "select f.formtypeid from Formtypes f " + 
             "where f.datatabledef.dtdefid=" + getRequest().getParameter("dtdefid") + " and f.formtypelist.formtype='_INTERNAL_'"
             );
      if (formtypeid!=null) {
         getAuth().setValue("formtypeid",formtypeid.toString());
      } else {
         mav.addObject("status","DTPULLERROR");
         return null;
      }
      Formstore fstore=(Formstore)getMainDAO().execUniqueQuery(
            "select f from Formstore f where f.formtypes.formtypeid=" + formtypeid
         );
      if (fstore!=null) {
         out.println(fstore.getFormcontent());
      }
      return null;

   }

   private ModelAndView replaceHtmlDocument(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String dtdefid=getRequest().getParameter("dtdefid");

      // pull formtypeid for add record function
      Object formtypeid=(Object)getMainDAO().execUniqueQuery(
          "select f.formtypeid from Formtypes f " + 
             "where f.datatabledef.dtdefid=" + getRequest().getParameter("dtdefid") + " and f.formtypelist.formtype='_INTERNAL_'"
             );
      if (formtypeid!=null) {
         getAuth().setValue("formtypeid",formtypeid.toString());
      } else {
         mav.addObject("status","DTPULLERROR");
         return null;
      }

      boolean islocal=new Boolean(getRequest().getParameter("islocal")).booleanValue();
      if (!islocal) {
         mav.addObject("status","REMOTEDT");
         return mav;
      }
      Datatabledef form=(Datatabledef)getMainDAO().execUniqueQuery(
         "select f from Datatabledef f where f.dtdefid=" + dtdefid
         );
      if (form==null) {
         mav.addObject("status","DTPULLERROR");
         return mav;
      }

      // See if user has permission to edit this form
      FORMSAuth auth=getAuth();
      if (!(form.getUploaduser()==new Long(auth.getValue("auserid")).longValue())) {
         // pull visibility level for form
         Datatableha fha=(Datatableha)getMainDAO().execUniqueQuery(
            "select f from Datatableha f where f.dthaid=" + form.getPdthaid()
            );
         if (!getPermHelper().hasGlobalPerm(auth.getValue("auserid"),
             fha.getVisibility(),"DTFORMEDITOTH")) {
             mav.addObject("status","NOAUTH");
             return mav;
         }
      }

      mav.addObject("dtdefid",dtdefid);
      mav.addObject("formtypeid",formtypeid);
      mav.addObject("status","REPLACEHTMLDOCUMENT");
      return mav;
   }

   private ModelAndView replaceHtmlDocumentSubmit(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {

         MultipartParser mp = new MultipartParser(request, 10*1024*1024); 
         Part part;
         String fileName=null;
         String fileSource=null;
         String dtdefid=null, formtypeid=null;
   
         while ((part = mp.readNextPart()) != null) {
            String name = part.getName();
            if (part.isParam()) {
               // it's a parameter part
               ParamPart paramPart = (ParamPart) part;
               if (name.equalsIgnoreCase("dtdefid")) {
                  dtdefid = paramPart.getStringValue();
               } else if (name.equalsIgnoreCase("formtypeid")) {
                  formtypeid = paramPart.getStringValue();
               }   
            }
            else if (part.isFile()) {
               // it's a file part
               FilePart filePart = (FilePart) part;
               fileName = filePart.getFileName();
               if (fileName != null) {
                  fileName=fileName.trim();
                  ByteArrayOutputStream baos = new ByteArrayOutputStream();
                  long size = filePart.writeTo(baos);
                  fileSource=baos.toString("ISO-8859-1");
               }
            }
         } 

         Datatabledef form=(Datatabledef)getMainDAO().execUniqueQuery(
            "select f from Datatabledef f where f.dtdefid=" + dtdefid
            );
         if (form==null) {
            mav.addObject("status","DTPULLERROR");
            return mav;
         }
   
         // See if user has permission to edit this form
         FORMSAuth auth=getAuth();
         if (!(form.getUploaduser()==new Long(auth.getValue("auserid")).longValue())) {
            // pull visibility level for form
            Datatableha fha=(Datatableha)getMainDAO().execUniqueQuery(
               "select f from Datatableha f where f.dthaid=" + form.getPdthaid()
               );
            if (!getPermHelper().hasGlobalPerm(auth.getValue("auserid"),
                fha.getVisibility(),"DTFORMEDITOTH")) {
                mav.addObject("status","NOAUTH");
                return mav;
            }
         }

         Formstore fstore=(Formstore)getMainDAO().execUniqueQuery(
               "select f from Formstore f where f.formtypes.formtypeid=" + formtypeid
            );
         if (fstore!=null) {
            fstore.setFormcontent(fileSource);
            getMainDAO().saveOrUpdate(fstore);
            mav.addObject("status","HTMLDOCUMENTREPLACED");
            return mav;
         } else {
            mav.addObject("status","HTMLDOCUMENTNOTREPLACED");
            return mav;
         }
   
      } catch (Exception e) {
   
         mav.addObject("status","HTMLDOCUMENTNOTREPLACED");
         return mav;
      }


   }


   private ModelAndView archiveStatus(ModelAndView mav) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      String dtdefid=getRequest().getParameter("dtdefid");
      boolean islocal=new Boolean(getRequest().getParameter("islocal")).booleanValue();
      if (!islocal) {
         mav.addObject("status","REMOTEDT");
         return mav;
      }
      Datatabledef form=(Datatabledef)getMainDAO().execUniqueQuery(
         "select f from Datatabledef f where f.dtdefid=" + dtdefid
         );
      if (form==null) {
         mav.addObject("status","DTPULLERROR");
         return mav;
      }

      // See if user has permission to move this form
      FORMSAuth auth=getAuth();
      if (!(form.getUploaduser()==new Long(auth.getValue("auserid")).longValue())) {
         // pull visibility level for form
         Datatableha fha=(Datatableha)getMainDAO().execUniqueQuery(
            "select f from Datatableha f where f.dthaid=" + form.getPdthaid()
            );
         if (!getPermHelper().hasGlobalPerm(auth.getValue("auserid"),
             fha.getVisibility(),"DTFORMDELOTH")) {
             mav.addObject("status","NOAUTH");
             return mav;
         }
      }

      mav.addObject("status","ARCHIVESTATUS");
      mav.addObject("dtdefid",dtdefid);
      mav.addObject("formacr",form.getFormacr());
      mav.addObject("formdesc",form.getFormdesc());
      mav.addObject("dtacr",form.getDtacr());
      mav.addObject("dtdesc",form.getDtdesc());
      mav.addObject("isarchived",form.getIsarchived());
      return mav;
   }

   private ModelAndView archiveStatusSubmit(ModelAndView mav) {
      String dtdefid=getRequest().getParameter("dtdefid");
      Datatabledef form=(Datatabledef)getMainDAO().execUniqueQuery(
         "select f from Datatabledef f where f.dtdefid=" + dtdefid
         );
      if (form==null) {
         mav.addObject("status","DTPULLERROR");
         return mav;
      }
      String isarchived=getRequest().getParameter("isarchived");
      if (isarchived!=null && isarchived.equals("1")) {
         form.setIsarchived(true);
      } else {
         form.setIsarchived(false);
      }
      getMainDAO().saveOrUpdate(form);
      clearSessionLists();
      mav.addObject("status","ARCHIVESTATUSOK");
      return mav;
   }

   // Empty session-stored forms lists so data are repulled
   private void clearSessionLists() {   
      try {
         getAuth().setSessionAttribute("dthalist",new ArrayList());
         getAuth().setSessionAttribute("formtypeslist",new ArrayList());
      } catch (Exception e) { }
   }   

   private Datatableha editCat() {
      String dthaid=getRequest().getParameter("dthaid");
      Datatableha ha=(Datatableha)getMainDAO().execUniqueQuery(
         "select ha from Datatableha ha where dthaid=" + dthaid
         );
      return ha;   
   }

   private String editCatSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      String dthaid=getRequest().getParameter("dthaid");
      String hadesc=getRequest().getParameter("hadesc");
      Datatableha ha=(Datatableha)getMainDAO().execUniqueQuery(
         "select ha from Datatableha ha where dthaid=" + dthaid
         );
      // check permission
      if (!getPermHelper().hasGlobalPerm(getAuth().getValue("auserid"),
          ha.getVisibility(),"DTFORMCAT")) {
          return "NOAUTH";
      }
      // Make sure description doesn't already exist in category   
      List l=getMainDAO().execQuery(
         "select ha from Datatableha ha where ha.pdthaid=" + dthaid + " and upper(ha.hadesc)=upper('" + hadesc + "')" 
         );
      if (l.size()>0) {
         return "CATNOTUNIQUE";
      }
      ha.setHadesc(hadesc);   
      getMainDAO().saveOrUpdate(ha);
      clearSessionLists();
      FormMetaUtil.setLastupdatelocal(getMainDAO());
      return "CATEDITED";
   }

   private Datatableha addCat() {
      String dthaid=getRequest().getParameter("dthaid");
      Datatableha ha=(Datatableha)getMainDAO().execUniqueQuery(
         "select ha from Datatableha ha where ha.dthaid=" + dthaid
         );
      return ha;   
   }

   private String addCatSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      String dthaid=getRequest().getParameter("dthaid");
      String hadesc=getRequest().getParameter("hadesc");
      Studies currstudy=(Studies)getMainDAO().execUniqueQuery(
         "select std from Studies std where std.studyid=" + getAuth().getValue("studyid")
         );
      // Make sure description doesn't already exist in category   
      List l=getMainDAO().execQuery(
         "select ha from Datatableha ha where ha.pdthaid=" + dthaid + " and upper(ha.hadesc)=upper('" + hadesc + "')" 
         );
      if (l.size()>0) {
         return "CATNOTUNIQUE";
      }
      Integer haordr=(Integer)getMainDAO().execUniqueQuery(
         "select max(ha.haordr) from Datatableha ha where ha.pdthaid=" + dthaid
         );
      Integer visibility=(Integer)getMainDAO().execUniqueQuery(
         "select ha.visibility from Datatableha ha where ha.dthaid=" + dthaid
         );
      if (haordr==null) haordr=1;
      else haordr++;
      Datatableha ha=new Datatableha();
      ha.setPdthaid(new Long(dthaid));
      ha.setHadesc(hadesc);   
      ha.setHaordr(haordr);   
      ha.setVisibility(visibility);   
      ha.setStudies(currstudy);   
      getMainDAO().saveOrUpdate(ha);
      clearSessionLists();
      return "CATADDED";
   }

   private ModelAndView moveCat(ModelAndView mav) {
      return moveCat(mav,false);
   }

   private ModelAndView moveCat(ModelAndView mav,boolean iscatrequest) {

      try {

        FORMSAuth auth=this.getAuth();

        Datatableha ftpfha=null;
        Datatabledef fd=null;
        int ftpfhavis;

        if (!iscatrequest) {

           // pull current formtype record
           fd=(Datatabledef)getMainDAO().execUniqueQuery(
              "select fd from Datatabledef fd " +
                 "where fd.dtdefid=" + request.getParameter("dtdefid")
              );   
     
           if (fd==null) throw new FORMSException("Couldn't pull datatable record");   
    
           // pull record's datatableha parent
           ftpfha=(Datatableha)getMainDAO().execUniqueQuery(
              "select ha from Datatableha ha where ha.dthaid=" + new Long(fd.getPdthaid()).toString()
              );
        
           ftpfhavis=ftpfha.getVisibility();   

           // See if user has permission to move this form
           if (!(fd.getUploaduser()==new Long(auth.getValue("auserid")).longValue())) {
              if (!getPermHelper().hasGlobalPerm(auth.getValue("auserid"),
                  ftpfhavis,"DTFORMMOVEOTH")) {
                  mav.addObject("status","NOAUTH");
                  return mav;
              }
           }

        } else {
    
           // pull record's datatableha parent
           ftpfha=(Datatableha)getMainDAO().execUniqueQuery(
              "select ha from Datatableha ha where ha.dthaid=" + request.getParameter("dthaid")
              );

           ftpfhavis=ftpfha.getVisibility();   

        }   

        String siteid=auth.getValue("siteid");
        String studyid=auth.getValue("studyid");

        List fha=getMainDAO().execQuery(
           "select fha from Datatableha fha " +
                 "where (fha.visibility=" + new Integer(ftpfhavis).toString() + " and " +
                        "((" +
                           "fha.visibility=" + VisibilityConstants.SYSTEM + 
                           ") or (" +
                           "fha.visibility=" + VisibilityConstants.SITE + " and fha.studies.sites.siteid=" + siteid + 
                           ") or (" +
                           "fha.visibility=" + VisibilityConstants.STUDY + " and fha.studies.studyid=" + studyid + 
                        "))) " +
                 "order by haordr"
           );

        Iterator i=fha.iterator();
        StringBuilder sb=new StringBuilder();
        while (i.hasNext()) {
           Datatableha ha=(Datatableha)i.next();
           sb.append(new Long(ha.getDthaid()).toString());
           if (i.hasNext()) sb.append(",");
        }

        ////////////////////////////////////////////////////////////////////
        // Organize data into heirarchical format for building HTML table //
        ////////////////////////////////////////////////////////////////////

        // ft holds number of rows in table.  Keep this info for later
        //int tablerows=ft.size();
        int tablerows=0;

        // tlist holds table columns
        ArrayList tlist=new ArrayList();
        // alist holds items within columns
        ArrayList alist=new ArrayList();

        // pull first table column
        i=fha.iterator();
        while (i.hasNext()) {
           Datatableha ha=(Datatableha)i.next();
           if (ha.getPdthaid()==null) {
              HashMap map=new HashMap();
              map.put("obj",ha);
              map.put("nrows",0);
              map.put("ncols",1);
              // remove element from collection so we don't have to compare it again
              i.remove();
              alist.add(map);
           }
        }   
        tlist.add(alist);
        // pull subsequent columns by finding children of elements in previous columns
        for (int ii=1;ii>-1;ii++) {
           // pull elements in prior column
           ArrayList plist=(ArrayList)tlist.get(ii-1);
           alist=new ArrayList();
           Iterator i2=plist.iterator();
           boolean again=false;
           //iterate through prior column elements
           while (i2.hasNext()) {
              // find children
              HashMap pmap=(HashMap)i2.next();
              Datatableha pha=(Datatableha)pmap.get("obj");
              i=fha.iterator();
              boolean havechild=false;
              int prevsize=alist.size();
              while (i.hasNext()) {
                 Datatableha cha=(Datatableha)i.next();
                 // write child out to alist
                 if (cha.getPdthaid().equals(pha.getDthaid())) {
                    HashMap cmap=new HashMap();
                    cmap.put("obj",cha);
                    cmap.put("nrows",0);
                    cmap.put("ncols",1);
                    // remove element from collection so we don't have to compare it again
                    i.remove();
                    alist.add(cmap);
                    again=true;
                    havechild=true;
                 }
                 
              }
              // HA-ONLY Specific Code
              if (!havechild) {
                 tablerows++;
                 pmap.put("nrows",1);
                 pmap.put("havechild","N");
              } else {
                 pmap.put("havechild","Y");
              }
           }
           if (!again) break;
           tlist.add(alist);
        }

        // add forms to appropriate category (must iterate backwards through tlist
        // since forms are attached as the right most element)
        for (int ii=(tlist.size()-1);ii>=0;ii--) {
           // pull column arraylist
           ArrayList plist=(ArrayList)tlist.get(ii);
           Iterator i2=plist.iterator();
           while (i2.hasNext()) {
              HashMap pmap=(HashMap) i2.next();
              Datatableha pha=(Datatableha)pmap.get("obj");
              // formlist holds forms belonging to an element
              ArrayList formlist=new ArrayList();
              Long compid=null;
              try {
                 compid=new Long(pha.getPdthaid());
              } catch (Exception cie) { }
              // loop through prior columns looking for parent elements
              int addrows=1;
              if (pmap.get("havechild").equals("N")){ 
                 pmap.put("ncols",tlist.size()-ii);
                 for (int jj=(ii-1);jj>=0;jj--) {
                    ArrayList pplist=(ArrayList)tlist.get(jj);
                    Iterator i4=pplist.iterator();
                    while (i4.hasNext()) {
                       HashMap ppmap=(HashMap) i4.next();
                       Datatableha ppha=(Datatableha)ppmap.get("obj");
                       // if find a parent element, add rows to that element.  Then update
                       // compid to that element's parent for next loop
                       if ((
                              (!(compid==null)) && ppha.getDthaid()==compid.longValue()
                           ) ||
                           (
                              ppha.getDthaid()==pha.getDthaid() && ppha.getPdthaid()==pha.getPdthaid()
                          )) {
                          int nrows=((Integer)ppmap.get("nrows")).intValue();
                          nrows=nrows+addrows;
                          ppmap.put("nrows",nrows);
                          try {
                             compid=new Long(ppha.getPdthaid());
                          } catch (Exception cie2) {}
                       }
                    }
                 }
              }
           }
        }
        
        /////////////////////////////////
        // Create HTML Table of result //
        /////////////////////////////////

        int tablecols=tlist.size();

        if (tablerows<1 || tablecols<1) {
           return null;
        }
        Table table=new Table(tablerows,tablecols);
        // iterate through table columns;
        for (int currcol=0;currcol<tlist.size();currcol++) {
           ArrayList rowlist=(ArrayList)tlist.get(currcol);
           int prevrow=0;
           Iterator rowiter=rowlist.iterator();
           while (rowiter.hasNext()) {
              HashMap map2=(HashMap)rowiter.next();
              Datatableha ha2=(Datatableha)map2.get("obj");
              int nrows=((Integer)map2.get("nrows")).intValue();
              int ncols=((Integer)map2.get("ncols")).intValue();
              if (nrows<=0 || ncols<=0) continue;
              Cell cell=new Cell(new Long(ha2.getDthaid()).toString(),nrows,ncols);
              cell.setContent("type","datatableha");
              cell.setContent("obj",ha2);
              boolean okvar=false;
              int currrow=prevrow;
              // Use previous row & exceptions to calculate cell row placement.  This could probably
              // be made more efficient by running through a calculation loop, but the loss here 
              // is quite minimal
              int firstrow=0;
              while (!okvar) {
                 try {
                    table.setCell(cell,currrow,currcol);
                    firstrow=currrow;
                    prevrow=currrow+1;
                    okvar=true;
                 } catch (Exception te) { 
                    currrow++;
                    if (currrow>500000) throw new FORMSException("Too many rows returned");
                 }
              }   
           }
        }

        mav.addObject("status","MOVECATSEL");
        if (fd!=null) mav.addObject("passobj",fd);
        mav.addObject("table",table);

        return mav;

      } catch (Exception e) {

        mav.addObject("status","MOVECATERROR");
        return mav;

      }

   }

   private String moveCatSubmit() {
      String dthaid=getRequest().getParameter("dthaid");
      if (getRequest().getParameter("objecttype").indexOf("Datatabledef")>=0) {
         // Move form
         String dtdefid=getRequest().getParameter("objectid");
         Datatabledef fd=(Datatabledef)getMainDAO().execUniqueQuery(
            "select fd from Datatabledef fd where dtdefid=" + dtdefid
            );
         fd.setPdthaid(new Long(dthaid));
         getMainDAO().saveOrUpdate(fd);
      } 
      clearSessionLists();
      return "DTMOVED";
   }

   private String removeCat() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      String dthaid=getRequest().getParameter("dthaid");
      Datatableha fa=(Datatableha)getMainDAO().execUniqueQuery(
         "select fa from Datatableha fa where fa.dthaid=" + dthaid
         );
      // check permission
      if (!getPermHelper().hasGlobalPerm(getAuth().getValue("auserid"),
          fa.getVisibility(),"DTFORMCAT")) {
          return "NOAUTH";
      }
      // make sure category is not a root category
      if (fa.getPdthaid()==null) {
         return "NOREMOVEROOT";
      }
      // make sure category contains no subcategories
      List clist=getMainDAO().execQuery(
         "select fa from Datatableha fa where fa.pdthaid=" + dthaid
         );
      if (clist.size()>0) {
         return "NOREMOVESUBCAT";
      }
      // Move existing forms to parent   
      long pdthaid=fa.getPdthaid();
      List l=getMainDAO().execQuery(
         "select fd from Datatabledef fd where fd.pdthaid=" + dthaid
         );
      Iterator i=l.iterator();
      while (i.hasNext()) {
         Datatabledef fd=(Datatabledef)i.next();
         fd.setPdthaid(pdthaid);
         getMainDAO().saveOrUpdate(fd);
      }
      // Remove record
      getMainDAO().delete(fa);
      clearSessionLists();
      return "CATCOLLAPSED";
   }

   // Re-order categories within parent category
   public List reorderCat() {

      Long dthaid=new Long(getRequest().getParameter("dthaid"));

      Long pdthaid=(Long)getMainDAO().execUniqueQuery(
                "select ha.pdthaid from Datatableha ha where ha.dthaid=" + dthaid
                );

      List l=getMainDAO().execQuery(
                "select ha from Datatableha ha where ha.pdthaid=:pdthaid " +
                   "order by ha.haordr" 
                ,new String[] { "pdthaid" }
                ,new Object[] { pdthaid }
                );

      return l;

   }

   public String reorderCatSubmit() {

      String corderstr=(String)request.getParameter("corderstr");
      String[] sa=corderstr.split(",");
      for (int i=0;i<sa.length;i++) {
         Long dthaid=new Long(sa[i].replaceFirst("^li_",""));
         Datatableha fa2mod=(Datatableha)getMainDAO().execUniqueQuery(
                   "select ha from Datatableha ha where ha.dthaid=:dthaid"
                   ,new String[] { "dthaid" }
                   ,new Object[] { dthaid }
                   );
         fa2mod.setHaordr(i+1);
         getMainDAO().saveOrUpdate(fa2mod);
      }
      clearSessionLists();
      return "CATREORDERED";

   }

   private ModelAndView viewAllCat(ModelAndView mav) {

      mav=moveCat(mav,true);
      String status=(String)mav.getModelMap().get("status");  
      if (status.equalsIgnoreCase("MOVECATSEL")) {
        mav.addObject("status","VIEWALLSEL"); 
      }
      return mav;
      
   }

   private ModelAndView listProfiles(ModelAndView mav) {

      try {
         List l=getMainDAO().execQuery(
            "select p from Dtpermprofiles p where " +
               "(p.profilelevel=" + VisibilityConstants.SYSTEM + ") or " +
               "(p.profilelevel=" + VisibilityConstants.SITE + 
                  " and p.siteid=" + getAuth().getValue("siteid") + ") or " +
               "(p.profilelevel=" + VisibilityConstants.STUDY + 
                  " and p.studyid=" + getAuth().getValue("studyid") + ") " 
                  );
         mav.addObject("status","LISTPROFILES");
         Iterator i=l.iterator();
         ArrayList newl=new ArrayList();
         while (i.hasNext()) {
            Dtpermprofiles p=(Dtpermprofiles)i.next();
            HashMap map=new HashMap();
            map.put("profile",p);
            map.put("level",new VisibilityConstants().getFieldName(p.getProfilelevel()));
            newl.add(map);
         }
         mav.addObject("list",newl);
         return mav;
      } catch (Exception e) {
         return null;
      }

   }


   // Submit new profile
   private String addProfileSubmit() {

      String status=null;
      String profileacr=getRequest().getParameter("profileacr").toUpperCase();
      String profiledesc=getRequest().getParameter("profiledesc");
      int profilelevel=new VisibilityConstants().getFieldValue(getRequest().getParameter("profilelevel"));
      
      // Make sure profile acronym is not duplicated witin level and site/study
      List l;
      long q_siteid;
      try {
         q_siteid=new Long(getAuth().getValue("siteid")).longValue();
      } catch (Exception sie) {
         q_siteid=new Long("-999999999").longValue();
      }
      long q_studyid;
      try {
         q_studyid=new Long(getAuth().getValue("studyid")).longValue();
      } catch (Exception sie) {
         q_studyid=new Long("-999999999").longValue();
      }
      l=getMainDAO().execQuery(
            "select p from Dtpermprofiles p where " +
                "p.profileacr=:profileacr and (" +
                   "(" + profilelevel + "=" + VisibilityConstants.SYSTEM + " and p.profilelevel=" + VisibilityConstants.SYSTEM + ") or " +
                   "(" + profilelevel + "=" + VisibilityConstants.SITE + " and p.profilelevel=" + VisibilityConstants.SITE +
                       " and p.siteid=:siteid) or " +
                   "(" + profilelevel + "=" + VisibilityConstants.STUDY  + " and p.profilelevel=" + VisibilityConstants.STUDY +
                       " and p.studyid=:studyid)" +
                ")"
             ,new String[] { "profileacr","siteid","studyid" }
             ,new Object[] { profileacr,q_siteid,q_studyid }   
         );
      if (l.size()>0) {
         status="DUPLICATEPROFILE";
         return status;
      }
      // If not duplicate, add to database
      Dtpermprofiles profile=new Dtpermprofiles();
      profile.setProfileacr(profileacr);
      profile.setProfiledesc(profiledesc);
      profile.setProfilelevel(profilelevel);
      try {
         profile.setStudyid(new Long(getAuth().getValue("studyid")).longValue());
      } catch (Exception sse) {
         // may be null for system level profiles
      }
      try {
         profile.setSiteid(new Long(getAuth().getValue("siteid")).longValue());
      } catch (Exception sse) {
         // may be null for system level profiles
      }
      getMainDAO().saveOrUpdate(profile);
      getMAV().addObject("profilelevel",new VisibilityConstants().getFieldName(profilelevel));
      getMAV().addObject("profileacr",profileacr);
      FormMetaUtil.setLastupdatelocal(getMainDAO());
      status="PROFILEADDED";
      return status;

   }

   // Modify profile interface
   private String modifyProfile() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String status=null;
      long dtprofileid=new Long(getRequest().getParameter("dtprofileid")).longValue();
      
      // Make sure profile acronym is not duplicated witin level and site/study
      Dtpermprofiles p=(Dtpermprofiles)getMainDAO().execUniqueQuery(
            "select p from Dtpermprofiles p where p.dtprofileid=" + dtprofileid 
         );
      if (p==null) {
         status="PROFILEPULLERROR";
         return status;
      }

      // See if user is permitted to modify profile
      if (!getPermHelper().hasGlobalPerm(getAuth().getValue("auserid"),p.getProfilelevel(),"DTFORMPERMPROF")) {
         // not permitted
         return "NOAUTH";
      }

      // else continue
      getMAV().addObject("profile",p);
      getMAV().addObject("level",new VisibilityConstants().getFieldName(p.getProfilelevel()));
      status="MODIFYPROFILE";
      return status;

   }

   // Submit modified profile
   private String modifyProfileSubmit() {

      String status=null;
      String profileacr=getRequest().getParameter("profileacr").toUpperCase();
      String profiledesc=getRequest().getParameter("profiledesc");
      int profilelevel=new VisibilityConstants().getFieldValue(getRequest().getParameter("profilelevel"));
      long dtprofileid=new Long(getRequest().getParameter("dtprofileid")).longValue();
      
      // Make sure profile acronym is not duplicated witin level and site/study
      List l;
      long q_siteid;
      try {
         q_siteid=new Long(getAuth().getValue("siteid")).longValue();
      } catch (Exception sie) {
         q_siteid=new Long("-999999999").longValue();
      }
      long q_studyid;
      try {
         q_studyid=new Long(getAuth().getValue("studyid")).longValue();
      } catch (Exception sie) {
         q_studyid=new Long("-999999999").longValue();
      }
      l=getMainDAO().execQuery(
            "select p from Dtpermprofiles p where " +
                "p.dtprofileid!=:dtprofileid and " +
                "p.profileacr=:profileacr and (" +
                   "(" + profilelevel + "=" + VisibilityConstants.SYSTEM + " and p.profilelevel=" + VisibilityConstants.SYSTEM + ") or " +
                   "(" + profilelevel + "=" + VisibilityConstants.SITE + " and p.profilelevel=" + VisibilityConstants.SITE +
                       " and p.siteid=:siteid) or " +
                   "(" + profilelevel + "=" + VisibilityConstants.STUDY  + " and p.profilelevel=" + VisibilityConstants.STUDY +
                       " and p.studyid=:studyid)" +
                ")"
             ,new String[] { "dtprofileid","profileacr","siteid","studyid" }
             ,new Object[] { dtprofileid,profileacr,q_siteid,q_studyid }   
         );
      if (l.size()>0) {
         status="DUPLICATEPROFILE";
         return status;
      }
      // If not duplicate, add to database
      Dtpermprofiles profile=(Dtpermprofiles)getMainDAO().execUniqueQuery(
         "select p from Dtpermprofiles p where dtprofileid=" + dtprofileid
         );
      if (profile!=null) {   
         profile.setProfileacr(profileacr);
         profile.setProfiledesc(profiledesc);
         getMainDAO().saveOrUpdate(profile);
         FormMetaUtil.setLastupdatelocal(getMainDAO());
         status="PROFILEMODIFIED";
         return status;
      }
      status="PROFILEPULLERROR";
      return status;

   }

   // Delete profile
   private String removeProfileSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String status=null;
      long dtprofileid=new Long(getRequest().getParameter("dtprofileid")).longValue();

      // Can only delete profile if no forms are using it
      List l=getMainDAO().execQuery(
         "select pa from Dtpermprofassign pa where pa.dtpermprofiles.dtprofileid=" + dtprofileid
         );
      if (l.size()>0) {
         status="PROFILENOTEMPTY";
         return status;
      }
      
      Dtpermprofiles profile=(Dtpermprofiles)getMainDAO().execUniqueQuery(
            "select profile from Dtpermprofiles profile where profile.dtprofileid=" + dtprofileid 
         );
      if (profile==null) {
         status="PROFILEPULLERROR";
         return status;
      }

      // See if user is permitted to modify profile
      if (!getPermHelper().hasGlobalPerm(getAuth().getValue("auserid"),profile.getProfilelevel(),"DTFORMPERMPROF")) {
         // not permitted
         return "NOAUTH";
      }

      getMainDAO().delete(profile);
      FormMetaUtil.setLastupdatelocal(getMainDAO());
      status="PROFILEDELETED";
      return status;

   }

}

