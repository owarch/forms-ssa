 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * FormtypesUtilController.java - User utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.security.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.*;
import javax.crypto.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;


public class FormtypesUtilController extends FORMSController {

   boolean isbadrequest;

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Use class-name based view
      mav=new ModelAndView("FormtypesUtil");
      isbadrequest=true;
      String spath=request.getServletPath();

      try {

         // Add new formtype interface (VISIBILITY SELECTION)
         if (spath.indexOf("newformtype.")>=0) {

            isbadrequest=false;
            mav.addObject("status","NEWFORMTYPE");
            String studyid=this.getAuth().getValue("studyid");
            if (studyid==null || studyid.length()<1) {
               mav.addObject("list",new VisibilityConstants().getFieldList(VisibilityConstants.SYSTEM));
            } else {
               mav.addObject("list",new VisibilityConstants().getFieldList());
            }
            return mav;

         // Add new formtype interface (ENTER FORMTYPE ATTRIBUTES)
         } else if (spath.indexOf("addformtype.")>=0) {

            if (!hasGlobalPerm("FTYPECREATE")) {
               safeRedirect("permission.err");
               return null;
            }

            isbadrequest=false;
            mav.addObject("status","ADDFORMTYPE");
            mav.addObject("visibility",new VisibilityConstants().getFieldName(
                       new Integer(getRequest().getParameter("visibility")).intValue()
                         ));
            return mav;

         // Submit new formtype to system
         } else if (spath.indexOf("addformtypesubmit.")>=0) {

            String status=addFormtypeSubmit();
            mav.addObject("status",status);
            return mav;

         // Modify formtype interface
         } else if (spath.indexOf("modifyformtype.")>=0) {

            modifyFormtype();
            isbadrequest=false;
            mav.addObject("status","MODIFYFORMTYPE");
            return mav;

         // Submit modified formtype
         } else if (spath.indexOf("modifyformtypesubmit.")>=0) {

            String status=modifyFormtypeSubmit();
            mav.addObject("status",status);
            return mav;

         // Delete specified formtype 
         } else if (spath.indexOf("removeformtypesubmit.")>=0) {

            String status=removeFormtypeSubmit();
            mav.addObject("status",status);
            return mav;

         } 

         out.println(spath);
         isbadrequest=false;
         return null;

      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Submit new formtype
   private String addFormtypeSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

            String status=null;
            String formtype=getRequest().getParameter("formtype").toUpperCase();
            int visibility=new VisibilityConstants().getFieldValue(getRequest().getParameter("visibility"));

            if (!hasGlobalPerm(visibility,"FTYPECREATE")) {
               safeRedirect("permission.err");
               return null;
            }
            
            // Make sure formtype is not duplicated witin level and site/study
            List l;
            long q_siteid=new Long(getAuth().getValue("siteid")).longValue();
            long q_studyid=new Long(getAuth().getValue("studyid")).longValue();
            long auserid=new Long(getAuth().getValue("auserid")).longValue();
            l=getMainDAO().execQuery(
                  "select ft from Formtypelist ft join fetch ft.studies join fetch ft.studies.sites where " +
                      "ft.formtype=:formtype and (" +
                         "(" + visibility + "=" + VisibilityConstants.SYSTEM + " and ft.visibility=" + VisibilityConstants.SYSTEM + ") or " +
                         "(" + visibility + "=" + VisibilityConstants.SITE + " and ft.visibility=" + VisibilityConstants.SITE +
                             " and ft.studies.sites.siteid=:siteid) or " +
                         "(" + visibility + "=" + VisibilityConstants.STUDY  + " and ft.visibility=" + VisibilityConstants.STUDY +
                             " and ft.studies.studyid=:studyid)" +
                      ")"
                   ,new String[] { "formtype","siteid","studyid" }
                   ,new Object[] { formtype,q_siteid,q_studyid }   
               );
            if (l.size()>0) {
               status="DUPLICATEFORMTYPE";
               return status;
            }
            // Return local allinst record
            Allinst ainst=(Allinst)getMainDAO().execUniqueQuery("select a from Allinst a where a.islocal=true");
            // If not duplicate, add to database
            Formtypelist ftype=new Formtypelist();
            ftype.setFormtype(formtype);
            ftype.setVisibility(visibility);
            Studies std=(Studies)getMainDAO().execUniqueQuery(
                  "select std from Studies std join fetch std.sites " +
                     "where std.sites.siteid=" + new Long(q_siteid).toString() + 
                     " and std.studyid=" + new Long(q_studyid).toString() 
               );
            ftype.setStudies(std);
            ftype.setAllinst(ainst);
            ftype.setCreateuser(auserid);
            getMainDAO().saveOrUpdate(ftype);
            isbadrequest=false;
            getMAV().addObject("visibility",new VisibilityConstants().getFieldName(visibility));
            getMAV().addObject("formtype",formtype);
            FormMetaUtil.setLastupdatelocal(getMainDAO());
            status="FORMTYPEADDED";
            return status;

   }

   // Modify formtype interface
   private String modifyFormtype() throws FORMSException, FORMSKeyException, FORMSSecurityException {

            String status=null;
            long formtypelistid=new Long(getRequest().getParameter("formtypelistid")).longValue();
            
            // Make sure formtype is not duplicated witin level and site/study
            Formtypelist ft=(Formtypelist)getMainDAO().execUniqueQuery(
                  "select ft from Formtypelist ft where " + 
                      "ft.formtypelistid=:formtypelistid" 
                   ,new String[] { "formtypelistid" }
                   ,new Object[] { formtypelistid }   
               );
            if (ft==null) {
               status="FORMTYPEPULLERROR";
               return status;
            }

            // See if user has permission to edit this form
            FORMSAuth auth=getAuth();
            if (!(ft.getCreateuser()==new Long(auth.getValue("auserid")).longValue())) {
               // pull visibility level for ft
               if (!getPermHelper().hasGlobalPerm(auth.getValue("auserid"),
                   ft.getVisibility(),"FTYPEEDITOTH")) {
                   return "NOAUTH";
               }
            }

            getMAV().addObject("formtypelistid",formtypelistid);
            getMAV().addObject("visibility",new VisibilityConstants().getFieldName(ft.getVisibility()));
            getMAV().addObject("formtype",ft.getFormtype());
            status="MODIFYFORMTYPE";
            return status;

   }

   // Submit modified formtype
   private String modifyFormtypeSubmit() throws FORMSException, FORMSKeyException, FORMSSecurityException {

            String status=null;
            String formtype=getRequest().getParameter("formtype").toUpperCase();
            int visibility=new VisibilityConstants().getFieldValue(getRequest().getParameter("visibility"));
            long formtypelistid=new Long(getRequest().getParameter("formtypelistid")).longValue();
            
            // Make sure formtype is not duplicated witin level and site/study
            List l;
            long q_siteid;
            try {
               q_siteid=new Long(getAuth().getValue("siteid")).longValue();
            } catch (Exception sie) {
               q_siteid=new Long("-999999999").longValue();
            }
            long q_studyid;
            try {
               q_studyid=new Long(getAuth().getValue("studyid")).longValue();
            } catch (Exception sie) {
               q_studyid=new Long("-999999999").longValue();
            }
            l=getMainDAO().execQuery(
                  "select ft from Formtypelist ft join fetch ft.studies join fetch ft.studies.sites where " + 
                      "ft.formtypelistid!=:formtypelistid and " +
                      "ft.formtype=:formtype and (" +
                         "(" + visibility + "=" + VisibilityConstants.SYSTEM + " and ft.visibility=" + VisibilityConstants.SYSTEM + ") or " +
                         "(" + visibility + "=" + VisibilityConstants.SITE + " and ft.visibility=" + VisibilityConstants.SITE +
                             " and ft.studies.sites.siteid=:siteid) or " +
                         "(" + visibility + "=" + VisibilityConstants.STUDY  + " and ft.visibility=" + VisibilityConstants.STUDY +
                             " and ft.studies.studyid=:studyid)" +
                      ")"
                   ,new String[] { "formtypelistid","formtype","siteid","studyid" }
                   ,new Object[] { formtypelistid,formtype,q_siteid,q_studyid }   
               );
            if (l.size()>0) {
               status="DUPLICATEFORMTYPE";
               return status;
            }
            // If not duplicate, add to database
            Formtypelist ftype=(Formtypelist)getMainDAO().execUniqueQuery(
               "select f from Formtypelist f where f.formtypelistid=" + new Long(formtypelistid).toString()
               );
            ftype.setFormtype(formtype);
            getMainDAO().saveOrUpdate(ftype);
            isbadrequest=false;
            FormMetaUtil.setLastupdatelocal(getMainDAO());
            status="FORMTYPEMODIFIED";
            // Clear out session form list so forms get repulled with current formtyhpe
            getAuth().setSessionAttribute("formtypeslist","");

            return status;

   }

   // Delete formtype
   private String removeFormtypeSubmit() throws FORMSException, FORMSKeyException, FORMSSecurityException {

            String status=null;
            long formtypelistid=new Long(getRequest().getParameter("formtypelistid")).longValue();
            
            // Make sure formtype is not duplicated witin level and site/study
            Formtypelist rr=(Formtypelist)getMainDAO().execUniqueQuery(
                  "select rr from Formtypelist rr where " +
                      "rr.formtypelistid=:formtypelistid" 
                   ,new String[] { "formtypelistid" }
                   ,new Object[] { formtypelistid }   
               );
            if (rr==null) {
               status="FORMTYPEPULLERROR";
               return status;
            }

            // See if user has permission to edit this form
            FORMSAuth auth=getAuth();
            if (!(rr.getCreateuser()==new Long(auth.getValue("auserid")).longValue())) {
               // pull visibility level for rr
               if (!getPermHelper().hasGlobalPerm(auth.getValue("auserid"),
                   rr.getVisibility(),"FTYPEDELOTH")) {
                   return "NOAUTH";
               }
            }

            // Cannot delete if there are current forms for the formtype
            List l=getMainDAO().execQuery(
               "select f from Formtypes f where f.formtypelist.formtypelistid=" + new Long(formtypelistid).toString()
               );
            if (l.size()>0) {
               status="FORMTYPEINUSE";
               return status;
            }
            isbadrequest=false;
            getMainDAO().delete(rr);
            FormMetaUtil.setLastupdatelocal(getMainDAO());
            status="FORMTYPEDELETED";
            return status;

   }


}


