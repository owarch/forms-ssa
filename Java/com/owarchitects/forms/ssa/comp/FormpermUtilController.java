 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * FormpermUtilController.java - User utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.security.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.*;
import javax.crypto.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;


public class FormpermUtilController extends FORMSController {

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // check permissions
      //if (!getAuth().getValue("rightslevel").equalsIgnoreCase("ADMIN")) {
      //   safeRedirect("permission.err");
      //   return null;
      //}

      // Use class-name based view
      mav=new ModelAndView("FormpermUtil");
      String spath=request.getServletPath();

      // profile permissions
      if (spath.indexOf("profilepermissions.")>=0) {

         mav.addObject("status",profilePermissions());
         return mav;

      }
      else if (spath.indexOf("viewprofileperm.")>=0) {

         List outlist=viewProfilePerm();
         mav.addObject("list",outlist);
         mav.addObject("status","VIEWPROFILEPERM");
         return mav;

      }
      else if (spath.indexOf("profilepermassign.")>=0) {

         mav.addObject("status",profilePermAssign());
         return mav;

      }
      else if (spath.indexOf("formpermissions.")>=0) {

         mav.addObject("status",formPermissions());
         return mav;

      }
      else if (spath.indexOf("viewformperm.")>=0) {

         mav.addObject("list",viewFormPerm());
         mav.addObject("status","VIEWFORMPERM");
         return mav;

      }
      else if (spath.indexOf("formpermassign.")>=0) {

         mav.addObject("status",formPermAssign());
         return mav;

      }

      // add new site interface
      else {
         out.println("BAD REQUEST! - " + spath);
         return null;
      }

   }

   // Manage profile permissions
   private String profilePermissions() {

      String status=null;
      long dtprofileid=new Long(getRequest().getParameter("dtprofileid")).longValue();

      Dtpermprofiles profile=(Dtpermprofiles)getMainDAO().execUniqueQuery(
            "select profile from Dtpermprofiles profile where profile.dtprofileid=" + dtprofileid
            );
      if (profile==null) {
         status="PROFILEPULLERROR";
         return status;
      }
      getMAV().addObject("profile",profile);

      // Pull role list and user list
      // Make sure role acronym is not duplicated witin level and site/study
      List l;
      long q_siteid;
      try {
         q_siteid=new Long(getAuth().getValue("siteid")).longValue();
      } catch (Exception sie) {
         q_siteid=new Long("-999999999").longValue();
      }
      long q_studyid;
      try {
         q_studyid=new Long(getAuth().getValue("studyid")).longValue();
      } catch (Exception sie) {
         q_studyid=new Long("-999999999").longValue();
      }
      l=getMainDAO().execQuery(
            "select rr from Resourceroles rr where " +
                   "(rr.rolelevel=:profilelevel) and (" + 
                      "(rr.rolelevel=" + VisibilityConstants.SYSTEM + ") or " +
                      "(rr.rolelevel=" + VisibilityConstants.SITE +
                          " and rr.siteid=:siteid) or " +
                      "(rr.rolelevel=" + VisibilityConstants.STUDY +
                          " and rr.studyid=:studyid) " +
                   ") " +
               "order by rr.rolelevel,rr.roleacr "    
             ,new String[] { "siteid","studyid","profilelevel" }
             ,new Object[] { q_siteid,q_studyid,profile.getProfilelevel() }   
         );

      // Create HashMap list (with rolelevel as a string) to pass to iframe
      ArrayList rolelist=new ArrayList();
      Iterator i=l.iterator();
      while (i.hasNext()) {
         HashMap map=new HashMap();
         Resourceroles r=(Resourceroles)i.next();
         String rolelevel=new VisibilityConstants().getFieldName(r.getRolelevel());
         map.put("rolelevel",rolelevel);
         map.put("role",r);
         rolelist.add(map);
      }
      ListResult result;
      result=new ListResult();
      result.setList(rolelist);
      result.setStatus("OK");
      this.getSession().setAttribute("iframeresult",result);

      // NO USER LIST FOR PROFILES

      status="PROFILEPERMISSIONS";
      return status;

   }

   // View Profile Permissions
   public List viewProfilePerm() {

      String dtprofileid=getRequest().getParameter("dtprofileid");

      // pull system/site/study permissions 
      List p=getMainDAO().execQuery(
         "select p from Permissions p join fetch p.permissioncats " +
            "where p.permissioncats.pcatvalue=" + PcatConstants.FORM_DATATABLE + " and " +
                  "p.permissioncats.pcatscope=" + PcatscopeConstants.RESOURCE 
            );
            
      // Holders for permissions      
      BitSet permset=null;
      List a=null;

      ResourcerolesDAO rdao=(ResourcerolesDAO)this.getContext().getBean("resourcerolesDAO");
      Resourceroles role=rdao.getRecord(new Long(request.getParameter("rroleid")).longValue());

      mav.addObject("dtprofileid",dtprofileid);
      mav.addObject("acrvar",role.getRoleacr());
      mav.addObject("idvar",role.getRroleid());
      mav.addObject("typevar","role");

      Dtrolepermprofassign assign=(Dtrolepermprofassign)getMainDAO().execUniqueQuery(
         "select p from Dtrolepermprofassign p where p.resourceroles.rroleid=:rroleid and " +
            "p.dtpermprofiles.dtprofileid=:dtprofileid "
         ,new String[] { "rroleid","dtprofileid" }
         ,new Object[] { new Long(request.getParameter("rroleid")),new Long(dtprofileid) }
         );   
      if (assign!=null) {
         permset=(BitSet)assign.getPermlevel();
      }

      // Use values from permission holders to set "checked" values in input fields
      Iterator ip=p.iterator();
      ArrayList newlist=new ArrayList();
      while (ip.hasNext()) {
         HashMap map=new HashMap();
         com.owarchitects.forms.commons.db.Permissions perm=(com.owarchitects.forms.commons.db.Permissions)ip.next();
         map.put("permobj",perm);
         int pcatvalue=perm.getPermissioncats().getPcatvalue();
         map.put("pcatvalue",new PcatConstants().getFieldName(pcatvalue));
         if (permset!=null && permset.get(perm.getPermpos()-1)) {
            map.put("permchecked","checked");
         } else {
            map.put("permchecked"," ");
         }   
         newlist.add(map);

      }
      return newlist;

   }


   // Assign Permissions
   public String profilePermAssign() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String dtprofileid=request.getParameter("dtprofileid");
      String typevar=request.getParameter("typevar");
      String idvar=request.getParameter("idvar");
      
      // make sure user has permission to change permission
      Integer profilelevel=(Integer)getMainDAO().execUniqueQuery(
            "select f.profilelevel from Dtpermprofiles f where f.dtprofileid=" + dtprofileid
         );
      if (profilelevel!=null && getPermHelper().hasGlobalPerm(getAuth().getValue("auserid"),profilelevel,"DTFORMPERMPROF")) {
         // do nothing, continue
      } else {
         // not permitted
         return "NOAUTH";
      }

      // return position parameters of set permissions for setting BitSet values
      Enumeration e=request.getParameterNames();
      ArrayList alist=new ArrayList();
      while (e.hasMoreElements()) {
         String pname=(String)e.nextElement();
         if (pname.substring(0,2).equalsIgnoreCase("r_")) {
            String pvalue=request.getParameter(pname);
            if (pvalue.equals("1")) {
               com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
                  "select p from Permissions p join fetch p.permissioncats " +
                     "where p.permid=:permid "
                  ,new String[] { "permid" }
                  ,new Object[] { new Long(pname.substring(2)) }
                  );
               alist.add(p);
            }
         }
      }

      ResourcerolesDAO rdao=(ResourcerolesDAO)getContext().getBean("resourcerolesDAO");
      Resourceroles role=rdao.getRecord(new Long(idvar).longValue());

      List l=getMainDAO().execQuery(
         "select p.permissioncats.pcatvalue,max(p.permpos) " +
            "from Permissions p " +
            "group by p.permissioncats.pcatvalue " 
            );
      // This having clause was throwing errors.  I think it's valid, so this
      // might be a bug.  We'll handle the subset later.
      //  + "having p.permissioncats.pcatvalue in ....."

      // Initialize BitSets
      BitSet permset=null;
      Iterator i=l.iterator();
      while (i.hasNext()) {
         Object[] oarray=(Object[])i.next();
         int setln=((Integer)oarray[1]).intValue();
         permset=new BitSet(setln);
      }
      i=alist.iterator();
      while (i.hasNext()) {
         com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)i.next();
         // set flagged permissions in bitset
         permset.set(p.getPermpos()-1,true);
      }
      if (permset!=null) {
         Dtrolepermprofassign frppa=(Dtrolepermprofassign)getMainDAO().execUniqueQuery(
            "select srp from Dtrolepermprofassign srp where srp.resourceroles.rroleid=:rroleid and " +
               "srp.dtpermprofiles.dtprofileid=:dtprofileid"
            ,new String[] { "rroleid","dtprofileid" }
            ,new Object[] { new Long(role.getRroleid()),new Long(dtprofileid) }
            );   
         if (frppa==null) {
            Dtpermprofiles profile=(Dtpermprofiles)getMainDAO().execUniqueQuery(
               "select p from Dtpermprofiles p where p.dtprofileid=" + dtprofileid
               );
            if (profile==null) return "PROFILEPULLERROR";
            frppa=new Dtrolepermprofassign();
            frppa.setResourceroles(role);
            frppa.setDtpermprofiles(profile);
         }
         frppa.setPermlevel(permset);
         getMainDAO().saveOrUpdate(frppa);
         FormMetaUtil.setLastupdatelocal(getMainDAO());
         return "PROFILEPERMASSIGNED";
      }
      return "PERMASSIGNERROR";

   }

   // Manage form permissions
   private String formPermissions() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String status=null;

      if (!new Boolean(getRequest().getParameter("islocal")).booleanValue()) {
         status="REMOTEFORM";
         return status;
      }

      long formtypeid=new Long(getRequest().getParameter("formtypeid")).longValue();

      Formtypes ftype=(Formtypes)getMainDAO().execUniqueQuery(
            "select ft from Formtypes ft join fetch ft.datatabledef where ft.formtypeid=" + formtypeid
            );
      if (ftype==null) {
         status="FORMPULLERROR";
         return status;
      }
      getMAV().addObject("ftype",ftype);

      Formha fha=(Formha)getMainDAO().execUniqueQuery(
         "select fha from Formha fha where fha.formhaid=" + ftype.getDatatabledef().getPformhaid()
         );
      if (fha==null) {
         status="FORMPULLERROR";
         return status;
      }

      // See if user has permission to move this form
      FORMSAuth auth=getAuth();
      if (!(ftype.getDatatabledef().getUploaduser()==new Long(auth.getValue("auserid")).longValue())) {
         // pull visibility level for form
         if (!getPermHelper().hasGlobalPerm(auth.getValue("auserid"),
             fha.getVisibility(),"DTFORMPERMOTH")) {
             return "NOAUTH";
         }
      }

      getMAV().addObject("fha",fha);

      List l;
      long q_siteid;
      try {
         q_siteid=new Long(getAuth().getValue("siteid")).longValue();
      } catch (Exception sie) {
         q_siteid=new Long("-999999999").longValue();
      }
      long q_studyid;
      try {
         q_studyid=new Long(getAuth().getValue("studyid")).longValue();
      } catch (Exception sie) {
         q_studyid=new Long("-999999999").longValue();
      }

      // Get list of relevant profiles
      l=getMainDAO().execQuery(
            "select p from Dtpermprofiles p where " +
                   "(p.profilelevel=:visibility) and (" + 
                      "(p.profilelevel=" + VisibilityConstants.SYSTEM + ") or " +
                      "(p.profilelevel=" + VisibilityConstants.SITE +
                          " and p.siteid=:siteid) or " +
                      "(p.profilelevel=" + VisibilityConstants.STUDY +
                          " and p.studyid=:studyid) " +
                   ") " +
               "order by p.profilelevel,p.profileacr "    
             ,new String[] { "siteid","studyid","visibility" }
             ,new Object[] { q_siteid,q_studyid,fha.getVisibility() }   
         );
      getMAV().addObject("profilelist",l);
      // create null profile, add to list
      Dtpermprofiles nullprofile=new Dtpermprofiles();
      nullprofile.setProfileacr("_NONE_");
      nullprofile.setProfiledesc("No profile assigned");
      l.add(0,nullprofile);


      // Get Form Permissions Profile Value
      Dtpermprofiles currprofile=(Dtpermprofiles)getMainDAO().execUniqueQuery(
         "select f.dtpermprofiles from Dtpermprofassign f where f.dtdefid=" + ftype.getDatatabledef().getDtdefid()
         );
      if (currprofile==null) currprofile=nullprofile;
      getMAV().addObject("currprofile",currprofile);

      HashMap pmap=new HashMap();
      pmap.put("currprofile",currprofile);
      pmap.put("profilelist",l);
      pmap.put("dtdefid",ftype.getDatatabledef().getDtdefid());
      this.getSession().setAttribute("iframe3result",pmap);

      // Pull role list and user list
      // Make sure role acronym is not duplicated witin level and site/study
      l=getMainDAO().execQuery(
            "select rr from Resourceroles rr where " +
                   "(rr.rolelevel=:visibility) and (" + 
                      "(rr.rolelevel=" + VisibilityConstants.SYSTEM + ") or " +
                      "(rr.rolelevel=" + VisibilityConstants.SITE +
                          " and rr.siteid=:siteid) or " +
                      "(rr.rolelevel=" + VisibilityConstants.STUDY +
                          " and rr.studyid=:studyid) " +
                   ") " +
               "order by rr.rolelevel,rr.roleacr "    
             ,new String[] { "siteid","studyid","visibility" }
             ,new Object[] { q_siteid,q_studyid,fha.getVisibility() }   
         );

      // Create HashMap list (with rolelevel as a string) to pass to iframe
      ArrayList rolelist=new ArrayList();
      Iterator i=l.iterator();
      while (i.hasNext()) {
         HashMap map=new HashMap();
         Resourceroles r=(Resourceroles)i.next();
         String rolelevel=new VisibilityConstants().getFieldName(r.getRolelevel());
         map.put("rolelevel",rolelevel);
         map.put("role",r);
         rolelist.add(map);
      }
      ListResult result;
      result=new ListResult();
      result.setList(rolelist);
      result.setStatus("OK");
      this.getSession().setAttribute("iframeresult",result);

      // PULL IN USER LIST
      AllusersDAO adao=(AllusersDAO)this.getContext().getBean("allusersDAO");      
      l=adao.getCurrentUsers();

      // Create typed list to pass to iframe
      ArrayList userlist=new ArrayList(Arrays.asList(l.toArray(new Allusers[0])));
      result=new ListResult();
      result.setList(userlist);
      result.setStatus("OK");
      this.getSession().setAttribute("iframe2result",result);

      status="FORMPERMISSIONS";
      return status;

   }

   // View Profile Permissions
   public List viewFormPerm() throws FORMSException, FORMSKeyException, FORMSSecurityException {

      String formtypeid=getRequest().getParameter("formtypeid");

      Formtypes ftype=(Formtypes)getMainDAO().execUniqueQuery(
            "select ft from Formtypes ft join fetch ft.datatabledef where ft.formtypeid=" + formtypeid
            );
      if (ftype==null) {
         return null;
      }
      getMAV().addObject("dtdefid",ftype.getDatatabledef().getDtdefid());

      // pull system/site/study permissions 
      List p=getMainDAO().execQuery(
         "select p from Permissions p join fetch p.permissioncats " +
            "where p.permissioncats.pcatvalue=" + PcatConstants.FORM_DATATABLE + " and " +
                  "p.permissioncats.pcatscope=" + PcatscopeConstants.RESOURCE 
            );
            
      // Holders for permissions      
      BitSet permset=null;
      List a=null;

      boolean haveperm=false;
      
      if (request.getParameter("rroleid")!=null) {

         ResourcerolesDAO rdao=(ResourcerolesDAO)this.getContext().getBean("resourcerolesDAO");
         Resourceroles role=rdao.getRecord(new Long(request.getParameter("rroleid")).longValue());
   
         mav.addObject("acrvar",role.getRoleacr());
         mav.addObject("idvar",role.getRroleid());
         mav.addObject("typevar","role");
   
         Dtrolepermassign assign=(Dtrolepermassign)getMainDAO().execUniqueQuery(
            "select p from Dtrolepermassign p where p.resourceroles.rroleid=:rroleid and " +
               "p.dtdefid=:dtdefid "
            ,new String[] { "rroleid","dtdefid" }
            ,new Object[] { new Long(request.getParameter("rroleid")),new Long(ftype.getDatatabledef().getDtdefid()) }
            );   
         if (assign!=null) {
            permset=(BitSet)assign.getPermlevel();
            haveperm=true;
         }

      } else {

         String auserid=getRequest().getParameter("auserid");

         AllusersDAO rdao=(AllusersDAO)this.getContext().getBean("allusersDAO");
         Allusers user=rdao.getRecord(new Long(auserid).longValue());
   
         mav.addObject("acrvar",user.getUserdesc());
         mav.addObject("idvar",user.getAuserid());
         mav.addObject("typevar","user");
   
         Dtuserpermassign assign=(Dtuserpermassign)getMainDAO().execUniqueQuery(
            "select p from Dtuserpermassign p where p.allusers.auserid=:auserid and " +
               "p.dtdefid=:dtdefid "
            ,new String[] { "auserid","dtdefid" }
            ,new Object[] { new Long(auserid),new Long(ftype.getDatatabledef().getDtdefid()) }
            );   
         if (assign!=null) {
            permset=(BitSet)assign.getPermlevel();
            haveperm=true;
         }


      }
      getMAV().addObject("haveperm",new Boolean(haveperm));

      // Use values from permission holders to set "checked" values in input fields
      Iterator ip=p.iterator();
      ArrayList newlist=new ArrayList();
      while (ip.hasNext()) {
         HashMap map=new HashMap();
         com.owarchitects.forms.commons.db.Permissions perm=(com.owarchitects.forms.commons.db.Permissions)ip.next();
         map.put("permobj",perm);
         int pcatvalue=perm.getPermissioncats().getPcatvalue();
         map.put("pcatvalue",new PcatConstants().getFieldName(pcatvalue));
         if (permset!=null && permset.get(perm.getPermpos()-1)) {
            map.put("permchecked","checked");
         } else {
            map.put("permchecked"," ");
         }   
         newlist.add(map);

      }
      return newlist;

   }


   // Assign Permissions
   public String formPermAssign() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String dtdefid=request.getParameter("dtdefid");
      String typevar=request.getParameter("typevar");
      String idvar=request.getParameter("idvar");
      String clearall=request.getParameter("clearall");
      if (clearall==null) clearall="";
      // return position parameters of set permissions for setting BitSet values
      Enumeration e=request.getParameterNames();
      ArrayList alist=new ArrayList();
      while (e.hasMoreElements()) {
         String pname=(String)e.nextElement();
         if (pname.substring(0,2).equalsIgnoreCase("r_")) {
            String pvalue=request.getParameter(pname);
            if (pvalue.equals("1")) {
               com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
                  "select p from Permissions p join fetch p.permissioncats " +
                     "where p.permid=:permid "
                  ,new String[] { "permid" }
                  ,new Object[] { new Long(pname.substring(2)) }
                  );
               alist.add(p);
            }
         }
      }

      List l=getMainDAO().execQuery(
         "select p.permissioncats.pcatvalue,max(p.permpos) " +
            "from Permissions p " +
            "group by p.permissioncats.pcatvalue " 
            );
      // This having clause was throwing errors.  I think it's valid, so this
      // might be a bug.  We'll handle the subset later.
      //  + "having p.permissioncats.pcatvalue in ....."
      
   
      // Initialize BitSets
      BitSet permset=null;
      Iterator i=l.iterator();
      while (i.hasNext()) {
         Object[] oarray=(Object[])i.next();
         int setln=((Integer)oarray[1]).intValue();
         permset=new BitSet(setln);
      }
      i=alist.iterator();
      while (i.hasNext()) {
         com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)i.next();
         // set flagged permissions in bitset
         permset.set(p.getPermpos()-1,true);
      }
      if (permset!=null) {

         if (typevar.equalsIgnoreCase("role")) {

            ResourcerolesDAO rdao=(ResourcerolesDAO)getContext().getBean("resourcerolesDAO");
            Resourceroles role=rdao.getRecord(new Long(idvar).longValue());
            Dtrolepermassign frpa=(Dtrolepermassign)getMainDAO().execUniqueQuery(
               "select srp from Dtrolepermassign srp where srp.resourceroles.rroleid=:rroleid and " +
                  "srp.dtdefid=:dtdefid"
               ,new String[] { "rroleid","dtdefid" }
               ,new Object[] { new Long(role.getRroleid()),new Long(dtdefid) }
               );   
            if (frpa==null) {
               if (clearall.equals("1")) {
                  return "NOMESSAGE";
               }
               frpa=new Dtrolepermassign();
               frpa.setResourceroles(role);
               frpa.setDtdefid(new Long(dtdefid).longValue());
            } else if (clearall.equals("1")) {
               getMainDAO().delete(frpa);
               FormMetaUtil.setLastupdatelocal(getMainDAO());
               return "FORMPERMCLEARED";
            }
            frpa.setPermlevel(permset);
            getMainDAO().saveOrUpdate(frpa);
            FormMetaUtil.setLastupdatelocal(getMainDAO());
            return "FORMPERMASSIGNED";
         }
         else if (typevar.equalsIgnoreCase("user")) {

            AllusersDAO adao=(AllusersDAO)getContext().getBean("allusersDAO");
            Allusers user=adao.getRecord(new Long(idvar).longValue());
            Dtuserpermassign frpa=(Dtuserpermassign)getMainDAO().execUniqueQuery(
               "select srp from Dtuserpermassign srp where srp.allusers.auserid=:auserid and " +
                  "srp.dtdefid=:dtdefid"
               ,new String[] { "auserid","dtdefid" }
               ,new Object[] { new Long(user.getAuserid()),new Long(dtdefid) }
               );   
            if (frpa==null) {
               if (clearall.equals("1")) {
                  return "NOMESSAGE";
               }
               frpa=new Dtuserpermassign();
               frpa.setAllusers(user);
               frpa.setDtdefid(new Long(dtdefid).longValue());
            } else if (clearall.equals("1")) {
               getMainDAO().delete(frpa);
               FormMetaUtil.setLastupdatelocal(getMainDAO());
               return "FORMPERMCLEARED";
            }
            frpa.setPermlevel(permset);
            getMainDAO().saveOrUpdate(frpa);
            FormMetaUtil.setLastupdatelocal(getMainDAO());
            return "FORMPERMASSIGNED";

         }
      }
      return "PERMASSIGNERROR";

   }


}


