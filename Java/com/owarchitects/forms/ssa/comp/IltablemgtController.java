 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * IltablemgtController.java - Invalid Login Table Management Interface
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import javax.servlet.http.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;

public class IltablemgtController extends FORMSController {

   private boolean showsite;

   public ModelAndView submitRequest() throws FORMSException {

      try {

         FORMSAuth auth=this.getAuth();

         ModelAndView mav=new ModelAndView();
         HttpServletResponse response=this.getResponse();
         PrintWriter out=this.getWriter();

         // check permissions
         if (!hasGlobalPerm(PcatConstants.SYSTEM,"INVALIDMGT")) {
            safeRedirect("permission.err");
            return null;
         }

         String oldsortcol=auth.getValue("ilsortcolumn");
         String oldsortdir=auth.getValue("ilsortdir");
         if (oldsortcol==null || oldsortcol.trim().equals("")) {
             auth.setValue("ilsortcolumn","prevtime");
             auth.setValue("ilsortdir","desc");
         }

         String sortcolumn=request.getParameter("SORTCOLUMN");

         if (sortcolumn==null) {

            // Process Data
            IltablemgtResult result=this.processData();
   
            this.getSession().setAttribute("iframeresult",result);
   
            // Direct output based on result
            if (result.getStatus().equalsIgnoreCase("OK")) {
   
               //result.addToMAV(mav,new String[] { "list","status" });
               result.addToMAV(mav,new String[] { "status" });
               return mav;
      
            } 
      
         }
         else if (sortcolumn!=null) {

            mav.addObject("status","SORT");
            if (oldsortcol.equalsIgnoreCase(sortcolumn)) {
               if (oldsortdir.equalsIgnoreCase("asc")) {
                  auth.setValue("ilsortdir","desc");
               } else {
                  auth.setValue("ilsortdir","asc");
               }
            } else {
               auth.setValue("ilsortcolumn",sortcolumn);
               auth.setValue("ilsortdir","asc");
            }

            mav.addObject("status","SORT");
            return mav;

         }
         return null;
   
      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   private IltablemgtResult processData() throws FORMSException {

      IltablemgtResult result=new IltablemgtResult();

      try {

        FORMSAuth auth=this.getAuth();

        List l=getEmbeddedDAO().execQuery(" select il from Invalidlogins as il order by " +
                    auth.getValue("ilsortcolumn") + " " + auth.getValue("ilsortdir") + ",ipaddr" );

        ArrayList<Map> passList=new ArrayList<Map>();
        
        Iterator i=l.iterator();
        String showreset="N";
        while (i.hasNext()) {
           Map<String,String> map = new HashMap<String,String>();
           // NOTE:  May retrieve either studies or studyrights object
           Object o=i.next();
           try {
              Invalidlogins sd=(Invalidlogins)o;
              map.put("ipaddr",sd.getIpaddr());
              map.put("hostname",sd.getHostname());
              map.put("currcount",new Long(sd.getCurrcount()).toString());
              map.put("totcount",new Long(sd.getTotcount()).toString());
              map.put("prevuname",sd.getPrevuname());
              map.put("prevtime",sd.getPrevtime().toString().substring(0,sd.getPrevtime().toString().indexOf(".")));
              passList.add(map);
              if (sd.getCurrcount()>0) {
                 showreset="Y";
              }
           } catch (Exception e) { }
        }   

        result.setStatus("OK");
        result.setShowreset(showreset);
        result.setList(passList);

        return result;

      } catch (Exception e) {

        throw new FORMSException("FORMS Service exception:  " + Stack2string.getString(e));

      }

   }
   
}


