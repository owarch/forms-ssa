 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.io.*;
import java.util.*;
import java.text.*;
//import org.apache.regexp.RE;
//import org.apache.commons.lang.StringUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils.*;
//import java.util.regex.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import java.security.spec.*;
import uk.ltd.getahead.dwr.WebContext;
import uk.ltd.getahead.dwr.WebContextFactory;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.velocity.app.*;
import org.apache.velocity.*;
import org.apache.commons.httpclient.util.URIUtil;


public class ProcessFormDwr extends FORMSSsaServiceClientDwr {

   static Integer LOCALSUBMIT=1;
   static Integer REMOTESUBMIT=2;

   private ArrayList keyvarlist;
   private ArrayList idvarlist;
   private FormDataParser parser;
   private String trString;
   private Long dtdefid;
   private Long auserid;
   private Linkedinst submitinst;
   private Integer submittype;

   // Main record submit function
   public String submitRequest(String inString) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {

         FORMSAuth auth=this.getAuth();

         String rtnval="";

         // perform initial transformations of form output data
         trString=FormDataUtil.transformDataStream(inString);
   
         // GRAB dtdefid
         dtdefid=new Long(FormDataUtil.getFieldValue(trString,"dtdefid_"));

         try {
            auserid=new Long(auth.getValue("auserid"));
         } catch (Exception aue) {
            auserid=new Long(-999999);
         }

   	 // Create RMS Temporary Record

         Long currtime=new Long(System.currentTimeMillis());
   
         String tempFileName = "t_" + dtdefid  + "_" + auserid  + "_" + 
            currtime + ".xml.encrypt";

         MiscUtil.writeStringToFile(this.getSyswork().toString() + File.separator + tempFileName,
                 EncryptUtil.encryptString(trString,SessionObjectUtil.getInternalKeyObj(getSession()))
            );

         getAuth().setObjectValue("submitinst",null);
         // If form is a remote installation form, submit to remote inst   
         Long ainstid=(Long)getMainDAO().execUniqueQuery(
            "select f.allinst.ainstid from Datatabledef f " +
               "where f.dtdefid=" + dtdefid +  "and f.allinst.islocal!=true"
               );
         if (ainstid!=null) {
            //
            // remote submission
            //
            getAuth().setObjectValue("submittype",REMOTESUBMIT);
            try {
               // pull linkedinst record for submission
               Linkedinst submitinst=(Linkedinst)getMainDAO().execUniqueQuery("select a.linkedinst from Allinst a where a.ainstid=" + ainstid);
               // Send records to service target
               SsaFormRecordProcessorParms parms=new SsaFormRecordProcessorParms();
               parms.setDatastring(trString);
               parms.setDtdefid(getRemotedtdefid());
               parms.setBeanname("formRecordProcessorServiceTarget");
               parms.setMethodname("submitRecord");
      
               FormRecordProcessorResult result=(FormRecordProcessorResult)this.submitServiceRequest(submitinst,parms);
               int transferstatus=result.getTransferstatus();
               
               if (transferstatus==FormRecordProcessorConstants.RECORD_LOCKED) {

                  // Save to sync directory
                  return writeSyncRecord();
      
               } else if (transferstatus==FormRecordProcessorConstants.ERROR) {

                  // Save to sync directory
                  return writeSyncRecord();
      
               }

               String fieldstr=new FormRecordProcessorConstants().getFieldName(transferstatus);
               boolean setvals=false;
               if (fieldstr.equals("ALREADY_EXISTS")) {
                  fieldstr="ALREADY:::" + result.getInfostring();
                  setvals=true;
               } else if (fieldstr.equals("ALREADY_NOTPERMITTED")) {
                  fieldstr="ALREADY_NOTPERMITTED:::" + result.getInfostring();
                  setvals=true;
               } else if (fieldstr.equals("NOENROLLMENT")) {
                  fieldstr="NOENROLLMENT:::" + result.getInfostring();
                  setvals=true;
               } else if (fieldstr.equals("ECODEPROB")) {
                  fieldstr="ECODEPROB:::" + result.getInfostring();
                  setvals=true;
               } else if (fieldstr.equals("NOTPERMITTED")) {
                  fieldstr="NOTPERMITTED:::" + result.getInfostring();
                  setvals=true;
               } 
               if (setvals) {   
                  getAuth().setObjectValue("submitinst",submitinst);
                  getAuth().setObjectValue("trstring",trString);
                  getAuth().setObjectValue("dtdefid_",dtdefid);
                  getAuth().setObjectValue("auserid_",auserid);
               }
               return fieldstr;
            } catch (Exception sfex) {

               // Save to sync directory
               return writeSyncRecord();
            }
         } 
         getAuth().setObjectValue("submittype",LOCALSUBMIT);

         // Continue with local submission

         // GRAB KEY FIELD VALUES IF APPLICABLE  

         List fkl=getMainDAO().execQuery(
            "select f from Datatablecoldef f join fetch f.datatabledef where f.datatabledef.dtdefid=" + dtdefid +
                " and f.iskeyfield=true " +
                "order by f.columnorder"
            );

         List ecl=getMainDAO().execQuery(
            "select f from Datatablecoldef f join fetch f.datatabledef where f.datatabledef.dtdefid=" + dtdefid +
                " and f.isecode=true " +
                "order by f.columnorder"
            );

         String ecodeVar=null;
         long ecodeVal=-1;
         if (ecl.size()>0) {
            try {
               ecodeVar=((Datatablecoldef)ecl.get(0)).getColumnname();
               ecodeVal=new Long(FormDataUtil.getFieldValue(trString,ecodeVar)).longValue();


            } catch (Exception e) {
               ecodeVar=null;
            }
         }

         Iterator fki=fkl.iterator();
         keyvarlist=new ArrayList();
         idvarlist=new ArrayList();
         // Create input string for ecode value comparison
         StringBuilder ecodesb=new StringBuilder();
         while (fki.hasNext()) {
            Datatablecoldef fcd=(Datatablecoldef)fki.next();
            String kvname=fcd.getColumnname().toLowerCase();
            String kvvalue=FormDataUtil.getFieldValue(trString,kvname);
            HashMap keyvarmap=new HashMap();
            keyvarmap.put("keyvar",kvname.toUpperCase());
            keyvarmap.put("keyval",kvvalue);
            keyvarlist.add(keyvarmap);
            if (fcd.getIsidfield()) {
               idvarlist.add(keyvarmap);
               ecodesb.append(kvvalue + "-");
            }
         }
         if (ecodesb.indexOf("-")>0) {
            ecodesb.deleteCharAt(ecodesb.lastIndexOf("-"));
         } 

         // Check ecode
         if (ecodeVar!=null) {
            if (new Long(EcodeUtil.getEcode(ecodesb.toString())).longValue()!=ecodeVal) {
                rtnval="ECODEPROB:::" + ecodesb.toString();
               return rtnval;
               
            }
         }

         // Pull user permissions
         PermHelper helper=getPermHelper();
         boolean hasperm_newrecord=false;
         boolean hasperm_editown=false;
         boolean hasperm_editother=false;
         try {
            hasperm_newrecord=helper.hasFormPerm(dtdefid.toString(),"NEWRECORD");
            hasperm_editown=helper.hasFormPerm(dtdefid.toString(),"EDITOWN");
            hasperm_editother=helper.hasFormPerm(dtdefid.toString(),"EDITOTHER");
         } catch (Exception permex) {
            // do nothing, no permission
         }

         // Create new formdataparser, creating backup javascript and arraylist containing
         // field names & values

         parser=new FormDataParser(trString);

         auth.setValue("backupstring",parser.getBackupJs());

         if (!(hasperm_newrecord || hasperm_editown || hasperm_editother)) {
            rtnval="NOTPERMITTED::: ";
            return rtnval;
         }

         // 
         // See if record already exists for key value combo
         // 

         int keymatch=keyvarlist.size();
         if (keymatch>0) {

            // create keyvar component of whereclause && make sure values exist for all
            // key fields
            StringBuilder sbwhere=new StringBuilder();
            StringBuilder sbinfo=new StringBuilder();
            Iterator i=keyvarlist.iterator();
            boolean hasallkeyvalues=true;
            while (i.hasNext()) {
               HashMap map=(HashMap)i.next();
               String keyvar=(String)map.get("keyvar");
               String keyval=(String)map.get("keyval");
               if (keyval==null || keyval.trim().length()<1) {
                  hasallkeyvalues=false;
               }
               sbwhere.append(" (fdv.varname='" + keyvar + "' and " +
                                "fdv.varvalue='" + keyval + "') ");
               sbinfo.append(keyvar + "=" + keyval);
               if (i.hasNext()) {
                  sbwhere.append(" or ");            
                  sbinfo.append(",&nbsp;");            
               }   
            }

            // Flag an error if some key fields are missing
            if (!hasallkeyvalues) {
               rtnval="NOKEYVAL:::" + sbinfo.toString() ;
               return rtnval;
            }

            // get count of matching key values by DataRecordId
            List l=getMainDAO().execQuery(
               "select fdv.datatablerecords.datarecordid,count(fdv.datatablerecords.datarecordid) from " +
                  "Datatablevalues fdv " +
                  "inner join fdv.datatablerecords " +
                  "inner join fdv.datatablerecords.datatabledef " +
                  "where (fdv.datatablerecords.datatabledef.dtdefid=" + dtdefid + ") and " +
                     "(fdv.datatablerecords.ishold!=true) and " +
                     "(fdv.datatablerecords.isaudit!=true) and " +
                     "(" + sbwhere.toString() + ")" +
                  "group by fdv.datatablerecords.datarecordid"
                );

            // If count>=number of keyvars, there is a match (NOTE: should never be greater)
            if (l.size()>0) {

               Iterator aiter=l.iterator();
               while (aiter.hasNext()) {
                  Object[] aarray=(Object[])aiter.next();
                  Long datarecordid=(Long)aarray[0];
                  Long nmatch=(Long)aarray[1];
                  if (nmatch.longValue()>=keyvarlist.size()) {
                     // Save record objects for retrieval
                     auth.setObjectValue("keyvarlist",keyvarlist);
                     auth.setObjectValue("formdataparser",parser);
                     auth.setObjectValue("dtdefid_",dtdefid);
                     auth.setObjectValue("auserid_",auserid);
                     auth.setObjectValue("datarecordid_",datarecordid);
                     if (recordIsLocked()) {
                        rtnval="LOCKED:::" + sbinfo.toString() ;
                        writeSyncRecord();
                     } else {
                        // If record exists, make sure user has permission to overwrite it
                        Datatablerecords rec=(Datatablerecords)getMainDAO().execUniqueQuery(
                           "select f from Datatablerecords f where f.datarecordid=" + datarecordid
                           );
                        boolean permitted=true;   
                        if (rec!=null && (rec.getSaveuser()==new Long(auth.getValue("auserid")).longValue())) {
                           if (!hasperm_editown) permitted=false;
                        }
                        if (rec!=null && (rec.getSaveuser()!=new Long(auth.getValue("auserid")).longValue())) {
                           if (!hasperm_editother) permitted=false;
                        }
                        if (permitted) rtnval="ALREADY:::" + sbinfo.toString() ;
                        else rtnval="ALREADY_NOTPERMITTED:::" + sbinfo.toString() ;
                     }
                     return rtnval;
                  }
               }

            }

            // Check against enrollment form if requested
            String checkenroll=FormDataUtil.getFieldValue(trString,"checkenrollform_");
            if (checkenroll.equals("Y")) {

               // pull ID field(s) from earlier keyfieldlist
               ArrayList idlist=new ArrayList();
               fki=fkl.iterator();
               StringBuilder fsb=new StringBuilder();
               while (fki.hasNext()) {
                  Datatablecoldef f=(Datatablecoldef)fki.next();
                  if (f.getIsidfield()) {
                     idlist.add(f);
                     fsb.append(f.getColumnname() + ',');
                  }
               }
               fsb.deleteCharAt(fsb.lastIndexOf(","));

               Datatabledef cdatatabledef=(Datatabledef)getMainDAO().execUniqueQuery("select f from Datatabledef f where f.dtdefid=" + dtdefid);

               // return study visibility heirarchy records create a stringbuffer containing ids
               List fhatemp=getMainDAO().execQuery(
                  "select fha from Formha fha " +
                        "where fha.visibility=" + VisibilityConstants.STUDY + " and fha.studies.studyid=" + auth.getValue("studyid") 
                  );

               i=fhatemp.iterator();
               StringBuilder sb=new StringBuilder();
               boolean vismatch=false;
               while (i.hasNext()) {
                  Formha ha=(Formha)i.next();
                  // write id to stringbuffer
                  sb.append(new Long(ha.getFormhaid()).toString());
                  if (i.hasNext()) sb.append(",");
                  // make sure that current form has study visibility
                  if (cdatatabledef.getPformhaid()==ha.getFormhaid()) {
                     vismatch=true;
                  }
               }

               // make sure current form has all ids assigned and create a where clause
               i=idvarlist.iterator();
               hasallkeyvalues=true;
               StringBuilder sbinfo2=new StringBuilder();
               StringBuilder sbwhere2=new StringBuilder();
               while (i.hasNext()) {
                  HashMap map=(HashMap)i.next();
                  String keyvar=(String)map.get("keyvar");
                  String keyval=(String)map.get("keyval");
                  if (keyval==null || keyval.trim().length()<1) {
                     hasallkeyvalues=false;
                  }
                  sbwhere2.append(" (fdv.varname='" + keyvar + "' and " +
                                   "fdv.varvalue='" + keyval + "') ");
                  sbinfo2.append(keyvar + "=" + keyval);
                  if (i.hasNext()) {
                     sbwhere2.append(" or ");            
                     sbinfo2.append(",&nbsp;");            
                  }   
               }

               boolean hasmatch=true;

               // check all enrollment forms for matching id fields and values
               if (vismatch && hasallkeyvalues) {
                  hasmatch=false;
                  List fdefl=getMainDAO().execQuery(
                     "select f from Datatabledef f " +
                        " where f.isenrollform=true  and f.pformhaid in (" + sb.toString() + ") and f.dtdefid!=" + dtdefid
                        );
                  Iterator fdefi=fdefl.iterator();
                  while (fdefi.hasNext()) {
                     Datatabledef fdef=(Datatabledef)fdefi.next();
                     l=getMainDAO().execQuery(
                        "select fdv.datatablerecords.datarecordid,count(fdv.datatablerecords.datarecordid) from " +
                           "Datatablevalues fdv " +
                           "inner join fdv.datatablerecords " +
                           "inner join fdv.datatablerecords.datatabledef " +
                           "where (fdv.datatablerecords.datatabledef.dtdefid=" + fdef.getDtdefid() + ") and " +
                              "(fdv.datatablerecords.ishold!=true) and " +
                              "(fdv.datatablerecords.isaudit!=true) and " +
                              "(" + sbwhere2.toString() + ")" +
                           "group by fdv.datatablerecords.datarecordid"
                         );
                     i=l.iterator();
                     Iterator aiter=l.iterator();
                     while (aiter.hasNext()) {
                        Object[] aarray=(Object[])aiter.next();
                        Long nmatch=(Long)aarray[1];
                        if (nmatch.intValue()==idvarlist.size()) {
                           hasmatch=true;    
                           break;
                        }   
                     }   
                     if (hasmatch) break;
                  }
               }

               // if no matching enrollment records return 
               if (!hasmatch) {
                  // Save record objects for retrieval
                  auth.setObjectValue("keyvarlist",keyvarlist);
                  auth.setObjectValue("formdataparser",parser);
                  auth.setObjectValue("dtdefid_",dtdefid);
                  auth.setObjectValue("auserid_",auserid);
                  rtnval="NOENROLLMENT:::" + sbinfo2.toString() ;
                  return rtnval;
               }   

            }

         }

         // Append new record to database
         appendRecord(dtdefid,parser);
         rtnval="APPENDED";
         return rtnval;

      } catch (Exception e) {

         return "BADERROR";

      }
      
   }

   // see if current record is locked by another user
   private boolean recordIsLocked() {
      try {
         Long datarecordid=(Long)getAuth().getObjectValue("datarecordid_");
         Long auserid=(Long)getAuth().getObjectValue("auserid_");
         Datatablerecords f=(Datatablerecords)getMainDAO().execUniqueQuery(
            "select f from Datatablerecords f where f.datarecordid=" + datarecordid + " and f.islocked=true and f.lockuser!=" + auserid
            );
         if (f!=null) {
            return true;
         }
      } catch (Exception e) { }
      return false;
   }

   // Replace existing record 
   public String submitReplace() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {
      
         // Retrieve record values
         retrieveAuthValues();

         if (submittype.equals(REMOTESUBMIT)) {
            try {
               // Send records to service target
               SsaFormRecordProcessorParms parms=new SsaFormRecordProcessorParms();
               parms.setDatastring(trString);
               parms.setDtdefid(getRemotedtdefid());
               parms.setBeanname("formRecordProcessorServiceTarget");
               parms.setMethodname("submitReplace");
      
               FormRecordProcessorResult result=(FormRecordProcessorResult)this.submitServiceRequest(submitinst,parms);
               int transferstatus=result.getTransferstatus();
               
               if (transferstatus==FormRecordProcessorConstants.RECORD_LOCKED) {

                  // Save to sync directory
                  writeSyncRecord();
                  return "LOCKED::::";
      
               } else if (transferstatus==FormRecordProcessorConstants.ERROR) {

                  // Save to sync directory
                  return writeSyncRecord();
      
               }
               
               String fieldstr=new FormRecordProcessorConstants().getFieldName(transferstatus);
               return fieldstr;

            } catch (Exception srex) {

               // Save to sync directory
               return writeSyncRecord();
            }
         }

         // Local Submit
         // create keyvar component of whereclause
         StringBuilder sbwhere=new StringBuilder();
         StringBuilder sbinfo=new StringBuilder();
         Iterator i=keyvarlist.iterator();
         while (i.hasNext()) {
            HashMap map=(HashMap)i.next();
            String keyvar=(String)map.get("keyvar");
            String keyval=(String)map.get("keyval");
            sbwhere.append(" (fdv.varname='" + keyvar + "' and " +
                             "fdv.varvalue='" + keyval + "') ");
            sbinfo.append(keyvar + "=" + keyval);
            if (i.hasNext()) {
               sbwhere.append(" or ");            
               sbinfo.append(",&nbsp;");            
            }   
         }
   
         // Find matching record
         List l=getMainDAO().execQuery(
            "select fdv.datatablerecords.datarecordid,count(fdv.datatablerecords.datarecordid) from " +
               "Datatablevalues fdv " +
               "inner join fdv.datatablerecords " +
               "inner join fdv.datatablerecords.datatabledef " +
               "where (fdv.datatablerecords.datatabledef.dtdefid=" + dtdefid + ") and " +
                  "(fdv.datatablerecords.ishold!=true) and " +
                  "(fdv.datatablerecords.isaudit!=true) and " +
                  "(" + sbwhere.toString() + ")" +
               "group by fdv.datatablerecords.datarecordid"
             );
   
         // If count>=number of keyvars, there is a match (NOTE: should never be greater)
         if (l.size()>0) {
            Iterator aiter=l.iterator();
            while (aiter.hasNext()) {
               Object[] aarray=(Object[])aiter.next();
               Long datarecordid=(Long)aarray[0];
               Long nmatch=(Long)aarray[1];
               if (nmatch.longValue()>=keyvarlist.size()) {
                  // Set record object to replaced
                  Datatablerecords fdr=(Datatablerecords)getMainDAO().execUniqueQuery(
                     "select f from Datatablerecords f where f.datarecordid=" + datarecordid
                     );
   
                  if (fdr!=null) {
                     fdr.setIsaudit(true);
                     fdr.setAudittype('R');
                     fdr.setAudittime(new Date());
                     fdr.setAudituser(auserid.longValue());
                     getMainDAO().saveOrUpdate(fdr,false);
                  }
                  // Append replacement record
                  appendRecord(dtdefid,parser);
                  clearAuthValues();
                  return "REPLACED";
               }
   
            }
         }
         return "NORECMATCH";

      } catch (Exception e) {

         return "ERROR";

      }

   }

   // Submit record to hold database
   public String submitHold() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      
      try {

         retrieveAuthValues();

         if (submittype.equals(REMOTESUBMIT)) {
            try {
               // Send records to service target
               SsaFormRecordProcessorParms parms=new SsaFormRecordProcessorParms();
               parms.setDatastring(trString);
               parms.setDtdefid(getRemotedtdefid());
               parms.setBeanname("formRecordProcessorServiceTarget");
               parms.setMethodname("submitHold");
      
               FormRecordProcessorResult result=(FormRecordProcessorResult)this.submitServiceRequest(submitinst,parms);
               int transferstatus=result.getTransferstatus();
               
               if (transferstatus==FormRecordProcessorConstants.ERROR) {

                  // Save to sync directory
                  return writeSyncRecord();
      
               }
               
               String fieldstr=new FormRecordProcessorConstants().getFieldName(transferstatus);
               return fieldstr;

            } catch (Exception shex) {
               // Save to sync directory
               return writeSyncRecord();
            }
         }

         // Local Submit
         // Append replacement record
         appendRecord(dtdefid,parser,true);
         clearAuthValues();
         return "HOLDAPPEND";

      } catch (Exception e) {

         return "ERROR";

      }

   }

   // Submit anyway (even though no enrollment record)
   public String submitAnyway() throws FORMSException,FORMSKeyException,FORMSSecurityException {
      
      try {

         retrieveAuthValues();

         if (submittype.equals(REMOTESUBMIT)) {
            try {
               // Send records to service target
               SsaFormRecordProcessorParms parms=new SsaFormRecordProcessorParms();
               parms.setDatastring(trString);
               parms.setDtdefid(getRemotedtdefid());
               parms.setBeanname("formRecordProcessorServiceTarget");
               parms.setMethodname("submitAnyway");
      
               FormRecordProcessorResult result=(FormRecordProcessorResult)this.submitServiceRequest(submitinst,parms);
               int transferstatus=result.getTransferstatus();
               
               if (transferstatus==FormRecordProcessorConstants.ERROR) {

                  // Save to sync directory
                  return writeSyncRecord();
      
               }
               
               String fieldstr=new FormRecordProcessorConstants().getFieldName(transferstatus);
               return fieldstr;

            } catch (Exception saex) {
               // Save to sync directory
               return writeSyncRecord();
            }
         }

         // Local Submit
         // Append replacement record
         appendRecord(dtdefid,parser);
         clearAuthValues();
         return "APPENDED";

      } catch (Exception e) {

         return "ERROR";

      }

   }


   private String writeSyncRecord() {

      try {
         Long currtime=new Long(System.currentTimeMillis());
         String syncFilePath = getSession().getServletContext().getRealPath("/") + File.separator + 
                "SyncFiles" + File.separator + "s_" + dtdefid  + "_" + auserid + "_" + 
                currtime + ".xml.encrypt";
         String trencryptString=EncryptUtil.encryptString(trString,SessionObjectUtil.getInternalKeyObj(getSession()));
         MiscUtil.writeStringToFile(syncFilePath,trencryptString);
   
         if (new File(syncFilePath).exists()) {
            return "SAVEDSYNC";
         } else {
            return "SYNCERROR";
         }

      } catch (Exception e) {
         return "SYNCERROR";
      }

   } 

   private long getRemotedtdefid() {

      return ((Long)getMainDAO().execUniqueQuery("select f.remotedtdefid from Datatabledef f where f.dtdefid=" + dtdefid)).longValue();

   }

   // append record to database
   private void appendRecord(Long dtdefid,FormDataParser parser,boolean ishold) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Datatabledef datatabledef=(Datatabledef)getMainDAO().execUniqueQuery(
         "select f from Datatabledef f where f.dtdefid=" + dtdefid
         );

      Datatablerecords newrecord=new Datatablerecords();
      newrecord.setDatatabledef(datatabledef);
      newrecord.setIshold(ishold);
      newrecord.setIsaudit(false);
      newrecord.setIslocked(false);
      newrecord.setSavetime(new Date());
      newrecord.setSaveuser(auserid.longValue());
      getMainDAO().save(newrecord,false);

      // Append field records

      List fieldlist=parser.getFieldList();
      Iterator fiter=fieldlist.iterator();
      while (fiter.hasNext()) {
         HashMap fmap=(HashMap)fiter.next();
         Datatablevalues newvalue=new Datatablevalues();
         newvalue.setDatatablerecords(newrecord);
         newvalue.setVarname(((String)fmap.get("fieldname")).toUpperCase());
         newvalue.setVarvalue((String)fmap.get("fieldvalue"));
         getMainDAO().save(newvalue,false);
      }

   }

   // append record to database
   private void appendRecord(Long dtdefid,FormDataParser parser) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      appendRecord(dtdefid,parser,false);
   }

   private void retrieveAuthValues() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=this.getAuth();
      dtdefid=(Long)auth.getObjectValue("dtdefid_");
      auserid=(Long)auth.getObjectValue("auserid_");
      trString=(String)auth.getObjectValue("trstring");
      parser=(FormDataParser)auth.getObjectValue("formdataparser");
      keyvarlist=(ArrayList)auth.getObjectValue("keyvarlist");
      submitinst=(Linkedinst)auth.getObjectValue("submitinst");
      submittype=(Integer)auth.getObjectValue("submittype");

   } 

   private void clearAuthValues() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FORMSAuth auth=this.getAuth();
      auth.setObjectValue("dtdefid_",null);
      auth.setObjectValue("auserid_",null);
      auth.setObjectValue("trstring",null);
      auth.setObjectValue("formdataparser",null);
      auth.setObjectValue("keyvarlist",null);
      auth.setObjectValue("submitinst",null);
      auth.setObjectValue("submittype",null);

   } 


   // Perform interim save
   public String interimSave(String inString) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {

         FORMSAuth auth=this.getAuth();

         String rtnval="";

         // perform initial transformations of form output data
         trString=FormDataUtil.transformDataStream(inString);
   
         // GRAB dtdefid
         dtdefid=new Long(FormDataUtil.getFieldValue(trString,"dtdefid_"));

         try {
            auserid=new Long(auth.getValue("auserid"));
         } catch (Exception aue) {
            auserid=new Long(-999999);
         }

   	 // Create RMS Temporary Record

         Long currtime=new Long(System.currentTimeMillis());
   
         String tempFileName = "i_" + dtdefid  + "_" + auserid  + "_" + 
            currtime + ".xml.encrypt";

         MiscUtil.writeStringToFile(this.getSyswork().toString() + File.separator + tempFileName,
                 EncryptUtil.encryptString(trString,SessionObjectUtil.getInternalKeyObj(getSession()))
            );

         rtnval="INTERIMSAVEOK";
         return rtnval;

      } catch (Exception e) {

         return "BADERROR";

      }
      
   }

}

