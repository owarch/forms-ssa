 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * AnalystSyspermUtilController.java - System permission assignment utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.security.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.*;
import javax.crypto.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;


public class AnalystSyspermUtilController extends FORMSController {

   boolean isbadrequest;

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // check permissions
      if (!getPermHelper().hasAnalystSysPerm("ROLES")) {
         safeRedirect("permission.err");
         return null;
      }

      // Use class-name based view
      mav=new ModelAndView("AnalystSyspermUtil");
      isbadrequest=true;
      String spath=request.getServletPath();

      try {

         if (spath.indexOf("viewperm.")>=0) {

            List outlist=viewPerm();
            isbadrequest=false;
            mav.addObject("list",outlist);
            mav.addObject("status","VIEWPERM");
            return mav;

         } else if (spath.indexOf("permassign.")>=0) {

            String clearall=permAssign();
            if (!clearall.equals("1")) {
               mav.addObject("status","PERMASSIGNED");
            } else {
               mav.addObject("status","PERMCLEARED");
            }
            return mav;

         } else {
            out.println(spath);
            return null;
         }   

      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // View Permissions
   private List viewPerm() {

      // pull system permissions 
      List p=getMainDAO().execQuery(
         "select p from Permissions p join fetch p.permissioncats " +
            "where p.permissioncats.pcatvalue in " +
               "(" + PcatConstants.ANALYST_SYSTEM + ")" +
               "order by p.permissioncats.pcatorder,p.permorder"
            );
            
      // Holders for permissions      
      BitSet systemset=null;
      List a=null;

      if (request.getParameter("aroleid")!=null) {

         Analystroles role=(Analystroles)getMainDAO().execUniqueQuery(
            "select a from Analystroles a where a.aroleid=" + (new Long(request.getParameter("aroleid")).longValue()));

         mav.addObject("acrvar",role.getRoleacr());
         mav.addObject("idvar",role.getAroleid());
         mav.addObject("typevar","role");

         a=getMainDAO().execQuery(
            "select s from Analystrolepermassign s where s.analystroles.aroleid=:aroleid"
            ,new String[] { "aroleid" }
            ,new Object[] { new Long(request.getParameter("aroleid")) }
            );   

         // Assign values to permission holders
         Iterator ia=a.iterator();
         while (ia.hasNext()) {
            Analystrolepermassign srp=(Analystrolepermassign)ia.next();
            systemset=(BitSet)srp.getPermlevel();
         }

      } else if (request.getParameter("auserid")!=null) {

         AllusersDAO adao=(AllusersDAO)this.getContext().getBean("allusersDAO");
         Allusers user=adao.getRecord(new Long(request.getParameter("auserid")).longValue());

         mav.addObject("acrvar",user.getUserdesc());
         mav.addObject("idvar",user.getAuserid());
         mav.addObject("typevar","user");

         a=getMainDAO().execQuery(
            "select s from Analystusersyspermassign s " +
               "where s.allusers.auserid=:auserid "
            ,new String[] { "auserid" }
            ,new Object[] { new Long(request.getParameter("auserid")) }
            );   
         
         if (a.size()>0) {
            mav.addObject("haveperm",new Boolean(true));
         } else {
            mav.addObject("haveperm",new Boolean(false));
         }

         // Assign values to permission holders
         Iterator ia=a.iterator();
         while (ia.hasNext()) {
            Analystusersyspermassign srp=(Analystusersyspermassign)ia.next();
            systemset=(BitSet)srp.getPermlevel();
         }

      }   
      // Use values from permission holders to set "checked" values in input fields
      Iterator ip=p.iterator();
      ArrayList newlist=new ArrayList();
      while (ip.hasNext()) {
         HashMap map=new HashMap();
         com.owarchitects.forms.commons.db.Permissions perm=(com.owarchitects.forms.commons.db.Permissions)ip.next();
         map.put("permobj",perm);
         int pcatvalue=perm.getPermissioncats().getPcatvalue();
         map.put("pcatvalue",new PcatConstants().getFieldName(pcatvalue));
         if (pcatvalue==PcatConstants.ANALYST_SYSTEM) {
            if (systemset!=null && systemset.get(perm.getPermpos()-1)) {
               map.put("permchecked","checked");
            } else {
               map.put("permchecked"," ");
            }   
         } 
         newlist.add(map);

      }

      return newlist;

   }


   // Assign Permissions
   private String permAssign() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String typevar=request.getParameter("typevar");
      String idvar=request.getParameter("idvar");
      String clearall=request.getParameter("clearall");
      if (clearall==null) clearall="";
      // return position parameters of set permissions for setting BitSet values
      Enumeration e=request.getParameterNames();
      ArrayList alist=new ArrayList();
      while (e.hasMoreElements()) {
         String pname=(String)e.nextElement();
         if (pname.substring(0,2).equalsIgnoreCase("r_")) {
            String pvalue=request.getParameter(pname);
            if (pvalue.equals("1")) {
               com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
                  "select p from Permissions p join fetch p.permissioncats " +
                     "where p.permid=:permid "
                  ,new String[] { "permid" }
                  ,new Object[] { new Long(pname.substring(2)) }
                  );
               alist.add(p);
            }
         }
      }

      // process role permissions
      if (typevar.equalsIgnoreCase("ROLE")) {

         Analystroles role=(Analystroles)getMainDAO().execUniqueQuery(
            "select a from Analystroles a where a.aroleid=" + (new Long(idvar).longValue()));

         List l=getMainDAO().execQuery(
            "select p.permissioncats.pcatvalue,max(p.permpos) " +
               "from Permissions p " +
               "group by p.permissioncats.pcatvalue " 
               );
         // This having clause was throwing errors.  I think it's valid, so this
         // might be a bug.  We'll handle the subset later.
         //  + "having p.permissioncats.pcatvalue in ....."

         // Initialize BitSets
         BitSet systemset=null;
         Iterator i=l.iterator();
         while (i.hasNext()) {
            Object[] oarray=(Object[])i.next();
            int setln=((Integer)oarray[1]).intValue();
            if (((Integer)oarray[0]).intValue()==PcatConstants.ANALYST_SYSTEM) {
               systemset=new BitSet(setln);
            } 
         }
         i=alist.iterator();
         while (i.hasNext()) {
            com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)i.next();
            if (p.getPermissioncats().getPcatvalue()==PcatConstants.ANALYST_SYSTEM) {
               // set flagged permissions in bitset
               systemset.set(p.getPermpos()-1,true);
            } 
         }
         if (systemset!=null) {
            // save value to database
            List srpl=getMainDAO().execQuery(
               "select srp from Analystrolepermassign srp where srp.analystroles.aroleid=:aroleid " 
               ,new String[] { "aroleid" }
               ,new Object[] { new Long(role.getAroleid()) }
               );   
            Analystrolepermassign srp;   
            if (srpl.size()>=1) { 
               srp=(Analystrolepermassign)srpl.get(0);
            } else {
               srp=new Analystrolepermassign();
               srp.setAnalystroles(role);
            }
            srp.setPermlevel(systemset);
            getMainDAO().saveOrUpdate(srp);
         }
      } 
      
      // process user permissions
      else if (typevar.equalsIgnoreCase("USER")) {

         AllusersDAO adao=(AllusersDAO)getContext().getBean("allusersDAO");
         Allusers user=adao.getRecord(new Long(idvar).longValue());

         List l=getMainDAO().execQuery(
            "select p.permissioncats.pcatvalue,max(p.permpos) " +
               "from Permissions p " +
               "group by p.permissioncats.pcatvalue " 
               );
         // This having clause was throwing errors.  I think it's valid, so this
         // might be a bug.  We'll handle the subset later.
         //  + "having p.permissioncats.pcatvalue in ...."

         // Initialize BitSets
         BitSet systemset=null;
         Iterator i=l.iterator();
         while (i.hasNext()) {
            Object[] oarray=(Object[])i.next();
            int setln=((Integer)oarray[1]).intValue();
            if (((Integer)oarray[0]).intValue()==PcatConstants.ANALYST_SYSTEM) {
               systemset=new BitSet(setln);
            } 
         }
         i=alist.iterator();
         while (i.hasNext()) {
            com.owarchitects.forms.commons.db.Permissions p=(com.owarchitects.forms.commons.db.Permissions)i.next();
            if (p.getPermissioncats().getPcatvalue()==PcatConstants.ANALYST_SYSTEM) {
               // set flagged permissions in bitset
               systemset.set(p.getPermpos()-1,true);
            } 
         }

         if (systemset!=null) {
            // save value to database
            List srpl=getMainDAO().execQuery(
               "select srp from Analystusersyspermassign srp where srp.allusers.auserid=:auserid " 
               ,new String[] { "auserid" }
               ,new Object[] { new Long(user.getAuserid()) }
               );   
            Analystusersyspermassign srp;   
            if (srpl.size()>=1) { 
               srp=(Analystusersyspermassign)srpl.get(0);
               if (clearall.equals("1")) {
                  getMainDAO().delete(srp);
               }
            } else {
               srp=new Analystusersyspermassign();
               srp.setAllusers(user);
            }
            if (!clearall.equals("1")) {
               srp.setPermlevel(systemset);
               getMainDAO().saveOrUpdate(srp);
            }   
         }
      }
      return clearall;

   }

}

