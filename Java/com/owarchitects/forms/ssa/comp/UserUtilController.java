 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * UserUtilController.java - User utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.security.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.*;
import javax.crypto.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;


public class UserUtilController extends FORMSController {

   boolean isbadrequest;

   public ModelAndView submitRequest() throws FORMSKeyException,FORMSSecurityException,FORMSException {

      // Use class-name based view
      mav=new ModelAndView("UserUtil");
      isbadrequest=true;
      String spath=request.getServletPath();

      try {

         if (spath.indexOf("adduser.")>=0) {

            // check permissions
            if (!hasGlobalPerm(PcatConstants.SYSTEM,"ACCTCREATE")) {
               safeRedirect("permission.err");
               return null;
            }

            isbadrequest=false;
            mav.addObject("status","ADDUSER");
            return mav;

         } 

         else if (spath.indexOf("addusersubmit.")>=0) {

            // check permissions
            if (!hasGlobalPerm(PcatConstants.SYSTEM,"ACCTCREATE")) {
               safeRedirect("permission.err");
               return null;
            }

            return addUserSubmit();

         }

         else if (spath.indexOf("acctmodify.")>=0) {

            // check permissions
            if (!hasGlobalPerm(PcatConstants.SYSTEM,"ACCTMOD")) {
               safeRedirect("permission.err");
               return null;
            }

            return acctModify();

         }

         else if (spath.indexOf("acctmodifysubmit.")>=0) {

            // check permissions
            if (!hasGlobalPerm(PcatConstants.SYSTEM,"ACCTMOD")) {
               safeRedirect("permission.err");
               return null;
            }

            return acctModifySubmit();

         }

         else if (spath.indexOf("pwreset.")>=0) {

            // check permissions
            if (!hasGlobalPerm(PcatConstants.SYSTEM,"ACCTPWRESET")) {
               safeRedirect("permission.err");
               return null;
            }

            return pwReset();

         }

         else if (spath.indexOf("pwchange.")>=0) {

            isbadrequest=false;
            mav.addObject("status","PWCHANGE");
            return mav;

         }

         else if (spath.indexOf("pwchangesubmit.")>=0) {

            return pwChangeSubmit();

         }

         else if (spath.indexOf("assignidinit.")>=0) {

            return assignIdInit();

         }

         else if (spath.indexOf("assignidsubmit.")>=0) {

            return assignIdSubmit();

         }

         else if (spath.indexOf("assignid.")>=0) {

            return assignId();

         }

         else if (spath.indexOf("acctdelete.")>=0) {

            // check permissions
            if (!hasGlobalPerm(PcatConstants.SYSTEM,"ACCTMOD")) {
               safeRedirect("permission.err");
               return null;
            }

            return acctDelete();

         }

      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

      return mav;

   }

   private ModelAndView addUserSubmit() throws FORMSException {

      try {

         AllinstDAO allinstDAO=(AllinstDAO)this.getContext().getBean("allinstDAO");
         LocalusersDAO localusersDAO=(LocalusersDAO)this.getContext().getBean("localusersDAO");
         EmbeddedusersDAO embeddedusersDAO=(EmbeddedusersDAO)this.getContext().getBean("embeddedusersDAO");

         // Return local installation

         Allinst inst=allinstDAO.getLocalinst();

         if (inst==null) {
            mav.addObject("status","USERADDNOTOK");
            return mav;
         }

         String username=request.getParameter("username");
         String usernumber=request.getParameter("usernumber");
         
         // Make sure username does not exist
         if (localusersDAO.getCurrentRecordByUsername(username)!=null) {
            mav.addObject("status","USERNAMEEXISTS");
            return mav;
         }
         
         // Make sure usernumber does not exist
         if (localusersDAO.getCurrentRecordByUsernumber(usernumber)!=null) {
            mav.addObject("status","USERNUMBEREXISTS");
            return mav;
         }

         // Create new local user from passed parameters
         Embeddedusers eu=new Embeddedusers();
         Localusers lu=new Localusers();
         Allusers au=new Allusers();

         eu.setUsername(username);
         String isdisabled=request.getParameter("isdisabled");
         if (isdisabled!=null && isdisabled.equals("1")) {
            eu.setIsdisabled(true); 
         } else {
            eu.setIsdisabled(false); 
         }

         String passwd=PasswordUtil.getTemporaryPassword();
         eu.setPassword(PasswordUtil.pwEncrypt(passwd));
         eu.setBadlogincount(0);

         // Create user key pair
         EncryptUtil.createKeyPair(session,username,passwd);
         
         // set fkpassword & dbpassword using users public key
         HashMap epass=getEpasswords();
         String fkpassword=(String)epass.get("fkpassword");
         String dbpassword=(String)epass.get("dbpassword");

         PublicKey pk=EncryptUtil.getPublicKey(session,username,passwd);
         eu.setFkpassword(EncryptUtil.encryptString(fkpassword,pk));
         eu.setDbpassword(EncryptUtil.encryptString(dbpassword,pk));

         getEmbeddedDAO().saveOrUpdate(eu);

         lu.setUsername(username);
         lu.setUsernumber(usernumber);

         String fullname=request.getParameter("fullname");
         if (fullname==null) fullname="";
         lu.setFullname(fullname); 
         
         String emailaddr=request.getParameter("emailaddr");
         if (emailaddr==null) emailaddr="";
         lu.setEmailaddr(emailaddr); 
         
         String isadmin=request.getParameter("isadmin");
         if (isadmin!=null && isadmin.equals("1")) {
            lu.setIsadmin(true); 
         } else {
            lu.setIsadmin(false); 
         }
         String istemppassword=request.getParameter("istemppassword");
         if (istemppassword!=null && istemppassword.equals("1")) {
            lu.setIstemppassword(true); 
         } else {
            lu.setIstemppassword(false); 
         }

         String expire=request.getParameter("accountexpiredate").trim();
         int month;
         int day;
         int year;
         Date expiredate;
         lu.setAccountexpiredate(null);
         if (expire.length()==10) {
            month=new Integer(expire.substring(0,2)).intValue()-1;
            day=new Integer(expire.substring(3,5)).intValue();
            year=new Integer(expire.substring(6)).intValue();
            if (month>=0 && day>0 && year>0) {
               expiredate=new Date(new GregorianCalendar(year,month,day).getTimeInMillis());
               lu.setAccountexpiredate(expiredate);
            }
         }
         lu.setIsdeleted(false); 

         getMainDAO().saveOrUpdate(lu);
         au.setAllinst(inst);
         au.setLuserid(lu.getLuserid());
         au.setUserdesc(lu.getUsername() + " [LOCAL]" );
         au.setIsdeleted(false);
         getMainDAO().saveOrUpdate(au);

         // Add user to system-level ALLUSERS role

         List l;

         l = getMainDAO().execQuery("select a from Resourceroles a " +
                        "where rolelevel=:rolelevel and upper(roleacr)=:roleacr" ,
                        new String[] { "rolelevel" , "roleacr" },
                        new Object[] { VisibilityConstants.SYSTEM , "ALLUSERS" }
                      );
         Resourceroles arole=null;             
         if (l.size() > 0){
            arole=(Resourceroles)l.get(0);
         }
         if (arole==null) {
            throw new FORMSException("Couldn't retrieve ALLUSERS system role");
         }
         Rroleuserassign rassign=new Rroleuserassign();
         rassign.setUser(au);
         rassign.setRole(arole);
         getMainDAO().saveOrUpdate(rassign);

         mav.addObject("status","USERADDOK");
         mav.addObject("username",username);
         mav.addObject("passwd",passwd);
         return mav;

      } catch (Exception e) {

        throw new FORMSException("FORMS Controller exception:  " + Stack2string.getString(e));

      }

   }

   private ModelAndView acctModify() throws FORMSException {

      try {

         LocalusersDAO localusersDAO=(LocalusersDAO)this.getContext().getBean("localusersDAO");
         EmbeddedusersDAO embeddedusersDAO=(EmbeddedusersDAO)this.getContext().getBean("embeddedusersDAO");
         String id=request.getParameter("id");
   
         // Create new local user from passed parameters
         Localusers lu=localusersDAO.getRecord(new Long(id).longValue());
         Embeddedusers eu=embeddedusersDAO.getRecord(lu.getUsername());
         mav.addObject("status","ACCTMODIFY");
         mav.addObject("lu",lu);
         mav.addObject("eu",eu);
         Date expiredate=lu.getAccountexpiredate();
         DateFormat df=new SimpleDateFormat("MM/dd/yyyy");
         if (expiredate!=null) {
            mav.addObject("expirestring",df.format(expiredate));
         } else {
            mav.addObject("expirestring","");
         }
         return mav; 

      } catch (Exception e) {

        throw new FORMSException("FORMS Controller exception:  " + Stack2string.getString(e));

      }

   }

   private ModelAndView acctModifySubmit() throws FORMSException {

      try {

         EmbeddedusersDAO embeddedusersDAO=(EmbeddedusersDAO)this.getContext().getBean("embeddedusersDAO");
         LocalusersDAO localusersDAO=(LocalusersDAO)this.getContext().getBean("localusersDAO");
         String id=request.getParameter("luserid");
   
         // Create new local user from passed parameters
         Localusers lu=localusersDAO.getRecord(new Long(id).longValue());

         String username=request.getParameter("username");
         String usernumber=request.getParameter("usernumber");

         List l;

         l = getMainDAO().execQuery("select a from Localusers a " +
                        "where luserid!=:luserid and upper(a.username) = upper(:username) and isdeleted=false" ,
                        new String[] { "luserid" , "username" },
                        new Object[] { new Long(id).longValue() , username }
                      );
         if (l.size() > 0){
            mav.addObject("status","USERNAMEEXISTS");
            return mav;
         }

         l = getMainDAO().execQuery("select a from Localusers a " +
                        "where luserid!=:luserid and upper(a.usernumber) = upper(:usernumber) and isdeleted=false" ,
                        new String[] { "luserid" , "usernumber" },
                        new Object[] { new Long(id).longValue() , usernumber }
                      );
         if (l.size() > 0){
            mav.addObject("status","USERNUMBEREXISTS");
            return mav;
         }

         Embeddedusers eu=embeddedusersDAO.getRecord(lu.getUsername());
         eu.setUsername(username);
         String isdisabled=request.getParameter("isdisabled");
         if (isdisabled!=null && isdisabled.equals("1")) {
            eu.setIsdisabled(true); 
         } else {
            eu.setIsdisabled(false); 
         }

         lu.setUsername(username);
         lu.setUsernumber(usernumber);

         String fullname=request.getParameter("fullname");
         if (fullname==null) fullname="";
         lu.setFullname(fullname); 
         
         String emailaddr=request.getParameter("emailaddr");
         if (emailaddr==null) emailaddr="";
         lu.setEmailaddr(emailaddr); 
         
         String isadmin=request.getParameter("isadmin");
         if (isadmin!=null && isadmin.equals("1")) {
            lu.setIsadmin(true); 
         } else {
            lu.setIsadmin(false); 
         }
         String istemppassword=request.getParameter("istemppassword");
         if (istemppassword!=null && istemppassword.equals("1")) {
            lu.setIstemppassword(true); 
         } else {
            lu.setIstemppassword(false); 
         }

         String expire=request.getParameter("accountexpiredate").trim();
         int month;
         int day;
         int year;
         Date expiredate;
         lu.setAccountexpiredate(null);
         if (expire.length()==10) {
            month=new Integer(expire.substring(0,2)).intValue()-1;
            day=new Integer(expire.substring(3,5)).intValue();
            year=new Integer(expire.substring(6)).intValue();
            if (month>=0 && day>0 && year>0) {
               expiredate=new Date(new GregorianCalendar(year,month,day).getTimeInMillis());
               lu.setAccountexpiredate(expiredate);
            }
         }

         l = getMainDAO().execQuery("select a from Allusers a " +
                        "where a.luserid=:luserid and a.allinst.islocal=true" ,
                        new String[] { "luserid" },
                        new Object[] { new Long(id).longValue() }
                      );
         if (l.size() > 0){

            Allusers au=(Allusers)l.get(0);
            au.setUserdesc(lu.getUsername() + " [LOCAL]" );
            getMainDAO().saveOrUpdate(au);

         }

         getMainDAO().saveOrUpdate(lu);
         getEmbeddedDAO().saveOrUpdate(eu);
         mav.addObject("status","ACCTMODIFYOK");
         return mav;

      } catch (Exception e) {

        throw new FORMSException("FORMS Controller exception:  " + Stack2string.getString(e));

      }

   }

   private ModelAndView pwReset() throws FORMSException {

      try {

         EmbeddedusersDAO embeddedusersDAO=(EmbeddedusersDAO)this.getContext().getBean("embeddedusersDAO");
         LocalusersDAO localusersDAO=(LocalusersDAO)this.getContext().getBean("localusersDAO");
         String id=request.getParameter("id");
   
         // Create new local user from passed parameters
         Localusers lu=localusersDAO.getRecord(new Long(id).longValue());
         if (lu!=null) {

            Embeddedusers eu=embeddedusersDAO.getRecord(lu.getUsername());

            lu.setIstemppassword(true); 
            String passwd=PasswordUtil.getTemporaryPassword();
            eu.setPassword(PasswordUtil.pwEncrypt(passwd));

            // Create user key pair
            EncryptUtil.createKeyPair(session,lu.getUsername(),passwd);
         
            // set fkpassword & dbpassword using users public key
            HashMap epass=getEpasswords();
            String fkpassword=(String)epass.get("fkpassword");
            String dbpassword=(String)epass.get("dbpassword");

            PublicKey pk=EncryptUtil.getPublicKey(session,lu.getUsername(),passwd);
            eu.setFkpassword(EncryptUtil.encryptString(fkpassword,pk));
            eu.setDbpassword(EncryptUtil.encryptString(dbpassword,pk));

            getMainDAO().saveOrUpdate(lu);
            getEmbeddedDAO().saveOrUpdate(eu);
            mav.addObject("status","PWRESETOK");
            mav.addObject("username",lu.getUsername());
            mav.addObject("passwd",passwd);

         } else {

            mav.addObject("status","PWRESETNOTOK");
            
         }
         return mav; 

      } catch (Exception e) {

        throw new FORMSException("FORMS Controller exception:  " + Stack2string.getString(e));

      }

   }

   private ModelAndView pwChangeSubmit() throws FORMSException {

      try {

         EmbeddedusersDAO embeddedusersDAO=(EmbeddedusersDAO)this.getContext().getBean("embeddedusersDAO");
         LocalusersDAO localusersDAO=(LocalusersDAO)this.getContext().getBean("localusersDAO");
         String oldpw=request.getParameter("oldpassword");
         String newpw=request.getParameter("newpassword");
         String encpw=PasswordUtil.pwEncrypt(newpw);
         String username=this.getAuth().getValue("username");
   
         // Create new local user from passed parameters
         Embeddedusers eu=embeddedusersDAO.getRecord(username);
         Localusers lu=localusersDAO.getCurrentRecordByUsername(username);
         if (eu!=null && lu!=null) {

            // Verify old password
            if (!PasswordUtil.pwEncrypt(oldpw).equals(eu.getPassword())) {

               mav.addObject("status","PWCHANGENOMATCH");
               return mav;

            }

            // Verify pw length/complexity
            if (newpw.length()<6 || !newpw.matches("^.*[^a-z].*$")) {

               mav.addObject("status","PWCHANGENOTCOMPLEX");
               return mav;

            }

            // Check password history
            List l;
      
            l = getMainDAO().execQuery("select a from Pwhistory a " +
                           "where upper(a.username) = upper(:username) " +
                           "order by passworddt" ,
                           new String[] { "username" },
                           new Object[] { username }
                         );
            Pwhistory pwh;             
            // loop through password history             
            for (int i=0;i<l.size();i++) {
               pwh=(Pwhistory)l.get(i);
               if (i<l.size()-11) {
                  // trim back to 10 records             
                  getMainDAO().delete(pwh);
               } else { 
                  // see if new pw matches one previously used
                  if (encpw.equals(((Pwhistory) l.get(i)).getPassword())) {
                     mav.addObject("status","PWCHANGEALREADYUSED");
                     return mav;
                  }
               }
            }

            // Set new password
            Date changedate=new Date(System.currentTimeMillis());
            lu.setIstemppassword(false); 
            lu.setPassworddt(changedate);
            getMainDAO().saveOrUpdate(lu);
            eu.setPassword(encpw);

            // Create user key pair
            EncryptUtil.createKeyPair(session,lu.getUsername(),newpw);
         
            // set fkpassword & dbpassword using users public key
            HashMap epass=getEpasswords();
            String fkpassword=(String)epass.get("fkpassword");
            String dbpassword=(String)epass.get("dbpassword");
   
            PublicKey pk=EncryptUtil.getPublicKey(session,lu.getUsername(),newpw);
            eu.setFkpassword(EncryptUtil.encryptString(fkpassword,pk));
            eu.setDbpassword(EncryptUtil.encryptString(dbpassword,pk));

            getEmbeddedDAO().saveOrUpdate(eu);
            mav.addObject("status","PWCHANGEOK");

            // Add Pwhistory record for new password
            pwh=new Pwhistory();
            pwh.setLocalusers(lu);
            pwh.setUsername(username);
            pwh.setPassword(encpw);
            pwh.setPassworddt(changedate);
            getMainDAO().saveOrUpdate(pwh);

         } else {

            mav.addObject("status","PWCHANGENOTOK");
            
         }
         return mav; 

      } catch (Exception e) {

        throw new FORMSException("FORMS Controller exception:  " + Stack2string.getString(e));

      }

   }


   private ModelAndView assignIdInit() throws FORMSException {

      // pull list of studies
      List studies=getMainDAO().execQuery( "select s from Studies s" );
      mav.addObject("list",studies);
      mav.addObject("status","ASSIGNIDINIT");
      return mav;

   }

   private ModelAndView assignId() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Get current records if any
      HttpServletRequest request=getRequest();

      String studyid=(String)request.getParameter("studyid");
      String luserid=(String)request.getParameter("luserid");

      Studies std=(Studies)getMainDAO().execUniqueQuery(
         "select s from Studies s where s.studyid=" + studyid
         );
      Localusers luser=(Localusers)getMainDAO().execUniqueQuery(
         "select l from Localusers l where l.luserid=" + luserid
         );

      List uarray=getMainDAO().execQuery( "select u from Useridassign u where u.studies.studyid=" +  studyid +
         " and u.localusers.luserid=" + luserid
         );

      // Get list of ID variables used in forms
      List hal=getMainDAO().execQuery(
         "select f from Formha f where f.studies.studyid=" + studyid
         );
      StringBuilder sb=new StringBuilder();
      Iterator i=hal.iterator();
      while (i.hasNext()) {
         Formha ha=(Formha)i.next();
         Long fid=new Long(ha.getFormhaid());
         sb.append(fid);
         if (i.hasNext()) sb.append(",");
      }
      List forml=getMainDAO().execQuery(
         "select f from Datatabledef f join fetch f.allinst where f.pformhaid in (" + sb + ")"
         );
      sb=new StringBuilder();
      ArrayList aidlist=new ArrayList();
      i=forml.iterator();
      while (i.hasNext()) {
         Datatabledef fdef=(Datatabledef)i.next();
         Long fid=new Long(fdef.getDtdefid());
         sb.append(fid);
         aidlist.add(fid);
         if (i.hasNext()) sb.append(",");
      }
      getAuth().setObjectValue("aidlist",aidlist);
      List l;
      if (sb.length()>0) {
         l=getMainDAO().execQuery(
            "select distinct f.columnname,f.columnorder from Datatablecoldef f " + 
               "where f.datatabledef.dtdefid in (" + sb + ") and f.isidfield=true " +
                  "order by f.columnorder,f.columnname"
                  );
      } else {
         l=new ArrayList();
      }
      ArrayList idlist=new ArrayList();      
      i=l.iterator();
      while (i.hasNext()) {
         Object[] oarray=(Object[])i.next();
         if (!idlist.contains(oarray[0])) {
            idlist.add(oarray[0]);
         }
      }
      ArrayList passlist=new ArrayList();
      i=idlist.iterator();
      while (i.hasNext()) {
         String var=(String)i.next();
         String val="";
         Iterator uiter=uarray.iterator();
         while (uiter.hasNext()) {
            Useridassign u=(Useridassign)uiter.next();
            if (u.getVarname().equalsIgnoreCase(var)) {
               val=u.getVarvalue();
            }
         }
         HashMap map=new HashMap();
         map.put("var",var);
         map.put("val",val);
         passlist.add(map);
      }
      mav.addObject("list",passlist);
      mav.addObject("studyid",getRequest().getParameter("studyid"));
      mav.addObject("luserid",getRequest().getParameter("luserid"));
      mav.addObject("status","ASSIGNID");
      return mav;

   }


   // Display ECODE for ID Combination
   private ModelAndView assignIdSubmit() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      HttpServletRequest request=getRequest();

      String studyid=(String)request.getParameter("studyid");
      String luserid=(String)request.getParameter("_luserid_");

      Studies std=(Studies)getMainDAO().execUniqueQuery(
         "select s from Studies s where s.studyid=" + studyid
         );
      Localusers luser=(Localusers)getMainDAO().execUniqueQuery(
         "select l from Localusers l where l.luserid=" + luserid
         );

      // delete current records if any exist
      List l=getMainDAO().execQuery( "select u from Useridassign u where u.studies.studyid=" +  studyid +
         " and u.localusers.luserid=" + luserid
         );
      Iterator i=l.iterator();
      while (i.hasNext()) {
         Useridassign u=(Useridassign)i.next();
         getMainDAO().delete(u);
      }

      // Create new values
      Enumeration e=request.getParameterNames();
      while (e.hasMoreElements()) {
         String k=(String)e.nextElement();
         if (!(k.equalsIgnoreCase("studyid") || k.equalsIgnoreCase("_luserid_"))) {
             String v=(String)request.getParameter(k);
             if (v!=null) {
                Useridassign u=new Useridassign();
                u.setStudies(std);
                u.setLocalusers(luser);
                u.setVarname(k);
                u.setVarvalue(v);
                getMainDAO().saveOrUpdate(u);
             }
         }
      }

      mav.addObject("status","IDASSIGNED");
      return mav;

   }


   private ModelAndView acctDelete() throws FORMSException {

      try {

         LocalusersDAO localusersDAO=(LocalusersDAO)this.getContext().getBean("localusersDAO");
         EmbeddedusersDAO embeddedusersDAO=(EmbeddedusersDAO)this.getContext().getBean("embeddedusersDAO");
         String id=request.getParameter("id");
   
         // Create new local user from passed parameters
         Localusers lu=localusersDAO.getRecord(new Long(id).longValue());
         Embeddedusers eu=embeddedusersDAO.getRecord(lu.getUsername());
         if (lu!=null) {

            lu.setIsdeleted(true); 
            getMainDAO().saveOrUpdate(lu);

            List l;

            l = getMainDAO().execQuery("select a from Allusers a " +
                           "where a.luserid=:luserid and a.allinst.islocal=true" ,
                           new String[] { "luserid" },
                           new Object[] { new Long(id).longValue() }
                         );
            if (l.size() > 0){
   
               Allusers au=(Allusers)l.get(0);
               au.setIsdeleted(true);
               getMainDAO().saveOrUpdate(au);
   
            }

            if (eu!=null) {
               getEmbeddedDAO().delete(eu);
            }

            mav.addObject("status","ACCTDELETEOK");
            mav.addObject("username",lu.getUsername());

         } else {

            mav.addObject("status","ACCTDELETENOTOK");
            
         }
         return mav; 

      } catch (Exception e) {

        throw new FORMSException("FORMS Controller exception:  " + Stack2string.getString(e));

      }

   }

   // Return HashMap of fk & db passwords

   private HashMap getEpasswords() {

      try {
         AccessDAO accessDAO=(AccessDAO)this.getContext().getBean("accessDAO");
         Access euser=accessDAO.getRecord();
         HashMap rm=new HashMap(2);
         SecretKey sk=EncryptUtil.getSecretKey(session,"formsinternal",SessionObjectUtil.getConfigObj(session).getKeyStorePassword());
         rm.put("fkpassword",EncryptUtil.decryptString(euser.getFkpassword(),sk));
         rm.put("dbpassword",EncryptUtil.decryptString(euser.getDbpassword(),sk));
         return rm;
     } catch (Exception e) {
         return null;
     } 
   
   }
       
}




