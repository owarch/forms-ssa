 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * PermUtilController.java - User utilities controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.security.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.http.*;
import javax.crypto.*;
import org.hibernate.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.orm.hibernate3.*;
import com.oreilly.servlet.*;
import com.oreilly.servlet.multipart.*;


public class PermUtilController extends FORMSController {

   boolean isbadrequest;

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // Use class-name based view
      mav=new ModelAndView("PermUtil");
      isbadrequest=true;
      String spath=request.getServletPath();

      // check permissions
      if (!hasGlobalPerm(PcatConstants.SYSTEM,"PERMSMGT")) {
         safeRedirect("permission.err");
         return null;
      }

      try {

         // Add permission interface
         if (spath.indexOf("addperm.")>=0) {

            isbadrequest=false;
            mav.addObject("catlist",getAuth().getSessionAttribute("optionsobject"));
            mav.addObject("status","ADDPERM");
            return mav;

         // Submit new permission
         } else if (spath.indexOf("addpermsubmit.")>=0) {

            String status=addPermSubmit();
            mav.addObject("status",status);
            return mav;

         // Modify permission interface
         } else if (spath.indexOf("modifyperm.")>=0) {

            isbadrequest=false;
            mav.addObject("status","MODIFYPERM");
            return mav;

         // Submit modified permission
         } else if (spath.indexOf("modifypermsubmit.")>=0) { 

            String status=modifyPermSubmit();
            mav.addObject("status",status);
            return mav;

         // Re-order permission interface
         } else if (spath.indexOf("permorder.")>=0) {

            List l=permOrder();
            isbadrequest=false;
            mav.addObject("list",l);
            mav.addObject("status","PERMORDER");
            return mav;

         // Submit re-ordering of permissions
         } else if (spath.indexOf("permordersubmit.")>=0) {

            String porderstr=(String)request.getParameter("porderstr");
            String[] sa=porderstr.split(",");
            for (int i=0;i<sa.length;i++) {
               Long permid=new Long(sa[i].replaceFirst("^li_",""));
               com.owarchitects.forms.commons.db.Permissions perm2mod=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
                         "select p from Permissions p where p.permid=:permid"
                         ,new String[] { "permid" }
                         ,new Object[] { permid }
                         );
               perm2mod.setPermorder(i+1);
               getMainDAO().saveOrUpdate(perm2mod);
            }
            mav.addObject("status","PERMREORDERED");
            return mav;

         // Order permission categories interface
         } else if (spath.indexOf("pcatorder.")>=0) {

            List l=getMainDAO().execQuery("select p from Permissioncats p order by p.pcatorder");

            isbadrequest=false;
            ArrayList newlist=new ArrayList();
            Iterator i=l.iterator();
            while (i.hasNext()) {
               Permissioncats p=(Permissioncats)i.next();
               HashMap map=new HashMap();
               map.put("pcat",p);
               map.put("pcatvalue",new PcatConstants().getFieldName(p.getPcatvalue()));
               newlist.add(map);
            }
            mav.addObject("list",newlist);
            mav.addObject("status","PCATORDER");
            return mav;

         // Submit permission category ordering
         } else if (spath.indexOf("pcatordersubmit.")>=0) {

            String porderstr=(String)request.getParameter("porderstr");
            String[] sa=porderstr.split(",");
            for (int i=0;i<sa.length;i++) {
               Long pcatid=new Long(sa[i].replaceFirst("^li_",""));
               com.owarchitects.forms.commons.db.Permissioncats pcat2mod=(com.owarchitects.forms.commons.db.Permissioncats)getMainDAO().execUniqueQuery(
                         "select p from Permissioncats p where p.pcatid=:pcatid"
                         ,new String[] { "pcatid" }
                         ,new Object[] { pcatid }
                         );
               pcat2mod.setPcatorder(i+1);
               getMainDAO().saveOrUpdate(pcat2mod);
            }
            mav.addObject("status","PERMREORDERED");
            return mav;

         // Delete specified permission
         } else if (spath.indexOf("deleteperm.")>=0) { 

            String permacr=getRequest().getParameter("permacr");
            String permdesc=getRequest().getParameter("permdesc");
            Long permid=new Long(getRequest().getParameter("permid"));

            com.owarchitects.forms.commons.db.Permissions perm2mod=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
                      "select p from Permissions p where p.permid=:permid"
                      ,new String[] { "permid" }
                      ,new Object[] { permid }
                      );

            perm2mod.setIsdeleted(true);
            getMainDAO().saveOrUpdate(perm2mod);
            mav.addObject("status","PERMDELETED");
            return mav;

         // List permission categories
         } else if (spath.indexOf("listcats.")>=0) {

            List l=getMainDAO().execQuery("select pc from Permissioncats pc order by pc.pcatorder ");
            ArrayList newlist=new ArrayList();
            Iterator i=l.iterator();
            while (i.hasNext()) {
               Permissioncats p=(Permissioncats)i.next();
               HashMap map=new HashMap();
               map.put("pcat",p);
               map.put("pcatvalue",new PcatConstants().getFieldName(p.getPcatvalue()));
               newlist.add(map);
            }
            mav.addObject("list",newlist);
            mav.addObject("status","LISTCATS");
            return mav;

         // Modify permission category attributes interface
         } else if (spath.indexOf("catmod.")>=0) {

            isbadrequest=false;
            mav.addObject("status","CATMOD");
            return mav;

         // Submit modified permission category attributes
         } else if (spath.indexOf("catmodsubmit.")>=0) { 

            String pcatdesc=getRequest().getParameter("pcatdesc");
            Long pcatid=new Long(getRequest().getParameter("pcatid"));

            Permissioncats cat2mod=(Permissioncats)getMainDAO().execUniqueQuery(
                      "select pc from Permissioncats pc where pc.pcatid=:pcatid"
                      ,new String[] { "pcatid" }
                      ,new Object[] { pcatid }
                      );
            cat2mod.setPcatdesc(pcatdesc);
            getMainDAO().saveOrUpdate(cat2mod);
            mav.addObject("status","CATMODIFIED");
            updateOptionsObject();
            return mav;

         } else {

         }

         out.println(spath);
         isbadrequest=false;
         return null;

      } catch (Exception e) { 
         throw new FORMSException("FORMS Controller exception:  ",e);
      }

   }

   // Set values in optionsobject (used/displayed by OptionsMenuController)  
   private void updateOptionsObject() throws FORMSException,FORMSKeyException,FORMSSecurityException {

         // Determine which categories to show
         String pcatshow=getAuth().getValue("pcatshow");

         List l;
         
         // pull category list for options menu
         l=getMainDAO().execQuery("select pc from Permissioncats pc order by pc.pcatorder");
         Permissioncats allcat=new Permissioncats();
         allcat.setPcatvalue(PcatConstants.ALL);
         l.add(allcat);
         ArrayList optlist=new ArrayList<HashMap>();
         Iterator i=l.iterator();
         while (i.hasNext()) {
            Permissioncats pc=(Permissioncats)i.next();
            HashMap omap=new HashMap();
            String cvalue=new PcatConstants().getFieldName(pc.getPcatvalue());
            omap.put("cat",cvalue);
            omap.put("desc",pc.getPcatdesc());
            omap.put("catid",pc.getPcatid());
            if (cvalue.equals(pcatshow)) {
               omap.put("checked","checked");
            } else {
               omap.put("checked"," ");
            }
            optlist.add(omap);
         }
         // make category list available to options controller
         getAuth().setSessionAttribute("optionsobject",optlist);
   }

   // Submit new permission
   private String addPermSubmit() {

            String pcatvaluestr=request.getParameter("pcatvalue");
            int pcatvalue=new PcatConstants().getFieldValue(pcatvaluestr);
            String permacr=request.getParameter("permacr");
            String permdesc=request.getParameter("permdesc");
            String status=null;

            List l;
            // Make sure no matching permission currently exists
            l=getMainDAO().execQuery("select perm from Permissions perm " +
                                        " where perm.isdeleted!=true and " +
                                              " perm.permissioncats.pcatvalue=:pcatvalue and " +
                                              " perm.permacr=:permacr " 
                                     ,new String[] { "pcatvalue","permacr" }
                                     ,new Object[] { pcatvalue,permacr }
                                    );
            if (l.size()>0) { 
               status="PERMEXISTS";
               return status;
            }

            // pull category record
            Permissioncats thiscat=(Permissioncats)getMainDAO().execUniqueQuery(
                      "select pc from Permissioncats pc where pc.pcatvalue=:pcatvalue"
                      ,new String[] { "pcatvalue" }
                      ,new Object[] { pcatvalue }
                      );

            // determine max order, pos values
            l=getMainDAO().execQuery("select max(perm.permorder),max(perm.permpos) " +
                                        " from Permissions perm " +
                                        " where perm.isdeleted!=true and " +
                                              " perm.permissioncats.pcatvalue=:pcatvalue " 
                                     ,new String[] { "pcatvalue" }
                                     ,new Object[] { pcatvalue }
                                    );
            // get next values for object                        
            Object[] oarray=(Object[])l.get(0);
            int setpermorder;
            int setpermpos;
            try {
               setpermorder=((Integer)oarray[0])+1;
            } catch (java.lang.NullPointerException npe) {
               setpermorder=1;
            }
            try {
               setpermpos=((Integer)oarray[1])+1;
            } catch (java.lang.NullPointerException npe) {
               setpermpos=1;
            }

            // persist new permissions object
            com.owarchitects.forms.commons.db.Permissions newperm=new com.owarchitects.forms.commons.db.Permissions();
            newperm.setPermorder(setpermorder);
            newperm.setPermpos(setpermpos);
            newperm.setPermacr(permacr);
            newperm.setPermdesc(permdesc);
            newperm.setPermissioncats(thiscat);
            getMainDAO().saveOrUpdate(newperm);
            status="PERMADDED";
            return status;

   }

   // Submit modified permission
   private String modifyPermSubmit() {

            String permacr=getRequest().getParameter("permacr");
            String permdesc=getRequest().getParameter("permdesc");
            String status="";
            Long permid=new Long(getRequest().getParameter("permid"));

            // Make sure permission doesn't already exist within category
            Long pcatid=(Long)getMainDAO().execUniqueQuery(
               "select p.permissioncats.pcatid from Permissions p where p.permid=:permid"
                      ,new String[] { "permid" }
                      ,new Object[] { permid }
                   );
            com.owarchitects.forms.commons.db.Permissions checkperm=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
                      "select p from Permissions p where p.permid!=:permid and p.permissioncats.pcatid=:pcatid and " +
                         "p.isdeleted!=true and p.permacr=:permacr" 
                      ,new String[] { "permid","pcatid","permacr" }
                      ,new Object[] { permid,pcatid,permacr }
                      );

            if (checkperm!=null) {
               status="PERMEXISTS";
               return status;
            }

            // Pull record to be changed
            com.owarchitects.forms.commons.db.Permissions perm2mod=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
                      "select p from Permissions p where p.permid=:permid"
                      ,new String[] { "permid" }
                      ,new Object[] { permid }
                      );

            perm2mod.setPermacr(permacr);
            perm2mod.setPermdesc(permdesc);
            getMainDAO().saveOrUpdate(perm2mod);
            status="PERMMODIFIED";
            return status;

   }

   // Re-order permission interface
   private List permOrder() {

            Long permid=new Long(getRequest().getParameter("permid"));

            com.owarchitects.forms.commons.db.Permissions checkperm=(com.owarchitects.forms.commons.db.Permissions)getMainDAO().execUniqueQuery(
                      "select p from Permissions p where p.permid=:permid" 
                      ,new String[] { "permid" }
                      ,new Object[] { permid }
                      );

            Long pcatid=new Long(checkperm.getPermissioncats().getPcatid());          

            List l=getMainDAO().execQuery(
                      "select p from Permissions p where p.permissioncats.pcatid=:pcatid " +
                         "order by p.permorder" 
                      ,new String[] { "pcatid" }
                      ,new Object[] { pcatid }
                      );
            
            return l;

   }

}


