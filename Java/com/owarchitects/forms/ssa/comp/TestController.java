 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *
 * TestController.java - Test controller
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.lang.Thread;
import javax.servlet.http.*;
import org.springframework.web.servlet.*;
import org.springframework.context.support.*;

public class TestController extends FORMSSsaServiceClientController {

   public ModelAndView submitRequest() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      /*   
      SsaServiceSystemStudyParms parms=new SsaServiceSystemStudyParms("studyUserListServiceTarget","getUserList");
      List l=submitServiceStudyRequest(parms);

      out.println("<BR>HI(1)!!!<BR>");

      List l=getMainDAO().execQuery("select f from Tablemodtime f"); 
      MiscUtil.writeStringToFile("c:\\michael\\temp\\junkfil1",ObjectXmlUtil.objectToXmlString(l));
      Iterator i=l.iterator();
      while (i.hasNext()) {
         Tablemodtime t=(Tablemodtime)i.next();
         MiscUtil.writeStringToFile("c:\\michael\\temp\\junkfil1_" + System.currentTimeMillis(),ObjectXmlUtil.objectToXmlString(t));
         out.println(t.getClassname() + "   " + t.getTablemodtime() + "<br>");
      }
      out.println("<BR>HI(2)!!!<BR>");
         
      l=getMainDAO().execQuery("select f from Studies f"); 
      MiscUtil.writeStringToFile("c:\\michael\\temp\\junkfil2",ObjectXmlUtil.objectToXmlString(l));
      i=l.iterator();
      while (i.hasNext()) {
         Studies t=(Studies)i.next();
         out.println(t.getRecmodtime() + "<br>");
      }
      out.println("<BR>HI(3)!!!<BR>");

CsaServiceSystemParms parms=new CsaServiceSystemParms();
parms.setBeanname("verifyConnectionServiceTarget");
parms.setMethodname("checkStatus");

MiscUtil.writeStringToFile("c:\\michael\\temp\\junkfil71",ObjectXmlUtil.objectToXmlString(parms));

String s=ObjectUtil.objectToString(parms);

MiscUtil.writeStringToFile("c:\\michael\\temp\\junkfil72",s);

Object o=ObjectUtil.objectFromString(s);

MiscUtil.writeStringToFile("c:\\michael\\temp\\junkfil73",ObjectXmlUtil.objectToXmlString(o));

s=ObjectUtil.objectToEncodedString(parms);

MiscUtil.writeStringToFile("c:\\michael\\temp\\junkfil74",s);

o=ObjectUtil.objectFromEncodedString(s);

MiscUtil.writeStringToFile("c:\\michael\\temp\\junkfil75",ObjectXmlUtil.objectToXmlString(o));
      */   

         
      List l=getMainDAO().getTable("Allusers"); 
      MiscUtil.writeStringToFile("c:\\michael\\temp\\junkfil_allusers",ObjectXmlUtil.objectToXmlString(l));
      MiscUtil.writeStringToFile("c:\\michael\\temp\\junkfil_1",new Integer(l.size()).toString());
      l=getMainDAO().getTable("Allusers_transfer"); 
      MiscUtil.writeStringToFile("c:\\michael\\temp\\junkfil_2",new Integer(l.size()).toString());
      MiscUtil.writeStringToFile("c:\\michael\\temp\\junkfil_allusers_transfer",ObjectXmlUtil.objectToXmlString(l));


out.println("HELLO!");

      return null;

   }

}

