 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/* 
 *
 * Authenticator.java -  Host/User Authentication Class
 *
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.security.*;
import javax.crypto.SecretKey;
import javax.servlet.http.*;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.*;
import org.springframework.web.context.support.*;
import org.apache.axis2.context.*;
import org.apache.axis2.client.*;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.DateUtils;

public class Authenticator extends FORMSResourceAccessor {

   public Authenticator(HttpServletRequest request,HttpServletResponse response) {

      super(request,response);

   }

   // Check machine lockout status
   public int checkHost() {

      String remoteip=request.getRemoteAddr();
      long maxipbad=SessionObjectUtil.getConfigObj(session).getMaxInvalidIPLogins();
      List l;
      l=getEmbeddedDAO().execQuery(
         " select il from Invalidlogins as il " +
            " where il.ipaddr=:remoteip and il.currcount>:maxipbad ", 
            new String[] { "remoteip","maxipbad" } ,
            new Object[] { remoteip, maxipbad }
                                                 );
      if (l.size()>0) {

         return AuthenticatorConstants.HOST_DENIED;

      } else {

         return AuthenticatorConstants.HOST_ALLOWED;

      }

   }

   // User Login
   public LoginResult login(String user,String passwd) throws FORMSException,Exception {

      String encpw="";
      String dbpw="";

      LoginResult lr=new LoginResult();

      // Process user-suplied data
      EmbeddedusersDAO eusersDAO=(EmbeddedusersDAO)this.getContext().getBean("embeddedusersDAO");

      Embeddedusers euser=eusersDAO.getRecord(user);

      if (euser!=null) {
         // Check if account locked
         if (euser.getIsdisabled()) {
            
            lr.setStatus(AuthenticatorConstants.ACCOUNT_LOCKED);
            lr.setBadlogincount(-1);
            logInvalidAttempt(user);
            return lr;
  
         }
  
         // See if password matches
         String pwck=PasswordUtil.pwEncrypt(passwd);
         if (pwck!=null && pwck.equals(euser.getPassword())) {
  
            //////////////////////
            // SUCCESSFUL LOGIN //
            //////////////////////

            // update bad login count
            euser.setBadlogincount(new Integer(0));           
            eusersDAO.saveOrUpdate(euser);
  
            // Retrieve (optionally create) user's private encryption key 
            PrivateKey privkey=EncryptUtil.getPrivateKey(session,user,passwd);
               
            if (privkey==null) {
  
               lr.setStatus(AuthenticatorConstants.KEY_ERROR);
               lr.setBadlogincount(-1);
               return lr;
  
            } 
            
            // Use private key to retrieve encryption, db passwords
  
            encpw=EncryptUtil.decryptString(euser.getFkpassword(),privkey);;
      
            LocalusersDAO lusersDAO=null;
            AllusersDAO ausersDAO=null;
            Localusers luser=null;
            try {
               lusersDAO=(LocalusersDAO)this.getContext().getBean("localusersDAO");
               ausersDAO=(AllusersDAO)this.getContext().getBean("allusersDAO");
               luser=lusersDAO.getCurrentRecordByUsername(user);
            } catch (org.springframework.beans.factory.NoSuchBeanDefinitionException nsbde) {
               // Re-initialize context
               SessionObjectUtil.readMainDBIntoContext(getSession(),user,passwd);
               initDataSource(true);
               lusersDAO=(LocalusersDAO)this.getContext().getBean("localusersDAO");
               ausersDAO=(AllusersDAO)this.getContext().getBean("allusersDAO");
               luser=lusersDAO.getCurrentRecordByUsername(user);
            }

            // Retrieve encryption key
            try {
  
               SecretKey sk=EncryptUtil.getEncryptionKey(session,encpw);
               // Write to session 
               SessionObjectUtil.setEncryptionKeyObj(session,sk);
               // Begin FORMSAuth object
               FORMSAuth auth=this.getAuth();
               Allusers auser=(Allusers)getMainDAO().execUniqueQuery(
                  "select a From Allusers a where a.allinst.islocal=true and a.luserid=" + luser.getLuserid()
                  );
               auth.setValue("username",user);
               auth.setValue("userdesc",auser.getUserdesc());
               auth.setValue("auserid",new Long(auser.getAuserid()).toString());
               auth.setValue("luserid",new Long(luser.getLuserid()).toString());
               auth.setValue("session","1");
  
            } catch (FORMSNoEncryptKeyException ne) {

               lr.setStatus(AuthenticatorConstants.NOENCKEY);
               lr.setBadlogincount(-1);
               return lr;
  
            } catch (Exception keyx) {

               lr.setStatus(AuthenticatorConstants.BADENCKEYPASS);
               lr.setBadlogincount(-1);
               return lr;

            }

            // initialize study sort order
            this.getAuth().setValue("showarchivedstudies","false");
            this.getAuth().setValue("studysortcolumn","siteno");
            this.getAuth().setValue("studysortdir","asc");

            if (luser.getIstemppassword()) {
               resetInvalidCount();
               logEvent("User login");
               lr.setStatus(AuthenticatorConstants.TEMPPWCHANGE);
               return lr;
            }

            // Check if password is expired
            Date pwexpdate=DateUtils.truncate( 
                            DateUtils.addDays(
                                luser.getPassworddt(),
                                SessionObjectUtil.getConfigObj(session).getPasswordExpire()
                            ),Calendar.DATE
                         );

            if (new Date().after(pwexpdate)) {
               logEvent("User login");
               lr.setStatus(AuthenticatorConstants.PWEXPIRED);
               return lr;
            }

            // Check days until expired
            long daystoexpire=(pwexpdate.getTime()-new Date().getTime())/(1000*60*60*24);
            if (daystoexpire<=7) {
               logEvent("User login");
               lr.setStatus(AuthenticatorConstants.PWEXPIRESSOON);
               lr.setDaystoexpire(daystoexpire);
               return lr;
            }

            // Check if account is expired
            try {
               Date acctexpdate=DateUtils.truncate(luser.getAccountexpiredate(),Calendar.DATE);
               if (acctexpdate!=null && new Date().after(acctexpdate)) {
                  logEvent("User login attempted - account expired");
                  lr.setStatus(AuthenticatorConstants.ACCT_EXPIRED);
                  return lr;
               }
            } catch (Exception aee) {
               // Do nothing - no account expiration date
            }

            // LOGIN OK - REDIRECT USER
            resetInvalidCount();
            // Write role list to auth object
            new RoleResolver(getRequest(),getResponse()).createAuthRroleidList();
            // Return status
            lr.setStatus(AuthenticatorConstants.LOGGED_IN);
            return lr;
  
         } else {
     
            // Check & update BadLoginCount, display result
            euser.setBadlogincount(euser.getBadlogincount().intValue()+1);           

            int badlogins=new Long(euser.getBadlogincount()).intValue();
            int maxbad=SessionObjectUtil.getConfigObj(session).getMaxInvalidLogins();
            if (badlogins<maxbad) {

               lr.setStatus(AuthenticatorConstants.FAILED);
               lr.setBadlogincount(badlogins);
               eusersDAO.saveOrUpdate(euser);
               logInvalidAttempt(user);
               return lr;
  
            } else if (badlogins==maxbad) {
  
               lr.setStatus(AuthenticatorConstants.WARNING);
               lr.setBadlogincount(badlogins);
               eusersDAO.saveOrUpdate(euser);
               logInvalidAttempt(user);
               return lr;
  
            } else {
  
               euser.setIsdisabled(true);
               lr.setStatus(AuthenticatorConstants.EXCESSIVE_LOGINS);
               lr.setBadlogincount(badlogins);
               eusersDAO.saveOrUpdate(euser);
               logInvalidAttempt(user);
               return lr;
  
            }
  
         }
  
      } else {

         // No such user
         logInvalidAttempt(user);
         lr.setStatus(AuthenticatorConstants.FAILED);
         lr.setBadlogincount(-1);
         return lr;
  
      }

   }

   // Reset invalid login attempts count
   private void resetInvalidCount() {

      String remoteip=request.getRemoteAddr();
      List l;
      l=getEmbeddedDAO().execQuery(
         " select il from Invalidlogins as il " +
            " where il.ipaddr=:remoteip ", 
            new String[] { "remoteip" } ,
            new Object[] { remoteip }
                                                 );
      if (l.size()>0) {

         Invalidlogins il=(Invalidlogins)l.get(0);
         il.setCurrcount(new Long(0).longValue());
         getEmbeddedDAO().saveOrUpdate(il);

      }  

   }

   // Log invalid login attempts
   private void logInvalidAttempt(String user) {

      String remoteip=request.getRemoteAddr();
      List l;
      l=getEmbeddedDAO().execQuery(
         " select il from Invalidlogins as il " +
            " where il.ipaddr=:remoteip ", 
            new String[] { "remoteip" } ,
            new Object[] { remoteip }
                                                 );
      if (l.size()>0) {

         Invalidlogins il=(Invalidlogins)l.get(0);
         long currcount=il.getCurrcount();
         long totcount=il.getTotcount();
         currcount++;
         totcount++;
         il.setHostname(request.getRemoteHost());
         il.setCurrcount(currcount);
         il.setTotcount(totcount);
         il.setPrevuname(user);
         il.setPrevtime(new Date());
         getEmbeddedDAO().saveOrUpdate(il);

      }  else {  

         Invalidlogins il=new Invalidlogins();
         il.setIpaddr(remoteip);
         il.setHostname(request.getRemoteHost());
         il.setCurrcount(new Long(1).longValue());
         il.setTotcount(new Long(1).longValue());
         il.setPrevuname(user);
         il.setPrevtime(new Date());
         getEmbeddedDAO().saveOrUpdate(il);

      }
   }

}



