 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 * JoinStudiesResult.java - MRH 03/2007
 */

package com.owarchitects.forms.ssa.comp;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;

public class JoinStudiesResult extends FORMSServiceResult implements Serializable {

   public String studydesc;

   //

   public void setStudydesc(String studydesc) {
      this.studydesc=studydesc;
   }

   public String getStudydesc() {
      return studydesc;
   }

   //

}

