
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/**
 *                                                                               
 * PROGRAM:  FORMSSSaTomcatServer.java                                           
 *                                                                               
 * PURPOSE:  This program provides an interface to start/stop/restart the FORMS  
 *    SSA Tomcat Server.                                                         
 *                                                                               
 */

package com.owarchitects.forms.ssa.startup;

import java.io.*;
import java.net.*;
import java.util.*;
import org.apache.commons.io.FileUtils.*;
import org.apache.commons.lang.ArrayUtils.*;
import HTTPClient.*;
import org.apache.commons.lang.StringUtils;


public class FORMSStartupConfig {

    // Read Startup.conf file
    public static HashMap readConfig(String filepath) {
       try {
          HashMap rm=new HashMap();
          BufferedReader in = new BufferedReader(new FileReader(filepath));
          String line;
          while((line = in.readLine()) != null) {
             if (StringUtils.contains(line.toLowerCase(),"keystore:")) {
                rm.put("keystore",StringUtils.trim(StringUtils.substringAfter(line,"keystore:")));
             } else if (StringUtils.contains(line.toLowerCase(),"keypass:")) {
                rm.put("keypass",StringUtils.trim(StringUtils.substringAfter(line,"keypass:")));
             } else if (StringUtils.contains(line.toLowerCase(),"hostallow:")) {
                rm.put("hostallow",StringUtils.trim(StringUtils.substringAfter(line,"hostallow:")));
             } else if (StringUtils.contains(line.toLowerCase(),"httpport:")) {
                rm.put("httpport",StringUtils.trim(StringUtils.substringAfter(line,"httpport:")));
             } else if (StringUtils.contains(line.toLowerCase(),"httpsport:")) {
                rm.put("httpsport",StringUtils.trim(StringUtils.substringAfter(line,"httpsport:")));
             }
             
          }
          return rm;
       } catch (Exception e) {   
          return null;
       } 
    }

}

