 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  Program:  RecordLockingServiceTarget.java - 
 *                                                          MRH 03/2008
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;

public class RecordLockingServiceTarget extends FORMSServiceTarget {

   // Lock data record
   public SsaServiceStatusResult lockRecord(SsaServicePassObjectParms parms) {
      SsaServiceStatusResult result=new SsaServiceStatusResult();
      try {
         // retrieve datatablerecords object
         Datatablerecords rec=(Datatablerecords)parms.getObject();
         // pull user information
         Allusers user=getAllusers(parms.getLuserid());
         // save locking information
         rec.setIslocked(true);
         rec.setLocktime(new Date(System.currentTimeMillis()));
         rec.setLockuser(user.getAuserid());
         rec.setLockinfo(user.getUserdesc());
         getMainDAO().saveOrUpdate(rec);
         result.setStatus(FORMSServiceConstants.OK);
      } catch (Exception e) {
         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
      }
      return result;
   }

   // unlock data record
   public SsaServiceStatusResult unlockRecord(SsaServicePassObjectParms parms) {
      SsaServiceStatusResult result=new SsaServiceStatusResult();
      try {
         // retrieve datatablerecords object
         Datatablerecords rec=(Datatablerecords)parms.getObject();
         // save locking information
         rec.setIslocked(false);
         rec.setLocktime(null);
         rec.setLockuser(null);
         rec.setLockinfo(null);
         getMainDAO().saveOrUpdate(rec);
         result.setStatus(FORMSServiceConstants.OK);
      } catch (Exception e) {
         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
      }
      return result;
   }

}

