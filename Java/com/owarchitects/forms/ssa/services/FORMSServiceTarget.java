 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 

/*
 *  Program:  FORMSServiceTarget.java - Defines service invocation and helper
 *            methods used by FORMS service target classes
 *                                                                MRH 04/2007
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.beans.*;
import java.lang.reflect.*;
import java.security.*;

import org.springframework.web.servlet.*;
import org.springframework.web.context.support.*;
//import org.apache.axis2.context.*;
//import org.apache.axis2.client.*;
//import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.dbcp.BasicDataSource;
import com.mchange.v2.c3p0.ComboPooledDataSource;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.velocity.Template;
import org.apache.velocity.app.VelocityEngine;

import org.springframework.beans.*;
import org.springframework.beans.factory.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.*;
import org.springframework.orm.hibernate3.*;
import org.springframework.context.support.*;
import org.springframework.transaction.support.TransactionSynchronizationManager;
import org.springframework.web.servlet.*;

import org.hibernate.*;
import org.hibernate.transform.*;


public abstract class FORMSServiceTarget extends AppResourceAccessor {

   // This method must be public to be viewed by reflection in FORMSServiceInterceptor
   public String invokeTargetMethod(HttpServletRequest request,HttpServletResponse response,Object parms) {

      Object o=null;
      Method m=null;
      String luserid;
      this.request=request;
      this.response=response;
      this.session=request.getSession();

      try {

         // Retrieve system parameters
         Class ic=parms.getClass();
         Method im;

         im=ic.getMethod("getMethodname");
         String methodname=(String)im.invoke(parms,new Object[]{ });

         try {
            im=ic.getMethod("getLuserid");
            luserid=(String)im.invoke(parms,new Object[]{ });
         } catch (NoSuchMethodException nse) {
            // CSA doesn't pass luserid
            luserid=null;
         }

         // Retrieve Linkedinstallations record for call

         Class c=this.getClass();

         // Initialize main FORMS database
         initDataSource();

         // Retrieve and invoke requested method

         try {
     
            // return methods with parameters passed
            m=c.getMethod(methodname,new Class[] { ic });
            // Invoke target method
            o=m.invoke(this,new Object[] { parms } );

         } catch (NoSuchMethodException nse) {

            try {

               // return methods with NO parameters passed (SystemOnly)
               m=c.getMethod(methodname,new Class[] { });
               // Invoke target method
               o=m.invoke(this,new Object[] { } );

            } catch (NoSuchMethodException nse2) {

               // return methods with parameters passed as Object
               m=c.getMethod(methodname,new Class[] { Object.class });
               // Invoke target method
               o=m.invoke(this,new Object[] { parms } );

            } 

         }

         // if no specific status has been set by target, set status to OK
         im=o.getClass().getMethod("getStatus");
         Integer currstatus=(Integer)im.invoke(o,new Object[]{ });
         if (currstatus==null) {
            Method rm=o.getClass().getMethod("setStatus",new Class[] { Integer.class });
            Object oj=rm.invoke(o,new Object[] { new Integer(FORMSServiceConstants.OK) } );
         }

         String returnstring=ObjectUtil.objectToEncodedString(o);
         return returnstring;

      }  catch (Exception e) {

         try {

            Class rc=m.getReturnType();
            o=rc.newInstance();
            Method rm=o.getClass().getMethod("setStatus",new Class[] { Integer.class });
            Object oj=rm.invoke(o,new Object[] { new Integer(FORMSServiceConstants.SERVICETARGET_EXCEPTION) } );
            rm=o.getClass().getMethod("setErrormessage",new Class[] { String.class });
            oj=rm.invoke(o,new Object[] { Stack2string.getString(e) } );
            return ObjectUtil.objectToEncodedString(oj);

         } catch (Exception e2) { }   

      }   

      return null;

   }

   // Return auserid (allusers table id) for calling user
   public Allusers getAllusers(String luserid) {
      try {
         if (luserid==null) return null;
         // SSA-Side service call
         Linkedinst li=(Linkedinst)getSession().getAttribute("linkedinst");
         Allusers ai;
         if (li!=null) {
            ai=(Allusers)getMainDAO().execUniqueQuery(
               "select a from Allusers a where a.luserid=" + luserid +
               " and a.allinst.linkedinst.linstid=" + li.getLinstid()
            );
         } else {
            ai=(Allusers)getMainDAO().execUniqueQuery(
               "select a from Allusers a where a.luserid=" + luserid +
               " and a.allinst.linkedinst=null"
            );
         }
         return ai;
      } catch (ClassCastException cce) {
         // CSA-Side service call (fill-in later)
         return null;
      } catch (Exception eee) {
         return null;
      }
   }

   public Long getAuserid(String luserid) {
      if (luserid==null) return null;
      return getAllusers(luserid).getAuserid();
   }

   public final Linkedinst getCallingInst() {
      return (Linkedinst)getSession().getAttribute("linkedinst");
   }

}


