 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  Program:  VerifyAuthService.java - Simply provides auth checking.   Mainly
 *            used by DWR classes to ensure calls originate from valid FORMS 
 *            session.
 *                                                          MRH 09/2007
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;

public class VerifyConnectionServiceTarget extends FORMSServiceTarget {

   // SSA connection verification
   public SsaServiceStatusResult checkStatus(FORMSSsaServiceParms params) {

      SsaServiceStatusResult result=new SsaServiceStatusResult();
      // Verify connection to FORMSMainDB
      try {
         List l=getMainDAO().getTable("Sites");
         if (l.size()>=1) {
            result.setStatus(new Integer(FORMSServiceConstants.OK));
         } else {
            result.setStatus(new Integer(FORMSServiceConstants.NO_DATA_ACCESS));
         }
      } catch (Exception e) {

         result.setStatus(new Integer(FORMSServiceConstants.NO_DATA_ACCESS));
      }

      return result;

   }

   // CSA connection verification
   public CsaServiceStatusResult checkStatus(FORMSCsaServiceParms params) {

      CsaServiceStatusResult result=new CsaServiceStatusResult();
      // Verify connection to FORMSMainDB
      try {
         List l=getMainDAO().getTable("Sites");
         if (l.size()>=1) {
            result.setStatus(new Integer(FORMSServiceConstants.OK));
         } else {
            result.setStatus(new Integer(FORMSServiceConstants.NO_DATA_ACCESS));
         }
      } catch (Exception e) {

         result.setStatus(new Integer(FORMSServiceConstants.NO_DATA_ACCESS));
      }

      return result;

   }

   public Object checkStatus(Object inobj) {

      try {
         // Try passing SSA parameter
         return checkStatus((FORMSSsaServiceParms)inobj);
      } catch (ClassCastException cce) {
         // Must be CSA call
         return checkStatus((FORMSCsaServiceParms)inobj);
            
      }

   }

}

