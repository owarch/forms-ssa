 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  Program:  FormRecordProcessorServiceTarget.java - Transfer SSA meta-data to CSA
 *                                                          MRH 01/2008
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import org.hibernate.*;
//import org.springframework.web.servlet.*;
//import org.springframework.web.servlet.mvc.*;
//import org.springframework.web.context.support.*;
import org.springframework.orm.hibernate3.*;
import java.lang.reflect.*;
//import org.apache.commons.io.FileUtils;


public class FormRecordProcessorServiceTarget extends FORMSSsaServiceClientServiceTarget {

   static Integer LOCALSUBMIT=1;
   static Integer REMOTESUBMIT=2;

   ArrayList keyvarlist;
   ArrayList idvarlist;
   FormDataParser parser;
   String formrecordstring;
   Long dtdefid;
   Long auserid;
   String luserid;
   Linkedinst submitinst;
   private Integer submittype; 

   public FormRecordProcessorResult submitRecord(CsaFormRecordProcessorParms parms) {

      this.dtdefid=parms.getDtdefid();
      this.auserid=parms.getAuserid();
      this.formrecordstring=parms.getDatastring();
      return submitRecord();

   }

   public FormRecordProcessorResult submitRecord(SsaFormRecordProcessorParms parms) {
      try {
         this.dtdefid=parms.getDtdefid();
         this.luserid=parms.getLuserid();
         getAuth().setValue("luserid",luserid);
         this.auserid=getAuserid(this.luserid);
         this.formrecordstring=parms.getDatastring();
         return submitRecord();
      } catch (Exception e) {
         // User has remote permission, but does not have permission on this SSA
         FormRecordProcessorResult result=new FormRecordProcessorResult();
         result.setTransferstatus(FormRecordProcessorConstants.PERMERROR);
         return result;
      }
   }

   public FormRecordProcessorResult submitRecord() {

      FormRecordProcessorResult result=new FormRecordProcessorResult();

      try {

         // GRAB KEY FIELD VALUES IF APPLICABLE  

         List fkl=getMainDAO().execQuery(
            "select f from Datatablecoldef f join fetch f.datatabledef where f.datatabledef.dtdefid=" + dtdefid + 
                " and f.iskeyfield=true " +
                "order by f.columnorder"
            );

         List ecl=getMainDAO().execQuery(
            "select f from Datatablecoldef f join fetch f.datatabledef where f.datatabledef.dtdefid=" + dtdefid + 
                " and f.isecode=true " +
                "order by f.columnorder"
            );

         String ecodeVar=null;
         long ecodeVal=-1;
         if (ecl.size()>0) {
            try {
               ecodeVar=((Datatablecoldef)ecl.get(0)).getColumnname();
               ecodeVal=new Long(FormDataUtil.getFieldValue(formrecordstring,ecodeVar)).longValue();
            } catch (Exception e) {
               ecodeVar=null;
            }
         }

         String formacr="";   
         Iterator fki=fkl.iterator();
         keyvarlist=new ArrayList();
         idvarlist=new ArrayList();
         // Create input string for ecode value comparison
         StringBuilder ecodesb=new StringBuilder();
         while (fki.hasNext()) {
            Datatablecoldef fcd=(Datatablecoldef)fki.next();
            formacr=fcd.getDatatabledef().getFormacr();
            String kvname=fcd.getColumnname().toLowerCase();
            String kvvalue=FormDataUtil.getFieldValue(formrecordstring,kvname);
            HashMap keyvarmap=new HashMap();
            keyvarmap.put("keyvar",kvname.toUpperCase());
            keyvarmap.put("keyval",kvvalue);
            keyvarlist.add(keyvarmap);
            if (fcd.getIsidfield()) {
               idvarlist.add(keyvarmap);
               ecodesb.append(kvvalue + "-");
            }
         }
         ecodesb.deleteCharAt(ecodesb.lastIndexOf("-"));

         // Create new formdataparser, creating backup javascript and arraylist containing
         // field names & values

         parser=new FormDataParser(formrecordstring);

         // If form is a remote installation form, submit to remote inst   
         Long ainstid=(Long)getMainDAO().execUniqueQuery(
            "select f.allinst.ainstid from Datatabledef f " +
               "where f.dtdefid=" + dtdefid +  "and f.allinst.islocal!=true"
               );
         if (ainstid==null) {
            try {
               getAuth().setObjectValue("submitinst",null);
               getAuth().setObjectValue("submittype",LOCALSUBMIT);

            } catch (Exception aex) {
               // Remote SSA cannot call auth
            }
         } else {
            //
            // remote submission
            //
            getAuth().setObjectValue("submittype",REMOTESUBMIT);

            try {

               // Check ecode
               if (ecodeVar!=null) {
                  if (new Long(EcodeUtil.getEcode(ecodesb.toString())).longValue()!=ecodeVal) {
                     result.setTransferstatus(FormRecordProcessorConstants.ECODEPROB);
                     result.setInfostring(ecodesb.toString());
                     getAuth().setObjectValue("submitinst",submitinst);
                     getAuth().setObjectValue("keyvarlist_",keyvarlist);
                     getAuth().setObjectValue("parser_",null);
                     getAuth().setObjectValue("datastring_",formrecordstring);
                     getAuth().setObjectValue("dtdefid_",dtdefid);
                     getAuth().setObjectValue("auserid_",auserid);
                     return result;
                  }
               }

               // pull linkedinst record for submission
               submitinst=getLinkedinst(ainstid);
               // Send records to service target
               SsaFormRecordProcessorParms parms=new SsaFormRecordProcessorParms();
               parms.setDatastring(formrecordstring);
               parms.setDtdefid(getRemotedtdefid().longValue());
               parms.setLuserid(this.luserid);
               parms.setBeanname("formRecordProcessorServiceTarget");
               parms.setMethodname("submitRecord");
      
               FormRecordProcessorResult remoteresult=(FormRecordProcessorResult)this.submitServiceRequest(submitinst,parms);
               int transferstatus=remoteresult.getTransferstatus();
               
               if (transferstatus==FormRecordProcessorConstants.RECORD_LOCKED) {

                  // Save to sync directory
                  result.setTransferstatus(transferstatus);
                  return result;
      
               } else if (transferstatus==FormRecordProcessorConstants.ERROR) {

                  // Save to sync directory
                  result.setTransferstatus(FormRecordProcessorConstants.WRITE_TO_SYNC);
                  return result;
      
               }
               
               String fieldstr=new FormRecordProcessorConstants().getFieldName(transferstatus);

               if (fieldstr.equals("ALREADY_EXISTS")) {
                  result.setTransferstatus(FormRecordProcessorConstants.ALREADY_EXISTS);
                  getAuth().setObjectValue("submitinst",submitinst);
                  getAuth().setObjectValue("keyvarlist_",keyvarlist);
                  getAuth().setObjectValue("parser_",null);
                  getAuth().setObjectValue("datastring_",formrecordstring);
                  getAuth().setObjectValue("dtdefid_",dtdefid);
                  getAuth().setObjectValue("auserid_",auserid);
                  return result;
               } else if (fieldstr.equals("ALREADY_NOTPERMITTED")) {
                  result.setTransferstatus(FormRecordProcessorConstants.ALREADY_NOTPERMITTED);
                  getAuth().setObjectValue("submitinst",submitinst);
                  getAuth().setObjectValue("keyvarlist_",keyvarlist);
                  getAuth().setObjectValue("parser_",null);
                  getAuth().setObjectValue("datastring_",formrecordstring);
                  getAuth().setObjectValue("dtdefid_",dtdefid);
                  getAuth().setObjectValue("auserid_",auserid);
                  return result;
               }
               result.setTransferstatus(transferstatus);
               return result;
            } catch (Exception sex) {

               // Save to sync directory
               result.setTransferstatus(FormRecordProcessorConstants.WRITE_TO_SYNC);
               return result;

            }
         } 

         PermHelper permhelper=new PermHelper(getRequest(),getResponse(),true);

         // Pull user permissions
         boolean hasperm_newrecord=false;
         boolean hasperm_editown=false;
         boolean hasperm_editother=false;
         try {
            hasperm_newrecord=permhelper.hasFormPerm(auserid.toString(),dtdefid.toString(),"NEWRECORD");
            hasperm_editown=permhelper.hasFormPerm(auserid.toString(),dtdefid.toString(),"EDITOWN");
            hasperm_editother=permhelper.hasFormPerm(auserid.toString(),dtdefid.toString(),"EDITOTHER");
         } catch (Exception permex) {
            // do nothing, no permission
         }

         if (!(hasperm_newrecord || hasperm_editown || hasperm_editother)) {
            result.setTransferstatus(FormRecordProcessorConstants.NOTPERMITTED);
            return result;
         }

         // Check ecode
         if (ecodeVar!=null) {
            if (new Long(EcodeUtil.getEcode(ecodesb.toString())).longValue()!=ecodeVal) {
               FORMSAuth auth=getAuth();
               result.setTransferstatus(FormRecordProcessorConstants.ECODEPROB);
               result.setInfostring(ecodesb.toString());
               auth.setObjectValue("keyvarlist_",keyvarlist);
               auth.setObjectValue("parser_",parser);
               auth.setObjectValue("datastring_",null);
               auth.setObjectValue("dtdefid_",dtdefid);
               auth.setObjectValue("auserid_",auserid);
               return result;
            }
         }

         // 
         // See if record already exists for key value combo
         // 

         int keymatch=keyvarlist.size();
         if (keymatch>0) {

            // create keyvar component of whereclause
            StringBuilder sbwhere=new StringBuilder();
            StringBuilder sbinfo=new StringBuilder("FORM=" + formacr + ", &nbsp;" );
            Iterator i=keyvarlist.iterator();
            while (i.hasNext()) {
               HashMap map=(HashMap)i.next();
               String keyvar=(String)map.get("keyvar");
               String keyval=(String)map.get("keyval");
               sbwhere.append(" (fdv.varname='" + keyvar + "' and " +
                                "fdv.varvalue='" + keyval + "') ");
               sbinfo.append(keyvar + "=" + keyval);
               if (i.hasNext()) {
                  sbwhere.append(" or ");            
                  sbinfo.append(", &nbsp;");            
               }   
            }
            result.setInfostring(sbinfo.toString());

            // get count of matching key values by DataRecordId
            List l=getMainDAO().execQuery(
               "select fdv.datatablerecords.datarecordid,count(fdv.datatablerecords.datarecordid) from " +
                  "Datatablevalues fdv " +
                  "inner join fdv.datatablerecords " +
                  "inner join fdv.datatablerecords.datatabledef " +
                  "where (fdv.datatablerecords.datatabledef.dtdefid=" + dtdefid + ") and " +
                     "(fdv.datatablerecords.ishold!=true) and " +
                     "(fdv.datatablerecords.isaudit!=true) and " +
                     "(" + sbwhere.toString() + ")" +
                  "group by fdv.datatablerecords.datarecordid"
                );

            // If count>=number of keyvars, there is a match (NOTE: should never be greater)
            if (l.size()>0) {

               Iterator aiter=l.iterator();
               while (aiter.hasNext()) {
                  Object[] aarray=(Object[])aiter.next();
                  Long datarecordid=(Long)aarray[0];
                  Long nmatch=(Long)aarray[1];
                  if (nmatch.longValue()>=keyvarlist.size()) {
                     // Save record objects for retrieval
                     try {
                        FORMSAuth auth=getAuth();
                        auth.setObjectValue("keyvarlist_",keyvarlist);
                        auth.setObjectValue("parser_",parser);
                        auth.setObjectValue("datastring_",null);
                        auth.setObjectValue("dtdefid_",dtdefid);
                        auth.setObjectValue("auserid_",auserid);
                        auth.setObjectValue("datarecordid_",datarecordid);
                     } catch (FORMSException authe) {
                        // Remote SSA users have no access to auth object (no encryption key).
                        // They must must resend data string
                     }
                     // see if record is locked by other user
                     if (recordIsLocked(datarecordid,auserid)) {
                        result.setTransferstatus(FormRecordProcessorConstants.RECORD_LOCKED);
                     } else {
                        // If record exists, make sure user has permission to overwrite it
                        Datatablerecords rec=(Datatablerecords)getMainDAO().execUniqueQuery(
                           "select f from Datatablerecords f where f.datarecordid=" + datarecordid
                           );
                        boolean permitted=true;   
                        if (rec!=null && (rec.getSaveuser()==auserid.longValue())) {
                           if (!hasperm_editown) permitted=false;
                        }
                        if (rec!=null && (rec.getSaveuser()!=auserid.longValue())) {
                           if (!hasperm_editother) permitted=false;
                        }
                        if (permitted) result.setTransferstatus(FormRecordProcessorConstants.ALREADY_EXISTS);
                        else result.setTransferstatus(FormRecordProcessorConstants.ALREADY_NOTPERMITTED);
                     }
                     return result;
                  }
               }

            }

            // Check against enrollment form if requested
            String checkenroll=FormDataUtil.getFieldValue(formrecordstring,"checkenrollform_");
            if (checkenroll.equals("Y")) {

               // pull ID field(s) from earlier keyfieldlist
               ArrayList idlist=new ArrayList();
               fki=fkl.iterator();
               StringBuilder fsb=new StringBuilder();
               while (fki.hasNext()) {
                  Datatablecoldef f=(Datatablecoldef)fki.next();
                  if (f.getIsidfield()) {
                     idlist.add(f);
                     fsb.append(f.getColumnname() + ',');
                  }
               }
               fsb.deleteCharAt(fsb.lastIndexOf(","));

               Datatabledef cdatatabledef=(Datatabledef)getMainDAO().execUniqueQuery("select f from Datatabledef f where f.dtdefid=" + dtdefid);
               Formha fha=(Formha)getMainDAO().execUniqueQuery(
                  "select f from Formha f join fetch f.studies " +
                     "where f.formhaid=" + cdatatabledef.getPformhaid()
                     );

               // return study visibility heirarchy records create a stringbuffer containing ids
               List fhatemp=getMainDAO().execQuery(
                  "select fha from Formha fha " +
                        "where fha.visibility=" + VisibilityConstants.STUDY + " and fha.studies.studyid=" + fha.getStudies().getStudyid()
                  );

               i=fhatemp.iterator();
               StringBuilder sb=new StringBuilder();
               boolean vismatch=false;
               while (i.hasNext()) {
                  Formha ha=(Formha)i.next();
                  // write id to stringbuffer
                  sb.append(new Long(ha.getFormhaid()).toString());
                  if (i.hasNext()) sb.append(",");
                  // make sure that current form has study visibility
                  if (cdatatabledef.getPformhaid()==ha.getFormhaid()) {
                     vismatch=true;
                  }
               }

               // make sure current form has all ids assigned and create a where clause
               i=idvarlist.iterator();
               boolean hasallkeyvalues=true;
               StringBuilder sbinfo2=new StringBuilder("FORM=" + formacr + ", &nbsp;" );
               StringBuilder sbwhere2=new StringBuilder();
               while (i.hasNext()) {
                  HashMap map=(HashMap)i.next();
                  String keyvar=(String)map.get("keyvar");
                  String keyval=(String)map.get("keyval");
                  if (keyval==null || keyval.trim().length()<1) {
                     hasallkeyvalues=false;
                  }
                  sbwhere2.append(" (fdv.varname='" + keyvar + "' and " +
                                   "fdv.varvalue='" + keyval + "') ");
                  sbinfo2.append(keyvar + "=" + keyval);
                  if (i.hasNext()) {
                     sbwhere2.append(" or ");            
                     sbinfo2.append(",&nbsp;");            
                  }   
               }

               result.setInfostring(sbinfo2.toString());

               boolean hasmatch=true;

               // check all enrollment forms for matching id fields and values
               if (vismatch && hasallkeyvalues) {
                  hasmatch=false;
                  List fdefl=getMainDAO().execQuery(
                     "select f from Datatabledef f " +
                        " where f.isenrollform=true  and f.pformhaid in (" + sb.toString() + ") and f.dtdefid!=" + dtdefid
                        );
                  Iterator fdefi=fdefl.iterator();
                  while (fdefi.hasNext()) {
                     Datatabledef fdef=(Datatabledef)fdefi.next();
                     l=getMainDAO().execQuery(
                        "select fdv.datatablerecords.datarecordid,count(fdv.datatablerecords.datarecordid) from " +
                           "Datatablevalues fdv " +
                           "inner join fdv.datatablerecords " +
                           "inner join fdv.datatablerecords.datatabledef " +
                           "where (fdv.datatablerecords.datatabledef.dtdefid=" + fdef.getDtdefid() + ") and " +
                              "(fdv.datatablerecords.ishold!=true) and " +
                              "(fdv.datatablerecords.isaudit!=true) and " +
                              "(" + sbwhere2.toString() + ")" +
                           "group by fdv.datatablerecords.datarecordid"
                         );
                     i=l.iterator();
                     Iterator aiter=l.iterator();
                     while (aiter.hasNext()) {
                        Object[] aarray=(Object[])aiter.next();
                        Long nmatch=(Long)aarray[1];
                        if (nmatch.intValue()==idvarlist.size()) {
                           hasmatch=true;    
                           break;
                        }   
                     }   
                     if (hasmatch) break;
                  }
               }

               // if no matching enrollment records return 
               if (!hasmatch) {
                  // Save record objects for retrieval
                  FORMSAuth auth=getAuth();
                  auth.setObjectValue("keyvarlist_",keyvarlist);
                  auth.setObjectValue("parser_",parser);
                  auth.setObjectValue("datastring_",null);
                  auth.setObjectValue("dtdefid_",dtdefid);
                  auth.setObjectValue("auserid_",auserid);
                  result.setTransferstatus(FormRecordProcessorConstants.NOENROLLMENT);
                  return result;
               }   

            }

         }

         // Append new record to database
         appendRecord();
         result.setTransferstatus(FormRecordProcessorConstants.APPENDED);

         return result;

      } catch (Exception e) {

         result.setTransferstatus(FormRecordProcessorConstants.ERROR);
         return result;

      }
      
   }

   private boolean recordIsLocked(Long datarecordid,Long auserid) {
      try {
         Datatablerecords f=(Datatablerecords)getMainDAO().execUniqueQuery(
            "select f from Datatablerecords f where f.datarecordid=" + datarecordid + " and f.islocked=true and f.lockuser!=" + auserid
            );
         if (f!=null) {
            return true;
         }
      } catch (Exception e) { }
      return false;
   }

   // Replace existing record
   public FormRecordProcessorResult submitReplace() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      FormRecordProcessorResult result=new FormRecordProcessorResult();

      try {

         retrieveAuthValues();

         if (submittype==null) {
            Datatabledef dtd=(Datatabledef)getMainDAO().execUniqueQuery("select d from Datatabledef d where d.dtdefid=" + dtdefid);
            if (dtd.getRemotedtdefid()!=null) {
               submitinst=getLinkedinst(dtd.getAllinst().getAinstid());
               submittype=REMOTESUBMIT;
            } else {
               submittype=LOCALSUBMIT;
            }
         }

         if (submittype!=null && submittype.equals(REMOTESUBMIT)) {
            try {
               // Send records to service target
               SsaFormRecordProcessorParms parms=new SsaFormRecordProcessorParms();
               parms.setDatastring(formrecordstring);
               parms.setDtdefid(getRemotedtdefid().longValue());
               parms.setLuserid(this.luserid);
               parms.setBeanname("formRecordProcessorServiceTarget");
               parms.setMethodname("submitReplace");
      
               FormRecordProcessorResult remoteresult=(FormRecordProcessorResult)this.submitServiceRequest(submitinst,parms);
               int transferstatus=remoteresult.getTransferstatus();
   
               if (transferstatus==FormRecordProcessorConstants.RECORD_LOCKED) {

                  // CSA hasn't maintained info to write to sync
                  result.setTransferstatus(transferstatus);
                  return result;
      
               } else if (transferstatus==FormRecordProcessorConstants.ERROR) {

                  // CSA hasn't maintained info to write to sync
                  result.setTransferstatus(FormRecordProcessorConstants.ERROR);
                  return result;
      
               }
                
               result.setTransferstatus(remoteresult.getTransferstatus());
               return result;
   
            } catch (Exception sex) {

               // CSA hasn't maintained info to write to sync
               result.setTransferstatus(FormRecordProcessorConstants.ERROR);
               return result;
   
            }
         }

         // create keyvar component of whereclause
         StringBuilder sbwhere=new StringBuilder();
         StringBuilder sbinfo=new StringBuilder();
         Iterator i=keyvarlist.iterator();
         while (i.hasNext()) {
            HashMap map=(HashMap)i.next();
            String keyvar=(String)map.get("keyvar");
            String keyval=(String)map.get("keyval");
            sbwhere.append(" (fdv.varname='" + keyvar + "' and " +
                             "fdv.varvalue='" + keyval + "') ");
            sbinfo.append(keyvar + "=" + keyval);
            if (i.hasNext()) {
               sbwhere.append(" or ");            
               sbinfo.append(",&nbsp;");            
            }   
         }
   
         // Find matching record
         List l=getMainDAO().execQuery(
            "select fdv.datatablerecords.datarecordid,count(fdv.datatablerecords.datarecordid) from " +
               "Datatablevalues fdv " +
               "inner join fdv.datatablerecords " +
               "inner join fdv.datatablerecords.datatabledef " +
               "where (fdv.datatablerecords.datatabledef.dtdefid=" + dtdefid + ") and " +
                  "(fdv.datatablerecords.ishold!=true) and " +
                  "(fdv.datatablerecords.isaudit!=true) and " +
                  "(" + sbwhere.toString() + ")" +
               "group by fdv.datatablerecords.datarecordid"
             );
   
         // If count>=number of keyvars, there is a match (NOTE: should never be greater)
         if (l.size()>0) {
            Iterator aiter=l.iterator();
            while (aiter.hasNext()) {
               Object[] aarray=(Object[])aiter.next();
               Long datarecordid=(Long)aarray[0];
               Long nmatch=(Long)aarray[1];
               if (nmatch.longValue()>=keyvarlist.size()) {
                  // Set record object to replaced
                  Datatablerecords fdr=(Datatablerecords)getMainDAO().execUniqueQuery(
                     "select f from Datatablerecords f where f.datarecordid=" + datarecordid
                     );
   
                  if (fdr!=null) {
                     fdr.setIsaudit(true);
                     fdr.setAudittype('R');
                     fdr.setAudittime(new Date());
                     fdr.setAudituser(auserid.longValue());
                     getMainDAO().saveOrUpdate(fdr,false);
                  }
                  // Append replacement record
                  appendRecord();
                  result.setTransferstatus(FormRecordProcessorConstants.REPLACED);
                  clearAuthValues();
                  return result;
               }
   
            }
         }
         result.setTransferstatus(FormRecordProcessorConstants.NO_MATCHING_RECORD);
         clearAuthValues();
         return result;

     } catch (Exception e) {

         result.setTransferstatus(FormRecordProcessorConstants.ERROR);
         clearAuthValues();
         return result;

      }

   }

   // CSA call
   public FormRecordProcessorResult submitReplace(CsaFormRecordProcessorParms parms) {
      try {

         this.dtdefid=parms.getDtdefid();
         this.auserid=parms.getAuserid();
         setLuserid(auserid);
         this.parser=new FormDataParser(parms.getDatastring());
         return submitReplace();

      } catch (Exception e) {

         // User has remote permission, but does not have permission on this SSA
         FormRecordProcessorResult result=new FormRecordProcessorResult();
         result.setTransferstatus(FormRecordProcessorConstants.PERMERROR);
         return result;

      }
   }

   // Remote SSA call
   public FormRecordProcessorResult submitReplace(SsaFormRecordProcessorParms parms) {
      try {
         this.dtdefid=parms.getDtdefid();
         this.auserid=getAuserid(parms.getLuserid());
         this.parser=new FormDataParser(parms.getDatastring());
         return submitReplace();
      } catch (Exception e) {
         // User has remote permission, but does not have permission on this SSA
         FormRecordProcessorResult result=new FormRecordProcessorResult();
         result.setTransferstatus(FormRecordProcessorConstants.PERMERROR);
         return result;
      }
   }

   // Submit record to hold database
   public FormRecordProcessorResult submitHold() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      retrieveAuthValues();

      FormRecordProcessorResult result=new FormRecordProcessorResult();

      if (submittype==null) {
         Datatabledef dtd=(Datatabledef)getMainDAO().execUniqueQuery("select d from Datatabledef d where d.dtdefid=" + dtdefid);
         if (dtd.getRemotedtdefid()!=null) {
            submitinst=getLinkedinst(dtd.getAllinst().getAinstid());
            submittype=REMOTESUBMIT;
         } else {
            submittype=LOCALSUBMIT;
         }
      }

      if (submittype!=null && submittype.equals(REMOTESUBMIT)) {

         try {
            // Send records to service target
            SsaFormRecordProcessorParms parms=new SsaFormRecordProcessorParms();
            parms.setDatastring(formrecordstring);
            parms.setDtdefid(getRemotedtdefid().longValue());
            parms.setLuserid(this.luserid);
            parms.setBeanname("formRecordProcessorServiceTarget");
            parms.setMethodname("submitHold");
   
            FormRecordProcessorResult remoteresult=(FormRecordProcessorResult)this.submitServiceRequest(submitinst,parms);
            int transferstatus=remoteresult.getTransferstatus();

            if (transferstatus==FormRecordProcessorConstants.ERROR) {

               // CSA hasn't maintained info to write to sync
               result.setTransferstatus(FormRecordProcessorConstants.ERROR);
               return result;
   
            }

            result.setTransferstatus(remoteresult.getTransferstatus());
            return result;

         } catch (Exception sex) {

            // CSA hasn't maintained info to write to sync
            result.setTransferstatus(FormRecordProcessorConstants.ERROR);
            return result;

         }
      }

      try {

         // Append replacement record
         appendRecord(true);
         result.setTransferstatus(FormRecordProcessorConstants.HOLDAPPEND);

         clearAuthValues();
         return result;

      } catch (Exception e) {

         result.setTransferstatus(FormRecordProcessorConstants.ERROR);
         clearAuthValues();
         return result;

      }

   }

   // Remote SSA call
   public FormRecordProcessorResult submitHold(CsaFormRecordProcessorParms parms) {

      try {

         this.dtdefid=parms.getDtdefid();
         this.auserid=parms.getAuserid();
         setLuserid(auserid);
         this.parser=new FormDataParser(parms.getDatastring());
         return submitHold();

      } catch (Exception e) {

         // User has remote permission, but does not have permission on this SSA
         FormRecordProcessorResult result=new FormRecordProcessorResult();
         result.setTransferstatus(FormRecordProcessorConstants.PERMERROR);
         return result;
      }
   }

   // Remote SSA call
   public FormRecordProcessorResult submitHold(SsaFormRecordProcessorParms parms) {

      try {

         this.dtdefid=parms.getDtdefid();
         this.auserid=getAuserid(parms.getLuserid());
         this.parser=new FormDataParser(parms.getDatastring());
         return submitHold();

      } catch (Exception e) {

         // User has remote permission, but does not have permission on this SSA
         FormRecordProcessorResult result=new FormRecordProcessorResult();
         result.setTransferstatus(FormRecordProcessorConstants.PERMERROR);
         return result;
      }
   }

   public SsaServiceStatusResult deleteRecord(SsaServicePassObjectParms parms) {

      SsaServiceStatusResult result=new SsaServiceStatusResult();

      try {

         Datatablerecords rec=(Datatablerecords)parms.getObject();
         rec.setIsaudit(true);
         rec.setAudittype('D');
         rec.setAudituser(getAuserid(parms.getLuserid()));
         rec.setAudittime(new Date());
         getMainDAO().saveOrUpdate(rec);
   
         result.setStatus(new Integer(FORMSServiceConstants.OK));
         return result;

      } catch (Exception e) {

         result.setStatus(new Integer(FORMSServiceConstants.SERVICETARGET_EXCEPTION));
         return result;

      }

   }

   // Submit anyway (Even though no enrollment record)
   public FormRecordProcessorResult submitAnyway() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      retrieveAuthValues();

      FormRecordProcessorResult result=new FormRecordProcessorResult();

      if (submittype==null) {
         Datatabledef dtd=(Datatabledef)getMainDAO().execUniqueQuery("select d from Datatabledef d where d.dtdefid=" + dtdefid);
         if (dtd.getRemotedtdefid()!=null) {
            submitinst=getLinkedinst(dtd.getAllinst().getAinstid());
            submittype=REMOTESUBMIT;
         } else {
            submittype=LOCALSUBMIT;
         }
      }

      if (submittype!=null && submittype.equals(REMOTESUBMIT)) {
         try {
            // Send records to service target
            SsaFormRecordProcessorParms parms=new SsaFormRecordProcessorParms();
            parms.setDatastring(formrecordstring);
            parms.setDtdefid(getRemotedtdefid().longValue());
            parms.setLuserid(this.luserid);
            parms.setBeanname("formRecordProcessorServiceTarget");
            parms.setMethodname("submitAnyway");

            FormRecordProcessorResult remoteresult=(FormRecordProcessorResult)this.submitServiceRequest(submitinst,parms);
            int transferstatus=remoteresult.getTransferstatus();

            if (transferstatus==FormRecordProcessorConstants.ERROR) {

               // CSA hasn't maintained info to write to sync
               result.setTransferstatus(FormRecordProcessorConstants.ERROR);
               return result;
   
            }
             
            result.setTransferstatus(remoteresult.getTransferstatus());
            return result;

         } catch (Exception sex) {

            // CSA hasn't maintained info to write to sync
            result.setTransferstatus(FormRecordProcessorConstants.ERROR);
            return result;

         }
      }
      
      try {

         // Append record
         appendRecord();
         result.setTransferstatus(FormRecordProcessorConstants.APPENDED);
         clearAuthValues();
         return result;

      } catch (Exception e) {

         result.setTransferstatus(FormRecordProcessorConstants.ERROR);
         clearAuthValues();
         return result;

      }

   }

   // CSA call
   public FormRecordProcessorResult submitAnyway(CsaFormRecordProcessorParms parms) {

      try {

         this.dtdefid=parms.getDtdefid();
         this.auserid=parms.getAuserid();
         setLuserid(auserid);
         this.parser=new FormDataParser(parms.getDatastring());
         return submitAnyway();

      } catch (Exception e) {

         // User has remote permission, but does not have permission on this SSA
         FormRecordProcessorResult result=new FormRecordProcessorResult();
         result.setTransferstatus(FormRecordProcessorConstants.PERMERROR);
         return result;

      }
   }

   // Remote SSA call
   public FormRecordProcessorResult submitAnyway(SsaFormRecordProcessorParms parms) {

      try {
         this.dtdefid=parms.getDtdefid();
         this.auserid=getAuserid(parms.getLuserid());
         this.parser=new FormDataParser(parms.getDatastring());
         return submitAnyway();
      } catch (Exception e) {

         // User has remote permission, but does not have permission on this SSA
         FormRecordProcessorResult result=new FormRecordProcessorResult();
         result.setTransferstatus(FormRecordProcessorConstants.PERMERROR);
         return result;
      }
   }

   // append record to database
   private void appendRecord(boolean ishold) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      Datatabledef datatabledef=(Datatabledef)getMainDAO().execUniqueQuery(
         "select f from Datatabledef f where f.dtdefid=" + dtdefid
         );


      Datatablerecords newrecord=new Datatablerecords();
      newrecord.setDatatabledef(datatabledef);
      newrecord.setIshold(ishold);
      newrecord.setIsaudit(false);
      newrecord.setIslocked(false);
      newrecord.setSavetime(new Date());
      newrecord.setSaveuser(auserid.longValue());
      getMainDAO().save(newrecord,false);

      // Append field records

      List fieldlist=parser.getFieldList();
      Iterator fiter=fieldlist.iterator();
      while (fiter.hasNext()) {
         HashMap fmap=(HashMap)fiter.next();
         Datatablevalues newvalue=new Datatablevalues();
         newvalue.setDatatablerecords(newrecord);
         newvalue.setVarname(((String)fmap.get("fieldname")).toUpperCase());
         newvalue.setVarvalue((String)fmap.get("fieldvalue"));
         getMainDAO().save(newvalue,false);
      }

   }

   private Long getRemotedtdefid() {

      return (Long)getMainDAO().execUniqueQuery("select f.remotedtdefid from Datatabledef f where f.dtdefid=" + dtdefid);

   }

   // append record to database
   private void appendRecord() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      appendRecord(false);
   }

   private void retrieveAuthValues() {

      try {
         FORMSAuth auth=getAuth();
         Object o;
         o=auth.getObjectValue("keyvarlist_");
         if (o!=null) keyvarlist=(ArrayList)o;
         o=auth.getObjectValue("parser_");
         if (o!=null) parser=(FormDataParser)o;
         o=auth.getObjectValue("datastring_");
         if (o!=null) formrecordstring=(String)o;
         o=auth.getObjectValue("dtdefid_");
         if (o!=null) dtdefid=(Long)o;
         o=auth.getObjectValue("auserid_");
         if (o!=null) auserid=(Long)o;
         o=auth.getObjectValue("submitinst");
         if (o!=null) submitinst=(Linkedinst)o;
         o=auth.getObjectValue("submittype");
         if (o!=null) submittype=(Integer)o;
      } catch (Exception authe) {
         // Remote SSA users have no access to auth object (no encryption key).
         // They must must resend data string

         // NOTE:  Remote SSA users will always be sending records to this server (LOCALSUBMIT)
         submittype=LOCALSUBMIT;
      }

   }   

   private void clearAuthValues() {

      try {
         FORMSAuth auth=getAuth();
         auth.setObjectValue("keyvarlist_",null);
         auth.setObjectValue("parser_",null);
         auth.setObjectValue("datastring_",null);
         auth.setObjectValue("dtdefid_",null);
         auth.setObjectValue("auserid_",null);
         auth.setObjectValue("submitinst",null);
         auth.setObjectValue("submittype",null);
      } catch (Exception authe) {
         // Remote SSA users have no access to auth object (no encryption key).
         // They must must resend data string
      }

   }   

   private void setLuserid(long auserid) {
      this.luserid=getMainDAO().execUniqueQuery("select luserid from Allusers a where a.auserid=" + auserid).toString();
   }

   private Linkedinst getLinkedinst(long ainstid) {
      return (Linkedinst)getMainDAO().execUniqueQuery("select a.linkedinst from Allinst a where a.ainstid=" + ainstid);
   }

}


