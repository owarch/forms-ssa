 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  Program:  UpdateInstInfoSoapTarget.java - Update remote installation users and form data
 *                                                          MRH 08/2008
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import java.sql.Timestamp;

public class SsaUpdateInstInfoSoapTarget extends FORMSSsaServiceClientSoapTarget {

   // called by LoginController to asyncronously update local installation with remote installation data
   public void updateRemoteInstInfo(InstInfoParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      if (parms.getNeeduserupdate()) {
         // Update allusers for remote installation users
         updateAllusers();
         // update local mod time
      }
      if (parms.getNeedformupdate()) {
         // Update form meta-data
         updateRemoteForms();
         // update local mod time
      }

   }

   //////
   //////
   //////

   // Update Allusers with current info from remote sites, create session list of study info
   private void updateAllusers() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {

            // This list holds the user list for each study.  This list will be written
            // to the session and be obtainable when needed
            List ruserlist=(List)new ArrayList();

            List lslist=getMainDAO().execQuery(
               "select l from Linkedstudies l join fetch l.linkedinst"
               );

            // This list holds unique users for all studies
            List auserlist=(List)new ArrayList();

            // Iterate through linked studies
            Iterator lsiter=lslist.iterator();

            while (lsiter.hasNext()) {

               // Pull user list for each study
               Linkedstudies ls=(Linkedstudies)lsiter.next();

               Linkedinst linst=ls.getLinkedinst();

               SsaServiceSystemStudyParms parms=new SsaServiceSystemStudyParms("studyUserListServiceTarget","getUserList");
               // Note we are not calling submitServiceStudyRequest here.  We're setting the
               // study parameters here rather than relying on the method to do it using
               // auth values

               String studyid=new Long(ls.getStudies().getStudyid()).toString();
               parms.setOristudyid(studyid);
               parms.setDeststudyid(new Long(ls.getRemotestudyid()).toString());

               SsaServiceListResult result=(SsaServiceListResult)submitServiceRequest(linst,parms);

               List l=result.getList();

               // Update auserlist (list for updating Allusers)
               Iterator i=l.iterator();
               while (i.hasNext()) {
                  Allusers user=(Allusers)i.next();
                  HashMap aumap=new HashMap();
                  aumap.put("linstid",new Long(linst.getLinstid()).toString());
                  aumap.put("linstacr",linst.getAcronym());
                  aumap.put("user",user);
                  auserlist.add(aumap);
               }

               HashMap map=new HashMap();
               map.put("studyid",studyid); 
               map.put("userlist",l); 
               ruserlist.add(map);

            }

            getSession().setAttribute("ruserlist",ruserlist);

            // Update Allusers from auserlist
            List sulist=getMainDAO().execQuery(
               "select a from Allusers a join fetch a.allinst join fetch a.allinst.linkedinst " +
                  "where a.allinst.islocal=false"
               );

            Iterator auiter=auserlist.iterator();
            while (auiter.hasNext()) {
               HashMap umap=(HashMap)auiter.next();
               String linstid=(String)umap.get("linstid");
               String linstacr=(String)umap.get("linstacr");
               Allusers user=(Allusers)umap.get("user");
               Iterator suiter=sulist.iterator();
               boolean dmatch=false;

               Allinst ainst=(Allinst)getMainDAO().execUniqueQuery(
                  "select a from Allinst a left join fetch a.linkedinst where a.linkedinst.linstid=" + linstid
                  );

               while (suiter.hasNext()) {
                  Allusers suser=(Allusers)suiter.next();

                  if (new Long(linstid).longValue()==suser.getAllinst().getLinkedinst().getLinstid()) {

                     if (user.getLuserid()==suser.getLuserid()) {
   
                        boolean needmod=false; 
                        if (suser.getIsdeleted()) {
                           suser.setIsdeleted(false);
                           needmod=true;
                        }
                        if (!(user.getUserdesc().substring(0,user.getUserdesc().indexOf("[")).equals(
                              suser.getUserdesc().substring(0,suser.getUserdesc().indexOf("["))
                           ))) {
                           suser.setUserdesc(user.getUserdesc().substring(0,user.getUserdesc().indexOf("[")) +
                                "[" + linstacr + "]");
                           needmod=true;     
                        }   
                        if (needmod) {
                           getMainDAO().saveOrUpdate(suser);
                        }

                        dmatch=true;
                        // remove from lists for next step checking for allusers
                        // to remove from database
                        auiter.remove();
                        suiter.remove();
                        break;
                     }
                  }
               }
               if (!dmatch) {
   
                  Allusers newuser=new Allusers();   
                  newuser.setUserdesc(user.getUserdesc().substring(0,user.getUserdesc().indexOf("[")) +
                       "[" + linstacr + "]");
                  newuser.setIsdeleted(false);
                  newuser.setAllinst(ainst);
                  newuser.setLuserid(user.getLuserid());
      
                  getMainDAO().saveOrUpdate(newuser);

               }
            }
            // Set isdeleted for records no longer in auserlist
            Iterator suiter=sulist.iterator();
            while (suiter.hasNext()) {
               Allusers suser=(Allusers)suiter.next();
               auiter=auserlist.iterator();
               boolean hasmatch=false;
               while (auiter.hasNext()) {
                  HashMap umap=(HashMap)auiter.next();
                  Allusers user=(Allusers)umap.get("user");
                  String linstid=(String)umap.get("linstid");
                  if (new Long(linstid).longValue()==suser.getAllinst().getLinkedinst().getLinstid()) {
                     hasmatch=true; 
                  }
               }
               if (!hasmatch) {
                  suser.setIsdeleted(true);
                  getMainDAO().saveOrUpdate(suser);
               }
            }

            // Update local modtime upon successful completion
            List lilist=getMainDAO().execQuery("select l from Linkedinst l where (not (l.isdisabled=true))");
            Iterator i=lilist.iterator();
            while (i.hasNext()) {
               Linkedinst l=(Linkedinst)i.next();
               // get current remote system time
               Timestamp rtime=getRemoteSystemTime(l);
               Userinfomodtime u=(Userinfomodtime)getMainDAO().execUniqueQuery(
                     "select a from Userinfomodtime a " +
                        "where a.linkedinst.linstid=" + l.getLinstid()
                  );
               if (u==null) {
                  u=new Userinfomodtime();
                  u.setLinkedinst(l);
               } 
               u.setRemotemod(rtime);
               getMainDAO().saveOrUpdate(u);

            }

      } catch (Exception e) { }

   }

   //////
   //////
   //////

   private void updateRemoteForms() throws FORMSException,FORMSKeyException,FORMSSecurityException {

      try {

            List lilist=getMainDAO().execQuery("select l from Linkedinst l where (not (l.isdisabled=true))");
            Iterator liiter=lilist.iterator();
            
            // update forms for all installations/studies
            while (liiter.hasNext()) {

               StringBuilder sb=new StringBuilder();
               Iterator i;

               Linkedinst linst=(Linkedinst)liiter.next();

               List lslist=getMainDAO().execQuery(
                  "select l from Linkedstudies l join fetch l.linkedinst join fetch l.studies where l.linkedinst.linstid=" + linst.getLinstid()
                  );

               // create stringbuffer containing studiyid's for next pull   
               i=lslist.iterator();
               while (i.hasNext()) {
                  Linkedstudies ls=(Linkedstudies)i.next();
                  sb.append(new Long(ls.getStudies().getStudyid()).toString());
                  if (i.hasNext()) sb.append(",");
               }

               List stdlist=getMainDAO().execQuery(
                  "select s from Studies s where studyid in (" + sb + ")"
                  );

               // pull root formha list for studies above for later use
               List halist=getMainDAO().execQuery(
                  "select f from Formha f join fetch f.studies " +
                     "where f.visibility=" + VisibilityConstants.STUDY + " and " +
                        "pformhaid is null and " +
                        "f.studies.studyid in (" + sb + ")"
                     );

               // pull root datatablehaha list for studies above for later use
               List dthalist=getMainDAO().execQuery(
                  "select f from Datatableha f join fetch f.studies " +
                     "where f.visibility=" + VisibilityConstants.STUDY + " and " +
                        "pdthaid is null and " +
                        "f.studies.studyid in (" + sb + ")"
                     );

               ///////////////////////////////////////////////
               // SUBMIT FORM DATA REQUEST TO REMOTE SERVER //
               ///////////////////////////////////////////////

               FormMetadataServiceParms parms=new FormMetadataServiceParms("formMetadataServiceTarget","getFormMetadata");   
               parms.setList(lslist);

               // retrieve and set last update time
               java.util.Date lastupdate=FormMetaUtil.getLastupdateremote(getMainDAO());
               parms.setLastupdate(lastupdate);
               // ainstid for linkedinst needed later
               Allinst ainst=(Allinst)getMainDAO().execUniqueQuery(
                  "select a from Allinst a where a.linkedinst.linstid=" + linst.getLinstid()
                  );
               long ainstid=ainst.getAinstid();

               FormMetadataServiceResult result=(FormMetadataServiceResult)submitServiceRequest(linst,parms);

               ///////////////////////////////////////////////////////
               // WORK DATA FROM SERVICE RESULT INTO LOCAL DATABASE //
               ///////////////////////////////////////////////////////

               // add all tba object to append list, then cycle through appendlist to update datasets
               ArrayList appendlist=new ArrayList();

               /////////////
               // DATATABLEDEF //
               /////////////

               // first, remove datatabledef records that no longer exist in remote data
               List dtdefidlist=result.getDtdefidlist();
               i=dtdefidlist.iterator();
               sb=new StringBuilder();
               while (i.hasNext()) {
                  HashMap map=(HashMap)i.next();
                  String appstr=(String)map.get("dtdefid");
                  if (appstr!=null) {
                     sb.append(map.get("dtdefid"));
                     if (i.hasNext()) sb.append(",");
                  }
               }
               if (sb.length()>0) {
                  List l=getMainDAO().execQuery(
                     "select f from Datatabledef f join fetch f.formtypes " +
                        "where f.allinst.ainstid=" + ainstid + " and " +
                           "f.remotedtdefid not in (" + sb + ")"
                           );
                  Iterator i2=l.iterator();
                  while (i2.hasNext()) {
                     Datatabledef fd=(Datatabledef)i2.next();
                     getMainDAO().delete(fd);
                  }
               }
               // next, add new/updated records
               List datatabledeflist=result.getDatatabledeflist();
               i=datatabledeflist.iterator();
               while (i.hasNext()) {

                  HashMap map=(HashMap)i.next();
                  Datatabledef_transfer rfd=(Datatabledef_transfer)map.get("datatabledef");
                  Long rstudyid=(Long)map.get("studyid");

                  // get root heirarchy record for datatabledef study
                  Iterator lsiter=lslist.iterator();
                  while (lsiter.hasNext()) {
                     Linkedstudies ls=(Linkedstudies)lsiter.next();

                     // rstudyid actually holds local (this installation) value
                     if (ls.getStudies().getStudyid()==rstudyid.longValue()) {
                        Iterator haiter=halist.iterator();
                        boolean foundha=false;
                        while (haiter.hasNext()) {
                           Formha ha=(Formha)haiter.next();
                           if (ls.getStudies().getStudyid()==ha.getStudies().getStudyid()) {
                              foundha=true;
                              rfd.setPformhaid(ha.getFormhaid());
                           }
                        }
                        if (!foundha) {
                           try {
                              rfd.setPformhaid(createHaRecord(ls.getStudies().getStudyid()).getFormhaid());
                           } catch (Exception haex) { }   
                        }   
                        Iterator dthaiter=dthalist.iterator();
                        boolean founddtha=false;
                        while (dthaiter.hasNext()) {
                           Datatableha dtha=(Datatableha)dthaiter.next();
                           if (ls.getStudies().getStudyid()==dtha.getStudies().getStudyid()) {
                              founddtha=true;
                              rfd.setPdthaid(dtha.getDthaid());
                           }
                        }
                        if (!founddtha) {
                           try {
                              rfd.setPdthaid(createDtHaRecord(ls.getStudies().getStudyid()).getDthaid());
                           } catch (Exception haex) { }   
                        }   
                     }
                  }

                  // see if current local record exists for record
                  Datatabledef_transfer lfd=(Datatabledef_transfer)getMainDAO().execUniqueQuery(
                     "select f from Datatabledef_transfer f where f.ainstid=" + ainstid + " and " +
                        "f.remotedtdefid=" + rfd.getDtdefid()
                     );
                  if (lfd==null) {
                     // if no pre-existing record, create new local record
                     Datatabledef sfd=new Datatabledef();
                     sfd.setAllinst(ainst);
                     sfd.setRemotedtdefid(rfd.getDtdefid());
                     sfd.setFormno(rfd.getFormno());
                     sfd.setFormver(rfd.getFormver());
                     sfd.setFormacr(rfd.getFormacr());
                     sfd.setFormdesc(rfd.getFormdesc());
                     sfd.setPformhaid(rfd.getPformhaid());
                     sfd.setPdthaid(rfd.getPdthaid());
                     sfd.setIsarchived(rfd.getIsarchived());
                     sfd.setIsenrollform(rfd.getIsenrollform());
                     sfd.setSsaonly(rfd.getSsaonly());
                     sfd.setDtacr(rfd.getDtacr());
                     sfd.setDtdesc(rfd.getDtdesc());
                     sfd.setDisplayas(rfd.getDisplayas());
                     getMainDAO().save(sfd);
                  } else {
                     // otherwise replace current local record with info from remote record
                     lfd.setFormno(rfd.getFormno());
                     lfd.setFormver(rfd.getFormver());
                     lfd.setFormacr(rfd.getFormacr());
                     lfd.setFormdesc(rfd.getFormdesc());
                     lfd.setIsarchived(rfd.getIsarchived());
                     lfd.setIsenrollform(rfd.getIsenrollform());
                     lfd.setSsaonly(rfd.getSsaonly());
                     lfd.setDtacr(rfd.getDtacr());
                     lfd.setDtdesc(rfd.getDtdesc());
                     lfd.setDisplayas(rfd.getDisplayas());
                     // use current pformhaid for pre-existing records
                     lfd.setPformhaid(lfd.getPformhaid());
                     lfd.setPdthaid(lfd.getPdthaid());
                     lfd.setIsarchived(rfd.getIsarchived());
                     getMainDAO().saveOrUpdate(lfd);
                  }

               }

               //////////////////
               // FORMTYPELIST //
               //////////////////

               // first, remove formtypelist records that no longer exist in remote data
               List formtypelistidlist=result.getFormtypelistidlist();
               i=formtypelistidlist.iterator();
               sb=new StringBuilder();
               while (i.hasNext()) {
                  sb.append(((Long)i.next()).toString());
                  if (i.hasNext()) sb.append(",");
               }
               if (sb.length()>0) {
                  List l=getMainDAO().execQuery(
                     "select f from Formtypelist f join fetch f.formtypes " +
                        "where f.allinst.ainstid=" + ainstid + " and " +
                           "f.remoteformtypelistid not in (" + sb + ")"
                           );
                  Iterator i2=l.iterator();
                  while (i2.hasNext()) {
                     Formtypelist fd=(Formtypelist)i2.next();
                     getMainDAO().delete(fd);
                  }
               }
               // next, add new/updated records
               List formtypelistlist=result.getFormtypelistlist();
               i=formtypelistlist.iterator();
               while (i.hasNext()) {
                  Formtypelist_transfer rfd=(Formtypelist_transfer)i.next();
                  long rstudyid=rfd.getStudyid();
                  Long lstudyid=null;
                  Studies lstudy=null;
                  // get local studyid for remote study
                  Iterator lsiter=lslist.iterator();
                  while (lsiter.hasNext()) {
                     Linkedstudies ls=(Linkedstudies)lsiter.next();
                     if (ls.getRemotestudyid()==rstudyid) {
                        lstudyid=new Long(ls.getStudies().getStudyid());
                        Iterator stditer=stdlist.iterator();
                        while (stditer.hasNext()) {
                           Studies cstudy=(Studies)stditer.next();
                           if (cstudy.getStudyid()==lstudyid.longValue()) {
                              
                              lstudy=cstudy;
                              break;
                           }
                        }
                        break;
                     }
                  }
                  // see if current local record exists for record
                  Formtypelist_transfer lfd=(Formtypelist_transfer)getMainDAO().execUniqueQuery(
                     "select f from Formtypelist_transfer f where f.ainstid=" + ainstid + " and " +
                        "f.remoteformtypelistid=" + rfd.getFormtypelistid()
                     );
                  if (lfd==null) {
                     // if no pre-existing record, create new local record
                     Formtypelist sfd=new Formtypelist();
                     sfd.setAllinst(ainst);
                     sfd.setRemoteformtypelistid(rfd.getFormtypelistid());
                     sfd.setStudies(lstudy);
                     sfd.setVisibility(rfd.getVisibility());
                     sfd.setFormtype(rfd.getFormtype());
                     getMainDAO().save(sfd);
                  } else {
                     // otherwise replace current local record with info from remote record
                     lfd.setStudyid(lstudyid.longValue());
                     lfd.setRemoteformtypelistid(rfd.getFormtypelistid());
                     lfd.setVisibility(rfd.getVisibility());
                     lfd.setFormtype(rfd.getFormtype());
                     getMainDAO().saveOrUpdate(lfd);
                  }

               }

               ////////////////
               // DATATABLECOLDEF //
               ////////////////

               // first pull list of dtdefids,remotedtdefids form installation
               List fdl=getMainDAO().execQuery(
                  "select f.dtdefid,f.remotedtdefid from Datatabledef_transfer f " +
                     "where f.ainstid=" + ainstid
                     );
               List datatablecoldeflist=result.getDatatablecoldeflist();
               i=datatablecoldeflist.iterator();
               // arraylist to hold list of dtdefids where datatablecoldef records already deleted
               ArrayList alreadydel=new ArrayList();
               Long o_dtdefid=null;
               Long n_dtdefid=null;
               while (i.hasNext()) {

                  Datatablecoldef_transfer rfs=(Datatablecoldef_transfer)i.next();

                  // delete current records if not already deleted
                  o_dtdefid=new Long(rfs.getDtdefid());
                  if (!alreadydel.contains(o_dtdefid)) {
                     alreadydel.add(o_dtdefid);
                     Iterator i2=fdl.iterator();
                     while (i2.hasNext()) {
                        Object[] oarray=(Object[])i2.next();
                        if (((Long)oarray[1]).longValue()==o_dtdefid.longValue()) {
                           n_dtdefid=(Long)oarray[0];
                           break;
                        }
                     }
                     // delete current records
                     if (n_dtdefid!=null) {
                        List dlist=getMainDAO().execQuery(
                           "select f from Datatablecoldef_transfer f where f.dtdefid=" + n_dtdefid
                           );
                        Iterator diter=dlist.iterator();
                        while (diter.hasNext()) {
                           Datatablecoldef_transfer fcd=(Datatablecoldef_transfer)diter.next();
                           getMainDAO().delete(fcd);
                        }
                     }
                  }
                  // add new/replacement record
                  if (n_dtdefid!=null) {
                     rfs.setDtdefid(n_dtdefid.longValue());
                     getMainDAO().save(rfs);
                  } 
               }

               ///////////////
               // FORMTYPES //
               ///////////////

               // First, repull datatabledef, formtypelist & pull formformats

               List formformatlist;

               datatabledeflist=getMainDAO().execQuery(
                  "select f from Datatabledef f " +
                     "where f.allinst.ainstid=" + ainstid 
                        );

               formtypelistlist=getMainDAO().execQuery(
                  "select f from Formtypelist f " +
                     "where f.allinst.ainstid=" + ainstid 
                        );

               formformatlist=getMainDAO().execQuery(
                  "select f from Formformats f join fetch f.supportedfileformats"
                        );

               // Remove formtypes that no longer exist in the data         

               List formtypesidlist=result.getFormtypesidlist();
               i=formtypesidlist.iterator();
               sb=new StringBuilder();
               while (i.hasNext()) {
                  sb.append(((Long)i.next()).toString());
                  if (i.hasNext()) sb.append(",");
               }
               if (sb.length()>0) {
                  List l=getMainDAO().execQuery(
                     "select f from Formtypes f " +
                        "where f.ainstid=" + ainstid + " and " +
                           "f.remoteformtypeid not in (" + sb + ")"
                           );
                  Iterator i2=l.iterator();
                  while (i2.hasNext()) {
                     Formtypes fd=(Formtypes)i2.next();
                     getMainDAO().delete(fd);
                  }
               }

               // next, add new/updated records
               List formtypeslist=result.getFormtypeslist();
               i=formtypeslist.iterator();
               while (i.hasNext()) {

                  HashMap map=(HashMap)i.next();
                  Formtypes_transfer rft=(Formtypes_transfer)map.get("formtypes");
                  String contenttype =(String)map.get("contenttype");
                  long rdtdefid=rft.getDtdefid(); 
                  long rformtypelistid=rft.getFormtypelistid(); 
                  Long ldtdefid=null;
                  Long lformtypelistid=null;
                  Long lformformatid=null;
                  Datatabledef ldatatabledef=null;
                  Formtypelist lformtypelist=null;
                  Formformats lformformats=null;

                  Iterator i2;
                  // get matching datatabledef identifier
                  i2=datatabledeflist.iterator();
                  while (i2.hasNext()) {
                     Datatabledef lfd=(Datatabledef)i2.next();
                     if (lfd.getRemotedtdefid()==rdtdefid) {
                        ldtdefid=new Long(lfd.getDtdefid());
                        ldatatabledef=lfd;
                        break;
                     }
                  }
                  // get matching formtypelist identifier
                  i2=formtypelistlist.iterator();
                  while (i2.hasNext()) {
                     Formtypelist lfd=(Formtypelist)i2.next();
                     if (lfd.getRemoteformtypelistid()==rformtypelistid) {
                        lformtypelistid=new Long(lfd.getFormtypelistid());
                        lformtypelist=lfd;
                        break;
                     }
                  }
                  // get matching formformats identifier
                  i2=formformatlist.iterator();
                  while (i2.hasNext()) {
                     Formformats lfd=(Formformats)i2.next();
                     if (lfd.getSupportedfileformats().getContenttype().equalsIgnoreCase(contenttype)) {
                        lformformatid=new Long(lfd.getFormformatid());
                        lformformats=lfd;
                        break;
                     }
                  }

                  // see if current local record exists for record
                  Formtypes_transfer lft=(Formtypes_transfer)getMainDAO().execUniqueQuery(
                     "select f from Formtypes_transfer f where f.ainstid=" + ainstid + " and " +
                        "f.remoteformtypeid=" + rft.getFormtypeid()
                     );
                  if (lft==null) {
                     // if no pre-existing record, create new local record
                     Formtypes sft=new Formtypes();
                     sft.setAinstid(ainstid);
                     sft.setRemoteformtypeid(rft.getFormtypeid());
                     sft.setDatatabledef(ldatatabledef);
                     sft.setFormtypelist(lformtypelist);
                     sft.setFormformats(lformformats);
                     getMainDAO().save(sft);
                  } else {
                     // otherwise replace current local record with info from remote record
                     lft.setDtdefid(ldtdefid);
                     lft.setFormtypelistid(lformtypelistid);
                     lft.setFormformatid(lformformatid);
                     getMainDAO().saveOrUpdate(lft);
                  }

               }

               ///////////////
               // FORMSTORE //
               ///////////////

               // Removal need not be done here, done by Formtype cascade
               // add new/updated records
               List formstorelist=result.getFormstorelist();
               i=formstorelist.iterator();
               while (i.hasNext()) {

                  Formstore_transfer rfs=(Formstore_transfer)i.next();
                  // see if current local record exists for record
                  Formtypes lft=(Formtypes)getMainDAO().execUniqueQuery(
                     "select f from Formtypes f where f.ainstid=" + ainstid + " and " +
                        "f.remoteformtypeid=" + rfs.getFormtypeid()
                        );
                  long lformtypeid=lft.getFormtypeid();      
                  Formstore_transfer lfs=(Formstore_transfer)getMainDAO().execUniqueQuery(
                     "select f from Formstore_transfer f where f.formtypeid=" + lformtypeid
                     );
                  if (lfs==null) {
                     // if no pre-existing record, create new local record
                     Formstore sfs=new Formstore();
                     sfs.setFormtypes(lft);
                     sfs.setFormcontent(rfs.getFormcontent());
                     getMainDAO().save(sfs);
                  } else {
                     // otherwise replace current local record with info from remote record
                     lfs.setFormcontent(rfs.getFormcontent());
                     getMainDAO().saveOrUpdate(lfs);
                  }

               }

               ////////////////////////
               // DTUSERPERMASSIGN //
               ////////////////////////
   
               // pull current form permissions for installation

               // construct stringbuffer of dtdefids
               i=datatabledeflist.iterator();
               sb=new StringBuilder();
               while (i.hasNext()) {
                  Datatabledef fd=(Datatabledef)i.next();
                  sb.append(fd.getDtdefid());
                  if (i.hasNext()) sb.append(",");
               }

               // get current Allusers information as well as userinfolist from result
               List luserlist=getMainDAO().execQuery(
                  "select a from Allusers a join fetch a.allinst where a.allinst.ainstid=" + ainstid + " or a.allinst.islocal=true"
                  );
               List userinfolist=result.getUserinfolist();   

               // retrieve updated list   
               List rpermlist=result.getFormuserpermlist();

               // pull current local dtuserpermassign records for remote forms 
               List cpermlist=getMainDAO().execQuery(
                  "select p from Dtuserpermassign_transfer p where p.dtdefid in (" + sb + ")"
                  );

               // remove current permissions (to be replaced) 
               Iterator cpermiter=cpermlist.iterator();
               while (cpermiter.hasNext()) {
                  Dtuserpermassign_transfer lperm=(Dtuserpermassign_transfer)cpermiter.next();
                     getMainDAO().delete(lperm);
               }

               // cycle through remote list, finding local auserid and then create new local record
               Iterator riter=rpermlist.iterator();
               while (riter.hasNext()) {
                  HashMap pmap=(HashMap)riter.next();
                  Long rauserid=(Long)pmap.get("auserid");
                  Long rdtdefid=(Long)pmap.get("dtdefid");
                  BitSet rpermlevel=(BitSet)pmap.get("permlevel");
                  Iterator ruiter=userinfolist.iterator();
                  while (ruiter.hasNext()) {
                     HashMap ruimap=(HashMap)ruiter.next();
                     Long ruiauserid=(Long)ruimap.get("auserid");
                     if (rauserid.equals(ruiauserid)) {
                        Long ruiluserid=(Long)ruimap.get("luserid");
                        Boolean ruiislocal=(Boolean)ruimap.get("islocal");
                        // find local auserid
                        Iterator luiter=luserlist.iterator();
                        while (luiter.hasNext()) {
                           Allusers luser=(Allusers)luiter.next();
                           long lauserid=luser.getAuserid();
                           long lluserid=luser.getLuserid();
                           boolean lislocal=luser.getAllinst().getIslocal();

                           if (
                                 // local to THIS installation
                                 (ruiislocal.booleanValue()==false && lislocal==true && lluserid==ruiluserid.longValue())
                              ) {

                              // save new permissions record
                              Dtuserpermassign newperm=new Dtuserpermassign();
                              newperm.setAllusers(luser);
                              newperm.setPermlevel(rpermlevel);
                              // find and set dtdefid
                              Iterator lfditer=datatabledeflist.iterator();
                              while (lfditer.hasNext()) {
                                 Datatabledef ldatatabledef=(Datatabledef)lfditer.next();
                                 if (rdtdefid.longValue()==ldatatabledef.getRemotedtdefid()) {
                                    newperm.setDtdefid(ldatatabledef.getDtdefid());
                                    break;
                                 }
                              }
                              try {
                                 getMainDAO().save(newperm);
                              } catch (Exception alre) {
                                 // already added
                              }
                              break;
                           }
                        }
                        break;
                     }
                  }
               }

               ////////////////////////////
               // FORMTYPEUSERPERMASSIGN //
               ////////////////////////////

               // construct stringbuffer of formtypelistids
               i=formtypelistlist.iterator();
               sb=new StringBuilder();
               while (i.hasNext()) {
                  Formtypelist fd=(Formtypelist)i.next();
                  sb.append(fd.getFormtypelistid());
                  if (i.hasNext()) sb.append(",");
               }

               // pull current local formtypeuserpermassign records for remote forms 
               // Using transfer class to avoid session problems
               cpermlist=getMainDAO().execQuery(
                  "select p from Formtypeuserpermassign_transfer p where p.formtypelistid in (" + sb + ")"
                  );

               // remove current permissions (to be replaced) 
               cpermiter=cpermlist.iterator();
               while (cpermiter.hasNext()) {
                  Formtypeuserpermassign_transfer lperm=(Formtypeuserpermassign_transfer)cpermiter.next();
                     getMainDAO().delete(lperm);
               }

               // retrieve updated list   
               rpermlist=result.getFormtypeuserpermlist();

               // cycle through remote list, finding local auserid and then update/create new local record
               riter=rpermlist.iterator();
               while (riter.hasNext()) {
                  HashMap pmap=(HashMap)riter.next();
                  Long rauserid=(Long)pmap.get("auserid");
                  Long rformtypelistid=(Long)pmap.get("formtypelistid");
                  BitSet rpermlevel=(BitSet)pmap.get("permlevel");

                  Iterator ruiter=userinfolist.iterator();
                  while (ruiter.hasNext()) {
                     HashMap ruimap=(HashMap)ruiter.next();
                     Long ruiauserid=(Long)ruimap.get("auserid");
                     if (rauserid.equals(ruiauserid)) {
                        Long ruiluserid=(Long)ruimap.get("luserid");
                        Boolean ruiislocal=(Boolean)ruimap.get("islocal");
                        // find local auserid
                        Iterator luiter=luserlist.iterator();
                        while (luiter.hasNext()) {
                           Allusers luser=(Allusers)luiter.next();
                           long lauserid=luser.getAuserid();
                           long lluserid=luser.getLuserid();
                           boolean lislocal=luser.getAllinst().getIslocal();
                           if (
                                 // local to THIS installation
                                 (ruiislocal.booleanValue()==false && lislocal==true && lluserid==ruiluserid.longValue())
                              ) {
                              // save new permissions record
                              Formtypeuserpermassign newperm=new Formtypeuserpermassign();
                              newperm.setAllusers(luser);
                              newperm.setPermlevel(rpermlevel);
                              // find and set formtypelistid
                              Iterator lfditer=formtypelistlist.iterator();
                              while (lfditer.hasNext()) {
                                 Formtypelist lformtypelist=(Formtypelist)lfditer.next();
                                 if (rformtypelistid.longValue()==lformtypelist.getRemoteformtypelistid()) {
                                    newperm.setFormtypelistid(lformtypelist.getFormtypelistid());
                                    break;
                                 }
                              }
                              try {
                                 getMainDAO().save(newperm);
                              } catch (Exception alre) {
                                 // already added
                              }
                              break;

                           }
                        }
                        break;
                     }
                  }
               }

               ////
               ////
               ////

            }

            // Update local modtime upon successful completion
            List lslist=getMainDAO().execQuery(
               "select l from Linkedstudies l join fetch l.linkedinst where (not (l.linkedinst.isdisabled=true))"
               );
            Iterator i=lslist.iterator();
            while (i.hasNext()) {
               Linkedstudies ls=(Linkedstudies)i.next();
               // get current remote system time
               Timestamp rtime=getRemoteSystemTime(ls.getLinkedinst());
               Dtmodtime fmt=(Dtmodtime)getMainDAO().execUniqueQuery(
                     "select a from Dtmodtime a " +
                        "where a.linkedstudies.lstdid=" + ls.getLstdid()
                  );
               if (fmt==null) {
                  fmt=new Dtmodtime();
                  fmt.setLinkedstudies(ls);
               } 
               fmt.setRemotemod(rtime);
               getMainDAO().saveOrUpdate(fmt);
            }

      } catch (Exception e) { }
   }

   //////
   //////
   //////

   private Formha createHaRecord(long studyid) throws FORMSException, FORMSSecurityException, FORMSKeyException {
      Studies s=(Studies)getMainDAO().execUniqueQuery(
         "select s from Studies s where studyid=" + studyid
         );
      if (s==null) return null;   
      Formha newha=new Formha();
      newha.setVisibility(VisibilityConstants.STUDY);
      newha.setStudies(s);
      newha.setHaordr(1);
      newha.setHadesc("Study-Level Forms");
      getMainDAO().saveOrUpdate(newha);
      return newha;
   }

   private Datatableha createDtHaRecord(long studyid) throws FORMSException, FORMSSecurityException, FORMSKeyException {
      Studies s=(Studies)getMainDAO().execUniqueQuery(
         "select s from Studies s where studyid=" + studyid
         );
      if (s==null) return null;   
      Datatableha newha=new Datatableha();
      newha.setVisibility(VisibilityConstants.STUDY);
      newha.setStudies(s);
      newha.setHaordr(1);
      newha.setHadesc("Study-Level Tables");
      getMainDAO().saveOrUpdate(newha);
      return newha;
   }


   private boolean isInList(List auserlist,Allusers user) {
      Iterator i=auserlist.iterator();
      boolean rval=false;
      while (i.hasNext()) {
         HashMap map=(HashMap)i.next();
         Allusers luser=(Allusers)map.get("user");
         if (luser.getAuserid()==user.getAuserid() &&
             luser.getAllinst().getAinstid()==user.getAllinst().getAinstid() &&
             luser.getUserdesc().equals(user.getUserdesc())) {
             rval=true;
             break;
         }
      }
      return rval;
   }

   private Timestamp getRemoteSystemTime(Linkedinst inst) throws FORMSException,FORMSKeyException,FORMSSecurityException {
      SsaServiceSystemParms parms=new SsaServiceSystemParms("systemTimeServiceTarget","getSystemTime");
      SsaServiceObjectResult result=(SsaServiceObjectResult)this.submitServiceRequest(inst,parms);
      return new Timestamp(((Date)result.getObject()).getTime());
   }

   //////
   //////
   //////


}
 


