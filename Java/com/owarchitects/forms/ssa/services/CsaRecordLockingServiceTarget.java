 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  Program:  CsaRecordLockingServiceTarget.java - 
 *                                                          MRH 03/2008
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;

public class CsaRecordLockingServiceTarget extends FORMSSsaServiceClientServiceTarget {

   // Lock data record
   public CsaServiceStatusResult lockRecord(CsaServicePassObjectParms parms) {
      CsaServiceStatusResult result=new CsaServiceStatusResult();
      try {
         // retrieve HashMap object
         HashMap map=(HashMap)parms.getObject();
         Datatablerecords rec=(Datatablerecords)map.get("record");
         Linkedinst forminst=(Linkedinst)map.get("forminst");
         int formloc=((Integer)map.get("formloc")).intValue();
         long auserid=new Long((String)map.get("auserid")).longValue();
         if (formloc==LockInfo.LOCALFORM) {
            // LOCAL FORM 
            // pull user information
            String userdesc=(String)getMainDAO().execUniqueQuery(
               "select a.userdesc from Allusers a where a.auserid=" + auserid
               );
            // save locking information
            rec.setIslocked(true);
            rec.setLocktime(new Date(System.currentTimeMillis()));
            rec.setLockuser(auserid);
            rec.setLockinfo(userdesc);
            getMainDAO().saveOrUpdate(rec);
            result.setStatus(FORMSServiceConstants.OK);
         } else {   
            // REMOTE FORM 
            SsaServiceStatusResult remoteresult=(SsaServiceStatusResult)submitServiceRequest(forminst,"recordLockingServiceTarget","lockRecord",rec);
            result.setStatus(remoteresult.getStatus());
         }   
      } catch (Exception e) {
         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
      }
      return result;
   }

   // Unlock data record
   public CsaServiceStatusResult unlockRecord(CsaServicePassObjectParms parms) {
      CsaServiceStatusResult result=new CsaServiceStatusResult();
      try {
         // retrieve HashMap object
         HashMap map=(HashMap)parms.getObject();
         Datatablerecords rec=(Datatablerecords)map.get("record");
         Linkedinst forminst=(Linkedinst)map.get("forminst");
         int formloc=((Integer)map.get("formloc")).intValue();
         long auserid=new Long((String)map.get("auserid")).longValue();
         if (formloc==LockInfo.LOCALFORM) {
            // LOCAL FORM 
            // pull user information
            String userdesc=(String)getMainDAO().execUniqueQuery(
               "select a.userdesc from Allusers a where a.auserid=" + auserid
               );
            // save locking information
            rec.setIslocked(false);
            rec.setLocktime(null);
            rec.setLockuser(null);
            rec.setLockinfo(null);
            getMainDAO().saveOrUpdate(rec);
            result.setStatus(FORMSServiceConstants.OK);
         } else {   
            // REMOTE FORM 
            SsaServiceStatusResult remoteresult=(SsaServiceStatusResult)submitServiceRequest(forminst,"recordLockingServiceTarget","unlockRecord",rec);
            result.setStatus(remoteresult.getStatus());
         }   
      } catch (Exception e) {
         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
      }
      return result;
   }

}

