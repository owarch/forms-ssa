 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  FORMSServiceInterceptorController.java - Server controller for FORMS service requests.
 *     Intercepts service request, passes input parameter to service target
 *     class, and returns result parameter class to calling controller.   MRH 09/2008
 */
package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.db.*;
import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import java.util.*;
import java.io.*;
import java.beans.*;
import java.lang.reflect.*;
import java.security.*;
import javax.crypto.SecretKey;
import org.springframework.web.servlet.*;
import org.springframework.web.context.support.*;
import org.springframework.context.support.*;
import org.apache.axis2.context.*;
import org.apache.axis2.client.*;
import org.apache.axis2.transport.http.HTTPConstants;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.time.DateUtils;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.lang.*;
import org.apache.commons.io.FileUtils;
import org.springframework.orm.hibernate3.HibernateJdbcException;


public class FORMSServiceInterceptorController extends FORMSController {

   // Override superclass handleRequestInternal method
   protected ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) throws FORMSException {

        try {

           // PrintWriter for diagnostic output
           out=response.getWriter();        

           this.request=request;
           this.response=response;
           this.syswork=null;
           this.sysconn=null;

           // Pull syswork, sysconn from session attribute preferably, alternately from cookie.
           try {
              this.syswork=request.getSession().getAttribute("syswork").toString();
           } catch (Exception swe) {}
           if (syswork==null || syswork.length()<1) {
              this.syswork=Base64.decode(this.getCookieValue(Base64.encode("workdir")).replaceAll("=",""));
           }   

           response.addCookie(new Cookie(Base64.encode("workdir").replaceAll("=",""),
                                         Base64.encode(syswork).replaceAll("=","")));
           try {
              this.sysconn=request.getSession().getAttribute("sysconn").toString();
           } catch (Exception swe) {}
           if (sysconn==null || sysconn.length()<1) {
              this.sysconn=Base64.decode(this.getCookieValue(Base64.encode("sysconn")).replaceAll("=",""));
           }   

           // formstime used for sysconn and auth
           String formstime=new Long(System.currentTimeMillis()).toString();

           // Get context
           session=request.getSession();
   
           // Immediately expire headers
           response.setHeader("Cache-Control","no-cache"); //HTTP 1.1
           response.setHeader("Pragma","no-cache"); //HTTP 1.0
           response.setDateHeader ("Expires", -1); //prevents caching at the proxy server

           try {

              // Initialize Main Data Source
              initDataSource();

              // Create mav used by override method
              this.mav=new ModelAndView();
              mav=submitRequest();

              return mav;

           } catch (Exception ke) {
              throw new FORMSException("FORMSController exception (submitRequest exception) - <BR>",ke);
           }
          
        } catch (Exception e) {

           throw new FORMSException("FORMSController exception - <BR>",e);

        }

   }     



   public ModelAndView submitRequest() throws FORMSException {

      try {

         // Retrieve object from encoded string
         Object parms=ObjectUtil.objectFromEncodedString(getRequest().getParameter("parm"));

         // Retrieve system information from passed object
         Class ic=parms.getClass();
         Method im;
         im=ic.getMethod("getBeanname");
         String beanname=(String)im.invoke(parms,new Object[] { });
         im=ic.getMethod("getMethodname");
         String methodname=(String)im.invoke(parms,new Object[]{ });

         try {
   
            ////////////////////////////
            // Remote SSA connections //
            ////////////////////////////

            im=ic.getMethod("getCodeword");
            String codeword=null;
            try {
               codeword=(String)im.invoke(parms,new Object[]{ });
            } catch (Exception cwe) {
               // Do Nothing (Local connection)
            }   
            im=ic.getMethod("getLuserid");
            String luserid=(String)im.invoke(parms,new Object[]{ });
            im=ic.getMethod("getConnectstring");
            String connectstring=(String)im.invoke(parms,new Object[]{ });
            im=ic.getMethod("getCheckstring");
            String checkstring=(String)im.invoke(parms,new Object[]{ });

            Linkedinst li=null;

            // remote server service calls
            if (codeword!=null) {

               // Establish connection to FORMSMainDB
               EmbeddedinstDAO embeddedinstDAO=(EmbeddedinstDAO)this.getContext().getBean("embeddedinstDAO");
               Embeddedinst ei=embeddedinstDAO.getRecordByCodeword(codeword);
           
               // get local installations public key for this site
               PrivateKey pk=EncryptUtil.getPrivateKey(session,"inst_loc_" + ei.getAcronym());
         
               connectstring=connectstring.replaceAll("\"","");
               connectstring=EncryptUtil.decryptLongString(connectstring,pk);
     
               Connectstring connstr=(Connectstring)ObjectXmlUtil.objectFromXmlString(connectstring);

               // refresh application context, obtaining connection to CSAMainDB
               SessionObjectUtil.readMainDBIntoContext(getSession(),connstr.getDbpassword());

               // get FORMSKey and make accessible to session
               SecretKey sk=EncryptUtil.getEncryptionKey(getSession(),connstr.getFkpassword());
               SessionObjectUtil.setEncryptionKeyObj(getSession(),sk);

               // Pull linkedinst record from FORMSMainDB
               LinkedinstDAO linkedinstDAO=(LinkedinstDAO)this.getContext().getBean("linkedinstDAO");
               li=linkedinstDAO.getRecord(ei.getAcronym());
   
               // Make linkedinst record (information) accessible to session
               session.setAttribute("linkedinst",li);
               // Make luserid information accessible to session
               session.setAttribute("luserid",luserid);

            // local server service calls
            } else {

               Sysconn sysconn=(Sysconn)ObjectXmlUtil.objectFromXmlString(
                  EncryptUtil.decryptString(connectstring,SessionObjectUtil.getInternalKeyObj(session))
                  );
   
               SessionObjectUtil.readMainDBIntoContext(getSession(),sysconn.getUsername(),sysconn.getPassword());

               // make sure auth object is initialized
               SessionObjectUtil.getAuthObj(request,response,syswork,connectstring);


            } 
   
            // retrieve object stored as bean
            Object thisobj=this.getContext().getBean(beanname);
            // return object class
            Class c=thisobj.getClass();

            Method cm;
            try {
               // Call method with passed parameters
               cm=c.getMethod(methodname,new Class[] { ic });
            } catch (NoSuchMethodException nsme1) {
               try {
                  // Call method with no parameters
                  cm=c.getMethod(methodname,new Class[] { });
               } catch (NoSuchMethodException nsme1b) {
                  // Call method passing Object
                  cm=c.getMethod(methodname,new Class[] { Object.class });
               }
            }
            Class rc=cm.getReturnType();
            Object o=null;

            try {
      
               // Retrieve installation information record return if no matching installation
               if (li==null && codeword!=null) {
                  o=rc.newInstance();
                  Method rm=o.getClass().getMethod("setStatus",new Class[] { Integer.class });
                  // Invoke method
                  Object oj=rm.invoke(o,new Object[] { new Integer(FORMSServiceConstants.NO_MATCHING_INSTALLATION) } );
                  out.println(ObjectUtil.objectToEncodedString(oj));
                  return null;
               } 
       
               // Check security checkstring return if nonmatch
               if (o==null && !EncryptUtil.serviceCheckDecrypt(session,li,checkstring)) {
                  o=rc.newInstance();
                  Method rm=o.getClass().getMethod("setStatus",new Class[] { Integer.class });
                  // Invoke method
                  Object oj=rm.invoke(o,new Object[] { new Integer(FORMSServiceConstants.CHECKSTRING_ERROR) } );
                  out.println(ObjectUtil.objectToEncodedString(oj));
                  return null;
               }

               Method m=c.getMethod("invokeTargetMethod",new Class[] { HttpServletRequest.class, HttpServletResponse.class, Object.class });

               // Invoke method
               out.println((String) m.invoke(thisobj,new Object[] { getRequest(), getResponse(), (Object) parms } ));

               return null;
      
            } catch (Exception e) {

               o=rc.newInstance();
               Method rm=o.getClass().getMethod("setStatus",new Class[] { Integer.class });
               // Invoke method
               Object oj=rm.invoke(o,new Object[] { new Integer(FORMSServiceConstants.SERVICEINTERCEPTOR_EXCEPTION) } );
               out.println(ObjectUtil.objectToEncodedString(oj));
               return null;
      
            }

            //////////////////////////////////////////
            // END Remote SSA connection processing //
            //////////////////////////////////////////
   
         } catch (NoSuchMethodException nsme) {

            /////////////////////
            // CSA connections //
            /////////////////////
            
            LoginResult lresult=new LoginResult();
   
            im=ic.getMethod("getUsername");
            String user=(String)im.invoke(parms,new Object[]{ });
            im=ic.getMethod("getPassword");
            String passwd=(String)im.invoke(parms,new Object[]{ });
            im=ic.getMethod("getCsaid");
            Long csaid=(Long)im.invoke(parms,new Object[]{ });

            // refresh application context, obtaining connection to CSAMainDB
            SessionObjectUtil.readMainDBIntoContext(getSession(),user,passwd);
   
            Authenticator cauth=new Authenticator(request,response);
   
            int hostresult=cauth.checkHost();
   
            if (hostresult==AuthenticatorConstants.HOST_DENIED) {

               lresult.setStatus(hostresult);
               out.println(ObjectUtil.objectToEncodedString(lresult));
               return null;
   
            } 

            lresult=cauth.login(user,passwd);
   
            // Ignore pw messages here, they will be handled on SSA
   
            if (
                lresult.getStatus()!=AuthenticatorConstants.LOGGED_IN &&
                lresult.getStatus()!=AuthenticatorConstants.PWEXPIRESSOON &&
                lresult.getStatus()!=AuthenticatorConstants.PWEXPIRED 
               ) {

               // return if not logged in
               out.println(ObjectUtil.objectToEncodedString(lresult));
               return null;
            }

            // Perform user request
   
            // retrieve object stored as bean
            Object thisobj=this.getContext().getBean(beanname);
            // return object class
            Class c=thisobj.getClass();

            Method cm;
            try {
               // Call method with passed parameters
               cm=c.getMethod(methodname,new Class[] { ic });
            } catch (NoSuchMethodException nsme2) {
               try {
                  // Call method with no parameters
                  cm=c.getMethod(methodname,new Class[] { });
               } catch (NoSuchMethodException nsme2b) {
                  try {
                  // Call method passing Object
                  cm=c.getMethod(methodname,new Class[] { Object.class });
                  } catch (Exception nsme2c) {
                     cm=c.getMethod(methodname,new Class[] { Object.class });
                  }
               }
            }
            Class rc=cm.getReturnType();
            Object o=null;

            try {

               Method m=c.getMethod("invokeTargetMethod",new Class[] { HttpServletRequest.class, HttpServletResponse.class, Object.class });

               // Invoke method
               String returnstr=(String)m.invoke(thisobj,new Object[] { getRequest(), getResponse(), (Object) parms } );
               out.println(returnstr);
               return null;
      
            } catch (Exception e) {

               o=rc.newInstance();
               Method rm=o.getClass().getMethod("setStatus",new Class[] { Integer.class });
               // Invoke method
               Object oj=rm.invoke(o,new Object[] { new Integer(FORMSServiceConstants.SERVICEINTERCEPTOR_EXCEPTION) } );

               out.println(ObjectUtil.objectToEncodedString(oj));
               return null;

            } 
   
            ///////////////////////////////////
            // END CSA connection processing //
            ///////////////////////////////////
   
         } 

      } catch (Exception se) {

         throw new FORMSException("FORMSServiceController threw an exception");

      }

   }   

}


