 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 

/*
 *  Program:  StudyUserListServiceTarget.java - Retrieve list of users having rights to a specific 
 *            study
 *                                                          MRH 04/2008
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;

public class StudyUserListServiceTarget extends FORMSStudyServiceTarget {

   // Retrieves a list of users who have any permissions on the current study

   public SsaServiceListResult getUserList() {

      List puserlist=(List)new ArrayList();
      List nuserlist=null;
      try {

            SsaServiceListResult result=new SsaServiceListResult();
      
            ////////////////////////////////////////////////////////////////////////
            // first, create list of study roles.  I think it's a safe assumption //
            // here that all roles have at least some assigned study permissions, //
            // so we'll assume that all users assigned to a study role has        //
            // some study permissions                                             //
            ////////////////////////////////////////////////////////////////////////
      
            Iterator i;
            List rolelist=getMainDAO().execQuery(
               "select r from Resourceroles r where rolelevel=" + VisibilityConstants.STUDY + 
                  " and studyid=" + getLocalstudyid()
                  );

            // Retrieve list of users
            List alluserlist=getMainDAO().execQuery(
               "select a from Allusers a join fetch a.allinst " + 
                  "where a.allinst.islocal=true"
               );

            nuserlist=(List)new ArrayList(alluserlist);   

            // create id list for subsequent queries
            i=nuserlist.iterator();
            StringBuilder ausersb=new StringBuilder();
            while (i.hasNext()) {
               Allusers users=(Allusers)i.next();
               ausersb.append(new Long(users.getAuserid()).toString());
               if (i.hasNext()) ausersb.append(",");
            }
      
            // Retrieve sysuserperms
            List sysuserpermlist=getMainDAO().execQuery(
               "select s from Sysuserpermassign s where s.studyid=" + getLocalstudyid() +
                    " and s.permissioncats.pcatvalue=" + PcatConstants.STUDY +
                    " and s.user.auserid in (" + ausersb.toString() + ") "
               );


            // create datatabledeflist for use later
            List datatabledeflist=getMainDAO().execQuery(
               "select f from Datatabledef f where f.pformhaid in " +
                  "(select f.formhaid from Formha f where f.studies.studyid=" + getLocalstudyid() + ")"
               );

            // create id list for subsequent queries
            i=datatabledeflist.iterator();
            StringBuilder datatabledefsb=new StringBuilder();
            while (i.hasNext()) {
               Datatabledef fdef=(Datatabledef)i.next();
               datatabledefsb.append(new Long(fdef.getDtdefid()).toString());
               if (i.hasNext()) datatabledefsb.append(",");
            }

            StringBuilder querysb=new StringBuilder("select f from Dtuserpermassign f " +
               "where f.allusers.auserid in (" + ausersb.toString() + ") ");

            if (datatabledefsb.length()>0) {
              querysb.append("and f.dtdefid in (" + datatabledefsb.toString() + ")"); 
            }

            // Retrieve formuserperms
            List formuserpermlist=getMainDAO().execQuery(querysb.toString());

            // Iterate through user list, return list of users beloning to study roles
            // or having study system or resource permissions
      
            Iterator uiter=alluserlist.iterator();
            while (uiter.hasNext()) {
               boolean hasperm=false;
               Allusers user=(Allusers)uiter.next();
               // First check roles to see if user belongs to study role
               PermHelper helper=new PermHelper(getRequest(),getResponse(),true);

               List userroles=helper.getRoleIdList(new Long(user.getAuserid()).toString());
      
               Iterator riter=userroles.iterator();
               while (riter.hasNext()) {
                  Long rroleid=(Long)riter.next();
                  Iterator r2iter=rolelist.iterator();
                  while (r2iter.hasNext()) {
                     Resourceroles role=(Resourceroles)r2iter.next();
                     if (role.getRroleid()==rroleid.longValue()) {
                        hasperm=true;
                        userHasPerm(user,puserlist);
                        break;
                     }
                  }
                  if (hasperm) break;
               }
               if (hasperm) continue;
               // Next check system permissions, to see if user has user-specific permissions
               Iterator supiter=sysuserpermlist.iterator();
               while (supiter.hasNext()) {
                  Sysuserpermassign sup=(Sysuserpermassign)supiter.next();
                  if (sup.getUser().getAuserid()==user.getAuserid()) {
                     if (((BitSet)sup.getPermlevel()).cardinality()>0) {
                        userHasPerm(user,puserlist);
                        hasperm=true;
                     }
                     break;
                  }
               }
               if (hasperm) continue;
               // Next check form permissions, to see if user has user-specific permissions
               Iterator fupiter=formuserpermlist.iterator();
               while (fupiter.hasNext()) {
                  Dtuserpermassign fup=(Dtuserpermassign)fupiter.next();
                  if (fup.getAllusers().getAuserid()==user.getAuserid()) {
                     if (((BitSet)fup.getPermlevel()).cardinality()>0) {
                        userHasPerm(user,puserlist);
                        hasperm=true;
                        break;
                     }
                  }
               }
      
            }
            updateUserLists(puserlist,nuserlist);
      
            result.setStatus(FORMSServiceConstants.OK);
            result.setList(puserlist);

            return result;

      } catch (Exception e) {
            return null;
      }

   }

   // add user to puserlist
   private void userHasPerm(Allusers user,List puserlist) {
      puserlist.add(user);
   }

   // update user lists
   private void updateUserLists(List puserlist,List nuserlist) {
      Iterator piter=puserlist.iterator();
      while (piter.hasNext()) {
         Allusers user=(Allusers)piter.next();
         nuserlist.remove(user);
      }
   }

}





