 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  Program:  SsaServiceListResult.java - 
 *                                                          MRH 03/2008
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;

public class JoinStudiesServiceTarget extends FORMSServiceTarget {

   public SsaServiceListResult getStudiesList() {

      SsaServiceListResult result=new SsaServiceListResult();
      // Verify connection to FORMSMainDB
      try {
         List l=getMainDAO().getTable("Studies");
         result.setStatus(new Integer(FORMSServiceConstants.OK));
         result.setList(l);
      } catch (Exception e) {
         result.setStatus(new Integer(FORMSServiceConstants.NO_DATA_ACCESS));
      }

      return result;

   }

   public JoinStudiesResult getStudy(JoinStudiesParms parms) {

      JoinStudiesResult result=new JoinStudiesResult();
      long studyid=parms.getStudyid();
      // Verify connection to FORMSMainDB
      try {
         Studies std=(Studies)getMainDAO().execUniqueQuery(
            "select std from Studies std where std.studyid=" + studyid
            );
         result.setStudydesc(std.getStudydesc());   
         result.setStatus(new Integer(FORMSServiceConstants.OK));
      } catch (Exception e) {
         result.setStatus(new Integer(FORMSServiceConstants.NO_DATA_ACCESS));
      }

      return result;

   }

}

