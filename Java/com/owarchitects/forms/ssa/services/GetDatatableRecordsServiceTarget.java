 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  Program:  GetDatatableRecordsServiceTarget.java - Retrieve all records from all forms 
 *            (by permission) for a given patient
 *                                                          MRH 01/2009
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;

public class GetDatatableRecordsServiceTarget extends FORMSServiceTarget {

   String sortvar=null;
   String sortdir=null;
   String dtdefid=null;

   // SSA connection verification
   public SsaServiceListResult getTableDisplayRecords(SsaServicePassObjectParms parms) {

      SsaServiceListResult result=new SsaServiceListResult();
      HashMap passmap=(HashMap)parms.getObject();
      sortvar=(String)passmap.get("sortvar");
      sortdir=(String)passmap.get("sortdir");
      dtdefid=(String)passmap.get("dtdefid");

      List<Datatablerecords> records;

      try {

         if (sortvar==null) {   
      
            StringBuilder queryString=new StringBuilder(
               "select distinct d from Datatablerecords d join fetch d.datatablevalues " +
                  " where d.datatabledef.dtdefid=" + dtdefid + " and d.isaudit!='T'"
               );   
      
            records=getMainDAO().execPagedQuery(queryString.toString(),0,getMaxRows().intValue());
      
         } else {
   
            StringBuilder queryString=new StringBuilder(
               "select distinct d.datatablerecords.datarecordid,d.varvalue " +
                   " from Datatablevalues d " +
                  " where d.datatablerecords.datatabledef.dtdefid=" + dtdefid + 
                    " and d.datatablerecords.isaudit!='T'" + 
                      " and upper(d.varname)='" + sortvar + "'" + 
                    " order by d.varvalue " + sortdir
               );   
            List ordridlist=getMainDAO().execQuery(queryString.toString());
   
            // If field is numeric, order by numeric value
            List ckNumList=getMainDAO().execQuery(
                  "select d.value from Dtfieldattrs d " +
                     " where d.datatabledef.dtdefid=" + dtdefid +
                      " and upper(d.fieldname)='" + sortvar + "'" + 
                      " and d.attr='VT'" + 
                      " and d.value in ('INT','DEC') "
               );
            if (!ckNumList.isEmpty()) {
   
               Collections.sort(ordridlist,new CompareByNum((String)ckNumList.get(0)));
   
            }
   
            // construct list for where clause
            Iterator ordriter=ordridlist.iterator();
            StringBuilder idsb=new StringBuilder("(");
            while (ordriter.hasNext()) {
               idsb.append(((Object[])ordriter.next())[0].toString());
               if (ordriter.hasNext()) idsb.append(",");
            }
            idsb.append(")");
            // Merge in missing values if any
            queryString=new StringBuilder(
               "select d.datarecordid from Datatablerecords d " +
                  " where d.datatabledef.dtdefid=" + dtdefid +  
                    " and d.datatablerecords.isaudit!='T'"  
               );   
            if (ordridlist.size()>0) {          
               queryString.append(" and d.datarecordid not in " + idsb); 
            }          
            List misslist=getMainDAO().execQuery(queryString.toString());
   
            ArrayList<Long> sortlist=new ArrayList();
   
            Iterator missiter=misslist.iterator();
            ordriter=ordridlist.iterator();
            idsb=new StringBuilder("(");
            int countvar=0;
            if (sortdir.equalsIgnoreCase("asc")) {
               while (missiter.hasNext() && countvar<getMaxRows().intValue()) {
                  countvar++;
                  if (countvar>1) idsb.append(",");
                  Long appValue=(Long)missiter.next();
                  idsb.append(appValue.toString());
                  sortlist.add(appValue);
               }
               while (ordriter.hasNext() && countvar<getMaxRows().intValue()) {
                  countvar++;
                  if (countvar>1) idsb.append(",");
                  Long appValue=(Long)(((Object[])ordriter.next())[0]);
                  idsb.append(appValue.toString());
                  sortlist.add(appValue);
               }
            } else {
               while (ordriter.hasNext() && countvar<getMaxRows().intValue()) {
                  countvar++;
                  if (countvar>1) idsb.append(",");
                  Long appValue=(Long)(((Object[])ordriter.next())[0]);
                  idsb.append(appValue.toString());
                  sortlist.add(appValue);
               }
               while (missiter.hasNext() && countvar<getMaxRows().intValue()) {
                  countvar++;
                  if (countvar>1) idsb.append(",");
                  Long appValue=(Long)missiter.next();
                  idsb.append(appValue.toString());
                  sortlist.add(appValue);
               }
            }
            idsb.append(")");
            
            queryString=new StringBuilder(
               "select distinct d from Datatablerecords d join fetch d.datatablevalues " +
                  " where d.datatabledef.dtdefid=" + dtdefid +  
                    " and d.isaudit!='T'" +  
                    " and d.datarecordid in " + idsb.toString()
               );   
            List<Datatablerecords> trecords=getMainDAO().execQuery(queryString.toString());
            records=new ArrayList(trecords.size());
            // create ordered list for output;
            Iterator sortiter=sortlist.iterator();
            while (sortiter.hasNext()) {
               Long drid=(Long)sortiter.next();
               Iterator<Datatablerecords> titer=trecords.iterator();
               while (titer.hasNext()) {
                  Datatablerecords record=titer.next();
                  if (record.getDatarecordid()==drid.longValue()) {
                     records.add(record);
                     titer.remove();
                  }
               }
            }
   
         }

      } catch (Exception ex) {
         records=(List)new ArrayList();
      }

      result.setList(records);
      result.setStatus(FORMSServiceConstants.OK);
      return result;

   }


   // SSA connection verification
   public SsaServiceListResult getSearchDisplayRecords(SsaServicePassObjectParms parms) {

      SsaServiceListResult result=new SsaServiceListResult();
      HashMap passmap=(HashMap)parms.getObject();
      sortvar=(String)passmap.get("sortvar");
      sortdir=(String)passmap.get("sortdir");
      dtdefid=(String)passmap.get("dtdefid");
      String matchcase=(String)passmap.get("matchcase");
      String exactmatch=(String)passmap.get("exactmatch");
      String searchtext=(String)passmap.get("searchtext");

      List<Datatablerecords> records;

      try {

         // find matching record values and construct list of datarecordids
         StringBuilder qsb=new StringBuilder();
         qsb.append("select distinct d.datatablerecords.datarecordid from Datatablevalues d " + 
                     "where d.datatablerecords.datatabledef.dtdefid=" + dtdefid + 
                     " and d.datatablerecords.isaudit!='T' and ");
         if (matchcase!=null && exactmatch!=null && matchcase.equalsIgnoreCase("X") && exactmatch.equalsIgnoreCase("X")) {
            qsb.append("d.varvalue='" + searchtext.trim() + "'");
         } else if (matchcase!=null && matchcase.equalsIgnoreCase("X")) {
            qsb.append("d.varvalue like '%" + searchtext.trim() + "%'");
         } else if (exactmatch!=null && exactmatch.equalsIgnoreCase("X")) {
            qsb.append("upper(d.varvalue)=upper('" + searchtext.trim() + "')");
         } else {
            qsb.append("upper(d.varvalue) like upper('%" + searchtext.trim() + "%')");
         } 
   
         List dtvalues=getMainDAO().execQuery(qsb.toString());
   
         if (dtvalues.size()>0) {
   
            Iterator dtiter=dtvalues.iterator();
            StringBuilder insb=new StringBuilder("(");
            while (dtiter.hasNext()) {
               insb.append(dtiter.next().toString());
               if (dtiter.hasNext()) {
                  insb.append(",");
               }
            }
            insb.append(")");
   
            if (sortvar==null) {
   
               StringBuilder queryString=new StringBuilder(
                  "select distinct d from Datatablerecords d join fetch d.datatablevalues where d.datatabledef.dtdefid=" + dtdefid +
                     " and d.isaudit!='T' " +
                     " and d.datarecordid in " + insb.toString() 
                  );   
         
               records=getMainDAO().execPagedQuery(queryString.toString(),0,getMaxRows().intValue());
   
            } else {
   
               StringBuilder queryString=new StringBuilder(
                  "select distinct d.datatablerecords.datarecordid,d.varvalue " +
                      " from Datatablevalues d " +
                     " where d.datatablerecords.datatabledef.dtdefid=" + dtdefid + 
                         " and d.datatablerecords.isaudit!='T' " +
                         " and d.datatablerecords.datarecordid in " + insb.toString() + 
                         " and upper(d.varname)='" + sortvar + "'" + 
                       " order by d.varvalue " + sortdir
                  );   
               List ordridlist=getMainDAO().execQuery(queryString.toString());
      
               // If field is numeric, order by numeric value
               List ckNumList=getMainDAO().execQuery(
                     "select d.value from Dtfieldattrs d " +
                        " where d.datatabledef.dtdefid=" + dtdefid +
                         " and upper(d.fieldname)='" + sortvar + "'" + 
                         " and d.attr='VT'" + 
                         " and d.value in ('INT','DEC') "
                  );
               if (!ckNumList.isEmpty()) {
      
                  Collections.sort(ordridlist,new CompareByNum((String)ckNumList.get(0)));
      
               }
      
               // construct list for where clause
               Iterator ordriter=ordridlist.iterator();
               StringBuilder idsb=new StringBuilder("(");
               while (ordriter.hasNext()) {
                  idsb.append(((Object[])ordriter.next())[0].toString());
                  if (ordriter.hasNext()) idsb.append(",");
               }
               idsb.append(")");
               // Merge in missing values if any
               queryString=new StringBuilder(
                  "select d.datarecordid from Datatablerecords d " +
                     " where d.datatabledef.dtdefid=" + dtdefid + 
                       " and d.isaudit!='T' " +
                       " and d.datarecordid in " + insb.toString() 
                  );   
               if (ordridlist.size()>0) {          
                  queryString.append(" and d.datarecordid not in " + idsb); 
               }          
               List misslist=getMainDAO().execQuery(queryString.toString());
      
               ArrayList<Long> sortlist=new ArrayList();
      
               Iterator missiter=misslist.iterator();
               ordriter=ordridlist.iterator();
               idsb=new StringBuilder("(");
               int countvar=0;
               if (sortdir.equalsIgnoreCase("asc")) {
                  while (missiter.hasNext() && countvar<getMaxRows().intValue()) {
                     countvar++;
                     if (countvar>1) idsb.append(",");
                     Long appValue=(Long)missiter.next();
                     idsb.append(appValue.toString());
                     sortlist.add(appValue);
                  }
                  while (ordriter.hasNext() && countvar<getMaxRows().intValue()) {
                     countvar++;
                     if (countvar>1) idsb.append(",");
                     Long appValue=(Long)(((Object[])ordriter.next())[0]);
                     idsb.append(appValue.toString());
                     sortlist.add(appValue);
                  }
               } else {
                  while (ordriter.hasNext() && countvar<getMaxRows().intValue()) {
                     countvar++;
                     if (countvar>1) idsb.append(",");
                     Long appValue=(Long)(((Object[])ordriter.next())[0]);
                     idsb.append(appValue.toString());
                     sortlist.add(appValue);
                  }
                  while (missiter.hasNext() && countvar<getMaxRows().intValue()) {
                     countvar++;
                     if (countvar>1) idsb.append(",");
                     Long appValue=(Long)missiter.next();
                     idsb.append(appValue.toString());
                     sortlist.add(appValue);
                  }
               }
               idsb.append(")");
               
               queryString=new StringBuilder(
                  "select distinct d from Datatablerecords d join fetch d.datatablevalues " +
                     " where d.datatabledef.dtdefid=" + dtdefid +  
                       " and d.isaudit!='T' " +
                       " and d.datarecordid in " + idsb.toString()
                  );   
               List<Datatablerecords> trecords=getMainDAO().execQuery(queryString.toString());
               records=new ArrayList(trecords.size());
               // create ordered list for output;
               Iterator sortiter=sortlist.iterator();
               while (sortiter.hasNext()) {
                  Long drid=(Long)sortiter.next();
                  Iterator<Datatablerecords> titer=trecords.iterator();
                  while (titer.hasNext()) {
                     Datatablerecords record=titer.next();
                     if (record.getDatarecordid()==drid.longValue()) {
                        records.add(record);
                        titer.remove();
                     }
                  }
               }
            }
   
         } else {
   
            records=new ArrayList();
   
         }


      } catch (Exception ex) {
         records=(List)new ArrayList();
      }

      result.setList(records);
      result.setStatus(FORMSServiceConstants.OK);
      return result;

   }

   private Integer getMaxRows() throws FORMSException, FORMSSecurityException, FORMSKeyException {

      Integer maxrows=null;
      try {
         maxrows=new Integer(getAuth().getValue("maxrows"));
      } catch (NumberFormatException nfe) {
         // do nothing
      }
      if (maxrows==null) {
         maxrows=200;
      }
      return maxrows;
   }

   private class CompareByNum implements Comparator<Object[]>{

      String type;

      public CompareByNum(String type) {
         this.type=type;
      }

      public int compare(Object[] oa1,Object[] oa2) {

         if (type.equalsIgnoreCase("INT")) {

            Long d1;
            try {
               d1=new Long(oa1[1].toString());
            } catch (Exception e) {
               d1=new Long(Long.MIN_VALUE);
            }
            Long d2;
            try {
               d2=new Long(oa2[1].toString());
            } catch (Exception e) {
               d2=new Long(Long.MIN_VALUE);
            }
            if (sortdir.equalsIgnoreCase("ASC")) {
               return d1.compareTo(d2);
            } else {
               return d2.compareTo(d1);
            }

         } else {

            Double d1;
            try {
               d1=new Double(oa1[1].toString());
            } catch (Exception e) {
               d1=new Double(Double.MIN_VALUE);
            }
            Double d2;
            try {
               d2=new Double(oa2[1].toString());
            } catch (Exception e) {
               d2=new Double(Double.MIN_VALUE);
            }
            if (sortdir.equalsIgnoreCase("ASC")) {
               return d1.compareTo(d2);
            } else {
               return d2.compareTo(d1);
            }

         }

      }

   }

}

