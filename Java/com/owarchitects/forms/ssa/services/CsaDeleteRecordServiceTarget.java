 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  Program:  CsaDeleteRecordServiceTarget.java - deletes patient record (local or remote)
 *                                                          MRH 03/2008
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;

public class CsaDeleteRecordServiceTarget extends FORMSSsaServiceClientServiceTarget {

   // Delete remote SSA record

   public CsaServiceStatusResult deleteRecord(CsaDeleteRecordParms parms) {

      try {

         Datatablerecords rec=parms.getRecord();
         Linkedinst forminst=parms.getForminst();

         if (parms.getFormloc()==LockInfo.LOCALFORM) {

            rec.setIsaudit(true);
            rec.setAudittype('D');
            rec.setAudituser(new Long(getAuth().getValue("auserid")).longValue());
            rec.setAudittime(new Date());
            getMainDAO().saveOrUpdate(rec);

         } else {

            submitServiceRequest(forminst,"formRecordProcessorServiceTarget","deleteRecord",rec);

         }   

         CsaServiceStatusResult result=new CsaServiceStatusResult();
         result.setStatus(FORMSServiceConstants.OK);
         return result;

      } catch (Exception e) {

         CsaServiceStatusResult result=new CsaServiceStatusResult();
         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
         return result;

      }

   }

}

