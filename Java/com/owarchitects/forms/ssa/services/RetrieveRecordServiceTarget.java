 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  Program:  RetrieveRecordServiceTarget.java - Retrieve patient record
 *            (by permission) for a given patient
 *                                                          MRH 01/2009
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;

public class RetrieveRecordServiceTarget extends FORMSServiceTarget {

   // SSA connection verification
   public RetrieveRecordServiceResult getPatientRecord(SsaServicePassObjectParms parms) {

      RetrieveRecordServiceResult result=new RetrieveRecordServiceResult();

      try {
   
         String auserid=null;
         try {
            auserid=getAuserid(parms.getLuserid()).toString();
         } catch (Exception e2) {
            // Do nothing
         }
   
         Long datarecordid=(Long)parms.getObject();
   
         String querystring="select f from Datatablerecords f where f.datarecordid=" + datarecordid;
         Datatablerecords rec=(Datatablerecords)getMainDAO().execUniqueQuery(querystring);
         result.setRecord(rec);
   
         try {
            if (rec!=null && (rec.getSaveuser()==(new Long(auserid)).longValue())) {
               result.setIsInputUser(true);
            }
         } catch (Exception e) {
            // DO NOTHING
         }

         result.setStatus(new Integer(FORMSServiceConstants.OK));

         return result;

      } catch (Exception e2) {

         result.setStatus(new Integer(FORMSServiceConstants.SERVICETARGET_EXCEPTION));
         return result;
      }
         
   }

}

