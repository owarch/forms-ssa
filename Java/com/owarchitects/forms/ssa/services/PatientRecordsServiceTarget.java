 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  Program:  PatientRecordsServiceTarget.java - Retrieve all records from all forms 
 *            (by permission) for a given patient
 *                                                          MRH 01/2009
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;

public class PatientRecordsServiceTarget extends FORMSServiceTarget {

   PatientFormsSortMap namemap;

   // SSA connection verification
   public SsaServiceListResult retrievePatientRecords(PatientFormsServiceParms parms) {

      SsaServiceListResult result=new SsaServiceListResult();
         
      List fdlist=parms.getList();  
      String wcls=parms.getWhereclause();  
      int varcount=parms.getVarcount();

      //////////////////////////
      //////////////////////////

      // create stringbuffer of resulting ids for insertion into where clause
      Iterator i=fdlist.iterator();
      StringBuilder fsb=new StringBuilder();
      while (i.hasNext()) {
         Long fid=((Datatabledef)i.next()).getRemotedtdefid();
         fsb.append(fid);
         if (i.hasNext()) fsb.append(",");
      }

      // pull matching records
      List l=getMainDAO().execQuery(
         "select f.datatablerecords.datarecordid,count(*) from Datatablevalues f " +
            "where (" + wcls + ") and " +
               "(f.datatablerecords.isaudit=false and f.datatablerecords.ishold=false and f.datatablerecords.datatabledef.dtdefid in (" + fsb.toString() + ")) " +
               "group by f.datatablerecords.datarecordid"
            );

      StringBuilder isb=new StringBuilder();
      i=l.iterator();
      while (i.hasNext()) {
         Object[] oarray=(Object[])i.next();
         Long did=(Long)oarray[0];
         Long cnt=(Long)oarray[1];
         if (cnt.intValue()==varcount) {
            isb.append(did + ",");
         }
      }
      if (isb.length()>0) {
         isb.deleteCharAt(isb.lastIndexOf(","));
      }
  
      // create list of all key variables for display (All available key fields will be displayed)
      List vnlist=getMainDAO().execQuery(
         "select distinct f.columnname from Datatablecoldef f where f.datatabledef.dtdefid in (" + fsb.toString() + ") and (iskeyfield=true) "
         );
      StringBuilder vnsb=new StringBuilder();
      i=vnlist.iterator();   
      while (i.hasNext()) {
         String s=(String)i.next();
         vnsb.append("'" + s + "'" );
         if (i.hasNext()) vnsb.append(",");
      }
  
      // now pull actual key variable values for all forms
      l=getMainDAO().execQuery(
        "select f from Datatablevalues f join fetch f.datatablerecords join fetch f.datatablerecords.datatabledef " +
           "where f.datatablerecords.datarecordid in (" + isb.toString() + ") and " +
              "(f.varname in (" + vnsb + ")) " +
              "order by f.datatablerecords.datatabledef.formno,f.datatablerecords.datatabledef.formver,f.datatablerecords.datarecordid,f.varname"
           );
  
      // reorganize data for display 
      ArrayList reclist=new ArrayList();
      i=l.iterator();
      PatientFormsSortMap varmap=null;
      long l_datarecordid=-999999;
      while (i.hasNext()) {
         Datatablevalues v=(Datatablevalues)i.next();
         if (v.getDatatablerecords().getDatarecordid()!=l_datarecordid) {
            // output previous varmap
            if (varmap!=null) reclist.add(varmap);
            varmap=new PatientFormsSortMap();
            varmap.put("datarecordid",v.getDatatablerecords().getDatarecordid());
            varmap.put("dtdefid",replaceID(fdlist,v.getDatatablerecords().getDatatabledef().getDtdefid()));
            varmap.put("FormNo",v.getDatatablerecords().getDatatabledef().getFormno());
            varmap.put("FormVer",v.getDatatablerecords().getDatatabledef().getFormver());
            varmap.put("FormAcr",v.getDatatablerecords().getDatatabledef().getFormacr());
            varmap.put(v.getVarname(),v.getVarvalue());
            varmap.put("User",getUserName(v.getDatatablerecords().getSaveuser()));
            varmap.put("Time",v.getDatatablerecords().getSavetime().toString().replaceFirst("\\..*$",""));
            l_datarecordid=v.getDatatablerecords().getDatarecordid();
         } else {
            varmap.put(v.getVarname(),v.getVarvalue());
         }
         if (!i.hasNext()) reclist.add(varmap);
      }

      result.setList(reclist);
      result.setStatus(FORMSServiceConstants.OK);
      return result;

   }

   // Replace local ID with id of remote server
   private long replaceID(List fdlist,long thisid) {
      Iterator i=fdlist.iterator();
      while (i.hasNext()) {
         Datatabledef fd=(Datatabledef)i.next();
         if (thisid==fd.getRemotedtdefid()) {
            return fd.getDtdefid();
         }
      }
      return thisid;
   }

   private String getUserName(long auserid) {

      try {
         if (namemap==null) {
            namemap=new PatientFormsSortMap();
            // create name map
            List ulist=getMainDAO().execQuery("select a from Allusers a");
            Iterator i=ulist.iterator();
            while (i.hasNext()) {
               Allusers user=(Allusers)i.next();
               namemap.put(new Long(user.getAuserid()),user.getUserdesc().replaceFirst("\\[.*$",""));
            }   
         }
         return (String)namemap.get(new Long(auserid));
      } catch (Exception e) {
         return new Long(auserid).toString();
      }

   }

}

