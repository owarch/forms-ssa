 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  Program:  CsaDataTransferServiceTarget.java - Transfer SSA meta-data to CSA
 *                                                          MRH 01/2008
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import org.hibernate.*;
//import org.springframework.web.servlet.*;
//import org.springframework.web.servlet.mvc.*;
//import org.springframework.web.context.support.*;
import org.springframework.orm.hibernate3.*;
import java.lang.reflect.*;
//import org.apache.commons.io.FileUtils;


public class CsaDataTransferServiceTarget extends FORMSServiceTarget {

   public CsaDataTransferResult getTable(CsaDataTransferParms parms) {

      CsaDataTransferResult result=new CsaDataTransferResult();
      try {

         String mappingclassname=parms.getMappingClassName();

         // For SSA-Specific Tables, send back empty list, if list is requested
         if (mappingclassname.indexOf("Access") >=0 ||
             mappingclassname.indexOf("Csainfo") >=0 ||

             mappingclassname.indexOf("Mediaha") >=0 ||
             mappingclassname.indexOf("Mediainfo") >=0 ||
             mappingclassname.indexOf("Mediapermprofassign") >=0 ||
             mappingclassname.indexOf("Mediapermprofiles") >=0 ||
             mappingclassname.indexOf("Mediarolepermassign") >=0 ||
             mappingclassname.indexOf("Mediarolepermprofassign") >=0 ||
             mappingclassname.indexOf("Mediastore") >=0 ||
             mappingclassname.indexOf("Mediauserpermassign") >=0 ||
             mappingclassname.indexOf("Remotemediacat") >=0 ||

             mappingclassname.indexOf("Report") >=0 ||
             mappingclassname.indexOf("Remotereportcat") >=0 ||

             mappingclassname.indexOf("Analystgrouppermassign") >=0 ||
             mappingclassname.indexOf("Analystgroups") >=0 ||
             mappingclassname.indexOf("Analystgroupuserassign") >=0 ||
             mappingclassname.indexOf("Analystha") >=0 ||
             mappingclassname.indexOf("Analysthagrouppermassign") >=0 ||
             mappingclassname.indexOf("Analysthauserpermassign") >=0 ||
             mappingclassname.indexOf("Analystinfo") >=0 ||
             mappingclassname.indexOf("Analystrolepermassign") >=0 ||
             mappingclassname.indexOf("Analystroles") >=0 ||
             mappingclassname.indexOf("Analystroleuserassign") >=0 ||
             mappingclassname.indexOf("Analyststore") >=0 ||
             mappingclassname.indexOf("Analystuserpermassign") >=0 ||
             mappingclassname.indexOf("Analystusersyspermassign") >=0 ||
    
             mappingclassname.indexOf("Datatablerecords") >=0 ||
             mappingclassname.indexOf("Datatablevalues") >=0 ||
             mappingclassname.indexOf("Dtfieldattrdefaults") >=0 ||
             mappingclassname.indexOf("Dtfieldattrs") >=0 ) {
            result.setResultlist(new ArrayList());
            result.setStatus(FORMSServiceConstants.OK);
            return result;
         }    
         List tablelist=null;

         // Construct query string to pull class, fetching parent objects
         StringBuilder qsb=new StringBuilder("select r from " + mappingclassname + " r");
         Class mclass=Class.forName(mappingclassname);
         Method[] mmeth=mclass.getDeclaredMethods();
         Iterator miter=Arrays.asList(mmeth).iterator();
         while (miter.hasNext()) {
            Method m=(Method)miter.next();
            Class[] p=m.getParameterTypes();
           if (p.length==1 && p[0].getName().indexOf("com.owarchitects.forms.commons.db.")==0) { 
               // Remove "set" from beginning & set to lower case for fieldname
               String vname=m.getName().toLowerCase().substring(3);
               qsb.append(" left join fetch r." + vname);
               vname=null;
            }
            m=null;
            p=null;
         }

         // execute constructed query
         tablelist=getMainDAO().execQuery(qsb.toString());

         if (tablelist!=null) {
            // Return resulting data list
            result.setResultlist(tablelist);
            qsb=null;
            mclass=null;
            mmeth=null;
            miter=null;
            tablelist=null;
            result.setStatus(FORMSServiceConstants.OK);
         } else {
            result.setStatus(FORMSServiceConstants.OTHER_ERROR);
         }
         mappingclassname=null;

      } catch (Exception e) {

         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
      }

      parms=null;
      return result;

   }

   public CsaDataTransferResult getIdentifiers(CsaDataTransferParms parms) {

      CsaDataTransferResult result=new CsaDataTransferResult();
      try {

         String mappingclassname=parms.getMappingClassName();

         // For SSA-Specific Tables, send back empty list, if list is requested
         if (mappingclassname.indexOf("Access") >=0 ||
             mappingclassname.indexOf("Csainfo") >=0 ||

             mappingclassname.indexOf("Mediaha") >=0 ||
             mappingclassname.indexOf("Mediainfo") >=0 ||
             mappingclassname.indexOf("Mediapermprofassign") >=0 ||
             mappingclassname.indexOf("Mediapermprofiles") >=0 ||
             mappingclassname.indexOf("Mediarolepermassign") >=0 ||
             mappingclassname.indexOf("Mediarolepermprofassign") >=0 ||
             mappingclassname.indexOf("Mediastore") >=0 ||
             mappingclassname.indexOf("Mediauserpermassign") >=0 ||
             mappingclassname.indexOf("Remotemediacat") >=0 ||

             mappingclassname.indexOf("Analystgrouppermassign") >=0 ||
             mappingclassname.indexOf("Analystgroups") >=0 ||
             mappingclassname.indexOf("Analystgroupuserassign") >=0 ||
             mappingclassname.indexOf("Analystha") >=0 ||
             mappingclassname.indexOf("Analysthagrouppermassign") >=0 ||
             mappingclassname.indexOf("Analysthauserpermassign") >=0 ||
             mappingclassname.indexOf("Analystinfo") >=0 ||
             mappingclassname.indexOf("Analystrolepermassign") >=0 ||
             mappingclassname.indexOf("Analystroles") >=0 ||
             mappingclassname.indexOf("Analystroleuserassign") >=0 ||
             mappingclassname.indexOf("Analyststore") >=0 ||
             mappingclassname.indexOf("Analystuserpermassign") >=0 ||
             mappingclassname.indexOf("Analystusersyspermassign") >=0 ||

             mappingclassname.indexOf("Datatablerecords") >=0 ||
             mappingclassname.indexOf("Datatablevalues") >=0 ||
             mappingclassname.indexOf("Dtfieldattrdefaults") >=0 ||
             mappingclassname.indexOf("Dtfieldattrs") >=0 ) {
            result.setResultlist(new ArrayList());
            result.setStatus(FORMSServiceConstants.OK);
            return result;
         }    

         // execute constructed query
         List idlist=getMainDAO().getTableIdentifiers(mappingclassname);

         if (idlist!=null) {
            // Return resulting data list
            result.setResultlist(idlist);
            idlist=null;
            result.setStatus(FORMSServiceConstants.OK);
         } else {
            result.setStatus(FORMSServiceConstants.OTHER_ERROR);
         }
         mappingclassname=null;

      } catch (Exception e) {

         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);

      }

      parms=null;
      return result;

   }

   public CsaDataTransferResult execQuery(CsaDataTransferParms parms) {

      CsaDataTransferResult result=new CsaDataTransferResult();
      try {

         String querystring=parms.getQuerystring();

         // Return null for dis-allowed tables
         if (querystring.indexOf("Access") >=0 ||
             querystring.indexOf("Csainfo") >=0 ||

             querystring.indexOf("Mediaha") >=0 ||
             querystring.indexOf("Mediainfo") >=0 ||
             querystring.indexOf("Mediapermprofassign") >=0 ||
             querystring.indexOf("Mediapermprofiles") >=0 ||
             querystring.indexOf("Mediarolepermassign") >=0 ||
             querystring.indexOf("Mediarolepermprofassign") >=0 ||
             querystring.indexOf("Mediastore") >=0 ||
             querystring.indexOf("Mediauserpermassign") >=0 ||

             querystring.indexOf("Analystgrouppermassign") >=0 ||
             querystring.indexOf("Analystgroups") >=0 ||
             querystring.indexOf("Analystgroupuserassign") >=0 ||
             querystring.indexOf("Analystha") >=0 ||
             querystring.indexOf("Analysthagrouppermassign") >=0 ||
             querystring.indexOf("Analysthauserpermassign") >=0 ||
             querystring.indexOf("Analystinfo") >=0 ||
             querystring.indexOf("Analystrolepermassign") >=0 ||
             querystring.indexOf("Analystroles") >=0 ||
             querystring.indexOf("Analystroleuserassign") >=0 ||
             querystring.indexOf("Analyststore") >=0 ||
             querystring.indexOf("Analystuserpermassign") >=0 ||
             querystring.indexOf("Analystusersyspermassign") >=0 ||

             querystring.indexOf("Datatablerecords") >=0 ||
             querystring.indexOf("Datatablevalues") >=0 ||
             querystring.indexOf("Dtfieldattrdefaults") >=0 ||
             querystring.indexOf("Dtfieldattrs") >=0 ) {
            result.setResultlist(new ArrayList());
            result.setStatus(FORMSServiceConstants.OK);
            return result;
         }    
         List resultlist=null;

         try {
            // execute constructed query
            String[] parmnames=parms.getParmnames();
            Object[] parmvalues=parms.getParmvalues();
            if (parmnames!=null && parmvalues!=null) {

               resultlist=getMainDAO().execQuery(querystring.toString(),parmnames,parmvalues);

            } else {
               resultlist=getMainDAO().execQuery(querystring.toString());
            }
         } catch (Exception rse) {

            // return null set
         }

         if (resultlist!=null) {
            // Return resulting data list
            result.setResultlist(resultlist);
            result.setStatus(FORMSServiceConstants.OK);
         } else {
            result.setStatus(FORMSServiceConstants.OTHER_ERROR);
         }

      } catch (Exception e) {

         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
      }

      return result;

   }

}

