 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  Program:  GetMediaServiceTarget.java - Retrieve list of media files for which 
 *            user has a right 
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;

public class GetMediaServiceTarget extends FORMSStudyServiceTarget {

   // Retrieves a list of users who have any permissions on the current study

   public SsaServiceListResult getMediainfo(SsaServiceSystemStudyParms parms) {

      SsaServiceListResult result=new SsaServiceListResult();

      try {

         String auserid=null;
         try {
            auserid=getAuserid(parms.getLuserid()).toString();
         } catch (Exception e2) {
            // Do nothing
         }

         if (auserid!=null) {

            // pull list of permitted files for user (mplist)
            PermHelper helper=new PermHelper(getRequest(),getResponse());
            result.setStatus(new Integer(FORMSServiceConstants.OK));
            List mplist=helper.getMediaPermList(auserid);
            List outlist=(List)new ArrayList();

            if (mplist.size()>0) {

               Iterator i=mplist.iterator();
               StringBuilder sb=new StringBuilder();
               while (i.hasNext()) {
                  HashMap map=(HashMap)i.next();
                  String mediainfoid=(String)map.get("mediainfoid");
                  sb.append(mediainfoid);
                  if (i.hasNext()) sb.append(",");
               }
               // Pull all file records in one query

               List medialist=getMainDAO().execQuery(
                  "select minfo from Mediainfo minfo where minfo.mediainfoid in (" + sb + ")" 
               );

               // add file records to mplist
               i=mplist.iterator();
               while (i.hasNext()) {
                  HashMap map=(HashMap)i.next();
                  String mediainfoid=(String)map.get("mediainfoid");
                  Iterator fiter=medialist.iterator();
                  while (fiter.hasNext()) {
                     Mediainfo minfo=(Mediainfo)fiter.next();
                     if (minfo.getMediainfoid()==new Long(mediainfoid).longValue()) {
                        outlist.add(minfo);
                        fiter.remove();
                     }
                  }
               }

            }

            result.setList(outlist);
            return result;

         } else {

            // User has no study permissions
            result.setStatus(new Integer(FORMSServiceConstants.OK));
            result.setList((List)new ArrayList(0));
            return result;

         }

      } catch (Exception e) {

         result.setStatus(new Integer(FORMSServiceConstants.SERVICETARGET_EXCEPTION));
         result.setList((List)new ArrayList(0));
         return result;

      }
   }

   public SsaServiceObjectResult getContent(SsaServicePassObjectParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      SsaServiceObjectResult result=new SsaServiceObjectResult();

      try {

         String auserid=null;
         String mediainfoid=(String)parms.getObject();
         try {
            auserid=getAuserid(parms.getLuserid()).toString();
         } catch (Exception e2) {
            // Do nothing
         }

         PermHelper permhelper=new PermHelper(getRequest(),getResponse(),true);
   
         if (!permhelper.hasMediaPerm(auserid,mediainfoid,"OPENDOC")) {

            result.setStatus(FORMSServiceConstants.NO_PERMISSION);
            return result;
         }

         Mediastore store=(Mediastore)getMainDAO().execUniqueQuery(
            "select s from Mediastore s join fetch s.mediainfo join fetch s.mediainfo.supportedfileformats "
               + "where s.mediainfo.mediainfoid=" + mediainfoid
            );

         Transferstore tstore=new Transferstore();
         tstore.setMediastoreid(store.getMediastoreid());
         java.sql.Blob b=store.getMediacontent();
         tstore.setMediacontent(new String(b.getBytes(1l,new Long(b.length()).intValue()),"ISO-8859-1"));
         tstore.setMediainfo(store.getMediainfo());

         // Stored 

         if (store!=null) {
            result.setStatus(FORMSServiceConstants.OK);
            result.setObject(tstore);   
         } else {
            result.setStatus(FORMSServiceConstants.OTHER_ERROR);
         }
   
         return result;

      } catch (Exception e) {      

         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
         return result;

      }      

   }

   public SsaServiceObjectResult getInfoRecord(SsaServicePassObjectParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      SsaServiceObjectResult result=new SsaServiceObjectResult();

      try {

         String auserid=null;
         String mediainfoid=(String)parms.getObject();
         try {
            auserid=getAuserid(parms.getLuserid()).toString();
         } catch (Exception e2) {
            // Do nothing
         }

         PermHelper permhelper=new PermHelper(getRequest(),getResponse(),true);
   
         if (!permhelper.hasMediaPerm(auserid,mediainfoid,"OPENDOC")) {

            result.setStatus(FORMSServiceConstants.NO_PERMISSION);
            return result;

         }

         Mediainfo minfo=(Mediainfo)getMainDAO().execUniqueQuery(
            "select s from Mediainfo s join fetch s.supportedfileformats "
               + "where s.mediainfoid=" + mediainfoid
            );

         // Stored 

         if (minfo!=null) {
            result.setStatus(FORMSServiceConstants.OK);
            result.setObject(minfo);   
         } else {
            result.setStatus(FORMSServiceConstants.OTHER_ERROR);
         }
   
         return result;

      } catch (Exception e) {      

         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
         return result;

      }      

   }

}


