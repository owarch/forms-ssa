 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 

/*
 *  Program:  FormMetadataServiceTarget.java - Retrieve list of users having rights to a specific 
 *            study
 *                                                          MRH 04/2008
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;

public class FormMetadataServiceTarget extends FORMSServiceTarget {

   public FormMetadataServiceResult getFormMetadata(FormMetadataServiceParms parms) {

      try {

            FormMetadataServiceResult result=new FormMetadataServiceResult();
            List l=(List)parms.getList();
            java.util.Date lastupdate=parms.getLastupdate();
            if (lastupdate==null) {
               lastupdate=new java.util.Date(0);
            }
            Iterator i=l.iterator();
            // run updates for each relevant study
            List dtdefidlist=(List)new ArrayList();
            List datatabledeflist=(List)new ArrayList();
            List datatablecoldeflist=(List)new ArrayList();
            List formtypesidlist=(List)new ArrayList();
            List formtypeslist=(List)new ArrayList();
            List formstorelist=(List)new ArrayList();
            List formtypelistidlist=(List)new ArrayList();
            List formtypelistlist=(List)new ArrayList();
            List formpermlist=(List)new ArrayList();
            List formuserpermlist=(List)new ArrayList();
            List formtypeuserpermlist=(List)new ArrayList();
            StringBuilder studysb=new StringBuilder();
            StringBuilder formsb=new StringBuilder();
            // keep separate stringbuffer for permissions (permissions are modified without modifying form)

            /////////////////////////////////////////////////////
            // pull datatabledef & formtypelist separately by study //
            /////////////////////////////////////////////////////

            Long lanstid=(Long)getMainDAO().execUniqueQuery("select a.ainstid from Allinst a where a.islocal=true");

            while (i.hasNext()) {

               Linkedstudies ls=(Linkedstudies)i.next();
               long oriid=ls.getStudies().getStudyid();
               long thisid=ls.getRemotestudyid();
               // pull all datatabledef records for study (formha indicates study)
               List halist=getMainDAO().execQuery("select f.formhaid from Formha f " + 
                                                     " where f.studies.studyid=" + thisid);
               Iterator haiter=halist.iterator();                                      
               StringBuilder sb=new StringBuilder();
               while (haiter.hasNext()) {
                  Long haid=(Long)haiter.next();
                  // write id to stringbuffer
                  sb.append(haid.toString());
                  if (haiter.hasNext()) sb.append(",");
               }   
               // pull datatabledef records (Local forms only)
               List fdlist=getMainDAO().execQuery(
                  "select f from Datatabledef_transfer f where f.ainstid=" + lanstid + " and f.pformhaid in (" + sb + ")"
                  );
               // create list of current dtdefids to send back to server and list of
               StringBuilder fsb=new StringBuilder();
               Iterator fdi=fdlist.iterator();
               while (fdi.hasNext()) {
                  Datatabledef_transfer fd=(Datatabledef_transfer)fdi.next();

                  HashMap idmap=new HashMap();
                  idmap.put("studyid",oriid);
                  idmap.put("remotedtdefid",fd.getDtdefid());
                  dtdefidlist.add(idmap);

                  java.util.Date recmodtime=fd.getRecmodtime();
                  HashMap fdmap=new HashMap();
                  fdmap.put("studyid",oriid);
                  fdmap.put("datatabledef",fd);
                  datatabledeflist.add(fdmap);

                  // check if any attributes have been modified for form

                  // create local stringbuffer for data pulls
                  if (fsb.length()>0) fsb.append(",");
                  fsb.append(new Long(fd.getDtdefid()).toString());

               }

               if (studysb.length()>0) studysb.append(",");
               studysb.append(new Long(thisid).toString());

               if (formsb.length()>0) formsb.append(",");
               formsb.append(fsb.toString());

            }   

            /////////////////////////////////////////////////
            // everything else need not be pulled by study //
            /////////////////////////////////////////////////

            // Pull all datatablecoldef records
            if (formsb.length()>0) {
               List fslist=getMainDAO().execQuery(
                  "select f from Datatablecoldef_transfer f " +
                     "where f.dtdefid in (" + formsb + ")"
                   );
               Iterator fsi=fslist.iterator();
               while (fsi.hasNext()) {
                  Datatablecoldef_transfer fs=(Datatablecoldef_transfer)fsi.next();
                  java.util.Date recmodtime=fs.getRecmodtime();
                  if (recmodtime.after(lastupdate)) {
                     datatablecoldeflist.add(fs);
                  }
               }
            }

            // Pull all formtype records
            StringBuilder ftsb=new StringBuilder();
            if (formsb.length()>0) {
               List ftlist=getMainDAO().execQuery(
                  "select f from Formtypes_transfer f " +
                     "where f.dtdefid in (" + formsb + ")"
                   );
               Iterator fti=ftlist.iterator();
               while (fti.hasNext()) {
                  Formtypes_transfer ft=(Formtypes_transfer)fti.next();
                  formtypesidlist.add(ft.getFormtypeid());
                  ftsb.append(new Long(ft.getFormtypeid()).toString());
                  if (fti.hasNext()) ftsb.append(",");
                  java.util.Date recmodtime=ft.getRecmodtime();
                  if (recmodtime.after(lastupdate)) {
                     // get contenttype
                     long formformatid=ft.getFormformatid();
                     Formformats fformats=(Formformats)getMainDAO().execUniqueQuery(
                        "select f from Formformats f join fetch f.supportedfileformats " +
                           "where f.formformatid=" + formformatid
                           );
                     String contenttype=fformats.getSupportedfileformats().getContenttype();
                     //
                     HashMap ftmap=new HashMap();
                     ftmap.put("formtypes",ft);
                     ftmap.put("contenttype",contenttype);
                     formtypeslist.add(ftmap);
                  }
               }
            }

            // Pull all formstore records
            if (ftsb.length()>0) {
               List fslist=getMainDAO().execQuery(
                  "select f from Formstore_transfer f " +
                     "where f.formtypeid in (" + ftsb + ")"
                   );
               Iterator fsi=fslist.iterator();
               while (fsi.hasNext()) {
                  Formstore_transfer fs=(Formstore_transfer)fsi.next();
                  java.util.Date recmodtime=fs.getRecmodtime();
                  if (recmodtime.after(lastupdate)) {
                     formstorelist.add(fs);
                  }
               }
            }

            // pull all formtypelist records for study 
            if (studysb.length()>0) { 
               List ftllist=getMainDAO().execQuery(
                  "select f from Formtypelist_transfer f where f.ainstid=" + lanstid + " and f.studyid in (" + studysb + ") " +
                      "and visibility=" + VisibilityConstants.STUDY
                  );
               Iterator ftli=ftllist.iterator();
               while (ftli.hasNext()) {
                  Formtypelist_transfer ftl=(Formtypelist_transfer)ftli.next();
   
                  formtypelistidlist.add(ftl.getFormtypelistid());
   
                  java.util.Date recmodtime=ftl.getRecmodtime();
                  if (recmodtime.after(lastupdate)) {
                     formtypelistlist.add(ftl);
                  }
               }
            }

            /////////////////////////
            // PERMISSIONS SECTION //
            /////////////////////////

            // first, get users and their assigned roles for later use
            ArrayList roleuserlist=new ArrayList();
            if (studysb.length()>0) {
               List rual=getMainDAO().execQuery(
                  "select r from Rroleuserassign r join fetch r.user join fetch r.role " +
                           "where r.role.studyid in (" + studysb + ") "
                  );
               Iterator ruai=rual.iterator();
               while (ruai.hasNext()) {
                  Rroleuserassign rrua=(Rroleuserassign)ruai.next();
                  Iterator rui=roleuserlist.iterator();
                  boolean hasmatch=false;
                  while (rui.hasNext()) {
                     // add this role to current role list if one exists for the auserid in roleuserlist
                     HashMap map=(HashMap)rui.next();
                     if (((Long)map.get("auserid")).longValue()==rrua.getUser().getAuserid()) {
                        ArrayList ridlist=(ArrayList)map.get("rroleidlist");
                        ridlist.add(new Long(rrua.getRole().getRroleid()));
                        hasmatch=true;
                     }
                  }
                  if (!hasmatch) {
                     // if no record currently exists in roleuserlist, create one
                     HashMap map=new HashMap();
                     map.put("auserid",new Long(rrua.getUser().getAuserid()));
                     Long rroleid=new Long(rrua.getRole().getRroleid());
                     ArrayList ridlist=new ArrayList();
                     ridlist.add(rroleid);
                     map.put("rroleidlist",ridlist);
                     roleuserlist.add(map);
                  }
               }
            }

            //////////////////////
            // FORM PERMISSIONS //
            //////////////////////

            // NOTE:  Return only local installation perms and perms for calling installation
            Linkedinst callinginst=this.getCallingInst();

            // return user id information to calling installation for setting permissions there
            // NOTE:  Only sending permissions information for remote users (only they will access form through remote site)
            List ulist=getMainDAO().execQuery(
               "select a.auserid,a.luserid,a.allinst.islocal from Allusers a " +
                  "where a.allinst.linkedinst.linstid=" + callinginst.getLinstid()
               );
            List userinfolist=(List)new ArrayList();
            Iterator uiter=ulist.iterator();
            while (uiter.hasNext()) {
               Object[] oarray=(Object[])uiter.next();
               HashMap map=new HashMap();
               map.put("auserid",oarray[0]);
               map.put("luserid",oarray[1]);
               map.put("islocal",oarray[2]);
               userinfolist.add(map);
            }

            if (formsb.length()>0) {
   
               // get role-based permissions
               ArrayList formrolepermlist=new ArrayList();
               ArrayList formrolepermapplist=new ArrayList();
               // role permissions assigned by profile level
               List fppal=getMainDAO().execQuery(
                  "select f from Dtpermprofassign f join fetch f.dtpermprofiles " + 
                     "where f.dtdefid in (" + formsb + ") and " +
                           "f.dtpermprofiles.studyid in (" + studysb + ") and " +
                           "f.dtpermprofiles.profilelevel=" + VisibilityConstants.STUDY 
                  );
               List frppal=getMainDAO().execQuery(
                  "select f from Dtrolepermprofassign f join fetch f.dtpermprofiles join fetch f.resourceroles " +
                           "where f.dtpermprofiles.studyid in (" + studysb + ") and " +
                                 "f.dtpermprofiles.profilelevel=" + VisibilityConstants.STUDY + " and " +
                                 "f.resourceroles.studyid in (" + studysb + ") "
                  );
               Iterator fppai=fppal.iterator();
               while (fppai.hasNext()) {
                  Dtpermprofassign fppa=(Dtpermprofassign)fppai.next();
                  Iterator frppai=frppal.iterator();
                  while (frppai.hasNext()) {
                     Dtrolepermprofassign frppa=(Dtrolepermprofassign)frppai.next();
                     if (fppa.getDtpermprofiles().getDtprofileid()==frppa.getDtpermprofiles().getDtprofileid()) {
                        HashMap rmap=new HashMap();
                        rmap.put("dtdefid",new Long(fppa.getDtdefid()));
                        rmap.put("rroleid",new Long(frppa.getResourceroles().getRroleid()));
                        rmap.put("permlevel",frppa.getPermlevel());
                        formrolepermlist.add(rmap);
                     }
                  }
               }
               // role permissions assigned specifically
               List rpl=getMainDAO().execQuery(
                  "select f from Dtrolepermassign f join fetch f.resourceroles " +
                     "where f.dtdefid in (" + formsb + ") and " +
                           "f.resourceroles.studyid in (" + studysb + ") "
                  );
               Iterator rpi=rpl.iterator();
               while (rpi.hasNext()) {
                  Dtrolepermassign f=(Dtrolepermassign)rpi.next();
                  // replace value in existing list (profile-based) if one exists
                  boolean hasmatch=false;
                  Iterator frpi=formrolepermlist.iterator();
                  while (frpi.hasNext()) {
                     HashMap map=(HashMap)frpi.next();
                     if (
                           ((Long)map.get("dtdefid")).longValue()==f.getDtdefid() &&
                           ((Long)map.get("rroleid")).longValue()==f.getResourceroles().getRroleid() 
                        ) {
                        map.put("permlevel",f.getPermlevel());
                        hasmatch=true;
                     }
                  }
                  // otherwise add record to append list
                  if (!hasmatch) {
                     HashMap rmap=new HashMap();
                     rmap.put("dtdefid",new Long(f.getDtdefid()));
                     rmap.put("rroleid",new Long(f.getResourceroles().getRroleid()));
                     rmap.put("permlevel",f.getPermlevel());
                     formrolepermlist.add(rmap);
                     formrolepermapplist.add(rmap);
                  }
               }
   
               // add append list to rolelist
               formrolepermlist.addAll(formrolepermapplist);
   
               // Create user permission list based on role permissions

               // cycle through roleuserlist, then through dtrolepermassign to create form user permissions
               Iterator rui=roleuserlist.iterator();
               while (rui.hasNext()) {
                  HashMap map=(HashMap)rui.next();
                  Long auserid=(Long)map.get("auserid");
                  ArrayList ridlist=(ArrayList)map.get("rroleidlist");
                  Iterator riditer=ridlist.iterator();
                  while (riditer.hasNext()) {
                     Long rroleid=(Long)riditer.next();
                     // cycle through form assignments for rroleid add information to formuserpermlist
                     Iterator frpi=formrolepermlist.iterator();
                     while (frpi.hasNext()) {
                        HashMap fmap=(HashMap)frpi.next();
                        Long crroleid=(Long)fmap.get("rroleid");
                        if (crroleid.longValue()==rroleid.longValue()) {
                           Long dtdefid=(Long)fmap.get("dtdefid");
                           BitSet permlevel=(BitSet)fmap.get("permlevel");
                           // if value already exists in formuserpermlist, update it
                           boolean hasmatch=false;
                           Iterator formuserpermiter=formuserpermlist.iterator();
                           while (formuserpermiter.hasNext()) {
                              HashMap fupmap=(HashMap)formuserpermiter.next();
                              Long fauserid=(Long)fupmap.get("auserid");
                              Long fdtdefid=(Long)fupmap.get("dtdefid");
                              if (auserid.longValue()==fauserid.longValue() && dtdefid.longValue()==fdtdefid.longValue()) {
                                 hasmatch=true;
                                 BitSet fpermlevel=(BitSet)fupmap.get("permlevel");
                                 BitSet newset=(BitSet)fpermlevel.clone();
                                 for (int j=0;j<newset.size();j++) {
                                    if (newset.get(j)==false && permlevel.get(j)==true) {
                                       newset.flip(j);
                                    }
                                 }
                                 fupmap.put("permlevel",newset);
                              }
                           }
                           // otherwise, add a new one
                           if (!hasmatch) {
                              HashMap fupmap=new HashMap();
                              fupmap.put("auserid",auserid);
                              fupmap.put("dtdefid",dtdefid);
                              fupmap.put("permlevel",permlevel);
                              if (returnUser(userinfolist,auserid)) {
                                 formuserpermlist.add(fupmap);
                              }
                           }
                        }
                     }
                  }
               }
   
               // override role-based permission based on user-assigned permissions where available
               List fupal=getMainDAO().execQuery(
                  "select f from Dtuserpermassign f join fetch f.allusers " +
                     "where f.dtdefid in (" + formsb + ")"
                  );
               Iterator fupai=fupal.iterator();
               while (fupai.hasNext()) {
                  Dtuserpermassign fupa=(Dtuserpermassign)fupai.next();
                  Long pdtdefid=fupa.getDtdefid();
                  Long pauserid=fupa.getAllusers().getAuserid();
                  BitSet ppermlevel=(BitSet)fupa.getPermlevel();
   
                  // update or create new dtuserpermassign record as needed 
                  Iterator formuserpermiter=formuserpermlist.iterator();
                  boolean hasmatch=false;
                  while (formuserpermiter.hasNext()) {
                     HashMap fupmap=(HashMap)formuserpermiter.next();
                     Long fauserid=(Long)fupmap.get("auserid");
                     Long fdtdefid=(Long)fupmap.get("dtdefid");
                     if (fauserid.longValue()==pauserid.longValue() && 
                         fdtdefid.longValue()==pdtdefid.longValue()) {
                         hasmatch=true;
                         // use user-based permission instead of role-base one
                         fupmap.put("permlevel",ppermlevel);
                     }
                  }
                  // otherwise, add a new one
                  if (!hasmatch) {
                     HashMap fupmap=new HashMap();
                     fupmap.put("auserid",pauserid);
                     fupmap.put("dtdefid",pdtdefid);
                     fupmap.put("permlevel",ppermlevel);
                     if (returnUser(userinfolist,pauserid)) {
                        formuserpermlist.add(fupmap);
                     }
                  }
   
               }

               // IMPORTANT STEP:  All users must have a record for all forms to replace any existing records on client
               // installation.  Any users not assigned a specific permission is assigned to have no permissions to a form

               ArrayList appendlist=new ArrayList();
               String[] idarray=formsb.toString().split(",");
               Iterator idi=Arrays.asList(idarray).iterator();
               // create empty permission bitset (get length from permissions dataset)
               Integer setln=(Integer)getMainDAO().execUniqueQuery("select max(p.permpos) from Permissions p " +
                                                       "where p.permissioncats.pcatdesc='Datatable Permissions'");
               BitSet emptyperm=new BitSet(setln);
               while (idi.hasNext()) {
                  Long cdtdefid=new Long((String)idi.next());
                  Iterator ui=userinfolist.iterator();
                  while (ui.hasNext()) {
                     HashMap umap=(HashMap)ui.next();
                     Long causerid=(Long)umap.get("auserid");
                     Iterator pi=formuserpermlist.iterator();
                     boolean hasmatch=false;
                     while (pi.hasNext()) {
                        HashMap fupmap=(HashMap)pi.next();
                        Long c2dtdefid=(Long)fupmap.get("dtdefid");
                        Long c2auserid=(Long)fupmap.get("auserid");
                        if (cdtdefid.equals(c2dtdefid) && causerid.equals(c2auserid)) {
                           hasmatch=true;
                        }
                     }
                     if (!hasmatch) {
                        HashMap newmap=new HashMap();
                        newmap.put("auserid",causerid);
                        newmap.put("dtdefid",cdtdefid);
                        newmap.put("permlevel",emptyperm);
                        appendlist.add(newmap);
                     }
                  }
               }
               formuserpermlist.addAll(appendlist);

            }

            //////////////////////////////
            // FORMTYPELIST PERMISSIONS // 
            //////////////////////////////

            // create stringbuffer containing comma-separated formtypelistids
            StringBuilder ftlsb=new StringBuilder();
            Iterator ftili=formtypelistidlist.iterator();
            while (ftili.hasNext()) {
               Long id=(Long)ftili.next();
               ftlsb.append(id);
               if (ftili.hasNext()) {
                  ftlsb.append(",");
               }
            }

            if (ftlsb.length()>0 && studysb.length()>0) {

               // get role-based permissions
               ArrayList formtyperolepermlist=new ArrayList();
               // role permissions assigned specifically
               List rtpl=getMainDAO().execQuery(
                  "select f from Formtyperolepermassign f join fetch f.resourceroles " +
                     "where f.formtypelistid in (" + ftlsb + ") and " +
                           "f.resourceroles.studyid in (" + studysb + ") "
                  );
   
               Iterator rtpi=rtpl.iterator();
               while (rtpi.hasNext()) {
                  Formtyperolepermassign f=(Formtyperolepermassign)rtpi.next();
                  // replace value in existing list (profile-based) if one exists
                  boolean hasmatch=false;
                  // otherwise add record to append list
                  HashMap rmap=new HashMap();
                  rmap.put("formtypelistid",new Long(f.getFormtypelistid()));
                  rmap.put("rroleid",new Long(f.getResourceroles().getRroleid()));
                  rmap.put("permlevel",f.getPermlevel());
                  formtyperolepermlist.add(rmap);
               }
   
               // Create user permission list based on role permissions
   
               // cycle through roleuserlist, then through formtyperolepermassign to create formtype user permissions
               Iterator rui=roleuserlist.iterator();
               while (rui.hasNext()) {
                  HashMap map=(HashMap)rui.next();
                  Long auserid=(Long)map.get("auserid");
                  ArrayList ridlist=(ArrayList)map.get("rroleidlist");
                  Iterator riditer=ridlist.iterator();
                  while (riditer.hasNext()) {
                     Long rroleid=(Long)riditer.next();
                     // cycle through formtype assignments for rroleid add information to formtypeuserpermlist
                     Iterator frpi=formtyperolepermlist.iterator();
                     while (frpi.hasNext()) {
                        HashMap fmap=(HashMap)frpi.next();
                        Long crroleid=(Long)fmap.get("rroleid");
                        if (crroleid.longValue()==rroleid.longValue()) {
                           Long formtypelistid=(Long)fmap.get("formtypelistid");
                           BitSet permlevel=(BitSet)fmap.get("permlevel");
                           // if value already exists in formtypeuserpermlist, update it
                           boolean hasmatch=false;
                           Iterator formtypeuserpermiter=formtypeuserpermlist.iterator();
                           while (formtypeuserpermiter.hasNext()) {
                              HashMap fupmap=(HashMap)formtypeuserpermiter.next();
                              Long fauserid=(Long)fupmap.get("auserid");
                              Long fformtypelistid=(Long)fupmap.get("formtypelistid");
                              if (auserid.longValue()==fauserid.longValue() && formtypelistid.longValue()==fformtypelistid.longValue()) {
                                 hasmatch=true;
                                 BitSet fpermlevel=(BitSet)fupmap.get("permlevel");
                                 BitSet newset=(BitSet)fpermlevel.clone();
                                 for (int j=0;j<newset.size();j++) {
                                    if (newset.get(j)==false && permlevel.get(j)==true) {
                                       newset.flip(j);
                                    }
                                 }
                                 fupmap.put("permlevel",newset);
                              }
                           }
                           // otherwise, add a new one
                           if (!hasmatch) {
                              HashMap fupmap=new HashMap();
                              fupmap.put("auserid",auserid);
                              fupmap.put("formtypelistid",formtypelistid);
                              fupmap.put("permlevel",permlevel);
                              if (returnUser(userinfolist,auserid)) {
                                 formtypeuserpermlist.add(fupmap);
                              }
                           }
                        }
                     }
                  }
               }
   
               // override role-based permission based on user-assigned permissions where available
               List ftupal=getMainDAO().execQuery(
                  "select f from Formtypeuserpermassign f join fetch f.allusers " +
                     "where f.formtypelistid in (" + ftlsb + ")"
                  );
               Iterator ftupai=ftupal.iterator();
               while (ftupai.hasNext()) {
                  Formtypeuserpermassign fupa=(Formtypeuserpermassign)ftupai.next();
                  Long pformtypelistid=fupa.getFormtypelistid();
                  Long pauserid=fupa.getAllusers().getAuserid();
                  BitSet ppermlevel=(BitSet)fupa.getPermlevel();
   
                  // update or create new formtypeuserpermassign record as needed 
                  Iterator formtypeuserpermiter=formtypeuserpermlist.iterator();
                  boolean hasmatch=false;
                  while (formtypeuserpermiter.hasNext()) {
                     HashMap fupmap=(HashMap)formtypeuserpermiter.next();
                     Long fauserid=(Long)fupmap.get("auserid");
                     Long fformtypelistid=(Long)fupmap.get("formtypelistid");
                     if (fauserid.longValue()==pauserid.longValue() && 
                         fformtypelistid.longValue()==pformtypelistid.longValue()) {
                         hasmatch=true;
                         // use user-based permission instead of role-base one
                         fupmap.put("permlevel",ppermlevel);
                     }
                  }
                  // otherwise, add a new one
                  if (!hasmatch) {
                     HashMap fupmap=new HashMap();
                     fupmap.put("auserid",pauserid);
                     fupmap.put("formtypelistid",pformtypelistid);
                     fupmap.put("permlevel",ppermlevel);
                     if (returnUser(userinfolist,pauserid)) {
                        formtypeuserpermlist.add(fupmap);
                     }
                  }
   
               }

               // IMPORTANT STEP:  All users must have a record for all forms to replace any existing records on client
               // installation.  Any users not assigned a specific permission is assigned to have no permissions to a form

               ArrayList appendlist=new ArrayList();
               String[] idarray=ftlsb.toString().split(",");
               Iterator idi=Arrays.asList(idarray).iterator();
               // create empty permission bitset (get length from permissions dataset)
               Integer setln=(Integer)getMainDAO().execUniqueQuery("select max(p.permpos) from Permissions p " +
                                                       "where p.permissioncats.pcatdesc='Formtype Permissions'");
               BitSet emptyperm=new BitSet(setln);
               while (idi.hasNext()) {
                  Long cformtypelistid=new Long((String)idi.next());
                  Iterator ui=userinfolist.iterator();
                  while (ui.hasNext()) {
                     HashMap umap=(HashMap)ui.next();
                     Long causerid=(Long)umap.get("auserid");
                     Iterator pi=formtypeuserpermlist.iterator();
                     boolean hasmatch=false;
                     while (pi.hasNext()) {
                        HashMap fupmap=(HashMap)pi.next();
                        Long c2formtypelistid=(Long)fupmap.get("formtypelistid");
                        Long c2auserid=(Long)fupmap.get("auserid");
                        if (cformtypelistid.equals(c2formtypelistid) && causerid.equals(c2auserid)) {
                           hasmatch=true;
                        }
                     }
                     if (!hasmatch) {
                        HashMap newmap=new HashMap();
                        newmap.put("auserid",causerid);
                        newmap.put("formtypelistid",cformtypelistid);
                        newmap.put("permlevel",emptyperm);
                        appendlist.add(newmap);
                     }
                  }
               }
               formtypeuserpermlist.addAll(appendlist);


            }

            result.setDtdefidlist(dtdefidlist);
            result.setDatatabledeflist(datatabledeflist);
            result.setDatatablecoldeflist(datatablecoldeflist);
            result.setFormtypesidlist(formtypesidlist);
            result.setFormtypeslist(formtypeslist);
            result.setFormstorelist(formstorelist);
            result.setFormtypelistidlist(formtypelistidlist);
            result.setFormtypelistlist(formtypelistlist);
            result.setFormuserpermlist(formuserpermlist);
            result.setFormtypeuserpermlist(formtypeuserpermlist);
            result.setUserinfolist(userinfolist);
            return result;
      
      } catch (Exception e) {
            return null;
      }

   }

   public boolean returnUser(List userinfolist,Long userid) {

      Iterator i=userinfolist.iterator();
      while (i.hasNext()) {
         HashMap map=(HashMap)i.next();
         Long auserid=(Long)map.get("auserid");
         if (auserid.equals(userid)) {
            return true;
         }
      }
      return false;

   }

}


