 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 

/*
 *  Program:  InstInfoModTimeServiceTarget.java - Retrieve Allusers and Forms metadata mod times
 *            study
 *                                                          MRH 04/2008
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.sql.Timestamp;
import java.util.*;
import java.io.*;

public class InstInfoModTimeServiceTarget extends FORMSServiceTarget {

   // Return a HashMap (as an object) containing mod times
   public SsaServiceObjectResult getModTimes() {

      try {

            SsaServiceObjectResult result=new SsaServiceObjectResult();
            HashMap rmap=new HashMap();
            Timestamp amod=(Timestamp)getMainDAO().execUniqueQuery(
               "select max(a.tablemodtime) from Tablemodtime a where a.classname in " +
                  "('com.owarchitects.forms.commons.db.Allusers','com.owarchitects.forms.commons.db.Dtuserpermassign','com.owarchitects.forms.commons.db.Rroleuserassign'," +
                   "'com.owarchitects.forms.commons.db.Sysuserpermassign','com.owarchitects.forms.commons.db.Sysrolepermassign','com.owarchitects.forms.commons.db.Dtrolepermassign'," +
                   "'com.owarchitects.forms.commons.db.Linkedinst','com.owarchitects.forms.commons.db.Linkedstudies')"
               );
            if (amod==null) amod=new Timestamp(0);   
            rmap.put("usermodtime",amod);   
            Timestamp fmod=(Timestamp)getMainDAO().execUniqueQuery("select max(f.localmod) from Dtmodtime f");
            if (fmod==null) fmod=new Timestamp(0);   
            rmap.put("dtmodtime",fmod);   
            result.setObject((Object)rmap);
            return result;

      } catch (Exception e) {

            return null;
      }

   }

}





