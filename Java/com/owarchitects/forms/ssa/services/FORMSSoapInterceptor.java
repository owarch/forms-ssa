 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  FORMSSoapInterceptor.java - Main FORMS SOAP service.  Calls FORMS service target beans,
 *     passing input parameter class, and returns result parameter class to calling
 *     controller.   MRH 04/2007
 */
package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.db.*;
import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import java.util.*;
import java.io.*;
import java.beans.*;
import java.lang.reflect.*;
import java.security.*;
import javax.crypto.SecretKey;
import org.springframework.web.servlet.*;
import org.springframework.web.context.support.*;
import org.springframework.context.support.*;
import org.apache.axis2.context.*;
import org.apache.axis2.client.*;
import org.apache.axis2.transport.http.HTTPConstants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.lang.time.DateUtils;

public class FORMSSoapInterceptor {

   public String invokeMethod(String inobj) throws Throwable {

      MessageContext mc=MessageContext.getCurrentMessageContext();
      HttpServletRequest request=(HttpServletRequest) mc.getProperty(HTTPConstants.MC_HTTP_SERVLETREQUEST);
      HttpServletResponse response=(HttpServletResponse) mc.getProperty(HTTPConstants.MC_HTTP_SERVLETRESPONSE);
      HttpSession session=request.getSession();
      ServiceGroupContext sc=mc.getServiceGroupContext();
      AbstractApplicationContext ctx=SessionObjectUtil.getApplicationContext(session);
 
      // Retrieve object from encoded string
      Object parms=ObjectUtil.objectFromEncodedString(inobj);

      // Retrieve system information from passed object
      Class ic=parms.getClass();
      Method im;
      im=ic.getMethod("getBeanname");
      String beanname=(String)im.invoke(parms,new Object[] { });
      im=ic.getMethod("getMethodname");
      String methodname=(String)im.invoke(parms,new Object[]{ });

      try {

         ////////////////////////////
         // Remote SSA connections //
         ////////////////////////////

         im=ic.getMethod("getCodeword");
         String codeword=null;
         try {
            codeword=(String)im.invoke(parms,new Object[]{ });
         } catch (Exception cwe) {
            // Do Nothing (Local connection)
         }   
         im=ic.getMethod("getLuserid");
         String luserid=(String)im.invoke(parms,new Object[]{ });
         im=ic.getMethod("getConnectstring");
         String connectstring=(String)im.invoke(parms,new Object[]{ });
         im=ic.getMethod("getCheckstring");
         String checkstring=(String)im.invoke(parms,new Object[]{ });

         Linkedinst li=null;

         // remote server service calls
         if (codeword!=null) {

            // Establish connection to FORMSMainDB
            EmbeddedinstDAO embeddedinstDAO=(EmbeddedinstDAO)ctx.getBean("embeddedinstDAO");
            Embeddedinst ei=embeddedinstDAO.getRecordByCodeword(codeword);
        
            // get local installations public key for this site
            PrivateKey pk=EncryptUtil.getPrivateKey(session,"inst_loc_" + ei.getAcronym());
      
            connectstring=connectstring.replaceAll("\"","");
            connectstring=EncryptUtil.decryptLongString(connectstring,pk);
  
            Connectstring connstr=(Connectstring)ObjectXmlUtil.objectFromXmlString(connectstring);
   
            // refresh application context, obtaining connection to CSAMainDB
            SessionObjectUtil.readMainDBIntoContext(session,connstr.getDbpassword());
   
            // Pull linkedinst record from FORMSMainDB
      
            LinkedinstDAO linkedinstDAO=(LinkedinstDAO)ctx.getBean("linkedinstDAO");
            li=linkedinstDAO.getRecord(ei.getAcronym());

            // Make linkedinst record (information) accessible to session
            session.setAttribute("linkedinst",li);
            // Make luserid information accessible to session
            session.setAttribute("luserid",luserid);

         // local server service calls
         } else {

            Sysconn sysconn=(Sysconn)ObjectXmlUtil.objectFromXmlString(
               EncryptUtil.decryptString(connectstring,SessionObjectUtil.getInternalKeyObj(session))
               );
            SessionObjectUtil.readMainDBIntoContext(session,sysconn.getUsername(),sysconn.getPassword());

         } 

         // retrieve object stored as bean
         Object thisobj=ctx.getBean(beanname);
         // return object class
         Class c=thisobj.getClass();

         Method cm;
         try {
            // Call method with passed parameters
            cm=c.getMethod(methodname,new Class[] { ic });
         } catch (NoSuchMethodException nsme1) {
            try {
               // Call method with no parameters
               cm=c.getMethod(methodname,new Class[] { });
            } catch (NoSuchMethodException nsme1b) {
               // Call method passing Object
               cm=c.getMethod(methodname,new Class[] { Object.class });
            }
         }
         Class rc=cm.getReturnType();
         Object o=null;

         try {
   
            // Retrieve installation information record return if no matching installation
            if (li==null && codeword!=null) {
               o=rc.newInstance();
               Method rm=o.getClass().getMethod("setStatus",new Class[] { Integer.class });
               // Invoke method
               Object oj=rm.invoke(o,new Object[] { new Integer(FORMSServiceConstants.NO_MATCHING_INSTALLATION) } );
               return ObjectUtil.objectToEncodedString(oj);
            } 
    
            // Check security checkstring return if nonmatch
            if (o==null && !EncryptUtil.serviceCheckDecrypt(session,li,checkstring)) {
               o=rc.newInstance();
               Method rm=o.getClass().getMethod("setStatus",new Class[] { Integer.class });
               // Invoke method
               Object oj=rm.invoke(o,new Object[] { new Integer(FORMSServiceConstants.CHECKSTRING_ERROR) } );
               return ObjectUtil.objectToEncodedString(oj);
            }

            Method m;

            // set connection string
            m=c.getMethod("setSysconnString",new Class[] { String.class });
            m.invoke(thisobj,new Object[] { connectstring } );
   
            // Invoke target method
            m=c.getMethod("invokeTargetMethod",new Class[] { mc.getClass() , Object.class });
            return (String) m.invoke(thisobj,new Object[] { mc, (Object) parms } );
   
         } catch (Exception e) {

            o=rc.newInstance();
            Method rm=o.getClass().getMethod("setStatus",new Class[] { Integer.class });
            // Invoke method
            Object oj=rm.invoke(o,new Object[] { new Integer(FORMSServiceConstants.SERVICEINTERCEPTOR_EXCEPTION) } );
            return ObjectUtil.objectToEncodedString(oj);
   
         }

         //////////////////////////////////////////
         // END Remote SSA connection processing //
         //////////////////////////////////////////

      } catch (NoSuchMethodException nsme) {

         /////////////////////
         // CSA connections //
         /////////////////////
         
         LoginResult lresult=new LoginResult();

         im=ic.getMethod("getUsername");
         String user=(String)im.invoke(parms,new Object[]{ });
         im=ic.getMethod("getPassword");
         String passwd=(String)im.invoke(parms,new Object[]{ });
         im=ic.getMethod("getCsaid");
         Long csaid=(Long)im.invoke(parms,new Object[]{ });

         // refresh application context, obtaining connection to CSAMainDB
         SessionObjectUtil.readMainDBIntoContext(session,user,passwd);

         Authenticator cauth=new Authenticator(request,response);

         int hostresult=cauth.checkHost();

         if (hostresult==AuthenticatorConstants.HOST_DENIED) {

            lresult.setStatus(hostresult);
            return ObjectUtil.objectToEncodedString(lresult);

         } 

         lresult=cauth.login(user,passwd);

         // Ignore pw messages here, they will be handled on SSA

         if (
             lresult.getStatus()!=AuthenticatorConstants.LOGGED_IN &&
             lresult.getStatus()!=AuthenticatorConstants.PWEXPIRESSOON &&
             lresult.getStatus()!=AuthenticatorConstants.PWEXPIRED 
            ) {
            // return if not logged in
            return ObjectUtil.objectToEncodedString(lresult);
         }

         // Perform user request

         // retrieve object stored as bean
         Object thisobj=ctx.getBean(beanname);
         // return object class
         Class c=thisobj.getClass();
   
         Method cm;
         try {
            // Call method with passed parameters
            cm=c.getMethod(methodname,new Class[] { ic });
         } catch (NoSuchMethodException nsme2) {
            try {
               // Call method with no parameters
               cm=c.getMethod(methodname,new Class[] { });
            } catch (NoSuchMethodException nsme2b) {
               try {
               // Call method passing Object
               cm=c.getMethod(methodname,new Class[] { Object.class });
               } catch (Exception nsme2c) {
                  cm=c.getMethod(methodname,new Class[] { Object.class });
               }
            }
         }
         Class rc=cm.getReturnType();
         Object o=null;

         try {
   
            Method m=c.getMethod("invokeTargetMethod",new Class[] { mc.getClass() , Object.class });
   
            // Invoke method
            String returnstring=(String) m.invoke(thisobj,new Object[] { mc, (Object) parms } );
            // set objects to null to encourage garbage collection
            parms=null;
            cm=null;
            rc=null;
            m=null;
            thisobj=null;
            c=null;
            nsme=null;
            cauth=null;
            return returnstring;
   
         } catch (Exception e) {
   
            o=rc.newInstance();
            Method rm=o.getClass().getMethod("setStatus",new Class[] { Integer.class });
            // Invoke method
            Object oj=rm.invoke(o,new Object[] { new Integer(FORMSServiceConstants.SERVICEINTERCEPTOR_EXCEPTION) } );
            return ObjectUtil.objectToEncodedString(oj);
   
         }

         ///////////////////////////////////
         // END CSA connection processing //
         ///////////////////////////////////

      } catch (Exception othere) {

          throw new FORMSException("FORMSEX");

      } 

   }   

}


