 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  Program:  RunReportServiceTarget.java - Retrieve list of report files for which 
 *            user has a right 
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

public class RunReportServiceTarget extends FORMSStudyServiceTarget {

   com.sas.sasserver.submit.SubmitInterface sas;
   com.sas.sasserver.sclfuncs.SclfuncsV3Interface iscl;

   public static final int SAS=1;
   public static final int R=2;

   public RunReportResult runReport(SsaServicePassObjectParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      RunReportResult result=new RunReportResult();

      sas=SessionObjectUtil.getSasConnection(getRequest(),getResponse()).getSubmit();
      iscl=SessionObjectUtil.getSasConnection(getRequest(),getResponse()).getIscl();
      FORMSAuth auth=getAuth();

      try {

         String auserid=null;
         String reportinfoid=(String)parms.getObject();
         try {
            auserid=getAuserid(parms.getLuserid()).toString();
         } catch (Exception e2) {
            // Do nothing
         }

         PermHelper permhelper=new PermHelper(getRequest(),getResponse(),true);
   
         if (!permhelper.hasReportPerm(auserid,reportinfoid,"RUNREPORT")) {

            result.setStatus(FORMSServiceConstants.NO_PERMISSION);
            return result;

         }

         iscl.symput("m_status","INIT");
         Reportinfo rinfo=(Reportinfo)getMainDAO().execUniqueQuery(
            "select r from Reportinfo r where r.reportinfoid=" + reportinfoid
            );
         String desc=rinfo.getDescription();
         int rtype=rinfo.getResourcetype();
         List slist=getMainDAO().execQuery(
            "select s from Reportstore s join fetch s.reportinfo join fetch s.reportinfo.supportedfileformats "
             + "where s.reportinfo.reportinfoid=" + reportinfoid
            );
         iscl.symput("m_description",desc);
         Iterator i=slist.iterator();
         Reportstore pstore=null;
         Reportstore hstore=null;
         Reportstore lstore=null;
         while (i.hasNext()) {
            Reportstore store=(Reportstore)i.next();
            if (store.getContenttype()==ReportConstants.PROGFILE) {
               pstore=store;
               auth.setObjectValue("pstore",pstore);
            } else if (store.getContenttype()==ReportConstants.HTMLFILE) {
               hstore=store;
               auth.setObjectValue("hstore",hstore);
            } else if (store.getContenttype()==ReportConstants.FORMLETTER) {
               lstore=store;
               auth.setObjectValue("lstore",lstore);
            }
         }

         String contenttype=pstore.getReportinfo().getSupportedfileformats().getContenttype();
         if (contenttype.indexOf("-sas-syntax")>=0) {
            getAuth().setValue("progtype","SAS");
            sas=SessionObjectUtil.getSasConnection(getRequest(),getResponse()).getSubmit();
            iscl=SessionObjectUtil.getSasConnection(getRequest(),getResponse()).getIscl();
            iscl.symput("m_status","INIT");
         } else if (contenttype.indexOf("-r-syntax")>=0) {
            getAuth().setValue("progtype","R");
         } else {
            throw new FORMSException("Reporting Program Type Is Not Supported");
         }

         result.setDescription(desc);
         if (hstore!=null) {
            result.setRunstatus("CONTINUE");
            result.setObject(velocityResolve(hstore.getReportcontent()));

         } else {
            result.setRunstatus("DONE");
            if (lstore==null) {
               result.setObject(execReport(parms,pstore));
               auth.setObjectValue("hstore",null);
               auth.setObjectValue("pstore",null);
               auth.setObjectValue("lstore",null);
            } else {
               execReport(parms,pstore);
               result.setObject(processLetter(lstore));
               auth.setObjectValue("hstore",null);
               auth.setObjectValue("pstore",null);
               auth.setObjectValue("lstore",null);
            }
         }
         result.setStatus(FORMSServiceConstants.OK);
         return result;

      } catch (Exception e) {      

         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
         return result;

      }      

   }

   public RunReportResult submitReport(SsaServicePassObjectParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      RunReportResult result=new RunReportResult();

      FORMSAuth auth=getAuth();

      if (auth.getValue("progtype").equalsIgnoreCase("SAS")) {

         sas=SessionObjectUtil.getSasConnection(getRequest(),getResponse()).getSubmit();
         iscl=SessionObjectUtil.getSasConnection(getRequest(),getResponse()).getIscl();

      }   
   
      try {
   
         String resultstr=execReport(parms,(Reportstore)auth.getObjectValue("pstore"));
         String status=iscl.symget("m_status");    
         String desc=iscl.symget("m_description");    
         result.setDescription(desc);
         Reportstore lstore=null;
         if (status.equalsIgnoreCase("INIT") || status.equalsIgnoreCase("DONE")) {
            // execute sas process
            // clear stored objects
            try {
                lstore=(Reportstore)auth.getObjectValue("lstore");
            } catch (Exception lse) { }
            if (lstore!=null) {
               result.setObject(processLetter(lstore));
               auth.setObjectValue("hstore",null);
               auth.setObjectValue("pstore",null);
               auth.setObjectValue("lstore",null);
            } else {
               auth.setObjectValue("hstore",null);
               auth.setObjectValue("pstore",null);
               auth.setObjectValue("lstore",null);
               result.setObject(resultstr);
            }
         } else {
            // display next round of the html process
            // execute sas process
            result.setObject(velocityResolve(((Reportstore)auth.getObjectValue("hstore")).getReportcontent()));
         }
         result.setRunstatus(status);

         return result;

      } catch (Exception e) {      

         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
         return result;

      }      

   }      

   public String execReport(SsaServicePassObjectParms parms,Reportstore pstore) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      // call stat software specific methods based on program-type
      FORMSAuth auth=getAuth();
      if (auth.getValue("progtype").equalsIgnoreCase("SAS")) {
         return execSASReport(parms,pstore);
      } else if (auth.getValue("progtype").equalsIgnoreCase("R")) {
         return execRReport(parms,pstore);
      } else {
         throw new FORMSException("Reporting Program Type is not Supported");
      }

   }

   private String execSASReport(SsaServicePassObjectParms parms,Reportstore pstore) {

      try {
   
         // PASS PARAMETERS LIST FROM CALLING SERVER TO SAS
         try {
            HashMap passmap=(HashMap)parms.getObject();
            Iterator passiter=passmap.entrySet().iterator();
            StringBuilder pb=new StringBuilder();
            while (passiter.hasNext()) {
                Map.Entry entry=(Map.Entry)passiter.next();
                String name = (String)entry.getKey();
                // For multiple selection items, just returm space delimited set of values,
                // otherwise just return the single value.
                String[] values = (String[])entry.getValue();
                StringBuilder sv=new StringBuilder();
                for (int i=0;i<values.length;i++) {
                   sv.append(" "+values[i]);
                }
                String value=sv.toString().substring(1);
                pb.append("<br>" + name + "=" + value);
                iscl.symput(name,value);
            }
         } catch (ClassCastException cce) {
            // No Parameters
         }
   
         // INITIALIZE PREPROCESSING MACRO VARIABLES SO WE DONT PICK UP OLD ONES OR HAVE JAVA NULL VALUES
         iscl.symput("m_diagout","N");
         iscl.symput("m_socketout","Y");
   
         com.sas.servlet.util.SocketListener socket=new com.sas.servlet.util.SocketListener();
         int port=socket.setup();
         socket.start();
      
         // Set these in case of socket output
         iscl.symput("m_server",(java.net.InetAddress.getLocalHost()).getHostAddress());
         iscl.symput("m_port",new Integer(port).toString());
   
         String sasout=null;
      
         // Submit program text
         sas.setProgramText(pstore.getReportcontent());
   
         String socketout=iscl.symget("m_socketout").trim();
   
         if (socketout.equalsIgnoreCase("Y")) {
   
            ByteArrayOutputStream sos=new ByteArrayOutputStream();
            socket.write(sos);
      
            sasout=new String(sos.toByteArray());
   
         } else {
   
            sasout="<pre><xmp>" + sas.getLastOutputText() + "</xmp></pre>";
   
         }
   
         socket.close();
   
         if (!iscl.symget("m_diagout").trim().equals("Y")) {
            return sasout;
         } else {
            StringBuilder sb=new StringBuilder();
            sb.append("<br><pre>");
            sb.append(sas.getLastLogText());
            sb.append("<br></pre>");
            return "<br>" + sasout + sb.toString();
         } 

      } catch (Exception e) {
         return "ERROR:  Could not retrieve program results from SAS server" + "<br><br>" + Stack2string.getString(e);
      }

   }

   public String execRReport(SsaServicePassObjectParms parms,Reportstore pstore) {
       
      try {

         StringBuilder pb=new StringBuilder();
         try {
            // PASS PARAMETERS TO R AS GLOBAL VARIABLES
            HashMap passmap=(HashMap)parms.getObject();
            Iterator passiter=passmap.entrySet().iterator();
            while (passiter.hasNext()) {
                Map.Entry entry=(Map.Entry)passiter.next();
                String name = (String)entry.getKey();
                // For multiple selection items, just returm space delimited set of values,
                // otherwise just return the single value.
                String[] values = (String[])entry.getValue();
                // For multiple selection items, just returm space delimited set of values,
                // otherwise just return the single value.
                StringBuilder sv=new StringBuilder();
                for (int i=0;i<values.length;i++) {
                   sv.append(" "+values[i]);
                }
                String value=sv.toString().substring(1).trim();
                pb.append("\n" + name + "<<-\"" + value + "\"\n");
            }
         } catch (ClassCastException cce) {
            // No Parameters
         }
   
         FORMSRConnection rconn=new FORMSRConnection(getRequest(),getResponse());
      
         // Submit program text
         if (pb.length()>0) {
            rconn.submitProgram(pb.toString() + velocityResolve(pstore.getReportcontent()));
         } else {
            rconn.submitProgram(velocityResolve(pstore.getReportcontent()));
         }


         StringBuilder sb=new StringBuilder();
         sb.append("<html>\n");
         sb.append("<script>\n");
         sb.append("function appendText(node,txt)\n");
         sb.append("{\n");
         sb.append("  var ele=document.createElement('div');\n");
         sb.append("  ele.innerHTML=txt;\n");
         sb.append("  node.appendChild(ele);\n");
         sb.append("}\n");
         sb.append("</script>\n");
         sb.append("<body>\n");
         sb.append("<br><pre>\n");
         sb.append(rconn.getTextOutput());
         sb.append("<br></pre><br><br>\n");
         List imgNames=rconn.getImageOutput();
         if (imgNames.size()>0) {
            sb.append("<br><br><font color='#FF0000'>WARNING:  This report contains one or more images. &nbsp;" +
                         "FORMS currently does not support retrieval of images in remote-site reports.</font><br><br>");
         }
         sb.append("</body>\n");
         /*
         sb.append("<script>\n");
         Iterator i=imgNames.iterator();
         // User increasing setTimeout here to load images, otherwise servlet has trouble loading many images at once.   
         int ii=0;
         while (i.hasNext()) {
            ii++;
            String encImgName=EncryptUtil.encryptString((String)i.next(),
               SessionObjectUtil.getInternalKeyObj(getSession()));
            sb.append("<br>NOTE:  FORMS Currently does not support retrieving images in remote reports");
            if (!i.hasNext()) {
               ii++;
               sb.append("setTimeout(\"appendText(document.getElementsByTagName('body')[0],'<br><br>');\"," + ii + "*1000);\n");   
            }
         }
         sb.append("</script>\n");
         */
         sb.append("</html>\n");
         return sb.toString();
   
      } catch (Exception e) {
         return "<br><br>ERROR:  Could not retrieve program results from R server. &nbsp;RServe listener may not be running.<br><br>";
      }

   }


   public String processLetter(Reportstore lstore) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      String ltr=lstore.getReportcontent();
      try {
         // execute SAS to retrieve macro variable names
         org.apache.velocity.Template template = getVelocityTemplate("getlettervars.sas");
         StringWriter sout=new StringWriter();
         VelocityContext vcontext=new VelocityContext();
         template.merge(vcontext, sout);
         sas.setProgramText(sout.toString());
         String[] mvars=iscl.symget("m_ltrvars").split(" ");
         Iterator m=Arrays.asList(mvars).iterator();
         // Replace parameters in letter file with macro variable values
         while (m.hasNext()) {
            String mv=(String)m.next();
            ltr=ltr.replaceAll("&&" + mv.toLowerCase() + "&&",iscl.symget(mv)).replaceAll("&&" + mv.toUpperCase() + "&&",iscl.symget(mv)); 
         }
         return ltr;
      } catch (Exception e) {
         return ltr;
      }
   }

   private String velocityResolve(String ins) {
      try {
         StringWriter w=new StringWriter();
         VelocityEngine velocity=SessionObjectUtil.getVelocityEngine(session,this.getContext(),this.getAppPath());
         VelocityContext ctx=new VelocityContext();
         ctx.put("m_status",iscl.symget("m_status"));
         velocity.evaluate(ctx,w,"RunReport",ins);
         return w.toString();
         
      } catch (Exception e) {
         return ins;
      }
   }

}


