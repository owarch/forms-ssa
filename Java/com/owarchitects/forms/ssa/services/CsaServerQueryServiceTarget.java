 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  Program:  CsaServerQueryServiceTarget.java - execute queries from CSA to SSA & remote SSA's 
 *                                                          MRH 03/2008
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;

public class CsaServerQueryServiceTarget extends FORMSSsaServiceClientServiceTarget {

   // Execute unique SSA query

   public CsaServiceObjectResult execUniqueServerQuery(ServerQueryParms parms) {

      CsaServiceObjectResult result=new CsaServiceObjectResult();
      try {

         String querystring=parms.getQuerystring();

         // Return null for dis-allowed tables
         if (querystring.indexOf("Access") >=0 ||
             querystring.indexOf("Csainfo") >=0) {
            result.setObject(new Object[0]);
            result.setStatus(FORMSServiceConstants.OK);
            return result;
         }    
         Object[] resultobj=null;
         try {
            // execute constructed query
            String[] parmnames=parms.getParmnames();
            Object[] parmvalues=parms.getParmvalues();
            if (parmnames!=null && parmvalues!=null) {
               Object o=(Object)getMainDAO().execUniqueQuery(querystring.toString(),parmnames,parmvalues);
               try {
                  resultobj=(Object[])o;
               } catch (Exception e) {
                  Object[] oarray=new Object[1];
                  oarray[0]=o;
                  resultobj=oarray;
               }
            } else {
               Object o=(Object)getMainDAO().execUniqueQuery(querystring.toString());
               try {
                  resultobj=(Object[])o;
               } catch (Exception e) {
                  Object[] oarray=new Object[1];
                  oarray[0]=o;
                  resultobj=oarray;
               }
            }
         } catch (Exception rse) {
            // return null set
         }

         if (resultobj!=null) {
            // Return resulting data list
            result.setObject(resultobj);
            result.setStatus(FORMSServiceConstants.OK);
         } else {
            result.setStatus(FORMSServiceConstants.OTHER_ERROR);
         }

      } catch (Exception e) {

         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
      }

      return result;

   }

   // Execute unique Remote SSA query

   public CsaServiceObjectResult execUniqueRemoteServerQuery(ServerQueryParms parms) {

      CsaServiceObjectResult result=new CsaServiceObjectResult();
      try {

         Linkedinst linkedinst=parms.getLinkedinst();
         String querystring=parms.getQuerystring();

         // Return null for dis-allowed tables
         if (querystring.indexOf("Access") >=0 ||
             querystring.indexOf("Csainfo") >=0) {
            result.setObject(new Object[0]);
            result.setStatus(FORMSServiceConstants.OK);
            return result;
         }    
         Object[] resultobj=null;
         try {
            // execute constructed query
            String[] parmnames=parms.getParmnames();
            Object[] parmvalues=parms.getParmvalues();
            if (parmnames!=null && parmvalues!=null) {
               resultobj=(Object[])execUniqueRemoteQuery(linkedinst,querystring.toString(),parmnames,parmvalues);

            } else {
               resultobj=(Object[])execUniqueRemoteQuery(linkedinst,querystring.toString());
            }
         } catch (Exception rse) {
            // return null set
         }

         if (resultobj!=null) {
            // Return resulting data list
            result.setObject(resultobj);
            result.setStatus(FORMSServiceConstants.OK);
         } else {
            result.setStatus(FORMSServiceConstants.OTHER_ERROR);
         }

      } catch (Exception e) {

         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
      }

      return result;

   }

   // Execute SSA query

   public CsaServiceListResult execServerQuery(ServerQueryParms parms) {

      CsaServiceListResult result=new CsaServiceListResult();
      try {

         String querystring=parms.getQuerystring();

         // Return null for dis-allowed tables
         if (querystring.indexOf("Access") >=0 ||
             querystring.indexOf("Csainfo") >=0 ) { 
            result.setList(new ArrayList());
            result.setStatus(FORMSServiceConstants.OK);
            return result;
         }    
         List resultlist=null;
         try {
            // execute constructed query
            String[] parmnames=parms.getParmnames();
            Object[] parmvalues=parms.getParmvalues();
            if (parmnames!=null && parmvalues!=null) {
               resultlist=getMainDAO().execQuery(querystring.toString(),parmnames,parmvalues);

            } else {
               resultlist=getMainDAO().execQuery(querystring.toString());
            }
         } catch (Exception rse) {
            // return null set
         }

         if (resultlist!=null) {
            // Return resulting data list
            result.setList(resultlist);
            result.setStatus(FORMSServiceConstants.OK);
         } else {
            result.setStatus(FORMSServiceConstants.OTHER_ERROR);
         }

      } catch (Exception e) {

         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
      }

      return result;

   }

   // Execute Remote SSA query

   public CsaServiceListResult execRemoteServerQuery(ServerQueryParms parms) {

      CsaServiceListResult result=new CsaServiceListResult();
      try {

         Linkedinst linkedinst=parms.getLinkedinst();
         String querystring=parms.getQuerystring();

         // Return null for dis-allowed tables
         if (querystring.indexOf("Access") >=0 ||
             querystring.indexOf("Csainfo") >=0 ) { 
            result.setList(new ArrayList());
            result.setStatus(FORMSServiceConstants.OK);
            return result;
         }    
         List resultlist=null;
         try {
            // execute constructed query
            String[] parmnames=parms.getParmnames();
            Object[] parmvalues=parms.getParmvalues();
            if (parmnames!=null && parmvalues!=null) {
               resultlist=execRemoteQuery(linkedinst,querystring.toString(),parmnames,parmvalues);

            } else {
               resultlist=execRemoteQuery(linkedinst,querystring.toString());
            }
         } catch (Exception rse) {
            // return null set
         }

         if (resultlist!=null) {
            // Return resulting data list
            result.setList(resultlist);
            result.setStatus(FORMSServiceConstants.OK);
         } else {
            result.setStatus(FORMSServiceConstants.OTHER_ERROR);
         }

      } catch (Exception e) {

         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
      }

      return result;

   }

}

