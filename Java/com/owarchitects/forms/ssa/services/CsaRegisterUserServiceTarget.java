 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  Program:  CsaRegisterUserServiceTarget.java - Register CSA installation
 *                                                          MRH 01/2008
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;
import org.hibernate.*;
//import org.springframework.web.servlet.*;
//import org.springframework.web.servlet.mvc.*;
//import org.springframework.web.context.support.*;
import org.springframework.orm.hibernate3.*;
//import org.apache.commons.io.FileUtils;


public class CsaRegisterUserServiceTarget extends FORMSServiceTarget {

   public CsaRegisterUserResult register(CsaServiceSystemParms parms) {

      CsaRegisterUserResult result=new CsaRegisterUserResult();
      CsainfoDAO csainfoDAO=(CsainfoDAO)this.getContext().getBean("csainfoDAO");
      Csainfo info=null;

      try {
         info=csainfoDAO.getRecord(parms.getCsaid());
      } catch (Exception e) { }
      if (info==null) {
         result.setStatus(FORMSServiceConstants.OTHER_ERROR);
         return result;
      }

      result.setFkpassword(info.getFkpassword());
      result.setDbpassword(info.getDbpassword());
      result.setStatus(FORMSServiceConstants.OK);
      return result;

   }

}

