 
/**
 *
 * This program is part of:
 *
 *    FORMS [Forms-Oriented Research Management System]
 *
 * Copyright (C) 2009 OpenWare Architects.  All Rights Reserved.
 *
 * FORMS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FORMS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FORMS.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
/*
 *  Program:  GetReportServiceTarget.java - Retrieve list of report files for which 
 *            user has a right 
 */

package com.owarchitects.forms.ssa.services;

import com.owarchitects.forms.commons.comp.*;
import com.owarchitects.forms.ssa.comp.*;
import com.owarchitects.forms.commons.db.*;
import java.util.*;
import java.io.*;

public class GetReportServiceTarget extends FORMSStudyServiceTarget {

   // Retrieves a list of users who have any permissions on the current study

   public SsaServiceListResult getReportinfo(SsaServiceSystemStudyParms parms) {

      SsaServiceListResult result=new SsaServiceListResult();

      try {

         String auserid=null;
         try {
            auserid=getAuserid(parms.getLuserid()).toString();
         } catch (Exception e2) {
            // Do nothing
         }

         if (auserid!=null) {

            // pull list of permitted files for user (mplist)
            PermHelper helper=new PermHelper(getRequest(),getResponse());
            result.setStatus(new Integer(FORMSServiceConstants.OK));
            List mplist=helper.getReportPermList(auserid);
            List outlist=(List)new ArrayList();

            if (mplist.size()>0) {

               Iterator i=mplist.iterator();
               StringBuilder sb=new StringBuilder();
               while (i.hasNext()) {
                  HashMap map=(HashMap)i.next();
                  String reportinfoid=(String)map.get("reportinfoid");
                  sb.append(reportinfoid);
                  if (i.hasNext()) sb.append(",");
               }
               // Pull all file records in one query

               List reportlist=getMainDAO().execQuery(
                  "select rinfo from Reportinfo rinfo where rinfo.reportinfoid in (" + sb + ")" 
               );

               // add file records to mplist
               i=mplist.iterator();
               while (i.hasNext()) {
                  HashMap map=(HashMap)i.next();
                  String reportinfoid=(String)map.get("reportinfoid");
                  Iterator fiter=reportlist.iterator();
                  while (fiter.hasNext()) {
                     Reportinfo rinfo=(Reportinfo)fiter.next();
                     if (rinfo.getReportinfoid()==new Long(reportinfoid).longValue()) {
                        outlist.add(rinfo);
                        fiter.remove();
                     }
                  }
               }

            }

            result.setList(outlist);
            return result;

         } else {

            // User has no study permissions
            result.setStatus(new Integer(FORMSServiceConstants.OK));
            result.setList((List)new ArrayList(0));
            return result;

         }

      } catch (Exception e) {

         result.setStatus(new Integer(FORMSServiceConstants.SERVICETARGET_EXCEPTION));
         return result;

      }
   }

   public SsaServiceObjectResult getContent(SsaServicePassObjectParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      SsaServiceObjectResult result=new SsaServiceObjectResult();

      try {

         String auserid=null;
         String reportinfoid=(String)parms.getObject();
         try {
            auserid=getAuserid(parms.getLuserid()).toString();
         } catch (Exception e2) {
            // Do nothing
         }

         PermHelper permhelper=new PermHelper(getRequest(),getResponse(),true);
   
         if (!permhelper.hasReportPerm(auserid,reportinfoid,"OPENDOC")) {

            result.setStatus(FORMSServiceConstants.NO_PERMISSION);
            return result;
         }

         Reportstore store=(Reportstore)getMainDAO().execUniqueQuery(
            "select s from Reportstore s join fetch s.reportinfo join fetch s.reportinfo.supportedfileformats "
               + "where s.reportinfo.reportinfoid=" + reportinfoid
            );

         // Stored 
         if (store!=null) {
            result.setStatus(FORMSServiceConstants.OK);
            result.setObject(store);   
         } else {
            result.setStatus(FORMSServiceConstants.OTHER_ERROR);
         }
   
         return result;

      } catch (Exception e) {      

         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
         return result;

      }      

   }

   public SsaServiceObjectResult getInfoRecord(SsaServicePassObjectParms parms) throws FORMSException,FORMSKeyException,FORMSSecurityException {

      SsaServiceObjectResult result=new SsaServiceObjectResult();

      try {

         String auserid=null;
         String reportinfoid=(String)parms.getObject();
         try {
            auserid=getAuserid(parms.getLuserid()).toString();
         } catch (Exception e2) {
            // Do nothing
         }

         PermHelper permhelper=new PermHelper(getRequest(),getResponse(),true);
   
         if (!permhelper.hasReportPerm(auserid,reportinfoid,"OPENDOC")) {

            result.setStatus(FORMSServiceConstants.NO_PERMISSION);
            return result;

         }

         Reportinfo rinfo=(Reportinfo)getMainDAO().execUniqueQuery(
            "select s from Reportinfo s join fetch s.supportedfileformats "
               + "where s.reportinfoid=" + reportinfoid
            );

         // Stored 

         if (rinfo!=null) {
            result.setStatus(FORMSServiceConstants.OK);
            result.setObject(rinfo);   
         } else {
            result.setStatus(FORMSServiceConstants.OTHER_ERROR);
         }
   
         return result;

      } catch (Exception e) {      

         result.setStatus(FORMSServiceConstants.SERVICETARGET_EXCEPTION);
         return result;

      }      

   }

}


